<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que procesa la inclusión del voto en el tipo encuesta
*/
if(!isset($carga)){
  $cargaA =false;
  exit;
}
if($carga!="OK"){
  exit;
}else{
  include('../private/inc_web/seguri.php');
require ("../private/basicos_php/funcion_control_votacion.php");

$datos_votado = "";
$cadena = "";
?>
<link href="temas/<?php echo "$tema_web"; ?>/css/encuestaStyle.css" rel="stylesheet">


                    <!--Comiezo-->
                    <div class="card-header-votaciones ">  </div>


<div class="card-body">
                    <?php
                    if (ISSET($_POST["add_voto"])) {

                        $id_votante = $_SESSION['ID'];
                        $idvot = fn_filtro_numerico($con, $idvot);
                        $id_provincia = fn_filtro_numerico($con, $_POST['id_provincia']);
                        $id_ccaa = $_SESSION['id_ccaa_usu']; /// este esta mal??
                        if (isset($valores)) {
                            $valores = fn_filtro($con, $_POST['valores']);
                        }//// posiblemente sobre
                        $id_ccaa = fn_filtro_numerico($con, $_POST['id_ccaa']);   ///o este esta mal??
                        $id_subzona = fn_filtro($con, $_POST['id_subzona']);
                        $id_grupo_trabajo = fn_filtro($con, $_POST['id_grupo_trabajo']);
                        $demarcacion = fn_filtro($con, $_POST['demarcacion']); //necesario
                        $clave_seg = fn_filtro($con, $_POST['clave_seg']);

                        reset($_POST);
                        unset($_POST["add_voto"],$_POST['id_provincia'],$_POST['id_ccaa'],$_POST['id_subzona'],$_POST['id_grupo_trabajo'], $_POST['demarcacion'], $_POST['clave_seg']);

                        foreach ($_POST as $k => $v){
                            $a[] = $v;
                          }
                          if (isset($a)){
                            $datos = count($a);
                          }else{
                            $datos = 0;
                          }
                        //$datos = count($a) - 7;
                      //  $datos = count($a);
                        if ($datos == 0) {
                            echo '<div class="alert alert-danger">  <strong>' . _("Hay un error, no ha seleccionado ninguna opción") .  ' <a class=" btn btn-info btn-sm" href="votaciones.php?c='. encrypt_url('vota_encuesta/vota_encuesta/idvot='.$idvot,$clave_encriptacion). '">' . _("volver") . '</a></strong></div>';
                        } else {



                            $sql3 = "SELECT  seguridad,nombre_votacion FROM $tbn1 WHERE ID ='$idvot' ";
                            $resulta3 = mysqli_query($con, $sql3) or die("error: " . mysqli_error());

                            while ($listrows3 = mysqli_fetch_array($resulta3)) {
                                $seguridad = $listrows3['seguridad'];
                                $nombre_votacion = $listrows3['nombre_votacion'];
                            }



                            if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                            } elseif (isset($_SERVER['HTTP_VIA'])) {
                                $ip = $_SERVER['HTTP_VIA'];
                            } elseif (isset($_SERVER['REMOTE_ADDR'])) {
                                $ip = $_SERVER['REMOTE_ADDR'];
                            }

                            $forma_votacion = 3;

//$error = FALSE;


                            if ($clave_seg == "" and ($seguridad==2 or $seguridad==4)) {
                                echo " <div class=\"alert alert-danger\">
   <strong>" . _("Falta la clave de seguridad") . " <br>" . _("vuelva a realizar la votación") . "</strong></div>";
                            } else {



                                list ($estado, $razon, $tipo_votante) = fn_mira_si_puede_votar($demarcacion, $_SESSION['ID'], $idvot, $id_ccaa, $id_provincia, $id_grupo_trabajo, $id_municipio);

/////////////////////////// si podemos procesar el formulario

                                if ($estado == "error") {
                                    if ($razon == "direccion_no_existe") {
                                        echo "<div class=\"alert alert-danger\">
   <strong>" . _("Esta direccion de correo no la tenemos registrada para esta provincia, quizas sea un error de nuestra base de datos , si consideras que tienes derecho a votar haz  enviarnos tus datos a traves de nuestro formulario") . "</a></strong></div>";
                                    }
                                    if ($razon == "ya_ha_votado") {
                                        echo "<div class=\"alert alert-danger\">
   <strong>" . _("Ya ha votado en esta votación") . "</strong></div>";
                                    }
                                } else if ($estado == "TRUE" and $razon == "usuario_ok") {
                                    $codi = hash("sha512", $clave_seg);


                                    $i = 0;

                                    while ($i < $datos) {
                                        $val = fn_filtro($con, $a[$i]);

                                    if($votoPonderado == true and ($tipo ==5 or $tipo ==6)){
                                        if($_SESSION['tipo_votante']==1){
                                          $vot = $valor_tipo_1;
                                        }else if($_SESSION['tipo_votante']==2){
                                          $vot = $valor_tipo_2;
                                        } else{
                                          $vot = $valor_tipo_3;
                                        }
                                      }else{
                                        $vot = 1;
                                      }


                                        //$datos_votado.="Orden $voto  | Identificador candidato -->  $val |  Valor voto ---  $vot"."<br/>"; //cojemos el array de votos para enviar por correo si es necesario

                                        $datos_votado .= $val . "-" . $vot . "#"; //cojemos el array de votos para enviar por correo si es necesario

                                        $i++;
                                    }
                                    ///// identificador para el usuario
                                    $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
                                    $cad = "";
                                    for ($i = 0; $i < 4; $i++) {
                                        $cad .= substr($str, rand(0, 62), 1);
                                    }
                                    $time = microtime(true);
                                    $timecad = $time . $cad;
                                    $vote_id = hash("sha256", $timecad);

                                    //////	identofocador unico
                                    $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
                                    $cad = "";
                                    for ($b = 0; $b < 4; $b++) {
                                        $cad .= substr($str, rand(0, 62), 1);
                                    }
                                    $time = microtime(true);
                                    $timecad = $time . $cad;
                                    $res_id = hash("sha256", $timecad);
                                    /// finidentificador unico

                                    $datos_del_voto = trim($datos_votado, '#'); ///quitamos la ultima coma de la cadena para meterlo en la bbdd

                                    $insql = "insert into $tbn10 (ID,voto,id_candidato,id_provincia,id_votacion,codigo_val,vote_id) values (\"$res_id\",\"$vot\",\"$datos_del_voto\"," . $_SESSION['localidad'] . ",\"$idvot\",\"$codi\",\"$vote_id\")";
                                    $mens = "mensaje añadido";
                                    $result = db_query($con, $insql, $mens);

                                    if (!$result) {
                                        echo "<div class=\"alert alert-danger\">
						   			<strong> UPSSS!!!!<br/>" . _("esto es embarazoso, hay un error y su votacion no ha sido registrada") . "  </strong> </div><strong>";
                                    }
                                    if ($result) {

                                        // ALTA del usuario en la lista de los que han votado para que no pueda volver a votar
                                        $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
                                        $cad = "";
                                        for ($i = 0; $i < 6; $i++) {
                                            $cad .= substr($str, rand(0, 62), 1);
                                        }
                                        $time = microtime(true);
                                        $timecad2 = $time . $cad;
                                        $user_id = hash("sha256", $timecad2);
                                        if ($seguridad==3 or $seguridad==4){
                                          $fecha_env = date("Y-m-d 00:00:01"); // si la votación lleva envios de correos a los interventores, quitamos la hora para que no se pueda vincular la hora del voto
                                        }else{
                                          $fecha_env = date("Y-m-d H:i:s");
                                        }
                                        $insql = "insert into $tbn2 (ID,id_provincia,id_votacion,id_votante,fecha,tipo_votante,ip,forma_votacion) values (\"$user_id\"," . $_SESSION['localidad'] . ",\"$idvot\"," . $_SESSION['ID'] . ",\" $fecha_env\",\" $tipo_votante\",\" $ip\",\" $forma_votacion\")";
                                        $mens = "<br/>¡¡¡ATENCION!!!!, " . _("el voto ha sido registrado , pero el usuario no ha sido bloqueado") . " <br> " . _("el ID de usuario es") . ":" . $_SESSION['ID'];
                                        $result = db_query($con, $insql, $mens);


                                        ////metemos el voto en un txt y lo registramos como medida de seguridad
                                        $Pollname = md5($idvot);
                                        $file = $FilePath . $Pollname . "_ballots.txt";
                                        $fh = fopen($file, 'a');
                                        fwrite($fh, $datos_votado . PHP_EOL) or die("Could not write file!");
                                        fclose($fh);  //  Cerramos el fichero

                                        $file2 = $FilePath . $Pollname . "_tally.txt";
                                        $fh2 = fopen($file2, 'r+');
                                        $búfer = fgets($fh2, 100);
                                        $partes = explode("|", $búfer);
                                        $partes1 = $partes[1] + 1;
                                        $partes = $partes[0] . "|" . $partes1;
                                        fseek($fh2, 0);
                                        fwrite($fh2, $partes) or die("Could not write file!");
                                        fclose($fh2);  //  Cerramos el fichero
                                        //// metemos el dato de que ha votado en la base de datos
                                        echo "<h4>". _("Ha sido recogido el voto de") . " : " . $_SESSION['nombre_usu'] . "</h4>";

                                        ///// metemos una copia encriptada del voto
                                        //// primero sacamos los datos de la tabla temporal para hacer una cadena quemeteremos tambien encriptada
                                        $sql = "SELECT 	ID, datos  FROM $tbn20 where id_votacion=\"$Pollname\" ORDER BY ID DESC";
                                        $mens = _("Error en la consulta a la tabla temporal");
                                        $result = db_query($con, $sql, $mens);
                                        if ($row = mysqli_fetch_array($result)) {
                                            mysqli_field_seek($result, 0);
                                            do {
                                                $cadena .= $row[1] . "_";  ////generamos la caena que vamos a meter en la tabla de votos de seguridad
                                                $id_borrado = $row[0];
                                            } while ($row = mysqli_fetch_array($result));
                                        }


                                        $cadena = trim($cadena, '_'); // quitamos de la cadena el ultimo _
                                        //$id_borrado=$id_borrado-4; ///miramos el id del ultimo registro de la tabla temporal y descontamos 4 para su posterior borrado

                                        $borrado = "DELETE FROM $tbn20 WHERE ID=" . $id_borrado . " ";
                                        $mens = " error en el borrado de " . $id_borrado;
                                        $result = db_query($con, $borrado, $mens);

                                        $cadena_temp = $res_id . "+" . $datos_del_voto; //esta variable sirve para esta querry y para la querry de la tabla temporal siguiente
                                        /////// miramos si hay que encriptar la cadena de voto
                                        /* if ($encripta=="SHA"){
                                          $cadena_temp = hash('sha256', $cadena_temp);
                                          }else */
                                        if ($encripta == "si") {

                                            /* require_once "Crypt/AES.php"; */

                                            //Function for encrypting with RSA
                                            // hacemos una busqueda y cargamos la clave publica
                                            $result = mysqli_query($con, "SELECT clave_publica FROM $tbn21  WHERE id_votacion='$idvot' ORDER by orden");
                                            if ($row = mysqli_fetch_array($result)) {
                                                mysqli_field_seek($result, 0);
                                                do {
                                                    $clave_publica = $row[0];

                                                    $cadena = rsa_encrypt($cadena, $clave_publica);
                                                } while ($row = mysqli_fetch_array($result));
                                            }
                                        }
                                        //metemos los datos codificados en la bbdd
                                        $ident = md5($timecad2);

                                        $shadatovoto = hash('sha256', $cadena_temp); // encriptamos el identificador del voto que se ha incluido + la cadena de lo que ha votado para su posterior comparación cuando comprobamos el voto
                                        $insql1 = "insert into $tbn19 (ID,voto,cadena,id_votacion) values (\"$ident\",\"$shadatovoto\",\"$cadena\",\"$Pollname\")";
                                        $mens1 = "ERROR en el añadido voto encriptado ";
                                        $result = db_query($con, $insql1, $mens1);

                                        //metemos el dato en la tabla temporal
                                        $cadena_temp = $res_id . "+" . $datos_del_voto;
                                        $insql2 = "insert into $tbn20 (datos,id_votacion) values (\"$cadena_temp\",\"$Pollname\")";
                                        $mens2 = "ERROR en el añadido  del voto tabla temporal";
                                        $result = db_query($con, $insql2, $mens2);



                                        //////////////////////metemos la seguridad del envio de correos a interventores

                                        if ($seguridad == 3 or $seguridad == 4) {
                                            include('../private/basicos_php/envio_interventores.php');
                                        }
                                        ///////// fin envio a interventores
                                        ?><!--si todo va bien damos las gracisa por participar-->
                                        <div class="alert alert-success">
                                            <h3  align="center"><?= _("Gracias por participar") ?></h3>
                                            <strong><?= _("En  breve estaran los resultados") ?> </strong></div>
                                        <p> <?= _("Guarde este codigo de su voto si quiere comprobar que es correctamente contabilizado") ?>
                                        </p>
                                        <div class='alert alert-success'>
                                            <p> <?php echo $res_id ?> </p>
                                        </div>
                                        <?php
                                    }
                                }
                            }
                        }
                    }
                    ?>
</div>

                    <!--Final-->
<?php } ?>

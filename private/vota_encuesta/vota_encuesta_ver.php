<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo con el formulario para comprobar, metiendo la clave que se puso, si el voto está correctamente recogido
*/
if(!isset($carga)){
  $cargaA =false;
  exit;
}
if($carga!="OK"){
  exit;
}else{
  include('../private/inc_web/seguri.php');

?>
<link href="temas/<?php echo "$tema_web"; ?>/css/encuestaStyle.css" rel="stylesheet">

                    <!--Comiezo-->
                    <div class="card-header-votaciones "> <h1 class="card-title"><?php echo "$nombre_votacion"; ?></h1> </div>


<div class="card-body">

                    <?php if($resumen!=""){?>
                                    <div class="card">
                                      <div class="card-body">
                                        <?php echo "$resumen";?>
                                      </div>
                                    </div>
                                        <?php
                                      }
                                      ?>

                    <?php
                    if ($seguridad == 2 or $seguridad == 4) {
                        ?>
                        <form id="form1" name="form1" method="post"  class="well" action="votaciones.php?c=<?php echo encrypt_url('vota_encuesta/resultados_encuesta_ver/idvot='.$idvot,$clave_encriptacion) ?>">
                          <p class="text-info"> <?= _("Puede verificar su voto, para ello es necesario que ponga la clave que uso al votar") ?></p> <br />
                            <label for="clave_seg"><?= _("Clave de seguridad") ?></label>
                            <input type="text" name="clave_seg" id="clave_seg" />

                            <br />
                            <input type="submit"  class="btn btn-lg btn-primary btn-block"  name="Verificar" id="Verificar" value="<?= _("Enviar") ?>" />
                            <p>&nbsp;</p>
                        </form>
                    <?php } ?>
</div>
                    <!--Final-->
<?php } ?>

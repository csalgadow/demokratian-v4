<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
###############################################################################################################################################################
###############################################################################################################################################################
##########                                                                                                                                           ##########
##########                                           CONFIGURACIÓN del BLOG de la PLATAFORMA DE VOTACIONES                                           ##########
##########                                                   Software creado por Carlos Salgado                                                      ##########
##########                                                                                                                                           ##########
###############################################################################################################################################################
###############################################################################################################################################################

/**
* Archivo con todas las variables de la CONFIGURACIÓN del BLOG
*/

##################################################################################################################################
###############                                 variables del menu de redes sociales                               ###############
##################################################################################################################################
$url_facebook = "";
$url_twitter = "";
$url_Google = "";
$url_Linkedin = "";
$url_Instagram = "";
$url_Pinterest = "";
$url_Youtube = "";
$url_Slack = "";
$url_tumblr = "";
$url_Email = "";

$active_facebook = true;
$active_twitter = false;
$active_Google = false;
$active_Linkedin = false;
$active_Instagram = false;
$active_Pinterest = false;
$active_Youtube = false;
$active_Slack = false;
$active_tumblr = false;
$active_Email = false;

##################################################################################################################################
###############                                      variables del pie de pagina                                   ###############
##################################################################################################################################
$active_pie_pagina = true;
$texto_pie_pagina = "Sistema de votaciones demokratian";

##################################################################################################################################
###############                                        Otras variables                                             ###############
##################################################################################################################################
$limit_noticias=3;  //numero maximo de notcias en la primera zona_pagina
$limit_palabras_noticia=30;  //numero maximo de palabras en la pagina  index  en las noticias

?>

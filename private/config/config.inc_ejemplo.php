<?php
/*#############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
#############################################################################################################################################################*/

/**
* Archivo de Archivo de ejemplo, por si se realiza la instalación de forma manual, con todas las variables de la CONFIGURACIÓN de la PLATAFORMA
*/

###############################################################################################################################################################
###############################################################################################################################################################
##########                                                                                                                                           ##########
##########                                                 CONFIGURACIÓN del la PLATAFORMA DE VOTACIONES                                             ##########
##########                                                   Software creado por Carlos Salgado                                                      ##########
##########                                                                                                                                           ##########
###############################################################################################################################################################
###############################################################################################################################################################

$hostu = "usuario";                              // Usuario de la BBDD
$hostp = "password";                          // Contraseña de la BBDD
$dbn = "votaciones_demokratian";              // Nombre de la BBDD
$host = "host";                            // localhost de la BBDD
$extension = "dk_";                             // prefijo de las tablas de la base de datos

$url_vot = "http://localhost/pruebas_2.2.x";      // Url donde instalamos nuestra aplicación
$carpetaPublica="public_html";                   //nombre de la carpeta publica
##################################################################################################################################
###############     variables del sistema de subida y/o redimensionado de imágenes de los candidatos y roots       ###############
###############                    Ojo, darle permisos de escritura en el servidor si corre en Linux               ###############
##################################################################################################################################

$upload_cat = "data/upload_pic";          //carpeta donde se guardan las imágenes de los candidatos
$upload_user = "data/upload_user";        //carpeta donde se guardan las imágenes de los usuarios
$baseUrl = "data/userfile/";      //   carpeta donde se guardan las imágenes y archivos de gestor ckeditor
##################################################################################################################################
###############                                        configuración de carpetas                                   ###############
###############                    Ojo, darle permisos de escritura en el servidor si corre en Linux               ###############
##################################################################################################################################

$FileRec = "../data_rec/";                  //   carpeta donde se generan los archivos de recuento
$FilePath = "../data_vut/";                 //   carpeta donde se generan los archivos del vut
$path_bakup_bbdd = "../admin/backup";       // Carpeta donde se guardan los back-up de la bbdd
###################################################################################################################################
#############                       Todo este grupo de variables no tienen porque ser modificadas.                    #############
#############                                 hacerlo solo si se tiene conocimientos                                  #############
###################################################################################################################################

$b_debugmode = 0; // 0 || 1  Forma de errores cuando hay problemas con la base de datos
##################################################################################################################################
#####################                               Otras variables del sistema                             #####################
##################################################################################################################################
$tiempo_session = 900;  // tiempo de caducidad de la sesión en segundos 900 son 15 minutos

$usuarios_sesion = "nqYnDSrLAF"; // nombre de la sesion
$usuarios_sesion2 = "nxLRDqEMNJ"; // nombre de la sesion de los interventores
$clave_encriptacion = "WuSeGpLempg"; //
$clave_encriptacion2 = "jOmGjIxXDZA";


###################################################################################################################################
##########                                               Configuración sitio                                             ##########
###################################################################################################################################


$nombre_web = "DEMOKRATIAN |  Centro de votaciones";    // Nombre del sitio web
$tema_web = "demokratian";                       // Nombre del tema (carpeta donde se encuentra)
$info_versiones = true;         //sistema de avisos de nuevas versiones, por defecto habilitado (true)
$timezone = false;    // zona horaria
$defaul_lang = "es_ES";  //idioma por defecto
$dir_lang = "locale"; //sitio donde guardamos los idiomas por defecto
###################################################################################################################################
##########                                 Configuración para uso municipal                                              ##########
###################################################################################################################################
$es_municipal = true;  // true si vamos a trabajar solo con un tipo de circunscripción y no queremos que pregunte la provincia
//Si se usa la opción de correos institucionales debe de ser true
###################################################################################################################################
##########              Configuración para uso de correos institucionales de empresas u organizaciones                   ##########
###################################################################################################################################
$insti = false;   //si usa el sistema de inscripción de correos institucionales, por defecto false
$term_mail = "";    // terminación del correo institucional incluyendo la arroba, por defecto vacio
$term_empre = "";    // Nombre de la empresa
###################################################################################################################################
##########                                 Configuración del uso de recaptcha                                            ##########
###################################################################################################################################
$reCaptcha = false;  // true si ponemos en marcha el recaptcha
$tipo_reCaptcha = "reCAPTCHA_v2";   // si queremos usar reCaptchaV2 o V3
$reCAPTCHA_site_key = "";        //Clave publica que proporciona google recaptcha
$reCAPTCHA_secret_key = "";      //Clave privada que proporciona google recaptcha
$sensibilidad = 0.5;

###################################################################################################################################
##########                                 Configuración de voto ponderado                                               ##########
###################################################################################################################################
$votoPonderado = false;  // true si queremos que se permita voto ponderado en las encuestas


###################################################################################################################################
##########                                               Autenticacion                                                   ##########
###################################################################################################################################
$cfg_autenticacion_solo_local = true;    // ¿Permitimos autenticación federada? cfg_autenticacion_solo_local==false -> SI
$cfg_dir_simplesaml = "";                    // Directorio donde hemos instalado simplesamlphp en modo SP.
$cfg_crear_usuarios_automaticamente = true;    // Cuando tenemos autenticación federada, ¿creamos usuarios automaticamente?
$cfg_tipo_usuario = 1;    // Al crear automáticamente, el tipo por defecto
?>

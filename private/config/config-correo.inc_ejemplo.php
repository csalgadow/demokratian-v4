<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################

###############################################################################################################################################################
###############################################################################################################################################################
##########                                                                                                                                           ##########
##########                                                 CONFIGURACIÓN del la PLATAFORMA DE VOTACIONES                                             ##########
##########                                                   Software creado por Carlos Salgado                                                      ##########
##########                                                                                                                                           ##########
###############################################################################################################################################################
###############################################################################################################################################################


###################################################################################################################################
##########                          Direcciones de correo para los distintos tipos de envíos                             ##########
###################################################################################################################################
$email_env = "info@gegege.es"; //dirección de correo general
$email_error = "info@gegege.es"; //sitio al que enviamos los correos de los que tienen problemas y no están en la bbdd,
                                         //este correo es el usado si no hay datos en la bbdd de los contactos por provincias
$email_control = "info@gegege.es"; //Dirección que envía el correo para el control con interventores

$email_error_tecnico = "info@gegege.es";//correo electrónico del responsable técnico
$email_sistema = "info@gegege.es"; //correo electrónico del sistema, de momento incluido en el envió de errores de la bbdd

$asunto_mens_error = "Usuario de votaciones pruebas LOCAL con problemas "; //asunto del mensaje de correo cuando hay problemas de acceso
$nombre_eq = "Votaciones pruebas LOCAL"; //asunto del correo

$nombre_sistema = "Sistema de votaciones pruebas LOCAL"; // Nombre del sistema cuando se envía el correo de recuperación de clave
$asunto = "Recuperar tu contraseña en pruebas LOCAL";// asunto para recuperar la contraseña

###################################################################################################################################
#############         Configuración del correo smtp , solo si es tenemos como true la variable $correo_smtp          #############
###################################################################################################################################
$correo_smtp = true;        //poner en false si queremos que el envió se realice con phpmail() y true si es por smtp
$user_mail = "qefdc";        // root de correo
$pass_mail = "efdacx";  //  de correo
$host_smtp = "dsfd";        // localhost del correo
$puerto_mail = 25;
$mail_sendmail = false; // en algunos servidores como 1¬1 hay que usar IsSendMail() en vez de IsSMTP() por defecto dejar en false
$mail_SMTPSecure = false; //Set the encryption system to use - ssl (deprecated) or tls
$mail_SMTPAuth = true; //Whether to use SMTP authentication
$mail_IsHTML = false; //
$mail_SMTPOptions = false; //por defecto false. Algunos servidores con certificados incorrectos no envían los correos por SMTP por lo que quitamos la validación de los certificados, NO SE RECOMIENDA EN ABSOLUTO usar esta opción en true.

?>

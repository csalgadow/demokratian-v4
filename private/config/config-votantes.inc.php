<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
###############################################################################################################################################################
###############################################################################################################################################################
##########                                                                                                                                           ##########
##########                                           CONFIGURACIÓN del BLOG de la PLATAFORMA DE VOTACIONES                                           ##########
##########                                                   Software creado por Carlos Salgado                                                      ##########
##########                                                                                                                                           ##########
###############################################################################################################################################################
###############################################################################################################################################################

/**
* Archivo con todas las variables de la CONFIGURACIÓN de los votantes
*/

##################################################################################################################################
###############                         variables con la ponderación por tipo de votante                           ###############
##################################################################################################################################
$valor_tipo_1 = 0.45;
$valor_tipo_2 = 0.33;
$valor_tipo_3 = 0.21;


##################################################################################################################################
###############                        variables con los nombres de los tipos de votante                           ###############
##################################################################################################################################
$nombre_votantes = 1;

if ($nombre_votantes == 1){
  $nombre_tipo_1 = _("Afiliado");
  $nombre_tipo_2 = _("Simpatizante verificado");
  $nombre_tipo_3 = _("Simpatizante");
}

if ($nombre_votantes == 2){
  $nombre_tipo_1 = _("Socio");
  $nombre_tipo_2 = _("Simpatizante verificado");
  $nombre_tipo_3 = _("Simpatizante");
}

if ($nombre_votantes == 3){
  $nombre_tipo_1 = _("Socio trabajador");
  $nombre_tipo_2 = _("Socio consumidor");
  $nombre_tipo_3 = _("Socio colaborador");
}

if ($nombre_votantes == 4){
  $nombre_tipo_1 = _("Propietario");
  $nombre_tipo_2 = _("Simpatizante");
  $nombre_tipo_3 = _("No propietario");
}

?>

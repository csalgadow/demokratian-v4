<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
*  Archivo que lista todos los grupos en los que se ha apuntado el usuario
*/
if(!isset($carga)){
  $carga =false;
  exit;
}
if($carga!="OK"){
  exit;
}else{
?>
<div class="card-header"> <h1 class="card-title"><?= _("Mis grupos de trabajo") ?></h1> </div>


<div class="card-body">
<div class="col-md content">

                    <?php
                    $sql = "SELECT a.ID ,a.subgrupo,a.tipo_votante, a.id_provincia, a.tipo, b.estado, b.admin, a.texto,a.id_ccaa FROM $tbn4 a,$tbn6 b where (a.ID= b.id_grupo_trabajo) and b.id_usuario=" . $_SESSION['ID'] . " order by a.tipo";
                    $result = mysqli_query($con, $sql);
                    if ($row = mysqli_fetch_array($result)) {
                        ?>
                        <div id="accordion" class="accordion">
                          <div class="card mb-0">

                            <?php
                            mysqli_field_seek($result, 0);
                            do {
                                ?>
                                <!-- comienzo votacion-->

                <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#votacion_<?php echo md5($row[0]); ?>">
                    <a class="card-title">
                <!-- comienzo datos titulo -->
                                                <?php echo "$row[1]" ?>
                                                <!-- fin  datos titulo -->
                                                      </a>
                                                </div>
                                                <div id="votacion_<?php echo md5($row[0]); ?>" class="card-body collapse" data-parent="#accordion" >
                                                <!-- cuerpo -->
                                                <p>

                                            <?php
                                            if ($row[4] == 2) {
                                                $optiones2 = mysqli_query($con, "SELECT  ccaa FROM $tbn3 where ID=$row[8]");
                                                $row_prov2 = mysqli_fetch_row($optiones2);
                                                echo _("Grupo CCAA") . " - " . $row_prov2[0];
                                            } else if ($row[4] == 3) {
                                                if ($es_municipal == false) {
                                                    echo _("Grupo Estatal");
                                                }
                                            } else {
                                                $optiones2 = mysqli_query($con, "SELECT  provincia FROM $tbn8 where ID=$row[3]");
                                                $row_prov2 = mysqli_fetch_row($optiones2);
                                                echo _("Grupo provincial") . $row_prov2[0];
                                            }
                                            ?>
                                                  </p>
                                                  <p>
                                                <?php
                                                if ($row[6] == 0) {  // si esta apuntado pero no es admin
                                                    if ($row[5] == 0) {  // si esta esperando aprobacion
                                                        ?>
                                                        <p class="text-info"><?= _("Pendiente de acceso") ?></p>
                                                    <?php } else if ($row[5] == 1) { //si ya esta aprobado  ?>
                                                        <a href="votaciones.php?c=<?php echo encrypt_url('votacion/votaciones_grupo/idgr='.$row[0],$clave_encriptacion) ?>" class="btn btn-primary btn-sm " ><?= _("Acceder") ?></a>
                                                    <?php } else if ($row[5] == 3) { //si esta bloqueado  ?>
                                                        <?= _("No tiene ecceso, si quiere volver a acceder hable con el administrador") ?>
                                                        <?php
                                                    }
                                                } else if ($row[6] == 1) { //si es admin
                                                    ?>
                                                    <a href="votaciones.php?c=<?php echo encrypt_url('votacion/votaciones_grupo/idgr='.$row[0],$clave_encriptacion) ?>" class="btn btn-primary btn-sm " > <?= _("Accede  Administrador") ?> </a>
                                                <?php } ?>

                                          </p>
                                          <p>  <?php echo "$row[7]" ?></p>


                                          <!-- fin cuerpo-->
                                          </div>

                                <?php
                            } while ($row = mysqli_fetch_array($result));
                            ?>
                          </div>
                        </div>
                        <?php
                    } else {?>
                      <div class="alert alert-info">
                        <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                        <i class="fa fa-times simple-remove" aria-hidden="true"></i>
                        </button>
                        <span>
                          <b> Info - </b> ?<?=_("¡No se ha encontrado ningún grupo de trabajo!");?></span>
                        </div>
                        <?php
                    }

                    ?>
</div>
</div>
<?php } ?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
*  Archivo que realiza la acción de apuntar a los usuarios a un grupo tras una petición de ajax de grupos_add.php
*/
if(!isset($carga)){
  $carga =false;
  exit;
}
if($carga!="OK"){
  exit;
}else{


if ($conec == 1) {
    $estado = 1;
} else {
    $estado = 0;
}
$idgr = fn_filtro_numerico($con, $variables['idgr']);
//miramos si ya esta en el grupo
$result2 = mysqli_query($con, "SELECT id FROM $tbn6 where id_usuario=" . $_SESSION['ID'] . " and id_grupo_trabajo=" . $idgr . " ");
$quants = mysqli_num_rows($result2);
    if ($quants != 0) {
        echo "<div class=\"alert alert-danger\"> <strong>";
        echo _("Ya estas apuntado a este grupo");
        echo "</strong></div>";
    } else {

        $insql = "insert into $tbn6 (id_usuario,id_grupo_trabajo,estado) values ( " . $_SESSION['ID'] . ",  \"" . $idgr . "\",\"$estado\")";
        $inres = @mysqli_query($con, $insql) or die("<strong><font color=#FF0000 size=3>  Imposible añadir. Cambie los datos e intentelo de nuevo.</font></strong>");

        $correcto = "ok";
        //$inmsg ="Has sido correctamente incluido, <br/> en breve podras participar";
        echo " <div class=\"alert alert-success\"> <strong>";
        echo _("Has sido correctamente incluido") . ". <br/>" . _("En breve podrás participar");
        echo "</div>";
    }
}
?>

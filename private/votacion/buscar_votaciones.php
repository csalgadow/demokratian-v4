<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
*  Archivo con el formulario para realizar una búsqueda de votaciones, respuesta en buscar_votaciones_res.php
*/
if(!isset($carga)){
  $carga =false;
  exit;
}
if($carga!="OK"){
  exit;
}else{
?>

                    <div class="card-header"> <h1 class="card-title"><?= _("Buscar votaciones") ?> </h1> </div>


                    <div class="card-body">
                      <div class="card-little row">


                    <!---->
                    <form action="votaciones.php?c=<?php echo encrypt_url('votacion/buscar_votaciones_res/s=SD',$clave_encriptacion) ?>" method=post name="frmDatos" id="frmDatos" class="form-horizontal">

                        <?php if ($es_municipal == false) { ?>
                          <div class="form-group row">
                            <div class="col-sm-1">
                              <input name="votaciones" type="radio" id="votaciones_0" value="todas" checked="CHECKED" />
                            </div>
                              <label for="votaciones_0" class="control-label col-sm-11">  <?= _("Todas") ?></label>
                          </div>


                          <div class="form-group row">
                            <div class="col-sm-1">
                              <input type="radio" name="votaciones" value="1_estatal" id="votaciones_1" />
                            </div>
                            <label for="votaciones_1" class="control-label col-sm-11"><?= _("Estatales") ?></label>
                            </div>

                          <div class="form-group row">
                            <div class="col-sm-1">
                              <input type="radio" name="votaciones" value="2_<?php echo $_SESSION['id_ccaa_usu']; ?>"  id="votaciones_2" />
                            </div>
                          <label for="votaciones_2" class="control-label col-sm-11"><?php echo $_SESSION['ccaa']; ?></label>
                        </div>

                          <div class="form-group row">
                            <div class="col-sm-1">
                              <input type="radio" name="votaciones" value="3_<?php echo $_SESSION['localidad']; ?>"  id="votaciones_3" />
                              </div>
                            <label for="votaciones_3" class="control-label col-sm-11"><?= _("Provincia de") ?> : <?php echo $_SESSION['provincia']; ?></label>
                          </div>


                                <?php
                                $result2 = mysqli_query($con, "SELECT a.ID ,a.subgrupo,a.tipo_votante, a.id_provincia, a.tipo FROM $tbn4 a,$tbn6 b where (a.ID= b.id_grupo_trabajo) and b.id_usuario=" . $_SESSION['ID'] . " and b.estado=1 order by a.tipo");
                                $quants2 = mysqli_num_rows($result2);

                                if ($quants2 != 0) {

                                    while ($listrows2 = mysqli_fetch_array($result2)) {
                                        $id_grupo = $listrows2['ID'];

                                        $id_prov = $listrows2['id_provincia'];
                                        $subgrupo = $listrows2['subgrupo'];
                                        ?>


                                                    <?php
                                                    if ($listrows2['tipo'] == 1) {
                                                        $pre = "4";
                                                        $tipo_asamblea = _("Grupo provincial");
                                                    } else if ($listrows2['tipo'] == 2) {
                                                        $pre = "5";
                                                        $tipo_asamblea = _("Grupo autonomico");
                                                    } else if ($listrows2['tipo'] == 3) {
                                                        $pre = "6";
                                                        $tipo_asamblea = _("Grupo estatal");
                                                    }
                                                    ?>
                                                    <div class="form-group row">
                                                      <div class="col-sm-1">
                                                    <input  type="radio"  name="votaciones"  value="<?php echo "$pre"; ?>_<?php echo "$id_grupo"; ?>" id="votaciones_<?php echo "$id_grupo"; ?>" >
                                                  </div>
                                                  <label for="votaciones_<?php echo "$id_grupo"; ?>" class="control-label col-sm-11">   <?php echo $subgrupo; ?> (<?php echo "$tipo_asamblea"; ?>)
                                                  </label>
                                                </div>



                                        <?php
                                    }
                                }
                                ?>


                        <?php } else { ?>
                            <input name="votaciones" type="hidden" value="todas">
                        <?php } ?>
                        <p>&nbsp;  </p>






                                    <div class="form-group row">
                                                    <label for="fecha_ini" class="control-label col-sm-3">  <?= _("Entre el") ?></label>
                                                      <div class="col-sm-9">
                                            <input type="date" name="fecha_ini"  id="fecha_ini"  placeholder="aaaa-mm-dd" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />

                                          </div>
                                        </div>



                                    <div class="form-group row">
                <label for="fecha_fin" class="control-label col-sm-3">
                                            <?= _("y el") ?> </label>
                                            <div class="col-sm-9">
                                            <input type="date" name="fecha_fin" id="fecha_fin"  placeholder="aaaa-mm-dd" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
                                            <p class="help-block"></p>
                                          </div>
                                        </div>




                        <p>
                            <button type="submit" class="btn btn-primary btn-block "><?= _("Buscar") ?></button>
                        </p>


                    </form>
                  </div>
</div>
                    <!---->



<?php } ?>

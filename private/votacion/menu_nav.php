<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
*  Archivo con el menú de navegación de los votantes que han accedido al sistema
*/
function fn_donde_estoy($donde) {

global $pagina;
    if ($pagina == $donde) {
    //  return $donde;
        return "class=\"active\" ";
    }else{
    }
}

?>
<nav class="nav-side-menu bg-blue flex-md-column flex-row">
  <div class="brand">menú</div>
  <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>

  <div class="menu-list">

    <ul id="menu-content" class="menu-content collapse out">
      <li class="user-box">
        <?php include("../private/votacion/caja_usuario.php"); ?>
      </li>
      <li <?php echo fn_donde_estoy("inicio_int"); ?> >
        <a href="votaciones.php?c=<?php echo encrypt_url('votacion/inicio_int/s=SD',$clave_encriptacion) ?>"><i class="fa fa-list fa-lg"> </i> <?= _("Lista de votaciones") ?></a>
      </li>
      <li <?php echo fn_donde_estoy("buscar_votaciones"); ?>>
        <a href="votaciones.php?c=<?php echo encrypt_url('votacion/buscar_votaciones/s=SD',$clave_encriptacion); ?>"><i class="fa fa-search fa-lg"> </i> <?= _("Buscar votaciones") ?> <span class="arrow"></span></a>
      </li>
      <li <?php echo fn_donde_estoy("mis_grupos"); ?>>
        <a href="votaciones.php?c=<?php echo encrypt_url('votacion/mis_grupos/s=SD',$clave_encriptacion); ?>"><i class="fa fa-archive fa-lg"></i></i> <?= _("Mis grupos de trabajo") ?><span class="arrow"></span></a>
      </li>
      <li <?php echo fn_donde_estoy("grupos_tabla"); ?>>
        <a href="votaciones.php?c=<?php echo encrypt_url('votacion/grupos_tabla/s=SD',$clave_encriptacion); ?>"><i class="fa fa-list-alt fa-lg"> </i>  <?= _("Grupos de trabajo") ?><span class="arrow"></span></a>
      </li>
      <li>
        <a href="javascript:void(0);" data-href="aux_vota.php?c=<?php echo encrypt_url('votacion/ayuda_user/s=SD',$clave_encriptacion) ?>" class="openlargeModal"><i class="fa fa-question-circle fa-lg"></i> <?= _("Ayuda") ?></a>
      </li>
      <li>
        <a href="javascript:void(0);" data-href="aux_vota.php?c=<?php echo encrypt_url('votacion/notificar/s=SD',$clave_encriptacion) ?>" class="opennormalModal"><i class="fa fa-exclamation-triangle fa-lg"> </i> <?= _("Notificar un problema") ?></a>
      </li>
      <li <?php echo fn_donde_estoy("user"); ?>>
        <a href="votaciones.php?c=<?php echo encrypt_url('votacion/user/s=SD',$clave_encriptacion); ?>"><i class="fa fa-user fa-lg"> </i> <?= _("Modificar datos personales") ?></a>
      </li>
      <li>
        <a href="index.php"><i class="fa fa-sign-out fa-lg"> </i>  <?= _("Desconexión") ?></a>
      </li>
    </ul>
  </div>
</nav>

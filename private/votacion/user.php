<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
*  Archivo con el formulario que permite cambiar algunos datos del usuario
*/
if(!isset($carga)){
  $carga =false;
  exit;
}
if($carga!="OK"){
  exit;
}else{


if (ISSET($_POST["inprod12"])) {

    $pass1 = fn_filtro($con, $_POST['pass1']);
    $pass2 = fn_filtro($con, $_POST['pass2']);

    if ($pass1 == "" or $pass2 == "") {
        $texto1 = "<div class=\"alert alert-danger\">
             <a class=\"close\" data-dismiss=\"alert\">x</a>Faltan datos</div>";
    } elseif ($pass1 != $pass2) {
        $texto1 = "<div class=\"alert alert-danger\">
             <a class=\"close\" data-dismiss=\"alert\">x</a>password no coinciden</div>";
    } else {
        //$passw = md5($pass1);
        $passw = password_hash($pass1, PASSWORD_BCRYPT);
        $sSQL12 = "UPDATE $tbn9 SET pass='$passw' WHERE ID='" . $_SESSION['ID'] . "'";
        mysqli_query($con, $sSQL12) or die("Imposible modificar datos");
        $texto1 = "<div class=\"alert alert-success\">
             <a class=\"close\" data-dismiss=\"alert\">x</a>Realizadas las Modificaciones</div>";
    }
}


if (ISSET($_POST["modifka_perfil"])) {
    $perfil = fn_filtro($con, $_POST['perfil']);
    $sSQL12 = "UPDATE $tbn9 SET  perfil='$perfil' WHERE ID='" . $_SESSION['ID'] . "'";
    mysqli_query($con, $sSQL12) or die("Imposible modificar datos");
    $texto1 = "<div class=\"alert alert-success\">
             <a class=\"close\" data-dismiss=\"alert\">x</a>Realizadas las Modificaciones</div>";
}

$result = mysqli_query($con, "SELECT ID,nombre_usuario, correo_usuario,usuario,apellido_usuario, imagen_pequena, perfil,imagen FROM $tbn9 WHERE id ='" . $_SESSION['ID'] . "' ");
$row = mysqli_fetch_row($result);

?>
<link  href="assets/cropperjs/cropper.min.css" rel="stylesheet">
<script type="module" src="assets/cropperjs/cropper.min.js" ></script>
<div class="card-header"> <h1 class="card-title"><?= _("MODIFICAR DATOS PERSONALES") ?> </h1> </div>


<div class="card-body">
  <div class="card-little">


                    <?php
                    if (isset($texto1)) {
                        echo " $texto1";
                    }
                    ?>

  <div class="form-group row">
    <div class="col-sm-4">  <?= _("Usuario") ?>:  </div>
    <div class="col-sm-8">  <strong> <?php echo"$row[3]"; ?></strong></div>
  </div>

  <div class="form-group row">
    <div class="col-sm-4">   <?= _("Nombre") ?>:  </div>
    <div class="col-sm-8"> <strong><?php echo $_SESSION['nombre_usu']; ?> <?php echo $_SESSION['apellido_usu']; ?> </strong></div>
    </div>

    <div class="form-group row">
      <div class="col-sm-4">    <?= _("Correo electronico") ?>:  </div>
      <div class="col-sm-8">   <strong><?php echo"$row[2]"; ?></strong></div>
    </div>


<?php
if ($row[5] == "peq_usuario.jpg" or $row[5] == "") {
  $thumb_photo_exists="temas/".$tema_web."/images/user.png";
}else{
  $thumb_photo_exists = $upload_user . "/" . $row[5];
} ?>

<div class="row">
  <div class="col-md-2">&nbsp;</div>

    <div class="image_area">
      <form method="post">
        <div class="caja_crop">
        <label for="upload_image">
          <img src="<?php echo $thumb_photo_exists; ?>" width="270" height="270"  id="uploaded_image" class="img-circle  mx-auto d-block" />
          <div class="overlay">
            <div class="text_crop">Click para cambiar la imagen</div>
          </div>
          <input type="file" name="image" class="image" id="upload_image" style="display:none" />
        </label>
      </div>
      </form>
    </div>

     <div id="success2"></div>

  </div>


<fieldset>
    <form action="<?php $_SERVER['PHP_SELF'] ?>" method=post name="frmDatos" id="frmDatos" class="form-horizontal">
    <div class="form-group row">
          <label for="pass1" class="control-label col-sm-4"> <?= _("Password") ?> </label>
            <div class="col-sm-8"> <input name="pass1" type="password" id="pass1" value="" class="form-control" /></div>
          </div>


          <div class="form-group row">
                <label for="pass2" class="control-label col-sm-4"> <?= _("Password") ?> (<?= _("repitalo") ?>) </label>
                  <div class="col-sm-8">   <input name="pass2" type="password" id="pass2" value=""class="form-control" / ></div>
            </div>

            <input name="inprod12" type="submit" id="inprod12" value="<?= _("modificar password") ?>" class="btn btn-primary btn-block" />

    </form>
</fieldset>

<fieldset>
<form action="<?php $_SERVER['PHP_SELF'] ?>" method=post name="frmDatos2" id="frmDatos2" class="form-horizontal">

                <div class="form-group row">
                      <label for="perfil" class="control-label col-sm-4"><?= _("Perfil publico") ?></label>
                                              <div class="col-sm-8">
                                            <textarea name="perfil"  id="perfil"  rows="10" cols="100" class="form-control" ><?php echo"$row[6]"; ?></textarea>

                                        </div>
                                    </div>

                <input name="modifka_perfil" type="submit"  id="modifka_perfil" value="<?= _("modificar texto perfil") ?>" class="btn btn-primary btn-block" />


                    </form>
</fieldset>
                  </div>
                  </div>
<!-- modal cropeo imagen -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xxl" role="document">
    <div class="modal-content">


        <div class="modal-header">
          <h5 class="modal-title">Recortar la imagen antes de añadirla</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
          </button>
        </div>


        <div class="modal-body">
          <div class="img-container">
              <div class="row">
                  <div class="col-md-8">
                      <img class="image_toCrop" src="" id="sample_image" />
                  </div>
                  <div class="col-md-4">
                      <div class="preview"></div>
                  </div>
              </div>
          </div>
        </div>


        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" id="crop" class="btn btn-primary">Recortar la imagen y añadir</button>
        </div>


    </div>
  </div>
</div>
>
<!-- /  modal cropeo imagen -->
                  <script>

                  $(document).ready(function(){

                   var $modal = $('#modal');

                   var image = document.getElementById('sample_image');

                   var cropper;

                   $('#upload_image').change(function(event){
                     var files = event.target.files;

                     var done = function(url){
                       image.src = url;
                       $modal.modal('show');
                     };

                     if(files && files.length > 0)
                     {
                       reader = new FileReader();
                       reader.onload = function(event)
                       {
                         done(reader.result);
                       };
                       reader.readAsDataURL(files[0]);
                     }
                   });

                   $modal.on('shown.bs.modal', function() {
                     cropper = new Cropper(image, {
                       aspectRatio: 1,
                       viewMode: 3,
                       preview:'.preview'
                     });
                   }).on('hidden.bs.modal', function(){
                     cropper.destroy();
                       cropper = null;
                   });

                   $('#crop').click(function(){
                     canvas = cropper.getCroppedCanvas({
                       width:400,
                       height:400
                     });

                     canvas.toBlob(function(blob){
                       url = URL.createObjectURL(blob);
                       var reader = new FileReader();
                       reader.readAsDataURL(blob);
                       reader.onloadend = function(){
                         var base64data = reader.result;
                         $.ajax({
                           url: "aux_vota.php?c=<?php echo encrypt_url('votacion/user_crop_image/id='.$row[0],$clave_encriptacion) ?>",
                           method:'POST',
                           data:{image:base64data},
                           success:function(data)
                           {

                             $modal.modal('hide');

                              $('#uploaded_image').attr('src', data);
                              $('#user_image').attr('src', data);
                               $('#success2').html('<div class="alert alert-success"><?= _("Se ha subido la imagen")?> </div>');
                               $('#success2').show();

                           }
                         });
                       };
                     });
                   });

                  });


                  </script>

<?php } ?>

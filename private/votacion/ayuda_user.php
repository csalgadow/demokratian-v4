<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
*  Archivo con la ayuda para los votantes
*/
require_once("../private/config/config.inc.php");
require_once("../private/basicos_php/lang.php");

?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <title>Ayuda</title>
    </head>
    <body>

      <div class="modal-header">
             <h5 class="modal-title"><?= _("Ayuda a usuarios de la plataforma de votaciones") ?><!---titulo--></h5>
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
         </div>
         <div class="modal-body">
                 <!---texto --->

                <!--
              =========================== texto ayuda
                -->

                <p><?= _("A la hora de diseñar la aplicación, se ha realizado pensando  fundamentalmente en:") ?></p>
                <p>&nbsp;</p>

                <ul>
                    <li><?= _("Una fácil gestión de votantes en la que se pueda tramitar por parte del administrador los censos de votantes y que además puedan votar, en función del tipo de votación, afiliados/socios, simpatizantes verificados y simpatizantes") ?></li>
                    <li><?= _("Que se puedan realizar varios tipos de votación, actualmente  hay 4 tipos de votaciones.") ?></li>
                    <li><?= _("Posibilidad de múltiples tipos de circunscripciones, estatales, autonómicas, provinciales y municipales, además pueden pueden existir grupos de trabajo abiertos o cerrados.") ?></li>
                    <li><?= _("No trazabilidad del voto, su voto es secreto y nadie puede saber que ha votado. (a excepción del tipo de votación debate ya que en este tipo puede modificar el sentido  de su voto si a medida que transcurre el debate cambia de parecer)") ?></li>
                </ul>

                <h4><?= _("Mi perfil de usuario") ?></h4>
                <p><?= _("Si ha accedido a la plataforma de votaciones, significa que tiene Ud un perfil de usuario, puede ver que perfil tiene Ud, así como su nombre en la parte superior del menú. Si detecta algún error, puede notificarlo mediante el menú “Notificar un problema”") ?>
                    <?= _("Si quiere cambiar su foto, password, o información de perfil público, puede hacerlo en el menú “modificar datos personales”") ?></p>
                <p>&nbsp;</p>
                <h4><?= _("¿Quién puede votar?") ?></h4>
                <p><?= _("Existen 3 tipos de perfiles de votantes, los afiliados/socios, los  simpatizantes verificados (1), y simpatizantes. Los administradores podrán  decidir para cada votación que usuarios podrán votar.  Existen también grupos de trabajo a los que  se debe de inscribir para poder participar en las votaciones de esos grupos.") ?></p>
                <h4><br />
                    <?= _("Qué son las  circunscripciones.") ?></h4>
                <p><?= _("La plataforma de votaciones permite diferentes ámbitos de votación, Ud estará censado en una provincia, y podrá votar todas aquellas consultas provinciales que se creen, además podrá participar en las consultas de su Comunicad autónoma, en las Estatales, asi como en las de su pueblo o ciudad.") ?>
                    <?= _("Además de estas circunscripciones también podemos participar en grupos de trabajo a los cuales deberá de apuntarse para poder participar. Estos grupos de trabajo pueden ser de ámbito estatal, autonómico o provincial") ?></p>
                <h4><br />
                    <?= _("Como participo en un grupo de trabajo.") ?></h4>
                <p><?= _("Para ver que grupos de trabajo hay en su provincia,  comunidad autónoma, o estatales,  puede  hacerlo mediante el menú &ldquo;grupos de trabajo&rdquo;. Puede elegir los grupos según el ámbito,  o hacer búsquedas por nombre. Una vez ha encontrado el grupo al que quiere  pertenecer, debe de solicitar su ingreso.") ?> <br />
                    <?= _("Una vez que su ingreso es aceptado,(o si es un grupo de  ingreso automático) para acceder a sus grupos  mediante el menú, &ldquo;mis grupos de trabajo&rdquo;") ?></p>
                <h4><?= _("¿Qué tipos de votación hay?") ?></h4>
                <p><?= _("Existen 4 tipos de votaciones:") ?></p>
                <p>&nbsp;</p>
                <ul>
                    <li><strong><?= _("Primarias") ?></strong>.
                        <?= _("Podrá elegir las opciones ordenándolas en una lista. Para hacerlo deberá coger la opción que quiera de la lista de la izquierda hacer click en el boton del la opcion con lo que esta opción se desplazará a la lista de la izquierda. Una vez en la lista, si quiere puede modificar el orden o eliminar alguna de sus elecciones El cómputo de esta votación es ponderado, es decir, tendrá un valor  en función del orden, y podra ser de dos tipos:") ?>
                        <ul>
                            <li><strong><?= _("Ponderación DOWDALL") ?></strong>
                                <?= _("el primero tendrá 1 punto, el segundo 0.5 el tercero 0.25 y sucesivamente. (1/orden)") ?></li>
                            <li><strong><?= _("Ponderación  BORDA") ?></strong> ,
                                <?= _("que, por ejemplo si se eligen 6 opciones, el primero tendrá 6 puntos, el segundo 5, el tercero 4, etc") ?></li>
                        </ul>
                    </li>
                    <li><strong><?= _("VUT (voto  único transferible)") ?></strong>,
                        <?= _("sistema de recuento sobre el que se puede encontrar  información") ?>
                        <a href="http://es.wikipedia.org/wiki/Voto_%C3%BAnico_transferible" >
                            <?= _("aquí") ?></a>,
                        <?= _("Para votar, deberá ordenar la lista. Para borrar una opción puede  hacerlo mediante la flecha del lateral") ?></li>
                    <li><strong><?= _("Encuesta") ?></strong>. 
                        <?= _("Permite elegir una opción -o más entre,  varias. Debera marcar las opciones elegidas. Es un voto que no tiene ningún  tipo de ponderación.") ?></li>
                    <li><strong><?= _("El debate") ?></strong>:
                        <?= _("Este tipo de votación es una fórmula mixta de foro y votación en el que se  pueden enviar comentarios cortos que aparecen publicado en un tablón al estilo  facebook y  si se necesita, votar una o  varias preguntas.  Este voto se puede  modificar a medida que transcurre el debate si cambiamos de opinión. Este  sistema no asegura la no trazabilidad del voto, es decir, si alguien accede a  la bbdd podría llegar a ver que ha votado quien y tampoco tiene  sistema de envío de correos a interventores.") ?></li>
                </ul>
                <p>&nbsp;</p>

                <h4><?= _("Como votar") ?></h4>
                <p><?= _("Al acceder a la aplicación ya le aparecerán las votaciones  que hay activas, separadas estas por circunscripciones y grupos de trabajo. También  puede acceder para ver todas las votaciones mediante el menú &ldquo;lista de  votaciones&rdquo; . En las cajas desplegables podrá ver la información de la votación,  las fechas entre las que esta activa, el tipo de votación, si ya ha votado o  aun puede votar, asi como, cuando se publiquen los resultados, podrá acceder a  ellos. Para acceder a la votación podrá hacerlo mediante el enlace situado en  la misma ficha que pone VOTAR.") ?></p>
                <p>&nbsp;</p>
                <h4><?= _("Seguridad  de las votaciones.") ?></h4>
                <p><?= _("Existe la posibilidad de activar un sistema de  control-verificacion de lo votado para  votaciones importantes. El sistema  sigue  manteniendo la casi imposible   trazabilidad del voto, es decir, el voto es secreto y aunque alguien  accediera a la base de datos seria muy difícil   conocer que ha votado quien.(imposible   decir &ldquo;imposible&rdquo;,  ya que hay  cracks que consigue hasta entrar en los bancos…. ).") ?> </p>
                <p><?= _("Tenemos  las siguientes opciones.") ?></p>
                <ul>
                    <li><strong><?= _("Comprobación  de voto") ?></strong>  
                        <?= _("(no funciona en VUT). El sistema  permite a los votantes ver que se mantiene su voto en la base de datos tal cual  lo han emitido, este sistema no funciona con voto VUT que se implementara más  tarde.") ?></li>
                    <li><strong><?= _("Interventores.") ?></strong>
                        <?= _("El  sistema realiza el envío de un correo  anónimo con cada voto a los interventores que se designen, de esta forma en  caso de duda podrían recontar los votos, sería como tener una versión voto en  papel que tendrían interventores.") ?> <br />
                        .<br />
                        <?= _("Estas  opciones no estaran disponibles en una votacion tipo debate.") ?></li>
                </ul>
                <p>&nbsp;</p>



                <!--
            ===========================  fin texto ayuda
                -->
</div>
    </body>
</html>

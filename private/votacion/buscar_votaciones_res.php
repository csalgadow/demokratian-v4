<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
*  Archivo con la respuesta de la búsqueda de votaciones de buscar_votaciones.php
*/
if(!isset($carga)){
  $carga =false;
  exit;
}
if($carga!="OK"){
  exit;
}else{
?>
<div class="card-body">
                    <?php
                    $VarSinResultados = "";
                    $dato_votacion = "";
                    $esta_activa = "si";
                    $id_provincia = $_SESSION['localidad'];
                    $tipo_user = $_SESSION['tipo_votante'];
                    $id_ccaa = $_SESSION['id_ccaa_usu'];

                    /**/
                    if ($_POST['fecha_ini'] == "") {

                        $fecha_com = date("Y-m-d", strtotime("1970-01-01"));
                        $dato_com = _("inicio de los tiempos");
                    } else {
                        $fecha_com = date("Y-m-d", strtotime($_POST['fecha_ini']));
                        $dato_com = $_POST['fecha_ini'];
                    }
                    if ($_POST['fecha_fin'] == "") {
                        $fecha_fin = date("Y-m-d", strtotime("2030-01-01"));
                        $dato_fin = _("final de los tiempos");
                    } else {
                        $fecha_fin = date("Y-m-d", strtotime($_POST['fecha_fin']));
                        $dato_fin = $_POST['fecha_fin'];
                    }



                    echo '<h5 class="text-info">' . _("Se han encotrado las siguientes votaciones entre el") . ' <strong> ' .$dato_com . '</strong> ' . _("y el") . ' <strong> ' . $dato_fin . '</strong></h5>';
                    echo "<p>&nbsp;</p>";

                    if ($_POST['votaciones'] != "todas") {
                        //echo "duda";
                        $parte = explode("_", $_POST['votaciones']);
                        //echo $parte[0]; // piece1
                        //echo $parte[1]; // piece2
                        $cuenta = $parte[0];
                        $cuenta_fin = $parte[0];
                        $dato_votacion = $parte[1];
                    } else {
                        $cuenta = 1;
                        $cuenta_fin = 6;
                    }

                    //$cuenta=1;
                    while ($cuenta <= $cuenta_fin) {

                        if ($cuenta == 1) {
                            $sql = "SELECT ID ,activa, nombre_votacion ,resumen, tipo  ,fecha_com, fecha_fin ,activos_resultados, fin_resultados ,recuento,seguridad 	FROM $tbn1  where demarcacion=1 and tipo_votante >=" . $_SESSION['tipo_votante'] . "  and activa like '$esta_activa' and  fecha_com>'$fecha_com' and  fecha_fin<'$fecha_fin'  ORDER BY  ID  DESC";
                            $que_votacion = _("de tipo Estatal");
                        }
                        if ($cuenta == 2) {

                            $sql = "SELECT ID ,activa, nombre_votacion ,resumen, tipo  ,fecha_com, fecha_fin ,activos_resultados, fin_resultados,recuento,seguridad 	 FROM $tbn1 where demarcacion=2 and id_ccaa=" . $_SESSION['id_ccaa_usu'] . " and tipo_votante >=" . $_SESSION['tipo_votante'] . "   and activa like '$esta_activa' and  fecha_com>'$fecha_com'  and  fecha_fin<'$fecha_fin'  ORDER BY  ID DESC";
                            $que_votacion = _("en la CCAA de") . $_SESSION['ccaa'] . "";
                        }
                        if ($cuenta == 3) {

                            $sql = "SELECT ID ,activa, nombre_votacion ,resumen, tipo  ,fecha_com, fecha_fin ,activos_resultados, fin_resultados,recuento,seguridad 	 FROM $tbn1 where demarcacion=3 and id_provincia =" . $_SESSION['localidad'] . " and tipo_votante >=" . $_SESSION['tipo_votante'] . "  and activa like '$esta_activa'   and  fecha_com>'$fecha_com' and  fecha_fin<'$fecha_fin' ORDER BY  ID DESC";
                            $que_votacion = _("en la provincia de") . $_SESSION['provincia'] . "";
                        }

                        if ($cuenta == 4) {
                            if ($dato_votacion != "") {
                                $sql = "SELECT ID ,activa, nombre_votacion ,resumen, tipo  ,fecha_com, fecha_fin ,activos_resultados, fin_resultados,recuento,seguridad 	FROM $tbn1  where id_grupo_trabajo =" . $dato_votacion . " and demarcacion=4 and tipo_votante >=" . $_SESSION['tipo_votante'] . "   and activa like '$esta_activa'   and  fecha_fin<'$fecha_fin' and  fecha_com>'$fecha_com' ORDER BY  ID DESC";
                                $result2 = mysqli_query($con, "SELECT subgrupo,tipo_votante, id_provincia, tipo FROM $tbn4  where ID=" . $dato_votacion . "");
                                $quants2 = mysqli_num_rows($result2);

                                if ($quants2 != 0) {
                                    while ($listrows2 = mysqli_fetch_array($result2)) {
                                        //$id_grupo = $listrows2['ID'];
                                        //$id_prov = $listrows2['id_provincia'];
                                        $que_votacion = "del \"" . $listrows2['subgrupo'] . "\"";
                                    }
                                }
                            } else {
                                $sql = "SELECT a.ID ,a.activa, 	a.nombre_votacion ,	a.resumen, a.tipo  ,a.fecha_com, a.fecha_fin ,a.activos_resultados, a.fin_resultados, a.recuento ,a.seguridad,a.id_grupo_trabajo 	FROM $tbn1 a,$tbn6 b where (a.id_grupo_trabajo = b.id_grupo_trabajo) and b.id_usuario=" . $_SESSION['ID'] . " and a.demarcacion=4 and a.tipo_votante >=" . $_SESSION['tipo_votante'] . "   and a.activa like '$esta_activa' and b.estado=1   and  a.fecha_fin<'$fecha_fin' and  a.fecha_com>'$fecha_com' ORDER BY  ID DESC";
                                $que_votacion = _("en grupo Grupo provincial");
                            }
                        }

                        if ($cuenta == 5) {
                            if ($dato_votacion != "") {
                                $sql = "SELECT ID ,activa, nombre_votacion ,resumen, tipo  ,fecha_com, fecha_fin ,activos_resultados, fin_resultados ,recuento,seguridad,id_grupo_trabajo	FROM $tbn1  where id_grupo_trabajo =" . $dato_votacion . " and demarcacion=4 and tipo_votante >=" . $_SESSION['tipo_votante'] . "   and activa like '$esta_activa'   and  fecha_fin<'$fecha_fin' and  fecha_com>'$fecha_com' ORDER BY  ID DESC";
                                $result2 = mysqli_query($con, "SELECT subgrupo,tipo_votante, id_provincia, tipo FROM $tbn4  where ID=" . $dato_votacion . "");
                                $quants2 = mysqli_num_rows($result2);
                                if ($quants2 != 0) {
                                    while ($listrows2 = mysqli_fetch_array($result2)) {
                                        // $id_grupo = $listrows2['ID'];
                                        //$id_prov = $listrows2['id_provincia'];
                                        $que_votacion = "del \"" . $listrows2['subgrupo'] . "\"";
                                    }
                                }
                            } else {
                                $sql = "SELECT a.ID ,a.activa, 	a.nombre_votacion ,	a.resumen, a.tipo  ,a.fecha_com, a.fecha_fin ,a.activos_resultados, a.fin_resultados, a.recuento ,a.seguridad,a.id_grupo_trabajo 	FROM $tbn1 a,$tbn6 b where (a.id_grupo_trabajo = b.id_grupo_trabajo) and b.id_usuario=" . $_SESSION['ID'] . " and a.demarcacion=5 and a.tipo_votante >=" . $_SESSION['tipo_votante'] . "   and a.activa like '$esta_activa' and b.estado=1  and  a.fecha_fin<'$fecha_fin' and  a.fecha_com>'$fecha_com' ORDER BY  a.ID DESC";
                                $que_votacion = _("en Grupo autonomico");
                            }
                        }
                        if ($cuenta == 6) {
                            if ($dato_votacion != "") {
                                $sql = "SELECT ID ,activa, nombre_votacion ,resumen, tipo  ,fecha_com, fecha_fin ,activos_resultados, fin_resultados ,recuento,seguridad,id_grupo_trabajo 	FROM $tbn1  where id_grupo_trabajo =" . $dato_votacion . " and demarcacion = 4 and tipo_votante >=" . $_SESSION['tipo_votante'] . "   and activa like '$esta_activa'   and  fecha_fin<'$fecha_fin' and  fecha_com>'$fecha_com' ORDER BY  ID DESC";
                                $result2 = mysqli_query($con, "SELECT subgrupo,tipo_votante, id_provincia, tipo FROM $tbn4  where ID=" . $dato_votacion . "");
                                $quants2 = mysqli_num_rows($result2);
                                if ($quants2 != 0) {
                                    while ($listrows2 = mysqli_fetch_array($result2)) {
                                        $id_grupo = $listrows2['ID'];
                                        $id_prov = $listrows2['id_provincia'];
                                        $que_votacion = "del \"" . $listrows2['subgrupo'] . "\"";
                                    }
                                }
                            } else {
                                $sql = "SELECT a.ID ,a.activa, 	a.nombre_votacion ,	a.resumen, a.tipo  ,a.fecha_com, a.fecha_fin ,a.activos_resultados, a.fin_resultados, a.recuento ,a.seguridad,a.id_grupo_trabajo	FROM $tbn1 a,$tbn6 b where (a.id_grupo_trabajo = b.id_grupo_trabajo) and b.id_usuario=" . $_SESSION['ID'] . " and a.demarcacion=6 and a.tipo_votante >=" . $_SESSION['tipo_votante'] . "   and a.activa like '$esta_activa' and b.estado=1  and  a.fecha_fin<'$fecha_fin' and  a.fecha_com>'$fecha_com' ORDER BY  a.ID DESC";
                                $que_votacion = _("en grupo Estatal");
                            }
                        }



                        $result = mysqli_query($con, $sql);
                        if ($row = mysqli_fetch_array($result)) {
                            ?>


                            <h3><?= _("Listado de votaciones") ?>   <?php
                                if ($es_municipal == false) {
                                    echo "$que_votacion";
                                }
                                ?> </h3>

                                <div id="accordion" class="accordion">
                                  <div class="card mb-0">
                                <?php
                                mysqli_field_seek($result, 0);

                                do {
                                    ?>
                                    <?php
                                    $hoy = strtotime(date('Y-m-d H:i'));
                                    $fecha_ini = strtotime($row[5]);
                                    $fecha_final = strtotime($row[6]);
                                    $fecha_ini_ver = date("d-m-Y", strtotime($row[5]));
                                    $fecha_fin_ver = date("d-m-Y", strtotime($row[6]));
                                    $hora_ini_ver = date("H:i", strtotime($row[5]));
                                    $hora_fin_ver = date("H:i", strtotime($row[6]));



                                    if ($row[1] == "no") {
                                        $activo = _("Votacion NO activa");
                                    } else if ($fecha_ini <= $hoy && $fecha_final >= $hoy) {
                                        //$activo="Votacion NO activa";

                                        /* }
                                          else { */
                                        $id_votante = $_SESSION['ID'];
                                        $id_votacion = $row[0];
                                        $conta_vot = "SELECT id FROM $tbn2 WHERE id_votacion like \"$id_votacion\" and id_votante='$id_votante' ";

                                        $result_cont_vot = mysqli_query($con, $conta_vot);
                                        $quants_vot = mysqli_num_rows($result_cont_vot);

                                        if ($quants_vot != 0) {
                                            $activo = _("Ya ha votado");
                                            $activo1 = '<span class="text-info">'._("Ya ha votado").'</span>';
                                        } else {
                                                                    if ($row[4] == 1) {
                                                                        $dir = 'vota_orden/primarias';
                                                                        $texto1_activo = _("VOTAR");
                                                                        $texto2_activo = _("Votación NO activa");
                                                                        $image_activo = '<i class="fa fa-bell text-success"></i>';
                                                                    } else if ($row[4] == 2) {
                                                                        $dir = 'vota_vut/vut';
                                                                        $texto1_activo = _("VOTAR");
                                                                        $texto2_activo = _("Votación NO activa");
                                                                        $image_activo = '<i class="fa fa-bell text-success"></i>';
                                                                    } else if ($row[4] == 3) {
                                                                        $dir = 'vota_encuesta/vota_encuesta';
                                                                        $texto1_activo = _("VOTAR");
                                                                        $texto2_activo = _("Votación NO activa");
                                                                        $image_activo = '<i class="fa fa-bell text-success"></i>';
                                                                    } else if ($row[4] == 4) {
                                                                        $dir = 'vota_debate/debate';
                                                                        $texto1_activo = _("DEBATE ABIERTO");
                                                                        $texto2_activo = _("Debate cerrado");
                                                                        $image_activo = '<i class="fa fa-comments text-success"></i>';
                                                                    }

                                                                    $activo1 = '<span class="text-warning ">'. $image_activo.' '.$texto1_activo .' </span>' ;
                                                                    $activo = '<a href="votaciones.php?c='.encrypt_url($dir.'/idvot='.$row[0],$clave_encriptacion). '" class="btn btn-success btn-xs">'.$texto1_activo.'</a>';
                                                                              }
                                    } else {

                                        if ($row[4] == 1) {
                                            if ($fecha_final <= $hoy) {
                                                $texto1_activo = _("Votación finalizada");
                                                $texto2_activo = _("Votación finalizada");
                                            } else {
                                                $texto1_activo = _("Votación sin iniciar");
                                                $texto2_activo = _("Votación sin iniciar");
                                            }
                                        } else if ($row[4] == 2) {
                                            if ($fecha_final <= $hoy) {
                                                $texto1_activo = _("Votación finalizada");
                                                $texto2_activo = _("Votación finalizada");
                                            } else {
                                                $texto1_activo = _("Votación sin iniciar");
                                                $texto2_activo = _("Votación sin iniciar");
                                            }
                                        } else if ($row[4] == 3) {
                                            if ($fecha_final <= $hoy) {
                                                $texto1_activo = _("Votación finalizada");
                                                $texto2_activo = _("Votación finalizada");
                                            } else {
                                                $texto1_activo = _("Votación sin iniciar");
                                                $texto2_activo = _("Votación sin iniciar");
                                            }
                                        } else if ($row[4] == 4) {
                                            $dir = 'vota_debate/debate';
                                            if ($fecha_fin <= $hoy) {
                                                $texto1_activo = '<i class="fa fa-comments text-info"></i> '. _("Debate finalizado")  ;
                                                $texto2_activo = "<a href='$dir?idvot=$row[0]'>". _("Debate finalizado")."</a>";
                                            } else {
                                                $texto1_activo = '<i class="fa fa-comments text-info"></i> '._("Debate sin iniciar") ;
                                                $texto2_activo = "<a href='$dir?idvot=$row[0]' >".  _("Debate sin iniciar")."</a>";
                                            }
                                        }

                                        $activo1 = '<span class="text-info ">'.$texto1_activo .'</span>' ;
                                        $activo = $texto2_activo;
                                    }
                                    ?>
                                    <!-- comienzo votacion-->

                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#votacion_<?php echo md5($row[0]); ?>">
                        <a class="card-title">
                    <!-- comienzo datos titulo -->
                                              <?php echo "$activo1"; ?>    |    <?php echo $row[2] ?>

                                                <?php
                                                if ($cuenta == 4 or $cuenta == 5 or $cuenta == 6) {
                                                    $sql_grupo = mysqli_query($con, "SELECT subgrupo FROM $tbn4 where ID=" . $row[11] . " ");
                                                    $row_grupo = mysqli_fetch_row($sql_grupo);
                                                    ?>
                                                |  <?= _("Grupo") ?> : : <?php echo $row_grupo[0]; ?> |
                                                <?php } ?>



                                                    <!-- fin  datos titulo -->
                                                          </a>
                                                    </div>
                                                    <div id="votacion_<?php echo md5($row[0]); ?>" class="card-body collapse" data-parent="#accordion" >
                                                    <!-- cuerpo -->
                                                    <p>
                                                        <?= _("Estado de la votación") ?> : <?php echo "$activo"; ?>
                                                        <br/> <?= _("Fechas votación: desde las") ?> <?php echo $hora_ini_ver; ?> <?= _("del") ?>  <?php echo $fecha_ini_ver; ?> <?= _("a las") ?> <?php echo $hora_fin_ver; ?>  <?= _("del") ?>
                                                        <?php echo $fecha_fin_ver; ?>
                                                        <br/> <?= _("Tipo de votación") ?>: <?php
                                                        if ($row[4] == 1) {
                                                            echo _("primarias");
                                                        } else if ($row[4] == 2) {
                                                            echo _("VUT");
                                                        } else if ($row[4] == 3) {
                                                            echo _("encuesta");
                                                        } else if ($row[4] == 4) {
                                                            echo _("Debate");
                                                        }
                                                        ?>


                                                        <?php
                                                        if ($row[7] == "si") {
                                                          if ($row[4] != 2 or $row[4] != 4) {
                                                                                        $id_encriptada = md5($row[0]);
                                                                                        $nombre_fichero1 = $id_encriptada . ".php";
                                                                                        $nombre_fichero2 = $id_encriptada. "_list.php";
                                                                                        ?>
                                                                                        <a  data-toggle="modal" href="aux_vota.php?d=<?php echo $nombre_fichero1 ?>" data-target="#modal-resultados" class="btn btn-primary btn-xs"><?= _("Resultados") ?></a>
                                                                                        <a  data-toggle="modal" href="aux_vota.php?d=<?php echo $nombre_fichero2 ?>" data-target="#modal-resultados" class="btn btn-primary btn-xs"><?= _("Listar votos") ?></a>
<?php
                                                            } else if ($row[4] == 2 ) {
                                                              $id_encriptada = md5($row[0]);
                                                              $nombre_fichero2 = $id_encriptada. "_list.php";
                                                              ?>
                                                                <a data-toggle="modal"  href="aux_vota.php?c=<?php echo encrypt_url('vota_vut/resultados/idvot='.$row[0],$clave_encriptacion) ?>" data-target="#modal-resultados" class="btn btn-primary btn-xs"><?= _("Resultados") ?></a>
                                                                <a data-toggle="modal"  href="aux_vota.php?d=<?php echo $nombre_fichero2 ?>" data-target="#modal-resultados" class="btn btn-primary btn-xs"><?= _("Listar votos") ?></a>

                                                                <?php
                                                                }  else if ($row[4] == 3 || $row[4] == 5 || $row[4] == 6 || $row[4] == 7) {
                                                                  ?>
                                                                  <a href="votaciones.php?c=<?php echo encrypt_url('debate/esultados_debate/idvot='.$row[0],$clave_encriptacion) ?>" class="btn btn-primary btn-xs"><?= _("Resultados") ?></a>
<?php
                                                            }

                                                          //  $activo_a_1 = "<br/><a href='$dir_a_1?idvot=$row[0]'  class=modify>" . _("Resultados") . "</a>";
                                                        } else {
                                                          //  $activo_a_1 = "";
                                                        }
                                                      //  echo "$activo_a_1";
                                                        ?>

                                                        <?php echo $row[3] ?></p>
                          <!-- fin cuerpo-->
                  </div>



                                           <!--fin votacion-->
                                    <?php
                                    $VarSinResultados = "hay_resultado";
                                } while ($row = mysqli_fetch_array($result));
                                ?>
                            </div>
                            <?php
                        } else {
                            // $VarSinResultados="";
                        }
                        ?>
                        <?php
                        $cuenta++;
                    }


                    if ($VarSinResultados != "hay_resultado") { ?>
                      <div class="alert alert-info">
                        <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                        <i class="fa fa-times simple-remove" aria-hidden="true"></i>
                        </button>
                        <span>
                          <b> Info - </b> <?=_("¡No hay resultados con estos criterios de busqueda!");?></span>
                        </div>
                        <?php
                    }
                    ?>



                  </div>
                </div>
                <!--fin acordion -->

              </div>
              <?php } ?>

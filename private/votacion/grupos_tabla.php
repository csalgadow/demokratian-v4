<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
*  Archivo que lista todos los grupos a los que puede apuntarse un usuario
*/
if(!isset($carga)){
  $carga =false;
  exit;
}
if($carga!="OK"){
  exit;
}else{
?>


          <link rel="stylesheet" type="text/css" href="assets/DataTables/datatables.min.css" />



                    <!--Comiezo-->
                    <div class="card-header">
                      <h1 class="card-title">Grupos de trabajo </h1></div>

<div class="col-md content">
                    <div class="card-body">


                    <?php
                    $esta_activa = 1;
                    $tipo = 1;


                    $sql = "select ID, tipo,texto,subgrupo,  acceso from $tbn4 where  tipo_votante<=" . $_SESSION['tipo_votante'] . "  and  (id_ccaa=" . $_SESSION['id_ccaa_usu'] . " or id_ccaa=0)  and (id_provincia=" . $_SESSION['localidad'] . " or id_provincia=0 and acceso=1 or acceso=2) order by id desc";
                    ?>
                    <?php
                    $result = mysqli_query($con, $sql);
                    if ($row = mysqli_fetch_array($result)) {
                        ?>
                        <table id="tabla1" class="table table-striped table-bordered dt-responsive nowrap" data-page-length="25" >
                            <thead>
                                <tr>
                                    <th width="73%">Grupo</th>
                                    <?php if ($es_municipal == false) { ?> <th width="6%">Tipo</th> <?php } ?>
                                    <th width="21%">&nbsp;</th>


                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                mysqli_field_seek($result, 0);
                                do {
                                    ?>
                                    <tr>
                                        <td>
                                            <h6><?php echo $row[3]; ?></h6>
                                            <?php echo $row[2]; ?>
                                        </td>
                                        <?php if ($es_municipal == false) { ?>
                                            <td>
                                                <?php
                                                if ($row[1] == 1) {
                                                    echo _("Provincial");
                                                } else if ($row[1] == 2) {
                                                    echo _("Autonomico");
                                                } else if ($row[1] == 3) {
                                                    echo _("Estatal");
                                                }
                                                ?>

                                            </td>
                                        <?php } ?>
                                        <td>
                                            <?php
                                            $options_usu = "select  ID,admin,estado from $tbn6 where  id_grupo_trabajo=" . $row[0] . " and id_usuario=" . $_SESSION['ID'] . "  order by ID";
                                            $result_cont = mysqli_query($con, $options_usu);

                                            $quants = mysqli_num_rows($result_cont);
                                            $row_cont = mysqli_fetch_row($result_cont);
                                            /////aqui miramos si esta o no en el grupo
                                            if ($quants != 0) {
                                                if ($row_cont[1] == 0) {  // si esta apuntado pero no es admin
                                                    if ($row_cont[2] == 0) {  // si esta esperando aprobacion
                                                        ?>
                                                        Pendiente de acceso
                                                    <?php } else if ($row_cont[2] == 1) { //si ya esta aprobado ?>
                                                        <a href="votaciones.php?c=<?php echo encrypt_url('votacion/votaciones_grupo/idgr='.$row[0],$clave_encriptacion) ?>" class="btn btn-success  btn-block"><?= _("Acceder") ?> </a>
                                                    <?php } else if ($row_cont[2] == 3) { //si esta bloqueado ?>
                                                        <?= _("No tiene ecceso, si quiere volver a acceder hable con el administrador") ?>
                                                        <?php
                                                    }
                                                } else if ($row_cont[1] == 1) { //si es admin
                                                    ?>
                                                    <a href="votaciones.php?c=<?php echo encrypt_url('votacion/votaciones_grupo/idgr='.$row[0],$clave_encriptacion) ?>" class="btn btn-success  btn-block" ><?= _("Administrador") ?></a>
                                                    <?php
                                                }
                                            } else {  // si se tiene que apuntar
                                                ?>

                                                <a href="javascript:void(0);" data-href="aux_vota.php?c=<?php echo encrypt_url('votacion/grupos_add/idgr='.$row[0],$clave_encriptacion) ?>" class="openlargeModal btn btn-info  btn-block"><?= _("Apuntarme") ?></a>
                                            <?php } ?>

                                        </td>

                                    </tr>

                                    <?php
                                } while ($row = mysqli_fetch_array($result));
                                ?>
                            </tbody>
                        </table>
                        <?php
                    } else { ?>
                      <div class="alert alert-info">
                        <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                        <i class="fa fa-times simple-remove" aria-hidden="true"></i>
                        </button>
                        <span>
                          <b> Info - </b> ?<?=_("¡No se ha encontrado ningún grupo de trabajo!");?></span>
                        </div>
                        <?php
                    }
                    ?>
</div>
</div>
                    <!--Final-->

                    <script type="text/javascript" src="assets/DataTables/datatables.min.js" ></script>

                    <script type="text/javascript" language="javascript" class="init">
                      $(document).ready(function() {
                        $('#tabla1').DataTable({
                          responsive: true,
                          dom: 'Blfrtip',

                          lengthMenu: [
                            [10, 25, 50, -1],
                            [10, 25, 50, "Todos"]
                          ],
                          language: {
                            processing: "<?= _("Tratamiento en curso") ?>...",
                            search: "<?= _("Buscar") ?>:",
                            lengthMenu: "<?= _("Ver") ?> _MENU_  <?= _("resultados por pagina") ?>",
                            info: "<?= _("Mostrando") ?> _PAGE_ <?= _("de") ?> _PAGES_ <?= _("paginas") ?>",
                            infoEmpty: "<?= _("No se han encitrado resultados") ?>",
                            infoFiltered: "(<?= _("filtered from") ?> _MAX_ <?= _("total records") ?>)",
                            infoPostFix: "",
                            loadingRecords: "<?= _("Cargando") ?>...",
                            zeroRecords: "<?= _("No se han encontrado resultados - perdone") ?>",
                            emptyTable: "<?= _("No se han encontrado resultados - perdone") ?>",
                            paginate: {
                              first: "<?= _("Primero") ?>",
                              previous: "<?= _("Anterior") ?>",
                              next: "<?= _("Siguiente") ?>",
                              last: "<?= _("Ultimo") ?>"
                            },
                            aria: {
                              sortAscending: ": <?= _("activar para ordenar columna de forma ascendente") ?>",
                              sortDescending: ": <?= _("activar para ordenar columna de forma descendente") ?>"
                            }
                          }
                        });
                      });
                    </script>
                    <script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js" ></script>
        <script src="https://cdn.datatables.net/responsive/2.2.7/js/responsive.bootstrap4.min.js" ></script>
                    <!--end datatables -->
<?php } ?>

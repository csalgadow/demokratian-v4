<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
*  Archivo con los datos del usuario y su foto
*/
include('../private/config/config-votantes.inc.php');
?>
<div id="usuario">
  <div class="imagen_avatar">
    <?php if ($_SESSION['imagen'] == "peq_usuario.jpg" or $_SESSION['imagen'] == "") {
      $user_image= "temas/".$tema_web."/images/user.png";
     } else {
       $user_image= $upload_user."/".$_SESSION['imagen'];
     }?>
<img src="<?php echo $user_image; ?>" alt="<?php echo $_SESSION['nombre_usu']; ?>" width="90" height="90" class="img-circle" id="user_image"/>
 </div>
<ul>
<li>  <span class="user"><?= _("Usuario") ?>:</span> <span class="letra2_c_user"><?php echo $_SESSION['nombre_usu']; ?> <?php echo $_SESSION['apellido_usu']; ?></span></li>
<?php if ($es_municipal == false) { ?>
<li>  <span class="user"><?= _("Provincia") ?>:</span> <span class="letra2_c_user"><?php echo $_SESSION['provincia']; ?></span></li>
<li>  <span class="user"><?= _("CCAA") ?>: </span><span class="letra2_c_user"><?php echo $_SESSION['ccaa']; ?></span></li>
<?php if ($_SESSION['id_municipio'] != 0) { ?>
<li>  <span class="user"><?= _("Municipio") ?>: </span><span class="letra2_c_user"><?php echo $_SESSION['municipio']; ?></span></li>
<?php
    }
  }
?>
<li>  <span class="user text-red"> <?php
          if ($_SESSION['tipo_votante'] == 1) {
              echo $nombre_tipo_1;
          } elseif ($_SESSION['tipo_votante'] == 2) {
              echo $nombre_tipo_2;
          } elseif ($_SESSION['tipo_votante'] == 3) {
              echo $nombre_tipo_3;
          }
          ?></span></li>
  <li><span class="luser text-red">        <?php
          if ($_SESSION['nivel_usu'] == 2) {
              echo _("Administrador General");
          } elseif ($_SESSION['nivel_usu'] == 3) {
              echo _("Administrador CCAA");
          } elseif ($_SESSION['nivel_usu'] == 4) {
              echo _("Administrador provincia");
          } elseif ($_SESSION['nivel_usu'] == 5) {
              echo _("Administrador Grupo  provincial");
          } elseif ($_SESSION['nivel_usu'] == 6) {
              echo _("Administrador Grupo Estatal");
          } elseif ($_SESSION['nivel_usu'] == 7) {
              echo _("Administrador Grupo CCAA");
          }
          ?></span></li>
</ul>
</div>

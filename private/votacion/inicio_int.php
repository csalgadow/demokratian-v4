<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
*  Archivo que lista todas las votaciones que hay activas
*/
if(!isset($carga)){
  $carga =false;
  exit;
}
if($carga!="OK"){
  exit;
}else{
?>

<div class="card-body">

<!--comienzo accordion-->
    <?php
    $esta_activa = "si";
    $id_provincia = $_SESSION['localidad'];
    $tipo_user = $_SESSION['tipo_votante'];
    $id_ccaa = $_SESSION['id_ccaa_usu'];


    $fecha = date("Y-m-d", strtotime("-3 month"));

    echo _("Se muestran las votaciones con una antiguedad de menos de 3 meses");

    echo "<br/><p>";
    echo _("Usamos las zona horaria de");
    echo " <strong>" . date_default_timezone_get() . " </strong> ";
    echo _("para los horarios de las votaciones");
    echo"</p><p></p>";

    $cuenta = 1;
    while ($cuenta <= 7) {
        if ($cuenta == 1) {
            $sql = "SELECT ID ,activa, nombre_votacion ,resumen, tipo  ,fecha_com, fecha_fin ,activos_resultados, fin_resultados ,recuento,seguridad	FROM $tbn1  where demarcacion=1 and tipo_votante >=" . $_SESSION['tipo_votante'] . "  and activa like '$esta_activa' and  fecha_fin>'$fecha' ORDER BY  ID  DESC";
            $que_votacion = _("Estatales");
        }
        if ($cuenta == 2) {
            $sql = "SELECT ID ,activa, nombre_votacion ,resumen, tipo  ,fecha_com, fecha_fin ,activos_resultados, fin_resultados,recuento,seguridad	 FROM $tbn1 where demarcacion=2 and id_ccaa=" . $_SESSION['id_ccaa_usu'] . " and tipo_votante >= " . $_SESSION['tipo_votante'] . "   and activa like '$esta_activa'  and  fecha_fin>'$fecha' ORDER BY  ID DESC";
            $que_votacion = _("en la comunidad autonoma de") . $_SESSION['ccaa'] . "";
        }
        if ($cuenta == 3) {
            $sql = "SELECT ID ,activa, nombre_votacion ,resumen, tipo  ,fecha_com, fecha_fin ,activos_resultados, fin_resultados ,recuento, seguridad	 FROM $tbn1 where demarcacion=3 and id_provincia =" . $_SESSION['localidad'] . " and tipo_votante >= " . $_SESSION['tipo_votante'] . "  and activa like '$esta_activa'  and  fecha_fin>'$fecha' ORDER BY  ID DESC";
            $que_votacion = _("en la provincia de") . $_SESSION['provincia'] . "";
        }
        if ($cuenta == 4) { /// municipal
            $sql = "SELECT ID ,activa, nombre_votacion ,resumen, tipo  ,fecha_com, fecha_fin ,activos_resultados, fin_resultados, recuento,seguridad	 FROM $tbn1 where demarcacion=7 and id_municipio =" . $_SESSION['id_municipio'] . " and tipo_votante >= " . $_SESSION['tipo_votante'] . "  and activa like '$esta_activa'  and  fecha_fin>'$fecha' ORDER BY  ID DESC";
            $que_votacion = _("Municipales de") . $_SESSION['municipio'] . "";
        }

        if ($cuenta == 5) {
            $sql = "SELECT a.ID ,a.activa, 	a.nombre_votacion ,	a.resumen, a.tipo  ,a.fecha_com, a.fecha_fin ,a.activos_resultados, a.fin_resultados, a.recuento ,a.seguridad,a.id_grupo_trabajo	FROM $tbn1 a,$tbn6 b where (a.id_grupo_trabajo = b.id_grupo_trabajo) and b.id_usuario=" . $_SESSION['ID'] . " and a.demarcacion=4 and a.tipo_votante >=" . $_SESSION['tipo_votante'] . "   and a.activa like '$esta_activa' and b.estado=1  and  a.fecha_fin>'$fecha' ORDER BY  ID DESC";
            $que_votacion = _("Grupo provincial");
        }
        if ($cuenta == 6) {
            $sql = "SELECT a.ID ,a.activa, 	a.nombre_votacion ,	a.resumen, a.tipo  ,a.fecha_com, a.fecha_fin ,a.activos_resultados, a.fin_resultados ,a.recuento ,a.seguridad,a.id_grupo_trabajo	FROM $tbn1 a,$tbn6 b where (a.id_grupo_trabajo = b.id_grupo_trabajo) and b.id_usuario=" . $_SESSION['ID'] . " and a.demarcacion=5 and a.tipo_votante >=" . $_SESSION['tipo_votante'] . "   and a.activa like '$esta_activa' and b.estado=1 and  a.fecha_fin>'$fecha' ORDER BY  a.ID DESC";
            $que_votacion = _("Grupo autonomico");
        }
        if ($cuenta == 7) {
            $sql = "SELECT a.ID ,a.activa, 	a.nombre_votacion ,	a.resumen, a.tipo  ,a.fecha_com, a.fecha_fin ,a.activos_resultados, a.fin_resultados , a.recuento , a.seguridad,a.id_grupo_trabajo	FROM $tbn1 a,$tbn6 b where (a.id_grupo_trabajo = b.id_grupo_trabajo) and b.id_usuario=" . $_SESSION['ID'] . " and a.demarcacion=6 and a.tipo_votante >=" . $_SESSION['tipo_votante'] . "   and a.activa like '$esta_activa' and b.estado=1 and  a.fecha_fin>'$fecha' ORDER BY  a.ID DESC";
            $que_votacion = _("Grupo Estatal");
        }


        $result = mysqli_query($con, $sql);
        if ($row = mysqli_fetch_array($result)) {
            ?>

            <h3><?= _("Listado de votaciones") ?>   <?php
                if ($es_municipal == false) {
                    echo "$que_votacion";
                }
                ?> </h3>

      <div id="accordion" class="accordion">
        <div class="card mb-0">
                <?php
                mysqli_field_seek($result, 0);

                do {
                    ?>
                    <?php
                    $hoy = strtotime(date('Y-m-d H:i'));
                    $fecha_ini = strtotime($row[5]);
                    $fecha_fin = strtotime($row[6]);
                    $fecha_ini_ver = date("d-m-Y", strtotime($row[5]));
                    $fecha_fin_ver = date("d-m-Y", strtotime($row[6]));
                    $hora_ini_ver = date("H:i", strtotime($row[5]));
                    $hora_fin_ver = date("H:i", strtotime($row[6]));


                    if ($row[1] == "no") {
                        $activo = _("Votación NO activa");
                    } else if ($fecha_ini <= $hoy && $fecha_fin >= $hoy) {
                        //$activo="Votacion NO activa";

                        /* }
                          else { */
                        $id_votante = $_SESSION['ID'];
                        $id_votacion = $row[0];
                        $conta_vot = "SELECT id FROM $tbn2 WHERE id_votacion like \"$id_votacion\" and id_votante='$id_votante' ";

                        $result_cont_vot = mysqli_query($con, $conta_vot);
                        $quants_vot = mysqli_num_rows($result_cont_vot);

                        if ($quants_vot != 0) {
                            $activo = _("Ya ha votado");
                            $activo1 = '<span class="text-info">'._("Ya ha votado").'</span>';
                        } else {
                          if ($row[4] == 4) {
                              $dir = 'vota_debate/debate';
                              $texto1_activo = _("DEBATE ABIERTO");
                              $texto2_activo = _("Debate cerrado");
                              $image_activo = '<i class="fa fa-comments text-success"></i>';
                          } else {
                            $texto1_activo = _("VOTAR");
                            $texto2_activo = _("Votación NO activa");
                            $image_activo = '<i class="fa fa-bell text-success"></i>';
                            if ($row[4] == 1) {
                                $dir = 'vota_orden/primarias';
                            } else if ($row[4] == 2) {
                                $dir = 'vota_vut/vut';
                            } else if ($row[4] == 3 or $row[4] == 5  or $row[4] == 6 or $row[4] == 7) {
                                $dir = 'vota_encuesta/vota_encuesta';;
                            }
                          }

                            $activo1 = '<span class="text-warning ">'. $image_activo.' '.$texto1_activo .' </span>' ;
                            $activo = '<a href="votaciones.php?c='.encrypt_url($dir.'/idvot='.$row[0],$clave_encriptacion). '" class="btn btn-success btn-xs">'.$texto1_activo.'</a>';
                        }
                    } else {
                      if ($row[4] == 4) {
                          $dir = 'vota_debate/debate';
                          if ($fecha_fin <= $hoy) {
                              $texto1_activo = '<i class="fa fa-comments text-info"></i> '. _("Debate finalizado")  ;
                              $texto2_activo = '<a href="votaciones.php?c='.encrypt_url($dir.'/idvot='.$row[0],$clave_encriptacion).'"  class="btn btn-success btn-xs">' . _("Debate finalizado")  . '</a>';
                          } else {
                              $texto1_activo = '<i class="fa fa-comments text-info"></i> '._("Debate sin iniciar") ;
                              $texto2_activo = '<a href="votaciones.php?c='.encrypt_url($dir.'/idvot='.$row[0],$clave_encriptacion).'"  class="btn btn-success btn-xs">'.  _("Debate sin iniciar").'</a>';
                          }
                      } else  {
                            if ($fecha_fin <= $hoy) {
                                $texto1_activo = _("Votación finalizada");
                                $texto2_activo = _("Votación finalizada");
                            } else {
                                $texto1_activo = _("Votación sin iniciar");
                                $texto2_activo = _("Votación sin iniciar");
                            }
                        }

                        $activo1 = '<span class="text-info ">'.$texto1_activo .'</span>' ;
                        $activo = $texto2_activo;
                    }
                    ?>
                    <!-- comienzo votacion-->

    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#votacion_<?php echo md5($row[0]); ?>">
        <a class="card-title">
    <!-- comienzo datos titulo -->
    <?php echo "$activo1"; ?> | <?php echo $row[2] ?>
            <?php if ($cuenta == 5 or $cuenta == 6 or $cuenta == 7) {
                  $sql_grupo = mysqli_query($con, "SELECT subgrupo FROM $tbn4 where ID=" . $row[11] . " ");
                  $row_grupo = mysqli_fetch_row($sql_grupo);
            ?>
            |  <?= _("Grupo") ?> : <?php echo $row_grupo[0]; ?>
            <?php } ?>

    <!-- fin  datos titulo -->
          </a>
    </div>
    <div id="votacion_<?php echo md5($row[0]); ?>" class="card-body collapse" data-parent="#accordion" >
    <!-- cuerpo -->
    <p>
      <?= _("Estado de la votación") ?>: <?php echo "$activo"; ?>
      <br/> <?= _("Fechas votación: desde las") ?> <?php echo $hora_ini_ver; ?> del  <?php echo $fecha_ini_ver; ?> <?= _("a las") ?> <?php echo $hora_fin_ver; ?> <?= _("del") ?> <?php echo $fecha_fin_ver; ?>
      <br/> <?= _("Tipo de votación") ?>: <?php
                if ($row[4] == 1) {
                echo _("primarias");
                if ($row[9] == 0) {
                  echo _("con recuento BORDA");
                } else if ($row[9] == 1) {
                  echo _("con recuento DOWDALL");
                }
              } else if ($row[4] == 2) {
                echo _("VUT");
              } else if ($row[4] == 3) {
                echo _("Encuesta");
              } else if ($row[4] == 4) {
                echo _("Debate");
              }else if ($row[4] == 5) {
                echo _("ENCUESTA con poderación del voto según tipo votante");
              }else if ($row[4] == 6) {
                echo _("ENCUESTA con poderación del voto según tipo votante");
              }else if ($row[4] == 7) {
                echo _("ENCUESTA");
              }
              ?>
              <?php

              if ($row[7] == "si") {
                echo "<br/>";
                if ($row[4] != 2 or $row[4] != 4) {
                                              $id_encriptada = md5($row[0]);
                                              $nombre_fichero1 = $id_encriptada . ".php";
                                              $nombre_fichero2 = $id_encriptada. "_list.php";
                                              ?>
                                              <a href="javascript:void(0);" data-href="aux_vota.php?d=<?php echo $nombre_fichero1 ?>" class="openextraLargeModal btn btn-primary btn-xs"><?= _("Resultados") ?></a>
                                              <a href="javascript:void(0);" data-href="aux_vota.php?d=<?php echo $nombre_fichero2 ?>" class="openextraLargeModal btn btn-primary btn-xs"><?= _("Listar votos") ?></a>

                                              <?php

                                                if ($row[10] == 2 or $row[10] == 4) {
                                                  if ($row[4] == 1){
                                                  ?>
                                                    <a href="votaciones.php?c=<?php echo encrypt_url('vota_orden/vota_primarias_ver/idvot='.$row[0],$clave_encriptacion) ?>" class="btn btn-primary btn-xs"><?= _("Comprobar mi voto") ?></a>
                                                    <?php
                                                  }else if($row[4] == 3 || $row[4] == 5 || $row[4] == 6 || $row[4] == 7){
                                                    ?>
                                                      <a href="votaciones.php?c=<?php echo encrypt_url('vota_encuesta/vota_encuesta_ver/idvot='.$row[0],$clave_encriptacion) ?>" class="btn btn-primary btn-xs"><?= _("Comprobar mi voto") ?></a>
                                                      <?php
                                                  }
                                                }
                                            } else if ($row[4] == 2) {
                                              $id_encriptada = md5($row[0]);
                                              $nombre_fichero2 = $id_encriptada. "_list.php";
                                              ?>
                                              <a href="javascript:void(0);" data-href="aux_vota.php?c=<?php echo encrypt_url('vota_vut/resultados/idvot='.$row[0],$clave_encriptacion) ?>" class="openextraLargeModal btn btn-primary btn-xs"><?= _("Resultados") ?></a>
                                              <a href="javascript:void(0);" data-href="aux_vota.php?d=<?php echo $nombre_fichero2 ?>" class="openextraLargeModal  btn btn-primary btn-xs"><?= _("Listar votos") ?></a>


                                                <?php
                                            } else if ($row[4] == 4) {
                                              ?>
                                                <a href="votaciones.php?c=<?php echo encrypt_url('debate/esultados_debate/idvot='.$row[0],$clave_encriptacion) ?>" class="btn btn-primary btn-xs"><?= _("Resultados") ?></a>

                                                <?php
                                            }
                                        } else {

                                        }
                                        ?>



                                        <?php echo $row[3] ?>
                                      </p>
        <!-- fin cuerpo-->
</div>



                         <!--fin votacion-->
                    <?php
                } while ($row = mysqli_fetch_array($result));
                ?>


                </div>
            </div>
            <?php
        }
        ?>
        <?php
        $cuenta++;
    }
    ?>

<!--fin acordion -->

</div>


<?php }?>

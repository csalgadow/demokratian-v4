<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
*  Archivo que se carga en un modal y que permite apuntarse a un grupo. Realiza la petición mediante ajax y el archivo que realiza la acción es grupos_modificar.php
*/
if(!isset($carga)){
  $carga =false;
  exit;
}
if($carga!="OK"){
  exit;
}else{

$correcto = "";

if (empty($variables['idgr'])) {
    echo _("Por favor no altere la fuente");
    exit;
}

$idgr = fn_filtro_numerico($con, $variables['idgr']);

    $options_sub = sprintf("select ID, subgrupo,acceso 	 from $tbn4 where ID=%d", (int) $idgr);

    $per = mysqli_query($con, $options_sub);
    $num_rs_per = mysqli_num_rows($per);
    if ($num_rs_per == 0) {
        echo _("No existen grupos con ese Identificador");
        exit;
    }
    $rs_per = mysqli_fetch_assoc($per);
    ?>

<?php
$options_usu = "select  ID from $tbn6 where  id_grupo_trabajo=" . $idgr . " and id_usuario=" . $_SESSION['ID'] . " ";
$result_cont = mysqli_query($con, $options_usu);
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

$quants = mysqli_num_rows($result_cont);
?>

<div class="modal-header">
       <h5 class="modal-title"><?php echo $rs_per['subgrupo'] ?></h5>
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
   </div>
   <div class="modal-body">

<?php
if ($quants != 0) {
    ?>

            <div class="alert alert-success">
                <strong><?= _("Ya te has apuntado a este grupo") ?>, <br/>
                    <?= _("en breve podras participar") ?>
            </div>

    <?php
} else {
?>
  <div class="alert alert-warning pregunta">
<?= _("¿Estas seguro que quieres apuntarte este grupo de trabajo?") ?>
</div>

<div id="respuesta"> </div>
            <form action="javascript: fn_modificar();" method="post" id="frm_per" name="frm_per" class="well">

                <div id="el_boton">
                    <input name="add_sub" type=submit class="btn btn-primary pull-right" id="add_directorio" value="<?= _("Apuntarme a") ?> <?php echo $rs_per['subgrupo'] ?>">
                    </td>
                </div>
                <br>
            </form>

            <p></p>

            <!---->


    <script language="javascript" type="text/javascript">

        function fn_modificar() {

            var str = $("#frm_per").serialize();
            $.ajax({
                url: 'aux_vota.php?c=<?php echo encrypt_url('votacion/grupos_modificar/idgr='.$rs_per['ID'].'&conec='.$rs_per['acceso'],$clave_encriptacion) ?>',
                data: str,
                type: 'post',
                success: function (data) {
                    $("#respuesta").html(" " + data + " ");
                    $("#respuesta").show("slow");
                    $("#el_boton").hide("slow");
                    $(".pregunta").hide("slow");

                },
                error: function () {
                    // Fail message
                    $("#respuesta").html("<div class='alert alert-danger'>");
                    $("#respuesta > .alert-danger").html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                            .append("</button>");
                    $("#respuesta > .alert-danger").append("<strong>Uppps! <?= _("el servidor no esta respondiendo") ?>...</strong> <?= _("¡Perdone por las molestias!") ?>");
                    $("#respuesta > .alert-danger").append('</div>');
                },
            });
        }
        ;

    </script>

    <?php
}?>
</div>
<?php } ?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
*  Archivo con el formulario de contacto y que envía los datos mediante ajax, procesa el envío. basicos_php/procesar_notificar
*/
if(!isset($carga)){
  $carga =false;
  exit;
}
if($carga!="OK"){
  exit;
}else{
require_once("../private/inc_web/verifica.php");
$nivel_acceso = 11;
require_once('../private/inc_web/nivel_acceso.php');
require_once("../private/basicos_php/lang.php");

$result = mysqli_query($con, "SELECT  correo_usuario FROM $tbn9 WHERE id ='" . $_SESSION['ID'] . "' ");
$row = mysqli_fetch_row($result);
$email2 = $row[0];
?>


<div class="modal-header">
                <h5 class="modal-title"><?= _("Notificar un  problema") ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                    <!---texto --->

                <form method="post" name="formulario_Contacto" class="well" id="formulario_Contacto" >
                <h3>  <?= _("Si crees que hay un error o tienes algún problema, rellena este formulario") ?>:</h3>

                    <p>
                      <?= _("Hola soy") ?> <?php echo $_SESSION['nombre_usu']; ?>
                      <?php if ($es_municipal == false) { ?>, <?= _("de la provincia de") ?> <?php echo $_SESSION['provincia']; ?><?php } ?>
                      <?= _("y mi dirección de Email es") ?>:&nbsp; <?php echo $email2; ?>
                    </p>
                    <input name="nombre2" id="nombre2"  type="hidden" value="<?php echo $_SESSION['nombre_usu']; ?>">
                    <input name="email2" id="email2" type="hidden" value=" <?php echo $email2; ?>">
                    <input name="provincia2" id="provincia2" type="hidden" value="<?php echo $_SESSION['localidad']; ?>">
                    <?= _("Quiero contactar con") ?>

                    <div class="control-group">
                        <div class="controls">
                            <label>
                                <input name="contacto" type="radio" id="contacto_0" value="admin" checked>
                                <?= _("Administrador") ?> <?php
                                if ($es_municipal == false) {
                                    echo "provincial";
                                }
                                ?></label>
                            <br>
                            <label>
                                <input type="radio" name="contacto"  id="contacto_1" value="tecni">
                            </label>
                            <?= _("Responsable técnico") ?>
                            <p class="help-block"></p>
                        </div>
                    </div>


                    <div class="control-group">
                        <div class="controls">

                            <label for="asunto"><?= _("Asunto") ?>: </label>
                            <input type="text" class="form-control" placeholder="<?= _("Asunto a tratar") ?>" id="asunto" name="asunto" required data-validation-required-message="<?= _("Por favor, complete este campo") ?>" />
                            <p class="help-block"></p>
                        </div>
                    </div>



                    <div class="control-group">
                        <div class="controls">

                            <textarea rows="10" cols="100" class="form-control"
                                      placeholder="<?= _("Cuentanos el problema") ?>" id="texto" name="texto" required
                                      data-validation-required-message="<?= _("Cuentanos el problema") ?>" minlength="5"
                                      data-validation-minlength-message="<?= _("Min 5 characteres") ?>"
                                      maxlength="999" style="resize:none"></textarea>
                            <p class="help-block"></p>
                        </div>
                    </div>


                    <p class="letra_pequeña"><?= _("Por favor, si has encontrado algún error, intenta darnos  todos los detalles y datos para intentar localizarlo, si puedes hacer una  captura de pantalla del error, indícanoslo por si necesitamos contactar contigo  para recabar más información") ?></p>

                    <button type="submit" class="btn btn-primary pull-right"><?= _("Enviar") ?></button>
                    <br/>
                </form>

                <div id="success2"> </div>


          </div>



        <script type="text/javascript">
        $(document).ready(function () {
            $("#formulario_Contacto").bind("submit",function(){
                // Capturamnos el boton de envío
              //  var btnEnviar = $("#btnEnviar");
                $.ajax({
                    type: "POST",
                    //url: $(this).attr("action"),
                    url: "aux_vota.php?c=<?php echo encrypt_url('basicos_php/procesar_notificar/s=SD',$clave_encriptacion) ?>",

                    data:$(this).serialize(),
                    cache: false,
                    beforeSend: function(){
                        /*
                        * Esta función se ejecuta durante el envió de la petición al
                        * servidor.
                        * */
                        // btnEnviar.text("Enviando"); Para button
                        //btnEnviar.val("Enviando"); // Para input de tipo button
                        //btnEnviar.attr("disabled","disabled");
                    },
                    complete:function(data){
                        /*
                        * Se ejecuta al termino de la petición
                        * */
                        //btnEnviar.val("Enviar formulario");
                        //btnEnviar.removeAttr("disabled");
                    },
                    success: function(data){
                        /*
                        * Se ejecuta cuando termina la petición y esta ha sido
                        * correcta
                        * */
                        var result = data.trim().split("##");
                        if (result[0] == 'OK') {
                            $("#formulario_Contacto").hide("slow");
                            $('#success2').html(result[1]);
                            $('#success2').show();
                            $('#formulario_Contacto').trigger("reset");
                        } else if (result[0] == 'ERROR') {
                          $("#formulario_Contacto").hide("slow");
                            $("#success2").show();
                            $('#success2').html(result[1]);
                            $('#formulario_Contacto').trigger("reset");
                        } else{
                          $("#formulario_Contacto").hide("slow");
                          $("#success2").show();
                          $('#success2').html(data);
                          $('#formulario_Contacto').trigger("reset");
                        }
                    },
                    error: function(data){
                        /*
                        * Se ejecuta si la peticón ha sido erronea
                        * */
                        $('#success2').html("<div class='alert alert-danger'>");
                        $('#success2 > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                .append(" " +contrasenaForm+ " , hay un error</button>");
                    }
                });
                // Nos permite cancelar el envio del formulario
                return false;
            });
        });

        </script>
<?php } ?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
*  Archivo que lista todas las votaciones que hay activas en un grupo
*/
if(!isset($carga)){
  $carga =false;
  exit;
}
if($carga!="OK"){
  exit;
}else{


$id_provincia = $_SESSION['localidad'];
$tipo_user = $_SESSION['tipo_votante'];
$idgr = fn_filtro_numerico($con, $variables['idgr']);
$texto1_activo = "";
$texto2_activo = "";
?>

                    <?php
                    $sql_grupo = mysqli_query($con, "SELECT subgrupo,texto FROM $tbn4 where ID=" . $idgr . " ");
                    $row_grupo = mysqli_fetch_row($sql_grupo);
                    ?>


                    <div class="card-header"> <h1 class="card-title"><?= _("Listado de votaciones de este grupo") ?> </h1> </div>


                    <div class="card-body">
                      <div class="col-md content">

                        <p><a href="javascript:void(0);" data-href="aux_vota.php?c=<?php echo encrypt_url('votacion/ver_grupo/idgr='.$idgr,$clave_encriptacion) ?>" class="openextraLargeModal btn btn-info float-right"><i class="fa fa-users fa-lg"> </i>  <?= _("ver miembros de este grupo") ?>  </a>
                        </p>

                        <h3><?php echo $row_grupo[0]; ?></h3>

                    <p>   <?php echo $row_grupo[1]; ?> </p>
                    <?php
/////hay que sacar datos del grupo
                    $esta_activa = "si";
                    $sql = "SELECT ID ,activa, nombre_votacion ,resumen, tipo  ,fecha_com, fecha_fin ,activos_resultados, fin_resultados ,recuento,seguridad  FROM $tbn1 where id_grupo_trabajo =" . $idgr . " and tipo_votante >=" . $_SESSION['tipo_votante'] . " and activa like '$esta_activa' ORDER BY  ID  DESC ";
                    $result = mysqli_query($con, $sql);
                    if ($row = mysqli_fetch_array($result)) {
                        ?>

                        <div id="accordion" class="accordion">
                          <div class="card mb-0">


                            <?php
                            mysqli_field_seek($result, 0);
                            do {
                                ?>      <?php
                                $hoy = strtotime(date('Y-m-d'));
                                $fecha_ini = strtotime($row[5]);
                                $fecha_fin = strtotime($row[6]);
                                $fecha_ini_ver = date("d-m-Y", strtotime($row[5]));
                                $fecha_fin_ver = date("d-m-Y", strtotime($row[6]));
                                $hoy = strtotime(date('Y-m-d H:i'));
                                $hora_ini_ver = date("H:i", strtotime($row[5]));
                                $hora_fin_ver = date("H:i", strtotime($row[6]));


                                if ($row[1] == "no") {
                                    $activo = _("Votación NO activa");
                                } else if ($fecha_ini <= $hoy && $fecha_fin >= $hoy) {
                                    $id_votante = $_SESSION['ID'];
                                    $id_votacion = $row[0];
                                    $conta_vot = "SELECT id FROM $tbn2 WHERE id_votacion like \"$id_votacion\" and id_votante='$id_votante' ";

                                    $result_cont_vot = mysqli_query($con, $conta_vot);
                                    $quants_vot = mysqli_num_rows($result_cont_vot);

                                    if ($quants_vot != 0) {
                                      $activo = _("Ya ha votado");
                                      $activo1 = '<span class="text-info">'._("Ya ha votado").'</span>';
                                    } else {


                                      if ($row[4] == 1) {
                                          $dir = 'vota_orden/primarias';
                                          $texto1_activo = _("VOTAR");
                                          $texto2_activo = _("Votación NO activa");
                                          $image_activo = '<i class="fa fa-bell text-success"></i>';
                                      } else if ($row[4] == 2) {
                                          $dir = 'vota_vut/vut';
                                          $texto1_activo = _("VOTAR");
                                          $texto2_activo = _("Votación NO activa");
                                          $image_activo = '<i class="fa fa-bell text-success"></i>';
                                      } else if ($row[4] == 3) {
                                          $dir = 'vota_encuesta/vota_encuesta';
                                          $texto1_activo = _("VOTAR");
                                          $texto2_activo = _("Votación NO activa");
                                          $image_activo = '<i class="fa fa-bell text-success"></i>';
                                      } else if ($row[4] == 4) {
                                          $dir = 'vota_debate/debate';
                                          $texto1_activo = _("DEBATE ABIERTO");
                                          $texto2_activo = _("Debate cerrado");
                                          $image_activo = '<i class="fa fa-comments text-success"></i>';
                                      }


                                      $activo1 = '<span class="text-warning ">'. $image_activo.' '.$texto1_activo .' </span>' ;
                                      $activo = '<a href="votaciones.php?c='.encrypt_url($dir.'/idvot='.$row[0],$clave_encriptacion). '" class="btn btn-success btn-xs">'.$texto1_activo.'</a>';
                                    }
                                } else {
                                  if ($row[4] == 4) {
                                        $dir = 'vota_debate/debate';
                                        $texto1_activo = '<i class="fa fa-comments text-info"></i> '. _("Debate finalizado")  ;
                                        $texto2_activo = '<a href="votaciones.php?c='.encrypt_url($dir.'/idvot='.$row[0],$clave_encriptacion).'"  class="btn btn-success btn-xs">' . _("Debate finalizado")  . '</a>';
                                    }else  {
                                        $texto1_activo = _("Votación NO activa");
                                        $texto2_activo = _("Votación NO activa");
                                    }

                                    $activo1 = $texto1_activo;
                                    $activo = $texto2_activo;
                                }
                                ?>
                                <!-- comienzo votacion-->

                <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#votacion_<?php echo md5($row[0]); ?>">
                    <a class="card-title">
                <!-- comienzo datos titulo -->
                                                <?php echo $row[2] ?>
                                             <?php echo "$activo1"; ?>  </a>
                                         </div>
                                         <div id="votacion_<?php echo md5($row[0]); ?>" class="card-body collapse" data-parent="#accordion" >
                                         <!-- cuerpo -->
                                         <p>

                                            Tipo de votacion :
                                            <?php
                                            if ($row[4] == 1) {
                                                echo _("primarias");
                                            } else if ($row[4] == 2) {
                                                echo _("VUT");
                                            } else if ($row[4] == 3) {
                                                echo _("encuesta");
                                            }
                                            ?>

                                              <br/>  <?= _("Estado de la votación") ?>:
                                                <?php echo "$activo";
                                                ?>



                                            <?php

                                            if ($row[7] == "si") {
                                              echo "<br/>";
                                              if ($row[4] != 2 or $row[4] != 4) {
                                                                            $id_encriptada = md5($row[0]);
                                                                            $nombre_fichero1 = $id_encriptada . ".php";
                                                                            $nombre_fichero2 = $id_encriptada. "_list.php";
                                                                            ?>
                                                                            <a href="javascript:void(0);" data-href="aux_vota.php?d=<?php echo $nombre_fichero1 ?>" class="openextraLargeModal btn btn-primary btn-xs"><?= _("Resultados") ?></a>
                                                                            <a href="javascript:void(0);" data-href="aux_vota.php?d=<?php echo $nombre_fichero2 ?>" class="openextraLargeModal  btn btn-primary btn-xs"><?= _("Listar votos") ?></a>

                                                                            <?php

                                                                              if ($row[10] == 2 or $row[10] == 4) {
                                                                                if ($row[4] == 1){
                                                                                ?>
                                                                                  <a href="votaciones.php?c=<?php echo encrypt_url('vota_orden/vota_primarias_ver/idvot='.$row[0],$clave_encriptacion) ?>" class="btn btn-primary btn-xs"><?= _("Comprobar mi voto") ?></a>
                                                                                  <?php
                                                                                }else if($row[4] == 3 || $row[4] == 5 || $row[4] == 6 || $row[4] == 7){
                                                                                  ?>
                                                                                    <a href="votaciones.php?c=<?php echo encrypt_url('vota_encuesta/vota_encuesta_ver/idvot='.$row[0],$clave_encriptacion) ?>" class="btn btn-primary btn-xs"><?= _("Comprobar mi voto") ?></a>
                                                                                    <?php
                                                                                }
                                                                              }
                                                                          } else if ($row[4] == 2) {
                                                                            $id_encriptada = md5($row[0]);
                                                                            $nombre_fichero2 = $id_encriptada. "_list.php";
                                                                            ?>
                                                                            <a href="javascript:void(0);" data-href="aux_vota.php?c=<?php echo encrypt_url('vota_vut/resultados/idvot='.$row[0],$clave_encriptacion) ?>" class="openextraLargeModal btn btn-primary btn-xs"><?= _("Resultados") ?></a>
                                                                            <a href="javascript:void(0);" data-href="aux_vota.php?d=<?php echo $nombre_fichero2 ?>" class="openextraLargeModal  btn btn-primary btn-xs"><?= _("Listar votos") ?></a>


                                                                              <?php
                                                                          } else if ($row[6] == 4) {
                                                                            ?>
                                                                              <a href="votaciones.php?c=<?php echo encrypt_url('debate/esultados_debate/idvot='.$row[0],$clave_encriptacion) ?>" class="btn btn-primary btn-xs"><?= _("Resultados") ?></a>

                                                                              <?php
                                                                          }
                                                                      }  else {

                                            }

                                            ?>

                                            <br/>
                                            <?= _("Comienzo de la votación :Desde las") ?> <?php echo $hora_ini_ver; ?> <?= _("el dia") ?> <?php echo $fecha_ini_ver; ?>
                                            <br/><?= _("Final de la votación :Hasta las") ?> <?php echo $hora_fin_ver; ?> <?= _("del dia") ?> <?php echo $fecha_fin_ver; ?>
                                            <br/>
                                            <?php echo $row[5] ?>
                                          </p>


                                          <!-- fin cuerpo-->
                                          </div>


                                <?php
                            } while ($row = mysqli_fetch_array($result));
                            ?>
                          </div>
                        </div>
                        <?php
                    } else {?>
                      <div class="alert alert-info">
                        <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                        <i class="fa fa-times simple-remove" aria-hidden="true"></i>
                        </button>
                        <span>
                          <b> Info - </b> ?<?=_("¡No se ha encontrado ninguna votación!");?></span>
                        </div>
                        <?php
                    }

                    ?>

                  </div>
                  </div>


<?php } ?>

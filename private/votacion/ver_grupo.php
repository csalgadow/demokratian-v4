<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
*  Archivo que lista todos los usuarios que hay apuntados a un grupo
*/
if(!isset($carga)){
  $carga =false;
  exit;
}
if($carga!="OK"){
  exit;
}else{



if (empty($variables['idgr'])) {
    echo _("Por favor no altere el fuente");
    exit;
}



$idgr = fn_filtro_numerico($con, $variables['idgr']);

$sql = "SELECT a.ID,  a.nombre_usuario,  a.correo_usuario,  a.tipo_votante, a.usuario,a.apellido_usuario, a.imagen_pequena, a.perfil FROM $tbn9 a,$tbn6 b where (a.ID= b.id_usuario) and b.id_grupo_trabajo=" . $idgr . " and  b.estado = 1   order by a.nombre_usuario ";

$result = mysqli_query($con, $sql);
?>

<link href="https://fonts.googleapis.com/css?family=Raleway:400,500,700|Roboto:400,900" rel="stylesheet">

<!-- Bootstrap core CSS -->
<link href="assets/bootstrap-4.5.0/css/bootstrap.css" rel="stylesheet">
<!-- font-awesome CSS Files -->
<link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- DataTables for Bootstrap4 CSS File -->
<link rel="stylesheet" type="text/css" href="assets/DataTables/datatables.min.css" />

<link href="temas/<?php echo "$tema_web"; ?>/css/adminStyle.css" rel="stylesheet">


    <div class="modal-header">
                 <h5 class="modal-title"><?= _("Miembros de este grupo") ?></h5>
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
             </div>
             <div class="modal-body">


<!---->
        <?php
        if ($row = mysqli_fetch_array($result)) {
            ?>
<table id="tabla1" class="table table-striped table-bordered dt-responsive nowrap" data-page-length="25" >

                <thead>
                    <tr>
                        <th width="1%">&nbsp;</th>
                        <th width="20%"><?= _("Nombre") ?></th>
                        <th width="20%"><?= _("Nick") ?></th>
                        <th width="58%">&nbsp;</th>
                        <th width="1%">&nbsp;</th>

                    </tr>
                </thead>

                <tbody>
                    <?php
                    mysqli_field_seek($result, 0);

                    do {
                        ?>
                        <tr>
                            <td><?php if ($row[6] == "peq_usuario.jpg" or $row[6] == "") { ?><img src="../temas/<?php echo "$tema_web"; ?>/images/user.png" width="70" height="70" /><?php } else { ?><img src="<?php echo $upload_user; ?>/<?php echo"$row[6]"; ?>" alt="<?php echo"$row[1]"; ?> <?php echo"$row[4]"; ?>" width="70" height="70"  /> <?php } ?></td>
                            <td><?php echo "$row[1]" ?> <?php echo "$row[5]" ?></td>
                            <td><?php echo "$row[4]" ?></td>
                            <td><?php echo "$row[7]" ?> </td>
                            <td><?php
                                if ($row[3] == 1) {
                                    echo _("Afiliado");
                                } else if ($row[3] == 2) {
                                    echo _("Simpatizante Verificado");
                                } else if ($row[3] == 3) {
                                    echo _("Simpatizante");
                                }
                                ?></td>
                        </tr>

                        <?php
                    } while ($row = mysqli_fetch_array($result));
                    ?>


                </tbody>
            </table>

            <?php
        } else {
          ?>
            <div class="alert alert-info">
              <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
              <i class="fa fa-times simple-remove" aria-hidden="true"></i>
              </button>
              <span>
                <b> Info - </b> <?=_("¡No se ha encontrado ningún miembro en este grupo!");?></span>
              </div>
              <?php

        }
        ?>

        <!---->


    </div>



<script src="assets/jquery-3.5.1/jquery.min.js" ></script>
<script src="assets/bootstrap-4.5.0/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="assets/DataTables/datatables.min.js" ></script>
<script src="assets/DataTables/Buttons-1.6.2/js/dataTables.buttons.min.js" ></script>
<!--<script src="assets/DataTables/JSZip-2.5.0/jszip.min.js" ></script>
<script src="assets/DataTables/pdfmake-0.1.36/pdfmake.min.js" ></script>
<script src="assets/DataTables/pdfmake-0.1.36/vfs_fonts.js" ></script>-->
<script src="assets/DataTables/Buttons-1.6.2/js/buttons.html5.min.js" ></script>

<script type="text/javascript" language="javascript" class="init">
  $(document).ready(function() {
    $('#tabla1').DataTable({
      responsive: true,
      dom: 'Blfrtip',
      buttons: [
      //  'copyHtml5',
      //  'excelHtml5',
      //  'csvHtml5',
      //  'pdfHtml5'
      ],
      lengthMenu: [
        [10, 25, 50, -1],
        [10, 25, 50, "Todos"]
      ],
      language: {
        buttons: {
          copy: '<?= _("Copiar") ?>',
          copyTitle: '<?= _("Copiado en el portapapeles") ?>',
          copyKeys: '<?= _("Presione") ?> <i> ctrl </i> o <i> \ u2318 </i> + <i> C </i> <?= _("para copiar los datos de la tabla a su portapapeles") ?>. <br> <br> <?= _("Para cancelar, haga clic en este mensaje o presione Esc") ?>.',
          copySuccess: {
            _: '%d <?= _("lineas copiadas") ?>',
            1: '1 <?= _("linea copiada") ?>'
          }
        },
        processing: "<?= _("Tratamiento en curso") ?>...",
        search: "<?= _("Buscar") ?>:",
        lengthMenu: "<?= _("Ver") ?> _MENU_  <?= _("resultados por pagina") ?>",
        info: "<?= _("Mostrando") ?> _PAGE_ <?= _("de") ?> _PAGES_ <?= _("paginas") ?>",
        infoEmpty: "<?= _("No se han encitrado resultados") ?>",
        infoFiltered: "(<?= _("filtered from") ?> _MAX_ <?= _("total records") ?>)",
        infoPostFix: "",
        loadingRecords: "<?= _("Cargando") ?>...",
        zeroRecords: "<?= _("No se han encontrado resultados - perdone") ?>",
        emptyTable: "<?= _("No se han encontrado resultados - perdone") ?>",
        paginate: {
          first: "<?= _("Primero") ?>",
          previous: "<?= _("Anterior") ?>",
          next: "<?= _("Siguiente") ?>",
          last: "<?= _("Ultimo") ?>"
        },
        aria: {
          sortAscending: ": <?= _("activar para ordenar columna de forma ascendente") ?>",
          sortDescending: ": <?= _("activar para ordenar columna de forma descendente") ?>"
        }
      }
    });
  });
</script>
<script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js" ></script>
<script src="https://cdn.datatables.net/responsive/2.2.7/js/responsive.bootstrap4.min.js" ></script>
<!--end datatables -->

<?php } ?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo con los datos del pie de la página del blog
*/
 ?>
<?php include_once('../private/inc_web/version.php'); ?>
<!-- pie
================================================== -->

<footer class="demokratian-footer bg-black">
  <div class="container">
    <div class="row">
    <div class="col-lg-6 col-xs-12 text-lg-left text-center">
      <h4 class="white text-center"><?php
      if (isset($texto_pie_pagina)){
      echo $texto_pie_pagina;
      }else{
      echo _("Sistema de votaciones")." ".$nombre_web;
      } ?> </h4>
    </div>



    <div class="col-lg-6 col-xs-12 text-lg-right text-center">
      <ul class="list-inline">
            <li class="list-inline-item">
              <a href="index.php"><?= _("Inicio")?></a>
            </li>

            <li class="list-inline-item">
              <a href="https://demokratian.org"><?= _("Sobre")?> demokratian </a>
            </li>

            <?php

            $consultas = " SELECT id,title,url FROM $tbn36 where activo='2' ";
            $resultados=mysqli_query($con,$consultas);

              if ($row_pie = mysqli_fetch_array($resultados)) {
                mysqli_field_seek($resultados, 0);
                do {
              ?>
              <li class="list-inline-item">
                <a href="<?php echo "$row_pie[2]" ?>"><?php echo "$row_pie[1]" ?></a>
              </li>

                <?php
              } while ($row_pie = mysqli_fetch_array($resultados));
            }


             ?>
            <li class="list-inline-item">
              <a data-toggle="modal" href="#"  class="white" data-target=".contacta"><?= _("Contactar")?></a>
            </li>
          </ul>
    </div>
    </div>


    <div class="row">


    <div class="col-lg-12 col-xs-12 text-lg-right text-center">
            <p>&nbsp;</p>
      <p  class="white"><a href="https://www.demokratian.org" class="white">Demokratian v.<?php echo $DKversion; ?></a> &copy; Carlos Salgado Werner</p>

    </div>
    </div>
  </div>
</footer>

<!-- END pie
================================================== -->

<?php include("../private/blog/modal_contactar.php"); ?>

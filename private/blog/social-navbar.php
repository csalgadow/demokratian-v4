<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo con el bloque con la barra del menú social del blog
*/
if(!isset($bloque_activo)){
  include("../private/blog/error404.php");
  exit;
}
if($bloque_activo!="activo"){
  include("../private/blog/error404.php");
  exit;
}else{
?>
<div id="social-navbar">
  <nav class="nav social-nav pull-right d-lg-inline">
      <?php if($active_facebook == true){?>
        <!--Facebook-->
        <a href="<?php  echo $url_facebook; ?>" class="btn-floating btn-sm" type="button" role="button" title="facebook"><i class="social fa fa-facebook"></i></a>
      <?php }
        if($active_twitter == true){
      ?>
        <!--Twitter-->
        <a  href="<?php  echo $url_twitter; ?>" class="btn-floating btn-sm " type="button" role="button" title="twitter"><i class="social fa fa-twitter"></i></a>
      <?php }
        if($active_Google == true){
       ?>
        <!--Google +-->
        <a  href="<?php  echo $url_Google; ?>" class="btn-floating btn-sm" type="button" role="button" title="Google+"><i class="social fa fa-google-plus"></i></a>
      <?php }
        if($active_Linkedin == true){
       ?>
        <!--Linkedin-->
        <a  href="<?php  echo $url_Linkedin; ?>" class="btn-floating btn-sm" type="button" role="button" title="Linkedin"><i class="social fa fa-linkedin"></i></a>
      <?php }
        if($active_Instagram == true){
       ?>
        <!--Instagram-->
        <a  href="<?php  echo $url_Instagram; ?>" class="btn-floating btn-sm" type="button" role="button" title="Instagram"><i class="social fa fa-instagram"></i></a>
      <?php }
        if($active_Pinterest == true){
       ?>
        <!--Pinterest-->
        <a  href="<?php  echo $url_Pinterest; ?>" class="btn-floating btn-sm" type="button" role="button" title="Pinterest"><i class="social fa fa-pinterest"></i></a>
      <?php }
        if($active_Youtube == true){
       ?>
        <!--Youtube-->
        <a  href="<?php  echo $url_Youtube; ?>" class="btn-floating btn-sm" type="button" role="button" title="Youtube"><i class="social fa fa-youtube"></i></a>
      <?php }
        if($active_Slack == true){
       ?>
        <!--Slack-->
        <a  href="<?php  echo $url_Slack; ?>" class="btn-floating btn-sm" type="button" role="button" title="Slack"><i class="social fa fa-slack"></i></a>
      <?php }
        if($active_tumblr == true){
       ?>
        <!--Tumblr-->
        <a  href="<?php  echo $url_tumblr; ?>" class="btn-floating btn-sm" type="button" role="button" title="Tumblr"><i class="social fa fa-tumblr"></i></a>
      <?php }
        if($active_Email == true){
       ?>
        <!--Email-->
        <a  href="mailto:<?php  echo $url_Email; ?>" class="btn-floating btn-sm" type="button" role="button" title="Email"><i class="social fa fa-envelope"></i></a>
      <?php }
       ?>
  </nav>
</div>

<?php }?>

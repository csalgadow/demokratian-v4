<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que carga el bloque con el menú de navegación del blog
*/

if(!isset($bloque_activo)){
  include("../private/blog/error404.php");
  exit;
}
if($bloque_activo!="activo"){
  include("../private/blog/error404.php");
  exit;
}else{

  $consulta = " SELECT id,padre,title,url FROM $tbn36 where activo='1' ";
  $resultado=mysqli_query($con,$consulta);
  $row_cnt = mysqli_num_rows($resultado);


if($row_cnt!=0){
  while( $fila = mysqli_fetch_assoc( $resultado )){ // sacamos los datos de la consulta y los metemos en un array
      $nuevo_array[] = $fila;
  }


  function countSon ($array, $father_id ) {
    $count=0;
    foreach ($array as $value) {
      if ( $value["padre"] == $father_id ) $count++;
    }
    return $count;
  }

  function menu( $array, $padre=0 ) {
    $menu_html = '';
    $cant = countSon($array, $padre);


      if( $cant > 0 ){
          $menu_html .= '<ul class="nav ">';
       }

        foreach($array as $elemento) {
          if($elemento['padre']==$padre) {

          $menu_html .= '<li class="nav-item ">';
          $menu_html .= '<a class="nav-link" href="'.$elemento['url'].'">' .$elemento['title'].'</a>';
          $menu_html .= menu($array,$elemento['id']);
          $menu_html .= '</li>';
          }
        }

      if( $cant > 0 ){
          $menu_html .= '</ul>';
      }

    return $menu_html;
  }

}
?>


<div class="navbar navbar-expand-md navbar-dark  bg-blue" id="navbar-header">
<nav class="navbar" >
<!--  <a class="navbar-brand" href="index.php">INICIO</a>-->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarCollapse">
    <?php
    if($row_cnt!=0){
      echo menu($nuevo_array,0);
      }
    ?>

  </div>
</nav>

</div>


<?php  }
?>

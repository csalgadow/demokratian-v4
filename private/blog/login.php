<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que carga el bloque con el formulario de acceso de los votantes para poder acceder a la parte privada de la aplicación
*/
if(!isset($carga)){
  $carga =false;
  exit;
}
if($carga!="OK"){
  exit;
}else{


?>

<div class="container demokratian-access">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
          <h2 class="demokratian-access"><?= _("Acceso al espacio de votaciones") ?></h2>
        </div>
    </div>
    <div class="row">
      <div  id="info-access"  class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xs-offset-3">
        <h4><?= _("Bienvenido al sistema de votaciones") ?> <?php echo "$nombre_web"; ?></h4>
        <?php if ($insti == true) { ?>
            <p><a data-toggle="modal" href="#myModalInsti"  class="btn btn-start-order"  data-target=".myModalInsti"><?= _("Registrarme o recuperar mi contraseña") ?></a> </p>
        <?php } else { ?>
            <p><a data-toggle="modal" href="#myModal"  class="btn btn-start-order" data-target=".myModal"><?= _("Registrarme o recuperar mi contraseña") ?></a> </p>

        <?php } ?>

      <p><?= _("Si es la primera vez que accedes deberás generar tu clave y usuario usando la dirección de correo electrónico con la que estes registrado en la base de datos.") ?></p>
      </div>


      <div id="login-access" class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xs-offset-3">
          <?php
          if ($cfg_autenticacion_solo_local == false) {
              ?>
              <span><button class="form-control">
                  <span class="ui-icon ui-icon-carat-1-s" id="flecha"></span>
                  </button><p><?= _("Acceso para usuarios locales") ?></p></span>
          <?php } ?>

          <div class="well" id="panel_autenticacion_local">

              <?php
              include ("../private/inc_web/ms_error.php");
              if (isset($_GET['error_login'])) {
                  $error = $_GET['error_login'];
                  if (isset($_GET['tx'])) {
                      $tx = "<br/>" . $_GET['tx'];
                  }
                  ?>
                  <div class="alert alert-danger">
                      <a class="close" data-dismiss="alert">x</a>

                    <p>  <?= _("Error") ?>: <?php echo $error_login_ms[$error]; ?> </p>
                    <ul>
                      <?php if ($insti == true) { ?>
                        <li><a data-toggle="modal" href="#myModalInsti"  class="white"  data-target=".myModalInsti"><?= _("Registrarme o recuperar mi contraseña") ?></a> </li>
                      <?php }else{
                      ?>
                      <li><a data-toggle="modal" href="#myModal"  class="white" data-target=".myModal"><?= _("Si no estas registrado puedes hacerlo") ?></a></li>
                      <li><a data-toggle="modal" href="#myModal"  class="white" data-target=".myModal"><?= _("Recuperar contraseña") ?></a></li>
                    <?php }
                    ?>
                    </ul>
                  </div>

              <?php }
              ?>
              <!-- comienza el formulario de acceso -->

              <form action="votaciones.php" method="post"  class="form" role="form" id="login">
                  <div class="form-group">
                      <label for="user" class="form-label"><?= _("Usuario") ?> : </label>

                          <input type="text" id="user"   name="user" class="form-control" placeholder="<?= _("Usuario") ?> " required autofocus/>

                  </div>
                  <div class="form-group">


                      <label for="user" class="form-label"><?= _("Password") ?> : </label>
                      <input type="password" id="pass"  name="pass" class="form-control" placeholder="<?= _("Password") ?> " required />

                  </div>

                  <?php
                  if ($reCaptcha == true) {
                    if ($tipo_reCaptcha == "reCAPTCHA_v2") {
                      ?>
                      <div class="form-group">
                              <div class="g-recaptcha" data-sitekey="<?php echo "$reCAPTCHA_site_key"; ?>"></div>
                      </div>
                      <script src='https://www.google.com/recaptcha/api.js'  ></script>

                      <?php
                    }else{  // el tipo reCAPTCHA_v3
                          ?>
                          <script src="https://www.google.com/recaptcha/api.js?render=<?php echo $reCAPTCHA_site_key; ?>"></script>
                              <script>
                              $('#login').submit(function(event) {
                                  event.preventDefault();
                                  grecaptcha.ready(function() {
                                      grecaptcha.execute('<?php echo $reCAPTCHA_site_key; ?>', {action: 'registro'}).then(function(token) {
                                          $('#login').prepend('<input type="hidden" name="token" value="' + token + '">');
                                          $('#login').prepend('<input type="hidden" name="action" value="registro">');
                                          $('#login').unbind('submit').submit();
                                      });;
                                  });
                            });
                            </script>
                        <?php
                        }
                    } else{  // si no tenemos activo reCAPTCHA, metemos un token de control
                        $server =$_SERVER['SERVER_NAME'];
                        $clave= $clave_encriptacion2.$server;
                        $token = password_hash($clave, PASSWORD_DEFAULT, [15]);
                       ?>
                        <input id="token" name="token" type="hidden" value="<?php echo "$token" ?>">
                        <?php
                      }
                    ?>


                  <div class="form-group">
                      <div class="text-center">

                          <button class="btn btn-start-order" type="submit"><?= _("Acceder") ?> </button>
                      </div>
                  </div>

              </form>
              <!-- Fin del formulario de acceso -->
          </div>
          <?php
          if ($cfg_autenticacion_solo_local == false) {
              ?>
              <div class="well">
                  <form action="votaciones.php" method="post"  class="form-horizontal" role="form" >
                      <input type=hidden name=federada value=1>
                      <h3 class="form-signin-heading"><?= _("Autenticación externa") ?></h3>
                      <div class="form-group">
                          <div class="col-sm-offset-4 col-sm-6">
                              <button class="btn btn-start-order" type="submit"><?= _("Acceder") ?> </button>
                          </div>
                      </div>
                  </form>
              </div>

              <?php
          }
          ?>

      </div>





    </div>
</div>











<!--
================================= ventana modal
-->

<div class="modal fade myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
          <h5 class="modal-title"><?= _("Complete para registrarse o recuperar su contraseña") ?></h5>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
      <div class="modal-body ">
        <!---texto --->

                <fieldset>

                    <form name="contrasenaForm" class="well" id="contrasenaForm" >
                        <div  id="divcontrasenaForm" >


                            <!-- <form name="sentMessage" class="well" id="contactForm"  novalidate>-->
                            <!--<legend>Contact me</legend>-->
                            <div class="control-group">
                                <div class="controls">
                                    <input type="text" class="form-control" placeholder="<?= _("Su nombre") ?>" id="name" name="name" required data-validation-required-message="<?= _("Por favor, ponga su nombre") ?>" />
                                    <p class="help-block"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="controls">
                                    <input type="email" class="form-control" placeholder="<?= _("Su correo electronico") ?>" id="email" name="email" required  data-validation-required-message="<?= _("Por favor, ponga su correo electronico") ?>" />
                                </div>
                            </div>


                            <?php if ($es_municipal == false) {
                                ?>
                                <br/>
                                <div class="control-group">
                                    <span class="help-block"><?= _("Seleccione la provincia donde esta censado") ?>:</span>
                                    <?php
// listar para meter en una lista del tipo enlace
                                    //$especial = "";
                                    //$activo = 0;
                                    $lista = "";
                                    $tbn8 = $extension . "provincia";
                                    //$options = "select DISTINCT ID,provincia from $tbn8 where especial ='$especial' order by id  ";
                                    $options = "select DISTINCT ID,provincia from $tbn8  order by id  ";
                                    $resulta = mysqli_query($con, $options) or die("error: " . mysqli_error($con));
                                    while ($listrows = mysqli_fetch_array($resulta)) {
                                        $name = $listrows['provincia'];
                                        $id_pro = $listrows['ID'];
                                        $lista .= "<option value=\"$id_pro\"> $name</option>";
                                    }
                                    ?>


                                    <div class="form-group">
                                        <select class="form-control custom-select"  name="provincia" id="provincia" >
                                            <!-- <option value=""> Escoje una provincia</option>-->
                                            <?php echo "$lista"; ?>
                                        </select>
                                    </div>



                                </div>
                            <?php } else { ?>
                                <input type="hidden" name="provincia" id="provincia" value="001"  />
                            <?php } ?>


                            <button type="submit" class="btn btn-primary btn-block"><?= _("Enviar") ?></button><br />
                        </div>
                    </form>
                    <div id="success"> </div> <!-- mensajes -->
                </fieldset>

              </div>

            </div>
          </div>
        </div>


<!--
===========================  fin ventana modal
-->


<?php if ($insti == true) { ?>
    <!--
================================= ventana modal para cuando se usa correo institucional y hay circunscripcion multiple, el registro inicial
    -->

    <div class="modal fade myModalInsti" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title"><?= _("Complete para registrarse") ?> </h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
          <div class="modal-body ">



                    <fieldset>

                        <form name="contrasenaFormInsti" class="well" id="contrasenaFormInsti" >
                            <div  id="divcontrasenaFormInsti" >

                                <!--<legend>Contact me</legend>-->
                                <div class="control-group">
                                    <div class="controls">
                                        <input type="text" class="form-control"
                                               placeholder="<?= _("Su nombre") ?>" id="nameInsti" name="nameInsti" required data-validation-required-message="<?= _("Por favor, ponga su nombrea") ?>" />
                                        <p class="help-block"></p>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="controls">
                                        <input type="email" class="form-control" placeholder="<?= _("Su correo electronico") ?>" id="emailInsti" name="emailInsti" required  data-validation-required-message="<?= _("Por favor, ponga su correo electronico") ?>" />
                                    </div>
                                </div>


                                <?php if ($es_municipal == false) {
                                    ?>
                                    <div class="control-group">
                                        <span class="help-block"><?= _("Seleccione la provincia donde esta censado") ?>:</span>
                                        <?php
// listar para meter en una lista del tipo enlace
                                        //$especial = "";
                                        //$activo = 0;
                                        $lista = "";
                                        $tbn8 = $extension . "provincia";
                                      //  $options = "select DISTINCT ID,provincia from $tbn8 where especial ='$especial' order by id  ";
                                        $options = "select DISTINCT ID,provincia from $tbn8 order by id  ";
                                        $resulta = mysqli_query($con, $options) or die("error: " . mysqli_error());
                                        while ($listrows = mysqli_fetch_array($resulta)) {
                                            $name = $listrows['provincia'];
                                            $id_pro = $listrows['ID'];
                                            $lista .= "<option value=\"$id_pro\"> $name</option>";
                                        }
                                        ?>


                                        <div class="form-group">
                                            <select class="form-control custom-select"  name="provinciaInsti" id="provinciaInsti" >

                                                <?php echo "$lista"; ?>
                                            </select>
                                        </div>
                                        <div class="control-group">
                                            <span class="help-block">
                                                <?= _("selecciona municipio") ?> </span>
                                            <select name="municipioInsti" id="municipioInsti" class="form-control custom-select" > </select>

                                        </div>

                                    </div>
                                    <p></p>
                                <?php } ?>



                                <button type="submit" class="btn btn-primary btn-block" id="btnEnviar" name="btnEnviar"><?= _("Enviar") ?></button><br />
                            </div>
                        </form>
                        <div id="successInsti"> </div> <!-- mensajes -->
                    </fieldset>

                </div>
            </div>
        </div>
    </div>
    <!--
    ===========================  fin ventana modal
    -->
<?php }?>

<?php if ($insti == true) { ?>
    <script src="assets/js/recupera_contrasena_insti.js" ></script>
    <?php if ($es_municipal == false) { ?>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#provinciaInsti').change(function () {

                    var id_provincia = $('#provinciaInsti').val();
                    $('#municipioInsti').load('basicos_php/genera_select.php?id_provincia=' + id_provincia);
                    $("#municipioInsti").html(data);
                });
            });
        </script>
    <?php } ?>
<?php } ?>

<script type="text/javascript">
    aut_local_plegado = <?php
    if ($cfg_autenticacion_solo_local == false) {
        echo "true";
    } else {
        echo "false";
    } ?>;
    autenticacion_solo_local = <?php
    if ($cfg_autenticacion_solo_local == false) {
        echo "false";
    } else {
        echo "true";
    }  ?>;
    if (aut_local_plegado) {
       $("#panel_autenticacion_local").slideUp();
    } else {
        $("#panel_autenticacion_local").slideDown();
    }
    if (!autenticacion_solo_local) {
        $("#flip").click(function () {
        $("#panel_autenticacion_local").slideToggle();
        // $("#flecha").toggleClass("ui-icon ui-icon-carat-1-s", "ui-icon-carat-1-n ui-icon");
      });
    }
</script>

            <script src="assets/js/recupera_contrasena.js" ></script>


<?php
  }
?>

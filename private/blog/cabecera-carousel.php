<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que carga el bloque con la cabecera de tipo carrusel
*/
if(!isset($bloque_activo)){
  include("../private/blog/error404.php");
  exit;
}
if($bloque_activo!="activo"){
  include("../private/blog/error404.php");
  exit;
}else{

$sql_cabe = "SELECT titulo,texto, imagen, id_categoria  FROM $tbn30 where activo =1 and id_categoria = 1 order by id ";
$result_cabe = mysqli_query($con, $sql_cabe)or die(mysqli_error($con));
$contar = mysqli_num_rows($result_cabe);
$contar=$contar-1;
?>


<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
<?php    for($i=0; $i <= $contar; $i++){ ?>
    <li data-target="#myCarousel" data-slide-to="<?php echo "$i"; ?>" <?php if ($i==0) { echo 'class="active"'; }?>></li>
  <?php } ?>
  </ol>

  <div class="carousel-inner">

    <?php
$j=0;
        //$row_cabe = mysqli_fetch_row($result_cabe);
          if ($row_cabe = mysqli_fetch_array($result_cabe)) {
            do {

              ?>

              <div class="carousel-item <?php if ($j==0) { echo 'active'; }?>">
               <img src="<?php echo "$upload_cat"; ?>/<?php echo "$row_cabe[2]"; ?>" alt="<?php echo "$row_cabe[0]"; ?>" class="d-block img-fluid">
                <div class="container">
                  <div class="carousel-caption text-left">
                    <h1><?php echo "$row_cabe[0]"; ?></h1>
                    <p><?php echo "$row_cabe[1]"; ?></p>
                  </div>
                </div>
              </div>

        <?php
        $j++;
       } while ($row_cabe = mysqli_fetch_array($result_cabe));

      }else{  ?>

        <div class="carousel-item active">
         <img src="temas/<?php echo "$tema_web"; ?>/images/demonstration1.jpg" alt="Imagen 1" class="d-block img-fluid">
          <div class="container">
            <div class="carousel-caption text-left">
              <h1>Example headline.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
            </div>
          </div>
        </div>

        <div class="carousel-item">
          <img src="temas/<?php echo "$tema_web"; ?>/images/drops.jpg" alt="Imagen 2" class="d-block img-fluid">
          <div class="container">
            <div class="carousel-caption text-left">
              <h1>Another example headline.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
            </div>
          </div>
        </div>

        <div class="carousel-item">
          <img src="temas/<?php echo "$tema_web"; ?>/images/road.jpg" alt="Imagen 3" class="d-block img-fluid">
          <div class="container">
            <div class="carousel-caption text-left">
              <h1>One more for good measure.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
            </div>
          </div>
        </div>

<?php      }


     ?>

  </div>
  <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<?php } ?>

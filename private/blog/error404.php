<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que carga el error 404 pagina no encontrada
*/
?>
<div class="container">

      <h2><?= _("Error") ?> 404</h2>
          <h1><?= _("¡Página no localizada!") ?></h1>
          <br/>
          <br/>
          <div class="row">
  <div class="col-md-2" >
  </div>

  <div class="col-md-4" >

  <img src="temas/<?php echo "$tema_web"; ?>/images/404.png" class="img-fluid" alt="pues no está">
  </div>

  <div class="col-md-4" >

    <p><?= _("No se ha localizado la página solicitada en este servidor") ?>.</p>
    <p><?= _("Si usted ha introducido la URL manualmente, por favor revise su ortografía e inténtelo de nuevo") ?>.</p>
    <p><?= _("Si usted cree que esto es un error del servidor, por favor comuníqueselo al administrador del portal") ?>.</p>
    <h4><?= _("Ya lo sentimos, pero seguro que en nuestra web encuentra otras muchas más cuestiones de interés") ?>.</h4>
  </div>

  <div class="col-md-2" >
  </div>

</div>
<br/>
<br/>  <br/>
  <br/>
</div>

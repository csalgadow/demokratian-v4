<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que envía el correo del formulario para recuperar la contraseña mediante la petición ajax del archivo recupera_contrasena_insti.js.
* @todo ver si este archivo seria mejor que estuviera en basiscos_php
*/
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
if(!isset($carga)){
  $carga =false;
  exit;
}
if($carga!="OK"){
  exit;
}else{
require_once("../private/config/config.inc.php");
require_once("../private/inc_web/conexion.php");
include('../private/config/config-correo.inc.php');
require_once("../private/basicos_php/lang.php");

//require_once('../private/modulos/PHPMailer/PHPMailerAutoload.php');

require_once ('../private/modulos/PHPMailer/src/PHPMailer.php');
require_once ('../private/modulos/PHPMailer/src/SMTP.php');
require_once ('../private/modulos/PHPMailer/src/Exception.php');
//include("../private/modulos/PHPMailer/class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded
include_once ('../private/basicos_php/basico.php');



if (empty($_POST['nameInsti']) ||
        empty($_POST['emailInsti']) ||
        !filter_var($_POST['emailInsti'], FILTER_VALIDATE_EMAIL)) {
    echo _("No se han enviado argmentos") . " 1!";
    return false;
}
if ($es_municipal == false) {
    if (empty($_POST['provinciaInsti']) || empty($_POST['municipioInsti'])) {
        echo _("No se han enviado argmentos") . " 2!";
        return false;
    }
}


$name = fn_filtro_nodb($_POST['nameInsti']);
$email = fn_filtro_nodb($_POST['emailInsti']);

if ($es_municipal == false) {
    $provincia = fn_filtro_numerico($con, $_POST['provinciaInsti']);
    $municipio = fn_filtro_numerico($con, $_POST['municipioInsti']);
}
//$name = utf8_decode($name);
//$asunto = utf8_decode($asunto);
$ya_clave = "";  //definimos esta variable vacia

$domain = strstr($email, '@');
if ($domain != $term_mail) { ///comprobamos que el correo introducido corresponde al dominio Instucional.
    echo "ERROR##<div class=\"alert alert-warning\">
    <a href=\"#\" class=\"close\" data-dismiss=\"alert\">x</a>
    <strong>¡¡¡ERROR!!!</strong><br/> " . _("La dirección de correo") . " \" " . $email . " \" " . _("no corresponde a una dirección válida para el registro en este sitio") . "<br/>"
    . _("Use su dirección de") . "   " . $term_empre . "</div>";
} else {  // si el correo es correcto
    $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
    $cad = "";
    for ($i = 0; $i < 12; $i++) {
        $cad .= substr($str, rand(0, 62), 1);
    }

    $conta = "SELECT id,pass  FROM $tbn9 WHERE correo_usuario like \"$email\" ";

    $mens = _("Error al buscar un votante");
    $result_cont = db_query($con, $conta, $mens);
    $quants = mysqli_num_rows($result_cont);

    if ($quants != 0) { // si  esta en la base de datos sacamos los datos
        $row = mysqli_fetch_array($result_cont);
        $id_votante = $row[0];
        if ($row[1] != "") {
            $ya_clave = "ya_tiene_clave";
        }

        $sSQL = "UPDATE $tbn9 SET codigo_rec=\"$cad\"  WHERE ID='$id_votante'";  //preparamos la instruccion sql para modificar la bbdd
        $mens = "<br/>" . _("¡¡ATENCION!!")." ". _("hay un error en el registro o recuperación de claves");
        $resulta = db_query($con, $sSQL, $mens);   ///ejecutamos la instrucion sql que corresponda
    } else {

        if ($es_municipal == false) {
            $optiones = "select  id_ccaa from $tbn8 where ID ='" . $provincia . "'";
            $resultas = mysqli_query($con, $optiones) or die("error: " . mysqli_error($con));

            while ($listrowes = mysqli_fetch_array($resultas)) {
                $ccaa = $listrowes['id_ccaa'];
            }
        } else {
            $provincia = 001;
            $municipio = 1;
            $ccaa = 016;
        }

        $tipo_votante = 2; //los incluimos como simpatizantes verificados
        $sSQL = "insert into $tbn9 (id_provincia,correo_usuario, tipo_votante,id_ccaa,id_municipio,nombre_usuario,codigo_rec ) values (\"$provincia\", \"$email\",  \"$tipo_votante\", \"$ccaa\", \"$municipio\" ,\"$name\",\"$cad\")"; //preparamos la instruccion sql para incluir en la bbdd
        $mens = "<br/>" . _("¡¡¡ATENCION!!!! hay un error en el registro o recuperación de claves");
        $resulta = db_query($con, $sSQL, $mens);   ///ejecutamos la instrucion sql que corresponda
        $id_votante = mysqli_insert_id($con);
    }

///////////////////enviamos un correo
    function encrypt($string, $key) {
        $result = '';
        for ($i = 0; $i < strlen($string); $i++) {
            $char = substr($string, $i, 1);
            $keychar = substr($key, ($i % strlen($key)) - 1, 1);
            $char = chr(ord($char) + ord($keychar));
            $result .= $char;
        }
        return str_replace(array('+', '/', '='), array('-', '_', ''), base64_encode($result));
    }

    $cadena_para_encriptar = $provincia . "_" . "$id_votante" . "_" . "$cad";
    $cadena_encriptada = encrypt($cadena_para_encriptar, $clave_encriptacion);

    $cadena_para_encriptar2 = "nwt-" . $id_votante;
    $cadena_encriptada2 = encrypt($cadena_para_encriptar2, $clave_encriptacion2);

    $mensaje = _("Hola") . " " . $name . " <br/>";

    $mensaje .= _("Este mensaje ha sido enviado por el sistema de votaciones") . " " . $nombre_web . " " . _("el") . " " . date('d/m/Y', time());
    if ($ya_clave == "ya_tiene_clave") {
        $mensaje .= "<br/>" .
                _("Te enviamos un enlace para recuperar tu contraseña") . "<br/>";
    } else {
        $mensaje .= "<br/>" .
                _("Te enviamos un enlace para finalizar tu registro") . " <br/>" .
                _("Si no ha solicitados la contraseña, alguien que conoce su dirección de correo lo ha hecho, pero puedes obviar este correo ya que tu contraseña no ha sido modificada") . "<br/>";
    }
    $mensaje .= _("Para completar la operación es imprescindible acceder a la siguiente dirección web") . " <br/>"
     . $url_vot . '/index.php?c=new_pass/regedi='.$cadena_encriptada2 .'&idpr=' . $cadena_encriptada. ' ' ;



  //  $mensaje = str_replace("\n", "<br>", $mensaje);
  //  $mensaje = str_replace("\t", "    ", $mensaje);

    if ($correo_smtp == true) {  //comienzo envio smtp
        $mail = new PHPMailer();
        $mail->CharSet = 'UTF-8';
        $mail->ContentType = 'text/plain';
        //$mail->IsHTML(false);
        if ($mail_IsHTML == true) {
            $mail->IsHTML(true);
        } else {
            $mail->IsHTML(false);
        }

        if ($mail_sendmail == true) {
            $mail->IsSendMail();
        } else {
            $mail->IsSMTP();
        }

        //$mail->SMTPAuth = true;
        if ($mail_SMTPAuth == true) {
            $mail->SMTPAuth = true;
        } else {
            $mail->SMTPAuth = false;
        }

        if ($mail_SMTPSecure == false) {
            $mail->SMTPSecure = false;
            $mail->SMTPAutoTLS = false;
        } else if ($mail_SMTPSecure == "SSL") {
            $mail->SMTPSecure = 'ssl';
        } else {
            $mail->SMTPSecure = 'tls';
        }
        if ($mail_SMTPOptions == true) {  //algunos servidores con certificados incorrectos no envian los correos por SMTP por lo que quitamos la validadcion de los certificados, NO SE RECOMIENDA EN ABSOLUTO usar esta opción
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
        }


        $mail->Port = $puerto_mail; // Puerto a utilizar

        $mail->Host = $host_smtp;
        $mail->SetFrom($email_env, $nombre_sistema);
        $mail->Subject = $asunto;
        $mail->MsgHTML($mensaje);
        $mail->AddAddress($email, $name);
        $mail->Username = $user_mail;
        $mail->Password = $pass_mail;


        if (!$mail->Send()) {
            echo "ERROR##<div class=\"alert alert-warning\"> " . _("Error en el envio") . " : " . $mail->ErrorInfo . "</div>";
        } else {
            // echo "Enviado correctamente!";

            echo "
    OK##<div class=\"alert alert-success\">
    <strong>" . _("Se ha enviado su código de activación a su dirección de correo para validar su cuenta y así poder participar en las votaciones") . ".</strong><br/> " .
            _("Si no recibe el correo compruebe su carpeta de spam por si esta allí") . ". <br/>" .
            _("Ahora tiene que mirar su correo y copiar ese código de activación para introducirlo en el sistema") . ".
	</div>";
        }
    }


    if ($correo_smtp == false) { ///correo mediante mail de php
        //para el envío en formato HTML
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

//dirección del remitente
        $headers .= "From: $nombre_sistema <$email_env>\r\n";


//ruta del mensaje desde origen a destino
        $headers .= "Return-path: $email_env\r\n";

//$asunto="$asunto_mens_error";


        mail($email, $asunto, $mensaje, $headers);

        echo _("enviado correo por mail");
    }
}


return true;
?>
<?php } ?>

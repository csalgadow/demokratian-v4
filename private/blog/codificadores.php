<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que carga el formulario de acceso para que codificadores puedan acceder a la zona para incluir votos depositados en urna
*/
if(!isset($carga)){
  $carga =false;
  exit;
}
if($carga!="OK"){
  exit;
}else{
//// el borrado de las sesiones de los codificadores esta en la pagina index con un get que detecta que se carga este bloque
?>
    <div class="text-center">
        <h2><?= _("Sistema de control de codificadores") ?> <?php echo "$nombre_web"; ?></h2>
        <p><br>
        </p>
    </div>

    <div class="row">
      <div class="col-md-2"></div>
    <div class="col-md-4">
<p><?= _("Zona restringida") ?></p>
<p><?= _("A esta zona solo pueden acceder los usuarios con nivel de codificadores") ?></p>
<a data-toggle="modal" href="#myModal"  class="btn btn-start-order" data-target=".myModal"><?= _("He olvidado mi  contraseña o aun no tengo una") ?></a>
    </div>

    <div class="col-md-4">


        <form action="codificadores.php" method="post" class="form-signin" role="form" >
            <h4 class="form-signin-heading text-info"><?= _("Tienes que identificarte para acceder") ?></h4>
            <?php
            include ("../private/codificadores/ms_error.php");
            if (isset($_GET['error_login'])) {
                $error = $_GET['error_login'];
                ?>
                <div class="alert alert-danger">
                    <a class="close" data-dismiss="alert">x</a>

                  <p>  Error: <?php echo $error_login_ms[$error]; ?> </p>
                  <ul>
                    <li><a data-toggle="modal" href="#myModal"  class="white" data-target=".myModal"><?= _("Si no estas registrado puedes hacerlo") ?></a> </li>
                    <li><a data-toggle="modal" href="#myModal"  class="white" data-target=".myModal"><?= _("Recuperar contraseña") ?></a></li>
                  </ul>
                </div>

            <?php }
            ?>
            <div class="form-group">
              <label for="user" class="form-label"><?= _("Usuario") ?> : </label>
                    <input type="text" id="user"   name="user" class="form-control" placeholder="<?= _("Usuario") ?>" required autofocus/>

            </div>


            <div class="form-group">
              <label for="user" class="form-label"><?= _("Password") ?> : </label>
                    <input type="password" id="pass"  name="pass" class="form-control" placeholder="Password" required />
                  </div>
            <div class="form-group">
                    <label for="user" class="form-label"><?= _("numero de votación") ?> : </label>
                    <input type="text" class="form-control" placeholder="<?= _("numero de votación") ?>" name="votacion" id="votacion" required  data-validation-required-message="<?= _("numero de votación") ?>" />

            </div>
            <?php
            if ($reCaptcha == true) {
              if ($tipo_reCaptcha == "reCAPTCHA_v2") {
                ?>
                <div class="form-group">
                        <div class="g-recaptcha" data-sitekey="<?php echo "$reCAPTCHA_site_key"; ?>"></div>
                </div>
                <script src='https://www.google.com/recaptcha/api.js'  ></script>

                <?php
              }else{  // el tipo reCAPTCHA_v3
                    ?>
                    <script src="https://www.google.com/recaptcha/api.js?render=<?php echo $reCAPTCHA_site_key; ?>"></script>
                        <script>
                        $('#login').submit(function(event) {
                            event.preventDefault();
                            grecaptcha.ready(function() {
                                grecaptcha.execute('<?php echo $reCAPTCHA_site_key; ?>', {action: 'registro'}).then(function(token) {
                                    $('#login').prepend('<input type="hidden" name="token" value="' + token + '">');
                                    $('#login').prepend('<input type="hidden" name="action" value="registro">');
                                    $('#login').unbind('submit').submit();
                                });;
                            });
                      });
                      </script>
                  <?php
                  }
              } else{  // si no tenemos activo reCAPTCHA, metemos un token de control
                  $server =$_SERVER['SERVER_NAME'];
                  $clave= $clave_encriptacion2.$server;
                  $token = password_hash($clave, PASSWORD_DEFAULT, [15]);
                 ?>
                  <input id="token" name="token" type="hidden" value="<?php echo "$token" ?>">
                  <?php
                }
              ?>

            <div class="form-group">
            <div class="text-center">
              <button class="btn btn-start-order" type="submit"><?= _("Acceder") ?> </button>
            </div>
          </div>
            <p>&nbsp;</p>
        </form>
</div>
<div class="col-md-2"></div>
</div>


<div class="modal fade myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
          <h5 class="modal-title"><?= _("Complete para registrarse o recuperar su contraseña") ?></h5>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
      <div class="modal-body ">
        <!---texto --->
                    <fieldset>

                        <form name="sentMessage" class="well" id="contrasenaForm" >
                          <div  id="divcontrasenaForm" >
                            <!-- <form name="sentMessage" class="well" id="contactForm"  novalidate>-->
                            <!--<legend>Contact me</legend>-->
                            <div class="control-group">
                                <div class="controls">
                                    <input type="text" class="form-control"  placeholder="<?= _("Su nombre") ?>" id="name" name="name" required data-validation-required-message="<?= _("Por favor, ponga su nombre") ?>" />
                                    <p class="help-block"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="controls">
                                    <input type="email" class="form-control" placeholder="<?= _("Su correo electronico") ?>" id="email" name="email" required  data-validation-required-message="<?= _("Por favor, ponga su correo electronico") ?>" />
                                </div>
                            </div>



                            <br/>
                            <div class="control-group">
                                <span class="text-info"><?= _("Indique el número de votación") ?></span>
                                <div class="form-group">
                                    <input type="text" required class="form-control" id="n_votacion" name="n_votacion" placeholder="<?= _("numero de votación") ?>" data-validation-required-message="<?= _("Por favor, ponga el numero") ?>" />
                                </div>
                              </div>



                              <br/>
                            <button type="submit" class="btn btn-primary btn-block"><?= _("Enviar") ?></button><br />
                          </div>
                        </form>
                        <div id="success"> </div> <!-- mensajes -->
                    </fieldset>


                  </div>

                </div>
              </div>
            </div>
    <!--
    ===========================  fin ventana modal
    -->

      <script src="assets/js/codificadores/recupera_contrasena.js" ></script>
<?php } ?>

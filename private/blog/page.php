<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que carga el contenido de las páginas del blog
*/
function fn_filtro_numerico_ext($conexion, $cadena) {

    if (is_numeric($cadena)) {
        $cadena = $cadena;
    } else {
        $cadena = "";
        /*echo '<div class="alert alert-warning">
                <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                  <i class="fa fa-times simple-remove" aria-hidden="true"></i>
                </button>
                <span>
                  <b> '._("ERROR").' - </b> '._("No hay datos de esta votación, quizas haya terminado ya o no existe").'</span>
              </div>';
      //  Header("Location: ../index.php?error_login=9");
      */
      include("../private/blog/error404.php");
        exit;
    }
    $cadena = strip_tags($cadena);
    return mysqli_real_escape_string($conexion, $cadena);
}




if (isset($_GET['p']) != "") {
$id = fn_filtro_numerico_ext($con, base64_decode($_GET['p'])); //variable que nos llega con la votacion

    $result = mysqli_query($con, "SELECT titulo,	texto,	imagen,	fecha	 FROM $tbn33 where id=$id and activo=1");
    $row = mysqli_fetch_row($result);

    $row_titulo = $row[0];
    $row_texto = $row[1];
    $row_imagen = $row[2];
    $row_fecha = $row[3];


?>

<article id="page">
  <div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
      <div class="row meta"><time datetime="<?php echo date("Y-m-d", strtotime($row_fecha));?>"><?php echo date("d-m-Y", strtotime($row_fecha));?></time></div>
      <h2 class="demokratian-blocktext"><?php echo $row_titulo; ?></h2>


      <img src="<?php echo $upload_cat; ?>/<?php echo $row_imagen; ?>" alt="" class="img-fluid float-left mt-0 ml-0" width="500">
      <?php echo $row_texto; ?>

    </div>
  </div>

</article>
<?php
} else {
include("../private/blog/error404.php");
}


?>

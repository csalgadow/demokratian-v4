<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo con el bloque de texto de la primera página de fondo azul
*/
if(!isset($bloque_activo)){
  include("../private/blog/error404.php");
  exit;
}
if($bloque_activo!="activo"){
  include("../private/blog/error404.php");
  exit;
}else{

$idcont=1;   //Sección bloktext_blue
$limit_bloktext_blue=1; /// limite de textos mostrados

$sql_bloktext_blue = "SELECT id,	titulo,	texto,	fecha,	imagen,	id_categoria,	zona_pagina,	activo	 FROM $tbn33 where  id_categoria = '$idcont' and activo= 1 ORDER BY id DESC LIMIT $limit_bloktext_blue";
$result_bloktext_blue = mysqli_query($con, $sql_bloktext_blue)or die(mysqli_error($con));

if ($row_bloktext_blue = mysqli_fetch_array($result_bloktext_blue)) {
?>

<!-- START THE BLOCKTEXT -->
  <section class="blocktext bg-blue">
    <?php
      mysqli_field_seek($result_bloktext_blue, 0);
        do {
      ?>
          <div class="container">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
              <h2 class="demokratian-blocktext"><?php echo $row_bloktext_blue[1]; ?></h2>
              <p> <?php echo $row_bloktext_blue[2]; ?> </p>
            </div>
          </div>
          <?php
        } while ($row_bloktext_blue = mysqli_fetch_array($result_bloktext_blue));
        ?>
  </section>

<?php }
}
?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo con un modelo de presentación de candidatos u opciones que se ven en la zona pública
*/
?>
<div class="row bootstrap snippets" id="fondo">
    <?php
    $numcolumnas = $aux; //demomento el dato aux solo lleva el dato del numero de columnas, pero en el futuro sera un array de datos,
    $n_letras = 50;
    // $numcolumnas = 4;
    if ($numcolumnas == 2) {
        $class_columnas = "col-xs-12 col-sm-6 col-md-6";
    } else if ($numcolumnas == 3) {
        $class_columnas = "col-xs-12 col-sm-4 col-md-4";
    } else if ($numcolumnas == 4) {
        $class_columnas = "col-xs-12 col-sm-4 col-md-3";
    } else if ($numcolumnas == 6) {
        $class_columnas = "col-xs-12 col-sm-4 col-md-2"; //
    } else if ($numcolumnas == 12) {
        $class_columnas = "col-xs-6 col-sm-3 col-md-1";
    }


    $data = "shrink"; //gray, vertpan, tilt,  shrink
    if ($total_resultados > 0) {
        while ($row = mysqli_fetch_array($result)) {

                ?>
                <div class="<?php echo $class_columnas; ?> <?php echo $data; ?> ">
                    <div class="caja_ficha">
                        <h4 class="category centrado"><?php echo $row['nombre_usuario']; ?></h4>
                        <p  class="centrado"><a data-toggle="modal"  href="../votacion/perfil.php?idgr=<?php echo $row[0]; ?>" data-target="#ayuda_contacta" title="<?php echo $row['nombre_usuario']; ?>"  > <?php if ($row['imagen_pequena'] == "") { ?><img src="../temas/<?php echo "$tema_web"; ?>/images/user.png" width="110" height="110" class="borde" /><?php } else { ?><img src="<?php echo $upload_cat; ?>/<?php echo $row['imagen_pequena']; ?>" alt="<?php echo $row['nombre_usuario']; ?> " width="110" height="110"  class="borde"  /> <?php } ?></a></p>
                        <p class="description">
                            <?php
                            $txt = strip_tags($row['texto']);
                            $txt = mb_substr($txt, 0, $n_letras, 'UTF-8');
                            echo $txt;
                            ?>
                        </p>
                        <h6 class="title centrado"><a href="javascript:void(0);" data-href="aux_blog.php?a=perfil3/ballot/idgr=<?php echo $row[0]; ?>" class="opennormalModal " title="<?php echo "$row[3]"; ?>" ><?= _("más información") ?></a></h6>
                    </div>
                </div>



                <?php
        }

    }
    ?>
</div>

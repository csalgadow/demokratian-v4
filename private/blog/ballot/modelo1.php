<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo con un modelo de presentación de candidatos u opciones que se ven en la zona publica
*/
 ?>
<div class="row" id="fondo">
    <?php
    $numcolumnas = $aux; //demomento el dato aux solo lleva el dato del numero de columnas, pero en el futuro sera un array de datos,
    // $numcolumnas = 4;
    $n_letras = 350;
    if ($numcolumnas == 2) {
        $class_columnas = "col-md-6";
    } elseif ($numcolumnas == 3) {
        $class_columnas = "col-md-4";
    } elseif ($numcolumnas == 4) {
        $class_columnas = "col-md-3";
    } elseif ($numcolumnas == 6) {
        $class_columnas = "col-md-2";
    } elseif ($numcolumnas == 12) {
        $class_columnas = "col-md-1";
    }

    if ($total_resultados > 0) {
        while ($row = mysqli_fetch_array($result)) {
            ?>
          <div class="<?php echo $class_columnas; ?>">
            <div class="card profile-card">
              <div class="profile-header">&nbsp;</div>
                <div class="profile-body">
                  <div class="image-area">
                  <?php  if ($row['imagen_pequena'] != "") {
                          $thumb_photo_exists = $upload_cat . "/" . $row['imagen_pequena']; ?>
                 <img src="<?php echo $thumb_photo_exists; ?>" class="img-circle img-profile" width="170" height="170"/>
                <?php
              } else { ?>
                          <div class="card-image-area"></div>
                  <?php
                      } ?>
                </div>
                <div class="content-area">
                <h3 class="center_text"><?php echo $row['nombre_usuario']; ?></h3>
                    <p class="description">
                    <?php
                    if($row['texto']!=""){
                      $txt = strip_tags($row['texto']);
                      $txt = mb_substr($txt, 0, $n_letras, 'UTF-8');
                      echo $txt;
                      echo "(...)";
                    }?></p>
                  <a href="javascript:void(0);" data-href="aux_blog.php?a=perfil1/ballot/idgr=<?php echo $row[0]; ?>" class="opennormalModal btn btn-success btn-block" title="<?php echo "$row[3]"; ?>" ><?= _("VER FICHA COMPLETA") ?></a>
                  </div>
              </div>
            </div>
          </div>
        <?php
        }
    }
  ?>

</div>

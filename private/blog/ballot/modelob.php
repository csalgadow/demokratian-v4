<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo con un modelo de presentación de candidatos u opciones que se ven en la zona pública
*/
?>
<div class="row bootstrap  demo-2 no-js" id="fondo">
    <?php
    $numcolumnas = $aux; //demomento el dato aux solo lleva el dato del numero de columnas, pero en el futuro sera un array de datos,
    $n_letras = 80;
    // $numcolumnas = 4;
    if ($numcolumnas == 2) {
        $class_columnas = "col-xs-12 col-sm-6 col-md-6";
        $tam = 450; //
    } else if ($numcolumnas == 3) {
        $class_columnas = "col-xs-12 col-sm-4 col-md-4";
        $tam = 350; //
    } else if ($numcolumnas == 4) {
        $tam = 250; //
        $class_columnas = "col-xs-12 col-sm-4 col-md-3";
    } else if ($numcolumnas == 6) {
        $class_columnas = "col-xs-12 col-sm-4 col-md-2";
        $tam = 150; //
    } else if ($numcolumnas == 12) {
        $class_columnas = "col-xs-6 col-sm-3 col-md-1";
        $tam = 50; //
    }


    if ($total_resultados > 0) {
        $i = 1;
        while ($row = mysqli_fetch_array($result)) {
                ?>

                <div class="<?php echo $class_columnas; ?>  caja_ficha">
                    <!-- Top Navigation -->



                    <section id="grid" class="grid clearfix">
                        <a href="#" data-path-hover="m 0,0 0,47.7775 c 24.580441,3.12569 55.897012,-8.199417 90,-8.199417 34.10299,0 65.41956,11.325107 90,8.199417 L 180,0 z">
                            <figure>
                                <?php if ($row['imagen_pequena'] == "") { ?><img src="../temas/<?php echo "$tema_web"; ?>/images/user.png" width="<?php echo "$tam"; ?>" height="<?php echo "$tam"; ?>"  /><?php } else { ?><img src="<?php echo $upload_cat; ?>/<?php echo $row['imagen_pequena']; ?>" alt="<?php echo $row['nombre_usuario']; ?> " width="<?php echo "$tam"; ?>" height="<?php echo "$tam"; ?>"  class="borde"  /> <?php } ?>
                                <svg viewBox="0 0 180 320" preserveAspectRatio="none"><path d="m 0,0 0,171.14385 c 24.580441,15.47138 55.897012,24.75772 90,24.75772 34.10299,0 65.41956,-9.28634 90,-24.75772 L 180,0 0,0 z"/></svg>
                                <figcaption>
                                    <h2><?php echo $row['nombre_usuario']; ?> </h2>
                                    <p>
                                        <?php
                                        $txt = strip_tags($row['texto']);
                                        $txt = mb_substr($txt, 0, $n_letras, 'UTF-8');
                                        echo $txt;
                                        ?></p>
                                    <button type="button" data-href="aux_blog.php?a=perfilb/ballot/idgr=<?php echo $row[0]; ?>" class="opennormalModal " title="<?php echo "$row[3]"; ?>" ><?= _("más información") ?></button>
                                </figcaption>
                            </figure>
                        </a>

                    </section>

                </div><!-- /container -->

                <?php
        }
    }
    ?>

</div>
<script>
    (function () {

        function init() {
            var speed = 330,
                    easing = mina.backout;

            [].slice.call(document.querySelectorAll('#grid > a')).forEach(function (el) {
                var s = Snap(el.querySelector('svg')), path = s.select('path'),
                        pathConfig = {
                            from: path.attr('d'),
                            to: el.getAttribute('data-path-hover')
                        };

                el.addEventListener('mouseenter', function () {
                    path.animate({'path': pathConfig.to}, speed, easing);
                });

                el.addEventListener('mouseleave', function () {
                    path.animate({'path': pathConfig.from}, speed, easing);
                });
            });
        }

        init();

    })();
</script>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que carga el bloque con la cabecera de tipo fija
*/
if(!isset($bloque_activo)){
  include("../private/blog/error404.php");
  exit;
}
if($bloque_activo!="activo"){
  include("../private/blog/error404.php");
  exit;
}else{

     $sql_cabe = "SELECT titulo,texto, imagen, id_categoria  FROM $tbn30 where activo =1 and id_categoria = 0 order by id ";
    $result_cabe = mysqli_query($con, $sql_cabe)or die(mysqli_error($con));
     //$row_cabe = mysqli_fetch_row($result_cabe);
       if ($row_cabe = mysqli_fetch_array($result_cabe)) {

     $row_titulo = $row_cabe['titulo'];
     $row_texto = $row_cabe['texto'];
     $row_imagen = $upload_cat."/".$row_cabe['imagen'];
   }else{
     $row_titulo = "Demokratian.";
     $row_texto = "";
     $row_imagen = "temas/".$tema_web."/images/cabecera_votaciones.png";
   }

 ?>

    <!-- cabecera
    ================================================== -->
    <div class="page-header">
      <div class="text-center posicion">
        <img src="<?php echo "$row_imagen"; ?>" class="d-block img-fluid" alt="<?php echo "$nombre_web"; ?>">
        <div class="posicion-cabecera text-left">
          <h1><?php echo "$row_titulo"; ?></h1>
          <p><?php echo "$row_texto"; ?></p>
        </div>
      </div>
    </div>

    <!-- END cabecera
    ================================================== -->

<?php } ?>

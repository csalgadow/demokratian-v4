<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo con el bloque que presenta  3 contenidos en horizontal
*/
if(!isset($bloque_activo)){
  include("../private/blog/error404.php");
  exit;
}
if($bloque_activo!="activo"){
  include("../private/blog/error404.php");
  exit;
}else{
$puntos = '...';

$idcont=3;   //Sección marqueting
$limit_marqueting=3; /// limite de textos mostrados
$limit_palabras_marqueting=30;
$sql_marqueting = "SELECT id,	titulo,	texto,	fecha,	imagen,	id_categoria,	zona_pagina,	activo	 FROM $tbn33 where  id_categoria = '$idcont' and activo= 1 ORDER BY id DESC LIMIT $limit_marqueting";
$result_marqueting = mysqli_query($con, $sql_marqueting)or die(mysqli_error($con));

if ($row_marqueting = mysqli_fetch_array($result_marqueting)) {

?>
                  <!-- Marketing messaging and featurettes
                  ================================================== -->
                  <!-- Wrap the rest of the page in another container to center all the content. -->
              <section class="marketing bg-blue">
                  <div class="container">
                    <div class="row">
                      <?php
                        mysqli_field_seek($result_marqueting, 0);
                        do {
                          ?>
                    <!-- Three columns of text below the carousel -->

                      <div class="col-lg-4">
                        <?php if($row_marqueting[4]!=""){ ?>
                          <img src="<?php echo $upload_cat; ?>/<?php echo $row_marqueting[4]; ?>" class="bd-placeholder-img rounded-circle" width="140" height="140" focusable="false" alt="<?php echo $row_marqueting[1]; ?>" >
                        <?php } ?>
                        <h2><?php echo $row_marqueting[1]; ?></h2>
                        <p><?php
                            $array_str = explode(' ',$row_marqueting[2]);//transformo el string a  un array
                              //$limit_palabras_noticia=10;//coloco el num de palabras que necesito del string
                              $string_final = implode(' ', array_slice($array_str,0,$limit_palabras_marqueting));//aqui hago la operacion para obtener solo las palabras que necesito del array string y lo convierto a un string neuvamente
                              echo $string_final; // muestro el string final con las palabras que solo necesitaba?></p>
                        <p><a class="btn btn-secondary" href="index.php?c=page&p=<?php echo base64_encode($row_marqueting[0]); ?>&title=<?php echo urls_amigables($row_marqueting[1]); ?>" role="button"><?= _("Leer más") ?> &raquo;</a></p>
                      </div><!-- /.col-lg-4 -->

                      <?php }while ($row_marqueting = mysqli_fetch_array($result_marqueting));
                       ?>
                    </div><!-- /.row -->
                </section>
<?php }
    } ?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo con el formulario para generar una nueva contraseña y si no tiene usuario, también el usuario, para los interventores
*/
if (!isset($carga)) {
    $carga =false;
    exit;
}
if ($carga!="OK") {
    exit;
} else {
    ?>
<div class="container">
<?php

  if (!empty($variables['regedi'])) { ?>

        <div class="center-block">
            <h3><?= _("Pagina de validación de interventores del sistema de votaciones") ?>  <?php echo "$nombre_web"; ?></h3>
        </div>

                        <?php
                            function decrypt($string, $key)
                            {
                                $result = '';
                                $string = str_replace(array('-', '_'), array('+', '/'), $string);
                                $string = base64_decode($string);
                                for ($i = 0; $i < strlen($string); $i++) {
                                    $char = substr($string, $i, 1);
                                    $keychar = substr($key, ($i % strlen($key)) - 1, 1);
                                    $char = chr(ord($char) - ord($keychar));
                                    $result .= $char;
                                }
                                return $result;
                            }

                            $clave_encriptada = $variables['regedi'];

                            //$clave_encriptada = str_replace(array('-', '_'), array('+', '/'), $clave_encriptada);
                            $cadena_desencriptada = decrypt($clave_encriptada, $clave_encriptacion2);
                            $findme   = 'nwt-';
                            $pos = strpos($cadena_desencriptada, $findme);
                            if ($pos !== false) {
                                //    echo "La cadena '$findme' no fue encontrada en la cadena '$cadena_desencriptada'";

                                $array_cadena = explode('-', $cadena_desencriptada);
                                $clavecilla = $array_cadena[0];
                                $id_usuario = fn_filtro($con, $array_cadena[1]); ?>
                  <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-6">

                        <div class="card">
                          <div class="card-body">
                            <fieldset>
                              <legend><?= _("Introduzca los datos")?> </legend>
                              <div id="caja_contrasena">
                                <form method="post" name="sentMessage"  class="form-signin" id="contactForm"  role="form" >

                        <?php if ($clavecilla == "nwt") { ?>
                            <input name="idrec" type="hidden" id="idrec" value="<?php echo $variables['regedi']; ?>">
                            <?php
                            $usuario_consulta = mysqli_query($con, "SELECT ID,usuario,correo FROM $tbn11 WHERE ID='$id_usuario' ") or die("No se pudo realizar la consulta a la Base de datos");

                            while ($listrows = mysqli_fetch_array($usuario_consulta)) {
                                $ID = $listrows['ID'];
                                $usuario = $listrows['usuario'];
                                $correo_usuario = $listrows['correo'];
                            }
                         } ?>
                        <?php if ($_GET['idpr'] != "") { ?>
                            <input name="npdr" type="hidden" id="npdr" value="<?php echo $_GET['idpr'] ; ?>">
                        <?php } ?>

                        <div class="row control-group">

                              <label for="email" class="col-sm-6 form-label"><?= _("Su correo electronico es") ?> :</label>
                              <div class="col-sm-6">
                                <input type="email" class="form-control"  name="email" id="email" required  value="<?php echo $correo_usuario; ?>" readonly>
                            </div>
                        </div>
                        <?php if ($usuario != "") { ?>
                            <div class=" row control-group">
                                    <label for="usuario" class="col-sm-6 form-label"><?= _("Su nombre de usuario es") ?> : </label>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control" name="name" id="name" value="<?php echo "$usuario"; ?>"   data-validation-required-message="<?= _("Su usuario") ?>"  />
                                    <p class="text-info"><small><?= _("Debe de usar caracteres alfanumericos y no dejar espacios en blanco") ?></small> </p>
                                </div>
                            </div>
                        <?php } else { ?>
                          <div class="row control-group">
                                    <label for="name" class="col-sm-6 form-label"><?= _("Ponga el nombre de usuario que quiere usar") ?> </label>
                                    <div class="col-sm-6">
                                    <input type="text" required class="form-control" name="name" id="name"  placeholder="<?= _("Ponga el nombre de usuario que quiere usar") ?>" pattern="[A-Za-z0-9_]{6,15}" title="<?= _("Debe de usar entre 6 y 15 caracteres alfanumericos y no dejar espacios en blanco") ?>" data-validation-required-message="<?= _("Su usuario") ?>" />
                                    <p class="text-info"><small><?= _("Debe de usar entre 6 y 15 caracteres alfanumericos y no dejar espacios en blanco") ?> </small></p>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="row control-group">
                            <label for="pass" class="col-sm-6 form-label"><?= _("Su password") ?> : </label>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" placeholder="<?= _("Escoja su password") ?>" id="pass" name="pass" required  data-validation-required-message="<?= _("El password es un dato requerido") ?> " />
                                <p class="help-block"></p>
                            </div>
                        </div>


                        <div class="row control-group">
                            <label for="pass2" class="col-sm-6 form-label"><?= _("Repita su password") ?> : </label>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" placeholder="<?= _("Repita su password") ?>" id="pass2"  name="pass2"  data-validation-match-match="pass" required  data-validation-required-message="<?= _("El password es un dato requerido") ?> " data-validation-match-message="<?= _("El password tiene que ser igual") ?> " />
                                <p class="help-block"></p>
                            </div>
                        </div>



                        <button type="submit" class="btn btn-start-order btn-block"><?= _("Enviar") ?> </button><br />

                </form>
                </div>
  <div id="success"> </div> <!-- mensajes -->


            </fieldset>
        </div>
                <script src="assets/js/interventores/crea_contrasena.js" ></script>
              <?php
                            } else {
                                echo "<br/> <br/><br/>";
                                echo _("Hay algun tipo de error con la URL que ha enviado. Si ha copiado y pegado la URL compruebe que esta completa");
                                echo "<br/><br/><br/>";
                            }
           ?>
         </div>
         <div class="col-sm-3"></div>
         </div>
         </div>
</div>
          <?php } else {
               include("../private/blog/error404.php");
           } ?>

<?php
} ?>

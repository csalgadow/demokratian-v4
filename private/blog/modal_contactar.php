<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que carga el modal con el formulario de contactar del blog
*/
?>

<!--
==========================  ventana modal contactar
-->

                  <div class="modal fade contacta" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title"><?= _("Contactar") ?></h5>
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          </div>
                        <div class="modal-body ">

            <div class="modal-body">
                <div  id="divcontactaForm" >
                    <p><?= _("Si quieres contactar, completa este formulario y en breve te contestaremos") ?></p>
                    <p><?= _("Asegurate de que escribes bien tu direccion de correo") ?> </p>

                    <form name="formularioContacto" class="well" id="formularioContacto" >
                        <!--<legend>Contact me</legend>-->
                        <div class="control-group">
                            <div class="controls">
                                <input type="text" class="form-control" placeholder="<?= _("Su nombre") ?>" id="nombre2" name="nombre2" required data-validation-required-message="<?= _("Por favor, ponga su nombre") ?>" />
                                <p class="help-block"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="controls">
                                <input type="email2" class="form-control" placeholder="<?= _("Su correo electronico") ?>" id="email2" name="email2" required  data-validation-required-message="<?= _("Por favor, ponga su correo electronico") ?>" />
                            </div>
                        </div>

                        <?php if ($es_municipal == false) {
                          $lista="";
                          ?>
                            <div class="control-group">
                                <span class="help-block">
                                  <br/><?= _("Soy de la provincia") ?>: </span>


                                <div class="form-group">
                                    <select class="form-control custom-select"  name="provincia2" id="provincia2" >
                                        <!-- <option value=""> Escoje una provincia</option>-->
                                        <?php
                                        // listar para meter en una lista del tipo enlace
                                        //$especial = "";
                                        //$activo = 0;
                                        $lista = "";
                                        $tbn8 = $extension . "provincia";

                                        //$options = "select DISTINCT ID,provincia from $tbn8 where especial ='$especial' order by id  ";
                                        $options = "select DISTINCT ID,provincia from $tbn8  order by id  ";
                                        $resulta = mysqli_query($con, $options) or die("error: " . mysqli_error($con));
                                    
                                        while ($listrows = mysqli_fetch_array($resulta)) {
                                            $name = $listrows['provincia'];
                                            $id_pro = $listrows['ID'];
                                            $lista .= "<option value=\"$id_pro\"> $name</option>";
                                        }
                                        echo $lista;
                                        ?>
                                    </select>
                                </div>



                            </div>

                        <?php } ?>
                        <div class="control-group">
                            <div class="controls">
                                <textarea rows="10" cols="100" class="form-control"
                                          placeholder="<?= _("Cuentanos el problema") ?>" id="texto" name="texto" required
                                          data-validation-required-message="<?= _("Cuentanos el problemaa") ?>" minlength="5"
                                          data-validation-minlength-message="<?= _("Min 5 characteresa") ?>"
                                          maxlength="999" style="resize:none"></textarea>
                            </div>
                        </div>

<br/>
                        <button type="submit" class="btn btn-primary btn-block" id="btnEnviar" name="btnEnviar"><?= _("Enviar") ?></button><br />
                    </form>
                </div>
                <div id="success2"> </div> <!-- mensajes -->

            </div>



        </div>
    </div>
</div>
<!--
===========================  fin ventana modal contactar
-->

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que carga los distintos tipos de presentaciones que pueden tener los candidatos u opciones (archivos de modelos de la carpeta [ballot])
*/
function fn_filtro_numerico_ext($conexion, $cadena) {

    if (is_numeric($cadena)) {
        $cadena = $cadena;
    } else {
        $cadena = "";
        echo '<div class="alert alert-warning">
                <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                  <i class="fa fa-times simple-remove" aria-hidden="true"></i>
                </button>
                <span>
                  <b> '._("ERROR").' - </b> '._("No hay datos de esta votación, quizas haya terminado ya o no existe").'</span>
              </div>';
      //  Header("Location: ../index.php?error_login=9");
        exit;
    }
    $cadena = strip_tags($cadena);
    return mysqli_real_escape_string($conexion, $cadena);
}




if (isset($_GET['p']) != "") {
$idvot = fn_filtro_numerico_ext($con, base64_decode($_GET['p'])); //variable que nos llega con la votacion
$hay_datos = true;
} elseif (isset($_GET['c'] )) {

  $idvot=$variables['idvot'];
  $hay_datos = true;

  }else {
    $hay_datos = false;
  }

if($hay_datos == true){
//

    $presentacion = 0;
    $diseno="";
    $activa = "si"; //Si la votacion esta activa o no
    $sql_vot = "SELECT a.id_provincia,a.activa,a.tipo,a.tipo_votante,a.nombre_votacion,a.texto,a.resumen, a.fecha_com,a.fecha_fin, b.presentacion,
    b.cabecera,b.imagen_cab,b.aux,b.diseno FROM $tbn1 a,$tbn22 b  WHERE (a.ID= b.id_votacion) and a.ID='$idvot' and a.activa='$activa' and b.activo=0 ";
    $res_votacion = mysqli_query($con, $sql_vot);

    $row_vot = mysqli_fetch_row($res_votacion);
    if (is_null($row_vot)) {
        $no_hay_info = _("No hay datos de esta votación, quizas haya terminado ya o no existe");
        $hay_datos = false;
    } else {
        $id_provincia = $row_vot[0];
        $activa = $row_vot[1];
        $tipo = $row_vot[2];
        $tipo_votante = $row_vot[3];
        $nombre_votacion = $row_vot[4];
        $texto = $row_vot[5];
        $resumen = $row_vot[6];
        $fecha_com = $row_vot[7];
        $fecha_fin = $row_vot[8];
        $presentacion = $row_vot[9];
        $cabecera = $row_vot[10];
        $imagen_cab = $row_vot[11];
        $aux = $row_vot[12];
        $diseno = $row_vot[13];
        $hay_datos = true;
    }
}
?>
<?php if ($presentacion == 1) { ?>
    <link rel="stylesheet" type="text/css" href="temas/<?php echo "$tema_web"; ?>/ballot/modelo1/estilos.css">
<?php } elseif ($presentacion == 2) { ?>
    <link  rel="stylesheet" type="text/css" href="temas/<?php echo "$tema_web"; ?>/ballot/modelo2/estilos.css">
<?php } elseif ($presentacion == 3) { ?>
    <link  rel="stylesheet" type="text/css" href="temas/<?php echo "$tema_web"; ?>/ballot/modelo3/estilos.css">
<?php } elseif ($presentacion == 4 or $presentacion == 5 or $presentacion == 6) { ?>
    <link rel="stylesheet" type="text/css" href="temas/<?php echo "$tema_web"; ?>/ballot/modelo4/css/normalize.css">
    <link rel="stylesheet" type="text/css" href="temas/<?php echo "$tema_web"; ?>/ballot/modelo4/css/estilos.css">
    <link rel="stylesheet" type="text/css" href="temas/<?php echo "$tema_web"; ?>/ballot/modelo4/css/component.css">
    <script src="temas/<?php echo "$tema_web"; ?>/ballot/modelo4/js/snap.svg-min.js" ></script>
    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js" ></script>
    <![endif]-->
<?php } ?>
<?php if ($diseno != "") { ?>
    <style type="text/css">
<?php echo $diseno; ?>
    </style>
<?php } ?>



<div class="container demokratian-access">
<?php if ($hay_datos == true) { ?>
    <!---->

    <h1><?php echo "$nombre_votacion"; ?></h1>

    <?php echo "$resumen"; ?>
    <!-- Contenedor general -->

    <?php
    $sql = "SELECT * FROM $tbn7 WHERE id_votacion = '$idvot'  ORDER BY rand(" . time() . " * " . time() . ")  ";
    $result = mysqli_query($con, $sql);
    $total_resultados = mysqli_num_rows($result); // obtenemos el número de filas
    ?>
    <?php if ($presentacion == 1) { ?>
        <?php include('../private/blog/ballot/modelo1.php'); ?>
    <?php } elseif ($presentacion == 2) { ?>
        <?php include('../private/blog/ballot/modelo2.php'); ?>
    <?php } elseif ($presentacion == 3) { ?>
        <?php include('../private/blog/ballot/modelo3.php'); ?>
    <?php } elseif ($presentacion == 4) { ?>
        <?php include('../private/blog/ballot/modeloa.php'); ?>
    <?php } elseif ($presentacion == 5) { ?>
        <?php include('../private/blog/ballot/modelob.php'); ?>
    <?php } elseif ($presentacion == 6) { ?>
        <?php include('../private/blog/ballot/modeloc.php'); ?>
    <?php } ?>


<?php } else { ?>
    <div class="contenedor">
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <div class="alert alert-warning">
                <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                  <i class="fa fa-times simple-remove" aria-hidden="true"></i>
                </button>
                <span>
                  <b> <?php echo _("ERROR"); ?> - </b> <?php echo $no_hay_info; ?></span>
              </div>';

        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
    </div>
<?php } ?>
</div>

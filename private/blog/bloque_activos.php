<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que carga los bloques que tenemos activos, así como el orden establecido según nuestra configuración en la página de inicio del blog (zona del cuerpo de la página )
*/
if(!isset($carga)){
  $carga =false;
  exit;
}
if($carga!="OK"){
  exit;
}else{

  $result_body = mysqli_query($con, "SELECT bloque FROM $tbn35 where ID=$idpg");
  $row_body = mysqli_fetch_row($result_body);

  $array_orden_body = explode(",", $row_body[0]);
  $longitud_body = count($array_orden_body);



    if ($longitud_body==0){
      include("../private/blog/login.php");
    }else{
      $sql_body = "SELECT id,bloque FROM $tbn35 where ID>10 and activo=1";
      $result_body = mysqli_query($con, $sql_body);
      while( $fila_bloques = mysqli_fetch_assoc( $result_body )){ // sacamos los datos de la consulta y los metemos en un array
          $nuevo_array_bloques[] = $fila_bloques;
      }

        for($i=0; $i<$longitud_body; $i++){
        foreach($nuevo_array_bloques as $elemento) {
          if($elemento['id']==$array_orden_body[$i]) {
            $bloque_activo="activo"; // le decimos al bloque que vamos a cargar que esta activo
            include("../private/blog/".$elemento['bloque']);
             unset ($bloque_activo) ;// eliminamos la variable una vez que hemos cargado el bloque activo de esta iteración
            }
          }
        }

        }
}
?>

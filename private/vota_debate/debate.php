<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que carga las distintas partes del debate, los archivos wall,php y wall_votación.php 
*/
if(!isset($carga)){
  $cargaA =false;
  exit;
}
if($carga!="OK"){
  exit;
}else{
  include('../private/inc_web/seguri.php');
  include_once('../private/basicos_php/time_stamp.php');
?>

        <link href="temas/<?php echo "$tema_web"; ?>/css/debateStyle.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="assets/msdropdown/css/dd.css" />
        <link rel="stylesheet" href="assets/morris.js-0.5.1/morris.css">


        <div class="card-header-votaciones "> <h1 class="card-title"><?= _("Debate") ?>: <?php echo "$nombre_votacion"; ?></h1> </div>


        <div class="card-body">



                    <?php if($texto!=""){?>
                      <div class="card">
                        <div class="card-body">
                  <?php   echo $texto; ?>
</div>
</div>
                <?php   }?>

  <div class="row">
                <div class="col-md-6">

                    <!---->

<div class="card">
  <div class="card-body">
                  <!--  <div  class="caja_refrescar"> <a href="#" onclick="recargar()" class="btn btn-info float-right"> <span class="fa fa-refresh"></span>&nbsp;<?= _("Actualizar") ?></a></div>-->
                    <!--columna de comentarios-->


                    <div id="wrapper">

                        <div id="success2"></div>
                        <?php
                        $hoy = strtotime(date('Y-m-d H:i:s'));
                        $fecha_ini = strtotime($fecha_com);
                        $fecha_fin = strtotime($fecha_fin);


                        if ($fecha_ini <= $hoy && $fecha_fin >= $hoy) {
                            ?>




                            <form action="javascript: addMessage();" method="post"  class="well"  id="form_wall">




                                    <label>
                                        <select name="mi_estado" id="mi_estado" style="width: 200px;">

                                            <option value=""><?= _("Mi estado") ?></option>
                                            <option value="angel_32x32.png" data-image="temas/<?php echo "$tema_web"; ?>/images/emoticonos/angel_32x32.png" > </option>
                                            <!--        <option value="angry_32x32.png" data-image="temas/<?php echo "$tema_web"; ?>/images/emoticonos/angry_32x32.png">2</option>-->
                                            <option value="batman_32x32.png" data-image="temas/<?php echo "$tema_web"; ?>/images/emoticonos/batman_32x32.png" > </option>
                                            <option value="clown_32x32.png" data-image="temas/<?php echo "$tema_web"; ?>/images/emoticonos/clown_32x32.png"> </option>
                                            <option value="cool_32x32.png" data-image="temas/<?php echo "$tema_web"; ?>/images/emoticonos/cool_32x32.png"> </option>
                                            <option value="crying_32x32.png" data-image="temas/<?php echo "$tema_web"; ?>/images/emoticonos/crying_32x32.png"> </option>
                                            <!--    <option value="devil_32x32.png" data-image="temas/<?php echo "$tema_web"; ?>/images/emoticonos/devil_32x32.png" >7</option>-->
                                            <option value="displeased_32x32.png" data-image="temas/<?php echo "$tema_web"; ?>/images/emoticonos/displeased_32x32.png"> </option>
                                            <option value="happy_32x32.png" data-image="temas/<?php echo "$tema_web"; ?>/images/emoticonos/happy_32x32.png" > </option>
                                            <option value="harrypotter_32x32.png" data-image="temas/<?php echo "$tema_web"; ?>/images/emoticonos/harrypotter_32x32.png"> </option>
                                            <option value="hypnotize_32x32.png" data-image="temas/<?php echo "$tema_web"; ?>/images/emoticonos/hypnotize_32x32.png"> </option>
                                            <option value="hypnotized_32x32.png" data-image="temas/<?php echo "$tema_web"; ?>/images/emoticonos/hypnotized_32x32.png"> </option>
                                            <option value="inlove_32x32.png" data-image="temas/<?php echo "$tema_web"; ?>/images/emoticonos/inlove_32x32.png" > </option>
                                            <option value="kiss_32x32.png" data-image="temas/<?php echo "$tema_web"; ?>/images/emoticonos/kiss_32x32.png"> </option>
                                            <option value="kissed_32x32.png" data-image="temas/<?php echo "$tema_web"; ?>/images/emoticonos/kissed_32x32.png" > </option>
                                            <option value="laughtingoutloud_32x32.png" data-image="temas/<?php echo "$tema_web"; ?>/images/emoticonos/laughtingoutloud_32x32.png"> </option>
                                            <option value="money_32x32.png" data-image="temas/<?php echo "$tema_web"; ?>/images/emoticonos/money_32x32.png"> </option>
                                            <option value="party_32x32.png" data-image="temas/<?php echo "$tema_web"; ?>/images/emoticonos/party_32x32.png"> </option>
                                            <option value="pirate_32x32.png" data-image="temas/<?php echo "$tema_web"; ?>/images/emoticonos/pirate_32x32.png" > </option>
                                            <option value="question_32x32.png" data-image="temas/<?php echo "$tema_web"; ?>/images/emoticonos/question_32x32.png"> </option>
                                            <option value="sad_32x32.png" data-image="temas/<?php echo "$tema_web"; ?>/images/emoticonos/sad_32x32.png" > </option>
                                            <option value="sleeping_32x32.png" data-image="temas/<?php echo "$tema_web"; ?>/images/emoticonos/sleeping_32x32.png"> </option>
                                            <option value="surprised_32x32.png" data-image="temas/<?php echo "$tema_web"; ?>/images/emoticonos/surprised_32x32.png"> </option>
                                            <option value="terminator_32x32.png" data-image="temas/<?php echo "$tema_web"; ?>/images/emoticonos/terminator_32x32.png"> </option>
                                            <option value="tongue_32x32.png" data-image="temas/<?php echo "$tema_web"; ?>/images/emoticonos/tongue_32x32.png" > </option>
                                            <option value="vomited_32x32.png" data-image="temas/<?php echo "$tema_web"; ?>/images/emoticonos/vomited_32x32.png"> </option>
                                            <option value="watermelon_32x32.png" data-image="temas/<?php echo "$tema_web"; ?>/images/emoticonos/watermelon_32x32.png" > </option>
                                            <option value="wink_32x32.png" data-image="temas/<?php echo "$tema_web"; ?>/images/emoticonos/wink_32x32.png"> </option>
                                        </select>
                                    </label>

                                    <input name="IDST" type="hidden" id="IDST" value="<?php echo $_SESSION['ID']; ?>" />
                                    <div class="input-group">

                                                                      <textarea name="msg"  rows="4" id="msg" class="form-control" aria-label="With textarea" placeholder="<?= _("Tu comenatrio") ?>"></textarea>
                                                                      <div class="input-group-prepend">
                                                                        <input type="submit" name="submit" id="submit" value="comentar" class="input-group-text btn btn-primary"/>
                                                                      <!--  <button type="submit" name="submit" id="submit" value="comentar" class="input-group-text btn btn-secondary">Send</button>-->

                                                                      </div>

                                                                    </div>

                              <!--  <textarea name="msg"  rows="4" id="msg" class="form-control" placeholder="<?= _("Tu comenatrio") ?>"></textarea>


                                <input type="submit" name="submit" id="submit" value="comentar" class="btn btn-primary pull-right"/><p>&nbsp;</p>-->
                            </form>




                            <?php
                        } else {
                            echo "<h2>" . _("El debate esta cerrado") . "</h2>";
                        }
                        ?>
                    </div>

                                                <div class="row">
                                                  <div class="col-md-12">
                                                  <a href="#" onclick="recargar()" class="btn btn-info"> <span class="fa fa-refresh"></span>&nbsp;<?= _("Actualizar") ?></a>
                                                </div>
                                                </div>
                        <div id="wall"></div>

              </div>
            </div>



                    <!---->


                </div>



<!-- segunda columna votaciones-->

                <div class="col-md-6">
                  <div class="card">
                    <div class="card-body">

                    <div id="wrapper_votacion">



                        <div id="form">


                            <div id="wall_votacion"></div>
                        </div>


</div>
</div>
                    </div>


                    <!--Fin columna votaciones-->

                </div>
              </div>

              <?php if($resumen!=""){?>
                              <div class="card">
                                <div class="card-body">
                                  <?php echo "$resumen";?>
                                </div>
                              </div>
                                  <?php
                                }
                                ?>

</div>
        <script src="assets/raphael_2.2.1/raphael.min.js" ></script>
        <script src="assets/morris.js-0.5.1/morris.min.js" ></script>
        <script src="assets/msdropdown/jquery.dd.min.js" ></script>



        <script type="text/javascript">
                        function addMessage() {

                            //Tomas el valor del campo msg
                            var msg = $("#msg").val();
                            var IDST = $("#IDST").val();
                            var mi_estado = $("#mi_estado").val();

                            //Se envian los datos a url y al completarse se recarga el muro
                            //con la nueva informacion
                            $.ajax({
                                url: 'aux_vota.php?c=<?php echo encrypt_url('vota_debate/action/idvot='.$idvot,$clave_encriptacion) ?>',
                                data: 'msg=' + msg + '&IDST=' + IDST + '&mi_estado=' + mi_estado,
                                type: 'post',

                                error: function () {
                                    // Fail message
                                    $('#success2').html("<div class='alert alert-danger'>");
                                    $('#success2 > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                            .append("</button>");
                                    $('#success2 > .alert-danger').append("<strong>Sorry " + data + " uppps! el servidor no esta respondiendo...</strong> Intetelo despues. Perdone por las molestias!");
                                    $('#success2 > .alert-danger').append('</div>');
                                    //clear all fields
                                    $('#contactForm').trigger("reset");
                                },
                                success: function (data) {

                                  var result = data.trim().split("#");
                                  if (result[0] == 'OK') {
                                    //  $("#votacion").hide("slow");
                                    //  $('#respuestaOK').find('div').html(result[1]);
                                    //  $('#respuestaOK').show();
                                      loadWall();
                                    //  recargar();
                                  }

                                }
                            });
                        }
                        ;


        </script>
        <script type="text/javascript">
                        function loadWall() {
                            //Funcion para cargar el muro
                            //$("#rolling").toggle(); // para poner el "cargando"
                            $("#wall").css('opacity', 0.4);
                            $("#wall").load('aux_vota.php?c=<?php echo encrypt_url('vota_debate/wall/idvot='.$idvot,$clave_encriptacion) ?>&rand='+ Math.random()+ '#wall', function(){
                                //aquí puedes meter más código si necesitas;
                                $("#wall").css('opacity', 1);
                              //  $("#rolling").toggle();
                            });
                            //Devuelve el campo message a vacio
                            $("#msg").val("")
                            //var  idvot  =  $("#idvot").val();
                        }


                        ////////////////////// script para cargar la parte votacion
                        function loadWall_votacion ()  {
                            //Funcion para cargar el muro
                            //$("#rolling").toggle(); // para poner el "cargando"
                            $("#wall_votacion").css('opacity', 0.4);
                            $("#wall_votacion").load('aux_vota.php?c=<?php echo encrypt_url('vota_debate/wall_votacion/idvot='.$idvot.'&grprp='.$_SESSION['ID'],$clave_encriptacion) ?>&rand='+ Math.random()+ '#wall_votacion', function(){
                                //aquí puedes meter más código si necesitas;
                                $("#wall_votacion").css('opacity', 1);
                              //  $("#rolling").toggle();
                            });
                            //Devuelve el campo message a vacio

                            $("#id_pregunta").val("");
                            $("#voto").val("");
                            $("#enviar_voto").val("");

                        }

                        function recargar() {
                            $("#wall").fadeOut(1000);
                            $("#wall_votacion").fadeOut(1000);
                            loadWall_votacion();
                            loadWall();
                            <!--$(“#contenido”).load( pagina);-->
                            $("#wall_votacion").fadeIn(1000);
                            $("#wall").fadeIn(1000);

                          }

            //Cuando el documento esta listo carga el muro
            $(document).ready(function() {
                loadWall();
                loadWall_votacion();
            });

            // funcion para refrescar las dos partes del muro
            $(document).ready(function(){
              var refreshId = setInterval(recargar, 300000); // actualizamos cada 5 minutos automaticamente
              $.ajaxSetup({ cache: false });
            });
    </script>
    <script type="text/javascript">
            <!--scrip para el  combobox-->
            $(document).ready(function (e) {
            $("#mi_estado").msDropdown({visibleRows:5});
            <!--$("#tech").msDropdown();-->
            });
        </script>


    <script type="text/javascript">
      <?php
      //////miramos si hay preguntas para el loop del addVoto()
      $query_pregunta = mysqli_query($con, "SELECT  ID, pregunta, respuestas , id_votacion  FROM $tbn13  where id_votacion= '$idvot' ");
      //Si la consulta es verdadera
      if ($query_pregunta == true) {
      //Recorro todos los campos de la tabla y los muestro

          while ($row_pregunta = mysqli_fetch_array($query_pregunta)) {
              ?>
                        function addVoto_<?php echo $row_pregunta['ID']; ?>() {

                                //Tomas el valor del campo msg
                                var id_usuario = $("#id_usuario").val();
                                var id_pregunta = $("#id_pregunta<?php echo $row_pregunta['ID']; ?>").val();
                                var voto = $("[name='voto<?php echo $row_pregunta['ID']; ?>']:checked").val();
                                var enviar_voto = $("#enviar_voto").val();
                                var IDST = $("#IDST").val();

                                //Se envian los datos a url y al completarse se recarga el muro
                                //con la nueva informacion
                                $.ajax({
                                    url: 'aux_vota.php?c=<?php echo encrypt_url('vota_debate/action_voto/idvot='.$idvot,$clave_encriptacion) ?>',
                                    data: '&id_pregunta=' + id_pregunta + '&voto=' + voto + '&id_usuario=' + id_usuario + '&enviar_voto=' + enviar_voto + '&IDST=' + IDST,
                                    type: 'post',
                                    error: function (obj, msg, obj2) {
                                        alert(msg);
                                    },
                                    success: function (data) {
                                        loadWall_votacion();
                                    }
                                });
                            }
                            ;

        <?php
    }
}
?>

            </script>
<?php } ?>

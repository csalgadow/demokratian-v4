<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que carga los comentarios que hay en un debate y envía mediante ajax a action.php los datos para incluir un comentario
*/
if(!isset($carga)){
  $cargaA =false;
  exit;
}
if($carga!="OK"){
  exit;
}else{
  include_once('../private/inc_web/seguri.php');
  include_once('../private/basicos_php/time_stamp.php');
  include_once('../private/basicos_php/basico.php');
  include_once('../private/basicos_php/errores.php');
  //$idvot= 000001 ;
  $idvot = fn_filtro_numerico($con, $variables['idvot']);
  //Realizo la consulta de la tabla y ordeno por fecha (El ultimo mensaje de primero)

  $query = mysqli_query($con, "SELECT a.nombre_usuario, a.apellido_usuario, a.imagen_pequena,a.usuario, b.fecha, b.comentario, b.estado, b.id_usuario  FROM $tbn9 a, $tbn12 b where (a.ID= b.id_usuario) and id_votacion= '$idvot' ORDER BY fecha DESC");

  //Si la consulta es verdadera
  if ($query == true) {
    //Recorro todos los campos de la tabla y los muestro

    while ($row = mysqli_fetch_array($query)) {
        ?>
        <?php if ($row['id_usuario'] == $_SESSION['ID']) {
          $user_float="";
          $user_float2="float-left";
          $user_float3="float-right";
        } else{
          $user_float="right";
          $user_float2="float-right";
          $user_float3="float-left";
        }?>
    <div class="direct-chat-msg <?php echo $user_float; ?>">
        <?php if ($row['imagen_pequena'] == "peq_usuario.jpg" or $row['imagen_pequena'] == "") {
          $imagen_url="../temas/".$tema_web."/images/user.png";
        } else {
          $imagen_url= $upload_user."/".$row['imagen_pequena'];

        }
        $name_data=$row['nombre_usuario']." ".$row['apellido_usuario'];
          ?>

                                <div class="direct-chat-infos clearfix">
                                  <span class="direct-chat-name  <?php echo $user_float2; ?>"><?php echo $name_data; ?></span>
                                  <span class="direct-chat-timestamp  <?php echo $user_float3; ?>"><?php
                                      $fecha = $row['fecha'];
                                      time_stamp($fecha);
                                      ?> </span>
                                </div>
                                <!-- /.direct-chat-infos -->


                                <img class="direct-chat-img" src="<?php echo $imagen_url; ?>" alt="<?php echo $name_data; ?>">
                                <!-- /.direct-chat-img -->
                                <div class="direct-chat-text">
                                <?php if ($row['estado'] != "") { ?> <div class="<?php echo $user_float2; ?> user-state" ><img src="temas/<?php echo "$tema_web"; ?>/images/emoticonos/<?php echo $row['estado']; ?>" width="32" height="32" /></div> <?php } ?>

                                  <?php
$str = str_replace("\n", '<br/>', $row['comentario']);
                                  echo $str; ?>
                                </div>
                                <!-- /.direct-chat-text -->


<!---------->


        </div>



        <?php
    } ?>
      </div>
<?php
}
?>
<?php } ?>

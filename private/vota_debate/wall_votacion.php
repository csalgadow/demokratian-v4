<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que carga los los votos que hay en un debate y envía mediante ajax a actio_voton.php los datos para incluir/modificar un voto 
*/
if(!isset($carga)){
  $cargaA =false;
  exit;
}
if($carga!="OK"){
  exit;
}else{
  include_once('../private/inc_web/seguri.php');
  include_once('../private/basicos_php/time_stamp.php');
  include_once('../private/basicos_php/basico.php');
  include_once('../private/basicos_php/errores.php');

  $idvot = fn_filtro_numerico($con, $variables['idvot']);
  $id_user = fn_filtro_numerico($con, $variables['grprp']);
  $array_datos_res = "";

  $sql3 = "SELECT  fecha_com, fecha_fin  FROM $tbn1 WHERE ID ='$idvot' ";
  $resulta3 = mysqli_query($con, $sql3) or die("error: " . mysqli_error());

  while ($listrows3 = mysqli_fetch_array($resulta3)) {
      $fecha_com = $listrows3['fecha_com'];
      $fecha_fin = $listrows3['fecha_fin'];
  }
  $hoy = strtotime(date('Y-m-d H:i:s'));
  $fecha_ini = strtotime($fecha_com);
  $fecha_fin = strtotime($fecha_fin);

  if ($fecha_ini <= $hoy && $fecha_fin >= $hoy) {
      $estado_votacion = "abierto";
  }
  /* echo	$fecha_com;
    echo $fecha_fin;
   */
  $query_pregunta = mysqli_query($con, "SELECT  ID, pregunta, respuestas , id_votacion  FROM $tbn13  where id_votacion= '$idvot' ");

  //Si la consulta es verdadera
  if ($query_pregunta == true) {
      //Recorro todos los campos de la tabla y los muestro
      $quants = mysqli_num_rows($query_pregunta);
      ?>
      <script type="text/javascript">
          $(document).ready(function () {
              var elem4 = $('#botonvergrafica');
              var elem = $('.resultados_donut');
              elem.hide();
              elem4.hide();
          });
      </script>

      <h4> <?= _("Este debate tiene") ?> <?php echo $quants; ?> <?= _("pregunta") ?><?php
          if ($quants != 1) {
              echo _("s");
          }
          ?></h4>

      <?php
      while ($row_pregunta = mysqli_fetch_array($query_pregunta)) {
  //////////////////// limpiamos variables del loop anterior si lo hay
          $chekea1 = "";
          $chekea2 = "";
          $chekea3 = "";
          $chekea4 = "";
          $chekea5 = "";
          $ha_votado1 = "";
          $ha_votado2 = "";
          $ha_votado3 = "";
          $ha_votado4 = "";
          $ha_votado5 = "";
          $clase_votado1 = "";
          $clase_votado2 = "";
          $clase_votado3 = "";
          $clase_votado4 = "";
          $clase_votado5 = "";
          $boton_voto = "";

          ////////////miramos si ha votado y el que para indcarlos
          $query_votoa = mysqli_query($con, "SELECT  voto   FROM $tbn14  where id_pregunta = '" . $row_pregunta['ID'] . "' and id_votante ='" . $id_user . "' ");

//Si la consulta es verdadera
        if ($row_votoa = mysqli_fetch_array($query_votoa)) {
            mysqli_field_seek($query_votoa, 0);

            do {

                if ($row_votoa['voto'] == 1) {
                    $boton_voto = _("modificar");
                    $chekea1 = "checked=\"checked\" ";
                    $ha_votado1 = '<span class="info_voto ">'._("Su voto actual").'</span>';
                    $clase_votado1 = "class=\"alert alert-warning\"";
                } else if ($row_votoa['voto'] == 2) {
                    $boton_voto = _("modificar");
                    $chekea2 = "checked=\"checked\" ";
                    $ha_votado2 = '<span class="info_voto ">'._("Su voto actual").'</span>';
                    $clase_votado2 = "class=\"alert alert-warning\"";
                } else if ($row_votoa['voto'] == 3) {
                    $boton_voto = _("modificar");
                    $chekea3 = "checked=\"checked\" ";
                    $ha_votado3 = '<span class="info_voto ">'._("Su voto actual").'</span>';
                    $clase_votado3 = "class=\"alert alert-warning\"";
                } else if ($row_votoa['voto'] == 4) {
                    $boton_voto = _("modificar");
                    $chekea4 = "checked=\"checked\" ";
                    $ha_votado4 = '<span class="info_voto ">'._("Su voto actual").'</span>';
                    $clase_votado4 = "class=\"alert alert-warning\"";
                } else if ($row_votoa['voto'] == 5) {
                    $boton_voto = _("modificar");
                    $chekea5 = "checked=\"checked\" ";
                    $ha_votado5 = '<span class="info_voto ">'._("Su voto actual").'</span>';
                    $clase_votado5 = "class=\"alert alert-warning\"";
                }
            } while ($row_votoa = mysqli_fetch_array($query_votoa));
        } else {
            $chekea1 = "";
            $chekea2 = "";
            $chekea3 = "checked=\"checked\" ";
            $chekea4 = "";
            $chekea5 = "";
            $ha_votado1 = "";
            $ha_votado2 = "";
            $ha_votado3 = "";
            $ha_votado4 = "";
            $ha_votado5 = "";
            $clase_votado1 = "";
            $clase_votado2 = "";
            $clase_votado3 = "";
            $clase_votado4 = "";
            $clase_votado5 = "";
            $boton_voto = "";
        }
        ?>




        <div class="fondo_voto">

            <div class="titular-voto contenido-comentario">
              <h5 class="text-center">  <?php echo $row_pregunta['pregunta'];  ?></h5>


            </div>

            <?php
            $hoy = strtotime(date('Y-m-d'));
            $fecha_ini = strtotime($fecha_com);
            $fecha_fin = strtotime($fecha_fin);


            if ($estado_votacion == "abierto") {
                ?>




                <div class="texto_mensaje">
                    <form id="form<?php echo $row_pregunta['ID']; ?>" name="form<?php echo $row_pregunta['ID']; ?>" method="post" action="javascript: addVoto_<?php echo $row_pregunta['ID']; ?>();">


                        <?php
                        $respuestas = $row_pregunta['respuestas'];

                        switch ($row_pregunta['respuestas']) {
                            case 2:
                                echo '<p class="text-right text-info">'. _("Tienes 2 posibles respuestas").'</p>';
                                ?>



                                <table width="100%" border="0" class="clase_tabla">
                                    <tr>
                                        <td>
                                            <div  <?php echo $clase_votado1; ?>  ><label class="nik_debate">
                                                    <input name="voto<?php echo $row_pregunta['ID']; ?>" type="radio"  id="voto_0" value="1"  checked="checked"  />
                                                    <?= _("Me gusta") ?></label>
                                                <?php echo $ha_votado1; ?></div>
                                        </td></tr><tr><td>
                                            <div  <?php echo $clase_votado2; ?>  >
                                                <label class="nik_debate" >
                                                    <input type="radio" name="voto<?php echo $row_pregunta['ID']; ?>" value="2" id="voto_1"  <?php echo $chekea2; ?>/>
                                                    <?= _("No me gusta") ?></label>
                                              <?php echo $ha_votado2; ?></div>

                                        </td></tr></table>




                                <?php
                                break;
                            case 3:
                                echo '<p class="text-right text-info">'._("Tienes 3 posibles respuestas").'</p>';
                                ?>



                                <table width="100%" border="0" class="clase_tabla">
                                    <tr>
                                        <td>
                                            <div  <?php echo $clase_votado1; ?>  ><label class="nik_debate">
                                                    <input name="voto<?php echo $row_pregunta['ID']; ?>" type="radio"  id="voto_0" value="1"   <?php echo $chekea1; ?>/>
                                                    <?= _("Me gusta") ?></label>
                                                <?php echo $ha_votado1; ?></div>
                                        </td></tr><tr><td>
                                            <div  <?php echo $clase_votado3; ?>  >
                                                <label class="nik_debate" >
                                                    <input type="radio" name="voto<?php echo $row_pregunta['ID']; ?>" value="3" id="voto_2"   <?php echo $chekea3; ?>/>
                                                    <?= _("No lo tengo claro") ?></label>
                                                <?php echo $ha_votado3; ?></div>
                                        </td></tr><tr><td>
                                            <div  <?php echo $clase_votado2; ?>  >
                                                <label class="nik_debate" >
                                                    <input type="radio" name="voto<?php echo $row_pregunta['ID']; ?>" value="2" id="voto_1" <?php echo $chekea2; ?> />
                                                    <?= _("No me gusta") ?></label>
                                                <?php echo $ha_votado2; ?></div>
                                        </td></tr></table>




                                <?php
                                break;
                            case 4:
                                echo '<p class="text-right text-info">'._("Tienes 4 posibles respuestas").'</p>';
                                ?>

                                <p>&nbsp;</p>

                                <table width="100%" border="0" class="clase_tabla">
                                    <tr>
                                        <td>
                                            <div  <?php echo $clase_votado1; ?>  >
                                                <label class="nik_debate">
                                                    <input name="voto<?php echo $row_pregunta['ID']; ?>" type="radio"  id="voto_0" value="1"  <?php echo $chekea1; ?>/>
                                                    <?= _("Me gusta") ?></label>
                                                <?php echo $ha_votado1; ?></div>
                                        </td></tr><tr><td>
                                            <div  <?php echo $clase_votado3; ?>  >
                                                <label class="nik_debate" >
                                                    <input type="radio" name="voto<?php echo $row_pregunta['ID']; ?>" value="3" id="voto_2"  <?php echo $chekea3; ?> />
                                                    <?= _("No lo tengo claro") ?></label>
                                                <?php echo $ha_votado3; ?></div>
                                        </td></tr><tr><td>
                                            <div  <?php echo $clase_votado2; ?>  >
                                                <label class="nik_debate" >
                                                    <input type="radio" name="voto<?php echo $row_pregunta['ID']; ?>" value="2" id="voto_1"  <?php echo $chekea2; ?>/>
                                                    <?= _("No me gusta") ?></label>
                                                <?php echo $ha_votado2; ?></div>
                                        </td></tr><tr><td>
                                            <div  <?php echo $clase_votado4; ?>  >
                                                <label class="nik_debate" >
                                                    <input type="radio" name="voto<?php echo $row_pregunta['ID']; ?>" value="4" id="voto_3"  <?php echo $chekea4; ?>/>
                                                    <?= _("No me gusta nada. Bloqueo") ?></label>
                                                    <?php echo $ha_votado4; ?> </div>
                                        </td></tr></table>




                                <?php
                                break;
                            case 5:
                                echo '<p class="text-right text-info">'._("Tienes 5 posibles respuestas").'</p>';
                                ?>

                                <p>&nbsp;</p>

                                <table width="100%" border="0" class="clase_tabla">
                                    <tr>
                                        <td>
                                            <div  <?php echo $clase_votado5; ?>  >
                                                <label class="nik_debate" >
                                                    <input type="radio" name="voto<?php echo $row_pregunta['ID']; ?>" value="5" id="voto_4"  <?php echo $chekea5; ?>/>
                                                    <?= _("Me gusta mucho.") ?></label>
                                                     <?php echo $ha_votado5; ?></div>
                                        </td></tr><tr><td>
                                            <div  <?php echo $clase_votado1; ?>  >

                                                <label class="nik_debate">
                                                    <input name="voto<?php echo $row_pregunta['ID']; ?>" type="radio"  id="voto_0" value="1"  <?php echo $chekea1; ?>/>
                                                    <?= _("Me gusta") ?></label>
                                                    <?php echo $ha_votado1; ?></div>
                                        </td></tr><tr><td>
                                            <div  <?php echo $clase_votado3; ?>  >
                                                <label class="nik_debate" >
                                                    <input type="radio" name="voto<?php echo $row_pregunta['ID']; ?>" value="3" id="voto_2"   <?php echo $chekea3; ?> />
                                                    <?= _("No lo tengo claro") ?></label>
                                                     <?php echo $ha_votado3; ?></div>
                                        </td></tr><tr><td>
                                            <div  <?php echo $clase_votado2; ?>  >

                                                <label class="nik_debate" >
                                                    <input type="radio" name="voto<?php echo $row_pregunta['ID']; ?>" value="2" id="voto_1"  <?php echo $chekea2; ?>/>
                                                    <?= _("No me gusta") ?></label>
                                                    <?php echo $ha_votado2; ?></div>
                                        </td></tr><tr><td>


                                            <div  <?php echo $clase_votado4; ?>  >
                                                <label class="nik_debate" >
                                                    <input type="radio" name="voto<?php echo $row_pregunta['ID']; ?>" value="4" id="voto_3"  <?php echo $chekea4; ?> />
                                                    <?= _("No me gusta nada.") ?></label>
                                                    <span class="info_voto "><?php echo $ha_votado4; ?></span></div>
                                        </td></tr></table>

                                <?php
                                break;
                        }
                        ?>


                        <input name="IDST" type="hidden" id="IDST" value="<?php echo $id_user; ?>" />
                        <input name="id_votacion" type="hidden" id="id_votacion" value="<?php echo $row_pregunta['id_votacion']; ?>" />

                        <input name="id_pregunta<?php echo $row_pregunta['ID']; ?>" type="hidden" id="id_pregunta<?php echo $row_pregunta['ID']; ?>" value="<?php echo $row_pregunta['ID']; ?>" />
                        <p>
                            <input type="submit" name="enviar_voto<?php echo $row_pregunta['ID']; ?>" id="enviar_voto<?php echo $row_pregunta['ID']; ?>"
                            value="<?php if ($boton_voto == "modificar") { ?>  <?= _("Modificar mi voto") ?><?php } else { ?><?= _("Enviar mi voto") ?><?php } ?>"
                            class="btn btn-primary btn-block">
                    </form> </p>
                </div>





                <?php
            } //else { //echo "<h2>El debate esta cerrado</h2>";
            //}
            ?>




            <?php
            $sql = "SELECT COUNT(voto),voto  FROM $tbn14  WHERE  id_pregunta = '" . $row_pregunta['ID'] . "'  GROUP BY voto  ORDER BY voto ";
            $result = mysqli_query($con, $sql);

            if ($row = mysqli_fetch_array($result)) {
                //$i=1;
                ?>

                <?php
                mysqli_field_seek($result, 0);

                do {

                    if ($row[1] == 1) {
                        $nombre_dato = "Me gusta";
                    } else if ($row[1] == 2) {
                        $nombre_dato = "No me gusta";
                    } else if ($row[1] == 3) {
                        $nombre_dato = "No lo tengo claro";
                    } else if ($row[1] == 4) {
                        $nombre_dato = "No me gusta nada";
                    } else if ($row[1] == 5) {
                        $nombre_dato = "me gusta mucho";
                    }
//echo "<br/>Hay votos".$row[0]."<br> de la opcion -".$nombre_dato;

                    $array_datos_res .= "{label: '$nombre_dato', value:$row[0] },";
                } while ($row = mysqli_fetch_array($result));

                $array_datos_r = substr($array_datos_res, 0, -1);
                ?>


                <?php
            } else {

                $array_datos_r = "{label: 'votos', value:0 }";
            }
            ?>
            <script type="text/javascript">
                var array_js = new Array();
                array_js = [
        <?php echo "$array_datos_r"; ?>
                ];


            </script>
            <?php
//vacio los datos para la siguiente consulta
            $array_datos_res = "";
            ?>


            <div >
                <div id="donut_resultado<?php echo $row_pregunta['ID']; ?>" class="resultados_donut"></div>

                <div id="tabla_resultado<?php echo $row_pregunta['ID']; ?>"  class="resultados_grafica"></div>
            </div>
        </div>
        <script type="text/javascript">
            var array_colores = new Array();
            array_colores = [
                '#0066CC',
                '#FF8000',
                '#FDF512',
                '#F912FD',
                '#BBD03F',
                '#12DEFD',
                '#9102C6',
                '#39FF08',
                '#0BA462',
                '#990000'
            ];
            // Use Morris.Bar
            new Morris.Bar({
                element: 'tabla_resultado<?php echo $row_pregunta['ID']; ?>',
                data: array_js, //array de los datos
                xkey: 'label',
                ykeys: ['value'],
                labels: ['Y'],
                backgroundColor: '#9D9D9D',
                /* barFillColors: [
                 '#39FF08 #555',      // from light gray to dark gray (top to bottom)
                 '#555 #aaa black' // from dark day, through light gray, to black
                 ]*/
                /* */
                barColors:
                        function (row, series, type) {
                            if (type === 'bar') {
                                var blue = Math.ceil(255 * row.y / this.ymax);
                                return 'rgb(43,200,' + blue + ')';
                            } else {
                                return '#000';
                            }
                        }
            });
            /*
             * Play with this code and it'll update in the panel opposite.
             *
             * Why not try some of the options above?
             */
            new Morris.Donut({
                element: 'donut_resultado<?php echo $row_pregunta['ID']; ?>',
                data: array_js, //array de los datos
                backgroundColor: '#9D9D9D',
                labelColor: '#060',
                colors: array_colores
                        /*formatter: function (x) { return x + "%"} // da la funcion en porcentajes y no en absolutos
                         */
            });

            array_js = null;

        </script>


        <?php
    }
    ?>
    <?php if ($quants  >0) {?>
        <p>&nbsp;</p>
      <button id="botonvergrafica" class="btn btn-secondary btn-block" ><?= _("Ver grafica barras") ?></button>
      <button  id="botonverdonut" class="btn btn-secondary btn-block" ><?= _("Ver grafica circular") ?></button>
      <p>&nbsp;</p>
    <?php  }   ?>

    <?php
  } else {
    ?>	<h1><?= _("No hay ninguna pregunta planteada para este debate") ?><?php
    }
    ?>
    <script type="text/javascript">


                    $('#botonvergrafica').click(function() {
                        var elem = $('.resultados_donut');
                        var elem2 = $('.resultados_grafica');
                        var elem3 = $('#botonverdonut');
                        var elem4 = $('#botonvergrafica');
                        <!--elem.hide ("blind", { directi on: "vertical" }, 1000); -->
                        <!--elem2.show("blind", { direction: "vertical" }, 1000); -->
                        <!--elem4.hide("blind", { direction: "vertical" }, 1000);-->
                        <!--elem3.show("blind", { direction: "vertical" }, 1000);-->
                        elem.hide();
                        elem2. show();
                        elem4.hide();
                        elem3.show();
                      });

                    $('#botonverdonut').click(function() {
                      var elem = $('.resultados_donut');
                      var elem2 = $('.resultados_grafica');
                      var elem3 = $('#botonverdonut');
                      var elem4 = $('#botonvergrafica');
                      <!--elem.show ("blind", { direction: "vertical" }, 1000); -->
                      <!--elem2.hide("blind", { direction: "vertical" }, 1000); -->
                      <!--elem3.hide ("blind", { direction: "vertical" }, 1000);-->
                       <!--elem4.show ("blind", { direction        : "vertical" }, 1000);-->
                      elem.show();
                      elem2.hide();
                      elem3.hide();
                      elem4.show();
                    });
    </script>
<?php } ?>

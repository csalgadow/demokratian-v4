<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que mediante ajax ejecuta la inclusión/cambio de un voto en los debates
*/
require_once("../private/config/config.inc.php");
require_once("../private/inc_web/conexion.php");

//include('../inc_web/seguri.php');
include_once("../private/basicos_php/basico.php");
include_once('../private/basicos_php/errores.php');

//Defino mi variable mensaje



if (ISSET($_POST["enviar_voto"])) {

    $id_votacion = fn_filtro_numerico($con, $variables['idvot']);
    $id_pregunta = fn_filtro_numerico($con, $_POST['id_pregunta']);
    $voto = fn_filtro($con, $_POST['voto']);
    $id_usuario = fn_filtro($con, $_POST['IDST']);

    $query_votoa = mysqli_query($con, "SELECT  ID  FROM $tbn14  where id_pregunta = '" . $id_pregunta . "' and id_votante ='" . $id_usuario . "' ");
    if ($row_votoa = mysqli_fetch_array($query_votoa)) {
        mysqli_field_seek($query_votoa, 0);

        do {
            $id_voto = $row_votoa['ID'];
            $sSQL = "UPDATE $tbn14 SET  voto=\"$voto\" WHERE ID='$id_voto'";
            mysqli_query($con, $sSQL) or die("Imposible modificar pagina");
        } while ($row_votoa = mysqli_fetch_array($query_votoa));
    } else {

        $insql = "insert into $tbn14 (id_votante, 	id_pregunta, 	id_votacion, voto) values (  \"$id_usuario\",  \"$id_pregunta\", \"$id_votacion\", \"$voto\")";
        $mens = "";
        $resulta = db_query($con, $insql, $mens);
    }
}
?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que mediante ajax ejecuta la inclusión de un nuevo comentario en el debate
*/
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 1 Jul 2000 05:00:00 GMT"); // Fecha en el pasado

require_once("../private/config/config.inc.php");
require_once("../private/inc_web/conexion.php");

//include('../inc_web/seguri.php');
include_once("../private/basicos_php/basico.php");
include_once('../private/basicos_php/errores.php');

$msg = fn_filtro($con, $_POST['msg']);

$idvot = fn_filtro_numerico($con, $variables['idvot']);
$id_user = fn_filtro_numerico($con, $_POST['IDST']);
$mi_estado = fn_filtro($con, $_POST['mi_estado']);


if (!empty($msg)) {

    //Inserto el mensaje al tabla
    $time = time();
    $insql = "INSERT INTO $tbn12 (comentario,fecha,id_usuario,id_votacion,estado) VALUES ('" . $msg . "','" . $time . "','" . $id_user . "','" . $idvot . "','" . $mi_estado . "')";
    $mens = "UPSSS!!!!<br/>" . _("esto es embarazoso, hay un error al incluir su comentario en este debate");
    $resulta = db_query($con, $insql, $mens);

    echo "OK#".encrypt_url('vota_debate/wall/idvot='.$idvot,$clave_encriptacion)."#".$idvot."#".$clave_encriptacion;
}



?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###                                                         ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo con la ficha de los datos de un votante-usuario 
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}

if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 2;
include('../private/inc_web/nivel_acceso.php');

$id = fn_filtro_numerico($con, $variables['id']);
$result = mysqli_query($con, "SELECT ID, id_provincia ,nombre_usuario,apellido_usuario ,nivel_usuario,nivel_acceso,correo_usuario,	nif ,id_ccaa,tipo_votante ,usuario ,fecha_ultima ,bloqueo ,razon_bloqueo, imagen_pequena FROM $tbn9 where id=$id");
$row = mysqli_fetch_row($result);
?>


<div class="modal-header">
       <h5 class="modal-title"><?= _("Ficha del usuario") ?> "<?php echo $row[2]; ?>"</h5>
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
   </div>
   <div class="modal-body">
           <!---texto --->


                <table width="100%" >
                                <tr>
                                    <th width="34%"><?= _("Nombre y Apellidos") ?>: </td>
                                    <td><?php echo $row[2]; ?> <?php echo $row[3]; ?></td>
                                    <td colspan="2" rowspan="4" align="center"><?php if ($row[14] == "peq_usuario.jpg" or $row[14] == "") { ?><img src="../temas/<?php echo "$tema_web"; ?>/images/user.png" width="70" height="70" /><?php } else { ?><img src="<?php echo $upload_user; ?>/<?php echo"$row[14]"; ?>" alt="<?php echo"$row[1]"; ?> <?php echo"$row[4]"; ?>" width="70" height="70"  /> <?php } ?> </td>
                                </tr>
                                <tr>
                                    <th><?= _("Correp eletronico") ?></td>
                                    <td><?php echo $row[6]; ?></td>
                                </tr>
                                <tr>
                                    <th><?= _("Usuario") ?></td>
                                    <td><?php echo $row[10]; ?></td>
                                </tr>
                                <tr>
                                    <th><?= _("Nif") ?></td>
                                    <td><?php echo $row[7]; ?></td>
                                </tr>
                                <tr>
                                    <th>ID <?= _("CCAA") ?> :</td>
                                    <td width="22%"><?php echo $row[8]; ?></td>
                                  </tr>
                                    <tr>
                                    <td width="23%"><?= _("Provincia") ?> :</td>
                                    <td width="21%"><?php echo $row[1]; ?></td>
                                </tr>
                                <tr>
                                    <th><?= _("Nivel del usuario") ?></td>
                                    <td><?php echo $row[4]; ?></td>
                                  </tr>
                                  <tr>
                                    <td><?= _("Nivel de acceso") ?> </td>
                                    <td><?php echo $row[5]; ?></td>
                                </tr>
                                <tr>
                                    <th><?= _("Tipo Votante") ?></td>
                                    <td>
                                        <?php
                                        if ($row[9] == 1) {
                                            echo _("Socio");
                                        } elseif ($row[9] == 2) {
                                            echo _("Simpatizante Verificado");
                                        } elseif ($row[9] == 3) {
                                            echo _("Simpatizante");
                                        };
                                        ?>
                                    </td>
                                    <th><?= _("Ultimo acceso") ?></td>
                                    <td rowspan="2"><?php echo $row[11]; ?></td>
                                </tr>
                                <tr>
                                    <th><?= _("Bloqueado") ?></td>
                                    <td><?php echo $row[12]; ?></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><?= _("Razon bloqueo") ?> :<?php echo $row[13]; ?></td>
                                </tr>
                            </table>


        </div>         <!-- /modal-content -->
<?php } ?>

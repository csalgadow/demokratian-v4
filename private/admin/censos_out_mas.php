<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que permite pasar de tipo un tipo  de votante a otro de forma masiva
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 1;
include('../private/inc_web/nivel_acceso.php');

$fecha = date("Y-m-d H:i:s");
$mensaje1 = "";
$inmsg = "";
$mensaje1a = "";
?>

                    <!--Comiezo-->
                    <div class="card-header"> <h1 class="card-title"><?= _("MODIFICACIONES O BAJAS DE AFILIADOS/ SIMPATIZANTES DE FORMA MASIVA") ?></h1> </div>


<div class="card-body">

                    <?php
                    if ($inmsg != "") {
                        echo "<div class=\"alert alert-success\">" . _("¡¡¡Error!!!"). " ".  _("No se han  registrado estos usuarios") . ".</div>";
                        echo "$inmsg";
                    }
                    ?>
                    <?php
                    if (isset($mensaje1)) {
                        echo "$mensaje1";
                    }
                    ?>

                    <?php
                    if (isset($_POST['emails'])) {


                        $emails = strip_tags($_POST['emails'], '<\n><br/>');
                        $emailarray = explode("\n", $emails);

                        $numrows = count($emailarray);
                        for ($i = 0; $i < $numrows; $i++) {
                            $emailarray1 = explode(',', $emailarray[$i]);
                            $nif_val = fn_filtro($con, trim($emailarray1[1]));
                            $nif_val =trim ($nif_val);//quitamos espacios en blanco
                            //$user_val = fn_filtro($con, trim($emailarray1[1]));

                            if($emailarray1[0]!="" or $emailarray1[1]!=""){
                            $esCorrecto="OK";
                            if ($emailarray1[0]!="") {
                              $emailarray1[0] =trim ($emailarray1[0]);//quitamos espacios en blanco
                                if (!filter_var($emailarray1[0], FILTER_VALIDATE_EMAIL)) {
                                $error = "error";
                                $inmsg .= _("La direccion") . " " . $emailarray1[0] . " " . _("es errónea") . " <br/>";
                                $esCorrecto="ERROR";
                                } else{
                                  $esCorrecto="OK";
                                }
                              }
                            }else{
                              $esCorrecto="ERROR";
                            }
                            //else if ($nif_val == '') {
                            //    $inmsg .= _("ERROR , no hay nif en") . " " . $emailarray1[0] . " <br/>";
                            //}

                            if ($esCorrecto =="OK") {

                                /////miramos si el usaurio  existe
                                $user_mail = fn_filtro($con, trim($emailarray1[0]));
                                $usuarios_consulta = mysqli_query($con, "SELECT ID FROM $tbn9 WHERE nif='" . $nif_val . "' or correo_usuario='" . $user_mail . "' ") or die(mysqli_error($con));

                                $total_encontrados = mysqli_num_rows($usuarios_consulta);
                                $row = mysqli_fetch_row($usuarios_consulta);

                                mysqli_free_result($usuarios_consulta);

                                if ($total_encontrados != 0) {

                                    if ($_POST['tipo_usuario'] == 8) {
                                        ///borramos la lista de votaciones donde ha participado
                                        $borrado_usuario_x_votacion = "DELETE FROM $tbn2 WHERE id_usuario=" . $row[0] . " ";
                                        $mens = "ERROR en el borrado de usuario_x_votacion";
                                        $result_usuario_x_votacion = db_query($con, $borrado_usuario_x_votacion, $mens);
                                        /* if (!$result_usuario_x_votacion) {
                                          $mens_error.="<div class=\"alert alert-danger\">".$mens."</div>";
                                          }
                                          else{
                                          $mens_ok.="Borrados la relacion de votantes de esta votación <br/>";
                                          } */

                                        //borramos debate_comentario con esta id
                                        $borrado_debate_comentario = "DELETE FROM $tbn12 WHERE id_votante=" . $row[0] . " ";
                                        $mens = "ERROR en el borrado de debate comentario";
                                        $result_borrado_debate_comentario = db_query($con, $borrado_debate_comentario, $mens);

                                        /* if (!$result_borrado_debate_comentario) {
                                          $mens_error.="<div class=\"alert alert-danger\">".$mens."</div>";
                                          }
                                          else{
                                          $mens_ok.="Borrados los comentarios de este debate <br/>";
                                          } */

                                        //borramos debate_votos con esta id
                                        $borrado_debate_votos = "DELETE FROM $tbn14 WHERE id_votante=" . $row[0] . "";
                                        $mens = "ERROR en el borrado de debate votos";
                                        $result_borrado_debate_votos = db_query($con, $borrado_debate_votos, $mens);

                                        /* if (!$result_borrado_debate_votos) {
                                          $mens_error.="<div class=\"alert alert-danger\">".$mens."</div>";
                                          }
                                          else{
                                          $mens_ok.="Borrados los votos de este debate <br/>";
                                          } */


                                        //borramos la relacion entre votantes y modifciaciones de los administradores
                                        $borrado_votantes_x_admin = "DELETE FROM $tbn17 WHERE id_votante=" . $row[0] . " ";
                                        $mens = "ERROR en el borrado de votantes_x_admin";
                                        $result_votantes_x_admin = db_query($con, $borrado_votantes_x_admin, $mens);
                                        //miramos que imagen tiene el usuario para borrarla
                                        $sql_img = "SELECT id,imagen_pequena FROM $tbn9 WHERE ID=" . $val . " ";
                                        $result_img = mysqli_query($con, $sql_img);
                                        $row_img = mysqli_fetch_row($result_img);

                                        if ($row_img[1] != "peq_usuario.jpg") {
                                            $thumb_photo_exists = $upload_user . "/" . $row_img[1];
                                            if (file_exists($thumb_photo_exists)) {
                                                unlink($thumb_photo_exists);
                                            }
                                        }

                                        // y por ultimo borramos el votante
                                        $sSQL = "DELETE FROM $tbn9 WHERE id=" . $row[0] . "";
                                        $mens = "Error al eliminar los  datos de votantes";
                                        $result = db_query($con, $sSQL, $mens);
                                        if (!$result) {
                                            echo "<div class=\"alert alert-danger\">
                                                 <strong> UPSSS!!!!<br/>" . _("esto es embarazoso, hay un error en la conexion con la base de datos") . " </strong> </div><strong>";
                                        }
                                        if ($result) {
                                            $mensaje1 .= " ->" . (" Usuario") . ": " . trim($emailarray1[1]) . " . " . _("Correo") . ": " . trim($emailarray1[0]) . " " . _("Nif") . ": " . trim($emailarray1[2]) . " " . _("¡¡¡ELIMINADO!!!") . "<br/>";
                                            $fecha = date("Y-m-d H:i:s");
                                            $accion = "3"; //borrar votante
                                            $insql = "insert into $tbn17 (id_votante,id_admin,accion,fecha ) values (  \"" . $row[0] . "\",\"" . $_SESSION['ID'] . "\",   \"$accion\", \"$fecha\" )";
                                            $mens = "error incluir quien ha borrado el votantes";
                                            $resultado = db_query($con, $insql, $mens);
                                        }
                                        // borrado
                                    } else {  ///modificamos
                                        $tipo_usuario = fn_filtro($con, $_POST['tipo_usuario']);
                                        $sSQL = "UPDATE $tbn9 SET   tipo_votante=" . $tipo_usuario . "  WHERE id=" . $row[0] . "";
                                        $mens = "Error al modificar datos de votantes";
                                        $result = db_query($con, $sSQL, $mens);
                                        //mysqli_query($con, $sSQL) or die("Imposible modificar pagina");
                                        if (!$result) {
                                            echo "<div class=\"alert alert-danger\">
                                                 <strong> UPSSS!!!!<br/>" . _("esto es embarazoso, hay un error en la conexion con la base de datos") . " </strong> </div><strong>";
                                        }
                                        if ($result) {

                                            $mensaje1 .= " -> " . _("Usuario con NIF") . ": " . trim($emailarray1[1]) . " ". _("y/o") ." " . _("Correo") . ": " . trim($emailarray1[0]) . "  " . _("Convertido en usuario tipo") . " --> " . $tipo_usuario . "<br/>";

                                            $accion = "2"; //modificar votante
                                            $insql = "insert into $tbn17 (id_votante,id_admin,accion,fecha ) values (  \"" . $row[0] . "\",\"" . $_SESSION['ID'] . "\",   \"$accion\", \"$fecha\" )";
                                            $mens = "error incluir quien ha borrado el votante";
                                            $resultado = db_query($con, $insql, $mens);
                                        }
                                    }
                                } else {

                                    $inmsg .= _("Usuario con NIF") . " " . trim($emailarray1[1]) . " ". _("y/o") ."  " . _("con correo") . " " . trim($emailarray1[0]) . "   " . _("NO está registrado") . ".";
                                }
                            }

                            //$mensaje1a.="<br /><p>Estos usuarios han sido modificados:</p><br/>";
                        }
                    }
                    ?>

                  <p>  <?= _("Para dar de baja a suscriptores a su lista, escriba la dirección de correo y el nif separados por una coma (,) Use una línea para cada votante") ?> </p><p> <?= _("Si quiere buscar solo por correo o por nif, deje el espacio en blanco, pero ponga siempre la coma") ?>.</p><br />
                    <br/>
                    <?= _("Ejemplo") ?>: <br/>
                    <ul>
                        carlos@yahoo.es, 384955l <br/>
                        juan@iespana.es,384752z <br/>
                         , 00418493J<br/>
                         pdrito@gmail.com , <br/>
                         <br/>
                        etc
                    </ul>
                    <?php if ($inmsg != "") { ?> <br/><div class="alert alert-warning"> <?php echo "$inmsg"; ?> </div> <?php } ?>

                    <?php if ($mensaje1 != "") { ?>
                        <br/><div class="alert alert-success"> <?= _("Estos usuarios han sido modificados") ?> : </div>
                        <div class="alert alert-success">
                            <?php echo "$mensaje1"; ?></div><?php } ?>

                    <?php if ($mensaje1a != "") { ?>
                        <br/><div class="alert alert-success"> <?= _("Estos usuarios han sido añadidos") ?>:</div>
                        <div class="alert alert-success">
                            <?php echo "$mensaje1a"; ?></div><?php } ?>

                    <form Action="<?php $_SERVER['PHP_SELF'] ?>" Method=POST class="well form-horizontal">


                        <p><?= _("¿Que dessea hacer?, dar de baja o convertir en simpatizante si son socios") ?></p>
                        <p><?= _("Convertir en") ?> </p>
                        <table width="100%">
                            <tr>
                                <td width="15%" height="29">
                                    <input name="tipo_usuario" type="radio" id="tipo_usuario_0" value="1" /></td>
                                <td width="15%"><input name="tipo_usuario" type="radio" id="tipo_usuario_1" value="2" checked="checked" />
                                    (2)</td>
                                <td width="15%"><input name="tipo_usuario" type="radio" id="tipo_usuario_3" value="3" />
                                    (3)</td>
                                <td width="15%"><input name="tipo_usuario" type="radio" id="tipo_usuario_2" value="9" /></td>
                                <td width="40%"><input name="tipo_usuario" type="radio" id="tipo_usuario_4" value="8" /></td>
                            </tr>
                            <tr>
                                <td height="22"><?= _("Socio") ?></td>
                                <td><?= _("simpatizante verificado") ?> </td>
                                <td> <?= _("simpatizantes") ?> </td>
                                <td><?= _("Dar de Baja") ?></td>
                                <td><?= _("Borrar (No se recomienda en absoluto, Puede dar problemas en las votaciones en las que ha participado, si no està seguro, use dar de baja)") ?></td>
                            </tr>
                        </table>
                        <p>
                            <label for="emails"></label>
                            <textarea name="emails" cols="80" rows="30" id="emails" class="form-control" ></textarea>
                        </p>
                        <p>&nbsp;</p>   <input type="submit" value="<?= _("Modificar/borrar votantes de la lista") ?>"  class="btn btn-primary btn-block" > <p>&nbsp;</p>

                    </form>
</div>
                    <!--Final-->
                    <?php } ?>

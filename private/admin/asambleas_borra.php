<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que borra las asambleas, votaciones y la relación de usuarios con esta asamblea.
* @todo ver borramos todos los votos de las votaciones que hemos borrado.
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 2;
include('../private/inc_web/nivel_acceso.php');

$id = fn_filtro_numerico($con, $variables['id']);
?>



                    <!--Comiezo-->
                    <?php
                    //$result = mysqli_query($con, "SELECT * FROM $tbn4 where ID=$id");
                    $insql = "SELECT * FROM $tbn4 where ID=$id";
                    $mens = _("ERROR en la consulta de").  'admin/asambleas_borra.php' .   _("linea"); '49' ;
                    $result = db_query($con, $insql, $mens);
                    $row = mysqli_fetch_row($result);
                    ?>
                    <div class="card-header"> <h1 class="card-title"><?= _("BORRADA ASAMBLEA O GRUPO DE TRABAJO") ?></h1> </div>


                    <div class="card-body">

                        <div class="form-group row">
                            <div  class="col-sm-3 "><?= _("Nombre") ?> </div>

                            <div class="col-sm-9"><?php echo "$row[2]"; ?>

                            </div></div>

                        <div class="form-group row">
                            <div class="col-sm-3 "><?= _("TIPO") ?></div>

                            <div class="col-sm-9">
                                <?php
                                if ($row[1] == 1) {

                                    echo _("Provincial") . "  |   $row[4]";
                                } else if ($row[1] == 2) {

                                    echo _("Autonomico") . " |  $row[5]";
                                } else if ($row[1] == 3) {
                                    echo _("Estatal");
                                }
                                ?>
                            </div></div>

                        <div class="form-group row">
                            <div class="col-sm-3"><?= _("Forma de acceso al grupo") ?></div>

                            <div class="col-sm-9">
                                <?php
                                if ($row[7] == 1) {
                                    echo _("Abierto (no necesita validación para suscribirse)");
                                } else if ($row[7] == 2) {
                                    _("Administrado (Necesita que los administradores validen el acceso)");
                                } else {

                                    echo _("Cerrado (Solo los administradores añaden usuarios)");
                                }
                                ?>

                            </div></div>

                        <div class="form-group row">
                            <div class="col-sm-3 "><?= _("Activo") ?>  </div>

                            <div class="col-sm-9">
                                <?php
                                if ($row[8] == 2) {
                                    echo _("No");
                                } else {

                                    echo _("Si");
                                }
                                ?>


                            </div></div>





                        <div class="form-group row">
                            <div class="col-sm-3"><?= _("TIPO DE VOTANTE") ?></div>
                            <div class="col-sm-9">
                                <?php
                                if ($row[10] == 5) {
                                    echo _("Abierta");
                                } else if ($row[10] == 2) {
                                    echo _("Socios y simpatizantes verificados");
                                } else if ($row[10] == 3) {
                                    echo _("Socios y simpatizantes");
                                } else {
                                    echo _("Solo socios");
                                }
                                ?>

                            </div></div>

                        <div class="form-group row">

                            <div class="col-sm-3 control-label" ><?= _("Texto") ?></div>
                            <div class="col-sm-9">

                                <?php echo "$row[6]"; ?>

                            </div></div>
                        <p>&nbsp;</p>

                        <?php
                      //  $borrado1 = mysqli_query($con, "DELETE FROM $tbn4 WHERE id=" . $id . "") or die("No puedo ejecutar la instrucción de borrado SQL query");
                        $insql_borrado1 = "DELETE FROM $tbn4 WHERE id=" . $id . "";
                          $mens = _("ERROR en la consulta de").  'admin/asambleas_borra.php' .   _("linea"); '149' ;
                          $result_borrado1 = db_query($con, $insql_borrado1, $mens);
                        if (!$result_borrado1) {
                            echo "<div class=\"alert alert-warning\">" . _("Error en el borrado asamblea o grupo de trabajo") . "</div>";
                        } elseif (mysqli_affected_rows($con) == 0) {
                            echo "<div class=\"alert alert-warning\">" . _("Error en el borrado asamblea o grupo de trabajo") . "</div>";
                        } else {
                            echo "<div class=\"alert alert-success\">" . _("Borrada asamblea o grupo de trabajo") . "</div>";
                        }



                        //$borrado2 = mysqli_query($con, "DELETE FROM $tbn6 WHERE id_grupo_trabajo=$id") or die("No puedo ejecutar la instrucción de borrado SQL query");
                        $insql_borrado2 = "DELETE FROM $tbn6 WHERE id_grupo_trabajo=$id";
                          $mens = _("ERROR en la consulta de").  'admin/asambleas_borra.php' .   _("linea"); '163' ;
                          $result_borrado2 = db_query($con, $insql_borrado2, $mens);
                        if (!$result_borrado2) {
                            echo "<div class=\"alert alert-warning\">" . _("No se ha podido borrar relacion de usuarios de esta asamblea o grupo de trabajo") . ".</div>";
                        } elseif (mysqli_affected_rows($con) == 0) {
                            echo "<div class=\"alert alert-warning\">" . _("No se ha podido borrar a relacion de usuarios de esta asamblea o grupo de trabajo") . "</div>";
                        } else {
                            echo "<div class=\"alert alert-success\">" . _("Borrada relacion de usuarios de esta asamblea o grupo de trabajo") . "</div>";
                        }


                      //  $borrado3 = mysqli_query($con, "DELETE FROM $tbn1 WHERE id_grupo_trabajo=$id") or die("No puedo ejecutar la instrucción de borrado SQL query");
                        $insql_borrado3 = "DELETE FROM $tbn1 WHERE id_grupo_trabajo=$id";
                          $mens = _("ERROR en la consulta de").  'admin/asambleas_borra.php' .   _("linea"); '175' ;
                          $result_borrado3 = db_query($con, $insql_borrado3, $mens);
                        if (!$result_borrado3) {
                            echo "<div class=\"alert alert-warning\">" . _("No se han podido borrar  votaciones de esta asamblea o grupo de trabajo") . "</div>";
                        } elseif (mysqli_affected_rows($con) == 0) {
                            echo "<div class=\"alert alert-warning\">" . _("No se han podido borrar  votaciones de esta asamblea o grupo de trabajo") . "</div>";
                        } else {
                            echo "<div class=\"alert alert-success\">" . _("Borradas votaciones de esta asamblea o grupo de trabajo") . "</div>";
                        }


                        /* faltaria borrar los candidatos de esta votacion y ademas todos los votos y otras cosas relacionadas */
                        ?>




                </div>
            </div>
            <!--Final-->

<?php } ?>

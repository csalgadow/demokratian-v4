<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###                                                         ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo con el menú de opciones para gestionar las distintas acciones de la votación
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 9;
include('../private/inc_web/nivel_acceso.php');

$id= fn_filtro_numerico($con,$variables['id']);


if (ISSET($_POST["desactivar"])) {

    $activo = "no";
    //$id = fn_filtro_numerico($con, $_POST["id"]);
    $sSQL = "UPDATE $tbn1 SET activa=\"$activo\" WHERE id=" . $id . "";
    mysqli_query($con, $sSQL) or die("Imposible modificar");
}

if (ISSET($_POST["activar"])) {
    $activo = "si";
    //$id = fn_filtro_numerico($con, $_POST["id"]);
    $sSQL = "UPDATE $tbn1 SET activa=\"$activo\" WHERE id=" . $id . "";

    mysqli_query($con, $sSQL) or die("Imposible modificar");
}

//////////////////////////////
if (ISSET($_POST["desactivar_resultados"])) {
    $activar = "no";
    //$id = fn_filtro_numerico($con, $_POST["id"]);
    $sSQL = "UPDATE $tbn1 SET activos_resultados=\"$activar\" WHERE id=" . $id . "";
    mysqli_query($con, $sSQL) or die("Imposible modificar");
}

if (ISSET($_POST["activar_resultados"])) {
    $activar = "si";
  //  $id = fn_filtro_numerico($con, $_POST["id"]);
    $sSQL = "UPDATE $tbn1 SET activos_resultados=\"$activar\" WHERE id=" . $id . "";
    mysqli_query($con, $sSQL) or die("Imposible modificar");
}
?>
<div class="card-header"> <h1 class="card-title"><?= _("Panel de gestion de votación") ?></h1> </div>


<div class="card-body">

                    <!--Comiezo-->

                    <?php
                    $result = mysqli_query($con, "SELECT  ID , nombre_votacion,tipo, tipo_votante, activa, activos_resultados,seguridad,interventor,demarcacion,fecha_com, fecha_fin,encripta FROM $tbn1 where id=$id");
                    $row = mysqli_fetch_row($result);
                    ?>
                    <div class="row">
                      <div class="col-sm-3 "><br/><?= _("Nombre votación") ?></div>
                        <div class="col-sm-9"> <h2> <?php echo "$row[1]"; ?></h2></div>
                    </div>

                    <div class="row">
                        <div class="col-sm-3">
                            <?= _("Tipo de votación") ?>
                        </div>
                        <div class="col-sm-9">
                            <?php
                            if ($row[2] == 1) {
                                echo _("primarias");
                            } else if ($row[2] == 2) {
                                echo _("VUT");
                            } else if ($row[2] == 3) {
                                echo _("Encuesta");
                            } else if ($row[2] == 4) {
                                echo _("Debate");
                            } else if ($row[2] == 5) {
                                echo _("ENCUESTA con poderación del voto según tipo votante");
                            } else if ($row[2] == 6) {
                              echo _("ENCUESTA con poderación del voto según tipo votante")." ". ("BINARIA");
                            } else if ($row[2] == 7) {
                              echo _("ENCUESTA")." ". ("BINARIA");
                            }
                            ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-3">
                            <?= _("Fechas") ?>
                        </div>
                        <div class="col-sm-3">

                            <?php
                            echo $fecha_ini_ver = date("d-m-Y  / H:i", strtotime($row[9]));

// $hora_ini_ver=date("H:i", strtotime($row[9]));
                            ?>
                        </div>
                        <div class="col-sm-3">
                            <?php
                            echo $fecha_fin_ver = date("d-m-Y / H:i", strtotime($row[10]));
                            //$hora_fin_ver=date("H:i", strtotime($row[6]));
                            ?>

                        </div>
                        <div class="col-sm-3">
                            <?php
                            $hoy = strtotime(date('Y-m-d H:i'));
                            $fecha_ini = strtotime($row[7]);
                            $fecha_fin = strtotime($row[8]);
                            if ($fecha_ini <= $hoy && $fecha_fin >= $hoy) {
                                ?>
                                <?= _("fuera fecha") ?>
                                <?php
                            } else {
                                ?>
                                <?= _("en fecha") ?>
                            <?php }
                            ?>
                        </div>
                    </div>
                    <!---->
                    <div class="row">
                        <div class="col-sm-3">
                            <?= _("Tipo de votante") ?>
                        </div>
                        <div class="col-sm-9">
                            <?php
                            if ($row[3] == 1) {
                                echo _("solo socio");
                            } else if ($row[3] == 2) {
                                echo _("socio y simpatizante verificado");
                            } else if ($row[3] == 3) {
                                echo _("socio y simpatizante");
                            } else if ($row[3] == 5) {
                                echo _("abierta");
                            }
                            ?>
                        </div>
                    </div>
                    <p>&nbsp;</p>
                    <!---->
                    <div class="row">
                        <div class="col-sm-3"><?= _("VOTACIÓN") ?></div>
                        <div class="col-sm-3">
                            <a href="admin.php?c=<?php echo encrypt_url('admin/votacion/id='.$row[0].'&acc=modifika',$clave_encriptacion); ?>" class="btn btn-primary btn-block"><?= _("modificar") ?></a>
                        </div>
                        <div class="col-sm-3">
                            <a href="admin.php?c=<?php echo encrypt_url('admin/votacion_borrar/id='.$row[0],$clave_encriptacion); ?>" class="btn btn-danger btn-block" onClick="return borrarevento()"><span class="glyphicon glyphicon-warning-sign"></span> <?= _("BORRAR") ?></a>
                        </div>
                        <div class="col-sm-3">
                            <form id="form_<?php echo encrypt_url($row[0],$clave_encriptacion); ?>" name="form_<?php echo encrypt_url($row[0],$clave_encriptacion); ?>" method="post" action="">
                                <?php if ($row[4] == "no") { ?>
                                    <input type="submit" name="activar" id="activar" value="<?= _("activar") ?>" class="btn btn-primary btn-block"/>
                                <?php } else { ?>
                                    <input type="submit" name="desactivar" id="desactivar" value="<?= _("desactivar") ?>"  class="btn btn-success btn-block" />
                                <?php } ?>
                            </form>
                        </div>
                    </div>
                    <p>&nbsp;</p>
                    <!---->
                    <?php if ($row[2] == 6 or $row[2] == 7 ) {
                    }else{
                      ?>
                    <div class="row">

                        <?php if ($row[2] == 4) { ?>
                            <div class="col-sm-3"><?= _("Preguntas") ?></div>
                            <div class="col-sm-3">
                                <a href="admin.php?c=<?php echo encrypt_url('admin/preguntas/idvot='.$row[0],$clave_encriptacion); ?>" class="btn btn-primary btn-block"><?= _("Añadir preguntas") ?></a>
                            </div>
                        <?php } else { ?>
                            <div class="col-sm-3"><?= _("CANDIDATOS U OPCIONES") ?></div>
                            <div class="col-sm-3">
                                <a href="admin.php?c=<?php echo encrypt_url('admin/candidatos/idvot='.$row[0],$clave_encriptacion); ?>" class="btn btn-primary btn-block" ><?= _("añadir candidatos") ?></a>
                            </div>
                        <?php } ?>

                        <div class="col-sm-3">
                            <?php if ($row[2] == 4) { ?>
                                <a href="admin.php?c=<?php echo encrypt_url('admin/preguntas_busq1/idvot='.$row[0],$clave_encriptacion); ?>"  class="btn btn-primary btn-block "><?= _("Modificar preguntas") ?></a>
                            <?php } else { ?>
                                <a href="admin.php?c=<?php echo encrypt_url('admin/candidatos_busq1/idvot='.$row[0],$clave_encriptacion); ?>" class="btn btn-primary btn-block"><?= _("Gestionar candidatos") ?></a>
                            <?php } ?>
                        </div>
                        <div class="col-sm-3">

                            <?php

                            if ($row[2] == 4) {

                            } else {
                                ?>
                                <?php
                                $result_vot = mysqli_query($con, "SELECT id,activo FROM $tbn22  where id_votacion=$id");
                                $quants = mysqli_num_rows($result_vot);
                                //miramos si hay resultados , si los hay , entonces toca modificar
                                if ($quants == 1) {
                                    $row_c = mysqli_fetch_array($result_vot);
                                    if ($row[1] == 0) {
                                        $estado = _("activo");
                                    } else {
                                        $estado = _("inactivo");
                                    }
                                    ?>
                                    <a href="admin.php?c=<?php echo encrypt_url('admin/candidatos_pagina/idvot='.$row[0],$clave_encriptacion); ?>"  class="btn btn-success btn-block "><?= _("Modificar pagina externa") ?> / <?php echo $estado; ?></a>
                                    <?php
                                } else if ($quants > 1) {
                                    echo _("Hay un error de algun tipo en la base de datos");
                                } else {
                                    ?>
                                    <a href="admin.php?c=<?php echo encrypt_url('admin/candidatos_pagina/idvot='.$row[0],$clave_encriptacion); ?>"  class="btn btn-warning btn-block "><?= _("Candidatos pagina externa") ?></a>
                                    <?php
                                }
                            }

                            ?>
                        </div>
                    </div>
                    <?php } ?>

                    <!--fin gestion candidatos/preguntas -->
                    <p>&nbsp;</p>
                    <!---->

                    <?php if ($row[6] == 3 or $row[6] == 4 or $row[7] == "si") { ?>
                        <div class="row">
                            <div class="col-sm-3"><?= _("INTERVENTORES") ?></div>
                            <div class="col-sm-6"></div>
                            <div class="col-sm-3">
                                <a href="admin.php?c=<?php echo encrypt_url('admin/interventor_busq1/idvot='.$row[0],$clave_encriptacion); ?>" class="btn btn-primary btn-block"><?= _("Gestionar interventores") ?></a>
                            </div>
                        </div>
                        <p>&nbsp;</p>
                    <?php } ?>



                    <!---->
                    <?php if ($row[2] != 4) { ?>
                        <div class="row">
                            <div class="col-sm-3"> <?= _("RESULTADOS") ?></div>
                            <div class="col-sm-3">
                                <?php if ($row[2] == 1) { ?>
                                  <a href="javascript:void(0);" data-href="aux_modal.php?c=<?php echo encrypt_url('vota_orden/resultados_primarias/idvot='.$row[0],$clave_encriptacion) ?>" class="openextraLargeModal btn btn-warning btn-block"><?= _("Lanzar recuento") ?></a>
                                    <?php
                                } else if ($row[2] == 2) {
                                    if ($_SESSION['usuario_nivel'] == 0) { ?>
                                      <a href="javascript:void(0);" data-href="aux_modal.php?c=<?php echo encrypt_url('vota_vut/lanza_recuento/idvot='.$row[0],$clave_encriptacion); ?>" class="openextraLargeModal btn btn-warning btn-block"><?=_("Realizar recuento")?> </a>
                                  <?php  }
                                }else  if ($row[2] == 3 or $row[2] == 6 or $row[2] == 5 or $row[2] == 7) {
                                    ?>
                                    <a href="javascript:void(0);" data-href="aux_modal.php?c=<?php echo encrypt_url('vota_encuesta/resultados/idvot='.$row[0],$clave_encriptacion); ?>" class="openextraLargeModal btn btn-warning btn-block" ><?= _("Lanzar recuento") ?></a>
                                    <?php
                                } /*else  if ($row[2] == 5 or $row[2] == 7) {
                                    ?>
                                    <a href="javascript:void(0);" data-href="aux_modal.php?c=<?php echo encrypt_url('vota_encuesta_ponderada/resultados/idvot='.$row[0],$clave_encriptacion); ?>" class="openextraLargeModal btn btn-warning btn-block" ><?= _("Lanzar recuento") ?></a>
                                    <?php
                                }*/
                                ?>
                            </div>

                            <div class="col-sm-3">
                                <?php if ($row[2] == 1) { ?>
                                  <a href="javascript:void(0);" data-href="aux_modal.php?c=<?php echo encrypt_url('vota_orden/resultados_listar/idvot='.$row[0],$clave_encriptacion); ?>" class="openextraLargeModal btn btn-warning btn-block"><?= _("Listar votos") ?></a>
                                    <?php
                                }
                                if ($row[2] == 2) { ?>
                                  <a href="javascript:void(0);" data-href="aux_modal.php?c=<?php echo encrypt_url('vota_vut/resultados_listar/idvot='.$row[0],$clave_encriptacion); ?>" class="openextraLargeModal btn btn-warning btn-block" ><?= _("Listar votos") ?></a>
                                    <?php
                                }
                                 if ($row[2] == 3 or $row[2] == 6 or $row[2] == 5 or $row[2] == 7) { ?>
                                  <a href="javascript:void(0);" data-href="aux_modal.php?c=<?php echo encrypt_url('vota_encuesta/resultados_listar/idvot='.$row[0],$clave_encriptacion); ?>" class="openextraLargeModal btn btn-warning btn-block" ><?= _("Listar votos") ?></a>
                                    <?php
                                }
                              /*  if ($row[2] == 5 or $row[2] == 7) { ?>
                                 <a href="javascript:void(0);" data-href="aux_modal.php?c=<?php echo encrypt_url('vota_encuesta_ponderada/resultados_listar/idvot='.$row[0],$clave_encriptacion); ?>" class="openextraLargeModal btn btn-warning btn-block" ><?= _("Listar votos") ?></a>
                                   <?php
                               }*/
                                ?>
                            </div>
                            <div class="col-sm-3">
                                <form id="form_<?php echo $row[0]; ?>" name="form_<?php echo $row[0]; ?>" method="post" action="">


                                    <?php
                                    if ($row[2] == 4) {
                                        echo " <h3><span class=\"label label-info btn-block\">" . _("debate") . "<spam></h3>";
                                    } else {
                                        $id_encriptada = md5($id);
                                        if ($row[5] == "no") {
                                            if ($row[2] == 1 or $row[2] == 3 or $row[2] == 5 or $row[2] == 6 or $row[2] == 7) {
                                                $nombre_fichero1 = $FileRec . $id_encriptada . "_list.php";
                                                $nombre_fichero2 = $FileRec . $id_encriptada. ".php";
                                                if (file_exists($nombre_fichero1) and file_exists($nombre_fichero2)) {
                                                    //comprobamos que se pueden activar los resultados porque se han lanzado los recuentos y el listado
                                                    $disable = "";
                                                } else {
                                                    $disable = "disabled=\"disabled\"";
                                                }
                                            } else if ($row[2] == 2) {
                                                $nombre_fichero1 = $FileRec . $id_encriptada . "_list.php";
                                                if (file_exists($nombre_fichero1)) {
                                                    //comprobamos que se pueden activar los resultados porque se han lanzado los recuentos y el listado
                                                    $disable = "";
                                                } else {
                                                    $disable = "disabled=\"disabled\"";
                                                }
                                            }
                                            ?>
                                            <input type="submit" name="activar_resultados" id="activar_resultados" class="btn btn-primary btn-block" value="<?= _("activar resultados") ?>" <?php echo $disable; ?> />
                                        <?php } else {
                                            ?>
                                            <input type="submit" name="desactivar_resultados" id="desactivar_resultados"class="btn btn-success btn-block" value="<?= _("desactivar resultados") ?>" />

                                            <?php
                                        }
                                    }
                                    ?>
                                </form>
                            </div>

                        </div>
                        <p>&nbsp;</p>
                        <!---->

                        <?php if ($row[2] == 1 or $row[2] == 2) { ?>

                            <?php if ($row[11] == "si") { ?>
                                <div class="row">
                                    <div class="col-sm-3"><?= _("ENCRIPTACIÓN") ?></div>
                                    <div class="col-sm-3">

                                        <?php // if ($row[2] == 1) {  ?>
                                        <?php if ($_SESSION['usuario_nivel'] == "0") { ?>
                                            <a href="admin.php?c=<?php echo encrypt_url('admin/decodifica_rsa/idvot='.$row[0],$clave_encriptacion); ?>"   class="btn btn-warning btn-block"><?= _("comprobar rsa") ?></a>
                                        </div>
                                        <div class="col-sm-3">
                                            <a href="admin.php?c=<?php echo encrypt_url('admin/codificador/idvot='.$row[0],$clave_encriptacion); ?>" class="btn btn-primary btn-block"><?= _("Gestionar CODIFICADORES") ?></a>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>


                                <p>&nbsp;</p>
                                <?php
                            }
                        }
                        ?>

                        <!---->
                        <div class="row">
                            <div class="col-sm-3"><?= _("CENSOS") ?></div>
                            <div class="col-sm-3">
                                <?php if ($row[8] == "1") { ?>
                                    <a href="admin.php?c=<?php echo encrypt_url('admin/votantes_listado_multi/idvot='.$row[0].'&cen=com&lit=si',$clave_encriptacion); ?>" class="btn btn-primary btn-block"><?= _("Censo completo") ?></a>
                                <?php } else { ?>
                                    <a href="admin.php?c=<?php echo encrypt_url('admin/votantes_listado_multi/idvot='.$row[0].'&cen=com&lit=no',$clave_encriptacion); ?>" class="btn btn-primary btn-block"><?= _("Censo completo") ?></a>
                                <?php } ?>
                            </div>
                            <div class="col-sm-3">

                                <?php if ($row[8] == "1") { ?>
                                    <a href="admin.php?c=<?php echo encrypt_url('admin/votantes_listado_multi/idvot='.$row[0].'&cen=fal&lit=si',$clave_encriptacion); ?>" class="btn btn-primary btn-block  pull-right"><?= _("Faltan") ?></a>
                                <?php } else { ?>
                                    <a href="admin.php?c=<?php echo encrypt_url('admin/votantes_listado_multi/idvot='.$row[0].'&cen=fal&lit=no',$clave_encriptacion); ?>" class="btn btn-primary btn-block  pull-right"><?= _("Faltan") ?></a>
                                <?php } ?>
                            </div>
                            <div class="col-sm-3">

                                <?php if ($row[8] == "1") { ?>
                                    <a href="admin.php?c=<?php echo encrypt_url('admin/votantes_listado_multi/idvot='.$row[0].'&cen=stn&lit=si',$clave_encriptacion); ?>" class="btn btn-primary btn-block"><?= _("Ya ha votado") ?></a>
                                <?php } else { ?>
                                    <a href="admin.php?c=<?php echo encrypt_url('admin/votantes_listado_multi/idvot='.$row[0].'&cen=stn&lit=no',$clave_encriptacion); ?>" class="btn btn-primary btn-block"><?= _("Ya ha votado") ?></a>
                                <?php } ?>
                            </div>
                        </div>
                        <p>&nbsp;</p>
                        <!---->
                        <div class="row">
                            <div class="col-sm-3"> </div>
                            <div class="col-sm-3">

                                <?php if ($row[8] == "1") { ?>
                                    <a href="admin.php?c=<?php echo encrypt_url('admin/votantes_listado_multi/idvot='.$row[0].'&cen=cong&lit=si',$clave_encriptacion); ?>" class="btn btn-primary btn-block"><?= _("presencial/congreso") ?></a>
                                <?php } else { ?>
                                    <a href="admin.php?c=<?php echo encrypt_url('admin/votantes_listado_multi/idvot='.$row[0].'&cen=cong&lit=no',$clave_encriptacion); ?>" class="btn btn-primary btn-block"><?= _("presencial/congreso") ?></a>
                                <?php } ?>
                            </div>
                            <div class="col-sm-3">

                                <a href="admin.php?c=<?php echo encrypt_url('admin/participacion/idvot='.$row[0],$clave_encriptacion); ?>" class= "btn btn-success btn-block "><?= _("participación") ?></a>
                            </div></div>
                    <?php } ?>
                    <!--Final-->

</div>


        <script src="assets/js/admin_borravotacion.js" ></script>

        <script type="text/javascript"> // recargar la pagina despues cerrar el modal
        $('#extraLargeModal').on('hidden.bs.modal', function () {
            location.reload();
        });
      </script>
      <!--   <script type="text/javascript">
       $(document).ready(function(){
                $("#modal-resultados").on('hidden.bs.modal', function () {
                        location.reload();
                });
            });
        </script>
        <script type="text/javascript">// recargar la pagina despues cerrar el modal ya que el anterior no funciona
            $('#modal-resultados').on('shown.bs.modal', function () {
            location.reload(true);
        })
      </script> -->

<?php } ?>

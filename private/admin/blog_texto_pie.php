<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###                                                         ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que permite añadir un texto en el pie de página
*/
if (!isset($cargaA)) {
    $cargaA =false;
    exit;
}
if ($cargaA!="OK") {
    exit;
} else {
    $nivel_acceso = 11;
    include('../private/inc_web/nivel_acceso.php');
    if ($_SESSION['admin_blog']==1) {
        include("../private/basicos_php/modifika_config.php");

        $file = "../private/config/config-blog.inc.php";


        if (isset($_POST["modifika_activo_pie"])) {
            $com_string = "active_pie_pagina = ";
            $find = $com_string . fn_filtro_nodb($_POST['valor']);
            $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
            find_replace($find, $replace, $file, $case_insensitive = true);
        }

        if (isset($_POST["modifika_texto_pie"])) {
            $com_string = "texto_pie_pagina = \"";
            $find = $com_string . fn_filtro_nodb($_POST['valor']);
            $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
            find_replace($find, $replace, $file, $case_insensitive = true);
        }




        include($file); ?>

                    <!--Comiezo-->
                    <div class="card-header"> <h1 class="card-title"><?= _("Texto del pie de página") ?></h1> </div>


                    <div class="card-body">

                        <form name="form1" method="post" action="">

                          <div class="form-group row">
                            <div class="col-sm-3"><?= _("Visualizar")?> <?= _("Texto del pie de página") ?></div>

                          <div class="col-sm-3"><?php
                          if ($active_pie_pagina == true) {
                              echo _("Activo"); ?>
                              <input name="valor" type="hidden" id="valor" value="true">
                            <?php
                              $check1 = "checked=\"CHECKED\"";
                          } else {
                              echo _("Inactivo");?>
                              <input name="valor" type="hidden" id="valor" value="false">
                            <?php
                              $check2 = "checked=\"CHECKED\"";
                          } ?>

                          </div>
                          <div class="col-sm-3">
                              <label>
                              <input name="direccion_general" type="radio" id="direccion_general_0" value="true" <?php
                              if (isset($check1)) {
                                  echo $check1;
                              } ?> >
                              <?= _("Activo") ?></label>
                              <label>
                                <input type="radio" name="direccion_general" id="direccion_general_1" value="false" <?php
                                if (isset($check2)) {
                                    echo $check2;
                                } ?>>
                                <?= _("Inactivo") ?></label>
                              </div>

                              <div class="col-sm-3">
                                <input type="submit" name="modifika_activo_pie" id="modifika_activo_pie" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                              </div>

                                </div>
                              </form>


                              <div class="form-group row">
                                <div class="col-sm-3"><?= _("Texto del pie de página") ?></div>
                            <div class="col-sm-9">
                            <?php echo $texto_pie_pagina; ?>
                          </div>
                        </div>
                        <form name="form1" method="post" action="">
                        <div class="form-group row">
                          <div class="col-sm-3"></div>
                              <div class="col-sm-9">
                                    <input name="direccion_general" type="text" autofocus required class="form-control" id="direccion_general"  value="<?php echo $texto_pie_pagina; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $texto_pie_pagina; ?>" >

                                  </div>
                                </div>

                                <div class="form-group row">
                                  <div class="col-sm-3"></div>
                                  <div class="col-sm-9">
                                    <input type="submit" name="modifika_texto_pie" id="modifika_texto_pie" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                  </div>
                                </div>
                                </form>






                    </table>

</div>
                    <!--Final-->

<?php
    }
} ?>

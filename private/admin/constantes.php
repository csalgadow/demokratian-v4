<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###                                                         ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo para configurar las variables generales de la aplicación ( modifica el archivo config.inc.php)
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 1;
include('../private/inc_web/nivel_acceso.php');


include("../private/basicos_php/modifika_config.php");

$file = "../private/config/config.inc.php";


if (ISSET($_POST["modifika_url"])) {
    $com_string = "url_vot = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}

if (ISSET($_POST["modifika_nombre"])) {
    $com_string = "nombre_sistema = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}


if (ISSET($_POST["modifika_web"])) {
    $com_string = "nombre_web = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_tema"])) {
    $com_string = "tema_web = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}



if (ISSET($_POST["modifika_auth"])) {
    $com_string = "cfg_autenticacion_solo_local = ";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_versiones"])) {
    $com_string = "info_versiones = ";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}

if (ISSET($_POST["modifika_zonaHoraria"])) {
    $com_string = "timezone = ";
    if ($_POST['valor'] == "false" and $_POST['direccion_general'] == "true") {
        $dato_viejo = fn_filtro_nodb($_POST['valor']);
        $nuevo_dato = "\"" . fn_filtro_nodb($_POST['timezone']) . "\"";
    } else {
        if ($_POST['direccion_general'] == "false") {
            $dato_viejo = "\"" . fn_filtro_nodb($_POST['valor']) . "\"";
            $nuevo_dato = fn_filtro_nodb($_POST['direccion_general']);
        } else {
            $dato_viejo = "\"" . fn_filtro_nodb($_POST['valor']) . "\"";
            $nuevo_dato = "\"" . fn_filtro_nodb($_POST['timezone']) . "\"";
        }
    }
    $find = $com_string . $dato_viejo;
    $replace = $com_string . $nuevo_dato;
    find_replace($find, $replace, $file, $case_insensitive = true);
}


if (ISSET($_POST["modifika_locale"])) {
    $com_string = "defaul_lang = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}


if (ISSET($_POST["modifika_recaptcha"])) {
    $com_string = "reCaptcha = ";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}

if (ISSET($_POST["modifika_tipo_recaptcha"])) {
    $com_string = "tipo_reCaptcha = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}

if (ISSET($_POST["modifika_site_key"])) {
    $com_string = "reCAPTCHA_site_key = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}

if (ISSET($_POST["modifika_secret_key"])) {
    $com_string = "reCAPTCHA_secret_key = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}

if (ISSET($_POST["modifika_sensibilidad"])) {
    $com_string = "sensibilidad = ";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}

if (ISSET($_POST["modifika_tiempo_session"])) {
    $com_string = "tiempo_session = ";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}

if (ISSET($_POST["modifika_votoPonderado"])) {
    $com_string = "votoPonderado = ";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}

include($file);
?>



                    <!--Comiezo-->
                    <div class="card-header"> <h1 class="card-title"><?= _("Constantes de configuración") ?></h1> </div>


                    <div class="card-body">

                    <table width="100%" border="0"  class="table table-striped">
                        <tr>
                            <th width="39%" scope="row">&nbsp;</th>
                            <td width="5%">&nbsp;</td>
                            <td width="23%">&nbsp;</td>
                            <td width="33%">&nbsp;</td>
                        </tr>

                        <tr>
                            <th scope="row">&nbsp;</th>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Url de la web de la votación,(ojo  sin barra final)") ?></th>
                            <td>&nbsp;</td>
                            <td><?php echo $url_vot; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="text" autofocus required class="form-control" id="direccion_general"  value="<?php echo $url_vot; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $url_vot; ?>" >
                                    <input type="submit" name="modifika_url" id="modifika_url" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>

                        <tr>
                            <th scope="row"><?= _("Nombre del sitio web") ?></th>
                            <td>&nbsp;</td>
                            <td><?php echo $nombre_web; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="text" autofocus required class="form-control" id="direccion_general"  value="<?php echo $nombre_web; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $nombre_web; ?>" >
                                    <input type="submit" name="modifika_web" id="modifika_web" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row"> <?= _("Nombre del tema (carpeta donde se encuentra)") ?></th>
                            <td>&nbsp;</td>
                            <td><?php echo $tema_web; ?></td>
                            <td><form name="form1" method="post" action="">

                                    <?php
                                    $carpeta = "temas"; //ruta actual
                                    $lista = "";
                                    if (is_dir($carpeta)) {
                                        if ($dir = opendir($carpeta)) {
                                            while (($archivo = readdir($dir)) !== false) {
                                                if ($archivo != '.' && $archivo != '..' && $archivo != '.htaccess' && $archivo != 'index.html' && $archivo != 'index.php') {

                                                    if ($archivo == $tema_web) {
                                                        $check = "selected=\"selected\" ";
                                                    } else {
                                                        $check = "";
                                                    }
                                                    $lista .= "<option value=\"" . $archivo . "\" $check > " . $archivo . "</option>";
                                                }
                                            }
                                            closedir($dir);
                                        }
                                    }
                                    ?>
                                    <select name="direccion_general" class="form-control custom-select"  id="direccion_general" >
                                        <?php echo "$lista"; ?>
                                    </select>


                                    <input name="valor" type="hidden" id="valor" value="<?php echo $tema_web; ?>" >
                                    <input type="submit" name="modifika_tema" id="modifika_tema" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form>



                            </td>
                        </tr>
                        <tr>
                            <th scope="row">&nbsp;</th>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Autenfificación federada") ?></th>
                            <td>&nbsp;</td>
                            <td><?php
                                if ($cfg_autenticacion_solo_local == true) {
                                    echo "Solo Local";
                                    $check3 = "checked=\"CHECKED\"";
                                } else {
                                    echo"Autentificación federada";
                                    $check4 = "checked=\"CHECKED\"";
                                }
                                ?></td>
                            <td><form name="form1" method="post" action="">
                                    <label>
                                        <input name="direccion_general" type="radio" id="direccion_general_0" value="false" <?php
                                        if (isset($check4)) {
                                            echo $check4;
                                        }
                                        ?>>
                                        <?= _("Autentificación federada") ?></label>

                                    <label>
                                        <input type="radio" name="direccion_general" id="direccion_general_1" value="true" <?php
                                        if (isset($check3)) {
                                            echo $check3;
                                        }
                                        ?>>
                                        <?= _("Solo Local") ?></label>

                                    <input name="valor" type="hidden" id="valor" value="<?php
                                    if ($cfg_autenticacion_solo_local == true) {
                                        echo "true";
                                    } else {
                                        echo"false";
                                    }
                                    ?>" >
                                    <input type="submit" name="modifika_auth" id="modifika_auth" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row"><a name="avisos"></a><?= _("Avisos de actualizaciones") ?></th>
                            <td>&nbsp;</td>
                            <td><?php
                                if ($info_versiones == true) {
                                    echo _("Habilitado");
                                    $check15 = "checked=\"CHECKED\"";
                                } else {
                                    echo _("Deshabilitado");
                                    $check16 = "checked=\"CHECKED\"";
                                }
                                ?></td>
                            <td><form name="form1" method="post" action="">
                                    <label>
                                        <input name="direccion_general" type="radio" id="direccion_general_0" value="false" <?php
                                        if (isset($check16)) {
                                            echo $check16;
                                        }
                                        ?>>
                                        <?= _("Deshabilitado") ?></label>

                                    <label>
                                        <input type="radio" name="direccion_general" id="direccion_general_1" value="true" <?php
                                        if (isset($check15)) {
                                            echo $check15;
                                        }
                                        ?>>
                                        <?= _("Habilitado") ?></label>

                                    <input name="valor" type="hidden" id="valor" value="<?php
                                    if ($info_versiones == true) {
                                        echo "true";
                                    } else {
                                        echo"false";
                                    }
                                    ?>" >
                                    <input type="submit" name="modifika_versiones" id="modifika_versiones" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form>
                            </td>
                        </tr>

                        <tr>
                            <th scope="row"><?= _("Zona horaria") ?></th>
                            <td>&nbsp;</td>
                            <td><?php
                                if ($timezone == false) {
                                    echo _("Hora del servidor OK");
                                    $check17 = "checked=\"CHECKED\"";
                                } else {
                                    echo _("Zona horaria modificada a:") . " " . $timezone;
                                    $check18 = "checked=\"CHECKED\"";
                                }
                                ?></td>
                            <td><form name="form1" method="post" action="">
                                    <label>
                                        <input name="direccion_general" type="radio" id="direccion_general_0" value="false" <?php
                                        if (isset($check17)) {
                                            echo $check17;
                                        }
                                        ?>>
                                        <?= _("Usar hora del servidor") ?></label>

                                    <label>
                                        <input type="radio" name="direccion_general" id="direccion_general_1" value="true" <?php
                                        if (isset($check18)) {
                                            echo $check18;
                                        }
                                        ?>>
                                        <?= _("Usar otra zona horaria") ?></label>

                                    <input name="valor" type="hidden" id="valor" value="<?php
                                    if ($timezone == false) {
                                        echo "false";
                                    } else {
                                        echo $timezone;
                                    }
                                    ?>" >


                                    <?php
                                    $timeZONE = $timezone; // cambiamos el nombre de la variable ya que la usamos tambien en el bucle
                                    $regions = array(
                                        'Europa' => DateTimeZone::EUROPE,
                                        'America' => DateTimeZone::AMERICA,
                                        'Africa' => DateTimeZone::AFRICA,
                                        'Antartica' => DateTimeZone::ANTARCTICA,
                                        'Asia' => DateTimeZone::ASIA,
                                        'Atlantica' => DateTimeZone::ATLANTIC,
                                        'Indian' => DateTimeZone::INDIAN,
                                        'Pacific' => DateTimeZone::PACIFIC
                                    );
                                    $timezones = array();
                                    foreach ($regions as $name => $mask) {
                                        $zones = DateTimeZone::listIdentifiers($mask);
                                        foreach ($zones as $timezone) {
                                            // Lets sample the time there right now
                                            $time = new DateTime(NULL, new DateTimeZone($timezone));
                                            // Us dumb Americans can't handle millitary time
                                            $ampm = $time->format('H') > 12 ? ' (' . $time->format('g:i a') . ')' : '';
                                            // Remove region name and add a sample time
                                            $timezones[$name][$timezone] = substr($timezone, strlen($name) + 1) . ' - ' . $time->format('H:i') . $ampm;
                                        }
                                    }
// View
                                    echo '<select id="timezone" name="timezone" class="form-control custom-select"> ';
                                    foreach ($timezones as $region => $list) {
                                        print '<optgroup label="' . $region . '">' . "\n";
                                        foreach ($list as $timezone => $name) {
                                            if ($timeZONE == false) {
                                                if ($timezone == "Europe/Madrid") {
                                                    $checked = "selected";
                                                } else {
                                                    $checked = "";
                                                }
                                            } else {
                                                if ($timezone == $timeZONE) {
                                                    $checked = "selected";
                                                } else {
                                                    $checked = "";
                                                }
                                            }
                                            echo '<option value="' . $timezone . '" ' . $checked . '>' . $name . '</option>' . "\n";
                                        }
                                        echo '<optgroup>' . "\n";
                                    }
                                    echo '</select>';
                                    ?>

                                    <input type="submit" name="modifika_zonaHoraria" id="modifika_zonaHoraria" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Idioma por defecto de la web") ?></th>
                            <td>&nbsp;</td>
                            <td><?php echo $defaul_lang; ?></td>
                            <td>
                                <form name="form1" method="post" action="">

                                    <?php
                                    $carpeta_locale = "locale"; //ruta actual
                                    $lista_locale = "";
                                    if (is_dir($carpeta_locale)) {
                                        if ($dir_locale = opendir($carpeta_locale)) {
                                            while (($archivo_locale = readdir($dir_locale)) !== false) {
                                                if ($archivo_locale != '.' && $archivo_locale != '..' && $archivo_locale != '.htaccess' && $archivo_locale != 'index.html' && $archivo_locale != 'index.php' && $archivo_locale != 'messages.po' && $archivo_locale != 'messages.pot') {

                                                    if ($archivo_locale == $defaul_lang) {
                                                        $check_locale = "selected=\"selected\" ";
                                                    } else {
                                                        $check_locale = "";
                                                    }
                                                    $lista_locale .= "<option value=\"" . $archivo_locale . "\" $check_locale > " . $archivo_locale . "</option>";
                                                }
                                            }
                                            closedir($dir_locale);
                                        }
                                    }
                                    ?>
                                    <select name="direccion_general" class="form-control custom-select"  id="direccion_general" >
                                        <?php echo "$lista_locale"; ?>

                                    </select>


                                    <input name="valor" type="hidden" id="valor" value="<?php echo $defaul_lang; ?>" >
                                    <input type="submit" name="modifika_locale" id="modifika_locale" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Tiempo de caducidad de la sesión") ?></th>
                            <td>&nbsp;</td>
                            <td>
                                <?php echo $tiempo_session; ?>
                            </td>
                            <td>
                                <form name="form1" method="post" action="">
                                    <input name="direccion_general" type="number" min="60" max=" " step="60"  autofocus required class="form-control" id="tiempo_session"  value="<?php echo $tiempo_session; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $tiempo_session; ?>" >
                                    <input type="submit" name="modifika_tiempo_session" id="modifika_tiempo_session" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                    <p class="text-info"> <?= _("en segundos") ?></p>
                                </form>
                            </td>
                        </tr>

                        <tr>
                            <th scope="row"><?= _("Permitir votaciones ponderadas por tipos de usuarios") ?></th>
                            <td>&nbsp;</td>
                            <td><?php
                                if ($votoPonderado == true) {
                                    echo _("Habilitado");
                                    $check19 = "checked=\"CHECKED\"";
                                } else {
                                    echo _("Deshabilitado");
                                    $check20 = "checked=\"CHECKED\"";
                                }
                                ?></td>
                            <td><form name="form1" method="post" action="">
                              <label>
                                  <input type="radio" name="direccion_general" id="direccion_general_1" value="true" <?php
                                  if (isset($check19)) {
                                      echo $check19;
                                  }
                                  ?>>
                                  <?= _("Permitir") ?></label>

                                    <label>
                                        <input name="direccion_general" type="radio" id="direccion_general_0" value="false" <?php
                                        if (isset($check20)) {
                                            echo $check20;
                                        }
                                        ?>>
                                        <?= _("No permitir") ?></label>

                                    <input name="valor" type="hidden" id="valor" value="<?php
                                    if ($votoPonderado == true) {
                                        echo "true";
                                    } else {
                                        echo"false";
                                    }
                                    ?>" >
                                    <input type="submit" name="modifika_votoPonderado" id="modifika_votoPonderado" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form>
                            </td>
                        </tr>

                        <tr>
                            <th scope="row">&nbsp;</th>
                            <td>&nbsp;</td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>

                        <tr>
                            <th scope="row"><?= _("Configuración recaptcha") ?></th>
                            <td>&nbsp;</td>
                            <td>

                                <a href="https://www.google.com/recaptcha/intro/v3.html" target="_blank"><?= _("Mas información sobre") ?> recaptcha</a>

                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>


                        <tr>
                            <th scope="row"><?= _("Activar") ?> recaptcha</th>
                            <td>&nbsp;</td>
                            <td>
                                <?php
                                if ($reCaptcha == true) {
                                    echo _("Habilitado");
                                    $check18 = "checked=\"CHECKED\"";
                                } else {
                                    echo _("Deshabilitado");
                                    $check17 = "checked=\"CHECKED\"";
                                }
                                ?></td>
                            <td><form name="form1" method="post" action="">
                                    <label>
                                        <input name="direccion_general" type="radio" id="direccion_general_0" value="false" <?php
                                        if (isset($check17)) {
                                            echo $check17;
                                        }
                                        ?>>
                                        <?= _("Deshabilitado") ?></label>

                                    <label>
                                        <input type="radio" name="direccion_general" id="direccion_general_1" value="true" <?php
                                        if (isset($check18)) {
                                            echo $check18;
                                        }
                                        ?>>
                                        <?= _("Habilitado") ?></label>

                                    <input name="valor" type="hidden" id="valor" value="<?php
                                    if ($reCaptcha == true) {
                                        echo "true";
                                    } else {
                                        echo"false";
                                    }
                                    ?>" >
                                    <input type="submit" name="modifika_recaptcha" id="modifika_recaptcha" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form>
                            </td>
                        </tr>


                        <tr>
                            <th scope="row"><?= _("Tipo") ?> recaptcha</th>
                            <td>&nbsp;</td>
                            <td>
                                <?php
                                if ($tipo_reCaptcha == "reCAPTCHA_v2") {
                                    echo _("Seleccionado"). " reCAPTCHA v2";
                                    $check19 = "checked=\"CHECKED\"";
                                } else {
                                    echo _("Seleccionado"). " reCAPTCHA v3";
                                    $check20 = "checked= \"CHECKED\"";
                                }
                                ?></td>
                            <td><form name="form1" method="post" action="">
                                    <label>
                                        <input name="direccion_general" type="radio" id="direccion_general_0" value="reCAPTCHA_v2" <?php
                                        if (isset($check19)) {
                                            echo $check19;
                                        }
                                        ?>> reCAPTCHA V2</label>
                                        <?php if (function_exists('curl_init')== true){?>
                                    <label>
                                        <input type="radio" name="direccion_general" id="direccion_general_1" value="reCAPTCHA_v3" <?php
                                        if (isset($check20)) {
                                            echo $check20;
                                        }
                                        ?>> reCAPTCHA V3</label>
                                        <?php
                                        }else{
                                        ?>
                                        <div class="alert alert-warning"> <?=("No tiene instalado el módulo CURL en su servidor, no puede usar reCAPTCHA V3 si no lo activa")?> </div>
                                        <?php
                                        }
                                        ?>
                                    <input name="valor" type="hidden" id="valor" value="<?php
                                    if ($tipo_reCaptcha == "reCAPTCHA_v2") {
                                        echo "reCAPTCHA_v2";
                                    } else {
                                        echo "reCAPTCHA_v3";
                                    }
                                    ?>" >
                                    <?php if (function_exists('curl_init')== true){?>
                                    <input type="submit" name="modifika_tipo_recaptcha" id="modifika_tipo_recaptcha" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                    <?php  }   ?>
                                </form>
                            </td>
                        </tr>


                        <tr>
                            <th scope="row"><?= _("Clave publica") ?></th>
                            <td>&nbsp;</td>
                            <td>
                                <?php echo $reCAPTCHA_site_key; ?>
                            </td>
                            <td>
                                <form name="form1" method="post" action="">
                                    <input name="direccion_general" type="text" autofocus required class="form-control" id="direccion_general"  value="<?php echo $reCAPTCHA_site_key; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $reCAPTCHA_site_key; ?>" >
                                    <input type="submit" name="modifika_site_key" id="modifika_site_key" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form>
                            </td>
                        </tr>

                        <tr>
                            <th scope="row"><?= _("Clave privada") ?></th>
                            <td>&nbsp;</td>
                            <td>
                                <?php echo $reCAPTCHA_secret_key; ?>
                            </td>
                            <td>
                                <form name="form1" method="post" action="">
                                    <input name="direccion_general" type="text" autofocus required class="form-control" id="direccion_general"  value="<?php echo $reCAPTCHA_secret_key; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $reCAPTCHA_secret_key; ?>" >
                                    <input type="submit" name="modifika_secret_key" id="modifika_secret_key" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form>
                            </td>
                        </tr>
                        <?php
                          if (function_exists('curl_init')== true){
                      ?>
                      <tr>
                          <th scope="row"><?= _("Sensibilidad reCAPTCHA V3") ?></th>
                          <td>&nbsp;</td>
                          <td>
                              <?php echo $sensibilidad; ?>
                          </td>
                          <td>
                              <form name="form1" method="post" action="">
                                  <input name="direccion_general" type="number" autofocus required class="form-control" id="direccion_general"  value="<?php echo $sensibilidad; ?>" min="0.1" max="0.9" step="0.1" >
                                  <input name="valor" type="hidden" id="valor" value="<?php echo $sensibilidad; ?>" >
                                  <input type="submit" name="modifika_sensibilidad" id="modifika_sensibilidad" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                  <p class="text-info"> <?= _("Solo para la versión reCAPTCHA V3 (de 0.1 a 0.9)") ?></p>
                              </form>
                          </td>
                      </tr>

                        <?php
                      }
                      ?>

                    </table>

</div>
                    <!--Final-->

<?php } ?>

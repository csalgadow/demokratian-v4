<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###                                                         ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo con el formulario para la búsqueda de usuarios y mirar qué votaciones tienen o han creado
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 1;
include('../private/inc_web/nivel_acceso.php');


$lista1 = "";
?>



                    <!--Comiezo-->
          <div class="card-header"> <h1 class="card-title"><?= _("BUSCAR  EN EL CENSO DE USUARIOS PARA VER QUE VOTACIONES TIENEN") ?></h1> </div>


<div class="card-body">

                    <p>&nbsp;</p>
                    <!---->

                    <form id="form1" name="form1" method="post" action="admin.php?c=<?php echo encrypt_url('admin/usuarios_busq_mis/s=SD',$clave_encriptacion) ?>" class="well form-horizontal">

                        <div class="form-group row">
                            <label for="nombre_usuario" class="col-sm-3 control-label"><?= _("Nombre") ?> </label>

                            <div class="col-sm-9">
                                <input type="text" name="nombre_usuario" id="nombre_usuario" class="form-control" /></div></div>

                        <div class="form-group row">
                            <label for="correo_electronico" class="col-sm-3 control-label"> <?= _("Correo electronico") ?></label>

                            <div class="col-sm-9">
                                <input name="correo_electronico" type="email" id="correo_electronico" class="form-control"/>

                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="nif" class="col-sm-3 control-label"> <?= _("Nif") ?></label>

                            <div class="col-sm-5">
                                <input name="nif" type="text" id="nif" class="form-control" />

                            </div>
                        </div>


                        <?php if ($es_municipal == false) { ?>
                            <div class="form-group row">
                                <label for="provincia" class="col-sm-3 control-label"> <?= _("Provincia") ?></label>

                                <div class="col-sm-5">   <?php
                                    if ($_SESSION['nivel_usu'] == 2) {
                                        // listar para meter en una lista del cuestionario buscador

                                        $lista1 .= '<option value="%"  > Todas </option>';
                                        $options = "select DISTINCT id, provincia from $tbn8  where especial=0 order by ID";
                                        $resulta = mysqli_query($con, $options) or die("error: " . mysqli_error());

                                        while ($listrows = mysqli_fetch_array($resulta)) {
                                            $id_pro = $listrows['id'];
                                            $name1 = $listrows['provincia'];

                                            $lista1 .= "<option value=\"$id_pro\"  > $name1</option>";
                                        }
                                        ?>
                                        <h5> <?= _("Escoja una Provincia") ?> </h5>
                                        <select name="provincia" class="form-control custom-select" id="provincia" >
                                            <?php echo "$lista1"; ?>
                                        </select>
                                        <?php
                                    } else if ($_SESSION['nivel_usu'] == 3) {

                                        $options = "select DISTINCT id, provincia from $tbn8  where id_ccaa=" . $_SESSION['id_ccaa_usu'] . "  order by ID";
                                        $resulta = mysqli_query($con, $options) or die("error: " . mysqli_error());

                                        while ($listrows = mysqli_fetch_array($resulta)) {
                                            $id_pro = $listrows['id'];
                                            $name1 = $listrows['provincia'];

                                            $lista1 .= "<option value=\"$id_pro\" > $name1</option>";
                                        }
                                        ?>
                                        <h5> <?= _("Escoja una Provincia") ?> </h5>
                                        <select name="provincia" class="form-control custom-select" id="provincia" >
                                            <?php echo "$lista1"; ?>
                                        </select>
                                        <?php
                                    } else {

                                        $result2 = mysqli_query($con, "SELECT id_provincia FROM $tbn5 where id_usuario=" . $_SESSION['ID']);
                                        $quants2 = mysqli_num_rows($result2);

                                        if ($quants2 != 0) {

                                            while ($listrows2 = mysqli_fetch_array($result2)) {

                                                $name2 = $listrows2['id_provincia'];
                                                $optiones = mysqli_query($con, "SELECT  provincia FROM $tbn8 where ID=$name2");
                                                $row_prov = mysqli_fetch_row($optiones);

                                                $lista1 .= "    <label><input  type=\"radio\" name=\"provincia\" value=\"$name2\"    id=\"provincia\" /> " . $row_prov[0] . "</label> <br/>";
                                            }
                                            echo "$lista1";
                                        } else {
                                            echo _("No tiene asignadas provincias, no podra crear votación");
                                        }
                                    }
                                    ?>


                                </div>
                            </div>
                        <?php } ?>
                        <div class="form-group row">
                            <div class="col-sm-3"> <?= _("Nivel administrativo") ?></div>

                            <div class="col-sm-9">
                              <label for="nivel_admin_0" class="control-label">
                                <input name="nivel_admin" type="radio" id="nivel_admin_0" value="vacio" checked="checked" />
                                <?= _("Todos") ?></label>
                                <br/>
                                <label for="nivel_admin_1" class="control-label">
                                <input type="radio" name="nivel_admin" value="2" id="nivel_admin_1" />
                                <?= _("Administrador") ?></label>
                                <br/>

                                <?php if ($es_municipal == false) { ?>
                                  <label for="nivel_admin_2" class="control-label">
                                    <input type="radio" name="nivel_admin" value="3" id="nivel_admin_2" />
                                    <?= _("Administrador") ?> <?= _("CCAA") ?></label><br/>
                                    <label for="nivel_admin_3" class="control-label">
                                    <input type="radio" name="nivel_admin" value="4" id="nivel_admin_3" />
                                    <?= _("Administrador") ?> <?= _("Provincia") ?></label><br/>
                                    <label for="nivel_admin_4" class="control-label">
                                    <input type="radio" name="nivel_admin" value="5" id="nivel_admin_4" />
                                    <?= _("Administrador") ?> <?= _("Grupo Provincial") ?></label><br/>
                                <?php } ?>
                                <label for="nivel_admin_1" class="control-label">
                                <input type="radio" name="nivel_admin" value="6" id="nivel_admin_5" />
                                <?= _("Administrador") ?> <?= _("Grupo") ?> <?php
                                if ($es_municipal == false) {
                                    echo "Estatal";
                                }
                                ?></label>
                            </div></div>
                        <p>
                            <input type="submit" name="buscar" id="buscar" value="<?= _("Buscar") ?>"  class="btn btn-primary btn-block" />
                        </p>
                        <p>&nbsp; </p>
                        <p>
                            <input name="busqueda" type="hidden" id="busqueda" value="si">
                        </p>

                    </form>


</div>
                    <!--Final-->

<?php } ?>

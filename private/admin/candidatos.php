<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que crea los candidatos u opciones que se pueden votar
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 9;
include('../private/inc_web/nivel_acceso.php');


$idvot = fn_filtro($con, $variables['idvot']);
$fecha = date("Y-m-d h:i:s");

if (ISSET($_POST["add_candidato"])) {

$error_vut=false;
    $texto = fn_filtro_editor($con, $_POST['texto']);
    $nombre_cand = fn_filtro($con, $_POST['nombre_cand']);
    $provincia = fn_filtro($con, $_POST['provincia']);
    $sexo = fn_filtro($con, $_POST['sexo']);
    $id_votacion = $idvot;
    if (isset($_POST['numero_id_vut'])) {
        $numero_id_vut = fn_filtro($con, $_POST['numero_id_vut']);
        $sql_vut = "SELECT id, id_vut FROM $tbn7 WHERE  id_votacion ='$idvot' and id_vut= $numero_id_vut ORDER BY id_vut DESC";

        $result_vut = mysqli_query($con, $sql_vut);

        $row_vut = mysqli_fetch_row($result_vut); //comprobamos si existe ya el
        if ($row_vut) {
          $error_vut=true;
        }

    } else {
        $numero_id_vut = 0;
    }

    $nombre_usuario = $_SESSION['ID'];

if ($error_vut==false){
    $inres = "insert into $tbn7 (nombre_usuario, id_provincia, 	texto, sexo,id_votacion,anadido, fecha_anadido,id_vut) values (  \"$nombre_cand\",  \"$provincia\", \"$texto\", \"$sexo\", \"$id_votacion\", \"$nombre_usuario\", \"$fecha\", \"$numero_id_vut\")";
    $mens = "Mensaje error en el añadido de nuevo candidato";
    $resultado = db_query_id($con, $inres, $mens);
    if (!$resultado) {
        $inmsg = "<div class=\"alert alert-danger\">" .
                _("Error al añadir candidato") . "  \"  " . $nombre_cand . "  \"  " . _("ya que no se ha añadido a la base de datos") . "	</div>";
    } else {
//echo $resultado;
        $inmsg = '<div class="alert alert-success">' . _("Añadido opción o candidato") . ' <br/>' . $nombre_cand . ' <br/> '  . _("a la base de datos") . ' </div>
	<a href="admin.php?c='. encrypt_url('admin/candidatos_crop/idcat='.$resultado .'&idvot=' . $idvot,$clave_encriptacion).'" class="btn btn-success" >' . _("Si quiere puede añadir una imagen a") . ' ' . $nombre_cand . '</a><br/>
<a href="javascript:void(0);" data-href="aux_vota.php?c='.encrypt_url('votacion/perfil/idgr='.$resultado,$clave_encriptacion).'" class="opennormalModal btn btn-success" title="'.$nombre_cand .'" >'. _("Vista previa").'</a>';
    }
  }else{
    $inmsg = "<div class=\"alert alert-danger\">" .
            _("Error al añadir candidato") . " \" " . $nombre_cand . " \" " . _("Parece que esta intentando recargar la pagina y se esta produciendo un duplicado de datos") . "	</div>";
  }
}


$result_vot = mysqli_query($con, "SELECT id_provincia, tipo, seguridad  FROM $tbn1  where id=$idvot");
$row_vot = mysqli_fetch_row($result_vot);
?>
      <!--Comiezo-->
      <div class="card-header"> <h1 class="card-title"><?= _("INCLUIR NUEVA OPCIÓN o CANDIDATO") ?></h1> </div>


      <div class="card-body">

      <div class="row">
<div class="col-sm-8">


</div>
<div class="col-sm-4">
  <a href="admin.php?c=<?php echo encrypt_url('admin/candidatos_busq1/idvot='.$idvot,$clave_encriptacion); ?>" class="btn btn-primary btn-block"><?= _("Buscar en el directorio para modificar o borrar candiatos en esta encuesta") ?></a>
                    <?php if ($row_vot[2] == 3 or $row_vot[2] == 4) { ?>

                        <a href="admin.php?c=<?php echo encrypt_url('admin/interventor/idvot='.$idvot,$clave_encriptacion); ?>"  class="btn btn-primary btn-block"> <?= _("Incluir  interventores") ?></a>
                    <?php } ?>
</div>
</div>
<div class="separador"></div>
                    <?php
                    if (isset($inmsg)) {
                        echo "$inmsg";
                    }
                    ?>

<div class="separador"></div>
                    <form action="<?php $_SERVER['PHP_SELF'] ?>" method=post name="frmDatos" id="frmDatos" class="well form-horizontal">


                        <div class="form-group row">
                            <label for="nombre_cand" class="col-sm-3 control-label"><?= _("Nombre") ?> </label>

                            <div class="col-sm-9">
                                <input name="nombre_cand" type="text" id="nombre_cand" class="form-control" placeholder="Nombre del candidato" required autofocus data-validation-required-message="El nombre de la votación es un dato requerido">
                            </div>
                        </div>



                        <?php
/////miramos a ver si es de tipo vut para meterle el codigo de oreden vut
                        if ($row_vot[1] == 2) {
                            ?>
                            <div class="form-group row">
                                <label for="VUT" class="col-sm-3 control-label"></label>
                                <div class="col-sm-9">
                                    <?php
                                    ////tenemos que comprobar el ultimo numero que hemos metido para poner el siguiente
                                    $sql_vut = "SELECT id, id_vut FROM $tbn7 WHERE  id_votacion ='$idvot' ORDER BY id_vut DESC";

                                    $result_vut = mysqli_query($con, $sql_vut);
                                    $row_cnt = mysqli_num_rows($result_vut);
                                if ($row_cnt!= 0) {

                                    $row_vut = mysqli_fetch_row($result_vut);

                                        $numero_id_vut = $row_vut[1] + 1;
                                    } else {
                                        $numero_id_vut = 0;
                                    }
                                    echo $numero_id_vut;
                                    ///// si es de tipo vut generamos un campo oculto con el numero
                                    ?>
                                    | <?= _("Orden VUT") ?>
                                    <input name="numero_id_vut" type="hidden" id="numero_id_vut" value="<?php echo "$numero_id_vut"; ?>" />
                                </div>
                            </div>
                        <?php } ?>



                        <div class="form-group row">
                            <div  class="col-sm-3"><?= _("Sexo") ?></div>

                            <div class="col-sm-9"><label for="sexo_2">
                                    <input name="sexo" type="radio" id="sexo_2" value="O" checked="checked" />
                                    <?= _("Neutro") ?> </label>
                                (<?= _("opción sin sexo") ?>)
                                <span class="text-info"><?= _("¡¡¡ojo, SI ES UNA VOTACIÓN DE PRIMARIAS HAY QUE INDICAR SEXO!!!") ?></span>
                                <br/>
                                <label for="sexo_0">

                                    <input name="sexo" type="radio" id="sexo_0" value="H" />
                                    <?= _("Hombre") ?></label>
                                <br />
                                <label for="sexo_1">
                                    <input type="radio" name="sexo" value="M" id="sexo_1" />
                                    <?= _("Mujer") ?></label>
                                <br />
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label for="nombre" ><?= _("Texto") ?></label>


</div>
                    <div class="col-sm-12">

                                <textarea cols="80" id="texto" name="texto" rows="10"></textarea>
                                <!-- ckeditor -->
                                <script type="text/javascript" src="assets/ckeditor/ckeditor.js"></script>

                                <script>
                                 CKEDITOR.replace( 'texto', {
                                  height: 250,

                                  extraPlugins: 'imgbrowse',
                                  filebrowserImageBrowseUrl: '/assets/ckeditor/plugins/imgbrowse/imgbrowse.html?imgroot=<?php echo $baseUrl; ?>',
                                  <?php if($_SESSION['usuario_nivel']<=6){     // nivel para poder añadir imagenes  ?>
                                  filebrowserImageUploadUrl: 'aux_vota.php?c=<?php echo encrypt_url('basicos_php/imageUpload/conec=OK',$clave_encriptacion) ?>',
                                  <?php } ?>
                                //  filebrowserUploadUrl: "upload.php",

                                 });
                                </script>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                <p>&nbsp;</p>
                                <input name="provincia" type="hidden" id="provincia" value="<?php echo $row_vot[0]; ?>" />
                                <input name="add_candidato" type=submit  class="btn btn-primary btn-block" id="add_directorio" value="<?= _("Insertar nuevo candidato") ?>">
</div>
</div>
                                </form>

                            </div>
                        </div>

                        <!--Final-->
<?php } ?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que permite crear y modificar css personalizado para el blog
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 9;
include('../private/inc_web/nivel_acceso.php');
if($_SESSION['admin_blog']==1){

$file = "data/personal.css";
$texto="";

if (ISSET($_POST["modifika_pagina"])) {

    $css = fn_filtro_nodb($_POST['texto']);

    $abrir = fopen($file, 'w+');
    //fwrite($abrir, $css) or die("Could not write file!");

    if (fwrite($abrir, $css) === FALSE) {
          $inmsg= _("No se puede escribir en el archivo"). $file;
            exit;
        }
      $inmsg= _("Éxito, se escribió su codigo en el archivo");
    fclose($abrir);  //  Cerramos el fichero
}

if(file_exists($file))
   {
       $tamanio=filesize($file);
       if($tamanio>0)
       {
           $fp=fopen($file, "rb");
           $texto=fread($fp, $tamanio);
           fclose($fp);
       }
   }


?>


<div class="card-header-votaciones "> <h1 class="card-title"><?= _("Incluir CSS personalizado") ?></h1> </div>

<div class="card-body">


                    <!--Comiezo-->


                    <p>&nbsp;</p>


                    <?php  // Mensajes del sistema si se ha subido o modidificado bien
                    if (isset($inmsg)) {
                        echo "$inmsg";
                    }
                    ?>
                    <div class="form-group row">
                      <div class="col-sm-3">
                      </div>
                        <div class="col-sm-9">
                    <p class="text-info"><?= _("Puede incluir el código css personalizado que necesite") ?>. <?= _("El código css que incluya, modificara el código css que tiene la plantilla, por lo que puede personalizar en gran medida la web") ?>.
                    </p>
                    <p class="text-info">
                    <?= _("Este código solo altera los estilos de las páginas del blog") ?> (<?= _("o extranet") ?>).
                    </p>
                    <p class="text-info">
                    <?= _("Ejemplo") ?>:
                    </p>
                    <code>
                    #info-access {
	                     background-color: #9ebf42;
                     }
                   </code>

                 </div>
               </div>

                    <form action="<?php $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data" method=post class="well form-horizontal" id="form1">




                      <div class="form-group row">

                              <label for="texto"  class="col-sm-1 control-label" ><?= _("CSS") ?></label>

                              <div class="col-sm-11">
                              <textarea class="form-control" id="texto" name="texto" rows="20"><?php echo "$texto"; ?></textarea>

                          </div>
                      </div>


                        <div class="form-group">
                            <div class="col-sm-12">

                                <p>&nbsp;</p>

                                <input name="fecha" type="hidden" id="fecha" value="<?php echo"$fecha"; ?>" />
                                  <input name="modifika_pagina" type=submit  class="btn btn-primary btn-block" id="modifika_pagina" value="<?= _("MODIFICAR CSS PERSONALIZADO") ?>">

                            </div>
                        </div>



                                </form>
                        <!--Final-->
                </div>

              <?php
            }
              }
              ?>

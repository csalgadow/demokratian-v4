<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###                                                         ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que permite crear o modificar las preguntas en la votación tipo debate
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 9;
include('../private/inc_web/nivel_acceso.php');

if (isset($variables['id'])) {
    $id = fn_filtro_numerico($con, $variables['id']);
    $acc = fn_filtro_nodb($variables['acc']);
} else {
    $id = "";
    $acc = "";
}
$idvot = fn_filtro_numerico($con, $variables['idvot']);
$ids_provincia = $_SESSION['localidad'];
$row[1] = "";
$row[2] = "";

$fecha = date("Y-m-d h:i:s");
$fecha_ver = date("d-m-Y ");


if (ISSET($_POST["modifika_preguntas"])) {
    $nombre_cand = fn_filtro($con, $_POST['nombre_cand']);
    $respuestas = fn_filtro($con, $_POST['respuestas']);
    $nombre_usuario = $_SESSION['ID'];

    $sSQL = "UPDATE $tbn13 SET pregunta=\"$nombre_cand\",  respuestas=\"$respuestas\" ,fecha_modif=\"$fecha\",  modif=\"$nombre_usuario\" WHERE id='$id'";
    mysqli_query($con, $sSQL) or die("Imposible modificar pagina");

    $inmsg = "<div class=\"alert alert-success\">" . ("Realizadas las Modificaciones") . " <br>" . _("Asi ha quedado la pregunta") . " " . $nombre_cand . " </div>";
}


if (ISSET($_POST["add_preguntas"])) {

    $nombre_cand = fn_filtro($con, $_POST['nombre_cand']);

    $respuestas = fn_filtro($con, $_POST['respuestas']);
    $nombre_usuario = $_SESSION['ID'];

    $insql = "insert into $tbn13 (pregunta,  respuestas,id_votacion,anadido, fecha_anadido ) values (  \"$nombre_cand\",  \"$respuestas\", \"$idvot\", \"$nombre_usuario\", \"$fecha\" )";
    $inres = @mysqli_query($con, $insql) or die("<strong><font color=#FF0000 size=3>  Imposible añadir. Cambie los datos e intentelo de nuevo.</font></strong>");
    $inmsg = "<div class=\"alert alert-success\">" . _("Ha añadido la pregunta") . " <br/>" . $nombre_cand . "<br/>" . _("a la base de datos") . " </div> ";
}
?>

                    <!--Comiezo-->


                    <?php
                    if ($acc == "modifika") {
                        $result = mysqli_query($con, "SELECT * FROM $tbn13 where id=$id");
                        $row = mysqli_fetch_row($result);
                    }
                    ?>
                    <div class="card-header"> <h1 class="card-title"><?= _("Pregunta del DEBATE") ?> </h1> </div>


<div class="card-body">
  <div class="row">
      <div class="col-sm-8"></div>
        <div class="col-sm-4">
                    <a href="admin.php?c=<?php echo encrypt_url('admin/preguntas_busq1/idvot='.$idvot,$clave_encriptacion) ?>" class="btn btn-primary btn-block"><?= _("Ir al directorio de preguntas de este debate") ?></a>
</div>
</div>


                    <?php
                    if (isset($inmsg)) {
                        echo "$inmsg";
                    }
                    ?>



                    <form action="<?php $_SERVER['PHP_SELF'] ?>" method=post   name="frmDatos" id="frmDatos" class="well form-horizontal">
                        <div class="form-group row">
                            <label for="nombre_cand" class="col-sm-3 control-label"> <?= _("Pon tu pregunta") ?> </label>

                            <div class="col-sm-9">

                                <input name="nombre_cand" type="text" id="nombre_cand" value="<?php echo "$row[1]"; ?>"   class="form-control" placeholder="Escribe tu pregunta" required autofocus />
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-3 ">  <?= _("respuestas posibles") ?></div>

                            <div class="col-sm-9">

                                <?php
                                if ($row[2] == 2) {
                                    $chekeado2 = "checked=\"checked\" ";
                                } else if ($row[2] == 3) {
                                    $chekeado3 = "checked=\"checked\" ";
                                } else if ($row[2] == 4) {
                                    $chekeado4 = "checked=\"checked\" ";
                                } else {
                                    $chekeado5 = "checked=\"checked\" ";
                                }
                                ?>
                                <label for="respuestas_2" class="control-label">
                                    <input name="respuestas" type="radio" id="respuestas_2" value="2" <?php
                                    if (isset($chekeado2)) {
                                        echo "$chekeado2";
                                    }
                                    ?> />
                                    2 <?= _("(SI- NO") ?>)</label>
                                <br />
                                <label for="respuestas_0" class="control-label">
                                    <input name="respuestas" type="radio" id="respuestas_0" value="3" <?php
                                    if (isset($chekeado3)) {
                                        echo "$chekeado3";
                                    }
                                    ?> />
                                    3 <?= _("(SI-NO-NO SE)") ?></label>
                                <br />
                                <label for="respuestas_1" class="control-label">
                                    <input type="radio" name="respuestas" value="4" id="respuestas_1" <?php
                                    if (isset($chekeado4)) {
                                        echo "$chekeado4";
                                    }
                                    ?> />
                                    4 <?= _("(SI-NO-NO SE, BLOQUEO)") ?></label>
                                <br />
                                <label for="respuestas_3" class="control-label">
                                  <input type="radio" name="respuestas" value="5" id="respuestas_3"  <?php
                                    if (isset($chekeado5)) {
                                        echo "$chekeado5";
                                    }
                                    ?>/>
                                    5 <?= _("(Me gusta mucho, me gusta, indiferente, no me gusta, no me gusta nada)") ?> </label>
                                <br />
                            </div>
                        </div>


                        <?php if ($acc == "modifika") { ?>
                            <input name="modifika_preguntas" type=submit  class="btn btn-primary btn-block"  id="add_directorio" value="<?= _("MODIFICAR esta pregunta") ?>" />
                        <?php } else { ?>
                            <input name="add_preguntas" type=submit class="btn btn-primary btn-block"  id="add_directorio" value="<?= _("CREAR una nueva pregunta") ?>" />
                        <?php } ?>

                    </form>
                  </div>
                    <!--Final-->
<?php } ?>

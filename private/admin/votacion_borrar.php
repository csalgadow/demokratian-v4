<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###                                                         ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo borra una votación, también borra todos los datos relacionados, como votos, interventores, preguntas si es un debate, etc
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 2;
include('../private/inc_web/nivel_acceso.php');



$id = fn_filtro_numerico($con, $variables['id']);
$mens_error = "";
$mens_ok = "";
$mensajes_ok = "";
//realizamos la consulta para cargar los datos que luego presentaremos
$result = mysqli_query($con, "SELECT * FROM $tbn1 where id=$id");
$row = mysqli_fetch_row($result);


// si el tipo de votacion es cualquiera menos  el debate
if ($row[6] == 1 or $row[6] == 2 or $row[6] == 3) {
    //realizamos una consulta para saber que candidatos u opciones  tienen foto
    //imagen_pequena
    $sql_img = "SELECT id,imagen_pequena FROM $tbn7 WHERE id_votacion = '$id'  ";
    $result_img = mysqli_query($con, $sql_img);
    if ($row_img = mysqli_fetch_array($result_img)) {
        mysqli_field_seek($result_img, 0);
        do {
            if ($row_img[1] != "") {
                $thumb_photo_exists = $upload_cat . "/" . $row_img[1];
                if (file_exists($thumb_photo_exists)) {
                    unlink($thumb_photo_exists);
                }
            }
        } while ($row_img = mysqli_fetch_array($result_img));
    }
    //borramos los candidatos u opciones con esta id
    $borrado_candidatos = "DELETE FROM $tbn7 WHERE id_votacion=$id ";
    $mens = "ERROR en el borrado de candidatos";
    $result_borrado_candidatos = db_query($con, $borrado_candidatos, $mens);

    if (!$result_borrado_candidatos) {
        $mens_error .= $mens . "<br/>";
    } else {
        $mens_ok .= "Borrados los candidatos u opciones de esta votación <br/>";
    }
    //borramos usuario_x_votacion con esta id
    $borrado_usuario_x_votacion = "DELETE FROM $tbn2 WHERE id_votacion=$id ";
    $mens = "ERROR en el borrado de usuario_x_votacion";
    $result_usuario_x_votacion = db_query($con, $borrado_usuario_x_votacion, $mens);

    if (!$result_usuario_x_votacion) {
        $mens_error .= $mens . "<br/>";
    } else {
        $mens_ok .= "Borrados la relacion de votantes de esta votación <br/>";
    }

    //borramos interventores con esta id
    $borrado_interventores = "DELETE FROM $tbn11 WHERE id_votacion=$id ";
    $mens = "ERROR en el borrado de interventores";
    $result_borrado_interventores = db_query($con, $borrado_interventores, $mens);

    if (!$result_borrado_interventores) {
        $mens_error .= $mens . "<br/>";
    } else {
        $mens_ok .= "Borrados los interventores de esta votación <br/>";
    }

    //borramos los codificadores con esta id
    $borrado_codificadores = "DELETE FROM $tbn21 WHERE id_votacion=$id ";
    $mens = "ERROR en el borrado de codificadores";
    $result_borrado_codificadores = db_query($con, $borrado_codificadores, $mens);

    if (!$result_borrado_codificadores) {
        $mens_error .= $mens . "<br/>";
    } else {
        $mens_ok .= "Borrados los codificadores de esta votación <br/>";
    }

    //// borramos votacion_web (pagina externa) con esta id
    $borrado_votacion_web = "DELETE FROM $tbn22 WHERE id_votacion=$id ";
    $mens = "ERROR en el borrado de votacion_web";
    $result_borrado_votacion_web = db_query($con, $borrado_votacion_web, $mens);

    if (!$result_borrado_votacion_web) {
        $mens_error .= $mens . "<br/>";
    } else {
        $mens_ok .= "Borrados los datos de pagina externa de esta votación <br/>";
    }
    //// FIN borramos votacion_web (pagina externa) con esta id
}

////// FIN si el tipo de votacion es cualquiera menos  el debate
///// si son primarias o encuesta borramos los votos

if ($row[6] == 1 or $row[6] == 3) {
    //borramos votos con esta id
    $borrado_votos = "DELETE FROM $tbn10 WHERE id_votacion=$id ";
    $mens = "ERROR en el borrado de votos";
    $result_borrado_votos = db_query($con, $borrado_votos, $mens);

    if (!$result_borrado_votos) {
        $mens_error .= $mens . "<br/>";
    } else {
        $mens_ok .= "Borrados los votos de esta votación <br/>";
    }
}
///// FIN  si son primarias o encuesta borramos los votos
///si es debate , borramos las tablas relacionadas
if ($row[6] == 4) {
    //borramos debate_comentario con esta id
    $borrado_debate_comentario = "DELETE FROM $tbn12 WHERE id_votacion=$id ";
    $mens = "ERROR en el borrado de debate comentario";
    $result_borrado_debate_comentario = db_query($con, $borrado_debate_comentario, $mens);

    if (!$result_borrado_debate_comentario) {
        $mens_error .= $mens . "<br/>";
    } else {
        $mens_ok .= "Borrados los comentarios de este debate <br/>";
    }
    //borramos debate_preguntas con esta id
    $borrado_debate_preguntas = "DELETE FROM $tbn13 WHERE id_votacion=$id ";
    $mens = "ERROR en el borrado de debate preguntas";
    $result_borrado_debate_preguntas = db_query($con, $borrado_debate_preguntas, $mens);

    if (!$result_borrado_debate_preguntas) {
        $mens_error .= $mens . "<br/>";
    } else {
        $mens_ok .= "Borrados las preguntas de este debate <br/>";
    }
    //borramos debate_votos con esta id
    $borrado_debate_votos = "DELETE FROM $tbn14 WHERE id_votacion=$id ";
    $mens = "ERROR en el borrado de debate votos";
    $result_borrado_debate_votos = db_query($con, $borrado_debate_votos, $mens);

    if (!$result_borrado_debate_votos) {
        $mens_error .= $mens . "<br/>";
    } else {
        $mens_ok .= "Borrados los votos de este debate <br/>";
    }
}

////// FIN si es debate , borramos las tablas relacionadas
////// si es VUT borramos los votos de esta tabla
if ($row[6] == 2) {
    //borramos elvoto vut con esta id
    $borrado_elvoto = "DELETE FROM $tbn15 WHERE id_votacion=$id ";
    $mens = "ERROR en el borrado de votos VUT";
    $result_borrado_elvoto = db_query($con, $borrado_elvoto, $mens);

    if (!$result_borrado_elvoto) {
        $mens_error .= $mens . "<br/>";
    } else {
        $mens_ok .= "Borrados los votos de esta votación <br/>";
    }
}

////// FIN si es VUT borramos los votos de esta tabla
//////  por ultimo borramos la votacion con este id
$borrado_votacion = "DELETE FROM $tbn1 WHERE ID=$id ";
$mens = "ERROR en el borrado de votación";
$result_borrado_votacion = db_query($con, $borrado_votacion, $mens);

if (!$result_borrado_votacion) {
    $mens_error .= $mens . "<br/>";
} else {
    $mens_ok .= "Borrada la votación <br/>";
}

if ($mens_error != "") {
    $mensajes_error = "<div class=\"alert alert-danger\">" . $mens_error . " </div>";
}
if ($mens_ok != "") {
    $mensajes_error = "<div class=\"alert alert-success\">" . $mens_ok . " </div>";
}
?>


                    <!--Comiezo-->

                    <div class="card-header"> <h1 class="card-title"><?= _("VOTACIÓN BORRADA") ?></h1> </div>


                    <div class="card-body">
                    <?php echo $mensajes_error; ?>
                    <?php echo $mensajes_ok; ?>


                    <form action="<?php $_SERVER['PHP_SELF'] ?>" method=post name="frmDatos" id="frmDatos"  class="well form-horizontal" >

                        <div class="form-group row">
                            <label for="nombre" class="col-sm-3 control-label"><?= _("Nombre votación") ?></label>

                            <div class="col-sm-9">  <?php echo "$row[3]"; ?>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="demarcacion" class="col-sm-3 control-label"><?= _("Demarcación") ?></label>
                            <div class="col-sm-9">

                                <?php
                                if ($row[20] == 1) {
                                    echo _("Estatal");
                                } else if ($row[20] == 2) {
                                    echo _("Autonomica");
                                } else if ($row[20] == 3) {
                                    echo _("Provincial");
                                } else if ($row[20] == 4) {
                                    echo _("Grupo Provincial");
                                } else if ($row[20] == 5) {
                                    echo _("Grupo Autonomico");
                                } else if ($row[20] == 6) {
                                    echo _("Grupo provincial");
                                };
                                ?>

                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="fecha_ini" class="col-sm-3 control-label"> <?= _("Fecha comienzo") ?></label>
                            <div class="col-sm-4">

                                <?php
                                $fecha_i = date("d-m-Y", strtotime($row[13]));
                                echo "$fecha_i ";
                                ?> -

                                <?php
                                $hora_i = date("H", strtotime($row[13]));
                                echo " $hora_i ";
                                ?>
                                :
                                <?php
                                $min_i = date("i", strtotime($row[13]));
                                echo " $min_i ";
                                ?>



                            </div>

                        </div>



                        <div class="form-group row">
                            <label for="fecha_final" class="col-sm-3 control-label"><?= _("Fecha final") ?> </label>
                            <div class="col-sm-4">

                                <?php
                                $fecha_f = date("d-m-Y", strtotime($row[14]));
                                echo "$fecha_f ";
                                ?>

                                -

                                <?php
                                $hora_f = date("H", strtotime($row[14]));
                                echo " $hora_f ";
                                ?> :
                                <?php
                                $min_f = date("i", strtotime($row[14]));
                                echo " $min_f ";
                                ?>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="tipo" class="col-sm-3 control-label"><?= _("Tipo de votación") ?> </label>
                            <div class="col-sm-9">


                                <?php
                                echo "$row[6] | ";

                                if ($row[6] == 1) {
                                    echo _("PRIMARIAS");
                                } else if ($row[6] == 2) {
                                    echo _("VUT");
                                } else if ($row[6] == 3) {
                                    echo _("ENCUESTA");
                                } else if ($row[6] == 4) {
                                    echo _("DEBATE");
                                }
                                ?>

                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="tipo_usuario_0" class="col-sm-3 control-label"> <?= _("Tipo de votante") ?> </label>
                            <div class="col-sm-9">


                                <?php
                                if ($row[7] == 1) {
                                    echo _("Solo socios");
                                } else if ($row[7] == 2) {
                                    echo _("Socios y simpatizantes verificados");
                                } else if ($row[7] == 3) {
                                    echo _("Socios y simpatizantes");
                                } else if ($row[7] == 5) {
                                    echo _("Abierta");
                                }
                                ?>


                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="tipo_usuario_0" class="col-sm-3 control-label"><?= _("Estado") ?></label>
                            <div class="col-sm-9">


                                <?php
                                if ($row[2] == "si") {
                                    echo _("Activado");
                                } else {
                                    echo _("Desactivado");
                                };
                                ?>


                            </div>
                        </div>
                        <div id="accion_opciones" <?php
                        if (isset($display_debate)) {
                            echo "$display_debate";
                        }
                        ?> >


                            <div class="form-group row">
                                <label for="tipo_seg" class="col-sm-3 control-label"><?= _("Seguridad de control de voto") ?></label>
                                <div class="col-sm-9">


                                    <?php
                                    if ($row[21] == 1) {
                                        echo _("Sin trazabilidad ni interventores");
                                    } else if ($row[21] == 2) {
                                        echo _("Con comprobación de voto");
                                    } else if ($row[21] == 3) {
                                        echo _("Con interventores");
                                    } else if ($row[21] == 4) {
                                        echo _("Con comprobación de voto e interventores");
                                    }
                                    ?>


                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="numero_opciones" class="col-sm-3 control-label"><?= _("Numero de opciones que se pueden votar") ?> </label>
                                <div class="col-sm-9">

                                    <?php echo "$row[8]"; ?>

                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="resumen" class="col-sm-3 control-label"><?= _("Resumen") ?></label>
                                <div class="col-sm-9">

                                    <?php echo "$row[5]"; ?>


                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="texto" class="col-sm-3 control-label"><?= _("Texto") ?></label>
                                <div class="col-sm-9">

                                    <?php echo "$row[4]"; ?>


                                </div>
                            </div>


                    </form>

                    <!--Final-->
</div>


<?php } ?>

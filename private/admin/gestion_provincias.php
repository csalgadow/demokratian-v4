<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###                                                         ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que permite poner un correo para notificaciones distinto para cada provincia
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 1;
include('../private/inc_web/nivel_acceso.php');

if (ISSET($_POST["modifika_correo"])) {

    $id_provincia_mody = fn_filtro($con, $_POST['id_provincia_mody']);



    if (empty($_POST['correo_error'])) {
        $error = "error";
        $mensaje = "<div class=\"alert alert-warning\">El e-mail del usuario es un dato requerido</div>";
    } elseif (!filter_var($_POST['correo_error'], FILTER_VALIDATE_EMAIL)) {
        $error = "error";
        $mensaje = "<div class=\"alert alert-warning\">" . _("la direccion es erronea") . "</div>";
    } else {

        $correo_error = fn_filtro($con, $_POST['correo_error']);
        $sSQL = "UPDATE $tbn8 SET correo_notificaciones=\"$correo_error\" WHERE id='$id_provincia_mody'";
        mysqli_query($con, $sSQL) or die("Imposible modificar");
        $mensaje = "<div class=\"alert alert-success\">" . _("Modificado correo") . " " . $correo_error . " " . _("de la provincia numero") . " " . $id_provincia_mody . "</div>";
    }
}
?>



                    <!--Comiezo-->
                    <div class="card-header"> <h1 class="card-title"><?= _("Correos notificaciones Provincias") ?></h1> </div>


<div class="card-body">

                    <?php
                    if ($_SESSION['nivel_usu'] == 2) {
                        $sql = "select DISTINCT id, provincia,correo_notificaciones from $tbn8  order by ID";
                    } else if ($_SESSION['nivel_usu'] == 3) {
                        $ids_ccaa = $_SESSION['id_ccaa_usu'];
                        $sql = "SELECT  id, provincia,correo_notificaciones FROM $tbn8 where id_ccaa=$ids_ccaa";
                    } else if ($_SESSION['nivel_usu'] == 4) {
                        $id_usu = $_SESSION['ID'];
                        $sql = "SELECT  a.id, a.provincia,a.correo_notificaciones FROM $tbn8 a, $tbn5 b  where (a.id=b.id_provincia) and b.id_usuario='$id_usu'";
                    }
                    $result = mysqli_query($con, $sql);
                    if ($row = mysqli_fetch_array($result)) {
                        ?>
                        </p>
                        <?php
                        if (isset($mensaje)) {
                            echo $mensaje;
                        }
                        ?>


                            <table width=99%   class="table table-striped" cellspacing="0" >

                                <thead>
                                    <tr>
                                        <th width=5% align=center >Id</th>
                                        <th width=20% align=left ><?= _("Provincia") ?></th>
                                        <th width=65% align=center ><?= _("Correo para notificaciones de error") ?></td>
                                        <th width=10% align=center >&nbsp;</th>
                                    </tr></thead>

                                <tbody>

                                    <?php
                                    mysqli_field_seek($result, 0);

                                    do {
                                        ?>

                                        <tr>
                                    <form id="<?php echo "$row[1]" ?>" name="<?php echo "$row[1]" ?>" method="post" action="">
                                        <td><?php echo "$row[0]" ?>
                                        </td>
                                        <td> <?php echo $row[1] ?> </td>
                                        <td>
                                            <?php
                                            if ($row[0] == "000") {

                                                include ("../inc_web/verifica.php");
                                                echo"correo errores: $email_error <br/>";
                                                echo"correo notificaciones: $email_env";
                                            } else {
                                                ?>

                                                <input name="correo_error" type="email"  class="form-control"  id="correo_error" value="<?php echo "$row[2]" ?>" maxlength="200" />
                                            </td>

                                        <?php } ?>
                                        <td> <?php
                                            if ($row[0] == "000") {
                                                echo"&nbsp;";
                                            } else {
                                                ?> <input name="id_provincia_mody" type="hidden" id="id_provincia_mody" value="<?php echo "$row[0]" ?>" /> <input name="modifika_correo" type="submit"  class="btn btn-primary btn-xs" id="modifika_correo" value="<?= _("Modificar") ?>" /> <?php } ?></td>
                                    </form>     </tr>



                                    <?php
                                } while ($row = mysqli_fetch_array($result));
                                ?>
                                </tbody>
                            </table>
                            <?php
                        }
                        ?>

</div>


<?php } ?>

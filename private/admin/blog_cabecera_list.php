<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que lista las cabeceras que tenemos, da acceso a la modifciación o al borrado de cabeceras 
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 9;
include('../private/inc_web/nivel_acceso.php');
if($_SESSION['admin_blog']==1){
?>

                    <!--Comiezo-->

                    <?php

                        $sql = "SELECT id,	titulo, 	id_categoria, 	activo,	orden	 FROM $tbn30 ";
                        $result = mysqli_query($con, $sql)or die(mysqli_error($con));


                    ?>

                    <div class="card-header"> <h1 class="card-title"><?= _("Lista de imagenes de la cabecera") ?></h1> </div>

                    <div class="card-body">

                    <div class="row">
                      <div class="col-sm-9"></div>
                        <div class="col-sm-3">
                    <a href="admin.php?c=<?php echo encrypt_url('admin/blog_cabecera/s=SD',$clave_encriptacion); ?>" class="btn btn-primary btn-block" ><?= _("añadir cabecera") ?></a>
                      </div>
                    </div>

                    <div class="separador"></div>

                    <?php

                    if ($row = mysqli_fetch_array($result)) {
                        ?>

                        <table id="tabla1" class="table table-striped table-bordered dt-responsive nowrap" data-page-length="25">
                            <thead>
                                <tr>
                                    <th width="3%">ID</th>
                                    <th width="45%"><?= _("Titulo") ?></th>
                                    <th width="7%"><?= _("Categoria") ?></th>
                                    <th width="7%"><?= _("Activo") ?></th>
                                    <th width="7%"><?= _("Orden") ?></th>
                                    <th width="10%"><?= _("modificar") ?></th>
                                    <th width="10%"><?= _("borrar") ?></th>


                                </tr>
                            </thead>

                            <tbody>

                                <?php
                                mysqli_field_seek($result, 0);

                                do {
                                    ?>
                                    <tr>
                                        <td><?php echo "$row[0]" ?></td>
                                        <td><?php echo "$row[1]" ?></td>
                                        <td> <?php if ($row[2] == 0) {
                                                echo _("Cabecera fija");
                                                } else {
                                                echo _("Cabecera tipo Slider");
                                                } ?>
                                        </td>
                                        <td><?php
                                        if ($row[3]==0){
                                          echo '<a role="button" class="" data-toggle="tooltip" data-html="true" title="" data-original-title="<em>'. _("Modulo inactivo") .' </em> ">
                                          <i class=" fa fa-times-circle-o text-danger"></i>
                                          </a>';
                                          }else{
                                            echo '<a role="button" class="" data-toggle="tooltip" data-html="true" title="" data-original-title="<em>'. _("Modulo activo") .' </em> ">
                                            <i class="fa fa-check-square-o text-success"></i>
                                            </a>';
                                          }?></td>
                                        <td><?php echo "$row[4]" ?></td>
                                        <td><a href="admin.php?c=<?php echo  encrypt_url('admin/blog_cabecera/id='.$row[0] ,$clave_encriptacion);?>"  class="btn btn-primary btn-xs"><?= _("modificar") ?></a></td>
                                        <td> <a href="admin.php?c=<?php echo  encrypt_url('admin/blog_cabecera_borra/id='.$row[0] ,$clave_encriptacion);?>" onClick="return borrarevento()" class="btn btn-danger btn-xs"><?= _("borrar") ?> </a>  </td>
                                    </tr>


                                    <?php
                                } while ($row = mysqli_fetch_array($result));
                                ?>
                            </tbody>
                        </table>
                        <?php
                    } else {?>
                      <div class="alert alert-warning">
                        <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                        <i class="fa fa-times simple-remove" aria-hidden="true"></i>
                        </button>
                        <span>
                          <b> Info - </b> <?=_("¡No se ha encontrado ningún resultado!");?></span>
                        </div>
                        <?php

                    }
                    ?>
                  </div>

                    <!--Final-->


        <script type="text/javascript" src="assets/DataTables/datatables.min.js" ></script>
      <!--  <script src="assets/DataTables/Buttons-1.6.2/js/dataTables.buttons.min.js" ></script>
        <script src="assets/DataTables/JSZip-2.5.0/jszip.min.js" ></script>
        <script src="assets/DataTables/pdfmake-0.1.36/pdfmake.min.js" ></script>
        <script src="assets/DataTables/pdfmake-0.1.36/vfs_fonts.js" ></script>
        <script src="assets/DataTables/Buttons-1.6.2/js/buttons.html5.min.js" ></script>-->

        <script type="text/javascript" language="javascript" class="init">
          $(document).ready(function() {
            $('#tabla1').DataTable({
              responsive: true,
              dom: 'Blfrtip',
              buttons: [
              //  'copyHtml5',
              //  'excelHtml5',
              //  'csvHtml5',
              //  'pdfHtml5'
              ],
              lengthMenu: [
                [10, 25, 50, -1],
                [10, 25, 50, "Todos"]
              ],
              language: {
                buttons: {
                  copy: '<?= _("Copiar") ?>',
                  copyTitle: '<?= _("Copiado en el portapapeles") ?>',
                  copyKeys: '<?= _("Presione") ?> <i> ctrl </i> o <i> \ u2318 </i> + <i> C </i> <?= _("para copiar los datos de la tabla a su portapapeles") ?>. <br> <br> <?= _("Para cancelar, haga clic en este mensaje o presione Esc") ?>.',
                  copySuccess: {
                    _: '%d <?= _("lineas copiadas") ?>',
                    1: '1 <?= _("linea copiada") ?>'
                  }
                },
                processing: "<?= _("Tratamiento en curso") ?>...",
                search: "<?= _("Buscar") ?>:",
                lengthMenu: "<?= _("Ver") ?> _MENU_  <?= _("resultados por pagina") ?>",
                info: "<?= _("Mostrando") ?> _PAGE_ <?= _("de") ?> _PAGES_ <?= _("paginas") ?>",
                infoEmpty: "<?= _("No se han encitrado resultados") ?>",
                infoFiltered: "(<?= _("filtered from") ?> _MAX_ <?= _("total records") ?>)",
                infoPostFix: "",
                loadingRecords: "<?= _("Cargando") ?>...",
                zeroRecords: "<?= _("No se han encontrado resultados - perdone") ?>",
                emptyTable: "<?= _("No se han encontrado resultados - perdone") ?>",
                paginate: {
                  first: "<?= _("Primero") ?>",
                  previous: "<?= _("Anterior") ?>",
                  next: "<?= _("Siguiente") ?>",
                  last: "<?= _("Ultimo") ?>"
                },
                aria: {
                  sortAscending: ": <?= _("activar para ordenar columna de forma ascendente") ?>",
                  sortDescending: ": <?= _("activar para ordenar columna de forma descendente") ?>"
                }
              }
            });
          });
        </script>
        <!--end datatables -->
        <script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js" ></script>
        <script src="https://cdn.datatables.net/responsive/2.2.7/js/responsive.bootstrap4.min.js" ></script>
        <script src="assets/js/admin_borrarevento.js" ></script>

<?php }
  }
?>

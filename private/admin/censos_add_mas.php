<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo para añadir votantes de forma masiva, un votante por línea con los datos separados por comas  
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 1;
include('../private/inc_web/nivel_acceso.php');

$inmsg = "";
$mensaje2 = "";
$mensaje1 = "";
$lista1 = "";
?>


                    <!--Comiezo-->
                    <div class="card-header"> <h1 class="card-title"><?= _("AÑADIR VOTANTES DE FORMA MASIVA") ?></h1> </div>


<div class="card-body">


                    <?php
                    if (isset($_POST['emails'])) {
                        if ($_POST['emails'] != "") {
                            if ($_POST['provincia'] != '00') {

                                $provincia = fn_filtro($con, $_POST['provincia']);
                                $optiones = "select  id_ccaa from $tbn8 where ID ='" . $provincia . "'";
                                $resultas = mysqli_query($con, $optiones) or die("error: " . mysqli_error($con));

                                while ($listrowes = mysqli_fetch_array($resultas)) {
                                    $id_ccaa = $listrowes['id_ccaa'];
                                }
                            }

                            $emails = strip_tags($_POST['emails'], '<\n><br/>');
                            $emailarray = explode("\n", $emails);

                            $numrows = count($emailarray);
                            for ($i = 0; $i < $numrows; $i++) {
                                $emailarray1 = explode(',', $emailarray[$i]);
                                $nif_val = fn_filtro($con, trim($emailarray1[3]));
                                $user_val = fn_filtro($con, trim($emailarray1[1]));
                                $apellido_val = fn_filtro($con, trim($emailarray1[2]));
                                $nif_val =trim ($nif_val);//quitamos espacios en blanco
                                $user_val =trim ($user_val);//quitamos espacios en blanco
                                $apellido_val =trim ($apellido_val);//quitamos espacios en blanco
                                //en el caso que sea circunscripcion unica metemos el dato de municipio 001
                                if ($es_municipal == true) {
                                    $municipio_val = 001;
                                    if ($nif_val==""){
                                      $inmsg .= _("Tiene que indicar") ." 4 ".  _("campos separados por coma. Falta algún campo en ") . " " . $emailarray1[0] . " , " . $user_val . " , " . $nif_val . "<br/>";
                                    }
                                } else {
                                    $municipio_val = fn_filtro($con, trim($emailarray1[4]));
                                    $municipio_val =trim ($municipio_val);//quitamos espacios en blanco
                                }
                                //miramos si llegan todos los datos necesarios
                                $emailarray1[0] =trim ($emailarray1[0]);//quitamos espacios en blanco
                                if (!isset($emailarray1[0])) {
                                    $inmsg .= _("Falta correo") . " <br/>";
                                } elseif (!filter_var($emailarray1[0], FILTER_VALIDATE_EMAIL)) {
                                    $error = "error";
                                    $inmsg .= _("La direccion") . " " . $emailarray1[0] . " " . _("es erronea") . ". <br/>";
                                } else if ($_POST['provincia'] == '00') {
                                    $mensaje2 = _("ERROR ¡¡escoja una provincia!!") . " <br/>";
                                } else if ($municipio_val == "") {
                                    $error = "error";
                                    $inmsg .= _("Tiene que indicar") ." 5 ".  _("campos separados por coma. Falta algún campo en ") . " " . $emailarray1[0] . " , " . $user_val . " , " . $nif_val . "<br/>";
                                } else {


/////miramos si el usaurio ya existe
                                    $usuarios_consulta = mysqli_query($con, "SELECT ID FROM $tbn9 WHERE nif='" . $nif_val . "' or correo_usuario='" . trim($emailarray1[0]) . "' ") or die(mysqli_error($con));

                                    $total_encontrados = mysqli_num_rows($usuarios_consulta);

                                    mysqli_free_result($usuarios_consulta);



                                    if ($total_encontrados != 0) {

                                        $inmsg .= _("El Usuario") . " " . trim($emailarray1[1]) . " " . trim($emailarray1[2]) . " " . _("con correo") . " " . trim($emailarray1[0]) . "  " . _("y nif") . " " . trim($emailarray1[3]) . " " . _("ya está registrado") . ".<br/>";
                                    } else {

                                        $tipo_usuario = fn_filtro($con, $_POST['tipo_usuario']);
                                        $provincia = fn_filtro($con, $_POST['provincia']);
                                        $query = "	insert into $tbn9  (correo_usuario, nombre_usuario, apellido_usuario, nif, tipo_votante, id_provincia,id_ccaa,id_municipio )	values	 ( '" . trim($emailarray1[0]) . "', '" . $user_val . "', '" . $apellido_val . "', '" . $nif_val . "', '" . $tipo_usuario . "', '" . $provincia . "', '" . $id_ccaa . "', '" . $municipio_val . "')";
                                        $mens = _("error incluir datos de votantes");
                                        $result = db_query_id($con, $query, $mens);

                                        /* metemos un control para saber quien ha incluido este votante */
                                        $fecha = date("Y-m-d H:i:s");
                                        $accion = "1"; //uno , incluir nuevo votante
                                        $insql = "insert into $tbn17 (id_votante,id_admin,accion,fecha ) values (  \"$result\",\"" . $_SESSION['ID'] . "\",   \"$accion\", \"$fecha\" )";
                                        $mens = _("error incluir quien ha incluido el votantes");
                                        $resultado = db_query($con, $insql, $mens);



                                        //$result = @mysqli_query($con, $query) or die("<strong><font color=#FF0000 size=3>  Imposible añadir. Cambie los datos e intentelo de nuevo.</font></strong>");
                                        if (!$result) {
                                            echo "<div class=\"alert alert-danger\">
                                        <strong> UPSSS!!!!<br/>" . _("esto es embarazoso, hay un error en la conexion con la base de datos") . " </strong> </div><strong>";
                                        }
                                        if ($result) {

                                            $mensaje1 .= _("Usuario") . ": " . trim($emailarray1[1]) . "  " . trim($emailarray1[2]) . "  " . _("Correo") . ": " . trim($emailarray1[0]) . " " . _("Nif") . ": " . trim($emailarray1[3]);
                                            if ($es_municipal == false) {
                                                $mensaje1 .= " " . _("Idenitifcador municipio") . " : " . $municipio_val . "<br/>";
                                            } else {
                                                $mensaje1 .= "<br/>";
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            $inmsg .= _("No ha incluido datos de las personas que quiere incluir en el censo, por favor, complete los datos necesarios");
                        }
                    }
                    ?>
                    <?php if ($inmsg != "") { ?> <br/><div class="alert alert-warning"> <?php echo "$inmsg"; ?> </div> <?php } ?>
                    <?php echo "$mensaje2"; ?>
                    <?php if ($mensaje1 != "") { ?>
                        <br/><div class="alert alert-success"> <?= _("Estos usuarios han sido añadidos") ?>:</div>
                        <div class="alert alert-success">
                            <?php echo "$mensaje1"; ?></div><?php } ?>

                    <?= _("Para añadir nuevos suscriptores a su lista escriba la") ?> <br/>
                    <strong>  <?= _("dirección de correo, el nombre , el apellido,  el nif") ?> <?php if ($es_municipal == false) { ?> <?= _("y el identificador del municipio") ?> <?php } ?> <?= _("separadas por una coma") ?> (,) </strong>
                    <br/> <?= _("Use una línea para cada suscriptor , si desconoce el nombre ponga algo como, miembro, usuario..etc ya que este dato no se puede quedar vacío. La dirección de correo es imprescindible.") ?>.<br />
                    <br/>
                    <?= _("Ejemplo") ?>: <br/>
                    <ul>
                        carlos@yahoo.es,Carlos, Alvarez, 384955l <?php if ($es_municipal == false) { ?>,3305 <?php } ?><br/>
                        juan@iespana.es, Juan, Perez, 384752z <?php if ($es_municipal == false) { ?>,220 <?php } ?><br/>etc</ul>
                    <?= _("Recuerde elegir el tipo de usuario , añada primero los de un tipo y luego los del otro, no se pueden mezclar") ?>.

                    <form Action="<?php $_SERVER['PHP_SELF'] ?>" Method=POST >
                        <?php if ($es_municipal == false) { ?>
                            <?php
                            if ($_SESSION['nivel_usu'] == 2) {
                                // listar para meter en una lista del cuestionario buscador


                                $options = "select DISTINCT id, provincia from $tbn8  where especial=0 order by ID";
                                $resulta = mysqli_query($con, $options) or die("error: " . mysqli_error($con));

                                while ($listrows = mysqli_fetch_array($resulta)) {
                                    $id_pro = $listrows['id'];
                                    $name1 = $listrows['provincia'];
                                    $lista1 .= "<option value=\"$id_pro\"> $name1</option>";
                                }
                                ?>
                                <div  class="row">
                                  <div class="col-sm-2">
                                <h5>  <?= _("Escoja una Provincia") ?> </h5>
                              </div>
                                <div class="col-sm-5">  <select name="provincia" class="form-control custom-select"  id="provincia" ><?php echo "$lista1"; ?></select>
                                </div>
                              </div>
                                <?php
                            } else if ($_SESSION['nivel_usu'] == 3) {

                                $options = "select DISTINCT id, provincia from $tbn8  where id_ccaa=$ids_ccaa  order by ID";
                                $resulta = mysqli_query($con, $options) or die("error: " . mysqli_error($con));

                                while ($listrows = mysqli_fetch_array($resulta)) {
                                    $id_pro = $listrows['id'];
                                    $name1 = $listrows['provincia'];
                                    $lista1 .= "<option value=\"$id_pro\"> $name1</option>";
                                }
                                ?>
                                <div  class="row">
                                  <div class="col-sm-2">
                                <h5> <?= _("Escoja una Provincia") ?> </h5>
                              </div>
                                <div class="col-sm-5">
                                    <select name="provincia" class="form-control custom-select"  id="provincia" ><?php echo "$lista1"; ?></select>
                                </div>
                              </div>
                                <?php
                            } else {

                                $result2 = mysqli_query($con, "SELECT id_provincia FROM $tbn5 where id_usuario=" . $_SESSION['ID']);
                                $quants2 = mysqli_num_rows($result2);
//$row2=mysqli_fetch_row($result2);

                                if ($quants2 != 0) {

                                    while ($listrows2 = mysqli_fetch_array($result2)) {

                                        $name2 = $listrows2['id_provincia'];
                                        $optiones = mysqli_query($con, "SELECT  provincia FROM $tbn8 where ID=$name2");
                                        $row_prov = mysqli_fetch_row($optiones);
                                        $lista1 .= "<label><input  type=\"radio\" name=\"provincia\" value=\"" . $row_prov[0] . "\"  id=\"provincia\" /> " . $row_prov[1] . "</label> <br/>";
                                    }
                                    echo "$lista1";
                                } else {
                                    echo _("No tiene asignadas provincias, no podra crear votación");
                                }
                            }
                            ?>
                        <?php } else { ?>
                            <input name="provincia" type="hidden" id="provincia" value="001">
                        <?php } ?>
                        <div class="row">
                          <div class="col-sm-2">
                            <h5><?= _("Tipo de usuario") ?> : </h5>
                          </div>

                          <label for="tipo_usuario_0" class="col-sm-2 control-label text-center">
                            <input name="tipo_usuario" type="radio" id="tipo_usuario_0" value="1" />
                            <?= _("socios") ?> (1)
                          </label>
                          <label for="tipo_usuario_1" class="col-sm-2 control-label text-center">
                            <input name="tipo_usuario" type="radio" id="tipo_usuario_1" value="2" checked="checked" />
                            <?= _("simpatizantes verificado") ?>(2)
                          </label>
                          <label for="tipo_usuario_2" class="col-sm-2 control-label text-center">
                            <input name="tipo_usuario" type="radio" id="tipo_usuario_2" value="3" checked="checked" />
                            <?= _("simpatizantes") ?> (3)
                          </label>


                      </div>
                        <label for="emails"></label>
                        <textarea name="emails" cols="80" rows="30" id="emails" class="form-control" ></textarea>
                        <p>&nbsp;</p>   <input type="submit" value="<?= _("Añadir votantes de la lista") ?>"  class="btn btn-primary pull-right" > <p>&nbsp;</p>

                    </form>



</div>

                    <!--Final-->

<?php } ?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###                                                         ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que lista los votantes que ya han participado en una votación
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 5;
include('../private/inc_web/nivel_acceso.php');



$idvot = fn_filtro($con, $variables['idvot']);
$result_vot = mysqli_query($con, "SELECT id_provincia,nombre_votacion,id_ccaa,id_grupo_trabajo,demarcacion  FROM $tbn1 where id=$idvot");
$row_vot = mysqli_fetch_row($result_vot);

$id_provincia = $row_vot[0];

$nombre_votacion = $row_vot[1];
?>


                    <!--Comiezo-->
                    <?php if ($_SESSION['usuario_nivel'] == 0) { ?>
                      <div class="card-header"> <h1 class="card-title"><?= _("Participación de la votación") ?> <?php echo $nombre_votacion; ?></h1> </div>


<div class="card-body">
                        <h1></h1>


                        <?php
                        $sql = "SELECT ID FROM $tbn2  WHERE id_votacion = '$idvot' ";
                        $result = mysqli_query($con, $sql);
                        $numero = mysqli_num_rows($result); // obtenemos el número de filas

                        if ($row = mysqli_fetch_array($result)) {
                            ?>
                            <p>  <?= _("Numero de votantes") ?> <?php echo "$numero" ?> </p> <br/ >
                            <?php
                        }
                        ?>
                        <?php
                        /* esto si quisieramos saber cuantos votos hay de cada tipo de votante
                         */
                        $sql = "SELECT tipo_votante, COUNT(tipo_votante) FROM $tbn2  WHERE id_votacion = '$idvot' GROUP BY tipo_votante   ";
                        $result = mysqli_query($con, $sql);
                        ?>


                            <?php
                            if ($row = mysqli_fetch_array($result)) {
                                ?>
                                <p><?= _("Han votado") ?> </p>
                            <table id="tabla1" class="table table-striped table-bordered" >
                                <?php
                                mysqli_field_seek($result, 0);

                                do {
                                    ?>


                                    <tr>    <td width=80%>

                                            <?php
                                            if ($row[0] == 1) {
                                                echo _("Socios");
                                            } elseif ($row[0] == 2) {
                                                echo _("Simpatizantes verificados");
                                            } elseif ($row[0] == 3) {
                                                echo _("Simpatizantes");
                                            }
                                            ?> </td><td width=20%> <?php echo $row[1]; ?>
                                        </td>     </tr>
                                    <?php
                                } while ($row = mysqli_fetch_array($result));
                                ?>
                            </table>

                            <p>
                                <?php
                            } else {?>
                      <div class="alert alert-warning">
                        <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                        <i class="fa fa-times simple-remove" aria-hidden="true"></i>
                        </button>
                        <span>
                          <b> Info - </b> <?=_("¡No hay resultados de participación!");?></span>
                        </div>
                        <?php

                            }
                            ?>
                            <?php if ($es_municipal == false) { ?>
                                <?= _("votación por provincias") ?>


                                <?php
                                /* esto si quisieramos saber cuantos votos hay de cada tipo de votante
                                 */
                                $sql = "SELECT a.tipo_votante, COUNT(a.tipo_votante),a.id_provincia,b.provincia FROM $tbn2 a, $tbn8 b WHERE (a.id_provincia=b.ID) and a.id_votacion = '$idvot' GROUP BY a.id_provincia,a.tipo_votante  ";
                                $result = mysqli_query($con, $sql);
                                ?>


                                <?php
                                if ($row = mysqli_fetch_array($result)) {
                                    ?>
                                </p>
                                <p><?= _("Han votado") ?> </p>
                                <table id="tabla2" class="table table-striped table-bordered">
                                    <?php
                                    mysqli_field_seek($result, 0);

                                    do {
                                        ?>


                                        <tr>    <td width=45%><?php echo $row[3]; ?></td><td width=34%><?php
                                                if ($row[0] == 1) {
                                                    echo _("Socios");
                                                } elseif ($row[0] == 2) {
                                                    echo _("Simpatizantes verificados");
                                                } elseif ($row[0] == 3) {
                                                    echo _("Simpatizantes") . " <br/><br/>";
                                                }
                                                ?></td>
                                            <td width=21% align="right"><?php echo $row[1]; ?></td>
                                        </tr>
                                        <?php
                                    } while ($row = mysqli_fetch_array($result));
                                    ?>
                                </table>

                                <?php
                            } else {

                            }
                            ?>
                        <?php } ?>

                        <p>&nbsp;</p>
                    <?php } ?>

</div>

                    <!--Final-->

<?php } ?>

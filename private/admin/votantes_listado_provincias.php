<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###                                                         ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que lista las provincias y genera un menú lateral para coger una provincia de la que ver los censos
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{

$sql = "select DISTINCT id, provincia from $tbn8   where especial =0 order by ID ";
$result = mysqli_query($con, $sql);
if ($row = mysqli_fetch_array($result)) {
    ?>
    <div class="alert alert-info"><span><?= _("Provincias") ?></span></div>
        <ul class="nav-provincias">
            <?php
            mysqli_field_seek($result, 0);

            do {
              if(isset($id_nprov)){
                if ($id_nprov==$row['id']){
              $classSelect='class="active"';
            }else{
              $classSelect='';
            }
              }

                ?>
                <li>
                    <a href="admin.php?c=<?php echo encrypt_url('admin/votantes_listado_multi/idvot='.$idvot."&id_nprov=".$row['id']."&cen=".$cen."&lit=".$lit,$clave_encriptacion) ?>" <?php echo $classSelect ?> > <?php echo $row['provincia']; ?> </a>

                </li>



                <?php
            } while ($row = mysqli_fetch_array($result));
            ?></ul>

    <?php
} else {

    echo _("¡No se ha encontrado nada!");
}
?>
<?php } ?>

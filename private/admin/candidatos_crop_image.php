<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que da respuesta a la petición de ajax para subir las imagenes cropeadas de candidatos_crop.php
*/
if(!isset($cargaA)){
  $carga =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
  $nivel_acceso = 9;
  include('../private/inc_web/nivel_acceso.php');

      $idcat = fn_filtro_numerico($con, $variables['idcat']);

        $folderPath = $upload_cat.'/';
        $image_parts = explode(";base64,", $_POST['image']);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
        $filename= uniqid() . '.png';
        $file = $folderPath . $filename;

        $sImage =file_put_contents($file, $image_base64);
if ($sImage){

        ////miramos en la BBDD si hay fotos
            $result_img = mysqli_query($con, "SELECT imagen_pequena FROM $tbn7 WHERE id ='" . $idcat . "' ");
            $row_img = mysqli_fetch_row($result_img);
            $quants_img = mysqli_num_rows($result_img);
            if ($quants_img != 0) {

                if ($row_img[0] != "") {
                    $thumb_photo_exists = $upload_cat . "/" . $row_img[0];
                    if (file_exists($thumb_photo_exists)) {
                        unlink($thumb_photo_exists);
                    }
                }
            }


            $sSQL12 = "UPDATE $tbn7 SET  imagen_pequena='$filename' WHERE ID='" . $idcat . "'";
            mysqli_query($con, $sSQL12) or die("Imposible modificar datos");
            $texto1 = "<div class=\"alert alert-success\">" . _("Realizadas las Modificacion de la imagen en la base de datos") . "</div>";


      //  echo json_encode(["image uploaded successfully."]);
        echo $file ;

      }else{
        echo  "ERROR##<div class=\"alert alert-danger\">" . _("Error al subir la imagen") . "</div> ";
        }

      }
?>

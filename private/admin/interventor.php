<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###                                                         ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que permite crear o modificar interventores de votación
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 9;
include('../private/inc_web/nivel_acceso.php');


$idvot = fn_filtro_numerico($con, $variables['idvot']);
$fecha = date("Y-m-d H:i:s");
$row[0] = "";
$row[1] = "";
$row[2] = "";
$row[3] = "";
$row[4] = "";
$row[12] = "";

if (isset($variables['id'])) {
    $id = fn_filtro_numerico($con, $variables['id']);
    $acc = fn_filtro_nodb($variables['acc']);
} else {
    $acc = "";
}



if (ISSET($_POST["modifika_interventor"])) {

    $nombre = fn_filtro($con, $_POST['nombre']);
    $apellido = fn_filtro($con, $_POST['apellido']);
    $tipo = fn_filtro($con, $_POST['tipo']);
    if (isset($_POST['provincia'])) {
        $provincia = fn_filtro($con, $_POST['provincia']);
    } else {
        $provincia = "001";
    }

    if (empty($_POST['correo'])) {
        $texto1 = "<div class=\"alert alert-danger\">" . _("¡¡¡ERROR!!!") . " <br/> " . _("La direccion de correo es un dato necesario") . "</div>";
    } elseif (!filter_var($_POST['correo'], FILTER_VALIDATE_EMAIL)) {

        $texto1 = "<div class=\"alert alert-danger\">" . _("¡¡¡ERROR!!!") . " <br/> " . _("la direccion es erronea") . "</div>";
    } else {
        $correo = fn_filtro($con, $_POST['correo']);
        $nombre_usuario = $_SESSION['nombre_usu'];

        $sSQL = "UPDATE $tbn11 SET nombre=\"$nombre\",apellidos=\"$apellido\",  correo=\"$correo\" ,fecha_modif=\"$fecha\",  modif=\"$nombre_usuario\" ,  tipo=\"$tipo\", id_provincia=\"$provincia\" WHERE id='$id'";

        mysqli_query($con, $sSQL) or die("Imposible modificar pagina");

        $texto1 = "<div class=\"alert alert-success\">" . _("Realizadas las Modificaciones") . " <br>" . _("Asi ha quedado el interventor") . $nombre . " " . $apellido . "</div>";
    }
}

if (ISSET($_POST["add_interventor"])) {

    $nombre = fn_filtro($con, $_POST['nombre']);
    $apellido = fn_filtro($con, $_POST['apellido']);
    $tipo = fn_filtro($con, $_POST['tipo']);
    if (isset($_POST['provincia'])) {
        $provincia = fn_filtro($con, $_POST['provincia']);
    } else {
        $provincia = "001";
    }


    if (empty($_POST['correo'])) {
        $texto1 = "<div class=\"alert alert-danger\">" . _("¡¡¡ERROR!!!") . " <br/> " . _("La direccion de correo es un dato necesario") . "</div>";
    } elseif (!filter_var($_POST['correo'], FILTER_VALIDATE_EMAIL)) {

        $texto1 = "<div class=\"alert alert-danger\">" . _("¡¡¡ERROR!!!") . " <br/> " . _("la direccion es erronea") . "</div>";
    } else {
        $correo = fn_filtro($con, $_POST['correo']);
        $usuario = $_SESSION['ID'];
        $insql = "insert into $tbn11 (nombre, 	id_provincia, 	apellidos, correo,id_votacion,anadido, fecha_anadido,tipo) values (  \"$nombre\",  \"$provincia\", \"$apellido\", \"$correo\", \"$idvot\", \"$usuario\", \"$fecha\", \"$tipo\")";
        $mens = "Mensaje error añadido nuevo interventor";
        $resultados = db_query_id($con, $insql, $mens);
        if (!$resultados) {
            $texto1 = "<div class=\"alert alert-danger\">" .
                    _("Error al crear el interventor") . " <br/>" . $nombre . " " . $apellido . " " . _("con correo") . " " . $correo . "<br/>" . _("ya que no se ha añadido a la base de datos") . "	</div>";
        } else {
      //  $inres = @mysqli_query($con, $insql) or die("<strong><font color=#FF0000 size=3>  Imposible añadir. Cambie los datos e intentelo de nuevo.</font></strong>");
        $texto1 = "<div class=\"alert alert-success\">" . _("Añadido interventor") . " <br/>" . $nombre . " " . $apellido . " " . _("con correo") . " " . $correo . "<br/>" . _("a la base de datos") . "</div> ";
}

    }
}
?>


                    <!--Comiezo-->
                    <?php
                    $result_vot = mysqli_query($con, "SELECT nombre_votacion,seguridad,interventor, interventores, id_provincia, id_ccaa FROM $tbn1 where id=$idvot");
                    $row_vot = mysqli_fetch_row($result_vot);
                    ?>

                   <?php if ($acc == "modifika") { ?>
                            <?php
                            $result = mysqli_query($con, "SELECT * FROM $tbn11 where id=$id");
                            $row = mysqli_fetch_row($result);
                            ?>
                          <div class="card-header"> <h1 class="card-title">  <?= _("MODIFICAR INTERVENTOR/INTERVENTORA") ?></h1> </div>
                        <?php } else { ?>
                          <div class="card-header"> <h1 class="card-title">  <?= _("INCLUIR INTERVENTOR/INTERVENTORA") ?> </h1> </div>
                        <?php } ?>
                    <div class="card-body">
                    <h3>   <?= _("Votación") ?> :  <?php echo $row_vot[0]; ?></h3>

                    <p><?php
                        if (isset($texto1)) {
                            echo $texto1;
                        }
                        ?></p>
                    <p><a href="admin.php?c=<?php echo encrypt_url('admin/interventor_busq1/idvot='.$idvot,$clave_encriptacion) ?>" class="btn btn-primary" ><?= _("Ver interventores de esta votación") ?> </a></p>
                    <form action="<?php $_SERVER['PHP_SELF'] ?>" method=post   name="frmDatos" id="frmDatos"  class="separador" >

                        <div class="form-group row">
                            <label for="nombre" class="col-sm-3 control-label"><?= _("Nombre") ?></label>

                            <div class="col-sm-9">

                                <input name="nombre" type="text"  id="nombre" value="<?php echo "$row[2]"; ?>"   class="form-control" placeholder="<?= _("Nombre") ?>" required autofocus data-validation-required-message="<?= _("El nombre  es un dato requerido") ?>" />
                            </div></div>
                        <div class="form-group row">
                            <label for="apellido" class="col-sm-3 control-label"><?= _("Apellidos") ?></label>

                            <div class="col-sm-9">
                                <input name="apellido" type="text"  id="apellido" value="<?php echo "$row[3]"; ?>" class="form-control" placeholder="<?= _("Apellidos") ?>" required autofocus data-validation-required-message="<?= _("El apellido  es un dato requerido") ?>" />
                            </div></div>
                        <div class="form-group row">
                            <label for="correo" class="col-sm-3 control-label"><?= _("Correo") ?></label>

                            <div class="col-sm-9">
                                <input name="correo" type="email"  id="correo" value="<?php echo "$row[4]"; ?>"   class="form-control" placeholder="<?= _("Correo electronico") ?>" required  data-validation-required-message="<?= _("Por favor, ponga un correo electronico") ?>"  />
                            </div></div>

                        <?php
                       if ($row_vot[1] == 3 or $row_vot[1] == 4) {
                            if ($row_vot[2] == "si") {
                                ?>
                                <?php
                                if ($row[12] == 0) {
                                    $chekeado0 = "checked=\"checked\" ";
                                } else if ($row[12] == 2) {
                                    $chekeado1 = "checked=\"checked\" ";
                                } else if ($row[12] == 1) {
                                    $chekeado2 = "checked=\"checked\" ";
                                }
                                ?>
                                <div class="form-group row">
                                    <div class="col-sm-3"><?= _("Tipo de interventor") ?></div>

                                    <div class="col-sm-9">
                                        <label for="tipo_0" class="control-label">
                                            <input type="radio" name="tipo" value="0" id="tipo_0"  <?php
                                            if (isset($chekeado0)) {
                                                echo "$chekeado0";
                                            }
                                            ?>>
                                            <?= _("Correo") ?></label>
                                        <br>
                                        <label for="tipo_1" class="control-label">
                                            <input type="radio" name="tipo" value="2" id="tipo_1"  <?php
                                            if (isset($chekeado1)) {
                                                echo "$chekeado1";
                                            }
                                            ?> >
                                            <?= _("Especial") ?></label>
                                        <br>
                                      <label for="tipo_2" class="control-label">
                                            <input type="radio" name="tipo" value="1" id="tipo_2"  <?php
                                            if (isset($chekeado2)) {
                                                echo "$chekeado2";
                                            }
                                            ?> >
                                            <?= _("Correo") ?> + <?= _("Especial") ?></label>
                                    </div></div>
                            <?php } else {
                                ?>
                                <input name="tipo" type="hidden" id="tipo" value="0" />
                                <?php
                            }
                        } else if ($row_vot[2] == "si") {
                            ?>
                            <input name="tipo" type="hidden" id="tipo" value="2" />
                        <?php } ?>


                        <?php if ($es_municipal == false) {

                          if($row_vot[4]==000){  // si no hay cogido ninguna provincia porque no es circunscripcion estatal o autonomica
                          // decidir si estas dos sentencias if las mantenemos o hacemos que los interventores de correo solo les lleguen los de su provincia
                          // puede ser una decision interesnate para mejorar el uso de interventores.
                        //  if ($row_vot[1] == 3 or $row_vot[1] == 4) {

                              if ($row_vot[2] == "si") { ?>

                            <div class="form-group row">
                                <label for="provincia" class="col-sm-3 control-label"><?= _("Provincia") ?>
                                        <p class="text-info"> <?= _("Escoja una Provincia") ?> </p>
                                </label>

                                <div class="col-sm-9">

                                    <?php
                                    $lista1 = "";
                                    if ($_SESSION['nivel_usu'] == 2) {
                                        // listar para meter en una lista del cuestionario buscador


                                        $options = "select DISTINCT id, provincia from $tbn8  where especial=0 order by ID";
                                        $resulta = mysqli_query($con, $options) or die("error: " . mysqli_error());

                                        while ($listrows = mysqli_fetch_array($resulta)) {
                                          $id_pro = $listrows['id'];
                                          $name1 = $listrows['provincia'];

                                          if ($id_pro == $row[1]) {
                                              $check = "selected=\"selected\" ";
                                          } else {
                                              $check = "";
                                          }

                                            $lista1 .= "<option value=\"$id_pro\" $check > $name1</option>";
                                        }
                                        ?>
                                        <select name="provincia" class="form-control custom-select" id="provincia" >
                                            <?php echo "$lista1"; ?>
                                        </select>
                                        <?php
                                    } else if ($_SESSION['nivel_usu'] == 3) {

                                        $options = "select DISTINCT id, provincia from $tbn8  where id_ccaa=" . $_SESSION['id_ccaa_usu'] . "  order by ID";
                                        $resulta = mysqli_query($con, $options) or die("error: " . mysqli_error());

                                        while ($listrows = mysqli_fetch_array($resulta)) {
                                            $id_pro = $listrows['id'];
                                            $name1 = $listrows['provincia'];
                                          if ($id_pro == $row[1]) {
                                              $check = "selected=\"selected\" ";
                                              } else {
                                              $check = "";
                                              }
                                            $lista1 .= "<option value=\"$id_pro\" $check > $name1</option>";
                                        }
                                        ?>

                                        <select name="provincia" class="form-control custom-select" id="provincia" >
                                            <?php echo "$lista1"; ?>
                                        </select>
                                        <?php
                                    } else {

                                        $result2 = mysqli_query($con, "SELECT id_provincia FROM $tbn5 where id_usuario=" . $_SESSION['ID']);
                                        $quants2 = mysqli_num_rows($result2);
//$row2=mysqli_fetch_row($result2);

                                        if ($quants2 != 0) {

                                            while ($listrows2 = mysqli_fetch_array($result2)) {

                                                $name2 = $listrows2['id_provincia'];
                                                $optiones = mysqli_query($con, "SELECT  provincia FROM $tbn8 where ID=$name2");
                                                $row_prov = mysqli_fetch_row($optiones);
                                                if ($name2 == $row[1]) {
                                                    $check = "checked" ;
                                                    } else {
                                                    $check = "";
                                                    }

                                                $lista1 .= "    <label><input  type=\"radio\" name=\"provincia\" value=\"$name2\"    id=\"provincia\" $check > " . $row_prov[0] . "</label> <br/>";
                                            }
                                            echo "$lista1";
                                        } else {
                                            echo _("No tiene asignadas provincias");
                                        }
                                    }
                                  }

                               }else{
                                 // metemos la provincia que es por la circunscripcion
                                 if ($row_vot[4]!=100){
                                   ?>

                                <input name="provincia" type="hidden" id="provincia" value="<?php  echo $row_vot[4]; ?>" />

                                <?php
                              }
                               }
                              }

                              if ($row_vot[5]!=0 and $row_vot[4]==100 ){
                                $lista1="";
                                $options = "select DISTINCT id, provincia from $tbn8  where id_ccaa=" . $row_vot[5] . "  order by ID";
                                $resulta = mysqli_query($con, $options) or die("error: " . mysqli_error());

                                while ($listrows = mysqli_fetch_array($resulta)) {
                                    $id_pro = $listrows['id'];
                                    $name1 = $listrows['provincia'];
                                  if ($id_pro == $row[1]) {
                                      $check = "selected=\"selected\" ";
                                      } else {
                                      $check = "";
                                      }
                                    $lista1 .= "<option value=\"$id_pro\" $check > $name1</option>";
                                }
                                  ?>
                                  <div class="form-group row">
                                      <label for="provincia" class="col-sm-3 control-label"><?= _("Provincia") ?>
                                              <p class="text-info"> <?= _("Escoja una Provincia") ?> </p>
                                      </label>

                                      <div class="col-sm-9">
                                  <select name="provincia" class="form-control custom-select" id="provincia" >
                                      <?php echo "$lista1"; ?>
                                  </select>
                                </div>
                              </div>
                                <?php
                                }
                                  ?>


                        <?php if ($acc == "modifika") { ?>
                            <input name="modifika_interventor" type=submit  class="btn btn-primary btn-block"  id="modifika_interventor" value="<?= _("ACTUALIZAR  interventor") ?>" />
                        <?php } else { ?>
                            <input name="add_interventor" type=submit class="btn btn-primary btn-block"  id="add_interventor" value="<?= _("AÑADIR  interventor") ?>" />
                        <?php } ?>
                        <p>&nbsp;</p>
                    </form>

                    <!--Final-->
</div>
<?php } ?>

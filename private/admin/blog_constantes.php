<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###                                                         ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo para modificar las variables del blog, como facebook,twitter, etc
*/
if (!isset($cargaA)) {
    $cargaA =false;
    exit;
}
if ($cargaA!="OK") {
    exit;
} else {
    $nivel_acceso = 11;
    include('../private/inc_web/nivel_acceso.php');
    if ($_SESSION['admin_blog']==1) {
        include("../private/basicos_php/modifika_config.php");

        $file = "../private/config/config-blog.inc.php";

//<!-- facebook  -->
        if (isset($_POST["modifika_activo_facebook"])) {
            $com_string = "active_facebook = ";
            $find = $com_string . fn_filtro_nodb($_POST['valor']);
            $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
            find_replace($find, $replace, $file, $case_insensitive = true);
        }

        if (isset($_POST["modifika_url_facebook"])) {
            $com_string = "url_facebook = \"";
            $find = $com_string . fn_filtro_nodb($_POST['valor']);
            $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
            find_replace($find, $replace, $file, $case_insensitive = true);
        }
//<!--  / end -->

//<!-- twitter  -->
        if (isset($_POST["modifika_activo_twitter"])) {
            $com_string = "active_twitter = ";
            $find = $com_string . fn_filtro_nodb($_POST['valor']);
            $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
            find_replace($find, $replace, $file, $case_insensitive = true);
        }

        if (isset($_POST["modifika_url_twitter"])) {
            $com_string = "url_twitter = \"";
            $find = $com_string . fn_filtro_nodb($_POST['valor']);
            $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
            find_replace($find, $replace, $file, $case_insensitive = true);
        }
//<!--  / end -->
//<!-- Google  -->
        if (isset($_POST["modifika_activo_Google"])) {
            $com_string = "active_Google = ";
            $find = $com_string . fn_filtro_nodb($_POST['valor']);
            $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
            find_replace($find, $replace, $file, $case_insensitive = true);
        }

        if (isset($_POST["modifika_url_Google"])) {
            $com_string = "url_Google = \"";
            $find = $com_string . fn_filtro_nodb($_POST['valor']);
            $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
            find_replace($find, $replace, $file, $case_insensitive = true);
        }
//<!--  / end -->
//<!-- Linkedin  -->
        if (isset($_POST["modifika_activo_Linkedin"])) {
            $com_string = "active_Linkedin = ";
            $find = $com_string . fn_filtro_nodb($_POST['valor']);
            $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
            find_replace($find, $replace, $file, $case_insensitive = true);
        }

        if (isset($_POST["modifika_url_Linkedin"])) {
            $com_string = "url_Linkedin = \"";
            $find = $com_string . fn_filtro_nodb($_POST['valor']);
            $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
            find_replace($find, $replace, $file, $case_insensitive = true);
        }
//<!--  / end -->
//<!-- Instagram  -->
        if (isset($_POST["modifika_activo_Instagram"])) {
            $com_string = "active_Instagram = ";
            $find = $com_string . fn_filtro_nodb($_POST['valor']);
            $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
            find_replace($find, $replace, $file, $case_insensitive = true);
        }

        if (isset($_POST["modifika_url_Instagram"])) {
            $com_string = "url_Instagram = \"";
            $find = $com_string . fn_filtro_nodb($_POST['valor']);
            $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
            find_replace($find, $replace, $file, $case_insensitive = true);
        }
//<!--  / end -->
//<!-- Pinterest  -->
        if (isset($_POST["modifika_activo_Pinterest"])) {
            $com_string = "active_Pinterest = ";
            $find = $com_string . fn_filtro_nodb($_POST['valor']);
            $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
            find_replace($find, $replace, $file, $case_insensitive = true);
        }

        if (isset($_POST["modifika_url_Pinterest"])) {
            $com_string = "url_Pinterest = \"";
            $find = $com_string . fn_filtro_nodb($_POST['valor']);
            $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
            find_replace($find, $replace, $file, $case_insensitive = true);
        }
//<!--  / end -->
//<!-- Youtube  -->
        if (isset($_POST["modifika_activo_Youtube"])) {
            $com_string = "active_Youtube = ";
            $find = $com_string . fn_filtro_nodb($_POST['valor']);
            $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
            find_replace($find, $replace, $file, $case_insensitive = true);
        }

        if (isset($_POST["modifika_url_Youtube"])) {
            $com_string = "url_Youtube = \"";
            $find = $com_string . fn_filtro_nodb($_POST['valor']);
            $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
            find_replace($find, $replace, $file, $case_insensitive = true);
        }
//<!--  / end -->
//<!-- Slack  -->
        if (isset($_POST["modifika_activo_Slack"])) {
            $com_string = "active_Slack = ";
            $find = $com_string . fn_filtro_nodb($_POST['valor']);
            $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
            find_replace($find, $replace, $file, $case_insensitive = true);
        }

        if (isset($_POST["modifika_url_Slack"])) {
            $com_string = "url_Slack = \"";
            $find = $com_string . fn_filtro_nodb($_POST['valor']);
            $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
            find_replace($find, $replace, $file, $case_insensitive = true);
        }
//<!--  / end -->
//<!-- tumblr  -->
        if (isset($_POST["modifika_activo_tumblr"])) {
            $com_string = "active_tumblr = ";
            $find = $com_string . fn_filtro_nodb($_POST['valor']);
            $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
            find_replace($find, $replace, $file, $case_insensitive = true);
        }

        if (isset($_POST["modifika_url_tumblr"])) {
            $com_string = "url_tumblr = \"";
            $find = $com_string . fn_filtro_nodb($_POST['valor']);
            $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
            find_replace($find, $replace, $file, $case_insensitive = true);
        }
//<!--  / end -->
//<!-- Email  -->
        if (isset($_POST["modifika_activo_Email"])) {
            $com_string = "active_Email = ";
            $find = $com_string . fn_filtro_nodb($_POST['valor']);
            $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
            find_replace($find, $replace, $file, $case_insensitive = true);
        }

        if (isset($_POST["modifika_url_Email"])) {
            $com_string = "url_Email = \"";
            $find = $com_string . fn_filtro_nodb($_POST['valor']);
            $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
            find_replace($find, $replace, $file, $case_insensitive = true);
        }
//<!--  / end -->

        include($file); ?>

                    <!--Comiezo-->
                    <div class="card-header"> <h1 class="card-title"><?= _("Menú de redes sociales") ?></h1> </div>


                    <div class="card-body">

                    <table width="100%" border="0"  class="table table-striped">
                        <tr>
                            <th width="39%" scope="row">&nbsp;</th>
                            <td width="5%">&nbsp;</td>
                            <td width="23%">&nbsp;</td>
                            <td width="33%">&nbsp;</td>
                        </tr>

<!-- facebook  -->
                        <form name="form1" method="post" action="">
                        <tr>
                          <th scope="row"><?= _("Visualizar") ?> facebook</th>
                          <td>&nbsp;</td>
                          <td><?php
                          if ($active_facebook == true) {
                              echo _("Activo"); ?>
                              <input name="valor" type="hidden" id="valor" value="true">
                            <?php
                              $check1 = "checked=\"CHECKED\"";
                          } else {
                              echo _("Inactivo");?>
                              <input name="valor" type="hidden" id="valor" value="false">
                            <?php
                              $check2 = "checked=\"CHECKED\"";
                          } ?></td>
                            <td>
                              <label>
                              <input name="direccion_general" type="radio" id="direccion_general_0" value="true" <?php
                              if (isset($check1)) {
                                  echo $check1;
                              } ?> >
                              <?= _("Activo") ?></label>
                              <label>
                                <input type="radio" name="direccion_general" id="direccion_general_1" value="false" <?php
                                if (isset($check2)) {
                                    echo $check2;
                                } ?>>
                                <?= _("Inactivo") ?></label>


                                <input type="submit" name="modifika_activo_facebook" id="modifika_activo_facebook" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >

                                  </td>
                                </tr>
                              </form>
                        <tr>
                            <th scope="row"><?= _("URL de") ?> facebook</th>
                            <td>&nbsp;</td>
                            <td><?php echo $url_facebook; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="text" autofocus required class="form-control" id="direccion_general"  value="<?php echo $url_facebook; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $url_facebook; ?>" >
                                    <input type="submit" name="modifika_url_facebook" id="modifika_url_facebook" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>

<!--  / end -->

<!-- twitter  -->
                        <form name="form1" method="post" action="">
                        <tr>
                          <th scope="row"><?= _("Visualizar") ?> twitter</th>
                          <td>&nbsp;</td>
                          <td><?php
                          if ($active_twitter == true) {
                              echo _("Activo"); ?>
                              <input name="valor" type="hidden" id="valor" value="true">
                            <?php
                              $check3 = "checked=\"CHECKED\"";
                          } else {
                              echo _("Inactivo");?>
                              <input name="valor" type="hidden" id="valor" value="false">
                            <?php
                              $check4 = "checked=\"CHECKED\"";
                          } ?></td>
                            <td>
                              <label>
                              <input name="direccion_general" type="radio" id="direccion_general_0" value="true" <?php
                              if (isset($check3)) {
                                  echo $check3;
                              } ?> >
                              <?= _("Activo") ?></label>
                              <label>
                                <input type="radio" name="direccion_general" id="direccion_general_1" value="false" <?php
                                if (isset($check4)) {
                                    echo $check4;
                                } ?>>
                                <?= _("Inactivo") ?></label>


                                <input type="submit" name="modifika_activo_twitter" id="modifika_activo_twitter" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >

                                  </td>
                                </tr>
                              </form>
                        <tr>
                            <th scope="row"><?= _("URL de") ?> twitter</th>
                            <td>&nbsp;</td>
                            <td><?php echo $url_twitter; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="text" autofocus required class="form-control" id="direccion_general"  value="<?php echo $url_twitter; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $url_twitter; ?>" >
                                    <input type="submit" name="modifika_url_twitter" id="modifika_url_twitter" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>

<!--  / end -->
<!-- Google  -->
                        <form name="form1" method="post" action="">
                        <tr>
                          <th scope="row"><?= _("Visualizar") ?> Google+</th>
                          <td>&nbsp;</td>
                          <td><?php
                          if ($active_Google == true) {
                              echo _("Activo"); ?>
                              <input name="valor" type="hidden" id="valor" value="true">
                            <?php
                              $check5 = "checked=\"CHECKED\"";
                          } else {
                              echo _("Inactivo");?>
                              <input name="valor" type="hidden" id="valor" value="false">
                            <?php
                              $check6 = "checked=\"CHECKED\"";
                          } ?></td>
                            <td>
                              <label>
                              <input name="direccion_general" type="radio" id="direccion_general_0" value="true" <?php
                              if (isset($check5)) {
                                  echo $check5;
                              } ?> >
                              <?= _("Activo") ?></label>
                              <label>
                                <input type="radio" name="direccion_general" id="direccion_general_1" value="false" <?php
                                if (isset($check6)) {
                                    echo $check6;
                                } ?>>
                                <?= _("Inactivo") ?></label>


                                <input type="submit" name="modifika_activo_Google" id="modifika_activo_Google" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >

                                  </td>
                                </tr>
                              </form>
                        <tr>
                            <th scope="row"><?= _("URL de") ?> Google</th>
                            <td>&nbsp;</td>
                            <td><?php echo $url_Google; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="text" autofocus required class="form-control" id="direccion_general"  value="<?php echo $url_Google; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $url_Google; ?>" >
                                    <input type="submit" name="modifika_url_Google" id="modifika_url_Google" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>

<!--  / end -->
<!-- Linkedin  -->
                        <form name="form1" method="post" action="">
                        <tr>
                          <th scope="row"><?= _("Visualizar") ?> Linkedin</th>
                          <td>&nbsp;</td>
                          <td><?php
                          if ($active_Linkedin == true) {
                              echo _("Activo"); ?>
                              <input name="valor" type="hidden" id="valor" value="true">
                            <?php
                              $check7 = "checked=\"CHECKED\"";
                          } else {
                              echo _("Inactivo");?>
                              <input name="valor" type="hidden" id="valor" value="false">
                            <?php
                              $check8 = "checked=\"CHECKED\"";
                          } ?></td>
                            <td>
                              <label>
                              <input name="direccion_general" type="radio" id="direccion_general_0" value="true" <?php
                              if (isset($check7)) {
                                  echo $check7;
                              } ?> >
                              <?= _("Activo") ?></label>
                              <label>
                                <input type="radio" name="direccion_general" id="direccion_general_1" value="false" <?php
                                if (isset($check8)) {
                                    echo $check8;
                                } ?>>
                                <?= _("Inactivo") ?></label>


                                <input type="submit" name="modifika_activo_Linkedin" id="modifika_activo_Linkedin" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >

                                  </td>
                                </tr>
                              </form>
                        <tr>
                            <th scope="row"><?= _("URL de") ?> Linkedin</th>
                            <td>&nbsp;</td>
                            <td><?php echo $url_Linkedin; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="text" autofocus required class="form-control" id="direccion_general"  value="<?php echo $url_Linkedin; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $url_Linkedin; ?>" >
                                    <input type="submit" name="modifika_url_Linkedin" id="modifika_url_Linkedin" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>

<!--  / end -->
<!-- Instagram  -->
                        <form name="form1" method="post" action="">
                        <tr>
                          <th scope="row"><?= _("Visualizar") ?> Instagram</th>
                          <td>&nbsp;</td>
                          <td><?php
                          if ($active_Instagram == true) {
                              echo _("Activo"); ?>
                              <input name="valor" type="hidden" id="valor" value="true">
                            <?php
                              $check9 = "checked=\"CHECKED\"";
                          } else {
                              echo _("Inactivo");?>
                              <input name="valor" type="hidden" id="valor" value="false">
                            <?php
                              $check10 = "checked=\"CHECKED\"";
                          } ?></td>
                            <td>
                              <label>
                              <input name="direccion_general" type="radio" id="direccion_general_0" value="true" <?php
                              if (isset($check9)) {
                                  echo $check9;
                              } ?> >
                              <?= _("Activo") ?></label>
                              <label>
                                <input type="radio" name="direccion_general" id="direccion_general_1" value="false" <?php
                                if (isset($check10)) {
                                    echo $check10;
                                } ?>>
                                <?= _("Inactivo") ?></label>


                                <input type="submit" name="modifika_activo_Instagram" id="modifika_activo_Instagram" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >

                                  </td>
                                </tr>
                              </form>
                        <tr>
                            <th scope="row"><?= _("URL de") ?> Instagram</th>
                            <td>&nbsp;</td>
                            <td><?php echo $url_Instagram; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="text" autofocus required class="form-control" id="direccion_general"  value="<?php echo $url_Instagram; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $url_Instagram; ?>" >
                                    <input type="submit" name="modifika_url_Instagram" id="modifika_url_Instagram" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>

<!--  / end -->
<!-- Pinterest  -->
                        <form name="form1" method="post" action="">
                        <tr>
                          <th scope="row"><?= _("Visualizar") ?> Pinterest</th>
                          <td>&nbsp;</td>
                          <td><?php
                          if ($active_Pinterest == true) {
                              echo _("Activo"); ?>
                              <input name="valor" type="hidden" id="valor" value="true">
                            <?php
                              $check11 = "checked=\"CHECKED\"";
                          } else {
                              echo _("Inactivo");?>
                              <input name="valor" type="hidden" id="valor" value="false">
                            <?php
                              $check12 = "checked=\"CHECKED\"";
                          } ?></td>
                            <td>
                              <label>
                              <input name="direccion_general" type="radio" id="direccion_general_0" value="true" <?php
                              if (isset($check11)) {
                                  echo $check11;
                              } ?> >
                              <?= _("Activo") ?></label>
                              <label>
                                <input type="radio" name="direccion_general" id="direccion_general_1" value="false" <?php
                                if (isset($check12)) {
                                    echo $check12;
                                } ?>>
                                <?= _("Inactivo") ?></label>


                                <input type="submit" name="modifika_activo_Pinterest" id="modifika_activo_Pinterest" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >

                                  </td>
                                </tr>
                              </form>
                        <tr>
                            <th scope="row"><?= _("URL de") ?> Pinterest</th>
                            <td>&nbsp;</td>
                            <td><?php echo $url_Pinterest; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="text" autofocus required class="form-control" id="direccion_general"  value="<?php echo $url_Pinterest; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $url_Pinterest; ?>" >
                                    <input type="submit" name="modifika_url_Pinterest" id="modifika_url_Pinterest" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>

<!--  / end -->
<!-- Youtube  -->
                        <form name="form1" method="post" action="">
                        <tr>
                          <th scope="row"><?= _("Visualizar") ?> Youtube</th>
                          <td>&nbsp;</td>
                          <td><?php
                          if ($active_Youtube == true) {
                              echo _("Activo"); ?>
                              <input name="valor" type="hidden" id="valor" value="true">
                            <?php
                              $check13 = "checked=\"CHECKED\"";
                          } else {
                              echo _("Inactivo");?>
                              <input name="valor" type="hidden" id="valor" value="false">
                            <?php
                              $check14 = "checked=\"CHECKED\"";
                          } ?></td>
                            <td>
                              <label>
                              <input name="direccion_general" type="radio" id="direccion_general_0" value="true" <?php
                              if (isset($check13)) {
                                  echo $check13;
                              } ?> >
                              <?= _("Activo") ?></label>
                              <label>
                                <input type="radio" name="direccion_general" id="direccion_general_1" value="false" <?php
                                if (isset($check14)) {
                                    echo $check14;
                                } ?>>
                                <?= _("Inactivo") ?></label>


                                <input type="submit" name="modifika_activo_Youtube" id="modifika_activo_Youtube" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >

                                  </td>
                                </tr>
                              </form>
                        <tr>
                            <th scope="row"><?= _("URL de") ?> Youtube</th>
                            <td>&nbsp;</td>
                            <td><?php echo $url_Youtube; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="text" autofocus required class="form-control" id="direccion_general"  value="<?php echo $url_Youtube; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $url_Youtube; ?>" >
                                    <input type="submit" name="modifika_url_Youtube" id="modifika_url_Youtube" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>

<!--  / end -->
<!-- Slack  -->
                        <form name="form1" method="post" action="">
                        <tr>
                          <th scope="row"><?= _("Visualizar") ?> Slack</th>
                          <td>&nbsp;</td>
                          <td><?php
                          if ($active_Slack == true) {
                              echo _("Activo"); ?>
                              <input name="valor" type="hidden" id="valor" value="true">
                            <?php
                              $check15 = "checked=\"CHECKED\"";
                          } else {
                              echo _("Inactivo");?>
                              <input name="valor" type="hidden" id="valor" value="false">
                            <?php
                              $check16 = "checked=\"CHECKED\"";
                          } ?></td>
                            <td>
                              <label>
                              <input name="direccion_general" type="radio" id="direccion_general_0" value="true" <?php
                              if (isset($check15)) {
                                  echo $check15;
                              } ?> >
                              <?= _("Activo") ?></label>
                              <label>
                                <input type="radio" name="direccion_general" id="direccion_general_1" value="false" <?php
                                if (isset($check16)) {
                                    echo $check16;
                                } ?>>
                                <?= _("Inactivo") ?></label>


                                <input type="submit" name="modifika_activo_Slack" id="modifika_activo_Slack" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >

                                  </td>
                                </tr>
                              </form>
                        <tr>
                            <th scope="row"><?= _("URL de") ?> Slack</th>
                            <td>&nbsp;</td>
                            <td><?php echo $url_Slack; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="text" autofocus required class="form-control" id="direccion_general"  value="<?php echo $url_Slack; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $url_Slack; ?>" >
                                    <input type="submit" name="modifika_url_Slack" id="modifika_url_Slack" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>

<!--  / end -->
<!-- tumblr  -->
                        <form name="form1" method="post" action="">
                        <tr>
                          <th scope="row"><?= _("Visualizar") ?> tumblr</th>
                          <td>&nbsp;</td>
                          <td><?php
                          if ($active_tumblr == true) {
                              echo _("Activo"); ?>
                              <input name="valor" type="hidden" id="valor" value="true">
                            <?php
                              $check17 = "checked=\"CHECKED\"";
                          } else {
                              echo _("Inactivo");?>
                              <input name="valor" type="hidden" id="valor" value="false">
                            <?php
                              $check18 = "checked=\"CHECKED\"";
                          } ?></td>
                            <td>
                              <label>
                              <input name="direccion_general" type="radio" id="direccion_general_0" value="true" <?php
                              if (isset($check17)) {
                                  echo $check17;
                              } ?> >
                              <?= _("Activo") ?></label>
                              <label>
                                <input type="radio" name="direccion_general" id="direccion_general_1" value="false" <?php
                                if (isset($check18)) {
                                    echo $check18;
                                } ?>>
                                <?= _("Inactivo") ?></label>


                                <input type="submit" name="modifika_activo_tumblr" id="modifika_activo_tumblr" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >

                                  </td>
                                </tr>
                              </form>
                        <tr>
                            <th scope="row"><?= _("URL de") ?> tumblr</th>
                            <td>&nbsp;</td>
                            <td><?php echo $url_tumblr; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="text" autofocus required class="form-control" id="direccion_general"  value="<?php echo $url_tumblr; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $url_tumblr; ?>" >
                                    <input type="submit" name="modifika_url_tumblr" id="modifika_url_tumblr" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>

<!--  / end -->
<!-- Email  -->
                        <form name="form1" method="post" action="">
                        <tr>
                          <th scope="row"><?= _("Visualizar") ?> Email</th>
                          <td>&nbsp;</td>
                          <td><?php
                          if ($active_Email == true) {
                              echo _("Activo"); ?>
                              <input name="valor" type="hidden" id="valor" value="true">
                            <?php
                              $check19 = "checked=\"CHECKED\"";
                          } else {
                              echo _("Inactivo");?>
                              <input name="valor" type="hidden" id="valor" value="false">
                            <?php
                              $check20 = "checked=\"CHECKED\"";
                          } ?></td>
                            <td>
                              <label>
                              <input name="direccion_general" type="radio" id="direccion_general_0" value="true" <?php
                              if (isset($check19)) {
                                  echo $check19;
                              } ?> >
                              <?= _("Activo") ?></label>
                              <label>
                                <input type="radio" name="direccion_general" id="direccion_general_1" value="false" <?php
                                if (isset($check20)) {
                                    echo $check20;
                                } ?>>
                                <?= _("Inactivo") ?></label>


                                <input type="submit" name="modifika_activo_Email" id="modifika_activo_Email" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >

                                  </td>
                                </tr>
                              </form>
                        <tr>
                            <th scope="row"><?= _("Dirección de") ?> Email</th>
                            <td>&nbsp;</td>
                            <td><?php echo $url_Email; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="text" autofocus required class="form-control" id="direccion_general"  value="<?php echo $url_Email; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $url_Email; ?>" >
                                    <input type="submit" name="modifika_url_Email" id="modifika_url_Email" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>

<!--  / end -->


                    </table>

</div>
                    <!--Final-->

<?php
    }
} ?>

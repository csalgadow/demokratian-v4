<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo para bloquear correos si se está usando el sistema de correos institucionales o corporativos  
*/

if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 1;
include('../private/inc_web/nivel_acceso.php');


$inmsg = "";
$mensaje2 = "";
$mensaje1 = "";
$lista1 = "";
?>



                    <!--Comiezo-->
                    <div class="card-header"> <h1 class="card-title"><?= _("INCLUIR CORREOS Y BLOQUEARLOS DE FORMA MASIVA") ?></h1> </div>
                    <div class="card-body">

                    <p><?= _("Estas direcciones de correo no podran votar aunque tengan un correo de la empresa u organización") ?> </p>
                    <?php
                    if ($inmsg != "") {
                        echo "<div class=\"alert alert-success\">" . _("¡¡¡Error!!!  No se han  registrado estos usuarios") . ".</div>";
                        echo "$inmsg";
                    }
                    ?>
                    <?php echo "$mensaje1"; ?>


                    <?php
                    if (isset($_POST['emails'])) {
                        if ($_POST['provincia'] != '00') {

                            $provincia = fn_filtro($con, $_POST['provincia']);
                            $optiones = "select  id_ccaa from $tbn8 where ID ='" . $provincia . "'";
                            $resultas = mysqli_query($con, $optiones) or die("error: " . mysqli_error($con));

                            while ($listrowes = mysqli_fetch_array($resultas)) {
                                $id_ccaa = $listrowes['id_ccaa'];
                            }
                        }

                        $emails = strip_tags($_POST['emails'], '<\n><br/>');
                        $emailarray = explode("\n", $emails);

                        $numrows = count($emailarray);
                        for ($i = 0; $i < $numrows; $i++) {
                            $emailarray1 = explode(',', $emailarray[$i]);
                            $nombre = fn_filtro($con, trim($emailarray1[1]));
                            $apellidos = fn_filtro($con, trim($emailarray1[2]));
                            if ($nombre ==""){
                              $nombre="insttucional bloqueo";
                            }

                            if($apellidos==""){
                              $apellidos=NULL;
                            }




                            //miramos si llegan todos los datos necesarios
                            if($emailarray1[0]!="" or $emailarray1[1]!=""){
                            $esCorrecto="OK";
                            if ($emailarray1[0]!="") {
                              $emailarray1[0] =trim ($emailarray1[0]);//quitamos espacios en blanco
                                if (!filter_var($emailarray1[0], FILTER_VALIDATE_EMAIL)) {
                                $error = "error";
                                $inmsg .= _("La direccion") . " " . $emailarray1[0] . " " . _("es errónea") . " <br/>";
                                $esCorrecto="ERROR";
                                } else{
                                  $esCorrecto="OK";
                                }
                              }
                            }else{
                              $esCorrecto="ERROR";
                            }

                            if ($esCorrecto =="OK"){


/////miramos si el usaurio ya existe
                                $usuarios_consulta = mysqli_query($con, "SELECT ID FROM $tbn9 WHERE correo_usuario='" . trim($emailarray1[0]) . "' ") or die(mysqli_error($con));

                                $total_encontrados = mysqli_num_rows($usuarios_consulta);

                                mysqli_free_result($usuarios_consulta);



                                if ($total_encontrados != 0) {

                                    $inmsg .= _("El Usuario") . " " . trim($emailarray1[1]) . " " . _("con correo") . " " . trim($emailarray1[0]) . "  " . _("ya está bloqueado") . ".<br/>";
                                } else {


                                    $provincia = fn_filtro($con, $_POST['provincia']);
                                    $razon_bloqueo = fn_filtro($con, $_POST['razon_bloqueo']);
                                    $bloqueo = "si";
                                    $tipo_usuario = 3;// frzamos el tipo de usuario
                                    $municipio_val = 001; // los ponemos a todos en el municipio 001 para que no quede vacio
                                    $id_ccaa = 016;
                                    $nif_val="aleatorio_".rand(1000, 99999999);

                                    $query = "	insert into $tbn9  (correo_usuario, nombre_usuario,apellido_usuario, nif, tipo_votante, id_provincia,id_ccaa,id_municipio,bloqueo,razon_bloqueo )	values	 ( '" . trim($emailarray1[0]) . "', '" . $nombre . "', '" . $apellidos . "', '" . $nif_val . "', '" . $tipo_usuario . "', '" . $provincia . "', '" . $id_ccaa . "', '" . $municipio_val . "', '" . $bloqueo . "', '" . $razon_bloqueo . "')";
                                    $mens = "error incluir datos de votantes";
                                    $result = db_query_id($con, $query, $mens);

                                    /* metemos un control para saber quien ha incluido este votante */
                                    $fecha = date("Y-m-d H:i:s");
                                    $accion = "1"; //uno , incluir nuevo votante
                                    $insql = "insert into $tbn17 (id_votante,id_admin,accion,fecha ) values (  \"$result\",\"" . $_SESSION['ID'] . "\",   \"$accion\", \"$fecha\" )";
                                    $mens = "error incluir quien ha incluido el votantes";
                                    $resultado = db_query($con, $insql, $mens);



                                    //$result = @mysqli_query($con, $query) or die("<strong><font color=#FF0000 size=3>  Imposible añadir. Cambie los datos e intentelo de nuevo.</font></strong>");
                                    if (!$result) {
                                        echo "<div class=\"alert alert-danger\">
                                        <strong> UPSSS!!!!<br/>" . _("esto es embarazoso, hay un error en la conexion con la base de datos") . " </strong> </div><strong>";
                                    }
                                    if ($result) {

                                        $mensaje1 .= _("Usuario") . ": " . trim($emailarray1[1]) . "  " . _("Correo") . ": " . trim($emailarray1[0]);
                                        if ($es_municipal == false) {
                                            $mensaje1 .= _("Idenitifcador municipio") . " : " . $municipio_val . "<br/>";
                                        }
                                    }
                                }
                            }
                        }
                    }
                    ?>
                    <?php if ($inmsg != "") { ?> <br/><div class="alert alert-warning"> <?php echo "$inmsg"; ?> </div> <?php } ?>
                    <?php echo "$mensaje2"; ?>
                    <?php if ($mensaje1 != "") { ?>
                        <br/>
                        <div class="alert alert-success"> <?= _("Estos usuarios han sido bloqueados") ?>:</div>
                        <div class="alert alert-success">
                            <?php echo "$mensaje1"; ?></div>
                    <?php } ?>

                    <p class="text-info"><?= _("Para añadir nuevos suscriptores a su lista escriba  la dirección de correo, el nombre, los apellidos separados por una coma (,) Use una linea para cada suscriptor , si desconoce el nombre ponga algo como, miembro, usuario, etc"). ". " . _("La direccion de correo es imprescindible") ?>.</p>

                    <p class="text-info"><?= _("Ejemplo") ?>: </p>
                    <ul>
                      <li>carlos@yahoo.es, Carlos, Suarez</li>
                      <li>juan@yahoo.es, juan, Perez</li>
                      <li>paco@yahoo.es,  ,  </li>
                      <li>  etc</li>
                    </ul>


                    <form Action="<?php $_SERVER['PHP_SELF'] ?>" Method=POST class="well form-horizontal">
                        <?php if ($es_municipal == false) { ?>
                            <?php
                            if ($_SESSION['nivel_usu'] == 2) {
                                // listar para meter en una lista del cuestionario buscador


                                $options = "select DISTINCT id, provincia from $tbn8  where especial=0 order by ID";
                                $resulta = mysqli_query($con, $options) or die("error: " . mysqli_error($con));

                                while ($listrows = mysqli_fetch_array($resulta)) {
                                    $id_pro = $listrows['id'];
                                    $name1 = $listrows['provincia'];
                                    $lista1 .= "<option value=\"$id_pro\"> $name1</option>";
                                }
                                ?>
                                <h2> <?= _("Escoja una Provincia") ?> </h2>

                                <div class="col-sm-4">  <select name="provincia" class="form-control custom-select"  id="provincia" ><?php echo "$lista1"; ?></select>
                                </div>
                                <?php
                            } else if ($_SESSION['nivel_usu'] == 3) {

                                $options = "select DISTINCT id, provincia from $tbn8  where id_ccaa=$ids_ccaa  order by ID";
                                $resulta = mysqli_query($con, $options) or die("error: " . mysqli_error($con));

                                while ($listrows = mysqli_fetch_array($resulta)) {
                                    $id_pro = $listrows['id'];
                                    $name1 = $listrows['provincia'];
                                    $lista1 .= "<option value=\"$id_pro\"> $name1</option>";
                                }
                                ?>

                                <h2> <?= _("Escoja una Provincia") ?> </h2><div class="col-sm-9">
                                    <select name="provincia" class="form-control custom-select"  id="provincia" ><?php echo "$lista1"; ?></select>
                                </div>
                                <p>
                                    <?php
                                } else {

                                    $result2 = mysqli_query($con, "SELECT id_provincia FROM $tbn5 where id_usuario=" . $_SESSION['ID']);
                                    $quants2 = mysqli_num_rows($result2);
//$row2=mysqli_fetch_row($result2);

                                    if ($quants2 != 0) {

                                        while ($listrows2 = mysqli_fetch_array($result2)) {

                                            $name2 = $listrows2['id_provincia'];
                                            $optiones = mysqli_query($con, "SELECT  provincia FROM $tbn8 where ID=$name2");
                                            $row_prov = mysqli_fetch_row($optiones);
                                            $lista1 .= "<label><input  type=\"radio\" name=\"provincia\" value=\"$name2\"  checked=\"checked\"  id=\"provincia\" /> " . $row_prov[0] . "</label> <br/>";
                                        }
                                        echo "$lista1";
                                    } else {
                                        echo _("No tiene asignadas provincias, no podra crear votación");
                                    }
                                }
                                ?>
                            <?php } else { ?>
                                <input name="provincia" type="hidden" id="provincia" value="001">
                            <?php } ?>
                        </p>
                        <p>
                            <label for="razon_bloqueo"><?= _("Razon para bloquear estos correos") ?>:</label>
                            <textarea name="razon_bloqueo" cols="80" rows="3" id="razon_bloqueo" class="form-control" ></textarea>
                        </p>
                        <div class="col-sm-8"></div>
                        <label for="emails"></label>
                        <textarea name="emails" cols="80" rows="30" id="emails" class="form-control" ></textarea>
                        <p>&nbsp;</p>   <input type="submit" value="<?= _("Añadir a la lista de correos bloqueados") ?>"  class="btn btn-primary btn-block" > <p>&nbsp;</p>

                    </form>
</div>
                    <!--Final-->

<?php } ?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo para crear o modificar las asambleas
*/
if (!isset($cargaA)) {
    $cargaA =false;
    exit;
}
if ($cargaA!="OK") {
    exit;
} else {
    $nivel_acceso = 5;
    include('../private/inc_web/nivel_acceso.php');

    if (isset($variables['id'])) {
        $id = fn_filtro_numerico($con, $variables['id']);
        $acc = fn_filtro($con, $variables['acc']);
    } else {
        $acc = "";
    }


    if (isset($_POST["add_asamblea"])) {
        $creado = $_SESSION['ID'];
        $fecha = date("Y-m-d h:i:s");
        $nombre = fn_filtro($con, $_POST['nombre']);
        if (isset($_POST['provincia'])) {
            $provincia = fn_filtro($con, $_POST['provincia']);
        }
        if (isset($_POST['comunidad_autonoma'])) {
            $id_ccaa = fn_filtro($con, $_POST['comunidad_autonoma']);
        }
        $texto = fn_filtro_editor($con, $_POST['texto']);
        $tipo = fn_filtro($con, $_POST['tipo']);
        $acceso = fn_filtro($con, $_POST['acceso']);
        $activo = fn_filtro($con, $_POST['activo']);
        $tipo_usuario = fn_filtro($con, $_POST['tipo_usuario']);

        if ($_POST['tipo'] == 3) {
            $provincia = 0;
            $id_ccaa = 0;
        } elseif ($_POST['tipo'] == 2) {
            $provincia = 0;
        } else {
            //$result_ccaa = mysqli_query($con, "SELECT  id_ccaa  FROM $tbn8 where id=$provincia");
            $insql_ccaa = "SELECT  id_ccaa  FROM $tbn8 where id=$provincia";
            $mens = _("ERROR en la consulta de").  'admin/ asambleas.php' .   _("linea"); '75' ;
            $result_ccaa = db_query($con, $insql_ccaa, $mens);
            $row_ccaa = mysqli_fetch_row($result_ccaa);

            $id_ccaa = $row_ccaa[0];
        }

        $insql = "insert into $tbn4 (subgrupo, 	id_provincia, 	texto, id_ccaa,tipo, acceso,	activo , creado, tipo_votante) values (  \"$nombre\",  \"$provincia\", \"$texto\", \"$id_ccaa\", \"$tipo\", \"$acceso\", \"$activo\", \"$creado\" , \"$tipo_usuario\")";
        //  $inres = @mysqli_query($con, $insql) or die("<strong><font color=#FF0000 size=3>  Imposible añadir. Cambie los datos e intentelo de nuevo.</font></strong>");
        $mens = _("ERROR en la consulta de").  'admin/ asambleas.php' .   _("linea"); '83' ;
        $result = db_query($con, $insql, $mens);
        if (!$result) { //si no hay resultado
            $inmsg = "<div class=\"alert alert-danger\">" . _("Hay un error y no se ha realizado el añadido") . " <br/><strong>";
        } else {
            $inmsg = "<div class=\"alert alert-success\">" . _("Añadido nuevo grupo con nombre") . " <br/><strong>
	       $nombre </strong><br/>" . _("a la base de datos") . "</div>";
        }
    }

    if (isset($_POST["modifika_asamblea"])) {
        $creado = $_SESSION['ID'];
        $fecha = date("Y-m-d h:i:s");
        $nombre = fn_filtro($con, $_POST['nombre']);
        $texto = fn_filtro_editor($con, $_POST['texto']);
        $acceso = fn_filtro($con, $_POST['acceso']);
        $activo = fn_filtro($con, $_POST['activo']);
        $tipo_usuario = fn_filtro($con, $_POST['tipo_usuario']);


        $sSQL = "UPDATE $tbn4 SET subgrupo=\"$nombre\",texto=\"$texto\", acceso=\"$acceso\",	activo=\"$activo\" , creado=\"$creado\", tipo_votante=\"$tipo_usuario\" WHERE ID='$id'";
        //mysqli_query($con, $sSQL) or die("Imposible modificar pagina");
        $mens = _("ERROR en la consulta de").  'admin/ asambleas.php' .   _("linea"); ' ' ;
        $result = db_query($con, $sSQL, $mens);

        if (!$result) { //si no hay resultado
            $inmsg = "<div class=\"alert alert-danger\">" . _("Hay un error y no se ha realizado el añadido") . " <br/><strong>";
        } else {
            $inmsg = "<div class=\"alert alert-success\">" . _("Modificado  grupo con nombre") . " <br/><strong>
	       $nombre </strong><br/>" . _("a la base de datos") . "</div>";
        }
    }

    $tipo = "";
    $nombre = "";
    $id_provincia = "";
    $id_ccaa = "";
    $texto = "";
    $acceso = "";
    $activo = "";
    $tipo_votante = ""; ?>


                    <!--Comiezo-->
                    <?php
                    if ($acc == "modifika") {
                        //$result = mysqli_query($con, "SELECT * FROM $tbn4 where ID=$id");
                        $insql = "SELECT * FROM $tbn4 where ID=$id";
                        $mens = _("ERROR en la consulta de").  'admin/ asambleas.php' .   _("linea"); '129' ;
                        $result = db_query($con, $insql, $mens);

                        $row = mysqli_fetch_row($result);
                        $tipo = $row[1];
                        $nombre = $row[2];
                        $id_provincia = $row[4];
                        $id_ccaa = $row[5];
                        $texto = $row[6];
                        $acceso = $row[7];
                        $activo = $row[8];
                        $tipo_votante = $row[10];
                    } ?>
                    <div class="card-header"> <h1 class="card-title"> <?php
                        if ($acc == "modifika") {
                            echo _("MODIFICAR") . " " . _("ASAMBLEA O GRUPO DE TRABAJO");
                        } else {
                            echo _("INCLUIR NUEVA") . " " . _("ASAMBLEA O GRUPO DE TRABAJO");
                        } ?></h1>

                        </div>


                        <div class="card-body">
                    <p>&nbsp;</p>

                    <?php
                    if (isset($inmsg)) {
                        echo $inmsg;
                    } ?>

                    <form action="<?php $_SERVER['PHP_SELF'] ?>" method=post name="frmDatos" id="frmDatos" class="well form-horizontal">


                        <div class="form-group row">
                            <label for="nombre" class="col-sm-3 control-label"><?= _("Nombre") ?> </label>

                            <div class="col-sm-9">
                                <input name="nombre" type="text" autofocus required class="form-control" id="nombre" placeholder="<?= _("Nombre de la asamblea o grupo de trabajo") ?>" value="<?php echo "$nombre"; ?>" data-validation-required-message="<?= _("El nombre de la votación es un dato requerido") ?>">
                            </div>
                        </div>




                        <?php if ($es_municipal == false) { ?>
                            <div class="form-group row">
                                <div class="col-sm-3 "><?= _("TIPO") ?></div>

                                <div class="col-sm-9">
                                    <?php
                                    if ($acc == "modifika") {
                                        if ($tipo == 1) {
                                            echo _("Provincial") . "  | " . $id_provincia;
                                        } elseif ($tipo == 2) {
                                            echo _("Autonomico") . " |" . $id_ccaa;
                                        } elseif ($tipo == 3) {
                                            echo _("Estatal");
                                        } ?>


                                        <?php
                                    } else {
                                        /////si no es administrador general con nivel 0 miramos a ver que es
                                        if ($_SESSION['usuario_nivel'] <= 6 and $_SESSION['usuario_nivel'] != 0) {
                                            if ($_SESSION['nivel_usu'] == 6) {
                                                //administrador estatal
                                                ?>
                                                <input name="tipo" type="hidden" id="tipo" value="3" />

                                                <?= _("Estatal") ?>
                                                <?php
                                            } elseif ($_SESSION['nivel_usu'] == 4 or $_SESSION['nivel_usu'] == 5) {
                                                ?>
                                                <input name="tipo" type="hidden" id="tipo" value="1" />
                                                <?= _("Provincial") ?>
                                                <?php
                                            } elseif ($_SESSION['nivel_usu'] == 3) {
                                                ?>

                                                <label for="tipo_1">
                                                    <input name="tipo" type="radio" id="tipo_1" value="1"  onClick="habilita_provincial()"  />
                                                    <?= _("Provincial") ?></label>>
                                                |
                                                <label  for="tipo_2">
                                                    <input type="radio" name="tipo" value="2" id="tipo_2" onClick="habilita_autonomico()"/>
                                                    <?= _("Autonomico") ?></label>

                                                <?php
                                            }
                                        } ?>

                                        <?php
                                        ///si es siperadministrador le dejamos que acceda a todo
                                        if ($_SESSION['usuario_nivel'] == 0) {
                                            ?>  <p>
                                                <label for="tipo_1">
                                                    <input name="tipo" type="radio" id="tipo_1" value="1"  onClick="habilita_provincial()"  />
                                                    <?= _("Provincial") ?></label>
                                                |
                                                <label  for="tipo_2">
                                                    <input type="radio" name="tipo" value="2" id="tipo_2" onClick="habilita_autonomico()"/>
                                                    <?= _("Autonomico") ?></label>
                                                |
                                                <label  for="tipo_3">
                                                    <input name="tipo" type="radio" id="tipo_3" value="3" checked="checked"  onClick="habilita_estatal()"  />
                                                    <?= _("Estatal") ?></label>

                                            </p>
                                        <?php
                                        } ?>

                                    </div></div>
                                <div class="form-group row">
                                    <label for="nombre" class="col-sm-3 control-label"> </label>

                                    <div class="col-sm-9">


                                        <?php
                                        $lista1 = "";
                                        if ($_SESSION['usuario_nivel'] <= 6 and $_SESSION['usuario_nivel'] != 0) {
                                            if ($_SESSION['nivel_usu'] == 4 or $_SESSION['nivel_usu'] == 5) {
                                                //$result2 = mysqli_query($con, "SELECT id_provincia FROM $tbn5 where id_usuario=" . $_SESSION['ID']);
                                                $insql_result2 = "SELECT id_provincia FROM $tbn5 where id_usuario=" . $_SESSION['ID'];
                                                $mens = _("ERROR en la consulta de").  'admin/ asambleas.php' .   _("linea"); '254' ;
                                                $result2 = db_query($con, $insql_result2, $mens);
                                                $quants2 = mysqli_num_rows($result2);
                                                //$row2=mysqli_fetch_row($result2);

                                                if ($quants2 != 0) {
                                                    while ($listrows2 = mysqli_fetch_array($result2)) {
                                                        $name2 = $listrows2['id_provincia'];
                                                        //$optiones = mysqli_query($con, "SELECT  provincia FROM $tbn8 where ID=$name2");
                                                        $insql =  "SELECT  provincia FROM $tbn8 where ID=$name2";
                                                        $mens = _("ERROR en la consulta de").  'admin/ asambleas.php' .   _("linea"); '265' ;
                                                        $optiones = db_query($con, $insql, $mens);
                                                        $row_prov = mysqli_fetch_row($optiones);
                                                        $lista1 .= "    <label><input  type=\"radio\" name=\"provincia\" value=\"$name2\"  checked=\"checked\"  id=\"provincia\" /> " . $row_prov[0] . "</label> <br/>";
                                                    }
                                                } ?> <?php echo "$lista1"; ?> <br/>
                                                <?php
                                            } elseif ($_SESSION['nivel_usu'] == 3) {
                                                // si es administrador autonomico?>
                                                <div id="autonomico" class="caja_de_display" style="display:none">
                                                    <div align="left">
                                                        <?php
                                                        echo $_SESSION['id_ccaa_usu']; ?>

                                                    </div>
                                                </div>


                                                <div id="provincial"   class="caja_de_display"  style="display:none" >

                                                    <div align="left">
                                                        <?php
                                                        $ids_ccaa = $_SESSION['id_ccaa_usu'];

                                                //$result2 = mysqli_query($con, "SELECT  id, provincia FROM $tbn8 where id_ccaa=" . $_SESSION['id_ccaa_usu']);
                                                $insql2 = "SELECT  id, provincia FROM $tbn8 where id_ccaa=" . $_SESSION['id_ccaa_usu'];
                                                $mens = _("ERROR en la consulta de").  'admin/ asambleas.php' .   _("linea"); '289' ;
                                                $result2 = db_query($con, $insql2, $mens);

                                                while ($listrows2 = mysqli_fetch_array($result2)) {
                                                    $name2 = $listrows2[provincia];
                                                    $id2 = $listrows2[id];
                                                    $lista1 .= "    <label><input  type=\"radio\" name=\"provincia\" value=\"$id2\"  checked=\"checked\"  id=\"provincia\" /> " . $name2 . "</label> <br/>";
                                                } ?> <?php echo "$lista1"; ?> <br/>



                                                    </div>
                                                </div>


                                                <?php
                                            }
                                        } else {
                                            ?>
                                            <div id="autonomico" class="caja_de_display" style="display:none">
                                                <div align="left">


                                                    <p> <?= _("Escoja una Comunidad Autonoma") ?> <br /><?= _("si desea que la demarcacion sea Autonomica") ?></p>
                                                    <?php
                                                    $lista_ccaa = "";
                                            $options_ccaa = "select DISTINCT ID, ccaa from $tbn3  order by ID";
                                          //  $resulta_ccaa = mysqli_query($con, $options_ccaa) or die("error: " . mysqli_error($con));
                                            $mens = _("ERROR en la consulta de").  'admin/ asambleas.php' .   _("linea"); '316' ;
                                            $resulta_ccaa = db_query($con, $options_ccaa, $mens);

                                            while ($listrows_ccaa = mysqli_fetch_array($resulta_ccaa)) {
                                                $id_ccaa = $listrows_ccaa['ID'];
                                                $name_ccaa = $listrows_ccaa['ccaa'];
                                                $lista_ccaa .= "<option value=\"$id_ccaa\"> $name_ccaa</option>";
                                            } ?>
                                                    <select name="comunidad_autonoma" class="form-control custom-select" id="comunidad_autonoma" >

                                                        <?php echo "$lista_ccaa"; ?>
                                                    </select>


                                                </div>
                                            </div>



                                            <div id="provincial"  class="caja_de_display"   style="display:none" >

                                                <div align="left">
                                                    <?php
                                                    $lista1 = "";
                                            $options = "select DISTINCT id, provincia from $tbn8  where especial=0 order by ID";
                                            //$resulta = mysqli_query($con, $options) or die("error: " . mysqli_error($con));

                                            $mens = _("ERROR en la consulta de").  'admin/ asambleas.php' .   _("linea"); '344' ;
                                            $resulta = db_query($con, $options, $mens);

                                            while ($listrows = mysqli_fetch_array($resulta)) {
                                                $id_pro = $listrows['id'];
                                                $name1 = $listrows['provincia'];
                                                $lista1 .= "<option value=\"$id_pro\"> $name1</option>";
                                            } ?>
                                                    <p> <?= _("Escoja una Provincia") ?> <br /><?= _("si desea que la demarcacion sea Provincial") ?></p>

                                                    <select name="provincia" class="form-control custom-select" id="provincia" ><?php echo "$lista1"; ?></select>
                                                    <br/>


                                                </div>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>

                                </div></div>
                        <?php } else { ?>
                            <input name="tipo" type="hidden" id="tipo" value="3" />

                        <?php } ?>
                        <div class="form-group row">
                            <div class="col-sm-3"><?= _("Forma de acceso al grupo") ?></div>

                            <div class="col-sm-9">
                                <?php
                                if ($acceso == 1) {
                                    $chekeado11 = "checked=\"checked\" ";
                                } elseif ($acceso == 2) {
                                    $chekeado12 = "checked=\"checked\" ";
                                } else {
                                    $chekeado13 = "checked=\"checked\" ";
                                } ?>
                                <input name="acceso" type="radio" id="acceso_0" value="1"  <?php
                                if (isset($chekeado11)) {
                                    echo "$chekeado11";
                                } ?> />
                                <label for="acceso_0">       <?= _("Abierto (NO necesita validación para suscribirse)") ?></label>
                                <br />

                                <input name="acceso" type="radio" id="acceso_1" value="2"   <?php
                                if (isset($chekeado12)) {
                                    echo "$chekeado12";
                                } ?> />
                                <label for="acceso_1">       <?= _("Administrado (Necesita que los administradores validen el acceso)") ?></label>
                                <br/>
                                <input name="acceso" type="radio" id="acceso_2" value="3"   <?php
                                if (isset($chekeado13)) {
                                    echo "$chekeado13";
                                } ?> />
                                <label for="acceso_2">       <?= _("Cerrado (Solo los administradores añaden usuarios)") ?></label>





                            </div></div>
                        <div class="form-group row">
                            <div class="col-sm-3"><?= _("Activo") ?>  </div>

                            <div class="col-sm-9">
                                <?php
                                if ($activo == 2) {
                                    $chekeado32 = "checked=\"checked\" ";
                                } else {
                                    $chekeado31 = "checked=\"checked\" ";
                                } ?>


                                <input name="activo" type="radio" id="activo_0" value="1"  <?php
                                if (isset($chekeado31)) {
                                    echo "$chekeado31";
                                } ?>  />
                                <label for="activo_0">       <?= _("Si") ?></label>
                                <br />

                                <input type="radio" name="activo" value="2" id="activo_1"  <?php
                                if (isset($chekeado32)) {
                                    echo "$chekeado32";
                                } ?> />
                                <label for="activo_1">        <?= _("No") ?></label>
                            </div></div>





                        <div class="form-group row">
                            <div class="col-sm-3 "><?= _("TIPO DE VOTANTE") ?></div>
                            <div class="col-sm-9">
                                <?php
                                if ($tipo_votante == 5) {
                                    $chekeado45 = "checked=\"checked\" ";
                                } elseif ($tipo_votante == 2) {
                                    $chekeado42 = "checked=\"checked\" ";
                                } elseif ($tipo_votante == 3) {
                                    $chekeado43 = "checked=\"checked\" ";
                                } else {
                                    $chekeado41 = "checked=\"checked\" ";
                                } ?>


                                    <input name="tipo_usuario" type="radio" id="tipo_usuario_0" value="1"  <?php
                                    if (isset($chekeado41)) {
                                        echo "$chekeado41";
                                    } ?> />
                                  <label for="tipo_usuario_0">   <?= _("Solo socios") ?></label><br/>

                                    <input type="radio" name="tipo_usuario" value="2" id="tipo_usuario_1"  <?php
                                    if (isset($chekeado42)) {
                                        echo "$chekeado42";
                                    } ?>/>
                                    <label for="tipo_usuario_0"> <?= _("Socios y simpatizantes verificados") ?></label><br/>
                                <input type="radio" name="tipo_usuario" value="3" id="tipo_usuario_3"  <?php
                                if (isset($chekeado43)) {
                                    echo "$chekeado43";
                                } ?>/>
                                <label for="tipo_usuario_0"> <?= _("Socios y simpatizantes") ?></label><br/>
                               <!-- <input type="radio" name="tipo_usuario" value="5" id="tipo_usuario_2"  <?php
                                if (isset($chekeado45)) {
                                    echo "$chekeado45";
                                } ?>/>
                                Abierta (5) -->
                            </div></div>

                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label for="texto" ><?= _("Texto") ?></label>


<textarea cols="80" id="texto" name="texto" rows="10"><?php echo "$texto"; ?></textarea>

                                <script type="text/javascript" src="assets/ckeditor/ckeditor.js"></script>

                                <script>
                                 CKEDITOR.replace( 'texto', {
                                  height: 250,

                                  extraPlugins: 'imgbrowse',
                                  filebrowserImageBrowseUrl: '/assets/ckeditor/plugins/imgbrowse/imgbrowse.html?imgroot=<?php echo $baseUrl; ?>',
                                  <?php if ($_SESSION['usuario_nivel']<=6) {     // nivel para poder añadir imagenes?>
                                  filebrowserImageUploadUrl: 'aux_vota.php?c=<?php echo encrypt_url('basicos_php/imageUpload/conec=OK', $clave_encriptacion) ?>',
                                  <?php } ?>
                                //  filebrowserUploadUrl: "upload.php",

                                 });
                                </script>

                                <p>&nbsp;</p>


                                <?php if ($acc == "modifika") { ?>
                                    <input name="modifika_asamblea" type=submit  class="btn btn-primary btn-block"  id="add_asamblea" value="<?= _("MODIFICAR") ?>." ". <?= _("esta  asamblea o grupo") ?>" />
                                <?php } else { ?>
                                    <input name="add_asamblea" type=submit class="btn btn-primary btn-block"  id="add_asamblea" value="<?= _("CREAR una nueva asamblea o grupo") ?>" />
                                <?php } ?>
                                </form>




                            </div>
                        </div>


</div>
<script src='assets/js/admin/admin_funciones.js' type='text/javascript'></script>
                        <!--Final-->

<?php
} ?>

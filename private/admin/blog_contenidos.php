<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que crea y modifica los contenidos del blog en todas las categorías
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 9;
include('../private/inc_web/nivel_acceso.php');
if($_SESSION['admin_blog']==1){

$fecha = date("Y-m-d h:i:s");
$tam = 450; //tamaño en Kb
$size = $tam * 1024; // tamaño maximo de los archivos en bits
$situacion = "";

if (ISSET($_POST["add_pagina"])) {


    $titulo = fn_filtro($con, $_POST['titulo']);
    $texto = fn_filtro_editor($con, $_POST['texto']);
    $id_categoria = fn_filtro($con, $_POST['id_categoria']);
    $activo = fn_filtro($con, $_POST['activo']);
	  $fecha_form = fn_filtro($con, $_POST['fecha_form']);
    $grabar_bbdd = 0;
    $msg="";

    if ($_FILES['fileToUpload']['name']) {  ////miramos si hay archivo a subir
        $imagen_cab = basename($_FILES["fileToUpload"]["name"]);
        $tamano = $_FILES ['fileToUpload']['size'];
        $target_file = $upload_cat . "/" . $imagen_cab;
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

        // Check si existe el archivo
        if (file_exists($target_file)) {
            $msg = $msg . '<div class="alert alert-danger">' . _("Un archivo con ese nombre ya existe") . '</div>';
            $uploadOk = 0;
        }

        //miramos el tipo de archivo si es de imagen
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
            $msg = $msg . '<div class="alert alert-danger">' . _("Tu archivo tiene que ser JPG o GIF. Otros archivos no son permitidos") . '</div>';
            $uploadOk = 0;
            $imagen_cab = "";
        }

        if ($uploadOk == 1) {
            $ha_subido = move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file);
            $codigo_error = $_FILES['fileToUpload']['error'];
            if ($tamano > $size) {
                unlink($target_file);
                $msg = '<div class="alert alert-danger">' . _("Error, La imagen es demasiado grande, superior a") . ": " . $tam . "KB. <strong>" . _("La imagen no se ha subido").".</strong> "._("Tiene que ir al listado para modificar esta entrada y subir una imagen") . "</div>";
            } else {
                $msg = '<div class="alert alert-success">' . _("La imagen") . ' ' . $imagen_cab . ' ' . _("ha sido subida satisfactoriamente") . '</div>';
                $grabar_bbdd = 1; //´
            }

            if ($ha_subido == false) {
                $msg .= '<div class="alert alert-danger">' . _("ERRROR!! Hay un error inesperado al subir su archivo, codigo") . ': ' . $codigo_error . '  </div>';
                $grabar_bbdd = 0;
            }
        } else {
            $grabar_bbdd = 0;
        }
    } else {
        $grabar_bbdd = 0;
    }
$anadido = $_SESSION['ID'];

    if ($grabar_bbdd == 1) {

        $insql = "insert into $tbn33 (id_incluido,	titulo,	texto,	imagen,	id_categoria,		activo, fecha	) values (\"$anadido\", \"$titulo\", \"$texto\", \"$imagen_cab\", \"$id_categoria\",  \"$activo\", \"$fecha_form\")";
        $mens = _("Error al añadir una pagina de candidatos externa");
      //  $result = db_query($con, $insql, $mens);
        $result = db_query_id($con, $insql, $mens); // para coger la ultima id
        if (!$result) {
            $inmsg = '<div class="alert alert-danger">' . _("Hay un error al incluir los datos en la BBDD") . '</div>';
        } else {
          $id=$result;
            $inmsg = '<div class="alert alert-success"> ' . _("Añadido contenido a la base de datos") . ' </div>';
        }
    } else {  // si no se ha subido la imagen, añadimos los datos pero sin nombre de imagen
        $insql = "insert into $tbn33 (id_incluido,	titulo,	texto,	id_categoria,		activo, fecha	) values (\"$anadido\", \"$titulo\", \"$texto\", \"$id_categoria\",  \"$activo\", \"$fecha_form\")";
        $mens = _("Error al añadir una pagina de candidatos externa");
        $result = db_query_id($con, $insql, $mens); // para coger la ultima id
        if (!$result) {

            $inmsg = '<div class="alert alert-danger">' . _("Hay un error al incluir los datos en la BBDD") . '</div>';
        } else {
          $id=$result;
            $inmsg = '<div class="alert alert-success"> ' . _("Añadido contenido a la base de datos") . ' </div>';
        }
    }
}



if (ISSET($_POST["modifika_pagina"])) {
    $titulo = fn_filtro($con, $_POST['titulo']);
    $texto = fn_filtro_editor($con, $_POST['texto']);
    $id_categoria = fn_filtro($con, $_POST['id_categoria']);
    $activo = fn_filtro($con, $_POST['activo']);
    $fecha_form = fn_filtro($con, $_POST['fecha_form']);
    $id = fn_filtro_numerico($con, $variables['id']);
    $sube_img = 0;
    $msg="";

    if ($_FILES['fileToUpload']['name']) {  ////miramos si hay archivo a subir
        $imagen_cab = basename($_FILES["fileToUpload"]["name"]);
        $tamano = $_FILES ['fileToUpload']['size'];
        $target_file = $upload_cat . "/" . $imagen_cab;
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

        // Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
            $msg .= '<div class="alert alert-danger">' . _("Perdone, solo estan permitidos archivos con extension JPG, JPEG, PNG & GIF") . ' .';
            $msg .= '<br/>' . _("su archivo no ha sido incluido") . '.</div>';
            $sube_img = 0;
            $uploadOk = 0;
        }
            //miramos si existe un fichero con el mismo nombre
        if (file_exists($target_file)) {
            $msg .= '<div class="alert alert-danger">' . _("Perdone, ya existe un archivo con ese nombre") . ' </br> ' . _("su archivo no ha sido incluido") . '.</div>';

              /*  if ($borradoOk == 1) {//si hemos borrado una imagen, pero no hemos podido subir la imagen porque existe otra, actualizamos la bbdd y dejamos el campo vacio)
                    $imagen_cab = "";
                    $sube_img = 1; //
                    $uploadOk2 = 0;
                }*/
                $sube_img = 0;
                $uploadOk = 0;
            }

            if ($uploadOk == 1) { // si esta todo correcto, subimos el fichero
                $ha_subido = move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file);
                $codigo_error = $_FILES['fileToUpload']['error'];
                if ($tamano > $size) {
                    unlink($target_file);
                    $msg = '<div class="alert alert-danger">' . _("Error, La imagen es demasiado grande, superior a") . ': ' . $tam . 'KB. <strong>' . _("La imagen no se ha subido").'.</strong> </div>';
                    $imagen_cab = "";
                    $sube_img = 0; //
                } else {
                    $msg = '<div class="alert alert-success"> ' . _("La imagen") . ' ' . $imagen_cab .'  '. _("ha sido subida satisfactoriamente") . ' </div>';
                    $sube_img = 1; //´
                }

                if ($ha_subido == false) {
                    $msg .= '<div class="alert alert-danger">' . _("ERRROR!! Hay un error inesperado al subir su archivo, codigo") . ': ' . $codigo_error .  ' </div>';
                    $sube_img = 0;
                }else{
                  // si se ha subido correctamente la imagen, comprobamos si habia imagen antugua y la borramos
                  if (ISSET($_POST["row_imagen"])) {
                    $target_file_old = $upload_cat . "/" . $_POST["row_imagen"];
                    unlink($target_file_old);
                  }
                }
            }

    } else {
        //$grabar_bbdd=1;
    }

    if ($sube_img == 1) { //metemos los datos  con el nombre de la imagen
        $sSQL = "UPDATE $tbn33 SET  titulo=\"$titulo\",texto=\"$texto\",  imagen=\"$imagen_cab\" ,id_categoria=\"$id_categoria\",activo=\"$activo\",   fecha=\"$fecha_form\"	 WHERE id=$id";
        $mens = _("Error al añadir el contenido");
        $result = db_query($con, $sSQL, $mens);

        if (!$result) {
            $inmsg = _("Hay un error") . " " . $idvot;
        } else {
            $inmsg = '<div class="alert alert-success"> ' . _("Realizadas las Modificaciones") . '</div>';
        }
    } else {  //metemos los datos sin la imagen
        $sSQL = "UPDATE $tbn33 SET  titulo=\"$titulo\",texto=\"$texto\", id_categoria=\"$id_categoria\",activo=\"$activo\",  fecha=\"$fecha_form\"  WHERE id=$id";
        $mens = _("Error al añadir el contenido");
        $result = db_query($con, $sSQL, $mens);

        if (!$result) {
            $inmsg = '<div class="alert alert-danger"> ' . _("Hay un error al actualizar la base de datos") . ' </div>';
        } else {
            $inmsg = '<div class="alert alert-success"> ' . _("Realizadas las Modificaciones") . ' </div>';
        }
    }
}


  if (isset($variables['idcont'])) {
      $idcont = fn_filtro_numerico($con, $variables['idcont']);
  }else if (isset($id_categoria)){
    $idcont = $id_categoria;
  }else{
    $idcont = "%";
  }



  if (isset($variables['id'])){

      $acc = "modifika";

      $id = fn_filtro_numerico($con, $variables['id']);
      $result = mysqli_query($con, "SELECT id_incluido,	titulo,	texto,	imagen,	id_categoria,	zona_pagina,	activo, fecha	 FROM $tbn33 where id=$id");
      $row = mysqli_fetch_row($result);

      $row_titulo = $row[1];
      $row_texto = $row[2];
      $row_imagen = $row[3];
      $row_id_categoria = $row[4];
      $row_zonapagina = $row[5];
      $row_activo = $row[6];
      $row_fecha = $row[7];
      $titulo_h1= _("MODIFICAR CONTENIDO");

  }else{
      $acc = "";
      $row_titulo = "";
      $row_texto = "";
      $row_imagen = "";
      $row_id_categoria = "";
      $row_zonapagina = "";
      $row_activo = "";
      $row_fecha = "";

      $titulo_h1= _("NUEVO CONTENIDO");
  }

?>


<div class="card-header-votaciones "> <h1 class="card-title"><?php echo $titulo_h1; ?></h1> </div>

<div class="card-body">


                    <!--Comiezo-->

                <div class="row">
                  <div class="col-sm-8"></div>
                  <div class="col-sm-4">
                    <a href="admin.php?c=<?php echo encrypt_url('admin/blog_contenidos_list/idcont='.$idcont,$clave_encriptacion) ?>" class="btn btn-primary btn-block"><?= _("Volver al directorio de contenidos de esta categoria") ?></a>
                    <a href="admin.php?c=<?php echo encrypt_url('admin/blog_contenidos_list/idcont=%',$clave_encriptacion) ?>" class="btn btn-primary btn-block"><?= _("Volver al directorio de contenidos") ?></a>
                    <?php if($acc == "modifika"){?>
                      <a href="admin.php?c=<?php echo encrypt_url('admin/blog_contenidos/idcont='.$idcont,$clave_encriptacion) ?>" class="btn btn-primary btn-block"><?= _("Añadir un contenido") ?></a>
                    <?php } ?>
                  </div>
                </div>


                    <p>&nbsp;</p>


                    <?php  // Mensajes del sistema si se ha subido o modidificado bien
                    if (isset($inmsg)) {
                        echo "$inmsg";
                    }
                    ?>
                    <?php
                    if (isset($msg)) {
                        echo "$msg";
                    }
                    ?>
                    <form action="<?php $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data" method=post class="well form-horizontal" id="form1">
                      <?php if ($acc == "modifika") { ?>
                          <div class="form-group row">
                              <label for="presentacion" class="col-sm-3 control-label"><?= _("Enlace externo a la pagina") ?></label>
                              <div class="col-sm-7">
                                  <?php echo $url_vot; ?>?c=page&p=<?php echo base64_encode($id); ?>&title=<?php echo urls_amigables($row_titulo); ?>
                               </div>
                            <!--   <div class="col-sm-2">
                                 <a href="javascript:void(0);" data-href="aux_blog.php?c=page&p=<?php echo base64_encode($id); ?>" class="openextraLargeModal btn btn-primary btn-xs"><?= _("Ver previo") ?></a>
                               </div>-->
                          </div>
                      <?php } ?>


                      <div class="form-group row">
                          <label for="nombre" class="col-sm-3 control-label"><?= _("Titulo") ?> </label>

                          <div class="col-sm-9">
                              <input name="titulo" type="text"  id="titulo" value="<?php echo "$row_titulo"; ?>"  class="form-control"  required autofocus />
                          </div>
                      </div>


                      <div class="form-group row">

                              <label for="texto"  class="col-sm-3 control-label" ><?= _("Texto") ?></label>

                              <div class="col-sm-9">
                              <textarea cols="80" id="texto" name="texto" rows="10"><?php echo "$row_texto"; ?></textarea>
                              <!-- ckeditor -->
                              <script type="text/javascript" src="assets/ckeditor/ckeditor.js"></script>

                              <script>
                               CKEDITOR.replace( 'texto', {
                                height: 250,
                                extraPlugins: 'imgbrowse',
                                filebrowserImageBrowseUrl: '/assets/ckeditor/plugins/imgbrowse/imgbrowse.html?imgroot=<?php echo $baseUrl; ?>',
                                <?php if($_SESSION['usuario_nivel']<=6){     // nivel para poder añadir imagenes  ?>
                                filebrowserImageUploadUrl: 'aux_vota.php?c=<?php echo encrypt_url('basicos_php/imageUpload/conec=OK',$clave_encriptacion) ?>',
                                <?php } ?>
                              //  filebrowserUploadUrl: "upload.php",

                               });

                              </script>
                          </div>
                      </div>


                      <div class="form-group row">
                          <label for="padre" class="col-sm-3 control-label"> <?= _("Categoria") ?> </label>

                          <div class="col-sm-4">
                            <?php
                              $lista1="";
                            $sql = "SELECT id, titulo FROM $tbn31 WHERE activo='1'  ORDER BY 'id' ";
                            $resulta =  mysqli_query($con, $sql);


                            while ($listrows = mysqli_fetch_array($resulta)) {
                                $id_pro = $listrows['id'];
                                $name1 = $listrows['titulo'];
                                if ($id_pro == $idcont) {
                                    $check = "selected=\"selected\" ";
                                } else if ($id_pro == $row_id_categoria) {
                                    $check = "selected=\"selected\" ";
                                } else {
                                    $check = "";
                                }
                                $lista1 .= "<option value=\"$id_pro\"  $check> $name1</option>";
                            }
                            ?>
                            <select name="id_categoria" class="form-control custom-select" id="id_categoria" >

                                <?php echo "$lista1"; ?>
                            </select>



                          </div>
                        </div>


                        <?php
                        if ($acc == "modifika" ) {
                            if ($row_activo == 1) {
                                $chekeado1 = "checked=\"checked\" ";
                                $chekeado2 = "";
                            } else {
                                $chekeado1 = "";
                                $chekeado2 = "checked=\"checked\" ";
                            }
                        }else{
                          $chekeado2 = 'checked="checked"';
                          $chekeado1 = "";
                        }
                        ?>
                        <div class="form-group row">
                            <div class="col-sm-3 "><?= _("Estado") ?></div>
                            <div class="col-sm-9">
                                <label>
                                    <input name="activo" type="radio" id="activo_1" value="1"  <?php echo "$chekeado1"; ?>/>
                                    <?= _("Publicado") ?></label><span class="label label-warning"></span> <br/>
                                <label>
                                    <input name="activo" type="radio" id="activo_2" value="0"  <?php echo "$chekeado2"; ?>/>
                                    <?= _("Sin publicar") ?></label>

                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="fecha_form" class=" col-md-3 control-label"> <?= _("Fecha") ?> </label>

                            <div class=" col-md-3">

                                <?php
                                if ($acc == "modifika") {

                                    $fecha_i = date("Y-m-d", strtotime($row_fecha));
                                } else {

                                    $fecha_i = date("Y-m-d ");
                                }
                                ?>
                                <input  name="fecha_form" type="date" class="form-control" id="fecha_form" value="<?php echo "$fecha_i"; ?>"
                                placeholder="aaaa-mm-dd" required
                                oninvalid="this.setCustomValidity('<?= _("La fecha de inicio es un dato requerido") ?>')"
                                oninput="this.setCustomValidity('')"
                                required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}"
                                 />
                            </div>

                            <div class=" col-md-4">

                            </div>

                        </div>





              <!--          <?php

                        if ($acc == "modifika" ) {
                            if ($row_id_categoria == 0) {
                                $chekeado3 = "checked=\"checked\" ";
                                $chekeado4 = "";
                            } else {
                                $chekeado3 = "";
                                $chekeado4 = "checked=\"checked\" ";
                            }
                        }else{
                          $chekeado3 = "checked=\"checked\"";
                          $chekeado4 = "";
                        }
                        ?>
                        <div class="form-group row">
                            <div class="col-sm-3"><?= _("Tipo de cabecera") ?></div>
                            <div class="col-sm-9">
                                <label>
                                    <input name="id_categoria" type="radio" id="id_categoria_1" value="0"  <?php echo "$chekeado3"; ?>/>
                                    <?= _("Cabecera fija") ?></label>
                                    <br/>
                                <label>
                                    <input name="id_categoria" type="radio" id="id_categoria_2" value="1"  <?php echo "$chekeado4"; ?>/>
                                    <?= _("Cabecera tipo Slider") ?></label>

                            </div>
                        </div>-->


                        <div class="form-group row">
                            <label for="fileToUpload" class="col-sm-3 control-label"><?= _("Subir una imagen") ?></label></P>

                            <div class="col-sm-9"><label>
                                    <input type="hidden" name="MAX_FILE_SIZE" value="300000000">
                                    <input type="file" name="fileToUpload" id="fileToUpload">

                                    <span id="image"></span><br>
                                </label> <br />

                            </div>
                        </div>
                        <?php
                        if ($acc == "modifika" ) {
                            if ($row_imagen != "") {
                                ?>

                                <div class="form-group">
                                    <label for="row_imagen" class="col-sm-3 control-label"><?= _("Imagen principal contenido") ?></label>

                                    <div class="col-sm-9">
                                        <img src="<?php echo $upload_cat; ?>/<?php echo $row_imagen; ?>" class="img-responsive" alt="<?php echo $nombre_votacion; ?>">
                                    </div>
                                </div>
                                <input type="hidden" name="row_imagen" id="row_imagen" value="<?php echo $row_imagen; ?>">

                                <?php
                            }
                        }
                        ?>



                        <div class="form-group">
                            <div class="col-sm-12">

                                <p>&nbsp;</p>

                                <?php if ($acc == "modifika" ) { ?>
                                  <input name="modifika_pagina" type=submit  class="btn btn-primary btn-block" id="modifika_pagina" value="<?= _("MODIFICAR CONTENIDO") ?>">
                                <?php } else { ?>
                                  <input name="add_pagina" type=submit  class="btn btn-primary btn-block" id="add_pagina" value="<?= _("INCLUIR CONTENIDO") ?>">
                                <?php } ?>

                            </div>
                        </div>



                                </form>
                        <!--Final-->
                </div>

              <?php }
              }
              ?>

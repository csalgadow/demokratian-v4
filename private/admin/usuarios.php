<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###                                                         ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo con el formulario que permite acceder al espacio de creación y modificación de administradores
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 2;
include('../private/inc_web/nivel_acceso.php');
?>


                    <!--Comiezo-->
                    <div class="card-header"> <h1 class="card-title"><?= _("CREAR o MODIFICAR ADMINISTRADORES") ?></h1> </div>


                    <div class="card-body">



<br/><br/><br/>

                    <form action="admin.php?c=<?php echo encrypt_url('admin/usuarios_gestion/s=SD',$clave_encriptacion); ?>" method="post"  class="well form-horizontal">


                        <div class="alert alert-warning"> <p>  <?= _("Por razones de seguridad vuelva a escribir su nombre y contraseña") ?></p>
                            <p><?= _("Para poder acceder tiene que ser Ud. un administrador con nivel de acceso Maximo") ?></p></div>
<br/><br/><br/>
                        <?php
                        include ("../private/inc_web/ms_error.php");
                        if (isset($_GET['error_login'])) {
                            $error = $_GET['error_login'];
                            ?>
                            <div class="alert alert-danger">
                                <a class="close" data-dismiss="alert">x</a>

                                <?= _("Error") ?>: <?php echo $error_login_ms[$error]; ?> <br/>

                            </div>

                        <?php }
                        ?>

                        <div class="form-group row">
                            <label for="user_admin" class="col-sm-3 control-label"><?= _("Usuario") ?> : </label>

                            <div class="col-sm-4">
                                <input type="text" name="user_admin" id="user_admin" size="15"  class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="pass_admin" class="col-sm-3 control-label"><?= _("Password") ?> : </label>

                            <div class="col-sm-4">

                                <input type="password" name="pass_admin" id="pass_admin"  class="form-control" >
                            </div>
                        </div>
<br/><br/><br/>
                        <div class="row">
                          <input name=submit type=submit value="  <?= _("Entrar") ?>  "  class="btn btn-primary btn-block" >
                        </div>



                    </form>
</div>
                    <!--Final-->
  <?php } ?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que crea y modifica las cabeceras subiendo los archivos de imagen necesarios
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 9;
include('../private/inc_web/nivel_acceso.php');
if($_SESSION['admin_blog']==1){


$fecha = date("Y-m-d h:i:s");
$tam = 450; //tamaño en Kb
$size = $tam * 1024; // tamaño maximo de los archivos en bits
$situacion = "";

if (ISSET($_POST["add_pagina"])) {


    $titulo = fn_filtro($con, $_POST['titulo']);
    $texto = fn_filtro($con, $_POST['texto']);
    $id_categoria = fn_filtro($con, $_POST['id_categoria']);
    $activo = fn_filtro($con, $_POST['activo']);

    $grabar_bbdd = 0;


    if ($_FILES['fileToUpload']['name']) {  ////miramos si hay archivo a subir
        $imagen_cab = basename($_FILES["fileToUpload"]["name"]);
        $tamano = $_FILES ['fileToUpload']['size'];
        $target_file = $upload_cat . "/" . $imagen_cab;
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

        // Check si existe el archivo
        if (file_exists($target_file)) {
            $msg = $msg . "<div class=\"alert alert-warning\">" . _("Un archivo con ese nombre ya existe") . "</div>";
            $uploadOk = 0;
        }

        //miramos el tipo de archivo si es de imagen
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
            $msg = $msg . " <div class=\"alert alert-warning\">" . _("Tu archivo tiene que ser JPG o GIF. Otros archivos no son permitidos") . "</div>";
            $uploadOk = 0;
            $imagen_cab = "";
        }

        if ($uploadOk == 1) {
            $ha_subido = move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file);
            $codigo_error = $_FILES['fileToUpload']['error'];
            if ($tamano > $size) {
                unlink($target_file);
                $msg = "<div class=\"alert alert-warning\">" . _("Error, La imagen es demasiado grande, superior a") . ": " . $tam . "KB. <strong>" . _("La imagen no se ha subido").".</strong> "._("Tiene que ir al listado para modificar esta entrada y subir una imagen") . "</div>";
            } else {
                $msg = "<div class=\"alert alert-success\"> " . _("La imagen") . " " . $imagen_cab . " " . _("ha sido subida satisfactoriamente") . " </div>";
                $grabar_bbdd = 1; //´
            }

            if ($ha_subido == false) {
                $msg .= "<div class=\"alert alert-warning\">" . _("ERRROR!! Hay un error inesperado al subir su archivo, codigo") . ": " . $codigo_error . "  </div>";
                $grabar_bbdd = 0;
            }
        } else {
            $grabar_bbdd = 0;
        }
    } else {
        $grabar_bbdd = 0;
    }
$anadido = $_SESSION['ID'];

    if ($grabar_bbdd == 1) {

        $insql = "insert into $tbn30 (titulo, texto, id_categoria,activo,imagen,id_incluido) values ( \"$titulo\", \"$texto\",  \"$id_categoria\", \"$activo\", \"$imagen_cab\" , \"$anadido\")";
        $mens = _("Error al añadir una pagina de candidatos externa");
      //  $result = db_query($con, $insql, $mens);
        $result = db_query_id($con, $insql, $mens); // para coger la ultima id
        if (!$result) {
            $inmsg = "<div class=\"alert alert-warning\">" . _("Hay un error al incluir los datos en la BBDD") . "</div>";
        } else {
          $id=$result;
            $inmsg = "<div class=\"alert alert-success\"> " . _("Añadida cabecera a la base de datos") . " </div>";
        }
    } else {  // si no se ha subido la imagen, añadimos los datos pero sin nombre de imagen
        $insql = "insert into $tbn30 (titulo, texto, id_categoria,activo,id_incluido) values ( \"$titulo\", \"$texto\",  \"$id_categoria\", \"$activo\", \"$anadido\")";
        $mens = _("Error al añadir una pagina de candidatos externa");
        $result = db_query_id($con, $insql, $mens); // para coger la ultima id
        if (!$result) {

            $inmsg = "<div class=\"alert alert-warning\">" . _("Hay un error al incluir los datos en la BBDD") . "</div>";
        } else {
          $id=$result;
            $inmsg = "<div class=\"alert alert-success\"> " . _("Añadida cabecera a la base de datos") . " </div>";
        }
    }
}



if (ISSET($_POST["modifika_pagina"])) {
    $titulo = fn_filtro($con, $_POST['titulo']);
    $texto = fn_filtro($con, $_POST['texto']);
    $id_categoria = fn_filtro($con, $_POST['id_categoria']);
    $activo = fn_filtro($con, $_POST['activo']);

    $id = fn_filtro_numerico($con, $variables['id']);
    $sube_img = 0;


    if ($_FILES['fileToUpload']['name']) {  ////miramos si hay archivo a subir
        $imagen_cab = basename($_FILES["fileToUpload"]["name"]);
        $tamano = $_FILES ['fileToUpload']['size'];
        $target_file = $upload_cat . "/" . $imagen_cab;
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

        // Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
            $msg .= "<div class=\"alert alert-warning\">" . _("Perdone, solo estan permitidos archivos con extension JPG, JPEG, PNG & GIF") . " .";
            $msg .= "<br/>" . _("su archivo no ha sido incluido") . ".</div>";
            $sube_img = 0;
            $uploadOk = 0;
        }
            //miramos si existe un fichero con el mismo nombre
        if (file_exists($target_file)) {
            $msg .= "<div class=\"alert alert-warning\">" . _("Perdone, ya existe un archivo con ese nombre") . " </br> " . _("su archivo no ha sido incluido") . "</div>.";

              /*  if ($borradoOk == 1) {//si hemos borrado una imagen, pero no hemos podido subir la imagen porque existe otra, actualizamos la bbdd y dejamos el campo vacio)
                    $imagen_cab = "";
                    $sube_img = 1; //
                    $uploadOk2 = 0;
                }*/
                $sube_img = 0;
                $uploadOk = 0;
            }

            if ($uploadOk == 1) { // si esta todo correcto, subimos el fichero
                $ha_subido = move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file);
                $codigo_error = $_FILES['fileToUpload']['error'];
                if ($tamano > $size) {
                    unlink($target_file);
                    $msg = "<div class=\"alert alert-warning\">" . _("Error, La imagen es demasiado grande, superior a") . ": " . $tam . "KB. <strong>" . _("La imagen no se ha subido").".</strong> </div>";
                    $imagen_cab = "";
                    $sube_img = 0; //
                } else {
                    $msg = "<div class=\"alert alert-success\"> " . _("La imagen") . " " . $imagen_cab . " " . _("ha sido subida satisfactoriamente") . " </div>";
                    $sube_img = 1; //´
                }

                if ($ha_subido == false) {
                    $msg .= "<div class=\"alert alert-warning\">" . _("ERRROR!! Hay un error inesperado al subir su archivo, codigo") . ": " . $codigo_error . "  </div>";
                    $sube_img = 0;
                }else{
                  // si se ha subido correctamente la imagen, comprobamos si habia imagen antugua y la borramos
                  if (ISSET($_POST["row_imagen"])) {
                    $target_file_old = $upload_cat . "/" . $_POST["row_imagen"];
                    unlink($target_file_old);
                  }
                }
            }

    } else {
        //$grabar_bbdd=1;
    }

    if ($sube_img == 1) { //metemos los datos  con el nombre de la imagen
        $sSQL = "UPDATE $tbn30 SET titulo=\"$titulo\",texto=\"$texto\",  imagen=\"$imagen_cab\" ,id_categoria=\"$id_categoria\",activo=\"$activo\" WHERE id=$id";
        $mens = _("Error al añadir la cabecera");
        $result = db_query($con, $sSQL, $mens);

        if (!$result) {
            $inmsg = _("Hay un error") . " " . $idvot;
        } else {
            $inmsg = "<div class=\"alert alert-success\"> " . _("Realizadas las Modificaciones") . " </div>";
        }
    } else {  //metemos los datos sin la imagen
        $sSQL = "UPDATE $tbn30 SET titulo=\"$titulo\",texto=\"$texto\",  id_categoria=\"$id_categoria\"  ,activo=\"$activo\"  WHERE id=$id";
        $mens = _("Error al añadir la cabecera");
        $result = db_query($con, $sSQL, $mens);

        if (!$result) {
            $inmsg = "<div class=\"alert alert-warning\"> " . _("Hay un error al actualizar la base de datos") . " </div>";
        } else {
            $inmsg = "<div class=\"alert alert-success\"> " . _("Realizadas las Modificaciones") . " </div>";
        }
    }
}



  if (isset($variables['id'])){
      $acc = "modifika";

      $id = fn_filtro_numerico($con, $variables['id']);
      $result = mysqli_query($con, "SELECT id, titulo,	texto,	imagen,	id_categoria,	zona_pagina,	activo,	orden	 FROM $tbn30 where id=$id");
      $row = mysqli_fetch_row($result);

      $row_titulo = $row[1];
      $row_texto = $row[2];
      $row_imagen = $row[3];
      $row_id_categoria = $row[4];
      $row_zonapagina = $row[5];
      $row_activo = $row[6];
      $row_orden = $row[7];
      $titulo_h1= _("MODIFICAR CABECERA");

  }else{
      $acc = "";
      $row_titulo = "";
      $row_texto = "";
      $row_imagen = "";
      $row_id_categoria = "";
      $row_zonapagina = "";
      $row_activo = "";
      $row_orden = "";

      $titulo_h1= _("NUEVA CABECERA");
  }

?>


<div class="card-header-votaciones "> <h1 class="card-title"><?php echo $titulo_h1; ?></h1> </div>

<div class="card-body">


                    <!--Comiezo-->

                <div class="row">
                  <div class="col-sm-8"></div>
                  <div class="col-sm-4">
                    <a href="admin.php?c=<?php echo encrypt_url('admin/blog_cabecera_list/s=SD',$clave_encriptacion) ?>" class="btn btn-primary btn-block"><?= _("Volver al directorio de cabeceras") ?></a>
                    <?php if($acc == "modifika"){?>
                      <a href="admin.php?c=<?php echo encrypt_url('admin/blog_cabecera/s=SD',$clave_encriptacion) ?>" class="btn btn-primary btn-block"><?= _("Añadir una cabecera") ?></a>
                    <?php } ?>
                  </div>
                </div>


                    <p>&nbsp;</p>


                    <?php  // Mensajes del sistema si se ha subido o modidificado bien
                    if (isset($inmsg)) {
                        echo "$inmsg";
                    }
                    ?>
                    <?php
                    if (isset($msg)) {
                        echo "$msg";
                    }
                    ?>
                    <form action="<?php $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data" method=post class="well form-horizontal" id="form1">

                      <div class="form-group row">
                          <label for="titulo" class="col-sm-3 control-label"><?= _("Titulo") ?> </label>

                          <div class="col-sm-9">
                              <input name="titulo" type="text"  id="titulo" value="<?php echo "$row_titulo"; ?>"  class="form-control"  autofocus />
                          </div>
                      </div>


                      <div class="form-group row">

                              <label for="texto"  class="col-sm-3 control-label" ><?= _("Texto") ?></label>

                              <div class="col-sm-9">
                              <textarea cols="80" id="texto" name="texto" rows="10"><?php echo "$row_texto"; ?></textarea>
                              <!-- ckeditor -->
                              <script type="text/javascript" src="assets/ckeditor/ckeditor.js"></script>

                              <script>
                               CKEDITOR.replace( 'texto', {
                                height: 250,

                                extraPlugins: 'imgbrowse',
                                filebrowserImageBrowseUrl: '/assets/ckeditor/plugins/imgbrowse/imgbrowse.html?imgroot=<?php echo $baseUrl; ?>',
                                <?php if($_SESSION['usuario_nivel']<=6){     // nivel para poder añadir imagenes  ?>
                                filebrowserImageUploadUrl: 'aux_vota.php?c=<?php echo encrypt_url('basicos_php/imageUpload/conec=OK',$clave_encriptacion) ?>',
                                <?php } ?>
                              //  filebrowserUploadUrl: "upload.php",

                               });
                              </script>
                          </div>
                      </div>


                        <?php
                        if ($acc == "modifika" ) {
                            if ($row_activo == 1) {
                                $chekeado1 = "checked=\"checked\" ";
                                $chekeado2 = "";
                            } else {
                                $chekeado1 = "";
                                $chekeado2 = "checked=\"checked\" ";
                            }
                        }else{
                          $chekeado2 = 'checked="checked"';
                          $chekeado1 = "";
                        }
                        ?>
                        <div class="form-group row">
                            <div class="col-sm-3 "><?= _("Estado") ?></div>
                            <div class="col-sm-9">
                                <label>
                                    <input name="activo" type="radio" id="activo_1" value="1"  <?php echo "$chekeado1"; ?>/>
                                    <?= _("Activo") ?></label> <br/>
                                <label>
                                    <input name="activo" type="radio" id="activo_2" value="0"  <?php echo "$chekeado2"; ?>/>
                                    <?= _("Inactivo") ?></label>

                            </div>
                        </div>


                        <?php

                        if ($acc == "modifika" ) {
                            if ($row_id_categoria == 0) {
                                $chekeado3 = "checked=\"checked\" ";
                                $chekeado4 = "";
                            } else {
                                $chekeado3 = "";
                                $chekeado4 = "checked=\"checked\" ";
                            }
                        }else{
                          $chekeado3 = "checked=\"checked\"";
                          $chekeado4 = "";
                        }
                        ?>
                        <div class="form-group row">
                            <div class="col-sm-3"><?= _("Tipo de cabecera") ?></div>
                            <div class="col-sm-9">
                                <label>
                                    <input name="id_categoria" type="radio" id="id_categoria_1" value="0"  <?php echo "$chekeado3"; ?>/>
                                    <?= _("Cabecera fija") ?></label>
                                    <br/>
                                <label>
                                    <input name="id_categoria" type="radio" id="id_categoria_2" value="1"  <?php echo "$chekeado4"; ?>/>
                                    <?= _("Cabecera tipo Slider") ?></label>

                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="cabecera perso" class="col-sm-3 control-label"><?= _("Subir una imagen") ?></label><?= _("Recomendado un ancho de 1600px") ?></P>

                            <div class="col-sm-9"><label>
                                    <input type="hidden" name="MAX_FILE_SIZE" value="300000000">
                                    <input type="file" name="fileToUpload" id="fileToUpload">

                                    <span id="image"></span><br>
                                </label> <br />

                            </div>
                        </div>
                        <?php
                        if ($acc == "modifika" ) {
                            if ($row_imagen != "") {
                                ?>

                                <div class="form-group">
                                    <label for="cabecera" class="col-sm-3 control-label"><?= _("Cabecera de la pagina") ?></label>

                                    <div class="col-sm-9">
                                        <img src="<?php echo $upload_cat; ?>/<?php echo $row_imagen; ?>" class="img-responsive" alt="<?php echo $nombre_votacion; ?>">
                                    </div>
                                </div>
                                <input type="hidden" name="row_imagen" id="row_imagen" value="<?php echo $row_imagen; ?>">

                                <?php
                            }
                        }
                        ?>



                        <div class="form-group">
                            <div class="col-sm-12">

                                <p>&nbsp;</p>

                                <input name="fecha" type="hidden" id="fecha" value="<?php echo"$fecha"; ?>" />
                                <?php if ($acc == "modifika" ) { ?>
                                  <input name="modifika_pagina" type=submit  class="btn btn-primary btn-block" id="modifika_pagina" value="<?= _("MODIFICAR CABECERA") ?>">
                                <?php } else { ?>
                                  <input name="add_pagina" type=submit  class="btn btn-primary btn-block" id="add_pagina" value="<?= _("INCLUIR CABECERA") ?>">
                                <?php } ?>

                            </div>
                        </div>



                                </form>
                        <!--Final-->
                </div>

              <?php }
              }
              ?>

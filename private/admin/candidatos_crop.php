<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que para subir y cropear las imágenes de los candidatos u opciones, hace la petición por ajax a candidatos_crop_image.php
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 9;
include('../private/inc_web/nivel_acceso.php');

$idcat = fn_filtro_numerico($con, $variables['idcat']);
$idvot = fn_filtro_numerico($con, $variables['idvot']);
echo $idcat;
?>
<?php
$result_img = mysqli_query($con, "SELECT imagen_pequena FROM $tbn7 WHERE id =". $idcat ." ");
$row = mysqli_fetch_row($result_img);


    if ($row[0] != "") {

        $thumb_photo_exists = $upload_cat . "/" . $row[0];
    }else{
      $thumb_photo_exists="temas/".$tema_web."/images/user.png";
    }


 ?>

<link  href="assets/cropperjs/cropper.min.css" rel="stylesheet">
<script type="module" src="assets/cropperjs/cropper.min.js" ></script>

<div class="card-header"> <h1 class="card-title"><?= _("MODIFICAR IMAGEN DE LA OPCION O CANDIDATO") ?></h1> </div>


<div class="card-body">


<div class="row">

  <div class="col-md-7">
    <a href="admin.php?c=<?php echo encrypt_url('admin/candidatos_actualizar/id='.$idcat.'&idvot='.$idvot,$clave_encriptacion) ?>" class="btn btn-primary"><?= _("Modificar esta opcion o candidato") ?></a>
  </div>
      <div class="col-md-5">
        <a href="admin.php?c=<?php echo encrypt_url('admin/candidatos/idvot='.$idvot,$clave_encriptacion) ?>" class="btn btn-primary btn-block"><?= _("Añadir otra opcion o candidato en esta votación") ?></a>
        <a href="admin.php?c=<?php echo encrypt_url('admin/candidatos_busq1/idvot='.$idvot,$clave_encriptacion) ?>" class="btn btn-primary btn-block"><?= _("Buscar en el directorio para modificar o borrar candiatos en esta encuesta") ?></a>
  </div>
</div>
<div class="separador"></div>
<div class="separador"></div>

     <div class="row">
       <div class="col-md-5">&nbsp;</div>
       <div class="col-md-3">
         <div class="image_area">
           <form method="post">
             <div class="caja_crop">
             <label for="upload_image">
               <img src="<?php echo $thumb_photo_exists; ?>" width="270" height="270"  id="uploaded_image" class="img-circle  mx-auto d-block" />
               <div class="overlay">
                 <div class="text_crop">Click para cambiar la imagen</div>
               </div>
               <input type="file" name="image" class="image" id="upload_image" style="display:none" />
             </label>
           </div>
           </form>
         </div>
         </div>
          <div id="success2"></div>
          <div class="col-md-4">&nbsp;</div>
       </div>


       <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
         <div class="modal-dialog modal-xxl" role="document">
           <div class="modal-content">


               <div class="modal-header">
                 <h5 class="modal-title">Recortar la imagen antes de añadirla</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">×</span>
                 </button>
               </div>


               <div class="modal-body">
                 <div class="img-container">
                     <div class="row">
                         <div class="col-md-8">
                             <img class="image_toCrop" src="" id="sample_image" />
                         </div>
                         <div class="col-md-4">
                             <div class="preview"></div>
                         </div>
                     </div>
                 </div>
               </div>


               <div class="modal-footer">
                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                 <button type="button" id="crop" class="btn btn-primary">Recortar la imagen y añadir</button>
               </div>


           </div>
         </div>
     </div>

</div>

<script>

$(document).ready(function(){

 var $modal = $('#modal');

 var image = document.getElementById('sample_image');

 var cropper;

 $('#upload_image').change(function(event){
   var files = event.target.files;

   var done = function(url){
     image.src = url;
     $modal.modal('show');
   };

   if(files && files.length > 0)
   {
     reader = new FileReader();
     reader.onload = function(event)
     {
       done(reader.result);
     };
     reader.readAsDataURL(files[0]);
   }
 });

 $modal.on('shown.bs.modal', function() {
   cropper = new Cropper(image, {
     aspectRatio: 1,
     viewMode: 3,
     preview:'.preview'
   });
 }).on('hidden.bs.modal', function(){
   cropper.destroy();
     cropper = null;
 });

 $('#crop').click(function(){
   canvas = cropper.getCroppedCanvas({
     width:400,
     height:400
   });

   canvas.toBlob(function(blob){
     url = URL.createObjectURL(blob);
     var reader = new FileReader();
     reader.readAsDataURL(blob);
     reader.onloadend = function(){
       var base64data = reader.result;
       $.ajax({
         url: "aux_modal.php?c=<?php echo encrypt_url('admin/candidatos_crop_image/idcat='.$idcat,$clave_encriptacion) ?>",
         method:'POST',
         data:{image:base64data},
         success:function(data)
         {
           $modal.modal('hide');

            $('#uploaded_image').attr('src', data);
             $('#success2').html('<div class="alert alert-success"><?= _("Se ha subido la imagen")?> </div>');
             $('#success2').show();

         }
       });
     };
   });
 });

});


</script>



<?php } ?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###                                                         ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que permite el borrado de una pregunta
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 2;
include('../private/inc_web/nivel_acceso.php');


$id = fn_filtro_numerico($con, $variables['id']);
$idvot = fn_filtro_numerico($con, $variables['idvot']);
?>

                    <!--Comiezo-->

                    <?php
                    $result = mysqli_query($con, "SELECT * FROM $tbn13 where id=$id");
                    $row = mysqli_fetch_row($result);
                    ?>
                    <div class="card-header"> <h1 class="card-title"><?= _("BORRADO pregunta del DEBATE") ?> </h1> </div>


<div class="card-body">
  <div class="row">
      <div class="col-sm-8"></div>
        <div class="col-sm-4">
                    <a href="admin.php?c=<?php echo encrypt_url('admin/preguntas_busq1/idvot='.$idvot,$clave_encriptacion) ?>" class="btn btn-primary btn-block"><?= _("Ir al directorio de preguntas de este debate") ?></a>
</div>
</div>



                    <?php
                    $borrado = mysqli_query($con, "DELETE FROM $tbn13 WHERE id=" . $id . " ") or die("No puedo ejecutar la instrucción de borrado SQL query");
                    $inmsg = "<div class=\"alert alert-danger \">" . _("El REGISTRO HA SIDO BORRADO") . " </div> ";


                    echo "$inmsg";
                    ?>



                        <div class="form-group row"> <?= _("La pregunta") ?>

                            <div class="col-sm-9">
                                <?php echo "$row[1]"; ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div>  <?= _("Las respuestas posibles") ?></div>

                            <div class="col-sm-9">

                                <?php
                                if ($row[2] == 2) {
                                    echo "2 " . _("(SI- NO )");
                                } else if ($row[2] == 3) {
                                    echo" 3 " . _("(SI-NO-NO SE)");
                                } else if ($row[2] == 4) {
                                    echo "4 " . _("(SI-NO-NO SE, BLOQUEO)");
                                } else if ($row[2] == 5) {
                                    echo "5 " . _("(Me gusta mucho, me gusta, indiferente, no me gusta, no me gusta nada)");
                                }
                                ?>

                            </div>
                        </div>

                    </form>
</div>
                    <!--Final-->

<?php } ?>

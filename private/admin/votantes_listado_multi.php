<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###                                                         ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que cargar los distintos tipos de datos de censos de los archivos votantes_listado_xxx . 
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 9;
include('../private/inc_web/nivel_acceso.php');

  $idvot=$variables['idvot'];
  $cen=$variables['cen'];
  $lit=$variables['lit'];

if (isset($variables['id_nprov'])) {
    $id_nprov = fn_filtro($con, $variables['id_nprov']);
} else {
    $id_nprov = "";
}

//////////////////////
if ($es_municipal == true) {
    $id_nprov=001;
  }
?>
<div class="card-header"> <h1 class="card-title"><?= _('CENSOS')?></h1> </div>


<div class="card-body">


  <div class="row separador">

    <div class="col-sm-3">
        <a href="admin.php?c=<?php echo encrypt_url('admin/votantes_listado_multi/idvot='.$idvot."&id_nprov=".$id_nprov."&cen=com&lit=".$lit,$clave_encriptacion) ?>" class =" btn btn-secondary btn-block <?php if($cen=="com"){ echo "active"; }?> " ><?= _("Censo completo") ?></a>
    </div>
    <div class="col-sm-3">
        <a href="admin.php?c=<?php echo encrypt_url('admin/votantes_listado_multi/idvot='.$idvot."&id_nprov=".$id_nprov."&cen=fal&lit=".$lit,$clave_encriptacion) ?>" class =" btn btn-secondary btn-block   <?php if($cen=="fal"){ echo "active"; }?> "  ><?= _("Faltan por votar") ?></a>
      </div>
      <div class="col-sm-3">
        <a href="admin.php?c=<?php echo encrypt_url('admin/votantes_listado_multi/idvot='.$idvot."&id_nprov=".$id_nprov."&cen=stn&lit=".$lit,$clave_encriptacion) ?>" class =" btn btn-secondary btn-block  <?php if($cen=="stn"){ echo "active"; }?> "  ><?= _("Ya ha votado") ?></a>
      </div>
      <div class="col-sm-3">
        <a href="admin.php?c=<?php echo encrypt_url('admin/votantes_listado_multi/idvot='.$idvot."&id_nprov=".$id_nprov."&cen=pres&lit=".$lit,$clave_encriptacion) ?>" class ="btn btn-secondary btn-block   <?php if($cen=="pres"){ echo "active"; }?> "  ><?= _("Han votado presencialmente") ?></a>
    </div>
</div>
  <div class="row  separador">
<div class="col-lg-10">

                    <?php
                    $ids_provincia = $_SESSION['localidad'];
                    $idvot = fn_filtro_numerico($con, $idvot);


                    $var_carga = true;
                    if ($cen == "com") {
                        include("votantes_listado_int.php");
                    } else if ($cen == "fal") {
                        include("votantes_listado_int_no.php");
                    } else if ($cen == "stn") {
                        include("votantes_listado_int_si.php");
                    } else if ($cen == "pres") {
                        include("votantes_listado_int_pres.php");
                    } else if ($cen== "cong") {
                        include("votantes_listado_cong.php");
                    }
                    ?>


</div>


                    <!--Comiezo-->

                    <?php
                    if($es_municipal == false){
                      ?>
                    <div class="col-lg-2">
                      <?php
                      $var_carga = true;
                        if ($lit == "si") {
                          include("votantes_listado_provincias.php");
                          }
                        ?>
                      </div>
                    <?php } ?>
            </div>
</div>
                <!--Final-->

        <script type="text/javascript" src="assets/DataTables/datatables.min.js" ></script>
        <script src="assets/DataTables/Buttons-1.6.2/js/dataTables.buttons.min.js" ></script>
        <script src="assets/DataTables/JSZip-2.5.0/jszip.min.js" ></script>
        <script src="assets/DataTables/pdfmake-0.1.36/pdfmake.min.js" ></script>
        <script src="assets/DataTables/pdfmake-0.1.36/vfs_fonts.js" ></script>
        <script src="assets/DataTables/Buttons-1.6.2/js/buttons.html5.min.js" ></script>

        <script type="text/javascript" language="javascript" class="init">
          $(document).ready(function() {
            $('#tabla1').DataTable({
              responsive: true,
              dom: 'Blfrtip',
              buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
              ],
              lengthMenu: [
                [10, 25, 50, -1],
                [10, 25, 50, "Todos"]
              ],
              language: {
                buttons: {
                  copy: '<?= _("Copiar") ?>',
                  copyTitle: '<?= _("Copiado en el portapapeles") ?>',
                  copyKeys: '<?= _("Presione") ?> <i> ctrl </i> o <i> \ u2318 </i> + <i> C </i> <?= _("para copiar los datos de la tabla a su portapapeles") ?>. <br> <br> <?= _("Para cancelar, haga clic en este mensaje o presione Esc") ?>.',
                  copySuccess: {
                    _: '%d <?= _("lineas copiadas") ?>',
                    1: '1 <?= _("linea copiada") ?>'
                  }
                },
                processing: "<?= _("Tratamiento en curso") ?>...",
                search: "<?= _("Buscar") ?>:",
                lengthMenu: "<?= _("Ver") ?> _MENU_  <?= _("resultados por pagina") ?>",
                info: "<?= _("Mostrando") ?> _PAGE_ <?= _("de") ?> _PAGES_ <?= _("paginas") ?>",
                infoEmpty: "<?= _("No se han encitrado resultados") ?>",
                infoFiltered: "(<?= _("filtered from") ?> _MAX_ <?= _("total records") ?>)",
                infoPostFix: "",
                loadingRecords: "<?= _("Cargando") ?>...",
                zeroRecords: "<?= _("No se han encontrado resultados - perdone") ?>",
                emptyTable: "<?= _("No se han encontrado resultados - perdone") ?>",
                paginate: {
                  first: "<?= _("Primero") ?>",
                  previous: "<?= _("Anterior") ?>",
                  next: "<?= _("Siguiente") ?>",
                  last: "<?= _("Ultimo") ?>"
                },
                aria: {
                  sortAscending: ": <?= _("activar para ordenar columna de forma ascendente") ?>",
                  sortDescending: ": <?= _("activar para ordenar columna de forma descendente") ?>"
                }
              }
            });
          });
        </script>
        <script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js" ></script>
        <script src="https://cdn.datatables.net/responsive/2.2.7/js/responsive.bootstrap4.min.js" ></script>
        <!--end datatables -->
<?php } ?>

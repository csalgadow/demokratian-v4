<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###                                                         ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que hace el envío de un correo de comprobación desde la página de constantes de correo para verificar que funciona, si no funciona saca todos los errores para poder comprobar el problema
*/
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 1;
include('../private/inc_web/nivel_acceso.php');
include('../private/config/config-correo.inc.php');

//require_once('../private/modulos/PHPMailer/PHPMailerAutoload.php');


require_once '../private/modulos/PHPMailer/src/Exception.php';
require_once '../private/modulos/PHPMailer/src/PHPMailer.php';
require_once '../private/modulos/PHPMailer/src/SMTP.php';

?>

        <div class="modal-header">
             <h5 class="modal-title"><?= _("Prueba de configuracón de correo") ?></h5>
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
         </div>
         <div class="modal-body">
                 <!---texto --->
                <p>
                    <?php
                    $name = _("prueba correo");
                    $mensaje = _("Es una prueba de configuracion de correo");


                    $mail = new PHPMailer();

//Enable SMTP debugging.
                    $mail->SMTPDebug = 4;
//Set PHPMailer to use SMTP.
                    if ($mail_IsHTML == true) {
                        $mail->IsHTML(true);
                    } else {
                        $mail->IsHTML(false);
                    }

                    if ($mail_sendmail == true) {
                        $mail->IsSendMail();
                    } else {
                        $mail->IsSMTP();
                    }
//$mail->SMTPAuth = true;
                    if ($mail_SMTPAuth == true) {
                        $mail->SMTPAuth = true;
                    } else {
                        $mail->SMTPAuth = false;
                    }

                    if ($mail_SMTPSecure == false) {
                        $mail->SMTPSecure = false;
                        $mail->SMTPAutoTLS = false;
                    } else if ($mail_SMTPSecure == "SSL") {
                        $mail->SMTPSecure = 'ssl';
                    } else {
                        $mail->SMTPSecure = 'tls';
                        //$mail->SMTPAutoTLS = false;
                    }


                    if ($mail_SMTPOptions == true) {  //algunos servidores con certificados incorrectos no envian los correos por SMTP por lo que quitamos la validadcion de los certificados, NO SE RECOMIENDA EN ABSOLUTO usar esta opción
                        $mail->SMTPOptions = array(
                            'ssl' => array(
                                'verify_peer' => false,
                                'verify_peer_name' => false,
                                'allow_self_signed' => true
                            )
                        );
                    }

                    $mail->Port = $puerto_mail; // Puerto a utilizar

                    $mail->Host = $host_smtp;
                    $mail->SetFrom($email_env, $nombre_sistema);
                    $mail->MsgHTML($mensaje);
                    $mail->AddAddress($email_env, $name);
                    $mail->Username = $user_mail;
                    $mail->Password = $pass_mail;

                    $mail->Subject = _("prueba de configuracion");
                    $mail->Body = _("<i>Texto de correo en HTML</i>");
                    $mail->AltBody = "This is the plain text version of the email content";

                    if (!$mail->send()) {
                        echo "<div class=\"alert alert-warning\"> Mailer Error: " . $mail->ErrorInfo . "</div>";
                    } else {
                        echo "<div class=\"alert alert-success\">" . _("El correo ha sido enviado, compruebe su buzon de correo y si lo ha recibido la configuración es correcta") . " " . $email_env . "</div>";
                    }
                    ?>


                </p>



                <!--
            ===========================  fin texto ayuda
                -->             </div>            <!-- /modal-body -->
            <!-- /modal-footer -->
        </div>         <!-- /modal-content -->


<?php } ?>

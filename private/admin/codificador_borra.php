<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que borrar los codificadores
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 1;
include('../private/inc_web/nivel_acceso.php');


$idvot = fn_filtro_numerico($con, $variables['idvot']);

$id = fn_filtro_numerico($con, $variables['id']);
?>



                    <!--Comiezo-->
                    <div class="card-header"> <h1 class="card-title"><?= _("BORRAR CODIFICADOR") ?></h1> </div>


                    <div class="card-body">

                    <?php
                    $result = mysqli_query($con, "SELECT nombre,correo,orden FROM $tbn21 where id=$id");
                    $row = mysqli_fetch_row($result);
                    ?>

<div class="row">
  <div class="col-sm-8"> </div>
  <div class="col-sm-4">
                    <p><a href="admin.php?c=<?php echo encrypt_url('admin/codificador/idvot='.$idvot,$clave_encriptacion) ?>" class="btn btn-secondary btn-block"><?= _("Ver codificadores de esta votación") ?> </a></p>
        </div>
</div>
                        <div class="form-group row">
                            <div><?= _("Nombre") ?></div>

                            <div class="col-sm-9">
                                <?php echo "$row[0]"; ?>
                            </div></div>
                        <div class="form-group row">
                            <div><?= _("Correo") ?></div>

                            <div class="col-sm-9">   <?php echo "$row[1]"; ?>
                            </div></div>
                        <div class="form-group row">
                            <div><?= _("Orden") ?></div>

                            <div class="col-sm-9">   <?php echo "$row[2]"; ?>
                            </div></div>


                        <p>&nbsp;</p>


                    <?php
                    $borrado = mysqli_query($con, "DELETE FROM $tbn21 WHERE id=" . $id . "") or die("No puedo ejecutar la instrucción de borrado SQL query");
                    echo "<div class=\"alert alert-danger\">" . _("El registro ha sido borrado") . "</div>";
                    ?>

</div>

                    <!--Final-->


<?php } ?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que lista todas las votaciones de la busqueda del formulario gestion_zonas.php
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 9;
include('../private/inc_web/nivel_acceso.php');


if (ISSET($_POST["buscar_ccaa"])) {
    ///////////////////////miramos la comunidad autonoma para meterlo tambien en la sesion
    $id_ccaa = fn_filtro($con, $_POST['id_ccaa']);
    $options2 = "select DISTINCT ccaa from $tbn3  where ID = " . $id_ccaa . " ";
    $resulta2 = mysqli_query($con, $options2) or die("error: " . mysqli_error());
    while ($listrows2 = mysqli_fetch_array($resulta2)) {
        $nombre_zona = $listrows2['ccaa'];
    }
} elseif (ISSET($_POST["buscar_prov"])) {
///////////////// miramos el codigo de la provincia para meterlo en la sesion
    $id_provincia = fn_filtro($con, $_POST['id_provincia']);
    $options = "select DISTINCT provincia from $tbn8  where ID = " . $id_provincia . "";
    $resulta = mysqli_query($con, $options) or die("error: " . mysqli_error());
    while ($listrows = mysqli_fetch_array($resulta)) {
        $nombre_zona = $listrows['provincia'];
    }
} elseif (ISSET($_POST["buscar_municipio"])) {

    ///////////////////////miramos el municipio para meterlo tambien en la sesion
    $municipio = fn_filtro($con, $_POST['municipio']);
    $options2 = "select DISTINCT nombre from $tbn18  where id_municipio = " . $municipio . " ";
    $resulta2 = mysqli_query($con, $options2) or die("error: " . mysqli_error());
    while ($listrows2 = mysqli_fetch_array($resulta2)) {
        $nombre_zona = $listrows2['nombre'];
    }
}
?>

                    <!--Comiezo-->

                    <?php
                    if ($_SESSION['nivel_usu'] == 2) {

                        if (ISSET($_POST["buscar_ccaa"])) {
                            $id_ccaa = fn_filtro($con, $_POST['id_ccaa']);
                            $sql = "SELECT  ID , nombre_votacion,tipo, tipo_votante, activa, activos_resultados,seguridad,interventor,fecha_com, fecha_fin  FROM $tbn1 where demarcacion=2 AND id_ccaa=" . $id_ccaa . " ORDER BY 'ID' DESC ";
                            $tipos_buscado = _("AUTONOMICA") . "  -  " . $nombre_zona . " ";
                        } elseif (ISSET($_POST["buscar_prov"])) {
                            $id_provincia = fn_filtro($con, $_POST['id_provincia']);
                            $sql = "SELECT  ID , nombre_votacion,tipo, tipo_votante, activa, activos_resultados,seguridad,interventor,fecha_com, fecha_fin  FROM $tbn1 where demarcacion=3 AND id_provincia=" . $id_provincia . " ORDER BY 'ID' DESC";
                            $tipos_buscado = _("PROVINCIA") . " -  " . $nombre_zona . " ";
                        } elseif (ISSET($_POST["buscar_sub"])) {
                            $id_sub = fn_filtro($con, $_POST['id_sub']);
                            $sql = "SELECT  ID , nombre_votacion,tipo, tipo_votante, activa, activos_resultados,seguridad ,interventor,fecha_com, fecha_fin FROM $tbn1 where id_grupo_trabajo=" . $id_sub . " ORDER BY 'ID' DESC ";
                            $tipos_buscado = _("GRUPO DE TRABAJO");
                        } elseif (ISSET($_POST["buscar_municipio"])) {
                            $municipio = fn_filtro($con, $_POST['municipio']);
                            $sql = "SELECT  ID , nombre_votacion,tipo, tipo_votante, activa, activos_resultados,seguridad ,interventor,fecha_com, fecha_fin FROM $tbn1 where id_municipio=" . $municipio . " ORDER BY 'ID' DESC ";
                            $tipos_buscado = _("Municipio de") . " " . $nombre_zona . "";
                        } else {

                            $sql = "SELECT  ID , nombre_votacion,tipo, tipo_votante, activa, activos_resultados,seguridad,interventor ,fecha_com, fecha_fin  FROM $tbn1 where demarcacion=1 ORDER BY tipo  DESC ";
                            if ($es_municipal == false) {
                                $tipos_buscado = _("ESTATALES");
                            } else {
                                $tipos_buscado = _("Generales");
                            }
                        }
                    } else if ($_SESSION['nivel_usu'] == 3) { //administrador de ccaa -> demarcacion 2
                        if (ISSET($_POST["buscar_prov"])) {
                            $id_provincia = fn_filtro($con, $_POST['id_provincia']);
                            $sql = "SELECT  ID , nombre_votacion,tipo, tipo_votante, activa, activos_resultados,seguridad,interventor ,fecha_com, fecha_fin  FROM $tbn1 where demarcacion=3 AND id_provincia=" . $id_provincia . " ORDER BY 'ID' ";
                            $tipos_buscado = _("PROVINCIA") . "  - " . $nombre_zona . " ";
                        } elseif (ISSET($_POST["buscar_sub"])) {
                            $id_sub = fn_filtro($con, $_POST['id_sub']);
                            $sql = "SELECT  ID , nombre_votacion,tipo, tipo_votante, activa, activos_resultados,seguridad,interventor ,fecha_com, fecha_fin  FROM $tbn1 where id_grupo_trabajo=" . $id_sub . " ORDER BY 'ID' ";
                            $tipos_buscado = _("GRUPO DE TRABAJO");
                        } elseif (ISSET($_POST["buscar_municipio"])) {
                            $municipio = fn_filtro($con, $_POST['municipio']);
                            $sql = "SELECT  ID , nombre_votacion,tipo, tipo_votante, activa, activos_resultados,seguridad,interventor ,fecha_com, fecha_fin ,interventor FROM $tbn1 where id_municipio=" . $municipio . " ORDER BY 'ID' DESC ";
                            $tipos_buscado = _("Municipio") . " " . $nombre_zona . " ";
                        } else {
                            $sql = "SELECT  ID , nombre_votacion,tipo, tipo_votante, activa, activos_resultados,seguridad,interventor ,fecha_com, fecha_fin  FROM $tbn1 where id_ccaa = " . $_SESSION['id_ccaa_usu'] . " and demarcacion = 2 ORDER BY 'ID' ";
                            $tipos_buscado = _("AUTONOMICA") . "  -  " . $nombre_zona . " ";
                        }
                    } else if ($_SESSION['nivel_usu'] == 4) { //administardor provincial -> demarcacion  3
                        if (ISSET($_POST["buscar_sub"])) {
                            $id_sub = fn_filtro($con, $_POST['id_sub']);
                            $sql = "SELECT  ID , nombre_votacion,tipo, tipo_votante, activa, activos_resultados,seguridad,interventor ,fecha_com, fecha_fin  FROM $tbn1 where id_grupo_trabajo=" . $id_sub . " ORDER BY 'ID' ";
                            $tipos_buscado = _("GRUPO DE TRABAJO");
                        } elseif (ISSET($_POST["buscar_municipio"])) {
                            $municipio = fn_filtro($con, $_POST['municipio']);
                            $sql = "SELECT  ID , nombre_votacion,tipo, tipo_votante, activa, activos_resultados,seguridad ,interventor,fecha_com, fecha_fin FROM $tbn1 where id_municipio=" . $municipio . " ORDER BY 'ID' DESC ";
                            $tipos_buscado = _("Municipio") . " " . $nombre_zona . " ";
                        } else {
                            $id_provincia = fn_filtro($con, $_POST['id_provincia']);
                            $sql = "SELECT  ID , nombre_votacion,tipo, tipo_votante, activa, activos_resultados,seguridad,interventor ,fecha_com, fecha_fin  FROM $tbn1 where demarcacion=3 AND id_provincia=" . $id_provincia . " ORDER BY 'ID' ";
                            $tipos_buscado = _("PROVINCIA") . " - " . $nombre_zona . "";
                        }
                    } else {
                        $id_sub = fn_filtro($con, $_POST['id_sub']);
                        $sql = "SELECT  ID , nombre_votacion,tipo, tipo_votante, activa, activos_resultados,seguridad,interventor ,fecha_com, fecha_fin   FROM $tbn1 where id_grupo_trabajo=" . $id_sub . " ORDER BY 'ID'  ";
                        $tipos_buscado = _("GRUPO DE TRABAJO");
                    }
                    ?>
                    <div class="card-header"> <h1 class="card-title"><?= _("Votaciones  existentes") ?> <?php echo "$tipos_buscado"; ?></h1> </div>


<div class="card-body">

                    <?php
//$sql = "SELECT * FROM $tbn1 where id_provincia like '%$ids_provincia%' ORDER BY 'id_provincia' ";

                    $result = mysqli_query($con, $sql);
                    if ($row = mysqli_fetch_array($result)) {
                        ?>

                        <table id="tabla1" class="table table-striped table-bordered dt-responsive nowrap" data-page-length="25">

                            <thead>
                                <tr>
                                    <th width="3%">ID</th>
                                    <th width="10%"><?= _("Fecha") ?></th>
                                    <th width="35%"><?= _("Titulo") ?></th>
                                    <th width="7%"><?= _("Tipo") ?></th>
                                    <th width="10%"><?= _("Tipo votante") ?></th>
                                    <th width="10%" align="center"><?= _("estado") ?></th>

                                    <th width="15%" align="center"><?= _("Resultados") ?></th>
                                    <th width="10%">&nbsp;</th>
                                </tr>
                            </thead>

                            <tbody>

                                <?php
                                mysqli_field_seek($result, 0);

                                do {
                                    ?>
                                    <tr>
                                        <td><?php echo "$row[0]" ?></td>
                                        <td>
                                            <?php
                                            $hoy = strtotime(date('Y-m-d H:i'));
                                            $fecha_ini = strtotime($row[8]);
                                            $fecha_fin = strtotime($row[9]);
                                            if ($fecha_ini <= $hoy && $fecha_fin >= $hoy) {
                                                ?>
                                                <?= _("En fecha") ?>
                                                <?php
                                            } else {
                                                ?>
                                                <?= _("Fuera fecha") ?>
                                            <?php }
                                            ?>  </td>
                                        <td><?php echo "$row[1]" ?></td>
                                        <td>
                                            <?php
                                            if ($row[2] == 1) {
                                                echo _("Primarias");
                                            } else if ($row[2] == 2) {
                                                echo _("VUT");
                                            } else if ($row[2] == 3) {
                                                echo _("Encuesta");
                                            } else if ($row[2] == 4) {
                                                echo _("Debate");
                                            } else if ($row[2] == 5) {
                                                echo _("ENCUESTA con poderación del voto según tipo votante");
                                            } else if ($row[2] == 6) {
                                              echo _("ENCUESTA con poderación del voto según tipo votante")." ". ("BINARIA");
                                            } else if ($row[2] == 7) {
                                              echo _("ENCUESTA")." ". ("BINARIA");
                                            }
                                            ?>

                                        </td>
                                        <td>
                                            <?php
                                            if ($row[3] == 1) {
                                                echo _("solo socio");
                                            } else if ($row[3] == 2) {
                                                echo _("socio y simpatizante verificado");
                                            } else if ($row[3] == 3) {
                                                echo _("socio y simpatizante");
                                            } else if ($row[3] == 5) {
                                                echo _("abierta");
                                            }
                                            ?>

                                        </td>
                                        <td align="center">
                                            <?php if ($row[4] == "no") { ?>
                                                <?= _("INACTIVO") ?> <span class="glyphicon glyphicon-ban-circle  text-danger"></span>
                                            <?php } else {
                                                ?>
                                                <?= _("ACTIVO") ?> <span class="glyphicon glyphicon-ok  text-success"></span>
                                            <?php } ?>
                                        </td>
                                        <td align="center">
                                            <?php
                                            if ($row[2] == 4) {
                                                echo " <h4><span class=\"label label-info\">" . _("Debate") . "<spam></h4>";
                                            } else {
                                                if ($row[5] == "no") {
                                                    ?>
                                                    <?= _("RESULTADOS INACTIVOS") ?>  <span class="glyphicon glyphicon-eye-close  text-danger"></span>
                                                <?php } else {
                                                    ?>
                                                    <?= _("RESULTADOS ACTIVOS") ?> <span class="glyphicon glyphicon-eye-open  text-success"></span>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </td>
                                        <td>
                                          <a href="admin.php?c=<?php echo encrypt_url('admin/gestionar/id='.$row[0],$clave_encriptacion) ?>" class="btn btn-primary btn-xs"><?= _("GESTIONAR") ?></a>
                                        </td>
                                    </tr>

                                    <?php
                                } while ($row = mysqli_fetch_array($result));
                                ?>
                            </tbody>
                        </table>
                        <?php
                    } else {?>
                      <div class="alert alert-info">
                        <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                        <i class="fa fa-times simple-remove" aria-hidden="true"></i>
                        </button>
                        <span>
                          <b> Info - </b> <?=_("¡No se ha encontrado ningún grupo de trabajo!");?></span>
                        </div>
                        <?php
                    }


                    ?>
</div>
                    <script type="text/javascript" src="assets/DataTables/datatables.min.js" ></script>
                    <script src="assets/DataTables/Buttons-1.6.2/js/dataTables.buttons.min.js" ></script>
                    <script src="assets/DataTables/JSZip-2.5.0/jszip.min.js" ></script>
                    <script src="assets/DataTables/pdfmake-0.1.36/pdfmake.min.js" ></script>
                    <script src="assets/DataTables/pdfmake-0.1.36/vfs_fonts.js" ></script>
                    <script src="assets/DataTables/Buttons-1.6.2/js/buttons.html5.min.js" ></script>

                    <script type="text/javascript" language="javascript" class="init">
                      $(document).ready(function() {
                        $('#tabla1').DataTable({
                          responsive: true,
                          dom: 'Blfrtip',
                          buttons: [
                            'copyHtml5',
                            'excelHtml5',
                            'csvHtml5',
                            'pdfHtml5'
                          ],
                          lengthMenu: [
                            [10, 25, 50, -1],
                            [10, 25, 50, "Todos"]
                          ],
                          language: {
                            buttons: {
                              copy: '<?= _("Copiar") ?>',
                              copyTitle: '<?= _("Copiado en el portapapeles") ?>',
                              copyKeys: '<?= _("Presione") ?> <i> ctrl </i> o <i> \ u2318 </i> + <i> C </i> <?= _("para copiar los datos de la tabla a su portapapeles") ?>. <br> <br> <?= _("Para cancelar, haga clic en este mensaje o presione Esc") ?>.',
                              copySuccess: {
                                _: '%d <?= _("lineas copiadas") ?>',
                                1: '1 <?= _("linea copiada") ?>'
                              }
                            },
                            processing: "<?= _("Tratamiento en curso") ?>...",
                            search: "<?= _("Buscar") ?>:",
                            lengthMenu: "<?= _("Ver") ?> _MENU_  <?= _("resultados por pagina") ?>",
                            info: "<?= _("Mostrando") ?> _PAGE_ <?= _("de") ?> _PAGES_ <?= _("paginas") ?>",
                            infoEmpty: "<?= _("No se han encitrado resultados") ?>",
                            infoFiltered: "(<?= _("filtered from") ?> _MAX_ <?= _("total records") ?>)",
                            infoPostFix: "",
                            loadingRecords: "<?= _("Cargando") ?>...",
                            zeroRecords: "<?= _("No se han encontrado resultados - perdone") ?>",
                            emptyTable: "<?= _("No se han encontrado resultados - perdone") ?>",
                            paginate: {
                              first: "<?= _("Primero") ?>",
                              previous: "<?= _("Anterior") ?>",
                              next: "<?= _("Siguiente") ?>",
                              last: "<?= _("Ultimo") ?>"
                            },
                            aria: {
                              sortAscending: ": <?= _("activar para ordenar columna de forma ascendente") ?>",
                              sortDescending: ": <?= _("activar para ordenar columna de forma descendente") ?>"
                            }
                          }
                        });
                      });
                    </script>
                    <script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js" ></script>
                    <script src="https://cdn.datatables.net/responsive/2.2.7/js/responsive.bootstrap4.min.js" ></script>
                    <!--end datatables -->

<?php }  ?>

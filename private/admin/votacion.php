<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###                                                         ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo permite la creación o modificación de una votación
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 9;
include('../private/inc_web/nivel_acceso.php');

  $row_provincia = "";
  $row_activa = "";
  $row_nombre = "";
  $row_texto = "";
  $row_resumen = "";
  $row_tipo = "";
  $row_tipo_votante = "";
  $row_numero_opciones = "";
  $row_fecha_com = "";
  $row_fecha_fin = "";
  $row_ccaa = "";
  $row_grupo = "";
  $row_demarcacion = "";
  $row_seguridad = "";
  $row_interventores = "";
  $row_interventor = "";
  $row_recuento = "";
  $row_id_municipio = "";
  $row_encripta = "";
  $nombre = "";
  $provincia = "";
  $texto = "";
  $tipo = "";
  $tipo_votante = "";
  $resumen = "";
  $numero_opciones = "";
  $anadido = "";
  $fecha_anadido = "";
  $comunidad_autonoma = "";
  $subgrupo = 0; //posiblemente obsoleto
  $demarcacion = "";
  $fecha_ini = "";
  $fecha_fin = "";
  $id_grupo_trabajo = 0;
  $tipo_seg = "";
  $estado = "";
  $n_interventores = "";
  $interventor = "";
  $recuento = "";
  $municipio = 0;
  $encripta = "";
  $si_interventores = "";

  $fecha_ver = date("d-m-Y ");


if (isset($variables['id'])) {
    $id = fn_filtro_numerico($con, $variables['id']);
}
if (isset($variables['acc'])) {
    $acc = fn_filtro_nodb($variables['acc']);
}else{
  $acc ="";
}

if (ISSET($_POST["add_votacion"])) {


    $tipo = fn_filtro_numerico($con, $_POST['tipo']);
    $tipo_encuesta = fn_filtro_numerico($con, $_POST['tipo_encuesta']);
    if ($tipo==99){
      $tipo = $tipo_encuesta;
    }
    $tipo_votante = fn_filtro_numerico($con, $_POST['tipo_usuario']);
    $numero_opciones = fn_filtro($con, $_POST['numero_opciones']);
    if($tipo ==6 or $tipo ==7){
      $numero_opciones = 1;
    }
    if ($es_municipal == false) {
        $comunidad_autonoma = fn_filtro_numerico($con, $_POST['comunidad_autonoma']);
        $provincia = fn_filtro_numerico($con, $_POST['provincia']);
    }

    //posiblemente obsoleto (habia que quitar la entrada en la sentencia sql)
    if (isset($_POST['subgrupo'])) {
        $subgrupo = fn_filtro_numerico($con, $_POST['subgrupo']);
    } else {
        $subgrupo = 0;
    }
    // hasta aqui

    $demarcacion = fn_filtro($con, $_POST['demarcacion']);
    $tipo_seg = fn_filtro_numerico($con, $_POST['tipo_seg']);
    $estado = fn_filtro($con, $_POST['estado']);
    $nombre = fn_filtro($con, $_POST['nombre']);
    $resumen = fn_filtro_editor($con, $_POST['resumen']);
    $texto = fn_filtro_editor($con, $_POST['texto']);
    if (isset($_POST['encripta'])) {
        $encripta = fn_filtro($con, $_POST['encripta']);
    }
    if (isset($_POST['si_interventores'])) {
        $si_interventores = fn_filtro($con, $_POST['si_interventores']);
    }


    $fecha_in = $_POST['fecha_ini'] . " " . $_POST['hora_ini'] . ":" . $_POST['min_ini'];
    $fecha_ini = date("Y-m-d H:i", strtotime($fecha_in));
    $fecha_fi = $_POST['fecha_fin'] . " " . $_POST['hora_fin'] . ":" . $_POST['min_fin'];
    $fecha_fin = date("Y-m-d H:i", strtotime($fecha_fi));

    $anadido = $_SESSION['ID'];
    $fecha_anadido = date("Y-m-d");

    $recuento = fn_filtro($con, $_POST['tipo_recuento']);

    if ($si_interventores == "permitir") {
        if (isset($_POST['numero_interventores'])) {
            $n_interventores = fn_filtro_numerico($con, $_POST['numero_interventores']);
        }
        $interventor = "si";
        if ($n_interventores == 0) {
            $error = "error";
            $inmsg_error = _("Hay algun  error,  no se puede poner 0 en el numero de opciones ya que ha indicado que quiere interventores especiales, verifique el dato") . " <br/>" . _("¡¡¡OJO!!!Hay algunos datos que debera volver a indicarlos");
        }
    } else {
        $n_interventores = 0;
        $interventor = "no";
    }

    if ($tipo == 1 or $tipo == 2 or $tipo == 3) {
        if ($numero_opciones == "") {
            $error = "error";
            $inmsg_error = _("Hay algun  error, el numero de opciones es un dato requerido , verifique el dato") . " <br/>" . _("¡¡¡OJO!!!Hay algunos datos que debera volver a indicarlos");
        }
    }

    if ($tipo == 1 or $tipo == 2) {
        if ($numero_opciones == 0) {
            $error = "error";
            $inmsg_error = _("Hay algun  error, en las votaciones tipo VUT o PRIMARIAS no se puede poner 0 en el numero de opciones , verifique el dato") . " <br/>" . _("¡¡¡OJO!!!Hay algunos datos que debera volver a indicarlos");
        }
    }

    if ($_POST['demarcacion'] == "") {
        $error = "error";
        $inmsg_error = _("no ha indicado la demarcación de la votación");
    } elseif ($_POST['demarcacion'] == 2) { //Autonomica
        $provincia = 100;
    } else if ($_POST['demarcacion'] == 3) { //provincial
        $comunidad_autonoma = 0;
        if ($_POST['provincia'] == "") {
            $error = "error";
            $inmsg_error = _("Hay algun tipo de error, por alguna causa no ha llegado el dato de provincia, verifique que la ha indicado") . " <br/>" . _("¡¡¡OJO!!!Hay algunos datos que debera volver a indicarlos");
        } else {
            $provincia = fn_filtro($con, $_POST['provincia']);
        }
    } else { //grupo provincial
        $provincia = 00;
        $comunidad_autonoma = 0;
    }

    /**/

    if ($_POST['demarcacion'] == 4) {
        if ($_POST['grupo_trabajo_prov'] == "") {
            $error = "error";
            $inmsg_error = _("Hay algun tipo de error, por alguna causa no ha llegado el dato de grupo de trabajo, verifique que la ha indicado") . " <br/>" . _("¡¡¡OJO!!!Hay algunos datos que debera volver a indicarlos");
        } else {
            $id_grupo_trabajo = fn_filtro($con, $_POST['grupo_trabajo_prov']);
        }
    } else if ($_POST['demarcacion'] == 5) {
        if ($_POST['grupo_trabajo_aut'] == "") {
            $error = "error";
            $inmsg_error = _("Hay algun tipo de error, por alguna causa no ha llegado el dato de grupo de trabajo, verifique que la ha indicado") . " <br/>" . _("¡¡¡OJO!!!Hay algunos datos que debera volver a indicarlos");
        } else {
            $id_grupo_trabajo = fn_filtro($con, $_POST['grupo_trabajo_aut']);
        }
    } else if ($_POST['demarcacion'] == 6) {
        if ($_POST['grupo_trabajo_gen'] == "") {
            $error = "error";
            $inmsg_error = _("Hay algun tipo de error, por alguna causa no ha llegado el dato de grupo de trabajo, verifique que la ha indicado") . " <br/>" . _("¡¡¡OJO!!!Hay algunos datos que debera volver a indicarlos");
        } else {
            $id_grupo_trabajo = fn_filtro($con, $_POST['grupo_trabajo_gen']);
        }
    } else if ($_POST['demarcacion'] == 7) {
        $comunidad_autonoma = 0;
        if ($_POST['provincia2'] == "") {
            $error = "error";
            $inmsg_error = _("Hay algun tipo de error, por alguna causa no ha llegado el dato de provincia, verifique que la ha indicado") . " <br/>" . _("¡¡¡OJO!!!Hay algunos datos que debera volver a indicarlos");
        } else {
            $provincia = fn_filtro($con, $_POST['provincia2']);
        }

        if ($_POST['municipio'] == "") {
            $error = "error";
            $inmsg_error = _("Hay algun tipo de error, por alguna causa no ha llegado el dato de municipio, verifique que la ha indicado") . " <br/>" . _("¡¡¡OJO!!!Hay algunos datos que debera volver a indicarlos");
        } else {
            $municipio = fn_filtro($con, $_POST['municipio']);
        }
    }



    if (!isset($error)) {

        $insql = "insert into $tbn1 (nombre_votacion, 	id_provincia, 	texto, tipo, 	tipo_votante, resumen,numero_opciones,anadido,fecha_anadido,id_ccaa,id_subzona,demarcacion,fecha_com,fecha_fin,id_grupo_trabajo,seguridad,activa, interventores,interventor, recuento,id_municipio,encripta ) values (  \"$nombre\",  \"$provincia\", \"$texto\", \"$tipo\", \"$tipo_votante\", \"$resumen\", \"$numero_opciones\", \"$anadido\", \"$fecha_anadido\", \"$comunidad_autonoma\", \"$subgrupo\", \"$demarcacion\", \"$fecha_ini\", \"$fecha_fin\", \"$id_grupo_trabajo\", \"$tipo_seg\", \"$estado\", \"$n_interventores\", \"$interventor\", \"$recuento\", \"$municipio\", \"$encripta\")";
        //$inres = @mysqli_query($con, $insql) or die("<strong><font color=#FF0000 size=3>  Imposible añadir. Cambie los datos e intentelo de nuevo.</font></strong>");
        $mens = "Mensaje error añadido nueva votacion";
        $resultados = db_query_id($con, $insql, $mens);

        if (!$resultados) {
            $inmsg = "<div class=\"alert alert-danger\">" .
                    _("Error al crear la votación") . " \" " . $nombre . " \" " . _("ya que no se ha añadido a la base de datos") . "	</div>";
        } else {

            $id = str_pad($resultados, 6, "0", STR_PAD_LEFT);
            if ($tipo != 4) {

                $idvot_encriptada = md5($id);
                $fp = fopen($FilePath . $idvot_encriptada . "_tally.txt", "w+");
                fputs($fp, "Ballots   |0");
                fclose($fp);
                chmod($FilePath . $idvot_encriptada . "_tally.txt", 0700);



                $fp = fopen($FilePath . $idvot_encriptada . "_ballots.txt", "w+");
                fclose($fp);
                chmod($FilePath . $idvot_encriptada . "_ballots.txt", 0700);
                for ($i = 0; $i < 5; $i++) {  //metemos en la tabla temporal 4 registros vacios para que no de error luego
                    $cadena = "sinID+VotoInicial";
                    $insql2 = "insert into $tbn20 (datos,id_votacion) values (\"$cadena\",\"$idvot_encriptada\")";
                    $mens2 = "ERROR en el añadido  del voto tabla temporal";
                    $result = db_query($con, $insql2, $mens2);
                }
            }

            $inmsg = "<div class=\"alert alert-success\">" .
                    _("Añadida la votación") . " \" " . $nombre . " \" " . _("la base de datos") . "	</div>";

            if ($tipo == 4) {
                $inmsg .= '<a href="admin.php?c='. encrypt_url('admin/preguntas/idvot='.$id ,$clave_encriptacion).' " class="btn btn-secondary btn-blok">'. _("Para terminar de crear el debate puede formular la pregunta o preguntas") . '</a>';
            } else if($tipo == 6 or $tipo == 7) {

            } else {
                $inmsg .= '<a href="admin.php?c='. encrypt_url('admin/candidatos/idvot='.$id ,$clave_encriptacion).' " class="btn btn-secondary btn-blok">'. _("Para terminar de crear la encuesta /votación tiene que incluir las opciones o candidatos") . '</a>';
            }

            if ($tipo_seg == 3 or $tipo_seg == 4) {
                $inmsg .= '<a href="admin.php?c='. encrypt_url('admin/interventor/idvot='.$id ,$clave_encriptacion).' " class="btn btn-secondary btn-blok">'. _("¡¡¡RECUERDA QUE!!! para terminar de crear esta encuesta /votación tiene que incluir los interventores") . '</a>';
            }
        }
    }
}


if (ISSET($_POST["modifika_votacion"])) {

    $nombre = fn_filtro($con, $_POST['nombre']);
    if (isset($_POST['provincia'])) {
        $provincia = fn_filtro_numerico($con, $_POST['provincia']);
    }
    $texto = fn_filtro_editor($con, $_POST['texto']);
//$tipo=$_POST['tipo'];
    $resumen = fn_filtro_editor($con, $_POST['resumen']);
    $tipo_votante = fn_filtro($con, $_POST['tipo_usuario']);
    $numero_opciones = fn_filtro($con, $_POST['numero_opciones']);
    $comunidad_autonoma = fn_filtro_numerico($con, $_POST['comunidad_autonoma']);

    //posiblemente obsoleto (habia que quitar la entrada en la sentencia sql)
    if (isset($_POST['subgrupo'])) {
        $subgrupo = fn_filtro($con, $_POST['subgrupo']);
    } else {
        $subgrupo = 0;
    }
    // hasta aqui

    $demarcacion = fn_filtro($con, $_POST['demarcacion']);
    $tipo_seg = fn_filtro($con, $_POST['tipo_seg']);
    $estado = fn_filtro($con, $_POST['estado']);
    $tipo = fn_filtro($con, $_POST['tipo']);
    if (isset($_POST['encripta'])) {
        $encripta = fn_filtro($con, $_POST['encripta']);
    }
    if (isset($_POST['si_interventores'])) {
        $si_interventores = fn_filtro($con, $_POST['si_interventores']);
    }

    $fecha_in = $_POST['fecha_ini'] . " " . $_POST['hora_ini'] . ":" . $_POST['min_ini'];
    $fecha_ini = date("Y-m-d H:i", strtotime($fecha_in));
    $fecha_fi = $_POST['fecha_fin'] . " " . $_POST['hora_fin'] . ":" . $_POST['min_fin'];
    $fecha_fin = date("Y-m-d H:i", strtotime($fecha_fi));

    if (isset($_POST['tipo_recuento'])) {
        $recuento = fn_filtro($con, $_POST['tipo_recuento']);
    }
    if ($si_interventores == "permitir") {
        $n_interventores = fn_filtro_numerico($con, $_POST['numero_interventores']);
        $interventor = "si";
        if ($n_interventores == 0) {
            $error = "error";
            $inmsg_error = _("Hay algun  error,  no se puede poner 0 en el numero de opciones ya que ha indicado que quiere interventores especiales, verifique el dato") . " <br/>" . _("¡¡¡OJO!!!Hay algunos datos que debera volver a indicarlos");
        }
    } else {
        $n_interventores = 0;
        $interventor = "no";
    }
    $modif = $_SESSION['ID'];
    $fecha_modif = date("Y-m-d");


    if ($tipo == 1 or $tipo == 2 or $tipo == 3) {
        if ($numero_opciones == "") {
            $error = "error";
            $inmsg_error = _("Hay algun  error, el numero de opciones es un dato requerido , verifique el dato") . " <br/>" . _("¡¡¡OJO!!!Hay algunos datos que debera volver a indicarlos");
        }
    }
    if ($tipo == 1 or $tipo == 2) {
        if ($numero_opciones == 0) {
            $error = "error";
            $inmsg_error = _("Hay algun  error, en las votaciones tipo VUT o PRIMARIAS no se puede poner 0 en el numero de opciones , verifique el dato") . " <br/>" . _("¡¡¡OJO!!!Hay algunos datos que debera volver a indicarlos");
        }
    }

    if ($_POST['demarcacion'] == "") { //Autonomica
        $error = "error";
        $inmsg_error = _("no ha indicado la demarcación de la votación");
    } elseif ($_POST['demarcacion'] == 2) { //Autonomica
        $provincia = 100;
    } else if ($_POST['demarcacion'] == 3) { //provincial
        $comunidad_autonoma = 0;
        if ($_POST['provincia'] == "") {
            $error = "error";
            $inmsg_error = _("Hay algun tipo de error, por alguna causa no ha llegado el dato de provincia, verifique que la ha indicado") . " <br/>" . _("¡¡¡OJO!!!Hay algunos datos que debera volver a indicarlos");
        } else {
            $provincia = fn_filtro($con, $_POST['provincia']);
        }
    } else { //grupo provincial
        $provincia = 00;
        $comunidad_autonoma = 0;
    }

    /**/

    if ($_POST['demarcacion'] == 4) {
        if ($_POST['grupo_trabajo_prov'] == "") {
            $error = "error";
            $inmsg_error = _("Hay algun tipo de error, por alguna causa no ha llegado el dato de grupo de trabajo, verifique que la ha indicado") . " <br/>" . _("¡¡¡OJO!!!Hay algunos datos que debera volver a indicarlos");
        } else {
            $id_grupo_trabajo = fn_filtro($con, $_POST['grupo_trabajo_prov']);
        }
    } else if ($_POST['demarcacion'] == 5) {
        if ($_POST['grupo_trabajo_aut'] == "") {
            $error = "error";
            $inmsg_error = _("Hay algun tipo de error, por alguna causa no ha llegado el dato de grupo de trabajo, verifique que la ha indicado") . " <br/>" . _("¡¡¡OJO!!!Hay algunos datos que debera volver a indicarlos");
        } else {
            $id_grupo_trabajo = fn_filtro($con, $_POST['grupo_trabajo_aut']);
        }
    } else if ($_POST['demarcacion'] == 6) {
        if ($_POST['grupo_trabajo_gen'] == "") {
            $error = "error";
            $inmsg_error = _("Hay algun tipo de error, por alguna causa no ha llegado el dato de grupo de trabajo, verifique que la ha indicado") . " <br/>" . _("¡¡¡OJO!!!Hay algunos datos que debera volver a indicarlos");
        } else {
            $id_grupo_trabajo = fn_filtro($con, $_POST['grupo_trabajo_gen']);
        }
    } else if ($_POST['demarcacion'] == 7) {
        $comunidad_autonoma = 0;
        if ($_POST['provincia2'] == "") {
            $error = "error";
            $inmsg_error = _("Hay algun tipo de error, por alguna causa no ha llegado el dato de provincia, verifique que la ha indicado") . " <br/>" . _("¡¡¡OJO!!!Hay algunos datos que debera volver a indicarlos");
        } else {
            $provincia = fn_filtro($con, $_POST['provincia2']);
        }

        if ($_POST['municipio'] == "") {
            $error = "error";
            $inmsg_error = _("Hay algun tipo de error, por alguna causa no ha llegado el dato de municipio, verifique que la ha indicado") . " <br/>" . _("¡¡¡OJO!!!Hay algunos datos que debera volver a indicarlos");
        } else {
            $municipio = fn_filtro($con, $_POST['municipio']);
        }
    }

    if ($id == "") {
        $error = "error";
        $inmsg_error = _("Hay algun tipo de error, por alguna causa no ha llegado datos");
    }


    if (!isset($error)) {


        $sSQL = "UPDATE $tbn1 SET nombre_votacion=\"$nombre\",id_provincia=\"$provincia\", texto=\"$texto\", tipo_votante=\"$tipo_votante\", resumen=\"$resumen\" , numero_opciones=\"$numero_opciones\",id_ccaa=\"$comunidad_autonoma\", id_subzona=\"$subgrupo\" , fecha_com=\"$fecha_ini\",fecha_fin=\"$fecha_fin\", demarcacion=\"$demarcacion\", seguridad=\"$tipo_seg\" , activa=\"$estado\", interventores=\"$n_interventores\",interventor=\"$interventor\", id_municipio=\"$municipio\", encripta=\"$encripta\",modif=\"$modif\", fecha_modif=\"$fecha_modif\",id_grupo_trabajo=\"$id_grupo_trabajo\" WHERE id='$id'";

        //  mysqli_query($con, $sSQL) or die("Imposible modificar pagina");

        $mens = _("ERROR en la modificacion de la votacion la pagina") . " votacion.php";
        $modifica_votacion = db_query($con, $sSQL, $mens);

        if ($modifica_votacion == false) {
            $inmsg_error = _("Hay algun tipo de error, no se han actualizado los datos");
        } else {

            $inmsg = " <div class=\"alert alert-success\">" . _("Realizadas las Modificaciones") . " <br/>" . _("Asi ha quedado la votación") . ": \" " . $nombre . " \"</div>";

            if ($tipo == 4) {
                $inmsg .= '  <a href="admin.php?c='. encrypt_url('admin/preguntas/idvot='.$id ,$clave_encriptacion).' " class="btn btn-secondary btn-blok">'. _("Para terminar de crear el debate tiene que formular la pregunta o preguntas") . '</a> ';
            } else if($tipo == 6 or $tipo == 7) {

            } else {
                $inmsg .= '  <a href="admin.php?c='. encrypt_url('admin/candidatos/idvot='.$id ,$clave_encriptacion).' " class="btn btn-secondary btn-blok">'. _("Para terminar de crear la encuesta /votación tiene que incluir las opciones o candidatos") . '</a> ';
            }
            if ($tipo_seg == 3 or $tipo_seg == 4) {
                $inmsg .= '  <a href="admin.php?c='. encrypt_url('admin/interventor/idvot='.$id ,$clave_encriptacion).' " class="btn btn-secondary btn-blok">'. _("¡¡¡RECUERDA QUE!!! para terminar de crear la encuesta /votación tiene que incluir los interventores") . '</a> ';
            }
        }
    }
}
?>


                    <!--Comiezo-->


                    <?php
                    if ($acc == "modifika") {
                        $result = mysqli_query($con, "SELECT * FROM $tbn1 where id=$id");
                        $row = mysqli_fetch_row($result);

                        $row_provincia = $row[1];
                        $row_activa = $row[2];
                        $row_nombre = $row[3];
                        $row_texto = $row[4];
                        $row_resumen = $row[5];
                        $row_tipo = $row[6];
                        $row_tipo_votante = $row[7];
                        $row_numero_opciones = $row[8];
                        $row_fecha_com = $row[13];
                        $row_fecha_fin = $row[14];
                        $row_ccaa = $row[17];
                        $row_grupo = $row[19];
                        $row_demarcacion = $row[20];
                        $row_seguridad = $row[21];
                        $row_interventores = $row[22];
                        $row_interventor = $row[23];
                        $row_recuento = $row[24];
                        $row_id_municipio = $row[25];
                        $row_encripta = $row[26];
                    }
                    ?>
                    <div class="card-header">
                    <h1 class="card-title"> <?php
                        if ($acc == "modifika") {
                            echo _("MODIFICAR ESTA VOTACIÓN");
                        } else {
                            echo _("CREAR UNA NUEVA VOTACIÓN");
                        }
                        ?></h1>
                        </div>
<div class="card-body">

                      <?php
                      if (isset($inmsg)) {
                          echo "$inmsg";
                      }
                      ?>
                      <?php
                      if (isset($inmsg_error)) {
                          ?>
                          <div class="alert alert-danger">
                              <a class="close" data-dismiss="alert">x</a>
                              <?php echo "$inmsg_error"; ?>
                          </div>
                      <?php }
                      ?>
<div class="row separador">
  <div class=" col-md-12"><br></div>
</div>
                    <form action="<?php $_SERVER['PHP_SELF'] ?>" method=post name="frmDatos" id="frmDatos"  class="form-horizontal" >

                        <div class="form-group row">
                            <label for="nombre" class=" col-md-3 control-label"><?= _("Nombre votación") ?></label>

                            <div class=" col-md-9">
                                <input name="nombre" type="text"  id="nombre" value="<?php echo "$row_nombre"; ?>"
                                class="form-control" placeholder="<?= _("Nombre de la votación") ?>" required autofocus
                                oninvalid="this.setCustomValidity('<?= _("El nombre de la votación es un dato requerido") ?>')"
                                oninput="this.setCustomValidity('')"  />
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class=" col-md-3 "><?= _("Demarcación") ?></div>
                            <div class=" col-md-9">

                                <?php
                                if ($acc == "modifika") {
                                    if ($row_demarcacion == 1) {
                                        $chek_dem1 = "checked=\"checked\" ";
                                        $display2 = "  style=\"display:none\"";
                                        $display3 = "  style=\"display:none\"";
                                        $display4 = "  style=\"display:none\"";
                                        $display5 = "  style=\"display:none\"";
                                        $display6 = "  style=\"display:none\"";
                                        $display7 = "  style=\"display:none\"";
                                    } else if ($row_demarcacion == 2) {
                                        $chek_dem2 = "checked=\"checked\" ";

                                        $display3 = "  style=\"display:none\"";
                                        $display4 = "  style=\"display:none\"";
                                        $display5 = "  style=\"display:none\"";
                                        $display6 = "  style=\"display:none\"";
                                        $display7 = "  style=\"display:none\"";
                                    } else if ($row_demarcacion == 3) {

                                        $chek_dem3 = "checked=\"checked\" ";

                                        $display2 = "  style=\"display:none\"";

                                        $display4 = "  style=\"display:none\"";
                                        $display5 = "  style=\"display:none\"";
                                        $display6 = "  style=\"display:none\"";
                                        $display7 = "  style=\"display:none\"";
                                    } else if ($row_demarcacion == 4) {

                                        $chek_dem4 = "checked=\"checked\" ";

                                        $display2 = "  style=\"display:none\"";
                                        $display3 = "  style=\"display:none\"";

                                        $display5 = "  style=\"display:none\"";
                                        $display6 = "  style=\"display:none\"";
                                        $display7 = "  style=\"display:none\"";
                                    } else if ($row_demarcacion == 5) {

                                        $chek_dem5 = "checked=\"checked\" ";

                                        $display2 = "  style=\"display:none\"";
                                        $display3 = "  style=\"display:none\"";
                                        $display4 = "  style=\"display:none\"";
                                        $display6 = "  style=\"display:none\"";
                                        $display7 = "  style=\"display:none\"";
                                    } else if ($row_demarcacion == 6) {

                                        $chek_dem6 = "checked=\"checked\" ";

                                        $display2 = "  style=\"display:none\"";
                                        $display3 = "  style=\"display:none\"";
                                        $display4 = "  style=\"display:none\"";
                                        $display5 = "  style=\"display:none\"";
                                        $display7 = "  style=\"display:none\"";
                                    } else if ($row_demarcacion == 7) {

                                        $chek_dem7 = "checked=\"checked\" ";

                                        $display2 = "  style=\"display:none\"";
                                        $display3 = "  style=\"display:none\"";
                                        $display4 = "  style=\"display:none\"";
                                        $display5 = "  style=\"display:none\"";
                                        $display6 = "  style=\"display:none\"";
                                    };

                                } else {
                                    if ($_SESSION['nivel_usu'] == 2) {
                                        $chek_dem1 = "checked=\"checked\" ";
                                        $display1 = "  style=\"display:none\"";
                                        $display2 = "  style=\"display:none\"";
                                        $display3 = "  style=\"display:none\"";
                                        $display4 = "  style=\"display:none\"";
                                        $display5 = "  style=\"display:none\"";
                                        $display6 = "  style=\"display:none\"";
                                        $display7 = "  style=\"display:none\"";
                                    } else if ($_SESSION['nivel_usu'] == 3) {
                                        $chek_dem2 = "checked=\"checked\" ";
                                        $display1 = "  style=\"display:none\"";
                                        $display2 = "  style=\"display:none\"";
                                        $display3 = "  style=\"display:none\"";
                                        $display4 = "  style=\"display:none\"";
                                        $display5 = "  style=\"display:none\"";
                                        $display6 = "  style=\"display:none\"";
                                        $display7 = "  style=\"display:none\"";
                                    } else if ($_SESSION['nivel_usu'] == 4) {
                                        $chek_dem3 = "checked=\"checked\" ";
                                        $display1 = "  style=\"display:none\"";
                                        $display2 = "  style=\"display:none\"";

                                        $display4 = "  style=\"display:none\"";
                                        $display5 = "  style=\"display:none\"";
                                        $display6 = "  style=\"display:none\"";
                                        $display7 = "  style=\"display:none\"";
                                    } else if ($_SESSION['nivel_usu'] == 5) {
                                        $chek_dem6 = "checked=\"checked\" ";
                                        $display1 = "  style=\"display:none\"";
                                        $display2 = "  style=\"display:none\"";
                                        $display3 = "  style=\"display:none\"";
                                        $display4 = "  style=\"display:none\"";
                                        $display5 = "  style=\"display:none\"";
                                        $display6 = "  style=\"display:none\"";
                                        $display7 = "  style=\"display:none\"";
                                    } else if ($_SESSION['nivel_usu'] == 6) {
                                        $chek_dem5 = "checked=\"checked\" ";
                                        $display1 = "  style=\"display:none\"";
                                        $display2 = "  style=\"display:none\"";
                                        $display3 = "  style=\"display:none\"";
                                        $display4 = "  style=\"display:none\"";
                                        $display5 = "  style=\"display:none\"";
                                        $display6 = "  style=\"display:none\"";
                                        $display7 = "  style=\"display:none\"";
                                    } else if ($_SESSION['nivel_usu'] == 7) {
                                        $chek_dem4 = "checked=\"checked\" ";
                                        $display1 = "  style=\"display:none\"";
                                        $display2 = "  style=\"display:none\"";
                                        $display3 = "  style=\"display:none\"";
                                        $display4 = "  style=\"display:none\"";
                                        $display5 = "  style=\"display:none\"";
                                        $display6 = "  style=\"display:none\"";
                                        $display7 = "  style=\"display:none\"";
                                    }

                                }

                                ?>

                                <?php if ($_SESSION['nivel_usu'] == 2) { ?>
                                    <label for="demarcacion_0" class="control-label">
                                        <input name="demarcacion" type="radio" id="demarcacion_0" value="1" onClick="habilita_estatal()" <?php
                                        if (isset($chek_dem1)) {
                                            echo $chek_dem1;
                                        }
                                        ?> />
                                        <?php if ($es_municipal == false) { ?> <?= _("Estatal") ?><?php } else { ?> <?= _("General") ?><?php } ?></label>
                                    <br />
                                    <?php
                                }
                                if ($es_municipal == false) {
                                    if ($_SESSION['nivel_usu'] <= 3) {
                                        ?>
                                        <label for="demarcacion_1" class="control-label">
                                            <input type="radio" name="demarcacion" value="2" id="demarcacion_1"  onclick="habilita_autonomico()" <?php
                                            if (isset($chek_dem2)) {
                                                echo $chek_dem2;
                                            }
                                            ?>/>
                                            <?= _("Autonomica") ?></label>
                                        <br />
                                        <?php
                                    }

                                    if ($_SESSION['nivel_usu'] <= 4) {
                                        ?>
                                        <label for="demarcacion_2" class="control-label">
                                            <input type="radio" name="demarcacion" value="3" id="demarcacion_2" onClick="habilita_provincial()" <?php
                                            if (isset($chek_dem3)) {
                                                echo $chek_dem3;
                                            }
                                            ?>/>
                                            <?= _("Provincial") ?></label>
                                        <br />
                                        <label for="demarcacion_7" class="control-label">
                                            <input type="radio" name="demarcacion" value="7" id="demarcacion_7" onClick="habilita_municipal()" <?php
                                            if (isset($chek_dem7)) {
                                                echo $chek_dem7;
                                            }
                                            ?>/>
                                            <?= _("Municipal") ?></label>
                                        <br />
                                      <label for="demarcacion_3" class="control-label">
                                            <input type="radio" name="demarcacion" value="4" id="demarcacion_3" onClick="habilita_local()" <?php
                                            if (isset($chek_dem4)) {
                                                echo $chek_dem4;
                                            }
                                            ?>/>
                                            <?= _("Grupo provincial") ?></label>
                                        <br />
                                        <?php
                                    }

                                    if ($_SESSION['nivel_usu'] <= 3) {
                                        ?>
                                        <label for="demarcacion_4" class="control-label">
                                            <input type="radio" name="demarcacion" value="5" id="demarcacion_4" onClick="habilita_g_trabajo()" <?php
                                            if (isset($chek_dem5)) {
                                                echo $chek_dem5;
                                            }
                                            ?> />
                                            <?= _("Grupo Autonomico") ?></label>
                                        <br />
                                        <?php
                                    }
                                }
                                if ($_SESSION['nivel_usu'] <= 2) {
                                    ?>
                                    <label for="demarcacion_5" class="control-label">
                                        <input type="radio" name="demarcacion" value="6" id="demarcacion_5" onClick="habilita_g_trabajo_general()" <?php
                                        if (isset($chek_dem6)) {
                                            echo $chek_dem6;
                                        }
                                        ?>/>
                                        <?= _("Grupo") ?> <?php if ($es_municipal == false) { ?> <?= _("Estatal") ?><?php } else { ?> General<?php } ?></label>
                                    <br />
                                <?php } ?>
                                <?php
                                if ($_SESSION['nivel_usu'] == 6) {
                                    //  demarcacion grupos estatales
                                    ?>
                                    <?php if ($es_municipal == false) { ?> <?= _("Estatal") ?><?php } else { ?><?= _("de trabajo General") ?><?php } ?>
                                <?php } ?>
                                <?php
                                if ($es_municipal == false) {
                                    if ($_SESSION['nivel_usu'] == 7) {
                                        // demmarcacion grupos autonomicos
                                        ?>
                                        <label for="demarcacion_4" class="control-label">
                                            <input type="radio" name="demarcacion" value="5" id="demarcacion_4" onClick="habilita_g_trabajo()" <?php
                                            if (isset($chek_dem5)) {
                                                echo $chek_dem5;
                                            }
                                            ?>/>
                                            <?= _("Grupo Autonomico") ?></label>
                                        <br />
                                        <label for="demarcacion_3" class="control-label">
                                            <input type="radio" name="demarcacion" value="4" id="demarcacion_3" onClick="habilita_local()" <?php
                                            if (isset($chek_dem4)) {
                                                echo $chek_dem4;
                                            }
                                            ?>/>
                                            <?= _("Grupo provincial") ?></label>
                                        <br />
                                    <?php } ?>
                                    <?php
                                    if ($_SESSION['nivel_usu'] == 5) {
                                        //demarcacion grupos provincial
                                        ?>
                                        <?= _("Provincial") ?>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                        </div>


                        <!--                  -->

                        <div class="form-group row">
                            <label for="fecha_ini" class=" col-md-3 control-label"> </label>
                            <div class=" col-md-9">

                                <div id="autonomico"  class="caja_de_display"  <?php
                                if (isset($display2)) {
                                    echo $display2;
                                }
                                ?>>
                                    <div align="left">
                                        <?php
                                        $lista_ccaa = "";
                                        if ($_SESSION['nivel_usu'] == 2) {
                                            ?>
                                            <h4> <?= _("Escoja una Comunidad Autonoma") ?> </h4>
                                            <?php
                                            $options_ccaa = "select DISTINCT ID, ccaa from $tbn3  order by ID";
                                            $resulta_ccaa = mysqli_query($con, $options_ccaa) or die("error: " . mysqli_error());

                                            while ($listrows_ccaa = mysqli_fetch_array($resulta_ccaa)) {
                                                $id_ccaa = $listrows_ccaa['ID'];
                                                if ($id_ccaa == $row_ccaa) {
                                                    $check = "selected=\"selected\" ";
                                                } else {
                                                    $check = "";
                                                }
                                                $name_ccaa = $listrows_ccaa['ccaa'];
                                                $lista_ccaa .= "<option value=\"$id_ccaa\" $check> $name_ccaa</option>";
                                            }
                                            ?>
                                            <select name="comunidad_autonoma" class="form-control custom-select" id="comunidad_autonoma" >
                                                <?php echo "$lista_ccaa"; ?>
                                            </select>
                                            <?php
                                        } else {

                                            $ids_ccaa = $_SESSION['id_ccaa_usu'];
                                            $options_ccaa = "select DISTINCT  ID, ccaa from $tbn3  where id=$ids_ccaa";
                                            $resulta_ccaa = mysqli_query($con, $options_ccaa) or die("error: " . mysqli_error());

                                            while ($listrows_ccaa = mysqli_fetch_array($resulta_ccaa)) {
                                                $ccaa = $listrows_ccaa['ccaa'];
                                            }
                                            ?>
                                            <p><?= _("Comunidad autonoma de") ?> :
                                                <input name="comunidad_autonoma" type="hidden" id="comunidad_autonoma" value="<?php echo "$ids_ccaa"; ?>" />
                                                <?php
                                                echo "$ccaa";
                                            }
                                            ?></p>
                                    </div>
                                </div>



                                <div id="provincial"  class="caja_de_display"   <?php
                                if (isset($display3)) {
                                    echo $display3;
                                }
                                ?>  >
                                    <div align="left">
                                        <?php
                                        $lista1 = "";
                                        if ($_SESSION['nivel_usu'] == 2) {
                                            // listar para meter en una lista del cuestionario buscador


                                            $options = "select DISTINCT id, provincia from $tbn8  where especial=0 order by ID";
                                            $resulta = mysqli_query($con, $options) or die("error: " . mysqli_error());

                                            while ($listrows = mysqli_fetch_array($resulta)) {
                                                $id_pro = $listrows['id'];
                                                $name1 = $listrows['provincia'];
                                                if ($id_pro == $row_provincia) {
                                                    $check = "selected=\"selected\" ";
                                                } else {
                                                    $check = "";
                                                }
                                                $lista1 .= "<option value=\"$id_pro\"  $check> $name1</option>";
                                            }
                                            ?>
                                            <h4> <?= _("Escoja una Provincia") ?> </h4>
                                            <select name="provincia" class="form-control custom-select" id="provincia" >
                                                <?php echo "$lista1"; ?>
                                            </select>
                                            <?php
                                        } else if ($_SESSION['nivel_usu'] == 3) {

                                            $options = "select DISTINCT id, provincia from $tbn8  where id_ccaa=$ids_ccaa  order by ID";
                                            $resulta = mysqli_query($con, $options) or die("error: " . mysqli_error());

                                            while ($listrows = mysqli_fetch_array($resulta)) {
                                                $id_pro = $listrows['id'];
                                                $name1 = $listrows['provincia'];
                                                if ($id_pro == $row_provincia) {
                                                    $check = "selected=\"selected\" ";
                                                } else {
                                                    $check = "";
                                                }
                                                $lista1 .= "<option value=\"$id_pro\"  $check> $name1</option>";
                                            }
                                            ?>
                                            <h4> <?= _("Escoja una Provincia") ?> </h4>
                                            <select name="provincia" class="form-control custom-select" id="provincia" >
                                                <?php echo "$lista1"; ?>
                                            </select>
                                            <?php
                                        } else {

                                            $result2 = mysqli_query($con, "SELECT id_provincia FROM $tbn5 where id_usuario=" . $_SESSION['ID']);
                                            $quants2 = mysqli_num_rows($result2);
//$row2=mysqli_fetch_row($result2);

                                            if ($quants2 != 0) {

                                                while ($listrows2 = mysqli_fetch_array($result2)) {

                                                    $name2 = $listrows2['id_provincia'];
                                                    $optiones = mysqli_query($con, "SELECT  provincia FROM $tbn8 where ID=$name2");
                                                    $row_prov = mysqli_fetch_row($optiones);
                                                    if ($acc == "modifika") {
                                                        if ($name2 == $row_provincia) {
                                                            $check = "checked=\"checked\" ";
                                                        } else {
                                                            $check = "";
                                                        }
                                                    } else {
                                                        $check = "checked=\"checked\" ";
                                                    }

                                                    $lista1 .= "    <label><input  type=\"radio\" name=\"provincia\" value=\"$name2\"   $check id=\"provincia\" /> " . $row_prov[0] . "</label> <br/>";
                                                }
                                                echo "$lista1";
                                            } else {
                                                echo _("No tiene asignadas provincias, no podra crear votación");
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>

                                <!---->

                                <div id="g_municipal"  class="caja_de_display"   <?php
                                if (isset($display7)) {
                                    echo $display7;
                                }
                                ?> >
                                    <div align="left">
                                        <?php
                                        $lista2 = "";
                                        if ($_SESSION['nivel_usu'] == 2) {
                                            // listar para meter en una lista del cuestionario buscador


                                            $options = "select DISTINCT id, provincia from $tbn8  where especial=0 order by ID";
                                            $resulta = mysqli_query($con, $options) or die("error: " . mysqli_error());

                                            while ($listrows = mysqli_fetch_array($resulta)) {
                                                $id_pro = $listrows['id'];
                                                $name1 = $listrows['provincia'];
                                                if ($id_pro == $row_provincia) {
                                                    $check = "selected=\"selected\" ";
                                                } else {
                                                    $check = "";
                                                }
                                                $lista2 .= "<option value=\"$id_pro\"  $check> $name1</option>";
                                            }
                                            ?>
                                            <h4> <?= _("Escoja una Provincia") ?> </h4>
                                                <select name="provincia2" class="form-control custom-select" id="provincia2" >
                                                    <?php echo "$lista2"; ?>
                                                </select>

                                                <h4><?= _("Escoja Municipio") ?> </h4>

                                                <select name="municipio" id="municipio" class="form-control custom-select" > </select>







                                                <?php
                                            } else if ($_SESSION['nivel_usu'] == 3) {

                                                $options = "select DISTINCT id, provincia from $tbn8  where id_ccaa=$ids_ccaa  order by ID";
                                                $resulta = mysqli_query($con, $options) or die("error: " . mysqli_error());

                                                while ($listrows = mysqli_fetch_array($resulta)) {
                                                    $id_pro = $listrows['id'];
                                                    $name1 = $listrows['provincia'];
                                                    $name = $listrows['id'];
                                                    if ($acc == "modifika") {
                                                        if ($id_pro == $row_provincia) {
                                                            $check = "selected=\"selected\" ";
                                                        } else {
                                                            $check = "";
                                                        }
                                                    } else {
                                                        $check = "selected=\"selected\" ";
                                                    }
                                                    $lista2 .= "<option value=\"$id_pro\"  $check> $name1</option>";
                                                }
                                                ?>
                                                <h4> <?= _("Escoja una Provincia") ?> </h4>
                                                <select name="provincia2" class="form-control custom-select" id="provincia2" >
                                                    <?php echo "$lista2"; ?>
                                                </select>

                                                <h4><?= _("Escoja Municipio") ?> </h4>

                                                <select name="municipio" id="municipio" class="form-control custom-select" > </select>




                                                <?php
                                            } else {

                                                $result2 = mysqli_query($con, "SELECT id_provincia FROM $tbn5 where id_usuario=" . $_SESSION['ID']);
                                                $quants2 = mysqli_num_rows($result2);
//$row2=mysqli_fetch_row($result2);

                                                if ($quants2 != 0) {

                                                    while ($listrows2 = mysqli_fetch_array($result2)) {

                                                        $name2 = $listrows2['id_provincia'];
                                                        $name = $listrows2['id_provincia'];
                                                        $optiones = mysqli_query($con, "SELECT  provincia FROM $tbn8 where ID=$name2");
                                                        $row_prov = mysqli_fetch_row($optiones);

                                                        if ($acc == "modifika") {
                                                            if ($name2 == $row_provincia) {
                                                                $check = "checked=\"checked\" ";
                                                            } else {
                                                                $check = "";
                                                            }
                                                        } else {
                                                            $check = "checked=\"checked\" ";
                                                        }

                                                        $lista2 .= "    <label><input  type=\"radio\" name=\"provincia2\" value=\"$name2\"   $check id=\"provincia2\" /> " . $row_prov[0] . "</label> <br/>";
                                                    }
                                                    echo "$lista2";


                                                    echo" <h4>" . _("Escoja Municipio") . " </h3>

                                                    <select name=\"municipio\" id=\"municipio\" class=\"form-control custom-select\" > </select>";
                                                } else {
                                                    echo _("No tiene asignadas provincias, no podra crear votación");
                                                }
                                            }
                                            ?>
                                    </div>
                                </div>




                                <!---->

                                <div id="local"  class="caja_de_display"  <?php
                                if (isset($display4)) {
                                    echo $display4;
                                }
                                ?>>
                                    <h4> <?= _("Escoja la asamblea provincial") ?></h4>
                                    <?php
                                    $lista5 = "";
                                    if ($_SESSION['nivel_usu'] == 2) {

                                        $result2 = mysqli_query($con, "SELECT ID ,subgrupo,tipo_votante, id_provincia FROM $tbn4  where tipo=1 order by id_provincia");
                                        $quants2 = mysqli_num_rows($result2);
//$row2=mysqli_fetch_row($result2);

                                        if ($quants2 != 0) {

                                            while ($listrows2 = mysqli_fetch_array($result2)) {
                                                $id_grupo = $listrows2['ID'];
                                                $id_prov = $listrows2['id_provincia'];
                                                $subgrupo = $listrows2['subgrupo'];

                                                if ($id_grupo == $row_grupo) {
                                                    $check = "selected=\"selected\" ";
                                                } else {
                                                    $check = "";
                                                }

                                                $optiones = mysqli_query($con, "SELECT  provincia FROM $tbn8 where ID=$id_prov");
                                                $row_prov = mysqli_fetch_row($optiones);
                                                $lista5 .= "<option value=\"$id_grupo\" $check> " . $row_prov[0] . " - " . $subgrupo . " </option>";
                                            }
                                            echo " <select name=\"grupo_trabajo_prov\" class=\"form-control custom-select\" id=\"grupo_trabajo_prov\" > $lista5 </select>";
                                        } else {
                                            echo _("No tiene asignado grupos, no podra crear votación");
                                        }
                                    }



                                    //  admin CCAA, meter los que tiene asignados en su ccaa
                                    else if ($_SESSION['nivel_usu'] == 3) {
                                        echo $_SESSION['id_ccaa_usu'];
                                        $result2 = mysqli_query($con, "SELECT ID ,subgrupo,tipo_votante, id_provincia FROM $tbn4  where tipo=1 and id_ccaa=" . $_SESSION['id_ccaa_usu'] . " order by id_provincia");
                                        $quants2 = mysqli_num_rows($result2);
//$row2=mysqli_fetch_row($result2);

                                        if ($quants2 != 0) {

                                            while ($listrows2 = mysqli_fetch_array($result2)) {
                                                $id_grupo = $listrows2['ID'];
                                                $id_prov = $listrows2['id_provincia'];
                                                $subgrupo = $listrows2['subgrupo'];
                                                if ($id_grupo == $row_grupo) {
                                                    $check = "checked=\"checked\" ";
                                                } else {
                                                    $check = "";
                                                }

                                                $optiones = mysqli_query($con, "SELECT  provincia FROM $tbn8 where ID=$id_prov");
                                                $row_prov = mysqli_fetch_row($optiones);
                                                $lista5 .= "    <label><input  type=\"radio\" name=\"grupo_trabajo_prov\" value=\"$id_grupo\"  $check id=\"grupo_trabajo_prov\" /> " . $subgrupo . " - " . $row_prov[0] . "</label> <br/>";
                                            }
                                            echo "$lista5";
                                        } else {
                                            echo _("No tiene asignado grupos provinciales, no podra crear votación");
                                        }
                                    }





                                    //  provincial, meter los que tiene asignado
                                    else if ($_SESSION['nivel_usu'] == 4 or $_SESSION['nivel_usu'] == 7) {

                                        $result2 = mysqli_query($con, "SELECT a.ID ,a.subgrupo,a.tipo_votante, a.id_provincia FROM $tbn4 a,$tbn6 b where (a.ID= b.id_grupo_trabajo) and b.id_usuario=" . $_SESSION['ID'] . " and a.tipo=1 order by a.id_provincia");
                                        $quants2 = mysqli_num_rows($result2);
//$row2=mysqli_fetch_row($result2);

                                        if ($quants2 != 0) {

                                            while ($listrows2 = mysqli_fetch_array($result2)) {
                                                $id_grupo = $listrows2['ID'];

                                                $id_prov = $listrows2['id_provincia'];
                                                $subgrupo = $listrows2['subgrupo'];
                                                if ($id_grupo == $row_grupo) {
                                                    $check = "checked=\"checked\" ";
                                                } else {
                                                    $check = "";
                                                }

                                                $optiones = mysqli_query($con, "SELECT  provincia FROM $tbn8 where ID=$id_prov");
                                                $row_prov = mysqli_fetch_row($optiones);
                                                $lista5 .= "    <label><input  type=\"radio\" name=\"grupo_trabajo_prov\" value=\"$id_grupo\"  $check  id=\"grupo_trabajo_prov\" /> " . $subgrupo . " - " . $row_prov[0] . "</label> <br/>";
                                            }
                                            echo "$lista5";
                                        } else {
                                            echo _("No tiene asignado grupos provinciales, no podra crear votación");
                                        }
                                    }
                                    ?>
                                </div>
                                <?php
////////si es un administrador de grupo local provincial metemos sus datos pero
                                if ($_SESSION['nivel_usu'] == 5) {

                                    $result2 = mysqli_query($con, "SELECT a.ID ,a.subgrupo,a.tipo_votante, a.id_provincia FROM $tbn4 a,$tbn6 b where (a.ID= b.id_grupo_trabajo) and a.tipo=1 and b.id_usuario=" . $_SESSION['ID'] . " order by a.id_provincia");
                                    $quants2 = mysqli_num_rows($result2);
//$row2=mysqli_fetch_row($result2);

                                    if ($quants2 != 0) {

                                        while ($listrows2 = mysqli_fetch_array($result2)) {
                                            $id_grupo = $listrows2['ID'];
                                            $id_prov = $listrows2['id_provincia'];
                                            $subgrupo = $listrows2['subgrupo'];
                                            if ($id_grupo == $row_grupo) {
                                                $check = "checked=\"checked\" ";
                                            } else {
                                                $check = "";
                                            }

                                            $optiones = mysqli_query($con, "SELECT  provincia FROM $tbn8 where ID=$id_prov");
                                            $row_prov = mysqli_fetch_row($optiones);
                                            $lista5 .= "    <label><input  type=\"radio\" name=\"grupo_trabajo_prov\" value=\"$id_grupo\"  $check  id=\"grupo_trabajo_prov\" /> " . $subgrupo . " - " . $row_prov[0] . "</label> <br/>";
                                        }
                                        echo "$lista5";
                                    } else {
                                        echo _("No tiene asignado grupos, no podra crear votación");
                                    }
                                }
                                ?>


                                <div id="g_trabajo"  class="caja_de_display"   <?php
                                if (isset($display5)) {
                                    echo $display5;
                                }
                                ?>>
                                    <h4> <?= _("Escoja la asamblea Autonomico") ?> </h4>
                                    <?php
                                    $lista3 = "";
                                    if ($_SESSION['nivel_usu'] == 2) {

                                        $result2 = mysqli_query($con, "SELECT ID ,subgrupo,tipo_votante, id_ccaa FROM $tbn4  where tipo=2 order by id_ccaa");
                                        $quants2 = mysqli_num_rows($result2);
//$row2=mysqli_fetch_row($result2);

                                        if ($quants2 != 0) {

                                            while ($listrows2 = mysqli_fetch_array($result2)) {
                                                $id_grupo = $listrows2['ID'];
                                                $id_ccaa = $listrows2['id_ccaa'];
                                                $subgrupo = $listrows2['subgrupo'];
                                                if ($id_grupo == $row_grupo) {
                                                    $check = "selected=\"selected\" ";
                                                } else {
                                                    $check = "";
                                                }

                                                $optiones = mysqli_query($con, "SELECT ccaa FROM $tbn3 where ID=$id_ccaa");
                                                $row_prov = mysqli_fetch_row($optiones);
                                                $lista3 .= "<option value=\"$id_grupo\" $check> " . $row_prov[0] . " - " . $subgrupo . " </option>";
                                            }
                                            echo " <select name=\"grupo_trabajo_aut\" class=\"form-control custom-select\" id=\"grupo_trabajo_aut\" $check > $lista3 </select>";
                                        } else {
                                            echo _("No hay grupos Autonomicos, no podra crear votación");
                                        }
                                    }



                                    //  admin CCAA, meter los que tiene asignados en su ccaa
                                    else if ($_SESSION['nivel_usu'] == 3) {

                                        $result2 = mysqli_query($con, "SELECT ID ,subgrupo,tipo_votante, id_ccaa FROM $tbn4  where tipo=2 and id_ccaa=" . $_SESSION['id_ccaa_usu'] . " order by id_ccaa");
                                        $quants2 = mysqli_num_rows($result2);
//$row2=mysqli_fetch_row($result2);

                                        if ($quants2 != 0) {

                                            while ($listrows2 = mysqli_fetch_array($result2)) {
                                                $id_grupo = $listrows2['ID'];
                                                $id_ccaa = $listrows2['id_ccaa'];
                                                $subgrupo = $listrows2['subgrupo'];
                                                if ($id_grupo == $row_grupo) {
                                                    $check = "checked=\"checked\" ";
                                                } else {
                                                    $check = "";
                                                }

                                                $optiones = mysqli_query($con, "SELECT  ccaa FROM $tbn3 where ID=$id_ccaa");
                                                $row_prov = mysqli_fetch_row($optiones);
                                                $lista3 .= "    <label><input  type=\"radio\" name=\"grupo_trabajo_aut\" value=\"$id_grupo\"  $check  id=\"grupo_trabajo_aut\" /> " . $subgrupo . " - " . $row_prov[0] . "</label> <br/>";
                                            }
                                            echo "$lista3";
                                        } else {
                                            echo _("No tiene asignado grupos autonomicos, no podra crear votación");
                                        }
                                    } else if ($_SESSION['nivel_usu'] == 7) {

                                        $result2 = mysqli_query($con, "SELECT a.ID ,a.subgrupo,a.tipo_votante, a.id_ccaa FROM $tbn4 a ,$tbn6 b where (a.ID= b.id_grupo_trabajo) and a.tipo=2 and b.id_usuario=" . $_SESSION['ID'] . " order by a.id_ccaa");
                                        $quants2 = mysqli_num_rows($result2);
//$row2=mysqli_fetch_row($result2);

                                        if ($quants2 != 0) {

                                            while ($listrows2 = mysqli_fetch_array($result2)) {
                                                $id_grupo = $listrows2['ID'];
                                                $id_ccaa = $listrows2['id_ccaa'];
                                                $subgrupo = $listrows2['subgrupo'];
                                                if ($id_grupo == $row_grupo) {
                                                    $check = "checked=\"checked\" ";
                                                } else {
                                                    $check = "";
                                                }

                                                $optiones = mysqli_query($con, "SELECT  ccaa FROM $tbn3 where ID=$id_ccaa");
                                                $row_prov = mysqli_fetch_row($optiones);
                                                $lista3 .= "    <label><input  type=\"radio\" name=\"grupo_trabajo_aut\" value=\"$id_grupo\"  $check  id=\"grupo_trabajo_aut\" /> " . $subgrupo . " - " . $row_prov[0] . "</label> <br/>";
                                            }
                                            echo "$lista3";
                                        } else {
                                            echo _("No tiene asignado grupos Autonomicos, no podra crear votación");
                                        }
                                    }
                                    ?>
                                </div>






                                <div id="g_trabajo_general"  class="caja_de_display"  <?php
                                if (isset($display6)) {
                                    echo $display6;
                                }
                                ?>>
                                    <h4> <?= _("Escoja grupo  Estatal") ?> </h4>

                                    <?php
                                    $lista4 = "";
                                    if ($_SESSION['nivel_usu'] == 2) {

                                        $result2 = mysqli_query($con, "SELECT ID ,subgrupo,tipo_votante FROM $tbn4  where tipo=3 order by subgrupo");
                                        $quants2 = mysqli_num_rows($result2);
//$row2=mysqli_fetch_row($result2);

                                        if ($quants2 != 0) {

                                            while ($listrows2 = mysqli_fetch_array($result2)) {

                                                $id_grupo = $listrows2['ID'];
                                                $subgrupo = $listrows2['subgrupo'];
                                                if ($id_grupo == $row_grupo) {
                                                    $check = "selected=\"selected\" ";
                                                } else {
                                                    $check = "";
                                                }

                                                $lista4 .= "<option value=\"$id_grupo\" $check>  " . $subgrupo . " </option>";
                                            }
                                            echo " <select name=\"grupo_trabajo_gen\" class=\"form-control custom-select\" id=\"grupo_trabajo_gen\" $check > $lista4 </select>";
                                        } else {
                                            echo _("No hay grupos, no podra crear votación");
                                        }
                                    }
                                    ?>
                                </div>
                                <?php
                                if ($_SESSION['nivel_usu'] == 6) {

                                    echo "<input name=\"demarcacion\" type=\"hidden\" id=\"demarcacion\" value=\"6\" />  ";

                                    $result2 = mysqli_query($con, "SELECT a.ID ,a.subgrupo,a.tipo_votante FROM $tbn4 a,$tbn6 b where (a.ID= b.id_grupo_trabajo)  and a.tipo=3 and b.admin=1 order by a.subgrupo");
                                    $quants2 = mysqli_num_rows($result2);
//$row2=mysqli_fetch_row($result2);

                                    if ($quants2 != 0) {

                                        while ($listrows2 = mysqli_fetch_array($result2)) {

                                            $id_grupo = $listrows2['ID'];
                                            $subgrupo = $listrows2['subgrupo'];
                                            if ($id_grupo == $row_grupo) {
                                                $check = "selected=\"selected\" ";
                                            } else {
                                                $check = "";
                                            }

                                            $lista4 .= "<option value=\"$id_grupo\" $check>  " . $subgrupo . " </option>";
                                        }
                                        echo " <select name=\"grupo_trabajo_gen\" class=\"form-control custom-select\" id=\"grupo_trabajo_gen\" > $lista4 </select>";
                                    } else {
                                        echo _("No hay grupos, no podra crear votación");
                                    }
                                }
                                ?>
                                <br/>
                            </div>
                        </div>

                        <!--fin de grupos-->
<!-- fecha de la votacion -->
                        <div class="form-group row">
                            <label for="fecha_ini" class=" col-md-3 control-label"> <?= _("Fecha comienzo") ?></label>

                            <div class=" col-md-3">

                                <?php
                                if ($acc == "modifika") {

                                    $fecha_i = date("Y-m-d", strtotime($row_fecha_com));
                                } else {

                                    $fecha_i = "";
                                }
                                ?>


                                <input  name="fecha_ini" type="date" class="form-control" id="fecha_ini" value="<?php echo "$fecha_i"; ?>"
                                placeholder="aaaa-mm-dd" required
                                oninvalid="this.setCustomValidity('<?= _("La fecha de inicio es un dato requerido") ?>')"
                                oninput="this.setCustomValidity('')"
                                required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}"
                                 />
                            </div>



                            <label for="hora_ini" class=" col-md-1 control-label  text-right"> <?= _("Horas") ?> : </label>
                            <div class=" col-md-2">
                                <select name="hora_ini" id="hora_ini" class="custom-select" >
                                    <?php
                                    if ($acc == "modifika") {
                                        $hora_i = date("H", strtotime($row_fecha_com));
                                    } else {
                                        $hora_i = 8;
                                    }
                                    for ($i = 0; $i < 24; $i++) {
                                        if ($i == $hora_i) {
                                            $selecionado = "selected=\"selected\"";
                                        } else {
                                            $selecionado = "";
                                        }
                                        echo "<option value=\"" . $i . "\" " . $selecionado . ">" . $i . "</option>";
                                    }
                                    ?>
                                </select>
                              </div>

                            <label for="min_ini" class=" col-md-1 control-label  text-right"> <?= _("Minutos") ?> : </label>
                              <div class=" col-md-2">


                                <?php $min_i = date("i", strtotime($row_fecha_com)); ?>
                                <select name="min_ini" id="min_ini" class="custom-select">
                                    <option value="00" <?php if ($min_i == 00) { ?>selected="selected"<?php } ?>>00</option>
                                    <option value="30" <?php if ($min_i == 30) { ?>selected="selected"<?php } ?>>30</option>
                                </select>

                            </div>

                        </div>

<!--  comienzo de la sefunda fecha -->

                        <div class="form-group row">
                            <label for="fecha_final" class=" col-md-3 control-label"><?= _("Fecha final") ?> </label>




                            <div class=" col-md-3">

                                <?php
                                if ($acc == "modifika") {
                                    $fecha_f = date("Y-m-d", strtotime($row_fecha_fin));
                                } else {
                                    $fecha_f = "";
                                }
                                ?>
                        <input name="fecha_fin" type="date" class="form-control" id="fecha_fin" value="<?php echo "$fecha_f"; ?>"
                        placeholder="aaaa-mm-dd"  required
                        oninvalid="this.setCustomValidity('<?= _("La fecha de final es un dato requerido") ?>')"
                        oninput="this.setCustomValidity('')"
                        required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
                            </div>


                            <label for="hora_fin" class=" col-md-1 control-label text-right"> <?= _("Horas") ?> : </label>
                            <div class=" col-md-2">

                                <select name="hora_fin" id="hora_fin" class="custom-select">
                                    <?php
                                    if ($acc == "modifika") {
                                        $hora_f = date("H", strtotime($row_fecha_fin));
                                    } else {
                                        $hora_f = 23;
                                    }
                                    for ($i = 1; $i < 24; $i++) {
                                        if ($i == $hora_f) {
                                            $selecionado1 = "selected=\"selected\"";
                                        } else {
                                            $selecionado1 = "";
                                        }
                                        echo "<option value=\"" . $i . "\" " . $selecionado1 . ">" . $i . "</option>";
                                    }
                                    ?>
                                </select>
                                :<?php
                                if ($acc == "modifika") {
                                    $min_f = date("i", strtotime($row_fecha_fin));
                                } else {
                                    $min_f = 59;
                                }
                                ?>


                              </div>



                          <label for="hora_fin" class=" col-md-1 control-label text-right"> <?= _("Minutos") ?> : </label>
                              <div class=" col-md-2">

                                <select name="min_fin" id="min_fin" class="custom-select">
                                    <option value="00" <?php if ($min_f == 00) { ?>selected="selected"<?php } ?>>00</option>
                                    <option value="30" <?php if ($min_f == 30) { ?>selected="selected"<?php } ?>>30</option>
                                    <option value="59" <?php if ($min_f == 59) { ?>selected="selected"<?php } ?>>59</option>
                                </select>
                            </div>



                        </div>
<!-- fin fechas votaciòn -->

<!-- comienzo tipos de votación-->
                        <div class="form-group row">
                            <label for="tipo" class=" col-md-3 control-label"><?= _("Tipo de votación") ?> </label>
                            <div class=" col-md-9">


                                <?php
                                if ($acc == "modifika") {
                                    echo $row_tipo . "| ";
                                    ?>  <input name="tipo" type="hidden" id="tipo" value="<?php echo "$row_tipo"; ?>" />  <?php
                                    if ($row_tipo == 1) {
                                        echo _("PRIMARIAS");
                                        if ($row_interventores == 0) {
                                            echo _("con recuento BORDA");
                                        } else if ($row_interventores == 1) {
                                            echo _("con recuento DOWDALL");
                                        }
                                    } else if ($row_tipo == 2) {
                                        echo _("VUT");
                                        $display_seg = "  style=\"display:none\"";
                                    } else if ($row_tipo == 3) {
                                        echo _("ENCUESTA");
                                    } else if ($row_tipo == 5) {
                                        echo _("ENCUESTA con poderación del voto según tipo votante");
                                    } else if ($row_tipo == 6) {
                                          echo _("ENCUESTA con poderación del voto según tipo votante") . " ". _("binaria");
                                          $display_binario = "  style=\"display:none\"";
                                    } else if ($row_tipo == 7) {
                                          echo _("ENCUESTA") . " ". _("binaria");
                                          $display_binario = "  style=\"display:none\"";
                                    } else if ($row_tipo == 4) {
                                        echo _("DEBATE");
                                        $display_debate = "  style=\"display:none\"";
                                    }
                                } else {
                                    ?>

                                    <label>
                                        <input name="tipo" type="radio" id="tipo_0" value="1" checked="checked"  onClick="pon_opciones1()"/>
                                        <?= _("Primarias / orden") ?></label>
                                    <div id="recuento"  class="caja_de_display_b"  >
                                        <h5> <?= _("Tipo de recuento") ?></h5>

                                            <label for "tipo_recuento_0">
                                                <input name="tipo_recuento" type="radio" id="tipo_recuento_0" value="0" checked="CHECKED">
                                                <?= _("BORDA") ?></label>
                                              <br/>
                                            <label for="tipo_recuento_1" >
                                                <input type="radio" name="tipo_recuento" value="1" id="tipo_recuento_1">
                                                <?= _("DOWDALL") ?>
                                              </label>
                                    </div>

                                    <br />


                                    <label> <input type="radio" name="tipo" value="99" id="tipo_1" onClick="pon_opciones5()" />
                                       <?= _("Encuesta") ?></label>
                                          <div  id="tipo_encuesta"  class="caja_de_display_b" style="display:none" >
                                              <h5> <?= _("Tipo de Encuesta") ?></h5>
                                            <label>
                                            <input type="radio" name="tipo_encuesta" value="3" id="tipo_encuesta_2"  checked="CHECKED" onClick="pon_opciones6()"/>
                                            <?= _("Encuesta") ?></label>
                                            <br />

                                            <label>
                                                <input type="radio" name="tipo_encuesta" value="7" id="tipo_encuesta_3"  onClick="pon_opciones7()"/>
                                                <?= _("Encuesta") ?> <?= _("binaria") ?> (<?= _("SI, NO, BLANCO") ?>)</label>
                                                <br />

                                            <?php if($votoPonderado == true){ ?>
                                              <label>
                                                <input type="radio" name="tipo_encuesta" value="5" id="tip_encuestao_4" onClick="pon_opciones6()" />
                                                <?= _("Encuesta con poderación del voto según tipo votante") ?></label><br />
                                              <label>
                                                <input type="radio" name="tipo_encuesta" value="6" id="tipo_encuesta_5" onClick="pon_opciones7()" />
                                                <?= _("Encuesta con poderación del voto según tipo votante") ?>  <?= _("binaria") ?> (<?= _("SI, NO, BLANCO") ?>)</label><br />

                                          <?php } ?>
                                        </div>
                                  <br/>


                                    <label>
                                        <input type="radio" name="tipo" value="2" id="tipo_6" onClick="pon_opciones2()" />
                                        <?= _("VUT") ?></label><br />
                                    <label>


                                    <label>
                                        <input type="radio" name="tipo" value="4" id="tipo_7" onClick="pon_opciones4()" />
                                        <?= _("Debate") ?>
                                    </label><br />

                                <?php }
                                ?>

                            </div>
                        </div>

<!-- fin tipo de votación -->

<!-- tipos de votante -->

                        <div class="form-group row">
                            <label for="tipo_usuario_0" class=" col-md-3 control-label"> <?= _("Tipo de votante") ?> </label>
                            <div class=" col-md-9">


                                <?php
                                if ($row_tipo_votante == 1) {
                                    $chekeado1 = "checked=\"checked\" ";
                                } else if ($row_tipo_votante == 2) {
                                    $chekeado2 = "checked=\"checked\" ";
                                } else if ($row_tipo_votante == 3) {

                                    $chekeado3 = "checked=\"checked\" ";
                                } else if ($row_tipo_votante == 5) {

                                    $chekeado5 = "checked=\"checked\" ";
                                } else { //// si estamos creando indicamos por defecto la primera opcion
                                    $chekeado1 = "checked=\"checked\" ";
                                }
                                ?>

                                <label>
                                    <input name="tipo_usuario" type="radio" id="tipo_usuario_0" value="1" <?php
                                    if (isset($chekeado1)) {
                                        echo $chekeado1;
                                    }
                                    ?> />
                                    <?php echo $nombre_tipo_1; ?></label>

                                <br/>
                                <label>
                                    <input type="radio" name="tipo_usuario" value="2" id="tipo_usuario_1" <?php
                                    if (isset($chekeado2)) {
                                        echo $chekeado2;
                                    }
                                    ?>   />
                                    <?php echo $nombre_tipo_1; ?> y <?php echo $nombre_tipo_2; ?></label>

                                <br/>
                                <label>
                                    <input type="radio" name="tipo_usuario" value="3" id="tipo_usuario_2" <?php
                                    if (isset($chekeado3)) {
                                        echo $chekeado3;
                                    }
                                    ?> />
                                           <?= _("Todos los miembros del censo") ?>

                                    <br/>

                                                <!--<input type="radio" name="tipo_usuario" value="5" id="tipo_usuario_3" <?php
                                    if (isset($chekeado5)) {
                                        echo $chekeado5;
                                    }
                                    ?>  />
                                    Abierta (5) -->

                            </div>
                        </div>
<!-- fin tipos de votantes-->

                        <div class="form-group row">
                            <label for="tipo_usuario_0" class=" col-md-3 control-label"><?= _("Estado") ?></label>
                            <div class=" col-md-9">

                                <label>
                                    <?php
                                    if ($row_activa == "si") {
                                        $chekeado_estado1 = "checked=\"checked\" ";
                                    } else {
                                        $chekeado_estado2 = "checked=\"checked\" ";
                                    };
                                    ?>
                                    <br />
                                    <input name="estado" type="radio" id="estado_0" value="si" <?php
                                    if (isset($chekeado_estado1)) {
                                        echo $chekeado_estado1;
                                    }
                                    ?> />
                                    <?= _("Activado") ?></label>
                                <br />
                                <label>
                                    <input name="estado" type="radio" id="estado_1" value="no" <?php
                                    if (isset($chekeado_estado2)) {
                                        echo $chekeado_estado2;
                                    }
                                    ?>  />
                                    <?= _("Desactivado") ?></label>






                            </div>
                        </div>


                        <div class="form-group row" id="opciones" <?php if (isset($display_debate)) {  echo $display_debate;} ?> <?php if (isset($display_binario)) {  echo $display_binario;} ?>>


                            <label for="numero_opciones" class=" col-md-3 control-label"><?= _("Numero de opciones que se pueden votar") ?> </label>
                            <div class=" col-md-3">
                                    <input name="numero_opciones" type="number" class="form-control" id="numero_opciones"
                                    value="<?php if ($row_numero_opciones=="") { echo "0"; } else{ echo "$row_numero_opciones";} ?>" min="0" required
                                    oninvalid="this.setCustomValidity('<?= _("El numero de opciones es un dato requerido") ?>')"
                                    oninput="this.setCustomValidity('')"/>
                            </div>
                                    <div class=" col-md-6">
                                <span class="text-danger"><?= _("¡¡ATENCION! Si usa VUT o PRIMARIAS es imprescindible indicar un numero de opciones que se cogeran") ?> <br/><?= _("Solo para opcion encuesta, si no hay limite ponga un &quot;0&quot;") ?></span><br />
                              </div>
                        </div>


                        <div id="accion_opciones" <?php
                        if (isset($display_debate)) {
                            echo $display_debate;
                        }
                        ?>>


                            <div class="form-group row">
                                <label for="tipo_seg" class=" col-md-3 control-label"><?= _("Seguridad de control de voto") ?></label>
                                <div class=" col-md-9">


                                    <?php
                                    if ($acc == "modifika") {
                                        if ($row_seguridad == 1) {
                                            $chekeado21 = "checked=\"checked\" ";
                                        } else if ($row_seguridad == 2) {
                                            $chekeado22 = "checked=\"checked\" ";
                                        } else if ($row_seguridad == 3) {

                                            $chekeado23 = "checked=\"checked\" ";
                                        } else if ($row_seguridad == 4) {

                                            $chekeado24 = "checked=\"checked\" ";
                                        };
                                    } else {
                                        $chekeado21 = "checked=\"checked\" ";
                                    }
                                    ?>
                                    <div id="tipo_s_3"><input name="tipo_seg" type="radio" id="tipo_seg_3" value="1" <?php
                                        if (isset($chekeado21)) {
                                            echo $chekeado21;
                                        }
                                        ?>  />
                                        <?php if ($_SESSION['usuario_nivel'] <= 6) { ?>
                                      <label for="tipo_seg_3">  <?= _("Sin comprobación de voto ni interventores") ?> </label>
                                      <?php }else { ?>
                                        <label for="tipo_seg_3">  <?= _("Sin comprobación de voto") ?> </label>
                                      <?php }?>
                                      </div>
                                    <div  id="tipo_s_4"  <?php
                                    if (isset($display_seg)) {
                                        echo $display_seg;
                                    }
                                    ?>><input type="radio" name="tipo_seg" value="2" id="tipo_seg_4" <?php
                                          if (isset($chekeado22)) {
                                              echo $chekeado22;
                                          }
                                          ?>   />
                                            <?= _("Con comprobación de voto") ?>
                                    </div>
                        <?php if ($_SESSION['usuario_nivel'] <= 6) { ?>
                                    <div  id="tipo_s_5"><input type="radio" name="tipo_seg" value="3" id="tipo_seg_5" <?php
                                        if (isset($chekeado23)) {
                                            echo $chekeado23;
                                        }
                                        ?>  />
                                        <?= _("Con interventores") ?>
                                      </div>


                                    <div  id="tipo_s_6"  <?php
                                    if (isset($display_seg)) {
                                        echo $display_seg;
                                    }
                                    ?>><input type="radio" name="tipo_seg" value="4" id="tipo_seg_6" <?php
                                          if (isset($chekeado24)) {
                                              echo $chekeado24;
                                          }
                                          ?>  />
                                        <?= _("Con comprobación de voto e interventores") ?>  </div>
                              <?php } ?>
                              </div>
                            </div>


                            <?php if ($_SESSION['usuario_nivel'] == 0) { ?>
                                <div class="form-group row">
                                    <label for="numero_opciones" class=" col-md-3 control-label"><?= _("Permitir interventores especiales para incluir votos de urna") ?>

                                    </label>
                                    <div class=" col-md-2">


                                            <?php
                                            if ($row_interventor == "si") {
                                                $valor_chek = "checked=\"CHECKED\"";
                                            }
                                            ?>
                                            <input name="si_interventores" type="checkbox" value="permitir" <?php
                                            if (isset($valor_chek)) {
                                                echo $valor_chek;
                                            }
                                            ?>  >
                                                   <?= _("Permitir") ?>
                                                   <br/>
                                                   <span class="text-danger"><?= _("¡¡atención!!")?>, <?= _("permitirá que los interventores puedan meter votos en el sistema") ?></span>
                                        </div>
                                        <div class=" col-md-2">
                                            <?php
                                            if ($acc == "modifika") {
                                                $valor_inter = $row_interventores;
                                            } else {

                                                $valor_inter = 0;
                                            }
                                            ?>
                                            <input name="numero_interventores" type="number" class="form-control" id="numero_interventores" value="<?php
                                            if (isset($valor_inter)) {
                                                echo $valor_inter;
                                            }
                                            ?>" min="0"  />
                                          </div>
                                        <div class=" col-md-5"><?= _("Numero de interventores necesarios para incluir datos") ?>
                                      </div>



                                </div>

                            <?php } ?>


                            <?php if ($_SESSION['usuario_nivel'] == 0) { ?>
                      <!--      <div class="form-group row">
                                <label for="numero_opciones" class=" col-md-3 control-label"><?= _("Permitir encriptacion de voto") ?></label>
                                <div class=" col-md-2">


                                        <?php
                                        if ($row_encripta == "si") {
                                            $valor_chek2 = "checked=\"CHECKED\"";
                                        }
                                        ?>
                                        <input name="encripta" type="checkbox" value="si" <?php
                                        if (isset($valor_chek2)) {
                                            echo $valor_chek2;
                                        }
                                        ?>  >
                                               <?= _("Permitir") ?>


                                </div>
                                <div class=" col-md-7"></div>
                            </div>-->
                          <?php } ?>
                        </div>
                        <div class="form-group row">
                            <label for="resumen" class=" col-md-12 control-label"><?= _("Resumen") ?></label>
                            <div class=" col-md-12">


                                <textarea cols="80" id="resumen" name="resumen" rows="10"><?php echo $row_resumen; ?></textarea>

                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="texto" class=" col-md-12 control-label"><?= _("Texto") ?></label>
                            <div class=" col-md-12">


                                <textarea cols="80" id="texto" name="texto" rows="10"><?php echo $row_texto; ?></textarea>
                            </div>
                        </div>
  <div class="form-group row">
<div class=" col-md-12">
            <div class="separador"></div>
                                <?php if ($acc == "modifika") { ?>
                                    <input name="modifika_votacion" type=submit  class="btn btn-primary btn-block"  id="add_directorio" value="<?= _("MODIFICAR esta  votación") ?>" />
                                <?php } else { ?>
                                    <input name="add_votacion" type=submit class="btn btn-primary btn-block"  id="add_directorio" value="<?= _("CREAR una nueva votación") ?>" />
                                <?php } ?>
  </div>
    </div>

                    </form>
</div>
                    <!--Final-->

        <script src='assets/js/admin/admin_funciones.js' type='text/javascript' ></script>
        <!-- ckeditor -->
        <script type="text/javascript" src="assets/ckeditor/ckeditor.js"></script>

        <script>
         CKEDITOR.replace( 'resumen', {
          height: 250,
          extraPlugins: 'imgbrowse',
          filebrowserImageBrowseUrl: '/assets/ckeditor/plugins/imgbrowse/imgbrowse.html?imgroot=<?php echo $baseUrl; ?>',
        <?php if($_SESSION['usuario_nivel']<=6){     // nivel para poder añadir imagenes  ?>
          filebrowserImageUploadUrl: 'aux_vota.php?c=<?php echo encrypt_url('basicos_php/imageUpload/conec=OK',$clave_encriptacion) ?>',
          <?php } ?>
        //  filebrowserUploadUrl: "upload.php",

         });
        </script>
        <script>
         CKEDITOR.replace( 'texto', {
          height: 250,
          extraPlugins: 'imgbrowse',
          filebrowserImageBrowseUrl: '/assets/ckeditor/plugins/imgbrowse/imgbrowse.html?imgroot=<?php echo $baseUrl; ?>',
          <?php if($_SESSION['usuario_nivel']<=6){     // nivel para poder añadir imagenes  ?>
          filebrowserImageUploadUrl: 'aux_vota.php?c=<?php echo encrypt_url('basicos_php/imageUpload/conec=OK',$clave_encriptacion) ?>',
          <?php } ?>
         });
        </script>


<!-- /   fin ckeditor -->
<?php   if($es_municipal == false){ ?>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#provincia2').change(function () {
                    var id_provincia = $('#provincia2').val();

                    $('#municipio').load('aux_modal.php?c=<?php echo encrypt_url('basicos_php/genera_select/',$clave_encriptacion) ?>&id_provincia=' + id_provincia);
                    //$("#municipio").html(data);
                });
            });
        </script>


        <?php
        if ($acc == "modifika") { // cuando modificamos una votacion, cargar lasopciones de los municipios
            if ($row[25] != 0) {
                ?>
                <script type="text/javascript">
                    function loadPoblacion() {

                        $('#municipio').load('aux_modal.php?c=<?php echo encrypt_url('basicos_php/genera_select/id_municipio='.$row[25],$clave_encriptacion) ?>&id_provincia=<?php echo $row[1]; ?>');
                      //  $("#municipio").html(data);
                    }


                    $(document).ready(function () {
                        loadPoblacion();
                    });
                </script>

            <?php } else { ?>

                <?php if ($_SESSION['nivel_usu'] <= 2) { ?>
                    <script type="text/javascript">
                        function loadPoblacion() {
                            $('#municipio').load('aux_modal.php?c=<?php echo encrypt_url('basicos_php/genera_select/',$clave_encriptacion) ?>&id_provincia=1');
                        //    $("#municipio").html(data);
                        }


                        $(document).ready(function () {
                            loadPoblacion();
                        });
                    </script>

                <?php } else { ?>
                    <script type="text/javascript">
                        function loadPoblacion() {
                            $('#municipio').load('aux_modal.php?c=<?php echo encrypt_url('basicos_php/genera_select/',$clave_encriptacion) ?>&id_provincia=<?php echo $name; ?>');
                        //    $("#municipio").html(data);
                        }


                        $(document).ready(function () {
                            loadPoblacion();
                        });
                    </script>

                    <?php
                }
            }
            ?>


        <?php } else { // si se esta creando una nueva votación ?>
            <?php if ($_SESSION['nivel_usu'] <= 2) { ?>
                <script type="text/javascript">
                    function loadPoblacion() {

                        $('#municipio').load('aux_modal.php?c=<?php echo encrypt_url('basicos_php/genera_select/',$clave_encriptacion) ?>&id_provincia=1');
                    //    $("#municipio").html(data);
                    }


                    $(document).ready(function () {
                        loadPoblacion();
                    });
                </script>

            <?php } else { ?>
                <script type="text/javascript">
                    function loadPoblacion() {

                        $('#municipio').load('aux_modal.php?c=<?php echo encrypt_url('basicos_php/genera_select/',$clave_encriptacion) ?>&id_provincia=<?php echo $name; ?>');
                      //  $("#municipio").html(data);
                    }


                    $(document).ready(function () {
                        loadPoblacion();
                    });
                </script>

            <?php } ?>
        <?php } ?>
      <?php } ?>
<?php } ?>

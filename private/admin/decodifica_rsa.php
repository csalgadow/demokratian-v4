<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###                                                         ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que lista todos los votos una vez desencriptados con la clave privada de los codificadores
* @todo ver si hay que añadir ID  y voto de la tabla tbn19
*/

if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 3;
include('../private/inc_web/nivel_acceso.php');



$idvot = fn_filtro_numerico($con, $variables['idvot']);
$Pollname = md5($idvot);

////metemos el modulo de RSA
include('../private/modulos/phpseclib/Crypt/RSA.php');

//Function for decrypting with RSA
function rsa_decrypt($string, $private_key) {
    //Create an instance of the RSA cypher and load the key into it
    $cipher = new Crypt_RSA();
    $cipher->loadKey($private_key);
    //Set the encryption mode
    $cipher->setEncryptionMode(CRYPT_RSA_ENCRYPTION_PKCS1);

    $string = base64_decode($string);
    //Return the decrypted version
    return $cipher->decrypt($string);
}
?>

<div class="card-header"> <h1 class="card-title"><?= _("Lista de votos decodificados") ?></h1> </div>


<div class="card-body">
                    <!--Comiezo-->

                    <p><?= _("Puede ver una lista de todos los votos que hay encriptados una vez que estos se han desencriptado") ?></p>
                    <?php
                    $resulta = mysqli_query($con, "SELECT cadena FROM $tbn19  WHERE id_votacion='$Pollname' ");
                    if ($rowa = mysqli_fetch_array($resulta)) {
                        mysqli_field_seek($resulta, 0);
                        do {
                            $cadena = $rowa[0];


                            $result = mysqli_query($con, "SELECT clave_privada,id FROM $tbn21  WHERE id_votacion='$idvot' ORDER by orden DESC");
                            if ($row = mysqli_fetch_array($result)) {
                                mysqli_field_seek($result, 0);
                                do {
                                    $clave_privada = $row[0];
                                    $cadena = rsa_decrypt($cadena, $clave_privada);
                                } while ($row = mysqli_fetch_array($result));
                            }

                            echo $cadena;
                            // ver si hay que añadir ID  y voto de la tabla tbn19

                            echo "<br/>";
                            echo "<br/>";
                        } while ($rowa = mysqli_fetch_array($resulta));
                    }
                    ?>
</div>
                    <!--Final-->

<?php } ?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###                                                         ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo con las distintas opciones para gestionar los contenidos del blog, desde aquí se accede a las páginas de listar o crear contenidos por categoría o de forma global
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 9;
include('../private/inc_web/nivel_acceso.php');
if($_SESSION['admin_blog']==1){

?>
<div class="card-header"> <h1 class="card-title"><?= _("Panel de gestion de los contenidos del BLOG") ?></h1> </div>


<div class="card-body">

                    <!--Comiezo-->

                    <!--INCLUIR CONTENIDOS-->
                    <div class="row">
                        <div class="col-sm-3"><?= _("INCLUIR CONTENIDOS") ?></div>
                        <div class="col-sm-3">
                        </div>
                        <div class="col-sm-3">
                          <a href="admin.php?c=<?php echo encrypt_url('admin/blog_contenidos/idcab=1',$clave_encriptacion); ?>" class="btn btn-primary btn-block"><?= _("Incluir nuevos contenidos") ?></a>
                        </div>
                        <div class="col-sm-3">

                        </div>
                    </div>
                    <p>&nbsp;</p>

                    <!--CONTENIDOS BLOQUE TEXTO AZUL-->
                    <div class="row">
                        <div class="col-sm-3"><?= _("CONTENIDOS BLOQUE TEXTO AZUL") ?></div>
                        <div class="col-sm-3">
                            <a href="admin.php?c=<?php echo encrypt_url('admin/blog_contenidos_list/idcont=1',$clave_encriptacion); ?>" class="btn btn-primary btn-block"><?= _("Ver contenidos bloque texto fondo azul") ?></a>
                        </div>
                        <div class="col-sm-3">

                        </div>
                        <div class="col-sm-3">

                        </div>
                    </div>
                    <p>&nbsp;</p>

                    <!--CONTENIDOS BLOQUE TEXTO BLANCO-->
                    <div class="row">
                        <div class="col-sm-3"><?= _("CONTENIDOS BLOQUE TEXTO BLANCO") ?></div>
                            <div class="col-sm-3">
                                <a href="admin.php?c=<?php echo encrypt_url('admin/blog_contenidos_list/idcont=2',$clave_encriptacion); ?>" class="btn btn-primary btn-block"><?= _("Ver contenidos bloque texto fondo blanco") ?></a>
                            </div>
                        <div class="col-sm-3">


                        </div>
                        <div class="col-sm-3">


                        </div>
                    </div>
                    <p>&nbsp;</p>
                    <!--CONTENIDOS BLOQUE MARKETING-->
                    <div class="row">
                        <div class="col-sm-3"><?= _("CONTENIDOS BLOQUE MARKETING") ?></div>
                        <div class="col-sm-3">
                            <a href="admin.php?c=<?php echo encrypt_url('admin/blog_contenidos_list/idcont=3',$clave_encriptacion); ?>" class="btn btn-primary btn-block"><?= _("Ver contenidos bloque marketing") ?></a>
                        </div>
                        <div class="col-sm-3">


                        </div>
                        <div class="col-sm-3">


                        </div>
                    </div>
                    <!---->
                    <p>&nbsp;</p>
                    <!-- CONTENIDOS BLOQUE NOTICIAS-->
                    <div class="row">
                        <div class="col-sm-3"><?= _("CONTENIDOS BLOQUE NOTICIAS") ?></div>
                        <div class="col-sm-3">
                            <a href="admin.php?c=<?php echo encrypt_url('admin/blog_contenidos_list/idcont=4',$clave_encriptacion); ?>" class="btn btn-primary btn-block"><?= _("Ver contenidos bloque noticias") ?></a>
                        </div>
                        <div class="col-sm-3">


                        </div>
                        <div class="col-sm-3">


                        </div>
                    </div>
                    <!---->
                    <!--Final-->
                    <p>&nbsp;</p>
                    <!-- contenidos de pagina-->
                    <div class="row">
                        <div class="col-sm-3"><?= _("CONTENIDOS PAGINAS") ?></div>
                        <div class="col-sm-3">
                            <a href="admin.php?c=<?php echo encrypt_url('admin/blog_contenidos_list/idcont=5',$clave_encriptacion); ?>" class="btn btn-primary btn-block"><?= _("Ver contenidos otras paginas") ?></a>
                        </div>
                        <div class="col-sm-3">
                        </div>
                        <div class="col-sm-3">


                        </div>
                    </div>
                    <!---->
                    <!--Final-->

                    <!-- todos los contenidos-->
                    <div class="row">
                        <div class="col-sm-3"><?= _("CONTENIDOS") ?></div>
                        <div class="col-sm-3">

                        </div>
                        <div class="col-sm-3">
                        </div>
                        <div class="col-sm-3">
                            <a href="admin.php?c=<?php echo encrypt_url('admin/blog_contenidos_list/idcont=%',$clave_encriptacion); ?>" class="btn btn-primary btn-block"><?= _("Ver todos los contenidos") ?></a>
                        </div>
                    </div>
                    <!---->

</div>


<?php
    }
  }
 ?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo con el formulario de búsqueda de votantes para bloquearlos o desbloquearlos
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 1;
include('../private/inc_web/nivel_acceso.php');
?>

                    <!--Comiezo-->

                    <div class="card-header"> <h1 class="card-title"><?= _("BUSCAR  EN EL CENSO DE VOTANTES") ?></h1> </div>


    <div class="card-body">

                    <!---->

                    <form id="form1" name="form1" method="post" action="admin.php?c=<?php echo encrypt_url('admin/bloquear_censos_busq',$clave_encriptacion) ?>" class="well form-horizontal">

                        <div class="form-group row">
                            <label for="nombre" class="col-sm-3 control-label"><?= _("Estado") ?></label>

                            <div class="col-sm-9">

                                <label for="bloqueado_0" class="control-label">
                                    <input name="bloqueado" type="radio" id="bloqueado_0" value="si" checked="checked" />
                                    <?= _("bloqueado") ?></label>
                                <br />
                                <label for="bloqueado_1" class="control-label">
                                    <input type="radio" name="bloqueado" value="no" id="bloqueado_1" />
                                    <?= _("desbloqueado") ?></label>

                            </div></div>
                        <div class="form-group row">
                            <label for="nombre" class="col-sm-3 control-label"><?= _("Nombre") ?> </label>

                            <div class="col-sm-9">
                                <input type="text" name="nombre_usuario" id="nombre_usuario" class="form-control"/>

                            </div></div>
                        <div class="form-group row">
                            <label for="nombre" class="col-sm-3 control-label"><?= _("Correo electronico") ?>  </label>

                            <div class="col-sm-9">
                                <input type="text" name="correo_electronico" id="correo_electronico" class="form-control"/>
                            </div></div>
                        <div class="form-group row">
                            <label for="nombre" class="col-sm-3 control-label"><?= _("Nif") ?></label>

                            <div class="col-sm-4">

                                <input type="text" name="nif" id="nif" class="form-control"/>
                            </div></div>
                        <div class="form-group row">
                            <label for="nombre" class="col-sm-3 control-label"> <?= _("Tipo usuario") ?> </label>

                            <div class="col-sm-9">
                              <label for="tipo_usuario_3" class="control-label">
                                <input name="tipo_usuario" type="radio" id="tipo_usuario_3" checked="checked" />
                                <?= _("todos") ?></label> <br/>
                                <label for="tipo_usuario_0" class="control-label">
                                <input name="tipo_usuario" type="radio" id="tipo_usuario_0" value="1" />
                                <?= _("socios") ?> (1) </label> <br/>
                                <label for="tipo_usuario_1" class="control-label">
                                <input type="radio" name="tipo_usuario" value="2" id="tipo_usuario_1" />
                                <?= _("socios y simpatizantes verificados") ?> (2) </label> <br/>
                                <label for="tipo_usuario_4" class="control-label">
                                <input type="radio" name="tipo_usuario" value="3" id="tipo_usuario_4" />
                                <?= _("socios y simpatizantes") ?> (3)</label>


                            </div></div>
                        <?php if ($es_municipal == false) { ?>
                            <div class="form-group row">
                                <label for="nombre" class="col-sm-3 control-label"><?= _("Provincia") ?></label>

                                <div class="col-sm-9">

                                    <?php
                                    $lista1 = "";
                                    if ($_SESSION['nivel_usu'] == 2) {
                                        // listar para meter en una lista del cuestionario buscador


                                        $options = "select DISTINCT id, provincia from $tbn8  where especial=0 order by ID";
                                        $resulta = mysqli_query($con, $options) or die("error: " . mysqli_error());

                                        while ($listrows = mysqli_fetch_array($resulta)) {
                                            $id_pro = $listrows['id'];
                                            $name1 = $listrows['provincia'];
                                            /* if ($id_pro == $row[1]) {
                                              $check = "selected=\"selected\" ";
                                              } else {
                                              $check = "";
                                              } */
                                            $lista1 .= "<option value=\"$id_pro\"  > $name1</option>";
                                        }
                                        ?>
                                        <h3> <?= _("Escoja una Provincia") ?> </h3>
                                        <select name="provincia" class="form-control custom-select" id="provincia" >
                                            <?php echo "$lista1"; ?>
                                        </select>
                                        <?php
                                    } else if ($_SESSION['nivel_usu'] == 3) {

                                        $options = "select DISTINCT id, provincia from $tbn8  where id_ccaa=" . $_SESSION['id_ccaa_usu'] . "  order by ID";
                                        $resulta = mysqli_query($con, $options) or die("error: " . mysqli_error());

                                        while ($listrows = mysqli_fetch_array($resulta)) {
                                            $id_pro = $listrows['id'];
                                            $name1 = $listrows['provincia'];
//                                        if ($id_pro == $row[1]) {
//                                            $check = "selected=\"selected\" ";
//                                        } else {
//                                            $check = "";
//                                        }
                                            $lista1 .= "<option value=\"$id_pro\" > $name1</option>";
                                        }
                                        ?>
                                        <h3> <?= _("Escoja una Provincia") ?> </h3>
                                        <select name="provincia" class="form-control custom-select" id="provincia" >
                                            <?php echo "$lista1"; ?>
                                        </select>
                                        <?php
                                    } else {

                                        $result2 = mysqli_query($con, "SELECT id_provincia FROM $tbn5 where id_usuario=" . $_SESSION['ID']);
                                        $quants2 = mysqli_num_rows($result2);

                                        if ($quants2 != 0) {

                                            while ($listrows2 = mysqli_fetch_array($result2)) {

                                                $name2 = $listrows2['id_provincia'];
                                                $optiones = mysqli_query($con, "SELECT  provincia FROM $tbn8 where ID=$name2");
                                                $row_prov = mysqli_fetch_row($optiones);
                                                /* if ($acc == "modifika") {
                                                  if ($name2 == $row[1]) {
                                                  $check = "checked=\"checked\" ";
                                                  } else {
                                                  $check = "";
                                                  }
                                                  } else {
                                                  $check = "checked=\"checked\" ";
                                                  } */

                                                $lista1 .= "    <label><input  type=\"radio\" name=\"provincia\" value=\"$name2\"  id=\"provincia\" /> " . $row_prov[0] . "</label> <br/>";
                                            }
                                            echo "$lista1";
                                        } else {
                                            echo _("No tiene asignadas provincias, no podra crear votación");
                                        }
                                    }
                                    ?>
                                </div></div>
                        <?php } ?>
                        <!--Final-->
                        <input type="submit" name="buscar" id="buscar" value="<?= _("Buscar") ?>"  class="btn btn-primary btn-block"  />
                        <br/><br/>
                    </form>

</div>
<?php } ?>

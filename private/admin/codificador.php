<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que crear o modificar los codificadores
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 1;
include('../private/inc_web/nivel_acceso.php');


$row[0] = "";
$row[1] = "";
$row[2] = "";
$row[3] = "";
$texto1 = "";
$idvot = fn_filtro_numerico($con, $variables['idvot']);
$fecha = date("Y-m-d H:i:s");

if (isset($variables['id'])) {
    $id = fn_filtro_numerico($con, $variables['id']);
    $acc = fn_filtro_nodb($variables['acc']);
} else {
    $id = "";
    $acc = "";
}



if (ISSET($_POST["modifika_codificador"])) {

    $nombre = fn_filtro($con, $_POST['nombre']);
    $orden = fn_filtro($con, $_POST['orden']);

    $mail_expr = "/^[0-9a-z~!#$%&_-]([.]?[0-9a-z~!#$%&_-])*"
            . "@[0-9a-z~!#$%&_-]([.]?[0-9a-z~!#$%&_-])*$/";

    if (empty($_POST['correo'])) {
        $texto1 = "<div class=\"alert alert-danger\">" . _("¡¡¡ERROR!!!") . " <br/>" . _("La direccion de correo es un dato necesario") . "</div>";
    } elseif (!preg_match($mail_expr, $_POST['correo'])) {

        $texto1 = "<div class=\"alert alert-danger\">" . _("¡¡¡ERROR!!!") . " <br/>" . _("la direccion es erronea") . "</div>";
    } else {
        $correo = fn_filtro($con, $_POST['correo']);
        $nombre_usuario = $_SESSION['nombre_usu'];

        $sSQL = "UPDATE $tbn21 SET nombre=\"$nombre\",correo=\"$correo\" ,fecha_modif=\"$fecha\",  modif=\"$nombre_usuario\" ,  orden=\"$orden\"  WHERE id='$id'";

        mysqli_query($con, $sSQL) or die("Imposible modificar pagina");

        $texto1 = "<div class=\"alert alert-success\">" . _("Realizadas las Modificaciones") . " <br>" . _("Asi ha quedado el codificador") . " " . $nombre . " </div>";
    }
}

if (ISSET($_POST["add_codificador"])) {

    $nombre = fn_filtro($con, $_POST['nombre']);

    $orden = fn_filtro_numerico($con, $_POST['orden']);

    $mail_expr = "/^[0-9a-z~!#$%&_-]([.]?[0-9a-z~!#$%&_-])*"
            . "@[0-9a-z~!#$%&_-]([.]?[0-9a-z~!#$%&_-])*$/";

    if (empty($_POST['correo'])) {
        $texto1 = "<div class=\"alert alert-danger\">" . _("¡¡¡ERROR!!!") . " <br/>" . _("La direccion de correo es un dato necesario") . "</div>";
    } elseif (!preg_match($mail_expr, $_POST['correo'])) {

        $texto1 = "<div class=\"alert alert-danger\">" . _("¡¡¡ERROR!!!") . " <br/>" . _("la direccion es erronea") . "</div>";
    } else {
        $correo = fn_filtro($con, $_POST['correo']);
        $usuario = $_SESSION['ID'];
        $nivel_acceso = 0;
        $insql = "insert into $tbn21 (nombre,  correo,id_votacion,anadido, fecha_anadido,nivel_acceso,orden) values (  \"$nombre\",  \"$correo\", \"$idvot\", \"$usuario\", \"$fecha\", \"$nivel_acceso\", \"$orden\")";
        $inres = @mysqli_query($con, $insql) or die("<strong><font color=#FF0000 size=3>  Imposible añadir. Cambie los datos e intentelo de nuevo.</font></strong>");
        $texto1 = "<div class=\"alert alert-success\">" . _("Añadido codificador") . " <br/>" . $nombre . " " . _("con correo") . " " . $correo . "<br/> " . _("a la base de datos") . "</div> ";
    }
}
?>

                    <!--Comiezo-->
                    <?php
                    $result_vot = mysqli_query($con, "SELECT nombre_votacion,seguridad,interventor, interventores FROM $tbn1 where id=$idvot");
                    $row_vot = mysqli_fetch_row($result_vot);
                    ?>
                    <div class="card-header"> <h1 class="card-title"><?php if ($acc == "modifika") { ?>
                            <?php
                            $result = mysqli_query($con, "SELECT orden,nombre,correo FROM $tbn21 where id=$id");
                            $row = mysqli_fetch_row($result);
                            $orden = $row[0];
                            ?>
                            <?= _("MODIFICAR CODIFICADOR") ?>
                        <?php } else { ?>
                            <?= _("INCLUIR CODIFICADOR") ?> <?php } ?></h1> </div>


                    <div class="card-body">





                    <h5>   <?= _("En la votación") ?> :  "<?php echo $row_vot[0]; ?>"</h5>
                    <div class="alert alert-danger"><?= _("OJO, recuerde que cuantos más codificadores tenga, mas se ralentiza el proceso de votación y el servidor puede tener problemas de capacidad. No se recomienda más de 2 por votación.") ?></div>
                    <p><?php echo"$texto1"; ?></p>

                    <form action="<?php $_SERVER['PHP_SELF'] ?>" method=post   name="frmDatos" id="frmDatos"  class="well form-horizontal" >

                        <div class="form-group row">
                            <label for="nombre" class="col-sm-3 control-label"><?= _("Nombre y Apellidos") ?></label>

                            <div class="col-sm-9">

                                <input name="nombre" type="text"  id="nombre" value="<?php echo "$row[1]"; ?>"   class="form-control" placeholder="<?= _("Nombre") ?>" required autofocus data-validation-required-message="<?= _("El nombre  es un dato requerido") ?>" />
                            </div></div>

                        <div class="form-group row">
                            <label for="correo" class="col-sm-3 control-label"><?= _("Correo") ?></label>

                            <div class="col-sm-9">
                                <input name="correo" type="email"  id="correo" value="<?php echo "$row[2]"; ?>"   class="form-control" placeholder="<?= _("Correo electronico") ?>" required  data-validation-required-message="<?= _("Por favor, ponga un correo electronico") ?>"  />
                            </div></div>

                        <div class="form-group row">
                            <label for="orden" class="col-sm-3 control-label"><?= _("Orden") ?></label>

                            <div class="col-sm-2">
                                <?php
                                if ($acc == "") {

                                    $result = mysqli_query($con, "SELECT orden FROM $tbn21 where id_votacion=$idvot order by orden DESC ");
                                      if ($row = mysqli_fetch_array($result)) {
                                  //  $row = mysqli_fetch_row($result);
                                    $orden = $row[0] + 1;
                                  }else{
                                      $orden =1;
                                  }
                                }
                                ?>

                                <input name="orden" type="number" class="form-control" id="orden" value="<?php echo "$orden"; ?>" min="1" required />
                            </div>
                            <div class="col-sm-7">
                                <p class="text-danger"><?= _("Ojo, es importante que no existan numeros duplicados, si es asi tendra errores") ?></p>
                            </div>
                        </div>


                        <?php if ($acc == "modifika") { ?>
                            <input name="modifika_codificador" type=submit  class="btn btn-primary pull-right"  id="modifika_codificador" value="<?= _("ACTUALIZAR  codificador") ?>" />
                        <?php } else { ?>
                            <input name="add_codificador" type=submit class="btn btn-primary pull-right"  id="add_codificador" value="<?= _("AÑADIR codificador") ?>" />
                        <?php } ?>
                        <p>&nbsp;</p>
                    </form>

                    <!--Final-->
</div>
  <p>&nbsp;</p>
  <?php }
include('../private/admin/codificador_busq.php');
  ?>

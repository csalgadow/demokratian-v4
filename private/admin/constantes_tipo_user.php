<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###                                                         ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo para configurar las constantes de los usuarios ( modifica el archivo config-votantes.inc.php)
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 1;
include('../private/inc_web/nivel_acceso.php');


include("../private/basicos_php/modifika_config.php");

$file = "../private/config/config-votantes.inc.php";

if (ISSET($_POST["modifika_tipo_1"])) {
    $com_string = "valor_tipo_1 = ";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_tipo_2"])) {
    $com_string = "valor_tipo_2 = ";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_tipo_3"])) {
    $com_string = "valor_tipo_3 = ";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}

if (ISSET($_POST["modifika_nombre_votantes"])) {
    $com_string = "nombre_votantes = ";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}


include($file);
?>


                    <!--Comiezo-->
                    <div class="card-header"> <h1 class="card-title"><?= _("Constantes de configuración de los usuarios") ?> </h1> </div>


<div class="card-body">

                    <table width="100%" border="0"  class="table table-striped">

                        <tr>
                            <th colspan="4" scope="row"><h5><?= _("Valor de ponderación de los votantes según su tipo para la votación encuesta ponderada") ?> </h5></th>
                        </tr>
                        <?php if($votoPonderado == true){ ?>
                        <tr>
                            <th scope="row">&nbsp;</th>
                            <td>&nbsp;</td>
                            <td><p class="text-info"><?= _("Indique el valor entre") ?> 0.1 <?= _("y") ?> 1 </p>
                            <!--  <?= _("Use un") ?>   . (<?= _("punto") ?>) <?= _("como separador de los decimales") ?>-->
                            </td>
                            <td>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Usuario nivel más alto") ?> -> 1 | (<?php echo  $nombre_tipo_1 ; ?>)</th>
                            <td>&nbsp;</td>
                            <td><?php echo $valor_tipo_1; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="number" min="0.01" max="1" step="0.01" autofocus required class="form-control" id="direccion_general_a0"  value="<?php echo $valor_tipo_1; ?>" >
                                    <input name="valor" type="hidden" id="valor1" value="<?php echo $valor_tipo_1; ?>" >
                                    <input type="submit" name="modifika_tipo_1" id="modifika_tipo_1" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row"> <?= _("Usuario nivel medio") ?> -> 2 | (<?php echo  $nombre_tipo_2 ; ?>)</th>
                            <td>&nbsp;</td>
                            <td><?php echo $valor_tipo_2; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="number"  min="0.01" max="1" step="0.01"  autofocus required class="form-control" id="direccion_general_a1"  value="<?php echo $valor_tipo_2; ?>" >
                                    <input name="valor" type="hidden" id="valor2" value="<?php echo $valor_tipo_2; ?>" >
                                    <input type="submit" name="modifika_tipo_2" id="modifika_tipo_2" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Usuario nivel más bajo") ?> -> 3 | (<?php echo  $nombre_tipo_3 ; ?>)</th>
                            <td>&nbsp;</td>
                            <td><?php echo $valor_tipo_3; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general"  type="number" min="0.01" max="1" step="0.01"  autofocus required class="form-control" id="direccion_general_a2"  value="<?php echo $valor_tipo_3; ?>" >
                                    <input name="valor" type="hidden" id="valor3" value="<?php echo $valor_tipo_3; ?>" >
                                    <input type="submit" name="modifika_tipo_3" id="modifika_tipo_3" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>

                <?php } else {
                  ?>

                  <tr>
                      <td colspan="4" scope="row"><p class="text-info"><?= _("Tiene que activar la opción de ponderación del voto según el votante, en el menú de configuración de constantes, si quiere ponderar el voto.") ?> </p></td>
                  </tr>
                  <?php
                }?>

                        <tr>
                            <th colspan="4" scope="row"><h5><?= _("Opciones para nombrar a los tipos de usuarios") ?> </h5></th>
                        </tr>
                        <tr>
                            <th width="40%" scope="row">&nbsp;</th>
                            <td width="5%">&nbsp;</td>
                            <td width="35%">&nbsp;</td>
                            <td width="10%">&nbsp;</td>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Formula seleccionada") ?></th>
                            <td >&nbsp;</td>
                            <td >
                               <?php echo  $nombre_tipo_1 ; ?> , <?php echo  $nombre_tipo_2 ; ?> , <?php echo  $nombre_tipo_3 ; ?>
                            </td>
                            <td >
                                <form name="form1" method="post" action="">
                                    <input name="direccion_general" type="number" min="1" max="4" autofocus required class="form-control" id="direccion_general_a4"   value="<?php echo $nombre_votantes; ?>" >
                                    <input name="valor" type="hidden" id="valor9" value="<?php echo $nombre_votantes; ?>" >
                                    <input type="submit" name="modifika_nombre_votantes" id="modifika_nombre_votantes" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Opciones disponibles") ?></th>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <th scope="row">&nbsp;</th>
                            <td>&nbsp;</td>
                            <td><?php echo   _("Socio") ; ?> , <?php echo   _("Simpatizante verificado"); ?> , <?php echo  _("Simpatizante") ; ?></td>
                            <td>1 </td>
                        </tr>
                        <tr>
                            <th scope="row">&nbsp;</th>
                            <td>&nbsp;</td>
                            <td><?php echo   _("Afiliado") ; ?> , <?php echo   _("Simpatizante verificado"); ?> , <?php echo  _("Simpatizante") ; ?></td>
                            <td>2 </td>
                        </tr>
                        <tr>
                            <th scope="row">&nbsp;</th>
                            <td>&nbsp;</td>
                            <td><?php echo   _("Socio trabajador") ; ?> , <?php echo _("Socio consumidor"); ?> , <?php echo  _("Socio colaborador") ; ?></td>
                            <td> 3 </td>
                        </tr>
                        <tr>
                            <th scope="row">&nbsp;</th>
                            <td>&nbsp;</td>
                            <td><?php echo  _("Propietario") ; ?> , <?php echo  _("Simpatizante"); ?> , <?php echo _("No propietario") ; ?></td>
                            <td> 4 </td>
                        </tr>
                        <tr>
                            <th scope="row">&nbsp;</th>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>



                    </table>
</div>

                    <!--Final-->

<?php } ?>

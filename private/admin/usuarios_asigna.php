<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###                                                         ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que permite la asignación de provincias o de grupos de trabajo a los administradores, realiza el proceso mediante petición ajax al archivo basicos_php/usuarios_asigna_procesa.php
*/
//session_name($usuarios_sesion);
session_start();
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 2;
include('../private/inc_web/nivel_acceso.php');


$lista_sub = "";
$lista1 = "";

$id = fn_filtro_numerico($con, $variables['id']);
$result = mysqli_query($con, "SELECT ID, id_provincia, nombre_usuario, id_ccaa,nivel_usuario FROM $tbn9 where id=$id");
$row = mysqli_fetch_row($result);
?>
<!DOCTYPE html>
<html lang="es">
    <head>
      <?php include("temas/$tema_web/adminHead.php"); ?>
    </head>
    <body>

      <div class="modal-header">
              <h5 class="modal-title"><?= _("Usuario") ?> <?php echo $row[2]; ?><!---titulo--></h5>
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          </div>
          <div class="modal-body">
                  <!---texto --->

           <!-- /modal-header -->


                <div class="alert alert-warning"><?= _("El Usuario es") ?>
                    <?php
                    if ($row[4] == "3") {
                        echo " " . _("Administrador") . " " . ("CCAA");
                    } else if ($row[4] == "4") {
                        echo " " . _("Administrador") . " " . ("provincial");
                    } else if ($row[4] == "6") {
                        echo " " . _("Administrador") . " " . ("Grupos");
                        if ($es_municipal == false) {
                            echo _("estatales");
                        }
                    } else if ($row[4] == "5") {
                        echo " " . _("Administrador") . " " . ("grupos provinciales");
                    } else if ($row[4] == "7") {
                        echo " " . _("Administrador") . " " . ("grupos autonomicos");
                    }
                    ?> </div>

                <div id="success2"></div>

                <?php
                if ($row[4] == "4" or $row[4] == "5") {
                    $options = "select DISTINCT id, provincia from $tbn8 where especial=0 and id_ccaa=$row[3] order by ID";
                    $resulta = mysqli_query($con, $options) or die("error: " . mysqli_error($con));
                    $quantos = mysqli_num_rows($resulta);

                    if ($quantos != 1) {
                        ?>

                        <h3><?= _("Asignar o Actualizar provincias al usuario") ?> </h3><h3><?php echo $row[2]; ?></h3>

                        <!---->

                        <form  method="post" name="myForm" id="myForm">
                            <?php
                            while ($listrows = mysqli_fetch_array($resulta)) {
                                $id_pro = $listrows['id'];
                                $name1 = $listrows['provincia'];

                                $options_usu = "select  ID from $tbn5 where id_provincia=$id_pro and id_usuario=$id order by ID";
                                $result_cont = mysqli_query($con, $options_usu);
                                $quants = mysqli_num_rows($result_cont);
                                if ($quants != 0) {
                                    $chequed = "checked=\"checked\"";
                                } else {
                                    $chequed = "";
                                }

                                $lista1 .= "    <label><input type=\"checkbox\" name=\"myCheckboxes[]\" value=\"$id_pro\" id=\"myCheckboxes\" $chequed /> $name1</label> <br/>";
                            }
                            ?>

                            <?php echo "$lista1"; ?>




                            <input name="id" type="hidden" id="id" value="<?php echo $id; ?>">
                            <input name="tipo" type="hidden" id="tipo" value="provincias">
                            <br>
                            <input name="add_candidato" type=submit class="btn btn-primary pull-right" id="add_directorio" value="<?= _("Asigna provincias") ?>" onclick="submitForm()">                  </td>


                        </form>  <p>&nbsp;</p>
                        <?php
                    }else{

                    }
                }

////////////// fin del listado de provincias
                ?>
                <!---->
                <p>&nbsp;</p>

                <?php
                if ($row[4] == "6") {
                    //3 es el tyipo estatal
                    $options_sub = "select DISTINCT id, subgrupo from $tbn4 where  tipo=3 order by ID";
                } else if ($row[4] == "7") {

                    //grupo de trabajo autonomicos y provincial
                    $options_sub = "select DISTINCT id, subgrupo, id_provincia,id_ccaa,tipo from $tbn4 where  id_ccaa=$row[3] order by ID";
                } else {

                    //tipo 1 son los provinciales
                    $options_sub = "select DISTINCT id, subgrupo, id_provincia,tipo from $tbn4 where  id_ccaa=$row[3] and tipo=1 order by ID";
                }
                $resulta_sub = mysqli_query($con, $options_sub) or die("error: " . mysqli_error($con));
                $quantos_gr = mysqli_num_rows($resulta_sub);

                while ($listrows_sub = mysqli_fetch_array($resulta_sub)) {
                    $id_sub = $listrows_sub['id'];
                    $name_sub = $listrows_sub['subgrupo'];
                    if (isset($listrows_sub['id_provincia'])) {
                        $id_prov = $listrows_sub['id_provincia'];
                    }
                    if (isset($listrows_sub['id_ccaa'])) {
                        $id_ccaa = $listrows_sub['id_ccaa'];
                    }
                    if (isset($listrows_sub['tipo'])) {
                        $tipo = $listrows_sub['tipo'];
                        if ($tipo == 2) {
                            $tipos = _("Autonomico");
                            // $optiones=mysqli_query("SELECT  ccaa FROM $tbn3 where ID=$id_ccaa",$con);
                            // $row_prov=mysqli_fetch_row($optiones);
                        } else if ($tipo == 1) {
                            $tipos = _("Provincia de") . ": ";

                            $optiones = mysqli_query($con, "SELECT  provincia FROM $tbn8 where ID=$id_prov");
                            $row_prov = mysqli_fetch_row($optiones);
                        }
                    }

                    $options_usu = "select  ID from $tbn6 where  id_grupo_trabajo=$id_sub and id_usuario=$id and admin=1 order by ID";
                    $result_cont = mysqli_query($con, $options_usu);
                    $quants = mysqli_num_rows($result_cont);
                    if ($quants != 0) {
                        $chequed = "checked=\"checked\"";
                    } else {
                        $chequed = "";
                    }


                    $lista_sub .= "    <label><input type=\"checkbox\" name=\"myCheckboxes2[]\" value=\" " . $id_sub . " \" id=\"myCheckboxes2\" " . $chequed . " />" . $name_sub;
                    if (isset($tipos)) {
                        $lista_sub .= "   - " . $tipos;
                    }
                    if (isset($row_prov[0])) {
                        $lista_sub .= $row_prov[0];
                    }
                    $lista_sub .= "</label> <br/>";
                }
                ?>
                <?php
                if ($quantos_gr != 0) {
                    ?>

                    <h3><?= _("Asignar o actualizar grupos de trabajo al usuario") ?> </h3><h3><?php echo $row[2]; ?></h3>
                    <form action="" method="post" name="myForm2" id="myForm2">


                        <?php echo "$lista_sub"; ?>


                        <input name="id" type="hidden" id="id" value="<?php echo $id; ?>">
                        <input name="tipo2" type="hidden" id="tipo2" value="grupos">
                        <input name="add_sub" type=submit  id="add_directorio" class="btn btn-primary pull-right" value="<?= _("Asigna GRUPO DE TRABAJO O ASAMBLEA") ?>" onclick="submitForm2()"/>                  </td>


                    </form>
                    <p>&nbsp;</p>

                    <?php
                } else {?>
                      <div class="alert alert-info">
                        <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                        <i class="fa fa-times simple-remove" aria-hidden="true"></i>
                        </button>
                        <span>
                          <b> Info - </b> <?=_("¡No hay grupos de trabajo o asambleas que se puedan asignar a este usuario!");?></span>
                        </div>
                        <?php

                }
                ?>
                <!---->


            </div>





            <script
              src="https://code.jquery.com/jquery-3.5.1.min.js"
              integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
              crossorigin="anonymous" ></script>
            <script>
              window.jQuery || document.write('<script src="assets/jquery-3.5.1/jquery.min.js"><\/script>')
            </script>

            <script type="text/javascript">
            $(document).ready(function () {
                $("#myForm").bind("submit",function(){
                    // Capturamnos el boton de envío
                  //  var btnEnviar = $("#btnEnviar");
                    $.ajax({
                        type: "POST",
                        //url: $(this).attr("action"),
                        url: "aux_modal.php?c=<?php echo encrypt_url('basicos_php/usuarios_asigna_procesa',$clave_encriptacion) ?>",
                        data:$(this).serialize(),
                        cache: false,
                        beforeSend: function(){
                            /*
                            * Esta función se ejecuta durante el envió de la petición al
                            * servidor.
                            * */
                            // btnEnviar.text("Enviando"); Para button
                            //btnEnviar.val("Enviando"); // Para input de tipo button
                            //btnEnviar.attr("disabled","disabled");
                        },
                        complete:function(data){
                            /*
                            * Se ejecuta al termino de la petición
                            * */
                            //btnEnviar.val("Enviar formulario");
                            //btnEnviar.removeAttr("disabled");
                        },
                        success: function(data){
                            /*
                            * Se ejecuta cuando termina la petición y esta ha sido
                            * correcta
                            * */
                            var result = data.trim().split("##");
                            if (result[0] == 'OK') {

                                $('#success2').html(result[1]);
                                $('#success2').show();

                            } else if (result[0] == 'ERROR') {

                                $("#success2").show();
                                $('#success2').html(result[1]);

                            } else{

                              $("#success2").show();
                              $('#success2').html('<div class="alert alert-danger">¡error!: '+data+'</div>');

                            }
                        },
                        error: function(data){
                            /*
                            * Se ejecuta si la peticón ha sido erronea
                            * */
                            $('#success2').html("<div class='alert alert-danger'>");
                            $('#success2 > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                    .append(" " +contrasenaForm+ " , hay un error</button>");
                        }
                    });
                    // Nos permite cancelar el envio del formulario
                    return false;
                });
            });

            </script>


            <script type="text/javascript">
            $(document).ready(function () {
                $("#myForm2").bind("submit",function(){
                    // Capturamnos el boton de envío
                  //  var btnEnviar = $("#btnEnviar");
                    $.ajax({
                        type: "POST",
                        //url: $(this).attr("action"),
                        url: "aux_modal.php?c=<?php echo encrypt_url('basicos_php/usuarios_asigna_procesa',$clave_encriptacion) ?>",
                        data:$(this).serialize(),
                        cache: false,
                        beforeSend: function(){
                            /*
                            * Esta función se ejecuta durante el envió de la petición al
                            * servidor.
                            * */
                            // btnEnviar.text("Enviando"); Para button
                            //btnEnviar.val("Enviando"); // Para input de tipo button
                            //btnEnviar.attr("disabled","disabled");
                        },
                        complete:function(data){
                            /*
                            * Se ejecuta al termino de la petición
                            * */
                            //btnEnviar.val("Enviar formulario");
                            //btnEnviar.removeAttr("disabled");
                        },
                        success: function(data){
                            /*
                            * Se ejecuta cuando termina la petición y esta ha sido
                            * correcta
                            * */
                            var result = data.trim().split("##");
                            if (result[0] == 'OK') {

                                $('#success2').html(result[1]);
                                $('#success2').show();

                            } else if (result[0] == 'ERROR') {

                                $("#success2").show();
                                $('#success2').html(result[1]);

                            } else{

                              $("#success2").show();
                              $('#success2').html('<div class="alert alert-danger">¡error!:'+data+'</div>');

                            }
                        },
                        error: function(data){
                            /*
                            * Se ejecuta si la peticón ha sido erronea
                            * */
                            $('#success2').html("<div class='alert alert-danger'>");
                            $('#success2 > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                    .append(" " +contrasenaForm+ " , hay un error</button>");
                        }
                    });
                    // Nos permite cancelar el envio del formulario
                    return false;
                });
            });

            </script>

        <!--
       ===========================  fin texto ayuda
        -->
      </div>            <!-- /modal-body -->
    <!-- /modal-footer -->
</div>         <!-- /modal-content -->

<?php } ?>
</body>
</html>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###                                                         ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que permite generar nuevos administradores con sus características, o quitarle los permisos
*/
include('../private/config/config-correo.inc.php');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 2;
include('../private/inc_web/nivel_acceso.php');



//require_once('../private/modulos/PHPMailer/PHPMailerAutoload.php');

require_once '../private/modulos/PHPMailer/src/PHPMailer.php';
require_once '../private/modulos/PHPMailer/src/SMTP.php';
require_once '../private/modulos/PHPMailer/src/Exception.php';
//include("../private/modulos/PHPMailer/class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded
$id = fn_filtro_numerico($con, $variables['id']);
if (ISSET($_POST["add_candidato"])) {


    $tipo_usuario = fn_filtro($con, $_POST['tipo_usuario']);
    $nivel_acceso = fn_filtro($con, $_POST['nivel_acceso']);

    $datos_nombre = fn_filtro($con, $_POST['datos_nombre']);
    $id_provincia = fn_filtro_numerico($con, $_POST['id_provincia']);
    $usuario_blog = fn_filtro_numerico($con, $_POST['usuario_blog']);

    if ($_POST['tipo_usuario'] == 1) {
        $nivel_acceso = 10;
    }

////miramos la provincia y cogemos el codigo de ccaa
    if ($_POST['tipo_usuario'] >= 5 && $_POST['nivel_acceso'] <= 5) {
        $inmsg1 = "<div class=\"alert alert-danger\">" . _("Error, este tipo de usuario no puede tener este nivel de acceso. No se han realizado los cambios") . "</div>";
    } else {

        $sSQL = "UPDATE $tbn9 SET  nivel_usuario=\"$tipo_usuario\"  ,nivel_acceso=\"$nivel_acceso\", admin_blog=\"$usuario_blog\"   WHERE id='$id'";

        mysqli_query($con, $sSQL) or die("Imposible modificar pagina");


        $inmsg1 = "<div class=\"alert alert-success\">" . _("Modificado") . " " . $datos_nombre . " " . ("en la base de datos") . "</div>";

        if ($tipo_usuario == 4 or $tipo_usuario == 5) {

            $result2 = mysqli_query($con, "SELECT ID,admin FROM $tbn5 where id_usuario=$id and id_provincia=$id_provincia");
            $quants2 = mysqli_num_rows($result2);
            $row2 = mysqli_fetch_row($result2);

            if ($quants2 == 0) {
                if ($tipo_usuario == 4) {
                    $admin = 1;
                } else {
                    $admin = 2;
                }

                $insql_prov = "insert into $tbn5 (id_usuario,id_provincia,admin ) values (  \"$id\", \"$id_provincia\", \"$admin\")";
                $inres = @mysqli_query($con, $insql_prov) or die("<strong><font color=#FF0000 size=3>  Imposible añadir. Cambie los datos e intentelo de nuevo.</font></strong>");

                $enlace_asigna = '<a href="javascript:void(0);" data-href="aux_modal.php?c='. encrypt_url('admin/usuarios_asigna/id='.$id,$clave_encriptacion).'" class="openmodalAdmin btn btn-secondary btn-block">'.
                        _("¡¡¡ATENCION!!!") . ' ' . _("Se le ha asignado de forma predeterminada al usuario la provincia a la que pertenece") .  ' <br/> ' .
                        _("Si desea asignarle mas provincias de su comunidad autonoma o grupos de trabajo, puede hacerlo ahora") . '</a>';
            } else {
                if ($tipo_usuario == 4 and $row2[1] == 2) {
                    $sSQL2 = "UPDATE $tbn5 SET  admin='1' WHERE id='$row2[0]'"; //no funciona
                    mysqli_query($con, $sSQL2) or die("Imposible modificar pagina");
                } else if ($tipo_usuario == 5 and $row2[1] == 1) {
                    $adm = 2;
                    $sSQL2 = "UPDATE $tbn5 SET  admin='2' WHERE id='$row2[0]'"; //no funciona
                    mysqli_query($con, $sSQL2) or die("Imposible modificar pagina");
                }
            }
            $enlace_asigna = '  <a href="javascript:void(0);" data-href="aux_modal.php?c='. encrypt_url('admin/usuarios_asigna/id='.$id,$clave_encriptacion).'" class="openmodalAdmin btn btn-secondary btn-block">' . _("¡¡¡ATENCION!!!") . '<br/>' . _('Si desea asignarle mas provincias de su comunidad autonoma o grupos de trabajo, puede hacerlo ahora') . '</a>';
        } else {

        }
        if ($tipo_usuario == 6 or $tipo_usuario == 7) {
            $enlace_asigna = '<a href="javascript:void(0);" data-href="aux_modal.php?c='. encrypt_url('admin/usuarios_asigna/id='.$id,$clave_encriptacion).'" class="openmodalAdmin btn btn-secondary btn-blok">' . _("¡¡¡ATENCION!!!") . '<br/>'  . _('Si desea asignarle grupos de trabajo, puede hacerlo ahora') . '</a></div>';
        }

        if (isset($_POST['notificar'])) {
            if ($_POST['notificar'] == "notificar") {
                $correo_notificar = fn_filtro_nodb($_POST['correo_notificar']);
                if ($tipo_usuario == 2) {
                    $dato_nivel_correo = _("Administrador") . " <br/> " . _("Tenga en cuenta la responsabilidad que implica y sea cuidadoso al hacer las modificaciones") . ". <br/> " . _("Muchas gracias");
                } else if ($tipo_usuario == 3) {
                    $dato_nivel_correo = _("Administrador") . " " . _("CCAA") . " <br/> " . _("Tenga en cuenta la responsabilidad que implica y sea cuidadoso al hacer las modificaciones") . ". <br/> " . _("Muchas gracias");
                } else if ($tipo_usuario == 4) {
                    $dato_nivel_correo = _("Administrador") . " " . _("provincia") . " <br/> " . _("Tenga en cuenta la responsabilidad que implica y sea cuidadoso al hacer las modificaciones") . ". <br/> " . _("Muchas gracias");
                } else if ($tipo_usuario == 5) {
                    $dato_nivel_correo = _("Administrador") . " " . _("Asamblea local o grupo de trabajo provincial") . " <br/> " . _("Tenga en cuenta la responsabilidad que implica y sea cuidadoso al hacer las modificaciones") . ". <br/> " . _("Muchas gracias");
                } else if ($tipo_usuario == 6) {
                    $dato_nivel_correo = _("Administrador") . " " . _("grupo trabajo Estatal") . " <br/> " . _("Tenga en cuenta la responsabilidad que implica y sea cuidadoso al hacer las modificaciones") . ". <br/> " . _("Muchas gracias");
                } else {
                    $dato_nivel_correo = _("Votante");
                }




/////////// Comienzo del envio de correo
                $mensaje = _("Este mensaje fue enviado por") .  ' " ' . $_SESSION['nombre_usu'] . ' "  <br/>';

                $mensaje .= _("El") . " : " . date('d/m/Y', time());

                $mensaje .= '<br />' . _("Mensaje") . ': <br />' .
                        _("Hola") . ' ' . $datos_nombre . '<br /> ' .
                        _("Su nivel de usuario en el sistema de votaciones de") .' : " ' . $nombre_eq . ' "  '. _("ha sido modificado") . ". <br/>" .
                        _("Su nivel de acceso ahora es de") . ':  '. $dato_nivel_correo . '<br/><br/>'
                         .  _("Un saludo") .
                        '<br/>';


              //  $mensaje = str_replace("\n", "<br>", $mensaje);
              //  $mensaje = str_replace("\t", "    ", $mensaje);

                if ($correo_smtp == true) {  //comienzo envio smtp
                    $asunto = _("Cambio en el centro de votaciones");

                    $mail = new PHPMailer();
                    $mail->ContentType = 'text/plain';
                    //$mail->IsHTML(false);
                    if ($mail_IsHTML == true) {
                        $mail->IsHTML(true);
                    } else {
                        $mail->IsHTML(false);
                    }

                    if ($mail_sendmail == true) {
                        $mail->IsSendMail();
                    } else {
                        $mail->IsSMTP();
                    }
                    // $mail->SMTPAuth = true;
                    if ($mail_SMTPAuth == true) {
                        $mail->SMTPAuth = true;
                    } else {
                        $mail->SMTPAuth = false;
                    }
                    if ($mail_SMTPSecure == false) {
                        $mail->SMTPSecure = false;
                        $mail->SMTPAutoTLS = false;
                    } else if ($mail_SMTPSecure == "SSL") {
                        $mail->SMTPSecure = 'ssl';
                    } else {
                        $mail->SMTPSecure = 'tls';
                    }
                    if ($mail_SMTPOptions == true) {  //algunos servidores con certificados incorrectos no envian los correos por SMTP por lo que quitamos la validadcion de los certificados, NO SE RECOMIENDA EN ABSOLUTO usar esta opción
                        $mail->SMTPOptions = array(
                            'ssl' => array(
                                'verify_peer' => false,
                                'verify_peer_name' => false,
                                'allow_self_signed' => true
                            )
                        );
                    }


                    $mail->Port = $puerto_mail; // Puerto a utilizar
                    $mail->Host = $host_smtp;
                    $mail->SetFrom($email_env, $nombre_web);
                    $mail->Subject = $asunto;
                    $mail->MsgHTML($mensaje);
                    $mail->AddAddress($correo_notificar);
                    $mail->Username = $user_mail;
                    $mail->Password = $pass_mail;

                    if (!$mail->Send()) {
                        echo _("Error en el envio") . " " . $mail->ErrorInfo;
                    } else {
                        $inmsg = "<div class=\"alert alert-success\"> " . _("Ha sido enviado un correo para notificarle al usuario su cambio de nivel administrativo") . "</div>";
                    }
                }// fin envio por stmp

                if ($correo_smtp == false) { ///correo mediante mail de php
                    //para el envío en formato HTML
                    $headers = "MIME-Version: 1.0\r\n";
                    $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

                    //dirección del remitente
                    $headers .= "From: $from <$email_env>\r\n";
                    //ruta del mensaje desde origen a destino
                    $headers .= "Return-path: $email_env\r\n";

                    $asunto = _("Cambio en el centro de votaciones");
                    mail($correo_notificar, $asunto, $mensaje, $headers);
                }


                //$inmsg = "<div class=\"alert alert-success\"> " . _("Ha sido enviado un correo para notificarle al usuario su cambio de nivel administrativo") . "</div>";
            } else {
                $inmsg = "<div class=\"alert alert-success\"> " . _("No se ha notificado por correo") . "</div>";
            }
        }
    }
}
?>



                    <!--Comiezo-->
                    <div class="card-header"> <h1 class="card-title"><?= _("MODIFICAR NIVELES ACCESO Y ADMINISTRACIÓN") ?></h1> </div>


                    <div class="card-body">

  <p><a href="admin.php?c=<?php echo encrypt_url('admin/usuarios_gestion/s=SD',$clave_encriptacion) ?>" class="btn btn-secondary"><?= _("volver") ?></a></p>
                    <?php
                    if (isset($inmsg)) {
                        echo $inmsg;
                    }
                    ?>
                    <?php
                    if (isset($inmsg1)) {
                        echo $inmsg1;
                    }
                    ?>
                    <?php
                    if (isset($enlace_asigna)) {
                        echo $enlace_asigna;
                    }
                    ?>

                    <!---->
                    <?php
                    $result = mysqli_query($con, "SELECT ID, nivel_usuario , nivel_acceso,nombre_usuario ,	apellido_usuario, id_provincia, usuario, 	nif , 	correo_usuario, admin_blog  FROM $tbn9 where id=$id");
                    $row = mysqli_fetch_row($result);
                    ?>

                <!--  <p><a href="usuarios_busq.php" >volver</a></p>-->
                    <p>&nbsp; </p>
                    <form action="<?php $_SERVER['PHP_SELF'] ?>" method=post name="frmDatos" id="frmDatos"  class="well form-horizontal">


                        <h4><?= _("Va a modificar el perfil de") ?> : <strong> <?php echo $row[3]; ?> <?php echo $row[4]; ?> </strong>,
                          <?= _("con nombre de usuario") ?> :<strong> <?php echo $row[6]; ?> </strong>
                        <?php  if ($es_municipal == false){ ?>
                            <?= _("de la provincia") ?>  <?php echo $row[5]; ?>
                          <?php } ?>
                            <?= _("y con NIF") ?> ": <?php echo $row[7]; ?> "</h4>
                            <p>&nbsp;</p>
                            <div class="form-group row">
                                <div  class="col-sm-3">
                                <label for="notificar" class="control-label"><?= _("Notificar por correo") ?></label>

                                    <input name="correo_notificar" type="hidden" id="correo_notificar" value="<?php echo $row[8]; ?>" /></p>
                                    </div>
                                <div class="col-sm-5">
                                <input name="notificar" type="checkbox" id="notificar" value="notificar" /> <p> <?= _("Enviar a") ?>: <?php echo $row[8]; ?> </p>

                                <input name="notificar_correo" type="hidden" id="notificar_correo" value="<?php echo $row[8]; ?>" />
                                <input name="datos_nombre" type="hidden" id="datos_nombre" value=" <?php echo $row[3]; ?> <?php echo $row[4]; ?>" />
                              </div>
                              <div class="col-sm-4">
                                <p><span class="text-info card card-body"><?= _("¿quiere enviarle un correo a su direccion de correo electronico notificandole los cambios?") ?> </span>
                              </div>
                            </div>

                        <p>&nbsp;</p>
                        <div class="form-group row">
                            <div  class="col-sm-3"><p><?= _("Tipo") ?></p>

                                  </div>
                              <div class="col-sm-5">
                                    <?php
                                    if ($row[1] == 1) {
                                        $chekeado1 = "checked=\"checked\" ";
                                    } else if ($row[1] == 2) {
                                        $chekeado2 = "checked=\"checked\" ";
                                    } else if ($row[1] == 3) {
                                        $chekeado3 = "checked=\"checked\" ";
                                    } else if ($row[1] == 4) {
                                        $chekeado4 = "checked=\"checked\" ";
                                    } else if ($row[1] == 5) {
                                        $chekeado5 = "checked=\"checked\" ";
                                    } else if ($row[1] == 6) {
                                        $chekeado6 = "checked=\"checked\" ";
                                    } else if ($row[1] == 7) {
                                        $chekeado7 = "checked=\"checked\" ";
                                    }
                                    ;
                                    ?>
                                    <?php
                                    if ($_SESSION['nivel_usu'] == 2 && $_SESSION['usuario_nivel'] == 0) {
                                        ?>
                                        <label for="tipo_usuario_1">
                                            <input type="radio" name="tipo_usuario" value="2" id="tipo_usuario_1" <?php
                                            if (isset($chekeado2)) {
                                                echo $chekeado2;
                                            }
                                            ?>  />
                                            <?= _("Administrador") ?></label> <br/>

                                        <?php
                                    } else if ($row[1] == 2) {
                                        $bloquea = true;
                                        ?>

                                        <?= _("Administrador") ?>, <?= _("no tiene permisos para modificarlo") ?>
                                        <input name="tipo_usuario" type="hidden" id="tipo_usuario" value="<?php echo $row[1]; ?>" /><br/>

                                        <?php
                                    } else {

                                    }
                                    $bloquea = "";
                                    if ($bloquea == true) {

                                    } else {
                                        $bloquea = "";
                                        if ($es_municipal == false) {
                                            ?>
                                            <label for="tipo_usuario_2">
                                            <input type="radio" name="tipo_usuario" value="3" id="tipo_usuario_2" <?php
                                            if (isset($chekeado3)) {
                                                echo $chekeado3;
                                            }
                                            ?> />
                                            <?= _("Administrador") ?>  <?= _("CCAA") ?> </label><br/>

                                            <label for="tipo_usuario_3">
                                            <input type="radio" name="tipo_usuario" value="4" id="tipo_usuario_3" <?php
                                            if (isset($chekeado4)) {
                                                echo $chekeado4;
                                            }
                                            ?> />
                                                   <?= _("Administrador") ?>  <?= _("provincia") ?></label>
                                            <br/>
                                        <?php } ?>
                                        <label for="tipo_usuario_5">
                                        <input type="radio" name="tipo_usuario" value="6" id="tipo_usuario_5" <?php
                                        if (isset($chekeado6)) {
                                            echo $chekeado6;
                                        }
                                        ?> />
                                        <?= _("Administrador") ?>  <?= _("grupo trabajo Estatal") ?><span class="text-warning"> **</span>
                                      </label>
                                        <br/>

                                        <?php if ($es_municipal == false) { ?>
                                          <label for="tipo_usuario_7">
                                            <input type="radio" name="tipo_usuario" value="7" id="tipo_usuario_7" <?php
                                            if (isset($chekeado7)) {
                                                echo $chekeado7;
                                            }
                                            ?> />
                                            <?= _("Administrador") ?>  <?= _("grupo trabajo CCAA") ?> <span class="text-warning"> **</span> </label><br/>

                                            <label for="tipo_usuario_4">
                                            <input type="radio" name="tipo_usuario" value="5" id="tipo_usuario_4" <?php
                                            if (isset($chekeado5)) {
                                                echo $chekeado5;
                                            }
                                            ?>  />
                                            <?= _("Administrador") ?>  <?= _("Asamblea local o grupo de trabajo provincial") ?> <span class="text-warning">**</span></label><br/>
                                        <?php } ?>

                                          <label for="tipo_usuario_0">
                                            <input name="tipo_usuario" type="radio" id="tipo_usuario_0" value="1" <?php
                                            if (isset($chekeado1)) {
                                                echo $chekeado1;
                                            }
                                            ?>  />
                                             <?= _("Votante") ?></label>
                                            <br/> <?php } ?>
                                            </div>
                                            <div class="col-sm-4"><p class="text-info card card-body"> <?= _("El administrador del grupo superior también puede trabajar con las demarcaciones o grupos inferiores") ?></p>
                                            <span class="text-warning">** <?= _("Nivel de acceso maximo") ?> (6)</label></div>
</div>
<div class="form-group row">
<div class="col-sm-3">
                            <?= _("Nivel de acceso") ?>
</div>
<div class="col-sm-5">
                            <?php
                                    switch ($row[2]) {
                                        case 0:
                                            $chekea0 = "selected=\"selected\" ";
                                            break;
                                        case 1:
                                            $chekea1 = "selected=\"selected\" ";
                                            break;
                                        case 2:
                                            $chekea2 = "selected=\"selected\" ";
                                            break;
                                        case 3:
                                            $chekea3 = "selected=\"selected\" ";
                                            break;
                                        case 4:
                                            $chekea4 = "selected=\"selected\" ";
                                            break;
                                        case 5:
                                            $chekea5 = "selected=\"selected\" ";
                                            break;
                                        case 6:
                                            $chekea6 = "selected=\"selected\" ";
                                            break;
                                        case 7:
                                            $chekea7 = "selected=\"selected\" ";
                                            break;
                                        case 8:
                                            $chekea8 = "selected=\"selected\" ";
                                            break;
                                        case 9:
                                            $chekea9 = "selected=\"selected\" ";
                                            break;
                                    }
                                    ?>
                                    <p>
                                        <label for="nivel_acceso"></label>
                                        <select name="nivel_acceso" id="nivel_acceso" class="buttons custom-select">
                                            <?php if ($_SESSION['usuario_nivel'] != 0 && $row[2] == 0) { ?>
                                                <option value="0" <?php
                                                if (isset($chekea0)) {
                                                    echo $chekea0;
                                                }
                                                ?>> <?= _("No tiene permiso para modificar el nivel de este usuario") ?></option>
                                                    <?php } else { ?>

                                                <option value="9" <?php
                                                if (isset($chekea9)) {
                                                    echo $chekea9;
                                                }
                                                ?>>9</option>
                                                <option value="8" <?php
                                                if (isset($chekea8)) {
                                                    echo $chekea8;
                                                }
                                                ?>>8</option>
                                                <option value="7" <?php
                                                if (isset($chekea7)) {
                                                    echo $chekea7;
                                                }
                                                ?>>7</option>
                                                <option value="6" <?php
                                                if (isset($chekea6)) {
                                                    echo $chekea6;
                                                }
                                                ?>>6</option>
                                                <option value="5" <?php
                                                if (isset($chekea5)) {
                                                    echo $chekea5;
                                                }
                                                ?>>5</option>
                                                <option value="4" <?php
                                                if (isset($chekea4)) {
                                                    echo $chekea4;
                                                }
                                                ?>>4</option>
                                                <option value="3" <?php
                                                if (isset($chekea3)) {
                                                    echo $chekea3;
                                                }
                                                ?>>3</option>
                                                <option value="2" <?php
                                                if (isset($chekea2)) {
                                                    echo $chekea2;
                                                }
                                                ?>>2</option>

                                                <?php
                                                if ($_SESSION['usuario_nivel'] != 0) {

                                                } else {
                                                    ?>
                                                    <option value="1" <?php
                                                    if (isset($chekea1)) {
                                                        echo $chekea1;
                                                    }
                                                    ?>>1</option>
                                                    <option value="0" <?php
                                                    if (isset($chekea0)) {
                                                        echo $chekea0;
                                                    }
                                                    ?>>0</option><?php
                                                        }
                                                    }
                                                    ?>
                                        </select>
                                        <?php
                                        if ($es_municipal == false) {
                                            echo '<p class="text-info">' . _("Tiene que ser como minimo administrador provincial para tener nivel inferior a 5") . '</p>';
                                        } else {
                                            echo '<p class="text-info">' . _("Tiene que ser Administrador para tener un nivel inferior a 5") . '</p>';
                                        }
                                        ?>
                                      </div>

                                      <div class="col-sm-4">
                                        <span class="text-info card card-body">
                                    <p><strong><?= _("Los niveles o numero inferior, también heredan los permisos de los niveles superiores") ?>. </strong></p>
                                    <p>6 - <?= _("Nivel mínimo para poder añadir imagen e incluir interventores") ?>. </p>
                                    <p>5 - <?= _("Permite ver la participación") ?>.</p>
                                    <p>4 - <?= _("Permite crear asambleas o grupos de trabajo") ?>.</p>
                                    <p>3 - <?= _("Permite incluir codificadores RSA") ?>.</p>
                                    <p>2 - <?= _("Permite crear administradores locales y el borrado de votaciones, asambleas, candidatos, interventores, gestión de administradores") ?>.</p>
                                    <p>1 - <?= _("Permite modificar censos, incluir, borrar, bloquear, etc. Ver que votaciones tiene cada usuario") ?>.</p>
                                    <p>0 - <?= _("Todos los permisos, incluido crear administradores generales, acceso general a la configuración de la aplicación, descarga de la base de datos, borrado de codificadores RSA, etc") ?>.</p></span>
                                  </div>
                                </div>
                                <?php
                                   if ($_SESSION['nivel_usu'] == 2 && $_SESSION['usuario_nivel'] == 0) {
                                       ?>
                                   <div class="form-group row">
                                   <div class="col-sm-3">
                                                               <?= _("Administrador del blog") ?>
                                   </div>
                                   <div class="col-sm-5">
                                     <?php
                                     if ($row[9] == 1) {
                                         $chekeado_blog1 = "checked=\"checked\" ";
                                     } else  {
                                         $chekeado_blog2 = "checked=\"checked\" ";
                                     }

                                    ?>

                                         <label for="usuario_blog_1">
                                             <input type="radio" name="usuario_blog" value="1" id="usuario_blog_1" <?php
                                            if (isset($chekeado_blog1)) {
                                                 echo $chekeado_blog1;
                                             }
                                             ?>  />
                                             <?= _("Si") ?></label> <br/>
                                             <label for="tipo_usuario_2">
                                                 <input type="radio" name="usuario_blog" value="0" id="usuario_blog_2" <?php
                                                 if (isset($chekeado_blog2)) {
                                                     echo $chekeado_blog2;
                                                 }
                                                 ?>  />
                                                 <?= _("No") ?></label> <br/>
                                   </div>


                                   <div class="col-sm-4">
                                        <span class="text-info card card-body">
                                    <p><strong><?= _("Administrar blog exterior") ?>. </strong></p>
                                    <p>- <?= _("Tendrá acceso a todas las herramientas del blog") ?>. </p>
                                    </span>
                                      </div>
                                    <?php } ?>
                                    <p>&nbsp;</p>
                                    <p>&nbsp;</p>

                                        <input name="add_candidato" type=submit  class="btn btn-primary btn-block" id="add_directorio" value="<?= _("Actualizar") ?>">

                                        <input name="id_provincia" type="hidden" id="id_provincia" value="<?php echo $row[5]; ?>" />

                                    <p>&nbsp;</p>
                                    <p>&nbsp;</p>

                    </form>



</div>
                    <!--Final-->


<?php } ?>

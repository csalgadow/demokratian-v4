<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###                                                         ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que lista los votantes del censo de una votación
* Este archivo se usa en varias páginas
*/


if ($var_carga == true) {
    $contar = 1;

    $result_vot = mysqli_query($con, "SELECT id_provincia, activa,nombre_votacion,tipo_votante, id_grupo_trabajo, demarcacion,id_ccaa,id_municipio 	 FROM $tbn1 where id=$idvot");
    $row_vot = mysqli_fetch_row($result_vot);

    $id_provincia = $row_vot[0];
    $id_ccaa = $row_vot[6];
    $activa = $row_vot[1];
    $tipo_votante = $row_vot[3];
    $id_grupo_trabajo = $row_vot[4];
    $id_municipio = $row_vot[7];

    if ($row_vot[5] == 1) {
        if (isset($id_nprov)) {
            $id_provincia = fn_filtro($con, $id_nprov);
        } else {
            if ($es_municipal == true) {  //incluimos que si es municipal la provincia es 001
                $id_provincia = 001;
            } else {
                $id_provincia = "";
            }
        }

        $sql = "SELECT ID, 	nombre_usuario , correo_usuario,tipo_votante, apellido_usuario FROM $tbn9  WHERE id_provincia='$id_provincia' and  tipo_votante <='$tipo_votante' ";
    } else if ($row_vot[5] == 2) {
        $sql = "SELECT ID, 	nombre_usuario , correo_usuario,tipo_votante, apellido_usuario FROM $tbn9  WHERE id_ccaa='$id_ccaa' and tipo_votante <='$tipo_votante' ";
    } else if ($row_vot[5] == 3) {
        $sql = "SELECT ID, 	nombre_usuario , correo_usuario,tipo_votante, apellido_usuario FROM $tbn9  WHERE id_provincia='$id_provincia' and tipo_votante <='$tipo_votante' ";
    } else if ($row_vot[5] == 7) {
        $sql = "SELECT ID, 	nombre_usuario , correo_usuario,tipo_votante, apellido_usuario FROM $tbn9  WHERE id_municipio='$id_municipio' and tipo_votante <='$tipo_votante' ";
    } else {
        ////////////comprobar si funciona bien
        $sql = "SELECT a.ID, a.nombre_usuario , a.correo_usuario,a.tipo_votante, a.apellido_usuario FROM $tbn9 a,$tbn6 b  WHERE (a.ID= b.id_usuario) and id_grupo_trabajo='$id_grupo_trabajo' and a.tipo_votante <='$tipo_votante' ";
    }





    $result = mysqli_query($con, $sql);
    ?>

    <h2><?= _("Votación")?> &quot;<?php echo "$row_vot[2]" ?>&quot;</h2>

    <h3>  <?= _("Listado del censo") ?> </h3>
        <?php if ($es_municipal == false) { ?>
            <?php if ($row_vot[5] == 1 and isset($id_nprov)) { ?>
              <h4>  <?= _("en la provincia") ?> <strong> <?php echo $id_nprov; ?> </strong>   </h4>   <h4> <?= _("y tipo de votación") ?>
        <?php } else if ($row_vot[5] == 2) { ?>
                <h4>   <?= _("en la comunidad autonoma") ?> <strong> <?php echo $row_vot[6]; ?>  </h4>   <h4>  </strong> <?= _("y tipo de votación") ?>
        <?php } else if ($row_vot[5] == 3) { ?>
            <h4>   <?= _("en la provincia") ?> <strong> <?php echo $row_vot[0]; ?> </strong>  </h4>   <h4>  <?= _("y tipo de votación") ?>
            <?php
        }
    } else {
        ?> <h3><?= _("Tipo de votación") ?>  <?php } ?>

        <?php
        if ($row_vot[3] == 1) {
            echo _("solo para socios");
        } else if ($row_vot[3] == 2) {
            echo _("solo pata socios y simpatizantes");
        } else if ($row_vot[3] == 3) {
            echo _("abierta");
        }
        ?>   </h4>



    <?php
    if ($row = mysqli_fetch_array($result)) {
        ?>

            <table id="tabla1" class="table table-striped table-bordered dt-responsive nowrap" data-page-length="25">
                <thead>
                    <tr>
                        <th width="5%">&nbsp;</th>
                        <th width="20%"><?= _("NOMBRE") ?></th>
                        <th width="30%"><?= _("APELLIDO") ?></th>
                        <th width="30%"><?= _("CORREO") ?></th>
                        <th width="5%"><?= _("TIPO") ?></th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    mysqli_field_seek($result, 0);
                    do {
                        ?>
                        <tr>
                            <td><?php echo "$contar" ?></td>
                            <td><?php echo "$row[1]" ?></td>
                            <td><?php echo "$row[4]" ?></td>
                            <td><?php echo "$row[2]" ?> </td>
                            <td><?php
                                if ($row[3] == 1) {
                                    echo _("socio");
                                } else if ($row[3] == 2) {
                                    echo _("simpatizante verificado");
                                } else if ($row[3] == 3) {
                                    echo _("simpatizante");
                                } else if ($row[3] == 5) {
                                    echo _("Nada");
                                }
                                ?></td>
                        </tr>
                        <?php
                        $contar = $contar + 1;
                    } while ($row = mysqli_fetch_array($result));
                    ?>


                </tbody>
            </table>


        <?php

    } else {
        if ($es_municipal == false) {
            if ($id_provincia == "") {
                echo '<div class="alert alert-info">' . _("Escoja la provincia para la que quiere ver el censo") . '</div>';
            } else {
                echo  '<div class="alert alert-info">' . _("¡No se ha encontrado votantes para esta votación!") . '</div>' ;
            }
        }else{
          echo  '<div class="alert alert-info">' . _("¡No se ha encontrado votantes para esta votación!") . '</div>' ;
        }
    }
} else {
    echo _("error acceso");
}

?>

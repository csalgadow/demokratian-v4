<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###                                                         ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que lista todas las poblaciones y su código por provincias por si no se conocen los códigos de algún sitio a la hora de incluir votante
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 5;
include('../private/inc_web/nivel_acceso.php');

$lista = "";

?>
<div class="card-header"> <h1 class="card-title"><?= _("consultar poblaciones") ?></h1> </div>


<div class="card-body">
<?= _("seleccione una provincia para ver los codigos de los municipios");?>
<div class="row">
  <div class="col-sm-6">
                    <?php
                    $options = "select DISTINCT id, provincia from $tbn8  order by ID";
                    $resulta = mysqli_query($con, $options) or die("error: " . mysqli_error($con));

                    while ($listrows = mysqli_fetch_array($resulta)) {
                        $id_pro = $listrows['id'];
                        $name1 = $listrows['provincia'];
                        $lista .= "<option value=\"$id_pro\" > $name1</option>";
                    }
                    ?>

                    <select name="provincia_2" class="form-control custom-select"  id="provincia_2" >
                        <?php echo "$lista"; ?>
                    </select>
                  </div>
<div class="col-sm-6">
                    <div id="municipio">  </div>
</div>
</div>


            <!--Final-->
        <?php   if($es_municipal == false){ ?>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#provincia_2').change(function () {

                    var id_provincia = $('#provincia_2').val();

                    $('#municipio').load('aux_modal.php?c=<?php echo encrypt_url('basicos_php/poblaciones_genera_select/',$clave_encriptacion) ?>&id_provincia=' + id_provincia);
                  //  $("#municipio").html(data);

                });
            });
        </script>
      <?php } ?>
</div>
<?php } ?>

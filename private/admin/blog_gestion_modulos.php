<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###                                                         ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que permite gestionar los distintos módulos del blog, en qué posición van , etc
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 9;
include('../private/inc_web/nivel_acceso.php');
if($_SESSION['admin_blog']==1){
$idcab = fn_filtro_numerico($con, $variables['idcab']);

$result = mysqli_query($con, "SELECT bloque FROM $tbn35 where ID=$idcab");
$row = mysqli_fetch_row($result);
$orden = $row[0];

$array_orden = explode(",", $orden);
//$longitud = count($array_orden);

?>
<script
  src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
  integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
  crossorigin="anonymous"></script>
  <!-- Template Main CSS File -->
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <div class="card-header"> <h1 class="card-title">
    <?php
    switch ($idcab) {
      case 1:
           echo _("Módulos de la cabecera de la página de inicio del blog");
          break;
      case 2:
          echo _("Módulos de la cabecera del blog");
          break;
      case 3:
          echo _("Módulos del cuerpo de la página de inicio");
          break;
        }
    ?>  </h1> </div>
    <div class="card-body">



                        <!--Comiezo-->
                        <p class="text-info"><?= _("Arrastre los módulos de la columna de la derecha a la izquierda y póngalos en la posición que quiera que se visualicen en el blog") ?></p>
                        <div id="success"> </div>

                            <div class="row">
                              <div class="column">
                                <h4><?= _("Modulos que podemos usar") ?></h4>
                                <ul class="connected-sortable droppable-area1">
                                  <?php
                                  $sql = "SELECT id,title, description,activo FROM $tbn35 where ID>10";
                                  $result = mysqli_query($con, $sql);

                                  if ($row = mysqli_fetch_array($result)) {
                                      mysqli_field_seek($result, 0);
                                        do {
                                          if (!in_array($row['id'], $array_orden)){
                                          ?>
                                          <li class="draggable-item draggable-left" id="blog_<?php echo $row['id'];?>">
                                          <p>  <?php if ($row['activo']==1){
                                            echo '<a role="button" class="" data-toggle="tooltip" data-html="true" title="" data-original-title="<em>'. _("Modulo activo") .' </em> ">
                                            <i class="fa fa-check-square-o text-success"></i>
                                            </a>';
                                            }else{
                                            echo '<a role="button" class="" data-toggle="tooltip" data-html="true" title="" data-original-title="<em>'. _("Modulo inactivo") .' </em> ">
                                            <i class=" fa fa-times-circle-o text-danger"></i>
                                            </a>';
                                            }
                                             ?>
                                            <?php echo $row['title']; ?></p>
                                            <p class="text-info "><small><?php echo $row['description']; ?></small></p>
                                          </li>
                                          <?php
                                          }
                                      } while ($row = mysqli_fetch_array($result));
                                    }
                                      ?>
                                </ul>
                              </div>

                              <div class="column">
                                <h4><?= _("Modulos en uso") ?></h4>
                                <ul class="connected-sortable droppable-area2" id="datoBlog">
                                  <?php
                                  $sql = "SELECT id,title, description,activo FROM $tbn35 where ID>10";
                                  $result = mysqli_query($con, $sql);
                                  if ($row = mysqli_fetch_array($result)) {
                                      mysqli_field_seek($result, 0);
                                        do {
                                          if (in_array($row['id'], $array_orden)){
                                          ?>
                                          <li class="draggable-item draggable-right" id="blog_<?php echo $row['id'];?>">
                                            <p>  <?php if ($row['activo']==1){
                                              echo '<a role="button" class="" data-toggle="tooltip" data-html="true" title="" data-original-title="<em>'. _("Modulo activo") .' </em> ">
                                              <i class="fa fa-check-square-o text-success"></i>
                                              </a>';
                                              }else{
                                              echo '<a role="button" class="" data-toggle="tooltip" data-html="true" title="" data-original-title="<em>'. _("Modulo inactivo") .' </em> ">
                                              <i class=" fa fa-times-circle-o text-danger"></i>
                                              </a>';
                                              }
                                               ?>
                                              <?php echo $row['title']; ?></p>
                                              <p class="text-info "><small><?php echo $row['description']; ?></small></p>
                                          </li>
                                          <?php
                                          }
                                      } while ($row = mysqli_fetch_array($result));
                                    }
                                      ?>
                                </ul>
                              </div>
                            </div>


                        <script type="text/javascript">
                          $( init );
                          function init() {
                            $( ".droppable-area1, .droppable-area2" ).sortable({
                                connectWith: ".connected-sortable",
                                stack: '.connected-sortable ul'
                              }).disableSelection();
                          }
                        </script>

                        <script type="text/javascript">
                        $(document).ready(function(){
                            $('#datoBlog').sortable({
                                revert: true,
                                opacity: 0.6,
                                cursor: 'move',
                                update: function() {
                                    var order = $('#datoBlog').sortable("serialize");
                                    $.post("aux_vota.php?c=<?php echo encrypt_url('basicos_php/blogUpdateModulos/idcab='.$idcab,$clave_encriptacion) ?>",
                                    order,
                                    function(data){
                                        $('#success').html(data).slideDown('slow').delay(3000).slideUp('slow');
                                    });
                                }
                            });
                        });
                        </script>
                        <!--Final-->

    </div>
              <?php
            }
          } ?>

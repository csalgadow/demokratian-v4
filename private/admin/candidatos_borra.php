<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que borra los candidatos u opciones que se pueden votar
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 2;
include('../private/inc_web/nivel_acceso.php');



$id = fn_filtro($con, $variables['id']);
$idvot = fn_filtro($con, $variables['idvot']);




$result = mysqli_query($con, "SELECT * FROM $tbn7 where id=$id");
$row = mysqli_fetch_row($result);

/////////////////  miramos si el candidato ya ha obtenido votos, si es asi no se podria borrar
$sql2 = "SELECT ID FROM $tbn10 WHERE id_candidato='$id'";
if ($resulta2 = mysqli_query($con, $sql2)){

  $inmsg = "<div class=\"alert alert-danger \"> " . _("Este candidato ya ha obtenido votos en la votación por lo que no puede ser borrado") . " </div>";
}else{

//////////////////// miramos si se puede borrar por no ser un candidato de una votación VUT y la borramos en ese caso
$sql3 = "SELECT tipo FROM $tbn1 WHERE ID='$idvot'";
$resulta3 = mysqli_query($con, $sql3) or die("error: " . mysqli_error());
while ($listrows3 = mysqli_fetch_array($resulta3)) {
    $tipo = $listrows3['tipo'];
}

if ($tipo != 2) {
    $borrado = mysqli_query($con, "DELETE FROM $tbn7 WHERE id=$id ") or die("No puedo ejecutar la instrucción de borrado SQL query");

    $inmsg = "<div class=\"alert alert-sucess \"> " . _("¡¡El registro ha sido borrado!!") . "</div>";
} else {

    $inmsg = "<div class=\"alert alert-danger \"> " . _("Este candidato esta en una votación tipo VUT y no puede ser borrado") . " </div>";
}
}
?>

<div class="card-header"> <h1 class="card-title"><?= _("BORRADO de OPCION O CANDIDATO") ?></h1> </div>


<div class="card-body">
                    <!--Comiezo-->
                    <a href="admin.php?c=<?php echo encrypt_url('admin/candidatos_busq1/idvot='.$idvot,$clave_encriptacion) ?>" class="btn btn-primary pull-right"><?= _("Buscar en el directorio para modificar o borrar candiatos en esta encuesta") ?></a>


                    <!---->

                    <?php echo "$inmsg"; ?>


                        <div class="form-group row">
                            <label for="nombre" class="col-sm-3 control-label"><?= _("Nombre") ?> </label>

                            <div class="col-sm-9">
                                <?php echo "$row[3]"; ?>

                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="nombre" class="col-sm-3 control-label"><?= _("Sexo") ?></label>

                            <div class="col-sm-9">

                                <?php
                                if ($row[4] == "H") {
                                    echo _("Neutro (sin opcion de sexo)");
                                } else if ($row[4] == "M") {
                                    echo _("Hombre");
                                } else {
                                    echo _("Mujer");
                                }
                                ?>

                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="nombre" class="col-sm-3 control-label"><?= _("Imagen") ?> </label>

                            <div class="col-sm-9">


                                <?php
                                if ($row[12] == "") {
                                    echo _("No tiene imagen asociada");
                                } else {
                                    ////si en la BBDD si hay fotos
                                    $thumb_photo_exists = $upload_cat . "/" . $row[12];

                                    if (file_exists($thumb_photo_exists)) {
                                        unlink($thumb_photo_exists);
                                    }
                                    echo _("Borrada imagen") . " : " . $row[12] . " ";
                                }
                                ?>


                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="nombre" class="col-sm-3 control-label"><?= _("Texto") ?></label>
                            <div class="col-sm-19">
                                <?php echo "$row[2]"; ?>


                            </div>
                        </div>


</div>
                    <!--Final-->
<?php } ?>

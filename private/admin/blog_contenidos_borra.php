<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que borra los contenidos del blog en todas las categorías
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 9;
include('../private/inc_web/nivel_acceso.php');
if($_SESSION['admin_blog']==1){

  if (isset($variables['idcont'])) {
      $idcont = fn_filtro_numerico($con, $variables['idcont']);
  }


  if (isset($variables['id'])){



      $id = fn_filtro_numerico($con, $variables['id']);
      $result = mysqli_query($con, "SELECT id_incluido,	titulo,	texto,	imagen,	id_categoria,	zona_pagina,	activo, fecha	 FROM $tbn33 where id=$id");
      $row = mysqli_fetch_row($result);

      $row_titulo = $row[1];
      $row_texto = $row[2];
      $row_imagen = $row[3];
      $row_id_categoria = $row[4];
      $row_zonapagina = $row[5];
      $row_activo = $row[6];
      $row_fecha = $row[7];
      $titulo_h1= _("CONTENIDO BORRADO");


      $borrado1 = mysqli_query($con, "DELETE FROM $tbn33 WHERE id=" . $id . "") ;
      if (!$borrado1) {
          $inmsg= "<div class=\"alert alert-danger\">" . _("Error en el borrado del contenido") . "</div>";
      } elseif (mysqli_affected_rows($con) == 0) {
          $inmsg= "<div class=\"alert alert-danger\">" . _("Error en el borrado del contenido") . "</div>";
      } else {
          $inmsg= "<div class=\"alert alert-success\">" . _("Borrado correctamente el contenido") . "</div>";

          $target_file_old = $upload_cat . "/" . $row_imagen;
          $ifunlink = unlink($target_file_old);
          if($ifunlink){
            $inmsg.= "<div class=\"alert alert-success\">" . _("Borrada correctamente la imagen").": " .$row_imagen. "</div>";
          }else{
            $inmsg.= "<div class=\"alert alert-danger\">" . _("No se ha borrado la imagen").": " .$row_imagen. "</div>";
          }

      }


  }

?>


<div class="card-header-votaciones "> <h1 class="card-title"><?php echo $titulo_h1; ?></h1> </div>

<div class="card-body">


                    <!--Comiezo-->

                <div class="row">
                  <div class="col-sm-8"></div>
                  <div class="col-sm-4">
                    <a href="admin.php?c=<?php echo encrypt_url('admin/blog_contenidos_list/idcont='.$idcont,$clave_encriptacion) ?>" class="btn btn-primary btn-block"><?= _("Volver al directorio de contenidos") ?></a>
                      <a href="admin.php?c=<?php echo encrypt_url('admin/blog_contenidos/idcont='.$idcont,$clave_encriptacion) ?>" class="btn btn-primary btn-block"><?= _("Añadir un contenido") ?></a>
                  </div>
                </div>


                    <p>&nbsp;</p>


                    <?php  // Mensajes del sistema si se ha subido o modidificado bien
                    if (isset($inmsg)) {
                        echo "$inmsg";
                    }
                    ?>

                      <div class="form-group row">
                          <label for="nombre" class="col-sm-3 control-label"><?= _("Titulo") ?> </label>

                          <div class="col-sm-9"><?php echo "$row_titulo"; ?>
                          </div>
                      </div>


                      <div class="form-group row">

                              <label for="texto"  class="col-sm-3 control-label" ><?= _("Texto") ?></label>

                              <div class="col-sm-9"><?php echo "$row_texto"; ?></div>
                      </div>


                      <div class="form-group row">
                          <label for="padre" class="col-sm-3 control-label"> <?= _("Categoria") ?> </label>

                          <div class="col-sm-4">
                            <?php
                              $lista1="";
                            $sql = "SELECT id, titulo FROM $tbn31 WHERE activo='1'  ORDER BY 'id' ";
                            $resulta =  mysqli_query($con, $sql);


                            while ($listrows = mysqli_fetch_array($resulta)) {
                                $id_pro = $listrows['id'];
                                $name1 = $listrows['titulo'];
                                 if ($id_pro == $row_id_categoria) {
                                    echo $name1;;
                                }
                            }
                            ?>


                          </div>
                        </div>



                        <div class="form-group row">
                            <div class="col-sm-3 "><?= _("Estado") ?></div>
                            <div class="col-sm-9">
                              <?php
                                  if ($row_activo == 1) {
                                    _("Activo");
                                  } else {
                                    _("Inactivo");
                                  }
                                ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="fecha_ini" class=" col-md-3 control-label"> <?= _("Fecha comienzo") ?></label>
                              <div class=" col-md-3">
                              <?php echo date("Y-m-d", strtotime($row_fecha));?>
                              </div>
                              <div class=" col-md-4">
                              </div>
                        </div>


                        <?php

                            if ($row_imagen != "") {
                                ?>

                                <div class="form-group">
                                    <label for="row_imagen" class="col-sm-3 control-label"><?= _("Imagen principal contenido") ?></label>

                                    <div class="col-sm-9"><?php echo $row_imagen; ?></div>
                                </div>
                                <?php
                            }
                        ?>

                        <!--Final-->
                </div>

              <?php }
              }
              ?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###                                                         ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo con el acceso al archivo que permite generar una nueva copia de seguridad (dump_db.php) y que lista todas las copias de seguridad que hay hechas y guardadas en el servidor
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 1;
include('../private/inc_web/nivel_acceso.php');
?>

<div class="card-header"> <h1 class="card-title"><?= _("Copias de la base de datos actual") ?></h1> </div>


<div class="card-body">

                  <?php if ($_SESSION['usuario_nivel'] == 0) { ?>
                    <a href="admin.php?c=<?php echo encrypt_url('admin/dump_db/s=SD',$clave_encriptacion); ?>" class="btn btn-primary btn-block"><?= _("Generar y descargar una nueva copia de seguridad") ?> </a>
                  <?php } ?>

                    <!--Comiezo-->



                    <?php


                        $MyDirectory = opendir($path_bakup_bbdd) or die('Error');
                        echo '<h2>'. _("Copias de la base de datos") .'</h2>';
                        echo '<div class="card card-body"> <ul>';
                        while ($Entry = @readdir($MyDirectory)) {
                            if ($Entry == '.' or $Entry == '..' or $Entry == 'index.html' )
                                continue;
                            echo '<li><a href="aux_modal.php?c='.encrypt_url('basicos_php/descargar/f='.$Entry,$clave_encriptacion).' ">' . $Entry . '</a></li>';
                        }
                        echo '</ul></div>';
                        closedir($MyDirectory);

                    ?>
                    <!--Final-->

</div>
              <?php } ?>

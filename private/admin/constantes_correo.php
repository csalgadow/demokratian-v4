<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###                                                         ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo para configurar el correo ( modifica el archivo config.inc.php)
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 1;
include('../private/inc_web/nivel_acceso.php');


include("../private/basicos_php/modifika_config.php");

$file = "../private/config/config-correo.inc.php";

if (ISSET($_POST["modifika_general"])) {
    $com_string = "email_env = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_error"])) {
    $com_string = "email_error = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_control"])) {
    $com_string = "email_control = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_tecnico"])) {
    $com_string = "email_error_tecnico = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_sistema"])) {
    $com_string = "email_sistema = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_nombre"])) {
    $com_string = "nombre_sistema = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_asunto"])) {
    $com_string = "asunto = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_mens_error"])) {
    $com_string = "asunto_mens_error = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}

if (ISSET($_POST["modifika_user_mail"])) {
    $com_string = "user_mail = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}

if (ISSET($_POST["modifika_host_smtp"])) {
    $com_string = "host_smtp = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}

if (ISSET($_POST["modifika_pass_mail"])) {
    $com_string = "pass_mail = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_puerto_mail"])) {
    $com_string = "puerto_mail = ";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_mail_sendmail"])) {
    $com_string = "mail_sendmail = ";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}

if (ISSET($_POST["modifika_SMTPSecure"])) {
    $com_string = "mail_SMTPSecure = ";
    if ($_POST['valor'] == "false") {
        $dato_viejo = fn_filtro_nodb($_POST['valor']);
        $nuevo_dato = "\"" . fn_filtro_nodb($_POST['direccion_general']) . "\"";
    }
    if ($_POST['valor'] == "SSL" or $_POST['valor'] == "TLS") {
        if ($_POST['direccion_general'] == "false") {
            $dato_viejo = "\"" . fn_filtro_nodb($_POST['valor']) . "\"";
            $nuevo_dato = fn_filtro_nodb($_POST['direccion_general']);
        } else {
            $dato_viejo = "\"" . fn_filtro_nodb($_POST['valor']) . "\"";
            $nuevo_dato = "\"" . fn_filtro_nodb($_POST['direccion_general']) . "\"";
        }
    }
    $find = $com_string . $dato_viejo;
    $replace = $com_string . $nuevo_dato;
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_mail_SMTPAuth"])) {
    $com_string = "mail_SMTPAuth = ";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_mail_IsHTML"])) {
    $com_string = "mail_IsHTML = ";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}

include($file);
?>


                    <!--Comiezo-->
                    <div class="card-header"> <h1 class="card-title"><?= _("Constantes de configuración del correo") ?> </h1> </div>


<div class="card-body">

                    <table width="100%" border="0"  class="table table-striped">

                        <tr>
                            <th colspan="4" scope="row"><h5><?= _("Configuración del servidor de correo") ?> </h5></th>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Tipo de envio de correo") ?></th>
                            <td>&nbsp;</td>
                            <td><?php
                                if ($correo_smtp = true) {
                                    echo " SMTP";
                                } else {
                                    echo "Mail";
                                }
                                ?>
                            </td>
                            <td>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Usuario de correo") ?> </th>
                            <td>&nbsp;</td>
                            <td><?php echo $user_mail; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="text"  autofocus required class="form-control" id="direccion_general_a0"  value="<?php echo $user_mail; ?>" >
                                    <input name="valor" type="hidden" id="valor1" value="<?php echo $user_mail; ?>" >
                                    <input type="submit" name="modifika_user_mail" id="modifika_user_mail" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row"> <?= _("Host del correo") ?></th>
                            <td>&nbsp;</td>
                            <td><?php echo $host_smtp; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="text"  autofocus required class="form-control" id="direccion_general_a1"  value="<?php echo $host_smtp; ?>" >
                                    <input name="valor" type="hidden" id="valor2" value="<?php echo $host_smtp; ?>" >
                                    <input type="submit" name="modifika_host_smtp" id="modifika_host_smtp" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Contraseña") ?></th>
                            <td>&nbsp;</td>
                            <td>*******</td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general"  type="password"  autofocus required class="form-control" id="direccion_general_a2"  value="<?php echo $pass_mail; ?>" >
                                    <input name="valor" type="hidden" id="valor3" value="<?php echo $pass_mail; ?>" >
                                    <input type="submit" name="modifika_pass_mail" id="modifika_pass_mail" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Puerto del servidor") ?></th>
                            <td>&nbsp;</td>
                            <td><p>&nbsp;</p>
                                <p><?php echo $puerto_mail; ?></p></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="text"  autofocus required class="form-control" id="direccion_general_a3"  placeholder="<?= _("Direccion de correo") ?>" value="<?php echo $puerto_mail; ?>" >
                                    <input name="valor" type="hidden" id="valor4" value="<?php echo $puerto_mail; ?>" >
                                    <input type="submit" name="modifika_puerto_mail" id="modifika_puerto_mail" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row"><span class="col-sm-4 control-label">SMTPSecure</span></th>
                            <td>&nbsp;</td>
                            <td><?php
                                if ($mail_SMTPSecure == false) {
                                    echo _("Deshabilitado");
                                    $check1 = "checked=\"CHECKED\"";
                                } else if ($mail_SMTPSecure == "TLS") {
                                    echo"TLS";
                                    $check2 = "checked=\"CHECKED\"";
                                } else if ($mail_SMTPSecure == "SSL") {
                                    echo"SSL";
                                    $check3 = "checked=\"CHECKED\"";
                                }
                                ?></td>
                            <td><form name="form1" method="post" action="">
                                    <label>
                                        <input name="direccion_general" type="radio" id="direccion_general_0" value="false" <?php
                                        if (isset($check1)) {
                                            echo $check1;
                                        }
                                        ?>>
                                        <?= _("Deshabilitado") ?></label>
                                    <label>
                                        <input name="direccion_general" type="radio" id="direccion_general_2" value="TLS" <?php
                                        if (isset($check2)) {
                                            echo $check2;
                                        }
                                        ?>>
                                        TLS</label>
                                    <label>
                                        <input name="direccion_general" type="radio"  id="direccion_general_1" value="SSL" <?php
                                        if (isset($check3)) {
                                            echo $check3;
                                        }
                                        ?>>
                                        SSL</label>

                                    <input name="valor" type="hidden" id="valor5" value="<?php
                                    if ($mail_SMTPSecure == false) {
                                        echo "false";
                                    } else if ($mail_SMTPSecure == "TLS") {
                                        echo "TLS";
                                    } else if ($mail_SMTPSecure == "SSL") {
                                        echo"SSL";
                                    }
                                    ?>" >
                                    <input type="submit" name="modifika_SMTPSecure" id="modifika_SMTPSecure" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row"><span class="col-sm-4 control-label"><?= _("Autentificación por SMTP") ?> (SMTPAuth)</span></th>
                            <td>&nbsp;</td>
                            <td><?php
                                if ($mail_SMTPAuth == true) {
                                    echo _("Habilitado");
                                    $check4 = "checked=\"CHECKED\"";
                                } else {
                                    echo _("Deshabilitado");
                                    $check5 = "checked=\"CHECKED\"";
                                }
                                ?></td>
                            <td><form name="form1" method="post" action="">
                                    <label>
                                        <input name="direccion_general" type="radio" id="direccion_general_9" value="true" <?php
                                        if (isset($check4)) {
                                            echo $check4;
                                        }
                                        ?>>
                                               <?= _("Habilitado") ?>
                                    </label>
                                    <label>
                                        <input type="radio" name="direccion_general" id="direccion_general_3" value="false" <?php
                                        if (isset($check5)) {
                                            echo $check5;
                                        }
                                        ?>>
                                               <?= _("Deshabilitado") ?>
                                    </label>
                                    <input name="valor" type="hidden" id="valor6" value="<?php
                                    if ($mail_SMTPAuth == true) {
                                        echo "true";
                                    } else {
                                        echo"false";
                                    }
                                    ?>" >
                                    <input type="submit" name="modifika_mail_SMTPAuth" id="modifika_mail_SMTPAuth" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row"><span class="col-sm-4 control-label"><?= _("Envio html o texto plano") ?>(IsHTML)</span></th>
                            <td>&nbsp;</td>
                            <td><?php
                                if ($mail_IsHTML == true) {
                                    echo _("Habilitado");
                                    $check6 = "checked=\"CHECKED\"";
                                } else {
                                    echo _("Deshabilitado");
                                    $check7 = "checked=\"CHECKED\"";
                                }
                                ?>
                            </td>
                            <td><form name="form1" method="post" action="">
                                    <label>
                                        <input name="direccion_general" type="radio" id="direccion_general_4" value="true" <?php
                                        if (isset($check6)) {
                                            echo $check6;
                                        }
                                        ?>>
                                        <?= _("Habilitado") ?></label>
                                    <label>
                                        <input type="radio" name="direccion_general" id="direccion_general_5" value="false" <?php
                                        if (isset($check7)) {
                                            echo $check7;
                                        }
                                        ?>>
                                        <?= _("Deshabilitado") ?></label>
                                    <input name="valor" type="hidden" id="valor7" value="<?php
                                    if ($mail_IsHTML == true) {
                                        echo "true";
                                    } else {
                                        echo"false";
                                    }
                                    ?>" >
                                    <input type="submit" name="modifika_mail_IsHTML" id="modifika_mail_IsHTML" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Forma de envio de correo") ?></th>
                            <td>&nbsp;</td>
                            <td><?php
                                if ($mail_sendmail == true) {
                                    echo "IsSendMail";
                                    $check8 = "checked=\"CHECKED\"";
                                } else {
                                    echo"IsSMTP";
                                    $check9 = "checked=\"CHECKED\"";
                                }
                                ?></td>
                            <td><form name="form1" method="post" action="">
                                    <label>
                                        <input name="direccion_general" type="radio" id="direccion_general_6" value="false" <?php
                                        if (isset($check9)) {
                                            echo $check9;
                                        }
                                        ?>>
                                        IsSMTP</label>
                                    <label>
                                        <input type="radio" name="direccion_general" id="direccion_general_7" value="true" <?php
                                        if (isset($check8)) {
                                            echo $check8;
                                        }
                                        ?>>
                                        IsSendMail</label>
                                    <input name="valor" type="hidden" id="valor8" value="<?php
                                    if ($mail_sendmail == true) {
                                        echo "true";
                                    } else {
                                        echo"false";
                                    }
                                    ?>" >
                                    <input type="submit" name="modifika_mail_sendmail" id="modifika_mail_sendmail" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                          <th width="39%" scope="row">&nbsp;</th>
                          <td width="5%">&nbsp;</td>
                          <td width="23%">&nbsp;</td>
                          <td width="33%">
                              <p><a href="javascript:void(0);" data-href="aux_modal.php?c=<?php echo encrypt_url('admin/comprueba_correo/s=SD',$clave_encriptacion) ?>" class="openmodalAdmin btn btn-success btn-block"><?= _("Comprobar configuración de correo") ?></a></p>
                              <br/><p class="text-warning text-right"><?= _("Se enviara a") ?>: <?php echo $email_env; ?> </p> </td>
                        </tr>
                        <tr>
                            <th colspan="4" scope="row"><h5><?= _("Otros datos del correo") ?> </h5></th>
                        </tr>
                        <tr>
                            <th width="39%" scope="row">&nbsp;</th>
                            <td width="5%">&nbsp;</td>
                            <td width="23%">&nbsp;</td>
                            <td width="33%">&nbsp;</td>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Direccion de correo general") ?></th>
                            <td width="5%">&nbsp;</td>
                            <td width="23%"><?php echo $email_env; ?></td>
                            <td width="33%">
                                <form name="form1" method="post" action="">
                                    <input name="direccion_general" type="email" autofocus required class="form-control" id="direccion_general_a4"  placeholder="<?= _("Direccion de correo") ?>" value="<?php echo $email_env; ?>" >
                                    <input name="valor" type="hidden" id="valor9" value="<?php echo $email_env; ?>" >
                                    <input type="submit" name="modifika_general" id="modifika_general" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Sitio al que enviamos los correos de los que tienen problemas y no estan en la bbdd, este correo es el usado si no hay datos en la bbdd de los contactos por provincias") ?></th>
                            <td>&nbsp;</td>
                            <td><?php echo $email_error; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="email" autofocus required class="form-control" id="direccion_general_a5"  placeholder="<?= _("Direccion de correo") ?>" value="<?php echo $email_error; ?>" >
                                    <input name="valor" type="hidden" id="valor10" value="<?php echo $email_error; ?>" >
                                    <input type="submit" name="modifika_error" id="modifika_error" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Direccion que envia el correo para el control con interventores") ?></th>
                            <td>&nbsp;</td>
                            <td><?php echo $email_control; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="email" autofocus required class="form-control" id="direccion_general_a6"  placeholder="<?= _("Direccion de correo") ?>" value="<?php echo $email_control; ?>" >
                                    <input name="valor" type="hidden" id="valor11" value="<?php echo $email_control; ?>" >
                                    <input type="submit" name="modifika_control" id="modifika_control" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Correo electronico del responsable tecnico") ?></th>
                            <td>&nbsp;</td>
                            <td><?php echo $email_error_tecnico; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="email" autofocus required class="form-control" id="direccion_general_a7"  placeholder="<?= _("Direccion de correo") ?>" value="<?php echo $email_error_tecnico; ?>" >
                                    <input name="valor" type="hidden" id="valor12" value="<?php echo $email_error_tecnico; ?>" >
                                    <input type="submit" name="modifika_tecnico" id="modifika_tecnico" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Correo electronico del sistema, demomento incluido en el envio de errores de la bbdd") ?></th>
                            <td>&nbsp;</td>
                            <td><?php echo $email_sistema; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="email" autofocus required class="form-control" id="direccion_general_a8"  placeholder="<?= _("Direccion de correo") ?>" value="<?php echo $email_sistema; ?>" >
                                    <input name="valor" type="hidden" id="valor13" value="<?php echo $email_sistema; ?>" >
                                    <input type="submit" name="modifika_sistema" id="modifika_sistema" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row">&nbsp;</th>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>

                        <tr>
                            <th scope="row"> <?= _("Nombre del sistema cuando se envia el correo de recupercion de clave") ?></th>
                            <td>&nbsp;</td>
                            <td><?php echo $nombre_sistema; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="text" autofocus required class="form-control" id="direccion_general_a9"  value="<?php echo $nombre_sistema; ?>" >
                                    <input name="valor" type="hidden" id="valor14" value="<?php echo $nombre_sistema; ?>" >
                                    <input type="submit" name="modifika_nombre" id="modifika_nombre" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Asunto del correo para recuperar la contraseña") ?></th>
                            <td>&nbsp;</td>
                            <td><?php echo $asunto; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="text" autofocus required class="form-control" id="direccion_general_10"  value="<?php echo $asunto; ?>" >
                                    <input name="valor" type="hidden" id="valor15" value="<?php echo $asunto; ?>" >
                                    <input type="submit" name="modifika_asunto" id="modifika_asunto" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Asunto del mensaje de correo cuando hay problemas de acceso") ?></th>
                            <td>&nbsp;</td>
                            <td><?php echo $asunto_mens_error; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="text" autofocus required class="form-control" id="direccion_general_11"  value="<?php echo $asunto_mens_error; ?>" >
                                    <input name="valor" type="hidden" id="valor16" value="<?php echo $asunto_mens_error; ?>" >
                                    <input type="submit" name="modifika_mens_error" id="modifika_mens_error" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>

                    </table>
</div>

                    <!--Final-->

<?php } ?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que lista los ítems  del bloque del menú del blog exterior, desde aquí se puede acceder a modificar o borrar
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 1;
include('../private/inc_web/nivel_acceso.php');
if($_SESSION['admin_blog']==1){


$activo=$variables['activo'];
$sql = "SELECT id, title, description, url, nivel, padre, orden, activo  FROM $tbn36 WHERE activo='$activo' OR activo='0' ORDER BY 'orden' ";
$result = mysqli_query($con, $sql);
?>


                    <!--Comiezo-->
                    <div class="card-header"> <h1 class="card-title"><?= _("Enlaces del MENÚ") ?><?php if($activo=="2"){ ?> <?= _("del pie de pagina")?><?php } ?></h1> </div>


                    <div class="card-body">
                      <div class="row">

                      <div class="col-sm-8"></div>
                      <div class="col-sm-4">
                      <a href="admin.php?c=<?php echo encrypt_url('admin/blog_menu/activo='.$activo,$clave_encriptacion) ?>" class="btn btn-primary btn-block"><?= _("Añadir enlace al menú") ?></a>

                      </div>
                    </div>

                    <p>&nbsp;</p>
                    <?php
                    if (isset($borrado)) {
                        echo "$borrado";
                    }
                    ?>
                    <p>&nbsp;</p>

                    <form name="formulario" method="post" action="<?php $_SERVER['PHP_SELF'] ?>">
                        <?php
                        if ($row = mysqli_fetch_array($result)) {
                            ?>

                            <table id="tabla1" class="table table-striped table-bordered dt-responsive nowrap" data-page-length="25">
                                <thead>
                                    <tr>
                                        <th width=2%>ID</th>
                                        <th width=25%><?= _("Titulo") ?></th>
                                        <th width=30%><?= _("descripción") ?></th>
                                        <th width=25%><?= _("URL") ?></th>
                                        <th width=3%><?= _("Orden") ?></th>
                                        <?php if($activo==1){?>
                                        <th width=3%><?= _("Hijo de") ?>:</th>
                                        <?php }?>
                                        <th width=3% ><?= _("Activo") ?></th>
                                        <th width=6%><?= _("modificar") ?> <?= _("datos") ?></th>
                                        <th width=6% ><?= _("borrar") ?></th>

                                    </tr>
                                </thead>
                                <tbody>
                                  <?php
                                    mysqli_field_seek($result, 0);
                                    do {
                                        ?>
                                        <tr>
                                            <td><?php echo "$row[0]" ?></td>
                                            <td><?php echo "$row[1]" ?></td>
                                            <td><?php echo "$row[2]" ?></td>
                                            <td> <input class="form-control" type="text" value="<?php echo "$row[3]" ?>" readonly></td>
                                            <td><?php echo "$row[6]" ?></td>
                                            <?php if($activo==1){?>
                                            <td><?php echo ltrim($row[5], '0'); ?></td>
                                            <?php }?>
                                            <td><?php
                                            if ($row[7]==0){
                                              echo '<a role="button" class="" data-toggle="tooltip" data-html="true" title="" data-original-title="<em>'. _("Modulo inactivo") .' </em> ">
                                              <i class=" fa fa-times-circle-o text-danger"></i>
                                              </a>';
                                              }else{
                                                echo '<a role="button" class="" data-toggle="tooltip" data-html="true" title="" data-original-title="<em>'. _("Modulo activo") .' </em> ">
                                                <i class="fa fa-check-square-o text-success"></i>
                                                </a>';
                                              }?></td>
                                            <td><a href="admin.php?c=<?php echo encrypt_url('admin/blog_menu/id='.$row[0].'&activo='.$activo,$clave_encriptacion) ?>" class="btn btn-primary btn-block"><?= _("modificar") ?></a></td>
                                            <td><a href="admin.php?c=<?php echo encrypt_url('admin/blog_menu_borra/id='.$row[0].'&activo='.$activo,$clave_encriptacion) ?>" class="btn btn-danger btn-block" onClick="return borravotante()" ><?= _("Borrar") ?> </a></td>
                                        </tr>
                                        <?php
                                    } while ($row = mysqli_fetch_array($result));
                                    ?>

                                </tbody>
                                <tfoot>
                                    <tr>
                                      <th>ID</th>
                                      <th><?= _("Titulo") ?></th>
                                      <th><?= _("descripción") ?></th>
                                      <th><?= _("URL") ?></th>
                                      <th><?= _("Orden") ?></th>
                                      <?php if($activo==1){?>
                                      <th><?= _("Hijo de") ?>:</th>
                                      <?php }?>
                                      <th><?= _("Activo") ?></th>
                                      <th><?= _("modificar") ?><?= _("datos") ?></th>
                                      <th><?= _("borrar") ?></th>
                                    </tr>
                                </tfoot>

                            </table>


                            <p>&nbsp;</p>

                        </form>

                        <?php
                    } else {?>
                      <div class="alert alert-info">
                        <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                        <i class="fa fa-times simple-remove" aria-hidden="true"></i>
                        </button>
                        <span>
                          <b> Info - </b> <?=_("¡No se ha encontrado ningún enlace");?></span>
                        </div>
                        <?php


                    }
                    ?><!---->

</div>

        <script type="text/javascript" src="assets/DataTables/datatables.min.js" ></script>
        <script src="assets/DataTables/Buttons-1.6.2/js/dataTables.buttons.min.js" ></script>
        <script src="assets/DataTables/JSZip-2.5.0/jszip.min.js" ></script>
        <script src="assets/DataTables/pdfmake-0.1.36/pdfmake.min.js" ></script>
        <script src="assets/DataTables/pdfmake-0.1.36/vfs_fonts.js" ></script>
        <script src="assets/DataTables/Buttons-1.6.2/js/buttons.html5.min.js" ></script>

        <script type="text/javascript" language="javascript" class="init">
          $(document).ready(function() {
            $('#tabla1').DataTable({
              responsive: true,
              dom: 'Blfrtip',
              buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
              ],
              lengthMenu: [
                [10, 25, 50, -1],
                [10, 25, 50, "Todos"]
              ],
              "columns": [
                { "width": "4%" },
                { "width": "25%" },
                { "width": "30%" },
                { "width": "25%" },
                { "width": "4%" },
                { "width": "4%" },
                { "width": "2%" },
                { "width": "4%" },
                { "width": "4%" }
              ],
              language: {
                buttons: {
                  copy: '<?= _("Copiar") ?>',
                  copyTitle: '<?= _("Copiado en el portapapeles") ?>',
                  copyKeys: '<?= _("Presione") ?> <i> ctrl </i> o <i> \ u2318 </i> + <i> C </i> <?= _("para copiar los datos de la tabla a su portapapeles") ?>. <br> <br> <?= _("Para cancelar, haga clic en este mensaje o presione Esc") ?>.',
                  copySuccess: {
                    _: '%d <?= _("lineas copiadas") ?>',
                    1: '1 <?= _("linea copiada") ?>'
                  }
                },
                processing: "<?= _("Tratamiento en curso") ?>...",
                search: "<?= _("Buscar") ?>:",
                lengthMenu: "<?= _("Ver") ?> _MENU_  <?= _("resultados por pagina") ?>",
                info: "<?= _("Mostrando") ?> _PAGE_ <?= _("de") ?> _PAGES_ <?= _("paginas") ?>",
                infoEmpty: "<?= _("No se han encitrado resultados") ?>",
                infoFiltered: "(<?= _("filtered from") ?> _MAX_ <?= _("total records") ?>)",
                infoPostFix: "",
                loadingRecords: "<?= _("Cargando") ?>...",
                zeroRecords: "<?= _("No se han encontrado resultados - perdone") ?>",
                emptyTable: "<?= _("No se han encontrado resultados - perdone") ?>",
                paginate: {
                  first: "<?= _("Primero") ?>",
                  previous: "<?= _("Anterior") ?>",
                  next: "<?= _("Siguiente") ?>",
                  last: "<?= _("Ultimo") ?>"
                },
                aria: {
                  sortAscending: ": <?= _("activar para ordenar columna de forma ascendente") ?>",
                  sortDescending: ": <?= _("activar para ordenar columna de forma descendente") ?>"
                }
              }
            });
          });
        </script>
        <!--end datatables -->
        <script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js" ></script>
        <script src="https://cdn.datatables.net/responsive/2.2.7/js/responsive.bootstrap4.min.js" ></script>
        <script src="assets/js/admin_borrarevento.js" ></script>
<?php }
}
 ?>

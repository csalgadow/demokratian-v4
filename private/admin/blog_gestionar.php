<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###                                                         ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo con las distintas opciones para gestionar el aspecto del blog, desde aquí se accede a las páginas para activar módulos, poner el orden de los módulos, crear css, etc
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 9;
include('../private/inc_web/nivel_acceso.php');
if($_SESSION['admin_blog']==1){

?>
<div class="card-header"> <h1 class="card-title"><?= _("Panel de gestion del aspecto del BLOG") ?></h1> </div>


<div class="card-body">

                    <!--Comiezo-->
                    <!---->
                    <div class="row">
                        <div class="col-sm-3"><?= _("MODULOS") ?></div>
                            <div class="col-sm-3">
                                <a href="admin.php?c=<?php echo encrypt_url('admin/blog_modulos/s=SD',$clave_encriptacion); ?>" class="btn btn-primary btn-block"><?= _("Activar/desactivar modulos") ?></a>
                            </div>
                        <div class="col-sm-3">


                        </div>
                        <div class="col-sm-3">


                        </div>
                    </div>
                    <p>&nbsp;</p>

                    <!---->
                    <div class="row">
                        <div class="col-sm-3"><?= _("BLOQUES ZONA CABECERA") ?></div>
                        <div class="col-sm-3">
                            <a href="admin.php?c=<?php echo encrypt_url('admin/blog_gestion_modulos/idcab=1',$clave_encriptacion); ?>" class="btn btn-primary btn-block"><?= _("Modificar cabecera pagina inicio") ?></a>
                        </div>
                        <div class="col-sm-3">
                            <a href="admin.php?c=<?php echo encrypt_url('admin/blog_gestion_modulos/idcab=2',$clave_encriptacion); ?>" class="btn btn-primary btn-block"><?= _("Modificar cabecera resto de paginas") ?></a>
                        </div>
                        <div class="col-sm-3">

                        </div>
                    </div>
                    <p>&nbsp;</p>

                    <!---->
                    <div class="row">
                        <div class="col-sm-3"><?= _("BLOQUES ZONA CUERPO") ?></div>
                        <div class="col-sm-3">
                            <a href="admin.php?c=<?php echo encrypt_url('admin/blog_gestion_modulos/idcab=3',$clave_encriptacion); ?>" class="btn btn-primary btn-block"><?= _("Modificar cuerpo inicio") ?></a>
                        </div>
                        <div class="col-sm-3">

                        </div>
                        <div class="col-sm-3">

                        </div>
                    </div>
                    <p>&nbsp;</p>

                    <!---->

                    <div class="row">
                        <div class="col-sm-3"><?= _("CSS") ?></div>
                        <div class="col-sm-3">
                            <a href="admin.php?c=<?php echo encrypt_url('admin/blog_css/s=SD',$clave_encriptacion); ?>" class="btn btn-primary btn-block"><?= _("Incluir CSS personalizado") ?></a>
                        </div>
                        <div class="col-sm-3">


                        </div>
                        <div class="col-sm-3">


                        </div>
                    </div>
                    <!---->
                    <p>&nbsp;</p>
                    <!-- Menu redes sociales-->
                    <div class="row">
                        <div class="col-sm-3"><?= _("REDES SOCIALES") ?></div>
                        <div class="col-sm-3">
                            <a href="admin.php?c=<?php echo encrypt_url('admin/blog_constantes/s=SD',$clave_encriptacion); ?>" class="btn btn-primary btn-block"><?= _("Gestionar menú redes sociales") ?></a>
                        </div>
                        <div class="col-sm-3">


                        </div>
                        <div class="col-sm-3">


                        </div>
                    </div>
                    <!---->
                    <!--Final-->
                    <p>&nbsp;</p>
                    <!-- Pie de pagina-->
                    <div class="row">
                        <div class="col-sm-3"><?= _("PIE de página") ?></div>
                        <div class="col-sm-3">
                            <a href="admin.php?c=<?php echo encrypt_url('admin/blog_menu_busq/activo=2',$clave_encriptacion); ?>" class="btn btn-primary btn-block"><?= _("Gestionar menú del pie de página") ?></a>
                        </div>
                        <div class="col-sm-3">
                            <a href="admin.php?c=<?php echo encrypt_url('admin/blog_texto_pie/s=SD',$clave_encriptacion); ?>" class="btn btn-primary btn-block"><?= _("Texto pie de página") ?></a>
                        </div>
                        <div class="col-sm-3">


                        </div>
                    </div>
                    <!---->
                    <!--Final-->

</div>


<?php
    }
  }
 ?>

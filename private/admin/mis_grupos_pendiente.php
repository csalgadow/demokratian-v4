<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###                                                         ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que lista los usuarios pendientes de autorizar para que participen en un  grupo de trabajo
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 9;
include('../private/inc_web/nivel_acceso.php');

$bloqueado = "";
$razon_bloqueo = "";
$mensaje = "";

$idgr = fn_filtro($con, $variables['idgr']);
if (isset($_POST["update_multi"])) {
    unset($_POST['tabla1_length'], $_POST['update_multi']);

    if (isset($_POST["razon_bloqueo"])) {
        $razon_bloqueo = fn_filtro($con, $_POST["razon_bloqueo"]);
    }
    foreach ($_POST as $k => $v)
        $a[] = $v;
    $datos = count($a);
    $i = 0;

    while ($i < $datos) {
        $val = $a[$i];
        $desbloqueo = 1;  //codigo  o

        $update = mysqli_query($con, "UPDATE $tbn6 SET estado=\"$desbloqueo\",razon_bloqueo=\"$razon_bloqueo\"  WHERE id_usuario=$val and id_grupo_trabajo=" . $idgr . " ") or die("No puedo ejecutar la instrucción de  SQL query");
        if (!$update) {
            echo "<div class=\"alert alert-danger\">
                                        <strong> UPSSS!!!!<br/>" . _("esto es embarazoso, hay un error en la conexion con la base de datos") . " </strong> </div><strong>";
        }
        if ($update) {

            $mensaje .= " <div class=\"alert alert-success\">  " . _("Usuario con identificador") . ": " . $val . " " . _("ha sido añadido al grupo") . "</div>";
        }

        $i++;
    }
}


$sql = "SELECT a.ID,  a.nombre_usuario, a.correo_usuario,  a.tipo_votante, a.usuario, b.razon_bloqueo,b.estado, a.nif FROM $tbn9 a,$tbn6 b where (a.ID= b.id_usuario) and b.id_grupo_trabajo=" . $idgr . " and  b.estado = 0   order by a.nombre_usuario ";
$result = mysqli_query($con, $sql);
?>



<!--Comiezo-->



                <div class="card-header"> <h1 class="card-title"><?= _("Votantes que quieren participar en este grupo de trabajo") ?></h1> </div>


                <div class="card-body">


                    <p><?php echo $mensaje; ?>&nbsp;</p>

                    <form name="formulario" method="post" action="<?php $_SERVER['PHP_SELF'] ?>">
                        <?php
                        if ($row = mysqli_fetch_array($result)) {
                            ?>

                            <table id="tabla1" class="table table-striped table-bordered dt-responsive nowrap" data-page-length="25">
                                <thead>
                                    <tr>
                                        <th width=12%><?= _("Nombre") ?></th>
                                        <th width=10%><?= _("Correo") ?></th>
                                        <th width=6%><?= _("Tipo") ?></th>
                                        <th width=7%><?= _("NICK") ?></th>
                                        <th width=14%><?= _("Nif") ?></th>


                                        <th width=4% ><?= _("Marcar para añadir") ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    mysqli_field_seek($result, 0);

//echo "</tr> \n";
//$var_bol=true;
                                    do {
                                        ?>
                                        <tr>
                                            <td><?php echo "$row[1]" ?></td>
                                            <td><?php echo "$row[2]" ?></td>
                                            <td><?php echo "$row[3]" ?></td>
                                            <td><?php echo "$row[4]" ?></td>
                                            <td><?php echo "$row[7]" ?></td>

                                            <td>
                                                <input name="borrar_multiples<?php echo "$row[0]" ?>" type="checkbox" id="borrar_multiples" value="<?php echo "$row[0]" ?>" />

                                            </td>
                                        </tr>
                                        <?php
                                    } while ($row = mysqli_fetch_array($result));
                                    ?>

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th width=12%><?= _("Nombre") ?></th>
                                        <th width=10%><?= _("Correo") ?></th>
                                        <th width=6%><?= _("Tipo") ?></th>
                                        <th width=7%><?= _("NICK") ?></th>
                                        <th width=14%><?= _("Nif") ?></th>
                                        <th width=4% ><?= _("Marcar para añadir") ?></th>
                                    </tr>
                                </tfoot>

                            </table><p>&nbsp;</p>

                            <p>&nbsp;</p>
                            <input name="update_multi" type="submit"  class="btn btn-primary btn-block"   id="update_multi" value="<?= _("Incluir") ?>" />

                        </form>

                        <?php
                    } else {

                        echo "<div class=\"alert alert-success\">" . _("¡No hay ningun usuario pendiente en este grupo de trabajo!") . " </div>";
                    }
                    ?><!---->
</div>
                    <!--Final-->



        <script type="text/javascript" src="assets/DataTables/datatables.min.js" ></script>
        <script src="assets/DataTables/Buttons-1.6.2/js/dataTables.buttons.min.js" ></script>
        <script src="assets/DataTables/JSZip-2.5.0/jszip.min.js"></script>
        <script src="assets/DataTables/pdfmake-0.1.36/pdfmake.min.js" ></script>
        <script src="assets/DataTables/pdfmake-0.1.36/vfs_fonts.js" ></script>
        <script src="assets/DataTables/Buttons-1.6.2/js/buttons.html5.min.js" ></script>

        <script type="text/javascript" language="javascript" class="init">
          $(document).ready(function() {
            $('#tabla1').DataTable({
              responsive: true,
              dom: 'Blfrtip',
              buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
              ],
              lengthMenu: [
                [10, 25, 50, -1],
                [10, 25, 50, "Todos"]
              ],
              language: {
                buttons: {
                  copy: '<?= _("Copiar") ?>',
                  copyTitle: '<?= _("Copiado en el portapapeles") ?>',
                  copyKeys: '<?= _("Presione") ?> <i> ctrl </i> o <i> \ u2318 </i> + <i> C </i> <?= _("para copiar los datos de la tabla a su portapapeles") ?>. <br> <br> <?= _("Para cancelar, haga clic en este mensaje o presione Esc") ?>.',
                  copySuccess: {
                    _: '%d <?= _("lineas copiadas") ?>',
                    1: '1 <?= _("linea copiada") ?>'
                  }
                },
                processing: "<?= _("Tratamiento en curso") ?>...",
                search: "<?= _("Buscar") ?>:",
                lengthMenu: "<?= _("Ver") ?> _MENU_  <?= _("resultados por pagina") ?>",
                info: "<?= _("Mostrando") ?> _PAGE_ <?= _("de") ?> _PAGES_ <?= _("paginas") ?>",
                infoEmpty: "<?= _("No se han encitrado resultados") ?>",
                infoFiltered: "(<?= _("filtered from") ?> _MAX_ <?= _("total records") ?>)",
                infoPostFix: "",
                loadingRecords: "<?= _("Cargando") ?>...",
                zeroRecords: "<?= _("No se han encontrado resultados - perdone") ?>",
                emptyTable: "<?= _("No se han encontrado resultados - perdone") ?>",
                paginate: {
                  first: "<?= _("Primero") ?>",
                  previous: "<?= _("Anterior") ?>",
                  next: "<?= _("Siguiente") ?>",
                  last: "<?= _("Ultimo") ?>"
                },
                aria: {
                  sortAscending: ": <?= _("activar para ordenar columna de forma ascendente") ?>",
                  sortDescending: ": <?= _("activar para ordenar columna de forma descendente") ?>"
                }
              }
            });
          });
        </script>
        <script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js" ></script>
        <script src="https://cdn.datatables.net/responsive/2.2.7/js/responsive.bootstrap4.min.js" ></script>
        <!--end datatables -->
        <script src="assets/js/admin_borrarevento.js" ></script>


<?php } ?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo borra los ítems  del menú del blog exterior
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 1;
include('../private/inc_web/nivel_acceso.php');
if($_SESSION['admin_blog']==1){

$row[0] = "";
$row[1] = "";
$row[2] = "";
$row[3] = "";
$row[4] = "";
$row[5] = "";
$row[6] = "";
$row[7] = "";
$row[8] = "";


if (isset($variables['id'])) {
    $id = fn_filtro_numerico($con, $variables['id']);
}

if (isset($variables['activo'])) {
    $activo = fn_filtro_numerico($con, $variables['activo']);
}


        $result = mysqli_query($con, "SELECT 	id, id_incluido, title, description, url, nivel, padre, orden, activo  FROM $tbn36 where id=$id");
        $row = mysqli_fetch_row($result);


        $hijos = mysqli_query($con, "SELECT id FROM $tbn36 where padre=$id"); // comprobamos que no tenga enlaces hijo para impedir que se borre mientras los tenga
        $row_cnt_hijos = mysqli_num_rows($hijos);

        if ($row_cnt_hijos == 0){

        $borrado1 = mysqli_query($con, "DELETE FROM $tbn36 WHERE id=" . $id . "") ;
        if (!$borrado1) {
            $inmsg= "<div class=\"alert alert-danger\">" . _("Error en el borrado del enlace") . "</div>";
        } elseif (mysqli_affected_rows($con) == 0) {
            $inmsg= "<div class=\"alert alert-danger\">" . _("Error en el borrado del enlace") . "</div>";
        } else {
            $inmsg= "<div class=\"alert alert-success\">" . _("Borrado correctamente el enlace") . "</div>";
        }
      }else{
        $inmsg= "<div class=\"alert alert-danger\">" . _("Este enlace tiene") ."<strong>   ".$row_cnt_hijos." </strong>  "._("enlaces hijo").". ". _("No se podrá borrar mientras esto pase") . "</div>";

      }
?>



                    <!--Comiezo-->
                    <div class="card-header"> <h1 class="card-title"><?php  echo _("BORRADO UN ENLACE DEL MENÚ");?></h1> </div>

                      <div class="row">
                          <div class="col-sm-8"></div>
                          <div class="col-sm-4">
                              <a href="admin.php?c=<?php echo encrypt_url('admin/blog_menu_busq/activo='.$activo,$clave_encriptacion); ?>" class="btn btn-primary btn-block"><?= _("Ver todos los enlaces") ?></a>
                          </div>
                      </div>

                        <div class="card-body">
                    <p>&nbsp;</p>

                    <?php
                    if (isset($inmsg)) {
                        echo "$inmsg";
                    }
                    ?>
                    <fieldset>


                        <div class="form-group row">
                            <label for="title" class="col-sm-3 control-label"><?= _("Titulo") ?></label>

                            <div class="col-sm-9"><?php echo "$row[2]"; ?></div>
                          </div>
                        <div class="form-group row">
                            <label for="description" class="col-sm-3 control-label"><?= _("Descripción") ?></label>

                            <div class="col-sm-9"><?php echo "$row[3]"; ?></div>
                          </div>
                        <div class="form-group row">
                            <label for="url" class="col-sm-3 control-label"><?= _("URL") ?> </label>
                            <div class="col-sm-9"><?php echo "$row[4]"; ?></div>
                          </div>

                    <!--    <div class="form-group row">
                            <label for="nivel" class="col-sm-3 control-label"> <?= _("Nivel") ?> </label>
                            <div class="col-sm-4"><?php echo "$row[5]"; ?></div>
                          </div>-->

                          <div class="form-group row">
                              <label for="padre" class="col-sm-3 control-label"> <?= _("Padre") ?> </label>

                              <div class="col-sm-4">
                                <?php
                                $sql = "SELECT id, title FROM $tbn36 WHERE id=$row[6] ORDER BY 'orden' ";
                                $resulta =  mysqli_query($con, $sql);

                                $row_cnt = mysqli_num_rows($resulta);
                                if ($row_cnt !=0){
                                  $listrows = mysqli_fetch_row($resulta);
                                    echo  $listrows[1];
                                    }else{
                                      echo _("No tiene") ;
                                    }
                                ?>

                              </div>
                            </div>


                            <div class="form-group row">
                                <label for="orden" class="col-sm-3 control-label"> <?= _("Orden") ?> </label>
                                <div class=" col-md-9"><?php echo "$row[7]"; ?>  </div>
                              </div>
                        <div class="form-group row">
                            <div  class="col-sm-3 "><?= _("Activo") ?> </div>

                            <div class="col-sm-9"><?php
                                if ($row[8] == 0) {
                                    echo _("Desactivado");
                                } else if ($row[8] == 1) {
                                    echo _("Activado");
                                }else if ($row[8] == 2) {
                                    echo _("Activado");
                                }
                                ?></div>
                              </div>



                            <p>&nbsp;</p>



                    <p>&nbsp;</p>



                    <!--Final-->
</fieldset>
</div>

<?php
  }
}
?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que crea y modifica los ítems  del bloque del menú del blog exterior
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 1;
include('../private/inc_web/nivel_acceso.php');
if($_SESSION['admin_blog']==1){

$row[0] = "";
$row[1] = "";
$row[2] = "";
$row[3] = "";
$row[4] = "";
$row[5] = "";
$row[6] = "";
$row[7] = "";
$row[8] = "";


$fecha_ver = date("d-m-Y ");
$fecha = date("Y-m-d H:i:s");

if (isset($variables['id'])) {
    $id = fn_filtro_numerico($con, $variables['id']);
    $acc = "modifika";
}else{
  $acc="";
}

if (isset($variables['activo'])) {
    $activo = fn_filtro_numerico($con, $variables['activo']);
}


if (ISSET($_POST["modifika_menu"])) {

        $title = fn_filtro($con, $_POST['title']);
        $description = fn_filtro($con, $_POST['description']);
        $url = fn_filtro($con, $_POST['url']);
      //  $nivel = fn_filtro($con,$_POST['nivel']);
        $padre = fn_filtro_numerico($con,$_POST['padre']);
        $orden = fn_filtro_numerico($con, $_POST['orden']);
        $activar = fn_filtro_numerico($con, $_POST['activo']);




        $sSQL = "UPDATE $tbn36 SET title=\"$title\", description=\"$description\", url=\"$url\" ,padre=\"$padre\",  orden=\"$orden\"  ,activo=\"$activar\"   WHERE id='$id'";
        mysqli_query($con, $sSQL) or die("Imposible modificar pagina");
        $inmsg = " <div class=\"alert alert-success\">" . _("modificado") . "  <strong> " . $title .  "</strong> " . _("en la base de datos") . " </div>";

}

if (ISSET($_POST["add_menu"])) {

  $title = fn_filtro($con, $_POST['title']);
  $description = fn_filtro($con, $_POST['description']);
  $url = fn_filtro($con, $_POST['url']);
//  $nivel = fn_filtro($con,$_POST['nivel']);
  $padre = fn_filtro_numerico($con,$_POST['padre']);
  $orden = fn_filtro_numerico($con, $_POST['orden']);
  $activar = fn_filtro_numerico($con, $_POST['activo']);

  $id_incluido = $_SESSION['ID'];

        $insql = "insert into $tbn36 (id_incluido,  title,  description,  url,  padre, orden, activo ) values (  \"$id_incluido\",\"$title\",\"$description\",\"$url\",\"$padre\",\"$orden\",\"$activar\")";
        //$inres = @mysqli_query($con, $insql) or die("<strong><font color=#FF0000 size=3>" . _("Imposible añadir. Cambie los datos e intentelo de nuevo") . ".</font></strong>");
        $mens = "Mensaje error añadido nuevo enlace de menú";
        $resultados = db_query_id($con, $insql, $mens);
        if (!$resultados) {
            $inmsg = "<div class=\"alert alert-danger\">" .
                    _("Error al crear el enlace") . " \" " . $title . " \" " . _("ya que no se ha añadido a la base de datos") . "	</div>";
        } else {
        $inmsg = " <div class=\"alert alert-success\">" . _("Añadido") . " <strong> " . $title . "</strong> " . _("a la base de datos") . "</div> ";
      }
}

  if (isset($variables['id'])) {
        $result = mysqli_query($con, "SELECT 	id, id_incluido, title, description, url, nivel, padre, orden, activo  FROM $tbn36 where id=$id");
        $row = mysqli_fetch_row($result);
    }
?>



                    <!--Comiezo-->
                    <div class="card-header"> <h1 class="card-title">
                      <?php
                        if ($acc != "modifika") {
                            echo _("AÑADIR UN NUEVO ENLACE AL MENÚ");
                        } else {
                            echo _("MODIFICAR UN ENLACE DEL MENÚ");
                        }
                        ?>
                      </h1> </div>

                      <div class="row">
                          <div class="col-sm-8"></div>
                          <div class="col-sm-4">
                              <a href="admin.php?c=<?php echo encrypt_url('admin/blog_menu_busq/activo='.$activo,$clave_encriptacion); ?>" class="btn btn-primary btn-block"><?= _("Ver todos los enlaces") ?></a>
                          </div>
                      </div>

                        <div class="card-body">
                    <p>&nbsp;</p>

                    <?php
                    if (isset($inmsg)) {
                        echo "$inmsg";
                    }
                    ?>
<fieldset>
                    <form action="<?php $_SERVER['PHP_SELF'] ?>" method=post name="frmDatos" id="frmDatos" >

                        <div class="form-group row">
                            <label for="title" class="col-sm-3 control-label"><?= _("Titulo") ?></label>

                            <div class="col-sm-9">

                                <input name="title" type="text" id="title" value="<?php echo "$row[2]"; ?>" class="form-control" placeholder="<?= _("Titulo") ?>" required autofocus data-validation-required-message="<?= _("El nombre  es un dato requerido") ?>">
                            </div></div>
                        <div class="form-group row">
                            <label for="description" class="col-sm-3 control-label"><?= _("Descripción") ?></label>

                            <div class="col-sm-9">

                                <input name="description" type="text" id="description" value="<?php echo "$row[3]"; ?>" class="form-control" placeholder="<?= _("Descripción del enlace") ?>" >
                            </div></div>
                        <div class="form-group row">
                            <label for="url" class="col-sm-3 control-label"><?= _("URL") ?> (<?= _("Si es un elemento del que cuelgan otros enlaces, ponga") ?>:  #) </label>

                            <div class="col-sm-9">
                                <div class="controls">
                                    <input name="url" type="text"  id="url" value="<?php echo "$row[4]"; ?>"  class="form-control" placeholder="<?= _("Enlace") ?>" required  data-validation-required-message="<?= _("Por favor, ponga una url valida") ?>" />
                                    <p class="help-block"></p>
                                </div>
                            </div></div>

                    <!--    <div class="form-group row">
                            <label for="nivel" class="col-sm-3 control-label"> <?= _("Nivel") ?> </label>

                            <div class="col-sm-4">
                                <input name="nivel" type="text" id="nivel" value="<?php echo "$row[5]"; ?>" class="form-control" placeholder="<?= _("Nivel") ?>"/>
                            </div>
                          </div>-->
                          <?php if($activo==1){?>
                          <div class="form-group row">
                              <label for="padre" class="col-sm-3 control-label"> <?= _("Padre") ?> </label>

                              <div class="col-sm-4">
                                <?php
                                  $lista1="";
                                $sql = "SELECT id, title FROM $tbn36 WHERE activo='$activo' OR activo='0' ORDER BY 'orden' ";
                                $resulta =  mysqli_query($con, $sql);


                                while ($listrows = mysqli_fetch_array($resulta)) {
                                    $id_pro = $listrows['id'];
                                    $name1 = $listrows['title'];
                                    if ($id_pro == $row[6]) {
                                        $check = "selected=\"selected\" ";
                                    } else {
                                        $check = "";
                                    }
                                    $lista1 .= "<option value=\"$id_pro\"  $check> $name1</option>";
                                }
                                ?>
                                <select name="padre" class="form-control custom-select" id="padre" >
                                  <option value="000000">Raiz del menú</option>
                                    <?php echo "$lista1"; ?>
                                </select>



                              </div>
                            </div>
                          <?php }else{ ?>
                            <input id="padre" name="padre" type="hidden" value="000000">
                          <?php
                          }?>

                            <div class="form-group row">
                                <label for="orden" class="col-sm-3 control-label"> <?= _("Orden") ?> </label>

                                <div class=" col-md-3">
                                        <input name="orden" type="number" class="form-control" id="orden"
                                        value="<?php if ($row[7]=="") { echo "0"; } else{ echo "$row[7]";} ?>" min="0" required
                                        oninvalid="this.setCustomValidity('<?= _("El orden es un dato requerido") ?>')"
                                        oninput="this.setCustomValidity('')"/>
                                </div>
                                        <div class=" col-md-6">

                                  </div>

                              </div>
                        <div class="form-group row">
                            <div  class="col-sm-3 "><?= _("Activo") ?> </div>

                            <div class="col-sm-9">

                                <?php

                                if ($row[8] == 0) {
                                    $chekeado0 = "checked=\"checked\" ";
                                } else if ($row[8] == 1) {
                                    $chekeado1 = "checked=\"checked\" ";
                                }else if ($row[8] == 2) {
                                    $chekeado2 = "checked=\"checked\" ";
                                }
                                ?>
                                <label for="activo_0" class="control-label">
                                <input name="activo" type="radio" id="activo_0" value="0" <?php
                                if (isset($chekeado0)) {
                                    echo "$chekeado0";
                                }
                                ?> />  <?= _("Desactivado") ?> </label><br/>

                                <?php
                                if($activo==1){  ?>
                                <label for="activo_1" class="control-label">
                                <input type="radio" name="activo" value="1" id="activo_1"  <?php
                                if (isset($chekeado1)) {
                                    echo "$chekeado1";
                                }
                                ?> />
                                <?= _("Activado") ?> </label>

                              <?php  }
                                if($activo==2){  ?>

                                <label for="activo_2" class="control-label">
                                <input type="radio" name="activo" value="2" id="activo_2"  <?php
                                if (isset($chekeado2)) {
                                    echo "$chekeado2";
                                }
                                ?> />
                                <?= _("Activado") ?> </label>
                              <?php  } ?>
                            </div></div>




                            <?php if ($acc == "modifika") { ?>
                                <input name="modifika_menu" type=submit  class="btn btn-primary btn-block"  id="modifika_menu" value="<?= _("ACTUALIZAR ENLACE DEL MENÚ") ?>" />
                            <?php } else { ?>
                                <input name="add_menu" type=submit class="btn btn-primary btn-block"  id="add_menu" value="<?= _("AÑADIR  ENLACE AL MENÚ") ?>" />
                            <?php } ?>

                            <p>&nbsp;</p>

                    </form>

                    <p>&nbsp;</p>



                    <!--Final-->
</fieldset>
</div>

<?php }
}
?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que lista los resultados de la búsqueda de censos_buscador.php
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 1;
include('../private/inc_web/nivel_acceso.php');

////////////////Borrado multible/////////////////////////
if (isset($_POST["delete_multi"])) {
    $mens_ok = "";
    unset($_POST['tabla1_length']);


    foreach ($_POST as $k => $v)
        $a[] = $v;
    $datos = count($a) - 7;
    $i = 0;

    while ($i < $datos) {
        $val = fn_filtro($con, $a[$i]);

        ///borramos la lista de votaciones donde ha participado
        $borrado_usuario_x_votacion = "DELETE FROM $tbn2 WHERE id_votante=" . $val . " ";
        $mens = _("ERROR en el borrado de la tabla") . " usuario_x_votacion";
        $result_usuario_x_votacion = db_query($con, $borrado_usuario_x_votacion, $mens);

        //borramos debate_comentario con esta id
        $borrado_debate_comentario = "DELETE FROM $tbn12 WHERE id_usuario=" . $val . " ";
        $mens = _("ERROR en el borrado de la tabla") . " debate comentario";
        $result_borrado_debate_comentario = db_query($con, $borrado_debate_comentario, $mens);

        //borramos debate_votos con esta id
        $borrado_debate_votos = "DELETE FROM $tbn14 WHERE id_votante=" . $val . "";
        $mens = _("ERROR en el borrado de la tabla") . " debate votos";
        $result_borrado_debate_votos = db_query($con, $borrado_debate_votos, $mens);

        //borramos la relacion entre votantes y modifciaciones de los administradores
        $borrado_votantes_x_admin = "DELETE FROM $tbn17 WHERE id_votante=" . $val . " ";
        $mens = _("ERROR en el borrado de la tabla") . " votantes_x_admin";
        $result_votantes_x_admin = db_query($con, $borrado_votantes_x_admin, $mens);

        //borramos si es administrador, la relacion de provincias que pueda tener asignadas
        $borrado_usuario_admin_x_provincia = "DELETE FROM $tbn5 WHERE id_usuario=" . $val . " ";
        $mens = _("ERROR en el borrado de la tabla") . "usuario_admin_x_provincia";
        $result_usuario_admin_x_provincia = db_query($con, $borrado_usuario_admin_x_provincia, $mens);

        //borramos si es administrador, la relacion de grupos de trabajo que pueda tener asignadas
        $borrado_usuario_x_g_trabajo = "DELETE FROM $tbn6 WHERE id_usuario=" . $val . " ";
        $mens = _("ERROR en el borrado de la tabla") . " usuario_x_g_trabajo";
        $result_usuario_x_g_trabajo = db_query($con, $borrado_usuario_x_g_trabajo, $mens);

        //miramos que imagen tiene el usuario para borrarla
        $sql_img = "SELECT id,imagen_pequena FROM $tbn9 WHERE ID=" . $val . " ";
        $result_img = mysqli_query($con, $sql_img);
        $row_img = mysqli_fetch_row($result_img);

        if ($row_img[1] != "peq_usuario.jpg") {

            $thumb_photo_exists = $upload_user . "/" . $row_img[1];
            echo $thumb_photo_exists;
            if (file_exists($thumb_photo_exists)) {
                unlink($thumb_photo_exists);
            }
        }

        $borrar = "DELETE FROM $tbn9 WHERE id=" . $val . " ";
        $mens = "ERROR en el borrado del votante";
        $result = db_query($con, $borrar, $mens);

        if (!$result) {
            $mens_error .= $mens . "<br/>";
        } else {
            $mens_ok .= _("Borrados correctamente $datos usuarios") . " <br/>";
        }

        $i++;
    }
}
if (isset($_POST['provincia'])) {
    $id_provincia = fn_filtro($con, $_POST['provincia']);
} else {
    $id_provincia = "";
}
$nombre_usuario = fn_filtro($con, $_POST['nombre_usuario']);
$correo_usuario = fn_filtro($con, $_POST['correo_electronico']);
$nif = fn_filtro($con, $_POST['nif']);
$tipo_votante = fn_filtro($con, $_POST['tipo_usuario']);
if (isset($_POST['usuario'])) {
    $usuario = fn_filtro($con, $_POST['usuario']);
} else {
    $usuario = "";
}

$sql = "SELECT ID, id_provincia,  nombre_usuario, correo_usuario, nif, tipo_votante, usuario,bloqueo,apellido_usuario FROM $tbn9 WHERE id_provincia like '%$id_provincia%' and nombre_usuario like '%$nombre_usuario%' and correo_usuario like '%$correo_usuario%' and nif like '%$nif%' and tipo_votante like '%$tipo_votante%' ORDER BY 'nombre_usuario' ";
$result = mysqli_query($con, $sql);
?>


                    <!--Comiezo-->
                    <div class="card-header"> <h1 class="card-title"><?= _("CENSO") ?></h1> </div>


                    <div class="card-body">

                    <p>&nbsp;</p>
                    <?php
                    if (isset($borrado)) {
                        echo "$borrado";
                    }
                    ?>
                    <p>&nbsp;</p>

                    <form name="formulario" method="post" action="<?php $_SERVER['PHP_SELF'] ?>">
                        <?php
                        if ($row = mysqli_fetch_array($result)) {
                            ?>

                            <table id="tabla1" class="table table-striped table-bordered dt-responsive nowrap" data-page-length="25">
                                <thead>
                                    <tr>
                                        <th width=10%><?= _("Nombre") ?></th>
                                        <th width=10%><?= _("Apellido") ?></th>
                                        <th width=29%><?= _("Correo") ?></th>
                                        <th width=3%><?= _("Tipo") ?></th>
                                        <th width=3%><?= _("Nif") ?></th>
                                        <?php if ($es_municipal == false) { ?>
                                            <th width=7%><?= _("provincia") ?></th>
                                        <?php } ?>
                                        <th width=8%><?= _("bloqueado") ?></th>
                                        <th width=8%><?= _("modificar") ?><br>
                                            <?= _("datos") ?></th>
                                        <th width=6% align=center><?= _("borrar") ?><br>
                                            <?= _("datos") ?></th>
                                        <th width=6% align=center><?= _("borrado") ?><br />
                                            <?= _("multiple") ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    mysqli_field_seek($result, 0);
                                    do {
                                        ?>
                                        <tr>
                                            <td><?php echo "$row[2]" ?></td>
                                            <td><?php echo "$row[8]" ?></td>
                                            <td><?php echo "$row[3]" ?></td>
                                            <td><?php echo "$row[5]" ?></td>
                                            <td><?php echo "$row[4]" ?></td>
                                            <?php if ($es_municipal == false) { ?>
                                            <td><?php echo "$row[1]" ?></td>
                                            <?php } ?>
                                            <td><?php echo "$row[7]" ?></td>
                                            <td><a href="admin.php?c=<?php echo encrypt_url('admin/censos/id='.$row[0].'&acc=modifika',$clave_encriptacion) ?>" class="btn btn-primary btn-block"><?= _("modificar") ?></a></td>
                                            <td><a href="admin.php?c=<?php echo encrypt_url('admin/censos_borra/id='.$row[0].'&idvot='.$row[2],$clave_encriptacion) ?>" class="btn btn-danger btn-block" onClick="return borravotante()" ><?= _("Borrar") ?> </a></td>
                                            <td class="text-center"><input name="borrar_multiples<?php echo "$row[0]" ?>" type="checkbox" id="borrar_multiples" value="<?php echo "$row[0]" ?>"></td>
                                        </tr>
                                        <?php
                                    } while ($row = mysqli_fetch_array($result));
                                    ?>

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th><?= _("Nombre") ?></th>
                                        <th><?= _("Apellido") ?></th>
                                        <th><?= _("Correo") ?></th>
                                        <th><?= _("Tipo") ?></th>
                                        <th><?= _("Nif") ?></th>
                                        <?php if ($es_municipal == false) { ?>
                                            <th><?= _("provincia") ?></th>
                                        <?php } ?>
                                        <th><?= _("bloqueado") ?></th>
                                        <th><?= _("modificar") ?><br>
                                            <?= _("datos") ?></th>
                                        <th align=center><?= _("borrar") ?><br>
                                            <?= _("datos") ?></th>
                                        <th align=center><?= _("borrado") ?><br />
                                            <?= _("multiple") ?></th>
                                    </tr>
                                </tfoot>

                            </table>

                            <input name="provincia" type="hidden" value="<?php echo "$id_provincia"; ?>">
                            <input name="nombre_usuario" type="hidden" value="<?php echo "$nombre_usuario"; ?>">
                            <input name="correo_electronico" type="hidden" value="<?php echo "$correo_usuario"; ?>">
                            <input name="nif" type="hidden" value="<?php echo "$nif"; ?>">
                            <input name="tipo_usuario" type="hidden" value="<?php echo "$tipo_votante"; ?>">
                            <input name="usuario" type="hidden" value="<?php echo "$usuario"; ?>">
                            <p>&nbsp;</p>
                            <div class="row">
                              <div class="col-sm-8"></div>
                              <div class="col-sm-4">
                                <input name="delete_multi" type="submit" class="btn btn-primary pull-right"   id="delete_multi" value="<?= _("Borrar multiples") ?>" />
                              </div>
                            </div>
                        </form>

                        <?php
                    } else {?>
                      <div class="alert alert-info">
                        <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                        <i class="fa fa-times simple-remove" aria-hidden="true"></i>
                        </button>
                        <span>
                          <b> Info - </b> <?=_("¡No se ha encontrado ningún votante!");?></span>
                        </div>
                        <?php


                    }
                    ?><!---->

</div>

        <script type="text/javascript" src="assets/DataTables/datatables.min.js" ></script>
        <script src="assets/DataTables/Buttons-1.6.2/js/dataTables.buttons.min.js" ></script>
        <script src="assets/DataTables/JSZip-2.5.0/jszip.min.js" ></script>
        <script src="assets/DataTables/pdfmake-0.1.36/pdfmake.min.js" ></script>
        <script src="assets/DataTables/pdfmake-0.1.36/vfs_fonts.js" ></script>
        <script src="assets/DataTables/Buttons-1.6.2/js/buttons.html5.min.js" ></script>

        <script type="text/javascript" language="javascript" class="init">
          $(document).ready(function() {
            $('#tabla1').DataTable({
              responsive: true,
              dom: 'Blfrtip',
              buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
              ],
              lengthMenu: [
                [10, 25, 50, -1],
                [10, 25, 50, "Todos"]
              ],
              language: {
                buttons: {
                  copy: '<?= _("Copiar") ?>',
                  copyTitle: '<?= _("Copiado en el portapapeles") ?>',
                  copyKeys: '<?= _("Presione") ?> <i> ctrl </i> o <i> \ u2318 </i> + <i> C </i> <?= _("para copiar los datos de la tabla a su portapapeles") ?>. <br> <br> <?= _("Para cancelar, haga clic en este mensaje o presione Esc") ?>.',
                  copySuccess: {
                    _: '%d <?= _("lineas copiadas") ?>',
                    1: '1 <?= _("linea copiada") ?>'
                  }
                },
                processing: "<?= _("Tratamiento en curso") ?>...",
                search: "<?= _("Buscar") ?>:",
                lengthMenu: "<?= _("Ver") ?> _MENU_  <?= _("resultados por pagina") ?>",
                info: "<?= _("Mostrando") ?> _PAGE_ <?= _("de") ?> _PAGES_ <?= _("paginas") ?>",
                infoEmpty: "<?= _("No se han encitrado resultados") ?>",
                infoFiltered: "(<?= _("filtered from") ?> _MAX_ <?= _("total records") ?>)",
                infoPostFix: "",
                loadingRecords: "<?= _("Cargando") ?>...",
                zeroRecords: "<?= _("No se han encontrado resultados - perdone") ?>",
                emptyTable: "<?= _("No se han encontrado resultados - perdone") ?>",
                paginate: {
                  first: "<?= _("Primero") ?>",
                  previous: "<?= _("Anterior") ?>",
                  next: "<?= _("Siguiente") ?>",
                  last: "<?= _("Ultimo") ?>"
                },
                aria: {
                  sortAscending: ": <?= _("activar para ordenar columna de forma ascendente") ?>",
                  sortDescending: ": <?= _("activar para ordenar columna de forma descendente") ?>"
                }
              }
            });
          });
        </script>
        <!--end datatables -->
        <script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js" ></script>
        <script src="https://cdn.datatables.net/responsive/2.2.7/js/responsive.bootstrap4.min.js" ></script>
        <script src="assets/js/admin_borrarevento.js" ></script>
<?php } ?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###                                                         ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo con la barra de menú de la zona de administración
*/
if ($_SESSION['nivel_usu'] != "1") {
    ?>

    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">

      <div class="container">

        <a class="navbar-brand" href="#"><?php echo "$nombre_web"; ?></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>


        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav ml-auto">

              <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><?= _("Votaciones") ?></a>
                  <div class="dropdown-menu">
                    <a class="dropdown-item"  href="admin.php?c=<?php echo encrypt_url('admin/votacion/s=SD',$clave_encriptacion); ?>" ><?= _("Crear votación") ?></a>
                    <a class="dropdown-item"  href="admin.php?c=<?php echo encrypt_url('admin/gestion_zonas/s=SD',$clave_encriptacion); ?>"><?= _("Gestión  de votaciones") ?></a>
                    <a class="dropdown-item"  href="admin.php?c=<?php echo encrypt_url('admin/gestion_votaciones_mis/s=SD',$clave_encriptacion); ?>"><?= _("Gestion MIS votaciones") ?> <b class="glyphicon glyphicon-user"></b></a>
                  </div>
                </li>

                <?php if ($_SESSION['usuario_nivel'] <= "2") { ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><?= _("Censos") ?></a>
                    <div class="dropdown-menu">

                      <a class="dropdown-item"  href="admin.php?c=<?php echo encrypt_url('admin/censos_buscador/s=SD',$clave_encriptacion); ?>"><?= _("Buscar / modificar votantes") ?></a>
                      <a class="dropdown-item" href="admin.php?c=<?php echo encrypt_url('admin/censos/s=SD',$clave_encriptacion); ?>"><?= _("Incluir un votante") ?></a>
                      <a class="dropdown-item" href="admin.php?c=<?php echo encrypt_url('admin/censos_add_mas/s=SD',$clave_encriptacion); ?>"><?= _("Añadir votantes de foma masiva") ?></a>

                      <div class="dropdown-divider"><?= _("Bajas -modificaciones") ?></div>

                      <a class="dropdown-item" href="admin.php?c=<?php echo encrypt_url('admin/bloquear_censos_buscador/s=SD',$clave_encriptacion); ?>"><?= _("Bloquear / desbloquear votantes") ?></a>
                      <?php if ($_SESSION['usuario_nivel'] <= "1") { ?>
                          <a class="dropdown-item" href="admin.php?c=<?php echo encrypt_url('admin/censos_out_mas/s=SD',$clave_encriptacion); ?>"><?= _("Bajas-Modificar votantes masiva") ?></a>
                      <?php } ?>
                      <?php if ($es_municipal == false) { ?>

                          <?php if ($_SESSION['usuario_nivel'] <= "1") { ?>
                            <a class="dropdown-item" href="admin.php?c=<?php echo encrypt_url('admin/poblaciones_consultar/s=SD',$clave_encriptacion); ?>"><?= _("Buscar municipios") ?></a>
                          <?php } ?>
                      <?php } ?>
                      <?php if ($insti == true) { ?>
                          <a class="dropdown-item" href="admin.php?c=<?php echo encrypt_url('admin/censos_block_insti/s=SD',$clave_encriptacion); ?>"><?= _("Bloquear correos de forma masiva") ?></a>
                      <?php } ?>

                    </div>
                  </li>

                <?php } ?>

        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
              <?= _("Grupos trabajo") ?>
                <?php
                $sql_cont = "SELECT a.ID  FROM $tbn9 a,$tbn6 b where (a.ID= b.id_usuario) and b.estado = 0 ";
                $result_cont = mysqli_query($con, $sql_cont);
                $quants_cont = mysqli_num_rows($result_cont);
                if ($quants_cont == 0) {
                } else {
                    ?>
                    <span class="badge badge-danger"> <?php echo "$quants_cont"; ?></span>
                <?php }
                ?>
                </a>
            <div class="dropdown-menu">
              <?php if ($_SESSION['usuario_nivel'] <= "6") { ?>
                  <a  class="dropdown-item" href="admin.php?c=<?php echo encrypt_url('admin/asambleas/s=SD',$clave_encriptacion); ?>" class="menu"><?= _("Crear grupos de trabajo") ?></a>
                  <?php } ?>
              <?php if ($_SESSION['usuario_nivel'] <= "5") { ?>
                  <a  class="dropdown-item" href="admin.php?c=<?php echo encrypt_url('admin/asambleas_list/s=SD',$clave_encriptacion); ?>" class="menu"><?= _("Modificar grupos de trabajo") ?></a>
                  <?php } ?>
              <a  class="dropdown-item" href="admin.php?c=<?php echo encrypt_url('admin/mis_grupos_list/s=SD',$clave_encriptacion); ?>" class="menu"><?= _("Gestionar usuarios") ?> <?php  if ($quants_cont!=0){ ?><span class="badge badge-danger"> <?php echo "$quants_cont"; ?></spam><?php } ?></a>
                </div>
              </li>


<?php if ($_SESSION['usuario_nivel'] <= "2") { ?>
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><?= _("Gestion administracion") ?><b class="caret"></b></a>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="admin.php?c=<?php echo encrypt_url('admin/usuarios/s=SD',$clave_encriptacion); ?>"><?= _("Gestion usuarios administracion") ?></a>
                <a class="dropdown-item" href="admin.php?c=<?php echo encrypt_url('admin/usuarios_gestion_mis/s=SD',$clave_encriptacion); ?>" ><?= _("Votaciones por usuario") ?></a>

                <div class="dropdown-divider"></div>
                <?php if ($es_municipal == false) { ?>
              <a  class="dropdown-item" href="admin.php?c=<?php echo encrypt_url('admin/gestion_provincias/s=SD',$clave_encriptacion); ?>"  ><?= _("Gestion notificaciones provincias") ?></a>
            <?php } ?>

            <?php if ($_SESSION['usuario_nivel'] == "0") { // configuración de la web ?>
                <a  class="dropdown-item" href="admin.php?c=<?php echo encrypt_url('admin/dump_acceso/s=SD',$clave_encriptacion); ?>"><?= _("Copia de seguridad de la base de datos") ?></a>
                <a  class="dropdown-item" href="admin.php?c=<?php echo encrypt_url('admin/constantes/s=SD',$clave_encriptacion); ?>"  ><?= _("Configuración variables de la web") ?></a>
                <a  class="dropdown-item" href="admin.php?c=<?php echo encrypt_url('admin/constantes_correo/s=SD',$clave_encriptacion); ?>"  ><?= _("Configuración del correo electronico") ?></a>
                <a  class="dropdown-item" href="admin.php?c=<?php echo encrypt_url('admin/constantes_tipo_user/s=SD',$clave_encriptacion); ?>"  ><?= _("Configuración tipo usuario y ponderación voto") ?></a>

            <?php } // fin configuracion de la web ?>

              </div>
            </li>
            <?php } ?>
            <?php if($_SESSION['admin_blog']==1){ // zona de la administración del blog?>
              <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><?= _("Gestion BLOG") ?><b class="caret"></b></a>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="admin.php?c=<?php echo encrypt_url('admin/blog_gestionar_contenidos/s=SD',$clave_encriptacion); ?>"><?= _("Gestión contenidos") ?></a>
                  <!--  <a class="dropdown-item" href="admin.php?c=<?php echo encrypt_url('admin/blog_categorias/s=SD',$clave_encriptacion); ?>" ><?= _("Gestionar categorias") ?></a>-->

                    <div class="dropdown-divider"></div>

                  <a  class="dropdown-item" href="admin.php?c=<?php echo encrypt_url('admin/blog_gestionar/s=SD',$clave_encriptacion); ?>"  ><?= _("Gestionar módulos") ?> / <?= _("Aspecto") ?></a>
                  <a class="dropdown-item" href="admin.php?c=<?php echo encrypt_url('admin/blog_menu_busq/activo=1',$clave_encriptacion); ?>" ><?= _("Gestionar menú") ?></a>
                  <a class="dropdown-item" href="admin.php?c=<?php echo encrypt_url('admin/blog_cabecera_list/idcab=1',$clave_encriptacion); ?>" ><?= _("Gestionar cabecera") ?></a>
                  </div>
                </li>
              <?php
            }       // fin admin blog
            ?>

            <li class="nav-item">
            <!--  <a href="javascript:void(0);" data-href="https://demokratian.org/demokratian_4/manual.php" class="openmodalAdmin nav-link"><?= _("Ayuda") ?></a>-->
            <a href="https://demokratian.org/demokratian_4/manual.php" target="_blank" class="nav-link"><?= _("Ayuda") ?></a>
            </li>
            <?php include('../private/inc_web/version.php'); ?>
          </ul>
        </div>
      </div>
    </nav>

    <?php
}?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que  modifica los candidatos u opciones que se pueden votar
* @todo fusionar página con candidatos.php para que solo existe una
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 9;
include('../private/inc_web/nivel_acceso.php');

$id = fn_filtro_numerico($con, $variables['id']);
$idvot = fn_filtro_numerico($con, $variables['idvot']);

$ids_provincia = $_SESSION['localidad'];
$nombre_usuario = $_SESSION['ID'];



$fecha = date("Y-m-d H:i:s");
$fecha_ver = date("d-m-Y ");


if (ISSET($_POST["modifika_paginas"])) {

    $nombre_cand = fn_filtro($con, $_POST['nombre_cand']);
    $texto = fn_filtro_editor($con, $_POST['texto']);
    //$provincia = fn_filtro($con, $_POST['provincia']);
    $sexo = fn_filtro($con, $_POST['sexo']);


    $sSQL = "UPDATE $tbn7 SET nombre_usuario=\"$nombre_cand\",texto=\"$texto\",  sexo=\"$sexo\" ,fecha_modif=\"$fecha\",  modif=\"$nombre_usuario\" WHERE id='$id'";

    mysqli_query($con, $sSQL) or die("Imposible modificar pagina");

    $texto1 = "<div class=\"alert alert-success\"> " . _("Realizadas las Modificaciones") . " <br>" . _("Asi ha quedado el candidato") . " \" " . $nombre_cand . " \"</div>";
}
?>

<div class="card-header"> <h1 class="card-title"><?= _("MODIFICAR OPCION O CANDIDATO") ?></h1> </div>


<div class="card-body">


                    <?php
                    $result = mysqli_query($con, "SELECT * FROM $tbn7 where id=$id");
                    $row = mysqli_fetch_row($result);
                    ?>
                    <!--Comiezo-->
                    <div class="row">

                    <div class="col-sm-8">
                      <a href="javascript:void(0);" data-href="aux_vota.php?c=<?php echo encrypt_url('votacion/perfil/idgr='.$id,$clave_encriptacion) ?>" class="opennormalModal btn btn-success" title="<?php echo "$row[3]"; ?>" ><?= _("Vista previa") ?></a>
                    </div>
                        <div class="col-sm-4">
                    <a href="admin.php?c=<?php echo encrypt_url('admin/candidatos_busq1/idvot='.$idvot,$clave_encriptacion) ?>" class="btn btn-primary btn-block"><?= _("Buscar en el directorio para modificar o borrar candiatos en esta encuesta") ?></a>
                    <a href="admin.php?c=<?php echo encrypt_url('admin/candidatos/idvot='.$idvot,$clave_encriptacion) ?>" class="btn btn-primary btn-block"><?= _("Añadir otra opcion o candidato en esta votación") ?></a>

                    </div>
                  </div>

                    <!---->



                    <?php if (isset($texto1) ){
                        echo"$texto1"; }?>
                        <div class="separador"></div>
                        <div class="separador"></div>

                    <form action="<?php $_SERVER['PHP_SELF'] ?>" method=post   name="frmDatos" id="frmDatos"  class="well form-horizontal">


                        <div class="form-group row">
                            <label for="nombre" class="col-sm-3 control-label"><?= _("Nombre") ?> </label>

                            <div class="col-sm-9">
                                <input name="nombre_cand" type="text"  id="nombre_cand" value="<?php echo "$row[3]"; ?>"  class="form-control"  required autofocus />
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="nombre" class="col-sm-3 control-label"><?= _("Sexo") ?></label>

                            <div class="col-sm-9">

                                <?php
                                $chekeado1 = "";
                                $chekeado2 = "";
                                $chekeado3 = "";
                                if ($row[4] == "H") {
                                    $chekeado1 = "checked=\"checked\" ";
                                } else if ($row[4] == "M") {
                                    $chekeado2 = "checked=\"checked\" ";
                                } else {

                                    $chekeado3 = "checked=\"checked\" ";
                                }
                                ?>

                                <input type="radio" name="sexo" value="O" id="sexo_2" <?php echo "$chekeado3"; ?> />
                                <label> <?= _("Neutro (sin opcion de sexo)") ?></label>
                                <span class="label label-warning"> <?= _("¡¡¡ojo, SI ES UNA VOTACIÓN DE PRIMARIAS HAY QUE INDCAR SEXO!!!") ?></span>
                                <br/>
                                <label>
                                    <input name="sexo" type="radio" id="sexo_0" value="H"  <?php echo "$chekeado1"; ?> />
                                    <?= _("Hombre") ?></label>
                                <br />
                                <label>
                                    <input type="radio" name="sexo" value="M" id="sexo_1" <?php echo "$chekeado2"; ?> />
                                    <?= _("Mujer") ?></label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="nombre" class="col-sm-3 control-label"><?= _("Imagen") ?> </label>

                            <div class="col-sm-9">
                                <?php if ($row[12] != "") {
                                  ?>
                                  <img src="<?php echo $upload_cat; ?>/<?php echo"$row[12]"; ?>" alt="<?php echo"$row[3]"; ?> " width="150" height="150" class="img-circle" />
                                 <?php } ?>
                                  <a href="admin.php?c=<?php echo encrypt_url('admin/candidatos_crop/idcat='.$row[0].'&idvot='.$idvot,$clave_encriptacion) ?>" title="Imagen de <?php echo "$row[3]" ?>" class="btn btn-success" ><?= _("Modificar imagen") ?></a>

                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label for="texto" ><?= _("Texto") ?></label>


                                <textarea cols="80" id="texto" name="texto" rows="10"><?php echo "$row[2]"; ?></textarea>
                                <!-- ckeditor -->
                                <script type="text/javascript" src="assets/ckeditor/ckeditor.js"></script>

                                <script>
                                 CKEDITOR.replace( 'texto', {
                                  height: 250,

                                  extraPlugins: 'imgbrowse',
                                  filebrowserImageBrowseUrl: '/assets/ckeditor/plugins/imgbrowse/imgbrowse.html?imgroot=<?php echo $baseUrl; ?>',
                                  <?php if($_SESSION['usuario_nivel']<=6){     // nivel para poder añadir imagenes  ?>
                                  filebrowserImageUploadUrl: 'aux_vota.php?c=<?php echo encrypt_url('basicos_php/imageUpload/conec=OK',$clave_encriptacion) ?>',
                                  <?php } ?>
                                //  filebrowserUploadUrl: "upload.php",

                                 });
                                </script>
                            </div>
                        </div>
                        <div class="separador"></div>
                        <div class="row">
                            <div class="col-sm-12">
                        <input name="modifika_paginas" type="submit" class="btn btn-primary btn-block" id="modifika_paginas" value="<?= _("Modificar candidato") ?>" />                   </td>
                      </div>
                    </div>
                    </form>
</div>

<?php } ?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###                                                         ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que lista los votantes con las características del formularios usuarios_gestion.php y permite modificar, asignar asambleas, o provincias, etc
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 2;
include('../private/inc_web/nivel_acceso.php');

?>

                    <!--Comiezo-->
                    <div class="card-header"> <h1 class="card-title"><?= _("Votantes existentes con las caracteristicas seleccionadas") ?></h1> </div>


                    <div class="card-body">

                    <?php
                    $id_provincia = fn_filtro($con, $_POST['provincia']);
                    $nombre_usuario = fn_filtro($con, $_POST['nombre_usuario']);
                    $apellidos_usuario = fn_filtro($con, $_POST['apellidos_usuario']);
                    $correo_usuario = fn_filtro($con, $_POST['correo_electronico']);
                    $nif = fn_filtro($con, $_POST['nif']);
                    $tipo_votante = fn_filtro($con, $_POST['tipo_usuario']);
                    //$usuario = fn_filtro($con, $_POST['usuario']);
                    $nivel_admin = fn_filtro($con, $_POST['nivel_admin']);



                   $sql = "SELECT ID, id_provincia,  nombre_usuario, correo_usuario, nif, tipo_votante, usuario,bloqueo, nivel_usuario,	nivel_acceso,apellido_usuario,admin_blog FROM $tbn9
			WHERE id_provincia like '%$id_provincia%' and nombre_usuario like '%$nombre_usuario%' and correo_usuario like '%$correo_usuario%' and nif like '%$nif%' and tipo_votante like '%$tipo_votante%' and nivel_usuario like '%$nivel_admin%' and apellido_usuario like '%$apellidos_usuario%'  ORDER BY 'nombre_usuario' ";


                    $result = mysqli_query($con, $sql);
                    if ($row = mysqli_fetch_array($result)) {
                        ?>


                        <table id="tabla1" class="table table-striped table-bordered dt-responsive nowrap" data-page-length="25">
                            <thead>
                                <tr>
                                    <th width="5%">&nbsp;</th>
                                    <th width="12%"><?= _("Nombre") ?></th>
                                    <th width="20%"><?= _("Apellidos") ?></th>
                                    <th width="15%"><?= _("Correo") ?></th>
                                    <?php if ($es_municipal == false) { ?>
                                        <th width="11%"><?= _("Provincia") ?></th>
                                    <?php } ?>
                                    <th width="9%"><?= _("Nivel Acceso") ?></th>

                                    <th width="9%"><?= _("Tipo usuario") ?></th>
                                    <th width="11%">&nbsp;</th>
                                    <th width="11%">&nbsp;</th>
                                </tr>
                            </thead>

                            <tbody>

                                <?php
                                mysqli_field_seek($result, 0);

                                do {
                                    ?>
                                    <tr>
                                        <td><a href="javascript:void(0);" data-href="aux_modal.php?c=<?php echo encrypt_url('admin/usuario_datos/id='.$row[0],$clave_encriptacion) ?>" class="openmodalAdmin " title="<?php echo "$row[3]" ?>" ><span class="fa fa-user fa-lg  text-success" aria-hidden="true"></span></a></td>
                                        <td><?php echo "$row[2]" ?></td>
                                        <td><?php echo "$row[10]" ?></td>
                                        <td><?php echo "$row[3]" ?> </td>
                                        <?php if ($es_municipal == false) { ?>
                                            <td><?php echo "$row[1]" ?></td>
                                        <?php } ?>
                                        <td><?php echo "$row[9]" ?></td>
                                        <td><?php
                                            $enlace_asigna = "";
                                            if ($row[8] == "2") {
                                                echo _("Administrador");
                                                $enlace_asigna = "NO";
                                            } else if ($row[8] == "3") {
                                                echo _("Administrador") . " " . _("CCAA");
                                                // $enlace_asigna="<a href=\"usuarios_asigna.php?id=".$row[0]."\" class=\"fotos\" title=\" ".$row[3]." \" rel=\"gb_page_center[760, 620]\>Asignar grupos</a>";
                                                $enlace_asigna = "NO";
                                            } else if ($row[8] == "4") {
                                                echo _("Administrador") . " " . _("provincia");
                                                $enlace_asigna = _("Asignar provincias o grupos");
                                            } else if ($row[8] == "6") {
                                                echo _("Administrador") . " " . _("Grupos");
                                                if ($es_municipal == false) {
                                                    echo " " . _("estatales");
                                                }
                                                $enlace_asigna1 = _("Asignar grupos");
                                                if ($es_municipal == false) {
                                                    $enlace_asigna2 = " " . _("Estatales");
                                                    $enlace_asigna = $enlace_asigna1 . $enlace_asigna2;
                                                } else {
                                                    $enlace_asigna = $enlace_asigna1;
                                                }
                                            } else if ($row[8] == "5") {
                                                echo _("Administrador") . " " . _("grupos provinciales");
                                                $enlace_asigna = _("Asignar provincias o grupos");
                                            } else if ($row[8] == "7") {
                                                echo _("Administrador") . " " . _("grupos autonomicos");
                                                $enlace_asigna = _("Asignar grupos de trabajo");
                                            } else {
                                                echo _("Votante");
                                                $enlace_asigna = "NO";
                                            }

                                            if ($row[11] == 1){
                                              echo '<p class="text-info"> Admin blog </p>';
                                            }
                                            ?>
                                            </td>
                                        <td class="text-center">
                                            <?php
                                            if ($enlace_asigna == "NO") {
                                                echo "--";
                                            } else {
                                                ?>
                                                <a href="javascript:void(0);" data-href="aux_modal.php?c=<?php echo encrypt_url('admin/usuarios_asigna/id='.$row[0],$clave_encriptacion) ?>" class="openmodalAdmin btn btn-primary btn-sm"><?php echo "$enlace_asigna"; ?></a><?php }
                                            ?></td>
                                        <td class="text-center">

                                            <a href="admin.php?c=<?php echo encrypt_url('admin/usuarios_actualizar/id='.$row[0],$clave_encriptacion) ?>" class="btn btn-primary btn-sm" ><?= _("modificar") ?></a></td>
                                    </tr>

                                    <?php
                                } while ($row = mysqli_fetch_array($result));
                                ?>
                            </tbody>
                        </table>
                        <?php
                    } else {?>
                      <div class="alert alert-info">
                        <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                        <i class="fa fa-times simple-remove" aria-hidden="true"></i>
                        </button>
                        <span>
                          <b> Info - </b> <?=_("¡No hay resultados con estos criterios de busqueda!");?></span>
                        </div>
                        <?php
                    }
                    ?>

</div>
                    <!--Final-->


        <script type="text/javascript" src="assets/DataTables/datatables.min.js" ></script>
        <script src="assets/DataTables/Buttons-1.6.2/js/dataTables.buttons.min.js" ></script>
        <script src="assets/DataTables/JSZip-2.5.0/jszip.min.js" ></script>
        <script src="assets/DataTables/pdfmake-0.1.36/pdfmake.min.js" ></script>
        <script src="assets/DataTables/pdfmake-0.1.36/vfs_fonts.js" ></script>
        <script src="assets/DataTables/Buttons-1.6.2/js/buttons.html5.min.js" ></script>

        <script type="text/javascript" language="javascript" class="init">
          $(document).ready(function() {
            $('#tabla1').DataTable({
              responsive: true,
              dom: 'Blfrtip',
              buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
              ],
              lengthMenu: [
                [10, 25, 50, -1],
                [10, 25, 50, "Todos"]
              ],
              language: {
                buttons: {
                  copy: '<?= _("Copiar") ?>',
                  copyTitle: '<?= _("Copiado en el portapapeles") ?>',
                  copyKeys: '<?= _("Presione") ?> <i> ctrl </i> o <i> \ u2318 </i> + <i> C </i> <?= _("para copiar los datos de la tabla a su portapapeles") ?>. <br> <br> <?= _("Para cancelar, haga clic en este mensaje o presione Esc") ?>.',
                  copySuccess: {
                    _: '%d <?= _("lineas copiadas") ?>',
                    1: '1 <?= _("linea copiada") ?>'
                  }
                },
                processing: "<?= _("Tratamiento en curso") ?>...",
                search: "<?= _("Buscar") ?>:",
                lengthMenu: "<?= _("Ver") ?> _MENU_  <?= _("resultados por pagina") ?>",
                info: "<?= _("Mostrando") ?> _PAGE_ <?= _("de") ?> _PAGES_ <?= _("paginas") ?>",
                infoEmpty: "<?= _("No se han encitrado resultados") ?>",
                infoFiltered: "(<?= _("filtered from") ?> _MAX_ <?= _("total records") ?>)",
                infoPostFix: "",
                loadingRecords: "<?= _("Cargando") ?>...",
                zeroRecords: "<?= _("No se han encontrado resultados - perdone") ?>",
                emptyTable: "<?= _("No se han encontrado resultados - perdone") ?>",
                paginate: {
                  first: "<?= _("Primero") ?>",
                  previous: "<?= _("Anterior") ?>",
                  next: "<?= _("Siguiente") ?>",
                  last: "<?= _("Ultimo") ?>"
                },
                aria: {
                  sortAscending: ": <?= _("activar para ordenar columna de forma ascendente") ?>",
                  sortDescending: ": <?= _("activar para ordenar columna de forma descendente") ?>"
                }
              }
            });
          });
        </script>
        <script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js" ></script>
        <script src="https://cdn.datatables.net/responsive/2.2.7/js/responsive.bootstrap4.min.js" ></script>
        <!--end datatables -->
        <script src="assets/js/admin_borrarevento.js" ></script>

<?php } ?>

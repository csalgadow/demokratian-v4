<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo para borrar un votante, borra también los datos relacionados
* @todo ver si resulta conveniente borrar todos los datos que borramos o hay alguno que es conveniente no borrarlo como las votaciones en las que ha participado que puede generar falta de consistencia de datos
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 1;
include('../private/inc_web/nivel_acceso.php');


$id = fn_filtro_numerico($con, $variables['id']);
$mens_ok = "";
$mens_error = "";
?>

                    <!--Comiezo-->
                    <div class="card-header"> <h1 class="card-title"><?= _("BORRARDO AFILIADOS/SIMPATIZANTE") ?></h1> </div>


                    <div class="card-body">

                    <p>&nbsp;</p>
                    <!---->
                    <?php
                    $result = mysqli_query($con, "SELECT ID, id_provincia, nombre_usuario, apellido_usuario, nivel_usuario, nivel_acceso, correo_usuario, nif, 	id_ccaa, pass, tipo_votante ,usuario,	bloqueo, razon_bloqueo,imagen_pequena  FROM $tbn9 where id=$id");
                    $row = mysqli_fetch_row($result);
                    ?>  <form  class="well form-horizontal" >


                        <div class="form-group">
                            <label for="nombre" class="col-sm-3 control-label"><?= _("Nombre y Apellidos") ?></label>

                            <div class="col-sm-9">
                                <?php echo "$row[2]"; ?>
                            </div></div>

                        <div class="form-group">
                            <label for="correo" class="col-sm-3 control-label"><?= _("Correo electronico") ?></label>

                            <div class="col-sm-9"> <?php echo "$row[6]"; ?>
                            </div></div>

                        <div class="form-group">
                            <label for="nombre" class="col-sm-3 control-label"> <?= _("Nif") ?> </label>

                            <div class="col-sm-4">    <?php echo "$row[7]"; ?>
                            </div></div>


                        <div class="form-group">
                            <label for="nombre" class="col-sm-3 control-label"><?= _("provincia") ?>: </label>

                            <div class="col-sm-9">

                                <?php
                                $options = "select DISTINCT id, provincia from $tbn8  order by ID";
                                $resulta = mysqli_query($con, $options) or die("error: " . mysqli_error($con));

                                while ($listrows = mysqli_fetch_array($resulta)) {
                                    $id_pro = $listrows['id'];
                                    $name1 = $listrows['provincia'];

                                    if ($id_pro == $row[1]) {
                                        echo "$row[1]";
                                    }
                                }
                                ?>

                            </div></div>


                        <div class="form-group">
                            <label for="nombre" class="col-sm-3 control-label"><?= _("Tipo") ?> </label>

                            <div class="col-sm-9">

                                <?php
                                if ($row[10] == 1) {
                                    echo _("socio");
                                } else if ($row[10] == 2) {
                                    echo _("simpatizante verificado");
                                } else {
                                    echo _("simpatizante");
                                }
                                ?>

                            </div></div>



                        <div class="form-group">
                            <label for="nombre" class="col-sm-3 control-label"><?= _("Bloqueado") ?> </label>

                            <div class="col-sm-9">


                                <?php
                                if ($row[12] == "si") {
                                    echo _("SI");
                                } else {
                                    echo _("NO");
                                }
                                ?>


                            </div></div>
                        <div class="form-group">
                            <label for="nombre" class="col-sm-3 control-label"><?= _("Razón Bloqueo") ?> </label>

                            <div class="col-sm-9"><?php echo "$row[13]"; ?>
                            </div></div>
                        <p>&nbsp;</p>
                    </form>



                    <?php
                    ///borramos la lista de votaciones donde ha participado
                    $borrado_usuario_x_votacion = "DELETE FROM $tbn2 WHERE id_votante=" . $id . " ";
                    $mens = _("ERROR en el borrado de la tabla") . " usuario_x_votacion";
                    $result_usuario_x_votacion = db_query($con, $borrado_usuario_x_votacion, $mens);

                    if ($result_usuario_x_votacion == false) {
                        $mens_error .= $mens . "<br/>";
                    } else {
                        $mens_ok .= "Borrados la relacion de  votaciones donde ha participado, ojo, los votos no se pueden borrar<br/>";


                        //borramos debate_comentario con esta id
                        $borrado_debate_comentario = "DELETE FROM $tbn12 WHERE id_usuario=" . $id . " ";
                        $mens = _("ERROR en el borrado de la tabla") . " debate comentario";
                        $result_borrado_debate_comentario = db_query($con, $borrado_debate_comentario, $mens);


                        if ($result_borrado_debate_comentario == false) {
                            $mens_error .= $mens . "<br/>";
                        } else {
                            $mens_ok .= "Borrados los comentarios de debate si existian <br/>";

                            //borramos debate_votos con esta id
                            $borrado_debate_votos = "DELETE FROM $tbn14 WHERE id_votante=" . $id . "";
                            $mens = _("ERROR en el borrado de la tabla") . " debate votos";
                            $result_borrado_debate_votos = db_query($con, $borrado_debate_votos, $mens);

                            if ($result_borrado_debate_votos == false) {
                                $mens_error .= $mens . "<br/>";
                            } else {
                                $mens_ok .= "Borrados los votos de los debates si existian <br/>";


                                //borramos la relacion entre votantes y modifciaciones de los administradores
                                $borrado_votantes_x_admin = "DELETE FROM $tbn17 WHERE id_votante=" . $id . " ";
                                $mens = _("ERROR en el borrado de la tabla") . " votantes_x_admin";
                                $result_votantes_x_admin = db_query($con, $borrado_votantes_x_admin, $mens);
                                if ($result_votantes_x_admin == false) {
                                    $mens_error .= $mens . "<br/>";
                                } else {
                                    $mens_ok .= "Borradala relación de administración si existia<br/>";



                                    //borramos si es administrador, la relacion de provincias que pueda tener asignadas
                                    $borrado_usuario_admin_x_provincia = "DELETE FROM $tbn5 WHERE id_usuario=" . $id . " ";
                                    $mens = _("ERROR en el borrado de la tabla") . "usuario_admin_x_provincia";
                                    $result_usuario_admin_x_provincia = db_query($con, $borrado_usuario_admin_x_provincia, $mens);

                                    if ($result_usuario_admin_x_provincia == false) {
                                        $mens_error .= $mens . "<br/>";
                                    } else {
                                        $mens_ok .= "Borradas las provincias asignadas si las tenia <br/>";

                                        //borramos si es administrador, la relacion de grupos de trabajo que pueda tener asignadas
                                        $borrado_usuario_x_g_trabajo = "DELETE FROM $tbn6 WHERE id_usuario=" . $id . " ";
                                        $mens = _("ERROR en el borrado de la tabla") . " usuario_x_g_trabajo";
                                        $result_usuario_x_g_trabajo = db_query($con, $borrado_usuario_x_g_trabajo, $mens);
                                        if ($result_usuario_x_g_trabajo == false) {
                                            $mens_error .= $mens . "<br/>";
                                        } else {
                                            $mens_ok .= "Borrados los grupos de trabajo si los tenia <br/>";

                                            //miramos que imagen tiene el usuario para borrarla
                                            $sql_img = "SELECT id,imagen_pequena FROM $tbn9 WHERE ID=" . $id . " ";
                                            $result_img = mysqli_query($con, $sql_img);
                                            $row_img = mysqli_fetch_row($result_img);

                                            if ($row_img[1] != "peq_usuario.jpg") {
                                                $thumb_photo_exists = $upload_user . "/" . $row_img[1];
                                                if (file_exists($thumb_photo_exists)) {
                                                    unlink($thumb_photo_exists);
                                                }
                                            }
                                            //borramos el usuario
                                            $borrado = "DELETE FROM $tbn9 WHERE ID=" . $id . "";
                                            $mens = _("ERROR en el borrado del votante");
                                            $result = db_query($con, $borrado, $mens);
                                            if (!$result) {
                                                $mens_error .= $mens . "<br/>";
                                            } else {
                                                $mens_ok .= _("Borrado el votante") . " <br/>";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if ($mens_error != "") {
                        echo "<div class=\"alert alert-danger\">" . _("HAY UN ERROR, no se ha completado el proceso de borrado") . "</div><div class=\"alert alert-danger\">" . $mens_error . "</div>";
                    }
                    if ($mens_ok != "") {
                        echo "<div class=\"alert alert-success\">" . $mens_ok . "</div>";
                    }
                    ?>
                    <p>&nbsp;</p>

</div>
                    <!--Final-->


<?php } ?>

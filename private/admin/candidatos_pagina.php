<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que crea y modifica la página de candidatos u opciones que se pueden votar para que se visualicen en el blog (parte externa de la web)
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
$nivel_acceso = 9;
include('../private/inc_web/nivel_acceso.php');


$idvot = fn_filtro($con, $variables['idvot']);
if (isset($variables['est'])) {
    $est = fn_filtro($con, $variables['est']);
} else {
    $est = "";
}
$fecha = date("Y-m-d h:i:s");

$situacion = "";

if (ISSET($_POST["add_pagina"])) {


    $estado = fn_filtro($con, $_POST['estado']);
    $presentacion = fn_filtro($con, $_POST['presentacion']);
    $aux = fn_filtro($con, $_POST['aux1']);
    $diseno = fn_filtro($con, $_POST['diseno']);


        $insql = "insert into $tbn22 (id_votacion,activo, presentacion,  aux, diseno) values ( \"$idvot\", \"$estado\",  \"$presentacion\",   \"$aux\", \"$diseno\")";
        $mens = _("Error al añadir una pagina de candidatos externa");
        $result = db_query($con, $insql, $mens);

        if (!$result) {
            $inmsg = "<div class=\"alert alert-warning\">" . _("Hay un error al incluir los datos en la BBDD") . "</div>";
        } else {
            $inmsg = "<div class=\"alert alert-success\"> " . _("Añadida la pagina externa  a la base de datos") . " </div>";
        }
}

if (ISSET($_POST["modifika_pagina"])) {
    $id_pag = fn_filtro($con, $_POST['id_pag']);
    $estado = fn_filtro($con, $_POST['estado']);
    $presentacion = fn_filtro($con, $_POST['presentacion']);
    $aux = fn_filtro($con, $_POST['aux1']);
    $diseno = fn_filtro($con, $_POST['diseno']);


        $sSQL = "UPDATE $tbn22 SET activo=\"$estado\",presentacion=\"$presentacion\" ,aux=\"$aux\",diseno=\"$diseno\" WHERE id=$id_pag";
        $mens = _("Error al añadir una pagina de candidatos externa");
        $result = db_query($con, $sSQL, $mens);

        if (!$result) {
            $inmsg = "<div class=\"alert alert-warning\"> " . _("Hay un error al actualizar la base de datos") . " </div>";
        } else {
            $inmsg = "<div class=\"alert alert-success\"> " . _("Realizadas las Modificaciones") . " </div>";
    }
}


$result_vot = mysqli_query($con, "SELECT id,activo,presentacion, cabecera, imagen_cab,aux,diseno  FROM $tbn22  where id_votacion=$idvot");
//$row_vot = mysqli_fetch_row($result_vot);
$quants = mysqli_num_rows($result_vot);
//miramos si hay resultados y el usuario pude votar en este tipo de votaion y si esta inscrito
if ($quants == 1) {
    $row = mysqli_fetch_array($result_vot);
    $situacion = "modifica";
    $est = "modca";
} else if ($quants > 1) {
    $mensaje_error = _("Hay un error de algun tipo en la base de datos");
}


  $result_nom = mysqli_query($con, "SELECT nombre_votacion FROM $tbn1 where id=$idvot");
  $row_nom = mysqli_fetch_row($result_nom);
?>
<!--Comiezo-->

                <div class="card-header"> <h1 class="card-title"><?php if ($est == "modca") { ?><?= _("MODIFICAR") ?> <?php } else { ?><?= _("GENERAR") ?> <?php } ?> <?= _("PAGINA EXTERNA DE CANDIDATOS U OPCIONES") ?></h1> </div>


                <div class="card-body">

                    <h2><?php echo $row_nom[0]; ?></h2>
                    <p>&nbsp;</p>


                    <?php
                    if (isset($inmsg)) {
                        echo "$inmsg";
                    }
                    ?>
                    <?php
                    if (isset($msg)) {
                        echo "$msg";
                    }
                    ?>
                    <form action="admin.php?c=<?php echo encrypt_url('admin/candidatos_pagina/idvot='.$idvot.'&est=modca',$clave_encriptacion) ?>" enctype="multipart/form-data" method=post class="well form-horizontal" id="form1">

                        <?php if ($est == "modca") { ?>
                            <div class="form-group row">
                                <label for="presentacion" class="col-sm-3 control-label"><?= _("Enlace externo a la pagina") ?></label>
                                <div class="col-sm-7">
                                    <?php echo $url_vot; ?>?c=ballot&p=<?php echo base64_encode($idvot); ?>&title=<?php echo urls_amigables($row_nom[0]); ?>
                                 </div>
                                       <div class="col-sm-2">
                                   <a href="javascript:void(0);" data-href="aux_vota.php?c=<?php echo encrypt_url('blog/ballot/idvot='.$idvot,$clave_encriptacion) ?>" class="openextraLargeModal btn btn-primary btn-xs"><?= _("Ver previo") ?></a>
                          </div>
                            </div>
                        <?php } ?>
                        <?php
                        $chekeado1 = "checked=\"checked\"";
                        $chekeado2 = "";
                        if ($est == "modca") {
                            if ($row[1] == 0) {
                                $chekeado1 = "checked=\"checked\" ";
                            } else {
                                $chekeado2 = "checked=\"checked\" ";
                            }
                        }
                        ?>
                        <div class="form-group row">
                            <div  class="col-sm-3 "><?= _("Estado") ?></div>
                            <div for="estado_1" class="col-sm-9">
                                <label control-label>
                                    <input name="estado" type="radio" id="estado_1" value="0"  <?php echo "$chekeado1"; ?>/>
                                    <?= _("Activo") ?></label><span class="label label-warning"></span> <br/>
                                <label for="estado_2" control-label>
                                    <input name="estado" type="radio" id="estado_2" value="1"  <?php echo "$chekeado2"; ?>/>
                                    <?= _("Inactivo") ?></label>

                            </div>
                        </div>

                        <?php
                        $chekeado_pres1 = "";
                        $chekeado_pres2 = "";
                        $chekeado_pres3 = "";
                        $chekeado_pres4 = "";
                        $chekeado_pres5 = "";
                        $chekeado_pres6 = "";
                        if ($est == "modca") {
                            if ($row[2] == 1) {
                                $chekeado_pres1 = "checked=\"checked\" ";
                            } else if ($row[2] == 2) {
                                $chekeado_pres2 = "checked=\"checked\" ";
                            } else if ($row[2] == 4) {
                                $chekeado_pres4 = "checked=\"checked\" ";
                            } else if ($row[2] == 5) {
                                $chekeado_pres5 = "checked=\"checked\" ";
                            } else if ($row[2] == 6) {
                                $chekeado_pres6 = "checked=\"checked\" ";
                            } else {
                                $chekeado_pres3 = "checked=\"checked\" ";
                            }
                        }else{
                         $chekeado_pres1 = "checked=\"checked\"";
                         }
                        ?>
                        <div class="form-group row">
                            <div class="col-sm-3"><?= _("Presentación") ?></div>

                            <div class="col-sm-9">
                              <label for="presentacion_2" class="control-label">
                                    <input name="presentacion" type="radio" id="presentacion_2" value="1"  <?php echo "$chekeado_pres1"; ?> />
                                    <?= _("Presentacion") ?> 1</label>
                                <br/>
                                <label for="presentacion_0" class="control-label">

                                    <input name="presentacion" type="radio" id="presentacion_0" value="2"  <?php echo "$chekeado_pres2"; ?> />
                                    <?= _("Presentacion") ?> 2</label>
                                <br />
                                <label for="presentacion_1" class="control-label">
                                    <input name="presentacion" type="radio" id="presentacion_1" value="3"  <?php echo "$chekeado_pres3"; ?> />
                                    <?= _("Presentacion") ?> 3</label>
                                <br />
                                <label for="presentacion_3" class="control-label">
                                    <input name="presentacion" type="radio" id="presentacion_3" value="4"  <?php echo "$chekeado_pres4"; ?> />
                                    <?= _("Presentacion") ?> 4</label>
                                <br />
                                <label for="presentacion_4" class="control-label">
                                    <input name="presentacion" type="radio" id="presentacion_4" value="5"  <?php echo "$chekeado_pres5"; ?> />
                                    <?= _("Presentacion") ?> 5</label>
                                <br />
                                <label for="presentacion_5" class="control-label">
                                    <input name="presentacion" type="radio" id="presentacion_5" value="6"  <?php echo "$chekeado_pres6"; ?> />
                                    <?= _("Presentacion") ?> 6</label>
                                <br />
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="presentacion" class="col-sm-3 control-label"><?= _("Numero de columnas") ?></label>
                            <div class="col-sm-9">

                                <?php
                                $chekeado_col1 = "";
                                $chekeado_col2 = "";
                                $chekeado_col3 = "";
                                $chekeado_col4 = "";
                                $chekeado_col6 = "";
                                $chekeado_col12 = "";
                                if ($est == "modca") {
                                    if ($row[5] == 1) {
                                        $chekeado_col1 = "selected=\"selected\" ";
                                    } else if ($row[5] == 2) {
                                        $chekeado_col2 = "selected=\"selected\" ";
                                    } else if ($row[5] == 3) {
                                        $chekeado_col3 = "selected=\"selected\" ";
                                    } else if ($row[5] == 6) {
                                        $chekeado_col6 = "selected=\"selected\" ";
                                    } else if ($row[5] == 12) {
                                        $chekeado_col12 = "selected=\"selected\" ";
                                    } else {
                                        $chekeado_col4 = "selected=\"selected\" ";
                                    }
                                }else{
                                  $chekeado_col3 = "selected=\"selected\"";
                                 }
                                ?>


                                <select name="aux1" id="aux1" class="custom-select">
                                    <option value="1" <?php echo "$chekeado_col1"; ?>>1</option>
                                    <option value="2" <?php echo "$chekeado_col2"; ?>>2</option>
                                    <option value="3" <?php echo "$chekeado_col3"; ?>>3</option>
                                    <option value="4" <?php echo "$chekeado_col4"; ?>>4</option>
                                    <option value="6" <?php echo "$chekeado_col6"; ?>>6</option>
                                    <option value="12" <?php echo "$chekeado_col12"; ?>>12</option>
                                </select>
                            </div>
                        </div>

                        <?php
                        $chekeado_cab1 = "checked=\"checked\" ";
                        $chekeado_cab2 = "";
                        if ($est == "modca") {
                            if ($row[3] == 1) {
                                $chekeado_cab2 = "checked=\"checked\" ";
                            } else {
                                $chekeado_cab1 = "checked=\"checked\" ";
                            }
                        }
                        ?>



                        <div class="form-group row">
                            <label for="diseno" class="col-sm-3 control-label"><?= _("Incluir CSS personalizado") ?></label>
                            <div class="col-sm-9">
                                <textarea name="diseno" id="diseno" class="form-control" rows="8"><?php
                                    if ($est == "modca") {
                                        echo $row[6];
                                    }
                                    ?></textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12">

                                <p>&nbsp;</p>

                                <input name="fecha" type="hidden" id="fecha" value="<?php echo"$fecha"; ?>" />
                                <?php if ($est == "modca") { ?><input name="id_pag" type="hidden" id="id_pag" value="<?php echo $row[0]; ?>"><?php } ?>

                                <?php if ($situacion == "") { ?>
                                    <input name="add_pagina" type=submit  class="btn btn-primary btn-block" id="add_pagina" value="<?= _("GENERAR PAGINA EXTERNA") ?>">
                                <?php } else { ?>
                                    <input name="modifika_pagina" type=submit  class="btn btn-primary btn-block" id="modifika_pagina" value="<?= _("MODIFICAR PAGINA EXTERNA") ?>">
                                <?php } ?>
                                </form>




                            </div>
                        </div>


</div>
<?php } ?>

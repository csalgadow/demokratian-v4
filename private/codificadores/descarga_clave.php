<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
require_once("../private/config/config.inc.php");
require_once("../private/inc_web/conexion.php");
include_once ('../private/basicos_php/basico.php');
require_once("../private/basicos_php/url.inc.php");
/* establecer el limitador de cache a 'private' */
session_cache_limiter('private');
/* establecer la caducidad de la cache a 30 minutos */
session_cache_expire(30);
//session_name($usuarios_sesion2);
session_start();
if (empty($_SESSION['numero_vot'])) {
  session_destroy();
  Header("Location:" .$url_vot."/index.php?c=codificadores&error_login=5"); //envío al usuario a la pag. de autenticación
    exit;
}else {
    $fechaGuardada = $_SESSION["ultimoAcceso"];
    $ahora = date("Y-n-j H:i:s");
    $tiempo_transcurrido = (strtotime($ahora) - strtotime($fechaGuardada)); //comparamos el tiempo transcurrido

    if ($tiempo_transcurrido >= $tiempo_session) { //miramos si el tiempo es superior al que tenemos por configuración
        session_destroy();
        Header("Location:" .$url_vot."/index.php?c=codificadores&error_login=11");//envío al usuario a la pag. de autenticación
        exit;
    } else {
        $_SESSION["ultimoAcceso"] = $ahora; //actualizamos el tiempo
    }
}

if (ISSET($_POST["add_clave"])) {

    $clave_publica = fn_filtro($con, $_POST['clave_publica']);
    //$id_votacion=$_SESSION['ID_inter_0'];
    //$id_usuario=$_SESSION['nombre_inter_0'];

    $sSQL = "UPDATE $tbn21 SET clave_publica=\"$clave_publica\" WHERE ID='" . $_SESSION['ID_inter_0'] . "'";
    $result = mysqli_query($con, $sSQL) or die("Imposible modificar pagina");
    if (!$result) {
        //$okVot = false;
        //break;
        echo _("error al incluir su clave publica en el sistema");
        exit();
    } else {
        $texto1 =


        header("Content-Description: File Transfer");
        header("Content-Type: application/force-download");
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false);
        header("Content-Disposition: attachment; filename=\" " . $_SESSION['ID_inter_0'] . "_clave.txt\";");
        header("Content-Transfer-Encoding: binary");

         echo "\n\r\n\r   " . _("Se ha incluido la clave publica en la base de datos");
         echo  "\n\r\n\r";
         echo  "\n\r\n\r";
         echo  "   " ._("Su clave privada");
         echo  "\n\r\n\r";
         echo  $_POST['clave_privada'];
         echo  "\n\r\n\r";
         echo  "\n\r\n\r";
         echo  "\n\r\n\r";
         echo  "   " ._("Clave publica incluida en la base de datos");
         echo  "\n\r\n\r";

         echo  $_POST['clave_publica'];
         echo  "\n\r\n\r";
         echo  "\n\r\n\r";

    }
}


?>

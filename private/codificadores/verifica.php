<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que comprueba si la votación permite codificadores y valida al cofificador
*/
require_once("../private/config/config.inc.php");
require_once("../private/inc_web/conexion.php");
include_once ('../private/basicos_php/basico.php');
include_once ('../private/basicos_php/errores.php');
require_once("../private/inc_web/version.php");
require_once("../private/basicos_php/url.inc.php");
/* establecer el limitador de cache a 'private' */
session_cache_limiter('private');
/* establecer la caducidad de la cache a 30 minutos */
session_cache_expire(30);
//session_name($usuarios_sesion2);
session_start();




require_once("../private/basicos_php/lang.php");

if ($_SERVER['HTTP_REFERER'] == "") {
    Header("Location:" .$url_vot."/index.php?c=codificadores&error_login=0");
    exit;
}

if (isset($_POST['user']) && isset($_POST['pass']) && isset($_POST['votacion'])) {
  if ($reCaptcha == true) {

    if ($tipo_reCaptcha == "reCAPTCHA_v2") {
        if(isset($_POST['g-recaptcha-response'])){
          $captcha=$_POST['g-recaptcha-response'];
        }
        if(!$captcha){
          // falta el g-recaptcha
          $validoCaptcha = false;
          Header("Location:" .$url_vot."/index.php?c=codificadores&error_login=10");
          exit;
        }

        $ip = $_SERVER['REMOTE_ADDR'];
        // post request to server
        $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($reCAPTCHA_secret_key) .  '&response=' . urlencode($captcha);
        $response = file_get_contents($url);
        $responseKeys = json_decode($response,true);
        // should return JSON with success as true
        if($responseKeys["success"]) {
                //Captcha correcto
                $validoCaptcha = true;
        } else {
                // eres un robot, asi que terminamos
                $validoCaptcha = false;
                Header("Location:" .$url_vot."/index.php?c=codificadores&error_login=10");
                exit;
        }
      }else{  // se se usa recatcha v3
          define("RECAPTCHA_V3_SECRET_KEY", $reCAPTCHA_secret_key);

          $token = $_POST['token'];
          $action = $_POST['action'];

                  // call curl to POST request
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL,"https://www.google.com/recaptcha/api/siteverify");
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('secret' => RECAPTCHA_V3_SECRET_KEY, 'response' => $token)));
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          $response = curl_exec($ch);
          curl_close($ch);
          $arrResponse = json_decode($response, true);

            if($arrResponse["success"] != '1' && $arrResponse["action"] != $action && $arrResponse["score"] <= $sensibilidad) {
              // Si entra aqui, es un robot....
              $validoCaptcha = false;
                Header("Location:" .$url_vot."/index.php?c=codificadores&error_login=10");
                exit;
            } else {
                //Captcha correcto
                $validoCaptcha = true;
            }
      }
///////////////////////////////////////
  } else {
      // si no tenemos activado el sistema de captcha miramos el token a ver si esta bien
      //// comprobamos el token es correcto y llega bien
      $server =$_SERVER['SERVER_NAME'];
      $token=$_POST['token'];
      $clave= $clave_encriptacion2.$server;

          if(!password_verify($clave, $token)){ // comprobamos token para evitar accesos por ataque csrf
            $validoCaptcha = false;
              Header("Location:" .$url_vot."/index.php?c=codificadores&error_login=12");
              exit;
          }else{
            // si esta bien el token, damos el ok
      $validoCaptcha = true;
    }
  }

if($validoCaptcha == true){  // si la validación del Captcha es correta o no esta activada
    //evitamos el sql inyeccion
    $user1 = fn_filtro($con, $_POST['user']);
    $pass1 = fn_filtro($con, $_POST['pass']);
    $votacion1 = fn_filtro_numerico($con, $_POST['votacion']);

    //miramos si la votacion esta activa y tiene opcion de codificadores
    $result = mysqli_query($con, "SELECT activa,nombre_votacion,encripta,fecha_com,fecha_fin FROM $tbn1 where ID=$votacion1");
    if (mysqli_num_rows($result) == 0) {
        Header("Location:" .$url_vot."/index.php?c=codificadores&error_login=7");
        exit;
    }


    //  else {
    $row = mysqli_fetch_row($result);

    if ($row[0] == "si" && $row[2] == "si") {


        $usuario_consulta = mysqli_query($con, "SELECT 	ID, nombre,  pass, usuario FROM $tbn21 WHERE usuario='" . $user1 . "'  and id_votacion='" . $votacion1 . "'") or die(header("Location:  $url_vot?error_login=1"));



        if (mysqli_num_rows($usuario_consulta) != 0) {
            $login = stripslashes($user1);
            //$password = md5($pass1);


            $usuario_datos = mysqli_fetch_array($usuario_consulta);
            mysqli_free_result($usuario_consulta);

          //  mysqli_close($con);


            if ($login != $usuario_datos['usuario']) {
                Header("Location: " .$url_vot."/index.php?c=codificadores&error_login=4");
                exit;
            }

            if (!password_verify($pass1, $usuario_datos['pass'])) {
//            if ($password != $usuario_datos['pass']) {
                Header("Location:" .$url_vot."/index.php?c=codificadores&error_login=3");
                exit;
            }

            unset($login);
            unset($password);

            $_SESSION['ID_inter_0'] = $usuario_datos['ID'];
            $_SESSION['usuario_0'] = $usuario_datos['usuario'];
            $_SESSION['nombre_inter_0'] = $usuario_datos['nombre'];
            $_SESSION['numero_vot'] = fn_filtro($con, $_POST['votacion']);
            $_SESSION['nombre_votacion'] = $row[1];
            $_SESSION['fecha_com'] = $row[3];
            $_SESSION['fecha_fin'] = $row[4];
            $_SESSION["ultimoAcceso"] = date("Y-n-j H:i:s");  // ponemos la fehca y hora actual para ponerle caducidad a la sesion



            $fecha = date("Y-m-d H:i:s");
            $insql = "UPDATE $tbn21 SET fecha_ultimo =\"$fecha\" WHERE id=" . $usuario_datos['ID'] . " ";
            $inres = @mysqli_query($con, $insql);


            Header("Location:" . $_SERVER['PHP_SELF'] . "?d=".random_int(1000000, 9999999));
            exit;
        } else {
            Header("Location:" .$url_vot."/index.php?c=codificadores&error_login=2");
            exit;
        }
    } else {

        Header("Location:" .$url_vot."/index.php?c=codificadores&error_login=8");
        exit;
    }

    if (!isset($_SESSION['usuario_login']) && !isset($_SESSION['usuario_password'])) {
        session_destroy();
      //  die("Error cod.: 2 - �Acceso incorrecto! <br> <a href=login.php>VOLVER A INTENTARLO</a>");
        Header("Location:" .$url_vot."/index.php?c=codificadores&error_login=6");
        exit;
    }  else {
            // eres un robot, asi que terminamos
            $validoCaptcha = false;
            Header("Location:" .$url_vot."/index.php?c=codificadores&error_login=10");
            exit;
    }
  }
}


if (!isset($_SESSION['ID_inter_0'])) {
    session_destroy();
  //  die("Error cod.: 3 - Acceso incorrecto!");
    Header("Location:" .$url_vot."/index.php?c=codificadores&error_login=5");
    exit;
}/**/
?>

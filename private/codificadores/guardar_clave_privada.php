<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
if(!isset($cargaI)){
  $cargaI =false;
  exit;
}
if($cargaI!="OK"){
  exit;
}else{

if (empty($_SESSION['numero_vot'])) {
  session_destroy();
  Header("Location:" .$url_vot."/index.php?c=codificadores&error_login=5"); //envío al usuario a la pag. de autenticación
    exit;
}else {
    $fechaGuardada = $_SESSION["ultimoAcceso"];
    $ahora = date("Y-n-j H:i:s");
    $tiempo_transcurrido = (strtotime($ahora) - strtotime($fechaGuardada)); //comparamos el tiempo transcurrido

    if ($tiempo_transcurrido >= $tiempo_session) { //miramos si el tiempo es superior al que tenemos por configuración
        session_destroy();
        Header("Location:" .$url_vot."/index.php?c=codificadores&error_login=11"); //envío al usuario a la pag. de autenticación
        exit;
    } else {
        $_SESSION["ultimoAcceso"] = $ahora; //actualizamos el tiempo
    }
}


if (ISSET($_POST["add_clave"])) {

    $clave_privada = fn_filtro($con, $_POST['clave_privada']);


    $sSQL = "UPDATE $tbn21 SET clave_privada=\"$clave_privada\" WHERE ID='" . $_SESSION['ID_inter_0'] . "'";
    $result = mysqli_query($con, $sSQL) or die("Imposible modificar pagina");
    if (!$result) {
        $texto1 = _("Error al guarda datos");
        exit();
        // break;
    } else {
        $texto1 = _("Se ha incluido la clave privada en la base de datos");
    }
}
?>

<div class="col-md-3 col-xl-2">

  <nav class="nav-side-menu bg-blue flex-md-column flex-row">
    <div class="brand">menú</div>
    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>

    <div class="menu-list">

      <ul id="menu-content" class="menu-content collapse out">
        <li class="user-box">
          <div id="usuario">
          <ul>
          <li>  <span class="user"><?= _("Usuario") ?>:</span> <span class="letra2_c_user"><?php echo   $_SESSION['nombre_inter_0']; ?> </span></li>
          </ul>
          </div>
        </li>
        <li>
          <a href="codificadores.php?c=<?php echo encrypt_url('codificadores/log_out/id=SD',$clave_encriptacion) ?>"><i class="fa fa-sign-out fa-lg"> </i>  <?= _("Desconexión") ?></a>
        </li>
      </ul>
    </div>
  </nav>

      </div>


      <div class="col-md content_interventores">
        <div class="card contenido">
	         <div class="card-header-votaciones "> <h1 class="card-title"> <?= _("Clave privada") ?>.</h1> </div>

	          <div class="card-body">

                    <!--Comiezo-->

                    <h3> <?= _("Esta es la votación") ?>; <strong>" <?php echo $_SESSION['nombre_votacion']; ?> "</strong> </h3>




                    <?php
                    $result = mysqli_query($con, "SELECT orden,nombre,correo,clave_privada,clave_publica FROM $tbn21  WHERE ID='" . $_SESSION['ID_inter_0'] . "'");
                    $row = mysqli_fetch_row($result);
                    $orden = $row[0];
                    ?>

                    <div class="alert alert-info"><?= _("Datos del codificador") ?>:</div>
                    <div class="form-group row">
                        <div for="nombre" class="col-sm-3 control-label"> <?= _("Nombre") ?></div>

                        <div class="col-sm-9"><?php echo $row[1]; ?></div>
                    </div>
                    <p>&nbsp;</p>

                    <div class="form-group row">
                        <div for="nombre" class="col-sm-3 control-label"><?= _("Correo") ?></div>

                        <div class="col-sm-9"><?php echo $row[2]; ?></div>
                    </div>
                    <p>&nbsp;</p>
                    <div class="alert alert-info"><?= _("Las claves pública y privada guardadas en el servidor son") ?>:</div>
                    <div class="form-group row">
                        <div for="nombre" class="col-sm-3 control-label"><?= _("Tu clave privada") ?></div>

                        <div class="col-sm-9"><?php echo $row[3]; ?></div>
                    </div>
                    <p>&nbsp;</p>

                    <div class="form-group row">
                        <div  for="nombre" class="col-sm-3 control-div"><?= _("Tu clave publica") ?></div>

                        <div class="col-sm-9"><?php echo $row[4]; ?></div>
                    </div>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>


                    <!--Final-->
                  </div>
                </div>
              </div>
              <?php } ?>

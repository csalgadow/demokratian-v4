<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que  crea la contraseña mediante la petición ajax del archivo crea_contrasena.js.
* @todo ver si este archivo seria mejor que estuviera en basiscos_php
*/
require_once("../private/config/config.inc.php");
require_once("../private/inc_web/conexion.php");
require_once("../private/basicos_php/lang.php");

include_once ('../private/basicos_php/basico.php');
$errores = "";

if (empty($_POST['name']) ||
        empty($_POST['email']) ||
        empty($_POST['pass']) ||
        empty($_POST['pass2']) ||
        !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
    echo 'ERROR##<div class="alert alert-danger">
             <a class="close" data-dismiss="alert">x</a>' . _("No se han enviado todos los datos!"). '
             </div>';
    return false;
}
if (empty($_POST['npdr']) ||
        empty($_POST['idrec'])
) {
    echo 'ERROR##<div class="alert alert-danger">
             <a class="close" data-dismiss="alert">x</a>' . _("Algo esta haciendo mal!!") . "<br/>" . _("Acceda mediante el enlace que le hemos enviado al correo electronico") .'
             </div>';
    return false;
}


$name = fn_filtro($con, $_POST['name']);
$email = fn_filtro($con, $_POST['email']);
$pass = fn_filtro($con, $_POST['pass']);
$pass2 = fn_filtro($con, $_POST['pass2']);
//$npdr = $_POST['npdr'];
$idrec = $_POST['idrec'];

function comprobar_nombre_usuario($nombre_usuario) {
    //compruebo que el tamaño del string sea válido.
    if (strlen($nombre_usuario) < 5 || strlen($nombre_usuario) > 20) {

        $error = "error1";
        return $error;
    }

    //compruebo que los caracteres sean los permitidos
    $permitidos = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_";
    for ($i = 0; $i < strlen($nombre_usuario); $i++) {
        if (strpos($permitidos, substr($nombre_usuario, $i, 1)) === false) {
            $error = "error2";
            return $error;
        }
    }
    //echo $nombre_usuario . " es válido<br>";
    return $nombre_usuario;
}

$nombre_usuario_new = comprobar_nombre_usuario($name);

if ($nombre_usuario_new == "error1") {
    $errores = _("El nombre de usuario") . $nombre_usuario . " " . _("no es válido") . "<br/>" . _("Tiene que tener entre 5 y 20 caracteres") . " <br/>";


    //return false;
} elseif ($nombre_usuario_new == "error2") {
    $errores = _("El nombre de usuario") . $nombre_usuario . " " . _("no es válido, esta mal formado, no puede tener espacios en blanco, acentos, ñ ...") . "<br/>";
} else {


    function decrypt($string, $key) {
        $result = '';
        $string = str_replace(array('-', '_'), array('+', '/'), $string);
        $string = base64_decode($string);
        for ($i = 0; $i < strlen($string); $i++) {
            $char = substr($string, $i, 1);
            $keychar = substr($key, ($i % strlen($key)) - 1, 1);
            $char = chr(ord($char) - ord($keychar));
            $result .= $char;
        }
        return $result;
    }

    $clave_encriptada = $_POST['npdr'];
    $cadena_desencriptada = decrypt($clave_encriptada, $clave_encriptacion);
    $array_cadena = explode('_', $cadena_desencriptada);
    $id_votacion = fn_filtro($con, $array_cadena[0]);
    $id_usuario1 = fn_filtro($con, $array_cadena[1]);
    $clave_necesaria = fn_filtro($con, $array_cadena[2]);

//
    $clave_encriptada2 = $_POST['idrec'];
//$clave_encriptada2 = str_replace(array('-', '_'), array('+', '/'), $clave_encriptada2);
    $cadena_desencriptada2 = decrypt($clave_encriptada2, $clave_encriptacion2);
    $array_cadena2 = explode('-', $cadena_desencriptada2);
    $clavecilla = fn_filtro($con, $array_cadena2[0]);
    $id_usuario2 = fn_filtro($con, $array_cadena2[1]);

    if ($clavecilla == "nwt") {
        if ($id_usuario1 == $id_usuario2) {


            $result = mysqli_query($con, "SELECT id_votacion,correo FROM $tbn21 WHERE ID=$id_usuario1 and codigo_rec='$clave_necesaria'") or die("No se pudo realizar la consulta a la Base de datos");


            if ($row = mysqli_fetch_array($result)) {
                mysqli_field_seek($result, 0);
                do {


                    if ($id_votacion == $row[0] and $email == $row[1]) {

                        /// miramos si ese nombre de usuario se esta usando
                        $result_usu = mysqli_query($con, "SELECT usuario,correo FROM $tbn21 WHERE usuario='$nombre_usuario_new'") or die("No se pudo realizar la consulta a la Base de datos");
                        $quants_usu = mysqli_num_rows($result_usu);
                        if ($quants_usu == 0) {
                            $passw = password_hash($pass, PASSWORD_BCRYPT);
                            $sSQL12 = "UPDATE $tbn21 SET pass='$passw', usuario='$nombre_usuario_new' WHERE ID='$id_usuario1'";
                            mysqli_query($con, $sSQL12) or die("Imposible modificar datos");

                            $correcto = _("Sus datos han sido incluidos") . " <br/>" . _("Ya puede acceder al sistema de votaciones");
                        } else {
                            $rowa = mysqli_fetch_row($result_usu);
                            if ($rowa[1] == $email) {
                                $passw = password_hash($pass, PASSWORD_BCRYPT);
                                $sSQL12 = "UPDATE $tbn21 SET pass='$passw', usuario='$nombre_usuario_new' WHERE ID='$id_usuario1'";
                                mysqli_query($con, $sSQL12) or die("Imposible modificar datos");

                                $correcto = _("Sus datos han sido incluidos") . " <br/>" . _("Ya puede acceder al sistema de votaciones");
                            } else {
                                $errores = _("Este nombre de usuario ya se esta usando, cambielo, por favor");
                            }
                        }
                    } else {
                        $errores = _("hay algun problema con su correo, reviselo");
                    }
                } while ($row = mysqli_fetch_array($result));
            } else {
                $errores = _("¡Su correo no nos consta, compruebelo, por favor!");
            }

//aqui
        } else {
            $errores = _("Upsss, algo esta pasando y hay un error") . " 1";
        }
    } else {
        $errores = _("Upsss, algo esta pasando y hay un error");
    }
}
if ($errores != "") {
    echo "ERROR##<div class=\"alert alert-danger\">
             <a class=\"close\" data-dismiss=\"alert\">x</a>" . $errores . "
             </div>";
} else {
    echo 'OK##<div class="alert alert-success">
             <a class="close" data-dismiss="alert">x</a>'. $correcto .'
			 <br/><a  href="index.php?c=codificadores" class="btn btn-start-order btn-block">'._("Acceder al espacio de codificadores").'</a>
             </div>';
}


return true;
?>

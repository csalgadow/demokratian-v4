<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que envía el correo del formulario para crear la contraseña mediante la petición ajax del archivo recupera_contrasena.js.
* @todo ver si este archivo seria mejor que estuviera en basiscos_php
*/

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
if(!isset($cargaI)){
  $carga =false;
  exit;
}
if($cargaI!="OK"){
  exit;
}else{
require_once("../private/config/config.inc.php");
require_once("../private/inc_web/conexion.php");
include('../private/config/config-correo.inc.php');
require_once("../private/basicos_php/lang.php");
require_once("../private/basicos_php/url.inc.php");
//require_once('../private/modulos/PHPMailer/PHPMailerAutoload.php');

require_once ('../private/modulos/PHPMailer/src/PHPMailer.php');
require_once ('../private/modulos/PHPMailer/src/SMTP.php');
require_once ('../private/modulos/PHPMailer/src/Exception.php');
//include("../private/modulos/PHPMailer/class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded
include_once ('../../private/basicos_php/basico.php');


if (empty($_POST['name']) || empty($_POST['email']) || empty($_POST['name']) ||
        !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
      echo _("Faltan datos");
    return false;
}
$ya_clave = "";  //definimos esta variable vacia
$name = fn_filtro_nodb($_POST['name']);
$email = fn_filtro_nodb($_POST['email']);
$n_votacion = fn_filtro_numerico($con, $_POST['n_votacion']);
//$name = utf8_decode($name);
//$asunto = utf8_decode($asunto);

$conta = "SELECT id,pass  FROM $tbn21 WHERE correo like \"$email\" and id_votacion='$n_votacion' ";


$result_cont = mysqli_query($con, $conta);
$quants = mysqli_num_rows($result_cont);

if ($quants == 0) {

//echo "$provincia";
    echo "ERROR##<div class=\"alert alert-warning\">
    <a href=\"#\" class=\"close\" data-dismiss=\"alert\">x</a>
    <strong>¡¡¡ERROR!!!</strong><br/>" . _("La direccion de correo") . "  \" " . $email . " \"  "
    . _("no la tenemos registrada para esta votación, quizas sea un error de nuestra base de datos").
    ".<br/>"._("Si consideras que tienes derecho acceder contacta con el administrador") . ".
</div>";
    //. " <a data-toggle=\"modal\" href=\"#contacta\"  data-dismiss=\"modal\" >" . _("click aqui para enviarnos tus datos a traves de nuestro formulario") . "</a>.
//echo "Esta direccion de correo no la tenemos registrada para esta provincia, quizas sea un error de nuestra base de datos , si consideras que tienes derecho a votar haz <a href=\"voto_contacto.php\"> click aqui para enviarnos tus datos a traves de nuestro formulario</a>";
} else {
    $row = mysqli_fetch_array($result_cont);
    $id_votante = $row[0];
    if ($row[1] != "") {
        $ya_clave = "ya_tiene_clave";
    }



////////////////////////////incluimos en la base de datos los datos del envio

    $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
    $cad = "";
    for ($i = 0; $i < 12; $i++) {
        $cad .= substr($str, rand(0, 62), 1);
    }

    $sSQL = "UPDATE $tbn21 SET codigo_rec=\"$cad\"  WHERE ID='$id_votante'";

    mysqli_query($con, $sSQL) or die("Imposible modificar pagina");

///////////////////enviamos un correo
    function encrypt($string, $key) {
        $result = '';
        for ($i = 0; $i < strlen($string); $i++) {
            $char = substr($string, $i, 1);
            $keychar = substr($key, ($i % strlen($key)) - 1, 1);
            $char = chr(ord($char) + ord($keychar));
            $result .= $char;
        }
        return str_replace(array('+', '/', '='), array('-', '_', ''), base64_encode($result));
    }

    $cadena_para_encriptar = $n_votacion . "_" . "$id_votante" . "_" . "$cad";
    $cadena_encriptada = encrypt($cadena_para_encriptar, $clave_encriptacion);

    $cadena_para_encriptar2 = "nwt-" . $id_votante;
    $cadena_encriptada2 = encrypt($cadena_para_encriptar2, $clave_encriptacion2);

    $mensaje = _("Hola") . " " . $name . " <br/>";

    $mensaje .= _("Este mensaje ha sido enviado por el sistema de votaciones") . " " . $nombre_web . " " . _("el") . " " . date('d/m/Y', time());
    if ($ya_clave == "ya_tiene_clave") {
        $mensaje .= "<br/>" .
                _("Te enviamos un enlace  para recuperar tu contraseña") . "<br/>" .
                _("Si  no ha solicitados la contraseña, alguien que conoce su dirección de correo lo ha hecho, pero puedes obviar este correo ya que tu contraseña no ha sido modificada") . " \r\n";
    } else {
        $mensaje .= "<br/>" .
                _("Te enviamos un enlace  para finalizar tu registro") . " <br/>";
    }
    $mensaje .= _("Para completar la operación es imprescindible acceder a la siguiente dirección web") . " <br/>"
     . $url_vot . '/index.php?c=new_pass_codificador/regedi='.$cadena_encriptada2 .'&idpr=' . $cadena_encriptada. ' ' ;





  //  $mensaje = str_replace("\n", "<br>", $mensaje);
  //  $mensaje = str_replace("\t", "    ", $mensaje);

    if ($correo_smtp == true) {  //comienzo envio smtp
        $mail = new PHPMailer();
        $mail->CharSet = 'UTF-8';
        $mail->ContentType = 'text/plain';
        //$mail->IsHTML(false);
        if ($mail_IsHTML == true) {
            $mail->IsHTML(true);
        } else {
            $mail->IsHTML(false);
        }

        if ($mail_sendmail == true) {
            $mail->IsSendMail();
        } else {
            $mail->IsSMTP();
        }

        //$mail->SMTPAuth = true;
        if ($mail_SMTPAuth == true) {
            $mail->SMTPAuth = true;
        } else {
            $mail->SMTPAuth = false;
        }

        if ($mail_SMTPSecure == false) {
            $mail->SMTPSecure = false;
            $mail->SMTPAutoTLS = false;
        } else if ($mail_SMTPSecure == "SSL") {
            $mail->SMTPSecure = 'ssl';
        } else {
            $mail->SMTPSecure = 'tls';
        }

        if ($mail_SMTPOptions == true) {  //algunos servidores con certificados incorrectos no envian los correos por SMTP por lo que quitamos la validadcion de los certificados, NO SE RECOMIENDA EN ABSOLUTO usar esta opción
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
        }

        $mail->Port = $puerto_mail; // Puerto a utilizar

        $mail->Host = $host_smtp;
        $mail->SetFrom($email_env, $nombre_sistema);
        $mail->Subject = $asunto;
        $mail->MsgHTML($mensaje);
        $mail->AddAddress($email, $name);
        $mail->Username = $user_mail;
        $mail->Password = $pass_mail;


        if (!$mail->Send()) {
            echo " Error en el envio " . $mail->ErrorInfo;
        } else {
            // echo "Enviado correctamente!";

            echo "
<div class=\"alert alert-success\">
    <strong>" . _("Se ha enviado su código de activación a su direccion de correo para validar su cuenta y asi poder participar en las votaciones") . ".</strong><br/> " .
            _("Si no recibe el correo compruebe su carpeta de spam por si esta alli") . ". <br/>" .
            _("Ahora tiene que mirar su correo y copiar ese código de activación para introducirlo en el sistema") . ".</div>";
        }
    }


    if ($correo_smtp == false) { ///correo mediante mail de php
        //para el envío en formato HTML
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

//dirección del remitente
        $headers .= "From: $nombre_sistema <$email_env>\r\n";


//ruta del mensaje desde origen a destino
        $headers .= "Return-path: $email_env\r\n";

//$asunto="$asunto_mens_error";


        mail($email, $asunto, $mensaje, $headers);

        echo _("enviad correo por mail");
    }
}




return true;
}
?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
if(!isset($cargaI)){
  $cargaI =false;
  exit;
}
if($cargaI!="OK"){
  exit;
}else{
if (empty($_SESSION['numero_vot'])) {
  session_destroy();
  Header("Location:" .$url_vot."/index.php?c=codificadores&error_login=5"); //envío al usuario a la pag. de autenticación
    exit;
}else {
    $fechaGuardada = $_SESSION["ultimoAcceso"];
    $ahora = date("Y-n-j H:i:s");
    $tiempo_transcurrido = (strtotime($ahora) - strtotime($fechaGuardada)); //comparamos el tiempo transcurrido

    if ($tiempo_transcurrido >= $tiempo_session) { //miramos si el tiempo es superior al que tenemos por configuración
        session_destroy();
        Header("Location:" .$url_vot."/index.php?c=codificadores&error_login=11"); //envío al usuario a la pag. de autenticación
        exit;
    } else {
        $_SESSION["ultimoAcceso"] = $ahora; //actualizamos el tiempo
    }
}

?>

<div class="col-md-3 col-xl-2">

  <nav class="nav-side-menu bg-blue flex-md-column flex-row">
    <div class="brand">menú</div>
    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>

    <div class="menu-list">

      <ul id="menu-content" class="menu-content collapse out">
        <li class="user-box">
          <div id="usuario">
          <ul>
          <li>  <span class="user"><?= _("Usuario") ?>:</span> <span class="letra2_c_user"><?php echo   $_SESSION['nombre_inter_0']; ?> </span></li>
          </ul>
          </div>
        </li>
        <li>
          <a href="codificadores.php?c=<?php echo encrypt_url('codificadores/log_out/id=SD',$clave_encriptacion) ?>"><i class="fa fa-sign-out fa-lg"> </i>  <?= _("Desconexión") ?></a>
        </li>
      </ul>
    </div>
  </nav>

      </div>


      <div class="col-md content_interventores">
        <div class="card contenido">
	         <div class="card-header-votaciones "> <h1 class="card-title"> <?= _("Incluir clave privada") ?>.</h1> </div>

	          <div class="card-body">

                    <!--Comiezo-->
                    <h3><?= _("Ha accedido a la votación") ?> <strong>" <?php echo $_SESSION['nombre_votacion']; ?> "</strong><?= _("para inclir su privada") ?>.</h3>
                    <div class="alert alert-warning"> <?= _("Recuerde, es sumamente importante que guarde sus claves y realice correctamente el proceso") ?>.</div>


                    <form name="form1" method="post" action="codificadores.php?c=<?php echo encrypt_url('codificadores/guardar_clave_privada/id=SD',$clave_encriptacion) ?>">


                        <div class="form-group row">
                            <label for="nombre" class="col-sm-3 control-label"> <?= _("Tu clave privada") ?></label>

                            <div class="col-sm-9">
                                <textarea name="clave_privada" rows="15" autofocus required class="form-control" id="clave_privada"></textarea>
                            </div>
                        </div>
                        <p>&nbsp;</p>
                        <div class="row">

                        <div class="col-sm-12">
                            <h4><span class="label label-warning"><?= _("Al presionar el botón de guardar clave, se gradara su clave privada en el servidor") ?></span></h4>
                            <h4><span class="label label-warning"><?= _("De esta forma se podra iniciar el proceso de desencriptación") ?>.</span></h4>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-12">
                            <input name="add_clave" type=submit class="btn btn-primary btn-block"  id="add_clave" value="<?= _("Guardar claves") ?>" />

                        </div>
                      </div>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                    </form>

                    <!--Final-->

                  </div>
                </div>
              </div>
              <?php } ?>

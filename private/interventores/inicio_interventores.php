<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo al que se accede al validar el primer interventor, si hay que validar más interventores, presenta el formulario para incluir al siguiente y mediante petición ajax a add_interventor.php compruena y va validando interventores
*/
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Fecha en el pasado
include_once('../private/interventores/verifica.php');

//$nivel_acceso=11; if ($nivel_acceso <= $_SESSION['usuario_nivel']){
if (empty($_SESSION['numero_vot'])) {
    header("Location: $url_vot?error_login=5");
    exit;
}

$contador = 0;
?>



<div class="col-md-3 col-xl-2">

  <nav class="nav-side-menu bg-blue flex-md-column flex-row">
    <div class="brand">menú</div>
    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>

    <div class="menu-list">

      <ul id="menu-content" class="menu-content collapse out">

  <li>
        <a href="interventores.php?c=<?php echo encrypt_url('interventores/log_out/id=SD',$clave_encriptacion) ?>"><i class="fa fa-sign-out fa-lg"> </i>  <?= _("Desconexión") ?></a>
</li>
      </ul>
      </div>
      </nav>


    </div>


    <div class="col-md content_interventores">
      <div class="card contenido">

        <div class="card-header-votaciones "> <h1 class="card-title"><?= _("Ha accedido a la votación") ?> <strong>" <?php echo $_SESSION['nombre_votacion']; ?> "</strong> <?= _("como interventor") ?></h1> </div>


      	<div class="card-body">




                    <!--Comiezo-->

                    <h4><span class="text-warning"> <?= _("Recuerde que quedara registrado en todos los votos que incluya los interventores que lo estan haciendo") ?></span></h4>

                    <h4> <?= _("Esta votación necesita la validacion de") ?> <strong> <?php echo $_SESSION['numero_inter']; ?></strong> <?= _("interventores para permitir incluir datos de las votaciones en urna") ?> </h4>


                    <?php


                    if ($_SESSION['numero_inter'] != 1) {
                        echo " <div class=\"alert alert-success\"> " . _("Se ha registrado el primer interventor que ejerce las funciones de presidente de mesa y su nombre es").": " . $_SESSION['nombre_inter_0'] . " " . $_SESSION['apellidos_inter_0'] . " </div> ";
                        $faltan_validar = $_SESSION['numero_inter'];
                        //empezamos con el 1, ya que el primero lo tenemos ya egistrado en la sesison
                        $contador = 1;
                        for ($i = 1; $i < $faltan_validar; $i++) {

                            $ID_inter = "ID_inter_" . $i;
                            if (isset($_SESSION[$ID_inter])) {

                                $usuario = "usuario_" . $i;
                                $nombre_inter = "nombre_inter_" . $i;
                                $apellidos_inter = "apellidos_inter_" . $i;

                                echo " <div class=\"alert alert-success\">" . _("registrado el Interventor") . $_SESSION[$nombre_inter] . " " . $_SESSION[$apellidos_inter] . " " . _("con nombre de usuario") . " " . $_SESSION[$usuario] . "<br/> </div>";

                                $contador = $contador + 1;
                            } else {
                                ?>


                                <div id="caja_<?php echo $i; ?>">


                                <div class="row">
                                  <div class="col-sm-2"></div>
                                  <div class="col-sm-8">
                                    <fieldset>
                                      <legend class="text-info"><?= _("Registro del interventor") ?> <?php echo $i + 1; ?> <?= _("de") ?> <?php echo $_SESSION['numero_inter']; ?></legend>

                                    <form name="form1" method="post" action=""  class="well form-horizontal" onSubmit="GrabarInterventor_<?php echo $i; ?>();
                                                        return false">

                                        <div class="form-group row">

                                            <label for="usuario"  class="col-sm-3 control-label"><?= _("Usuario Interventor") ?> <?php echo $i + 1; ?></label>
                                            <div class="col-sm-5">
                                                <input type="text" name="usuario_<?php echo $i; ?>" id="usuario_<?php echo $i; ?>"  class="form-control">
                                            </div>
                                          </div>

                                        <div class="form-group row">
                                            <label for="passwordx"  class="col-sm-3 control-label" ><?= _("Password interventor") ?> <?php echo $i + 1; ?></label>
                                            <div class="col-sm-5">
                                                <input type="password" name="password_<?php echo $i; ?>" id="password_<?php echo $i; ?>"  class="form-control">
                                            </div>
                                          </div>
                                            <div class="row">
                                              <div class="col-sm-12">
                                                <input type="submit" name="regsitrar" id="regsitrar" value="Validar interventor  <?php echo $i + 1; ?> " class="btn btn-primary btn-block"  >
                                              </div>
                                            </div>


                                        <input name="n_interventor" type="hidden" id="n_interventor" value="<?php echo $i; ?>">

                                    </form>
                                    </fieldset>
                                    </div>
                                    <div class="col-sm-2"></div>
                                  </div>

                                    <script type="text/javascript">
                                        function GrabarInterventor_<?php echo $i; ?>() {

                                            //Tomas el valor del campo msg
                                            var usuario = $("#usuario_<?php echo $i; ?>").val();
                                            var password = $("#password_<?php echo $i; ?>").val();
                                            var n_interventor = <?php echo $i; ?>;
                                            var votacion1 = <?php echo $_SESSION['numero_vot']; ?>;

                                            //Si empieza el ajax muestro el loading
                                            $("#loading_2").ajaxStart(function () {
                                                $("#loading_2").show();
                                            });

                                            //Cuando termina el ajax oculta el loading
                                            $("#loading_2").ajaxStop(function () {
                                                $("#loading_2").hide();
                                            });

                                            //Se envian los datos a url y al completarse se recarga el muro
                                            //con la nueva informacion
                                            $.ajax({

                                                url: 'aux_blog.php?i=add_interventor',
                                                data: 'usuario=' + usuario + '&password=' + password + '&votacion1=' + votacion1 + '&n_interventor=' + n_interventor,
                                                type: 'post',
                                                error: function (obj, msg, obj2) {
                                                    alert(msg);
                                                },
                                                success: function (data) {
                                                    //$(".datos_BBDD").html('' + data +' ');
                                                    if (data == 'Error1') {

                                                        $(".datos_BBDD_<?php echo $i; ?>").html('<div class="alert alert-warning"> ¡¡' + data + '!!<br/> <?= _("hay un error tipo 1, usuario, password o tipo de interventor incorrectos") ?> </div>');// Incluimos el dato que nos devuelve el php con el nombre del lo que se ha incluido correctamente (no funciona)
                                                    } else if (data == 'Error2') {
                                                        $(".datos_BBDD_<?php echo $i; ?>").html('<div class="alert alert-warning"> ¡¡' + data + '!!<br/><?= _("Este usuario ya esta autentificado como interventor, introduzca otro") ?> </div>');// Incluimos el dato que nos devuelve el php con el nombre del lo que se ha incluido correctamente (no funciona)
                                                    } else if (data == 'Error3') {
                                                        $(".datos_BBDD_<?php echo $i; ?>").html('<div class="alert alert-warning"> ¡¡' + data + '!!<br/><?= _("Faltan datos") ?></div>');// Incluimos el dato que nos devuelve el php con el nombre del lo que se ha incluido correctamente (no funciona)
                                                    } else if (data == 'Error4') {
                                                        $(".datos_BBDD_<?php echo $i; ?>").html('<div class="alert alert-warning"> ¡¡' + data + '!!<br/><?= _("Esta votación no admite interventores") ?></div>');// Incluimos el dato que nos devuelve el php con el nombre del lo que se ha incluido correctamente (no funciona)
                                                    } else {

                                                        $(".loading").hide("slow"); //escondemos el efecto de cargando (no funciona)
                                                        $("#caja_<?php echo $i; ?>").hide("slow");	///escondemos donde pone lo de votar
                                                        $(".datos_BBDD_<?php echo $i; ?>").html('' + data + ' ');// Incluimos el dato que nos devuelve el php con el nombre del lo que se ha incluido correctamente (no funciona)
                                                        loadAcceso();
                                                    }
                                                }
                                            });
                                        }
                                        ;
                                    </script>
                                </div>
                                <div class="datos_BBDD_<?php echo $i; ?>"> </div>
                                <?php
                            }
                        }
                    } else if ($_SESSION['numero_inter'] == 1) {
                        echo "<br/><br/>";
                        echo _("en esta votación puede acceder con un solo interventor para añadir papeletas o gestionar votantes") . "<br/>";

                        if ($_SESSION['tipo'] == 1) {
                            $dir = "primarias_vota";
                        } else if ($_SESSION['tipo'] == 2) {
                            $dir = "vut";
                        } else if ($_SESSION['tipo'] == 3 or $_SESSION['tipo'] == 5 or $_SESSION['tipo'] == 6 or $_SESSION['tipo'] == 7) {
                            $dir = "encuesta_vota";
                        } else {

                        }

                        $activo = '<a href="interventores.php?c='.encrypt_url('interventores/'.$dir.'/idvot='.$_SESSION['numero_vot'],$clave_encriptacion).' " class="btn btn-primary btn-block" >'. _("Introducir votos manualmente").' <span class="fa fa-thumbs-up  text-warning"></span></a>';
                        $censos= '<a href="interventores.php?c='.encrypt_url('interventores/votantes_listado_multi/idvot='.$_SESSION['numero_vot'].'&cen=com&lit=si',$clave_encriptacion).'" class="btn btn-primary btn-block"  >' . _("Gestionar CENSO") . ' <span class="fa fa-thumbs-up   text-warning"></span></a></div>';
                    ?>
                    <div class="row">
                      <div class="col-md-6">
                        <?php
                            echo  $activo ;
                            ?>
                      </div>
                      <div class="col-md-6">
                        <?php
                            echo $censos;
                            ?>
                      </div>
                    </div>
                    <?php

                    } else {
                        echo"<div class=\"alert alert-danger\">" . _("Ups!!! algo raro esta pasando, vuelva a acceder") . "</div>";
                    }
                    echo "<br/>";
                    ?>

                    <div id="acceso"></div>
                    <?php
//echo "<pre>";
//print_r($_SESSION);
//echo "</pre>";
                    ?>
                    <!--Final-->
                </div>



            </div>



        <script type="text/javascript">
              function loadAcceso() {
                    //Funcion para cargar el muro

                  $("#acceso").load('aux_blog.php?i=acceso');

                                        }

        </script>

        <?php
        if ($_SESSION['numero_inter'] == $contador) {
            //echo "Ya estan validados todos los interventores para esta votación<br/>";
            ?>
            <script type="text/javascript">
                $(document).ready(function () {
                    loadAcceso();
                });
            </script>
            <?php
        }
        ?>

                                <!--<script type="text/javascript">
                                $(document).ready(function(){
                        loadAcceso();
                                 });
                                 </script>-->

    </body>
</html>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###                                                         ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que lista los votantes que aún no han votado en una votación. Este archivo se usa en varias páginas
*/
///// este scrip se usa en varias paginas

if ($var_carga == true) {
    if (isset($variables['id_nprov'])) {
        fn_filtro($con, $variables['id_nprov']);
    } else {
        $id_nprov = "";
    }
    $inter = "";
    $nombre = "";
    $mensa = "";
    $contar = 0;
///// cogemos los datos de la encuesta

    $result_vot = mysqli_query($con, "SELECT id_provincia, activa,nombre_votacion,tipo_votante, id_grupo_trabajo, demarcacion,id_ccaa, id_municipio  FROM $tbn1 where id=$idvot");
    $row_vot = mysqli_fetch_row($result_vot);

    $id_provincia_vot = $row_vot[0];
    $id_ccaa_vot = $row_vot[6];
    $activa = $row_vot[1];
    $tipo_votante = $row_vot[3];
    $id_grupo_trabajo = $row_vot[4];
    $id_municipio = $row_vot[7];

    if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } elseif (isset($_SERVER['HTTP_VIA'])) {
        $ip = $_SERVER['HTTP_VIA'];
    } elseif (isset($_SERVER['REMOTE_ADDR'])) {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    for ($i = 0; $i < $_SESSION['numero_inter']; $i++) {  //metemos el dato de os interventores que añaden este voto
        $id_inter = "ID_inter_" . $i;
        $inter .= $_SESSION[$id_inter];
        $inter .= "_";
    }

    $inter = trim($inter, '_'); //quitamos el ultimo _
    $ip = $ip . "+" . $nombre . "+" . $inter;  //añadimos la ip y quien ha apuntado la votacion presencial
////si vota presencial metemos el registro

    if (isset($variables['votacion']) == "ok") {
        $tipo_voto = 5;
        //$id = $id;
        $id=  $variables['id'];
        $usuarios_consulta = mysqli_query($con, "SELECT ID FROM $tbn2 where id_votante='$id' and id_votacion='$idvot'") or die(mysqli_error());

        $total_encontrados = mysqli_num_rows($usuarios_consulta);

        mysqli_free_result($usuarios_consulta);


        if ($total_encontrados != 0) {

            $mensa = "<div class=\"alert alert-warning\">¡¡¡Error!!! <br>El Usuario ya está registrado  o ha votado, operacion incorrecta.</div>";
        } else {



            $result_votante = mysqli_query($con, "SELECT correo_usuario,tipo_votante,id_provincia,nif,nombre_usuario FROM $tbn9 where id=$id");
            $row_votante = mysqli_fetch_row($result_votante);

            $correo_usuario = $row_votante[0];
            $tipo_usuario = $row_votante[1];
            $id_provincia_usu = $row_votante[2];
            $dni = $row_votante[3];
            $nombre_usuario = $row_votante[4];
            // ALTA del usuario en la lista de los que han votado para que no pueda volver a votar
            $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
            $cad = "";
            for ($i = 0; $i < 6; $i++) {
                $cad .= substr($str, rand(0, 62), 1);
            }
            $time = microtime(true);
            $timecad2 = $time . $cad;
            $user_id = hash("sha256", $timecad2);
            $fecha_env = date("Y-m-d H:i:s");

            $insql_votacion = "insert into $tbn2 (ID, id_provincia, 	id_votante, 	id_votacion, 	tipo_votante, 	fecha, 	correo_usuario, forma_votacion,ip ) values (\"$user_id\",  \"$id_provincia_usu\",  \"$id\", \"$idvot\", \"$tipo_votante\", \"$fecha_env\", \"$correo_usuario\", \"$tipo_voto\",\"$ip\")";
            //$inres = @mysqli_query($con, $insql_votacion) or die("<strong><font color=#FF0000 size=3>  Imposible añadir. Cambie los datos e intentelo de nuevo.</font></strong>");
            $mens = "error añadido de votante de forma presencial ";
            $result = db_query($con, $insql_votacion, $mens);
            if (!$result) {
                echo "<div class=\"alert alert-danger\"><strong> UPSSS!!!!<br/>esto es embarazoso, hay un error  </strong> </div>";
            } else {
                $mensa .= "<div class=\"alert alert-success\">Actualizado correctamente $nombre_usuario con nif $dni como votante presencial </div>";
            }
        }
    }

//aqui termina el scrip de bloqueo de voto
    if ($row_vot[5] == 1) {
        $id_provincia_url = $id_nprov;
        $sql = "SELECT ID,nombre_usuario , nif,tipo_votante
 FROM $tbn9 a
 WHERE  NOT EXISTS (
	 SELECT *
	 FROM $tbn2 b
	 WHERE a.id=b.id_votante and b.id_votacion=$idvot
 ) and a.tipo_votante <=$tipo_votante and a.id_provincia = '$id_provincia_url' ";
    } else if ($row_vot[5] == 2) {
        $sql = "SELECT ID,nombre_usuario , nif,tipo_votante
 FROM $tbn9 a
 WHERE  NOT EXISTS (
	 SELECT *
	 FROM $tbn2 b
	 WHERE a.id=b.id_votante and b.id_votacion=$idvot
 ) and a.tipo_votante <=$tipo_votante and a.id_ccaa = '$id_ccaa_vot' ";
    } else if ($row_vot[5] == 3) {
        $sql = "SELECT ID,nombre_usuario , nif,tipo_votante
 FROM $tbn9 a
 WHERE  NOT EXISTS (
	 SELECT *
	 FROM $tbn2 b
	 WHERE a.id=b.id_votante and b.id_votacion=$idvot
 ) and a.tipo_votante <=$tipo_votante and a.id_provincia = '$id_provincia_vot' ";
    } else if ($row_vot[5] == 7) {
        $sql = "SELECT ID,nombre_usuario , nif,tipo_votante
 FROM $tbn9 a
 WHERE  NOT EXISTS (
	 SELECT *
	 FROM $tbn2 b
	 WHERE a.id=b.id_votante and b.id_votacion=$idvot
 ) and a.tipo_votante <=$tipo_votante and a.id_municipio = '$id_municipio' ";
    } else {
        ///falta los grupos    $sql = "SELECT a.ID, a.nombre_usuario , a.correo_usuario,a.tipo_votante FROM $tbn9 a,$tbn6 b  WHERE (a.ID= b.id_usuario) and id_grupo_trabajo='$id_grupo_trabajo' and a.tipo_votante <='$tipo_votante' ";

        $sql = "SELECT a.ID, a.nombre_usuario , a.nif,a.tipo_votante
 FROM $tbn9 a,$tbn6 c
 WHERE  NOT EXISTS (
	 SELECT *
	 FROM $tbn2 b
	 WHERE a.id=b.id_votante  and b.id_votacion=$idvot
 ) and (a.ID=c.id_usuario) and a.tipo_votante <=$tipo_votante and c.id_grupo_trabajo='$id_grupo_trabajo' ";
    }





    $result = mysqli_query($con, $sql);
    ?>

    <h2><?= _("Votación de") ?> <?php echo "$row_vot[2]" ?> </h2>
    <p><?php
        if (isset($mensa)) {
            echo "$mensa";
        }
        ?></p>


    <h3 class="card-title"><?= _("Listado del censo que NO ha votado") ?> </h3>
<h4>
  <?php if ($es_municipal == false){ ?>
      <?php if ($row_vot[5] == 1 and $id_nprov != "") { ?>
         <?= _("para la provincia") ?> <strong> <?php echo $id_nprov; ?></strong> <?= _("y tipo de votación") ?>
        <?php } else if ($row_vot[5] == 2) { ?>
            <?= _("para la comunidad autonoma") ?> <strong><?php echo $row_vot[6]; ?></strong> <?= _("y tipo de votación") ?>
        <?php } else if ($row_vot[5] == 3) { ?>
            <?= _("para la provincia") ?> <strong><?php echo $row_vot[0]; ?></strong> <?= _("y tipo de votación") ?>
        <?php } else { ?>
           <?= _("Tipo de votación") ?>  <?php }
         }?>
    <?php
        if ($row_vot[3] == 1) {
            echo _("solo para socios");
        } else if ($row_vot[3] == 2) {
            echo _("solo pata socios y simpatizantes");
        } else if ($row_vot[3] == 3) {
            echo _("abierta");
        }
        ?> </h4>



    <?php
    if ($row = mysqli_fetch_array($result)) {
        ?>
        <form name="form1" method="post" action="<?php $_SERVER['PHP_SELF'] ?>">

          <table id="tabla1<?php echo $cen; ?>" class="table table-striped table-bordered dt-responsive nowrap" data-page-length="25">

                <thead>
                    <tr>
                        <th width="50%"><?= _("Nombre") ?></th>
                        <th width="15%"><?= _("Nif") ?></th>
                        <th width="10%"><?= _("Tipo") ?></th>
                        <th width="25%"><?= _("VOTA PRESENCIAL") ?></th>


                    </tr>
                </thead>

                <tbody>
                    <?php
                    mysqli_field_seek($result, 0);
                    do {
                        ?>
                        <tr>
                            <td><?php echo "$row[1]" ?></td>
                            <td><?php echo "$row[2]" ?></td>
                            <td><?php
                                if ($row[3] == 1) {
                                    echo _("socio");
                                } else if ($row[3] == 2) {
                                    echo _("simpatizante verificado");
                                } else if ($row[3] == 3) {
                                    echo _("simpatizante");
                                } else if ($row[3] == 5) {
                                    echo _("Aqui hay un error");
                                }
                                ?></td>
                            <td>
                                <a href="interventores.php?c=<?php echo encrypt_url('interventores/votantes_listado_multi/idvot='.$idvot.'&votacion=ok&id='.$row[0].'&id_nprov='.$id_nprov.'&cen='.$cen.'&lit='.$lit,$clave_encriptacion) ?> "  class="btn btn-warning btn-block"><?= _("Vota de forma presencial") ?> </a>
                            </td>

                        </tr>
                        <?php
                    } while ($row = mysqli_fetch_array($result));
                    ?>

                </tbody>
            </table>

        </form>

        <?php
    } else {
        if ($id_provincia_url == "") {
          echo '<div class="alert alert-warning">' . _('Escoja la provincia para la que quiere ver el censo') . '</div>';
      } else {
          echo '<div class="alert alert-warning">'  . _('¡No se ha encontrado votantes para esta votación!') . '</div>' ;
        }
    }
} else {
    echo _("error acceso");
}
?>

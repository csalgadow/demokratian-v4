<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo con los mensajes de error que se enseñan en el formulario de acceso
*/
$error_login_ms[0] = _("No se puede conectar");
$error_login_ms[1] = _("No se puede acceder a la base de datos");
$error_login_ms[2] = _("Usuario, password no existen, o tipo de interventor no adecuado por favor vuelva a intentarlo");
$error_login_ms[3] = _("Pasword incorrecto.");
$error_login_ms[4] = _("No es un Usuario registrado");
$error_login_ms[5] = _("No está autorizado a realizar esa acción");
$error_login_ms[6] = _("No está autorizado, por favor, regístrese");
$error_login_ms[7] = _("Esta votación no permite interventores");
$error_login_ms[8] = _("Ups, esta encuesta no está correctamente configurada para tener interventores");
$error_login_ms[10] = _("Ummmm, parece que es un robot, o al menos asi lo detecta el sistema. ¿Ha marcado la casilla de verificación?");
$error_login_ms[11] = _("Lo sentimos, su sesión ha caducado"). ". <br/>" ._("Ha pasasdo mas de")." ".($tiempo_session/60)*3 . " "._("minutos sin que realice ninguna acción") ;
$error_login_ms[12] = _("Ummmm, parece que está intentando acceder de forma ilegal, o al menos así lo detecta el sistema. ¿Está accediendo desde la página correcta?");
?>

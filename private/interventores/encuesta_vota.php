<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que presenta las opciones que se pueden votar en el tipo encuesta
*/
if(!isset($cargaI)){
  $cargaI =false;
  exit;
}
if($cargaI!="OK"){
  exit;
}else{

require_once("../private/config/config.inc.php");
require_once("../private/config/config-votantes.inc.php");
require_once("../private/inc_web/conexion.php");
include_once('../private/interventores/seguri_inter.php');
?>
<link href="temas/<?php echo "$tema_web"; ?>/css/encuestaStyle.css" rel="stylesheet">
<!--Comiezo-->


<div class="col-md-3 col-xl-2">
        <?php include("../private/interventores/menu.php"); ?>
      </div>


          <div class="col-md content_interventores">
        <div class="card contenido">
	<div class="card-header-votaciones "> <h1 class="card-title"><?php echo "$nombre_votacion"; ?></h1> </div>


	<div class="card-body">


                    <form id="numeroOpciones" name="numeroOpciones" class="form-horizontal well" action="interventores.php?c=<?php echo encrypt_url('interventores/encuesta_emite_voto/idvot='.$idvot,$clave_encriptacion) ?>" method="post">

                        <!-- Contenedor general -->
                        <div class="contenedor">
                          <?php if($votoPonderado == true and ($tipo ==5 or $tipo ==6)){  ?>
                          <div class="row caja_encuesta">
                            <div class="col-md-12">
                              <h5 class="text-info"><?= _("Esta votacion es una votacion ponderada") ?>. <?= _("Escoja tipo de votante") ?>: </h5>

                                <table border="0" class="table table-bordered table-condensed table-striped" id="table_valor" data-toggle="checkboxes"  data-max="1">
                                        <tr>
                                            <td align="left">
                                                  <input type="radio" name="valor" value="<?php echo $valor_tipo_1; ?>" id="encuesta_1" checked/>
                                                    <label for="valor_1" > <?php echo $nombre_tipo_1 . " "._("con voto de valor")." : ".$valor_tipo_1; ?>  </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                  <input type="radio" name="valor" value="<?php echo $valor_tipo_2; ?>" id="encuesta_2" />
                                                  <label for="encuesta_2">  <?php echo $nombre_tipo_2 ." "._("con voto de valor")." : ".$valor_tipo_2; ?>  </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                  <input type="radio" name="valor" value="<?php echo $valor_tipo_3; ?>" id="valor_3" />
                                                <label for="valor_3" >  <?php echo $nombre_tipo_3 ." "._("con voto de valor")." : ".$valor_tipo_3; ?>  </label>
                                            </td>
                                        </tr>
                                </table>
                            </div>
                          </div>
                        <?php } ?>

                            <div class="row caja_encuesta">
                              <div class="col-md-6">
                              <h5 class="text-info text-center"><?= _("Lista de opciones para esta votación") ?></h5>
                              </div>
                              <div class="col-md-6">
                                <h5 class="text-info text-center"><?= _("Puede escoger") ?> <?php echo "$numero_opciones"; ?> <?= _("opciones") ?>
                                  </div>
                                </div>
                                <div id="formulario" class="caja_encuesta">
                                  <fieldset>
                                    <?php
                                    if($tipo ==6 or $tipo ==7){ ?>
                                      <table border="0" class="table table-bordered table-condensed table-striped" id="table" data-toggle="checkboxes"  data-max="1">
                                              <tr>
                                                  <td align="left">

                                                        <input type="checkbox" name="encuesta_1" value="SI" id="encuesta_1" />

                                                          <label for="encuesta_1" <?php if($tipo ==6 or $tipo ==7){ echo 'class="caja_binaria"';} ?>> <?php echo _("SI") ?> </label>



                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td align="left">

                                                        <input type="checkbox" name="encuesta_1" value="NO" id="encuesta_2" />

                                                        <label for="encuesta_2" <?php if($tipo ==6 or $tipo ==7){ echo 'class="caja_binaria"';} ?>>  <?php echo _("NO") ?></label>

                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td align="left">

                                                        <input type="checkbox" name="encuesta_1" value="BLANCO" id="encuesta_3" />
                                                      <label for="encuesta_3" <?php if($tipo ==6 or $tipo ==7){ echo 'class="caja_binaria"';} ?>>    <?php echo _("BLANCO") ?></label>

                                                  </td>
                                              </tr>

                                      </table>
                            <?php   }else{
                                    $sql = "SELECT * FROM $tbn7 WHERE id_votacion = '$idvot' ORDER BY ID";
                                    $result = mysqli_query($con, $sql);
                                    if ($row = mysqli_fetch_array($result)) {

                                        ?>



                                            <table border="0" class="table table-bordered table-condensed table-striped" id="table" data-toggle="checkboxes"  data-max="<?php echo "$numero_opciones"; ?>">
                                                <?php
                                                mysqli_field_seek($result, 0);
                                                do {
                                                    ?>
                                                    <tr>
                                                        <td align="left">
                                                            <label >

                                                              <input type="checkbox" name="encuesta_<?php echo "$row[0]" ?>" value="<?php echo "$row[0]"; ?>" id="encuesta_<?php echo "$row[0]"; ?>" />  <?php if ($row[12] == "") { ?><?php } else { ?>
                                                                  <img src="<?php echo $upload_cat; ?>/<?php echo "$row[12]"; ?>" alt="<?php echo "$row[3]"; ?>" width="100" height="100" class="img-circle img-profile" /> <?php } ?>
                                                                <?php echo " $row[3]" ?>

                                                            </label>

                                                        </td>
                                                    </tr>

                                                    <?php
                                                } while ($row = mysqli_fetch_array($result));
                                                ?>
                                            </table>


                                          </fieldset>
                                    </div>

                                    <p>
                                        <?php
                                    } else {
                                        echo _("¡No se ha encontrado ningún candidato!");
                                    }
                                  }
                                    ?>


                                    <input name="add_voto" type="submit" class="btn btn-lg btn-primary btn-block" id="add_voto" value="<?= _("VOTA") ?>" />
                                <!--    <input name="id_provincia" type="hidden" id="id_provincia" value="<?php echo "$id_provincia"; ?>" />
                                    <input name="id_ccaa" type="hidden" id="id_ccaa" value="<?php echo "$id_ccaa" ?>" />
                                    <input name="id_subzona" type="hidden" id="id_subzona" value="<?php echo "$id_subzona" ?>" />
                                    <input name="id_grupo_trabajo" type="hidden" id="id_grupo_trabajo" value="<?php echo "$id_grupo_trabajo" ?>" />
                                    <input name="demarcacion" type="hidden" id="demarcacion" value="<?php echo "$demarcacion" ?>" />-->
                                </p>
                                <p>&nbsp;</p>
                                <p>&nbsp;</p>


                        </div>
                        <!-- -->




                    </form>

                  </div>
                </div>
              </div>

                    <!--Final-->

                    <script type='text/javascript' src='assets/js/jquery.checkboxes-1.2.2.min.js' ></script>
            <script>
                    jQuery(function($) {
                $('#table').checkboxes('max', <?php echo "$numero_opciones"; ?>);
            });
            </script>
        <?php } ?>

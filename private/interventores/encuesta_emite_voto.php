<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que procesa la inclusión del voto en el tipo encuesta
*/
if(!isset($cargaI)){
  $cargaI =false;
  exit;
}
if($cargaI!="OK"){
  exit;
}else{

require_once("../private/config/config.inc.php");
require_once("../private/inc_web/conexion.php");
include_once('../private/interventores/seguri_inter.php');
$inter = "";
$encripta = "";

?>
<link href="temas/<?php echo "$tema_web"; ?>/css/encuestaStyle.css" rel="stylesheet">

<div class="col-md-3 col-xl-2">
        <?php include("../private/interventores/menu.php"); ?>
      </div>


          <div class="col-md content_interventores">
        <div class="card contenido">
	<div class="card-header-votaciones "> <h1 class="card-title"><?php echo "$nombre_votacion"; ?></h1> </div>


	<div class="card-body">



                    <!--Comiezo-->
                    <?php
                    if (ISSET($_POST["add_voto"])) {

//$id_votante = $_SESSION['ID'];///ver si se puede borrar
                        $idvot = fn_filtro_numerico($con, $idvot); ///ver si se puede borrar y meter el post directamente
//$valores=fn_filtro($con,$_POST['valores']);

                        for ($i = 0; $i < $_SESSION['numero_inter']; $i++) {  //metemos el dato de os interventores que añaden este voto
                            $id_inter = "ID_inter_" . $i;
                            $inter .= $_SESSION[$id_inter];
                            $inter .= "_";
                        }
                        $inter = trim($inter, '_'); //quitamos el ultimo _

                        reset($_POST);
                        if($votoPonderado == true and ($tipo ==5 or $tipo ==6)){
                            if (ISSET($_POST["valor"])) {
                              $vot=$_POST["valor"];
                            } else{
                              echo '<div class="alert alert-danger"> <strong> '. _("Hay un error, no ha seleccionado valor del voto") .  '<a href="interventores.php?c='.encrypt_url('interventores/encuesta_vota/idvot='.$idvot,$clave_encriptacion).' "> ' . _("volver") . '</a></strong></div>';
                              exit;
                            }
                            unset($_POST["valor"]);

                        } else{
                          $vot = 1;
                        }

                        unset($_POST["add_voto"]);

                        if (count($_POST)==0){
                          echo '<div class="alert alert-danger"> <strong> '. _("Hay un error, no ha seleccionado ninguna opción") .  '<a href="interventores.php?c='.encrypt_url('interventores/encuesta_vota/idvot='.$idvot,$clave_encriptacion).' "> ' . _("volver") . '</a></strong></div>';
                          exit;
                        }

                        foreach ($_POST as $k => $v)
                            $a[] = $v;
                     $datos = count($a) ;

                        if ($datos == 0) {
                            echo '<div class="alert alert-danger"> <strong> '. _("Hay un error, no ha seleccionado ninguna opción") .  '<a href="interventores.php?c='.encrypt_url('interventores/encuesta_vota/idvot='.$idvot,$clave_encriptacion).' "> ' . _("volver") . '</a></strong></div>';
                        } else {



                            $sql3 = "SELECT  seguridad,nombre_votacion FROM $tbn1 WHERE ID ='$idvot' ";
                            $resulta3 = mysqli_query($con, $sql3) or die("error: " . mysqli_error());

                            while ($listrows3 = mysqli_fetch_array($resulta3)) {
                                $seguridad = $listrows3['seguridad'];
                                $nombre_votacion = $listrows3['nombre_votacion'];
                            }



                            if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                            } elseif (isset($_SERVER['HTTP_VIA'])) {
                                $ip = $_SERVER['HTTP_VIA'];
                            } elseif (isset($_SERVER['REMOTE_ADDR'])) {
                                $ip = $_SERVER['REMOTE_ADDR'];
                            }

                          //  $forma_votacion = 3;


/////////////////////////// si podemos procesar el formulario
//if(!$error) {


                            if ($idvot != "" and $inter != "") {
                                $codi = hash("sha512", "sin clave");
                                $datos_votado = "";
                                $i = 0;

                                while ($i < $datos) {
                                    $val = fn_filtro($con, $a[$i]);



                                    //$datos_votado.="Orden $voto  | Identificador candidato -->  $val |  Valor voto ---  $vot"."<br/>"; //cojemos el array de votos para enviar por correo si es necesario

                                    $datos_votado .= $val . "-" . $vot . "#"; //cojemos el array de votos para enviar por correo si es necesario

                                    $i++;
                                }

                                //////	identofocador unico
                                $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
                                $cad = "";
                                for ($b = 0; $b < 4; $b++) {
                                    $cad .= substr($str, rand(0, 62), 1);
                                }
                                $time = microtime(true);
                                $timecad = $time . $cad;
                                $res_id = hash("sha256", $timecad);
                                /// finidentificador unico

                                $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
                                $cad = "";
                                for ($i = 0; $i < 4; $i++) {
                                    $cad .= substr($str, rand(0, 62), 1);
                                }
                                $time = microtime(true);
                                $timecad = $time . $cad;
                                $vote_id = hash("sha256", $timecad);


                                $datos_del_voto = trim($datos_votado, '#'); ///quitamos la ultima coma de la cadena para meterlo en la bbdd

                                $fecha_env = date("Y-m-d H:i:s");
                                $especial = 1; //metemos en la base de datos que ese voto ha sido incluido por interventores

                                $provincia = $_SESSION['id_localidad']; // incluimos una provincia inexistente
                                $insql = "insert into $tbn10 (ID, voto, id_votacion, id_candidato,id_provincia,especial,incluido,vote_id,codigo_val) values (\"$res_id\",\"$vot\",\"$idvot\",\"$datos_del_voto\",\" $provincia \",\"$especial\",\"$inter\",\"$vote_id\",\"$codi\")";
                                //$insql = "insert into $tbn10 (ID,voto,id_candidato,id_provincia,id_votacion,codigo_val) values (\"$res_id\",\"$vot\",\"$datos_del_voto\",".$provincia.",\"$idvot\",\"$codi\")";
                                $mens = "mensaje añadido";
                                $result = db_query($con, $insql, $mens);

                                if (!$result) {
                                    echo "<div class=\"alert alert-danger\">
   			                              <strong> UPSSS!!!!<br/>" . _("esto es embarazoso, hay un error y su votacion no ha sido registrada") . "  </strong> </div><strong>";
                                }
                                if ($result) {



                                    ////metemos el voto en un txt y lo registramos como medida de seguridad
                                    $Pollname = md5($idvot);
                                    $file = $FilePath . $Pollname . "_ballots.txt";
                                    $fh = fopen($file, 'a');
                                    fwrite($fh, $datos_votado . PHP_EOL)or die("Could not write file!");
                                    fclose($fh);  //  Cerramos el fichero

                                    $file2 = $FilePath . $Pollname . "_tally.txt";
                                    $fh2 = fopen($file2, 'r+');
                                    $búfer = fgets($fh2, 100);
                                    $partes = explode("|", $búfer);
                                    $partes1 = $partes[1] + 1;
                                    $partes = $partes[0] . "|" . $partes1;
                                    fseek($fh2, 0);
                                    fwrite($fh2, $partes)or die("Could not write file!");
                                    fclose($fh2);  //  Cerramos el fichero
                                    //// metemos el dato de que ha votado en la base de datos
                                    // echo "Ha sido recogido el voto de: ".$_SESSION['nombre_usu']."<br/>";
                                    ///// metemos una copia encriptada del voto
                                    //// primero sacamos los datos de la tabla temporal para hacer una cadena quemeteremos tambien encriptada
                                    $sql = "SELECT 	ID, datos  FROM $tbn20 where id_votacion=\"$Pollname\" ORDER BY ID DESC";
                                    $mens = "Error en la consulta a la tabla temporal";
                                    $result = db_query($con, $sql, $mens);
                                    if ($row = mysqli_fetch_array($result)) {
                                        mysqli_field_seek($result, 0);
                                        $cadena = "";
                                        do {
                                            $cadena .= $row[1] . "_";  ////generamos la caena que vamos a meter en la tabla de votos de seguridad
                                            $id_borrado = $row[0];
                                        } while ($row = mysqli_fetch_array($result));
                                    }


                                    $cadena = trim($cadena, '_'); // quitamos de la cadena el ultimo _
                                    //$id_borrado=$id_borrado-4; ///miramos el id del ultimo registro de la tabla temporal y descontamos 4 para su posterior borrado

                                    $borrado = "DELETE FROM $tbn20 WHERE ID=" . $id_borrado . " ";
                                    $mens = " error en el borrado de " . $id_borrado;
                                    $result = db_query($con, $borrado, $mens);

                                    $cadena_temp = $res_id . "+" . $datos_del_voto; //esta variable sirve para esta querry y para la querry de la tabla temporal siguiente
                                    /////// miramos si hay que encriptar la cadena de voto

                                    if ($encripta == "si") {

                                        /* require_once "Crypt/AES.php"; */

                                        //Function for encrypting with RSA
                                        // hacemos una busqueda y cargamos la clave publica
                                        $result = mysqli_query($con, "SELECT clave_publica FROM $tbn21  WHERE id_votacion='$idvot' ORDER by orden");
                                        if ($row = mysqli_fetch_array($result)) {
                                            mysqli_field_seek($result, 0);
                                            do {
                                                $clave_publica = $row[0];

                                                $cadena = rsa_encrypt($cadena, $clave_publica);
                                            } while ($row = mysqli_fetch_array($result));
                                        }
                                    }
                                    //metemos los datos codificados en la bbdd
                                    $timecad2 = $time . $cad;
                                    $ident = md5($timecad2);

                                    $shadatovoto = hash('sha256', $cadena_temp);
                                    $insql1 = "insert into $tbn19 (ID,voto,cadena,id_votacion) values (\"$ident\",\"$shadatovoto\",\"$cadena\",\"$Pollname\")";
                                    $mens1 = "ERROR en el añadido voto encriptado ";
                                    $result = db_query($con, $insql1, $mens1);

                                    //metemos el dato en la tabla temporal
                                    $cadena_temp = $res_id . "+" . $datos_del_voto;
                                    $insql2 = "insert into $tbn20 (datos,id_votacion) values (\"$cadena_temp\",\"$Pollname\")";
                                    $mens2 = "ERROR en el añadido  del voto tabla temporal";
                                    $result = db_query($con, $insql2, $mens2);
                                    ?><!--si todo va bien damos las gracias por participar-->
                                    <div class="alert alert-success">   <h3  align="center"><?= _("Ha sido incluido el voto") ?></h3> </div>

                                    <a href="interventores.php?c=<?php echo encrypt_url('interventores/encuesta_vota/idvot='.$idvot,$clave_encriptacion) ?> " class="btn btn-primary btn-block"><?= _("Incluir otro voto") ?></a>
                                    <?php
                                }
                            }
                        }
                    }
                    ?>


                    <!--Final-->


                </div>
            </div>
        </div>
      <?php } ?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###                                                         ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que genera el voto del tipo VUT
*/
if(!isset($cargaI)){
  $cargaI =false;
  exit;
}
if($cargaI!="OK"){
  exit;
}else{
require_once("../private/config/config.inc.php");
require_once("../private/inc_web/conexion.php");
include_once('../private/interventores/seguri_inter.php');

$Seats = $numero_opciones;
$idvot=$variables['idvot'];
$Ballotname = md5($idvot);
$Pollname = md5($idvot);
$valores = "";
$datos_votado = "";
?>

<div class="col-md-3 col-xl-2">
        <?php include("../private/interventores/menu.php"); ?>
      </div>


          <div class="col-md content_interventores">
        <div class="card contenido">
	<div class="card-header-votaciones "> <h1 class="card-title"><?php echo "$nombre_votacion"; ?></h1> </div>


	<div class="card-body">




                    <!--Comiezo-->

                    <h3><span class="text-warning"><?= _("Recuerde que quedara registrado en todos los votos que incluya los interventores que lo estan haciendo") ?></span></h3>

                    <p><?= _("Esto es lo que ha incluido") ?></p>


                    <p>&nbsp;</p>
                    <?php // temporalmente añadido este aviso ya que se ha quitado el sicrip que impedia recargar ?>
                    <span class="text-warning"> <?= _("¡¡OJO!! Si recarga la pagina volvera a grabar el mimo voto") ?></span>
                    <?php
                    /* temporalmente quitado, tambien en vutconfirm ya que puede interesar que se puedan meter varias papeletas iguales
                      if($_SESSION['recarga']=="recarga"){
                      echo "<div class=\"alert alert-warning\"> No recargue la pagina , por favor </div>";
                      }else{ */
                    if (ISSET($_POST["submit"])) {
                      $i=1;
                      foreach ($_POST as $v) {
                            $datos_votacion[$i] = $v;
                            $i++;
                        }

                      //  $datos_votacion = each($_POST);
                        //echo $datos_votacion[1];
                        if ($datos_votacion[1] == "--") {

                            $errores = _("No ha votado nada") . " <br>" . _("vuelva a realizar la votación");
                        } else {
                            ?>

                            <?php
                            //$id_votante = $_SESSION['usuario_id'];
                            //$idvot = fn_filtro_numerico($con, $_POST['id_vot']);
                            // $id_ccaa = $_SESSION['id_ccaa_usu'];
                            //$clave_seg = fn_filtro($con, $_POST['clave_seg']);
                            $Cands = fn_filtro($con, $_POST['Cands']);


                            if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                            } elseif (isset($_SERVER['HTTP_VIA'])) {
                                $ip = $_SERVER['HTTP_VIA'];
                            } elseif (isset($_SERVER['REMOTE_ADDR'])) {
                                $ip = $_SERVER['REMOTE_ADDR'];
                            }

                          //  $forma_votacion = 2;
                            $clave_seg = "incluido_interv";
                            /////////////////////////// si podemos procesar el formulario
                            $codi = hash("sha512", $clave_seg);
                            ?>

                            <?php
                            reset($_POST);
                          //  unset($_POST['id_vot']); //borramos la variable del post
                            unset($_POST['clave_seg']); ///borramos la otra variable para dejar solo los datos de votos
                            unset($_POST['Cands']); ///borramos la otra variable para dejar solo los datos de votos
                            $cuenta = 1;
                            foreach ($_POST as $clave => $val){
                            //while (list ($clave, $val) = each($_POST)) {
                                // echo "$clave - $val +; ";
                                $array_id = explode('__', $clave);
                                if ($clave == "submit") {

                                } else {

                                    if ($val == "--") {

                                    } else {
                                        $valores .= fn_filtro($con, $array_id[1]) . ","; ///montamos una cadena separada por comas con los id de los candidatos para meterlos en la hoja de recuento
                                    }
                                }
                                $cuenta++;
                            }
                            ?>

                            <?php
                            $valores = trim($valores, ','); ///quitamos la ultima coma de la cadena
                            $valores_fin = fn_filtro($con, $valores) . "\n";

                            $fecha_env = date("Y-m-d H:i:s");


                            $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
                            $cad = "";
                            for ($i = 0; $i < 4; $i++) {
                                $cad .= substr($str, rand(0, 62), 1);
                            }


                            $time = microtime(true);
                            $timecad = $time . $cad;
                            $res_id = hash("sha256", $timecad);

                            for ($i = 0; $i < $_SESSION['numero_inter']; $i++) {  //metemos el dato de os interventores que añaden este voto
                                $id_inter = "ID_inter_" . $i;

                                $inter .= $_SESSION[$id_inter];
                                $inter .= "_";
                            }

                            $inter = trim($inter, '_'); //quitamos el ultimo _

                            $especial = 1; //metemos en la base de datos que ese voto ha sido incluido por interventores
                            $insql = "insert into $tbn15 (ID,voto,id_candidato,id_provincia, id_votacion,codigo_val,especial,incluido) values (\"$res_id\",\"$valores_fin\",\"0\",". $_SESSION['id_localidad'] . ", \"$idvot\",\"$codi\",\"$especial\",\"$inter\")";
                            $mens = "mensaje añadido";
                            $result = db_query($con, $insql, $mens);

                            if (!$result) {
                                echo "<strong><font color=#FF0000 siz<br/> UPSSS!!!!<br/>" . _("esto es embarazoso, hay un error y su votacion no ha sido registrada") . " </font></strong>";
                            }
                            if ($result) {
                                //si hay resultado del proceso anterior ejecutamos el resto
                                // abrimos el fichero y escribmos los datos
                                $fp = fopen($FilePath . $Ballotname . "_ballots.txt", "a+");
                                // fputs($fp,$NuBallot.$cr);
                                if (fwrite($fp, $valores_fin) === FALSE) {
                                    $errores = _("No se puede escribir al archivo") . " (" . $nombre_archivo . ")";
                                    exit;
                                }
                                fclose($fp);

                                //require("dctally.php");
                                $Ballots = NULL;

                                $_SESSION['recarga'] = "recarga"; //añadimos a la sesion para que no vuevan a recargar
                                ?>
                                <div class="table-responsive">
                                    <table width="90" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th width="17%">
                                                    <?= _("Orden") ?></th>
                                                <th width="83%"><?= _("Candidato - Opción") ?></th> </tr>
                                            <?php
                                            $valoresarray = explode(',', $valores);
                                            $numrows = count($valoresarray);
                                            for ($i = 0; $i < $numrows; $i++) {
                                                ?>
                                                <tr><td>
                                                        <?php
                                                        $cuenta = $i + 1;
                                                        echo "$cuenta";
                                                        ?>

                                                    </td ><td>
                                                        <?php
                                                        $result2 = mysqli_query($con, "SELECT nombre_usuario FROM $tbn7 WHERE id_vut = '$valoresarray[$i]' and id_votacion='$idvot'");
                                                        $linea = mysqli_fetch_row($result2);
                                                        echo $linea[0];
                                                        ?>
                                                    </td></tr>
                                            <?php } ?>

                                    </table></div>
                                <!--si todo va bien damos las gracisa por participar-->
                                <div class="alert alert-success">
                                    <h3  align="center"><?= _("Datos incluidos") ?></h3> </div>
                                <div class="alert alert-info">
                                   <p><strong><?= _("Puede seguir añadiendo papeletas") ?> <a href="interventores.php?c=<?php echo encrypt_url('interventores/vut/idvot='.$idvot,$clave_encriptacion) ?>" class="btn btn-secondary btn-block"  ><?= _("Incluir otro voto") ?></a> </strong></p></div>


                                <?php
                            }
                        }
                    } else {

                        $errores = "<div class=\"alert alert-warning\">" . _("no ha accedido de forma correcta a esta votación") . "</div>";
                    }
                    /* llave de cierre temporalmente quitado, tambien en vut.php ya que puede interesar que se puedan meter varias papeletas iguales
                      } */
                    ?>
                    <!--Final-->

                  </div>
                             </div>
                           </div>

                <?php }?>

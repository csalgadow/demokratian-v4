<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo con el menú de navegación de los interventores
*/
 ?>
<!--class="glyphicon glyphicon-user"-->

<nav class="nav-side-menu bg-blue flex-md-column flex-row">
  <div class="brand">menú</div>
  <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>

  <div class="menu-list">

    <ul id="menu-content" class="menu-content collapse out">

                <li> <a href="interventores.php?c=<?php echo encrypt_url('interventores/votantes_listado_multi/idvot='.$_SESSION['numero_vot']."&cen=com&lit=si",$clave_encriptacion) ?>" ><?= _("Censo de votantes") ?></a></li>
                <!--<li> <a href="interventores.php?c=<?php echo encrypt_url('interventores/votantes_listado_multi/idvot='.$idvot."&id_nprov=".$_SESSION['id_localidad']."&cen=pres&lit=".$lit,$clave_encriptacion) ?>" ><?= _("Votantes en urna") ?></a></li>-->
                <li> <a href="interventores.php?c=<?php echo encrypt_url('interventores/votantes_listado_multi/idvot='.$idvot."&id_nprov=".$_SESSION['id_localidad']."&cen=pres&lit=si",$clave_encriptacion) ?>" ><?= _("Votantes en urna") ?></a></li>
                <?php if ($_SESSION['tipo'] == 1) { ?>
                    <li> <a href="interventores.php?c=<?php echo encrypt_url('interventores/primarias_vota/idvot='.$idvot,$clave_encriptacion) ?> " ><?= _("Incluir votos") ?></a></li>
                    <li> <a href="interventores.php?c=<?php echo encrypt_url('interventores/datos_incluidos_primarias/idvot='.$idvot,$clave_encriptacion) ?> "  ><?= _("Ver los datos incluidos manualmente") ?></a></li>

                <?php } ?>
                <?php if ($_SESSION['tipo'] == 2) { ?>
                    <li> <a href="interventores.php?c=<?php echo encrypt_url('interventores/vut/idvot='.$idvot,$clave_encriptacion) ?>"  ><?= _("Incluir votos") ?></a></li>
                    <li> <a href="interventores.php?c=<?php echo encrypt_url('interventores/datos_incluidos_vut/idvot='.$idvot,$clave_encriptacion) ?> " ><?= _("Ver los datos incluidos manualmente") ?></a></li>

                <?php } ?>
                <?php if ($_SESSION['tipo'] == 3 or $_SESSION['tipo'] == 5 or $_SESSION['tipo'] == 6 or $_SESSION['tipo'] == 7) { ?>
                    <li> <a href="interventores.php?c=<?php echo encrypt_url('interventores/encuesta_vota/idvot='.$idvot,$clave_encriptacion) ?> " ><?= _("Incluir votos") ?></a></li>
                    <li> <a href="interventores.php?c=<?php echo encrypt_url('interventores/datos_incluidos_primarias/idvot='.$idvot,$clave_encriptacion) ?>" ><?= _("Ver los datos incluidos manualmente") ?></a></li>

                <?php } ?>
                <li><a href="interventores.php?c=<?php echo encrypt_url('interventores/log_out/id=SD',$clave_encriptacion) ?>"><i class="fa fa-sign-out fa-lg"> </i>  <?= _("Desconexión") ?></a></li>
              </ul>
          </div>
      </nav>

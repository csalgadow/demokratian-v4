<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo se carga en inicio_interventores.php y enseña, si ya estan todos validados, los enlaces para ver los censos o incluir votos
*/
if(!isset($cargaI)){
  $cargaI =false;
  exit;
}
if($cargaI!="OK"){
  exit;
}else{

include('../private/interventores/verifica.php');
//$nivel_acceso=11; if ($nivel_acceso <= $_SESSION['usuario_nivel']){
if (empty($_SESSION['numero_vot'])) {
    header("Location:" .$url_vot."?c=acceso_interventores&error_login=5");
    exit;
}
?>
<?php
$contar = 0;

for ($i = 0; $i < $_SESSION['numero_inter']; $i++) {
    $id_inter = "ID_inter_" . $i;
    if (isset($_SESSION[$id_inter])) {
        $contar = $contar + 1;
    }
}


if ($_SESSION['numero_inter'] == $contar) {
    echo _("Ya estan validados todos los interventores para esta votación") . "<br/>";

    if ($_SESSION['tipo'] == 1) {
        $dir = "primarias_vota";
    } else if ($_SESSION['tipo'] == 2) {
        $dir = "vut";
    } else if ($_SESSION['tipo'] == 3) {
        $dir = "encuesta_vota";
    } else {

    }

    $activo = '<a href="interventores.php?c='.encrypt_url('interventores/'.$dir.'/idvot='.$_SESSION['numero_vot'],$clave_encriptacion).' " class="btn btn-primary btn-block" >'. _("Introducir votos manualmente").' <span class="fa fa-thumbs-up  text-warning"></span></a>';
    $censos= '<a href="interventores.php?c='.encrypt_url('interventores/votantes_listado_multi/idvot='.$_SESSION['numero_vot'].'&cen=com&lit=si',$clave_encriptacion).'" class="btn btn-primary btn-block"  >' . _("Gestionar CENSO") . ' <span class="fa fa-thumbs-up   text-warning"></span></a></div>';

    ?>
    <div class="row">
      <div class="col-md-6">
        <?php
            echo  $activo ;
            ?>
      </div>
      <div class="col-md-6">
        <?php
            echo $censos;
            ?>
      </div>
    </div>
    <?php
    } else {
    $dif = $_SESSION['numero_inter'] - $contar;
    if ($dif != 1) {
        $plural = "es";
    }
    echo"<br/><br/><p>" . _("Aun tiene que validar") . " " . $dif . " interventor" . $plural . " " . _("para poder acceder a introducir datos") . " </p>";
}
}
?>

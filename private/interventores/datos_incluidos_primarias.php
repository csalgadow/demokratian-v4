<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que permite ver los votos que se han metido a mano en la votación tipo PRIMARIAS y ENCUESTA y que interventores lo han hecho
*/
if(!isset($cargaI)){
  $cargaI =false;
  exit;
}
if($cargaI!="OK"){
  exit;
}else{

require_once("../private/config/config.inc.php");
require_once("../private/inc_web/conexion.php");
include_once('../private/interventores/seguri_inter.php');
?>


<div class="col-md-3 col-xl-2">
        <?php include("../private/interventores/menu.php"); ?>
      </div>


          <div class="col-md content_interventores">
        <div class="card contenido">
	<div class="card-header-votaciones "> <h1 class="card-title"><?= _("Votos incluidos manualmente en la votación") ?></h1> </div>


	<div class="card-body">

                    <!--Comiezo-->

                              <?php
                    // Votos en urna
                    $sql = "select distinct vote_id from $tbn10 WHERE id_votacion = '$idvot' and especial=1";
                    $result = mysqli_query($con, $sql);
                    $urna = mysqli_num_rows($result); // obtenemos el número de filas
                    ?>

                    <div class="jumbotron">
                        <?php if ($urna != 0) { ?>
                            <p class="lead"><?= _("Votos introducidos de urna") ?>: <?php echo "$urna" ?></p>
                        <?php } ?>
                    </div>



                    <h3><?= _("Candidatos u opciones de esta votación") ?></h3>

                      <table id="tabla1" class="table table-striped table-bordered" data-page-length="25">
                        <tr>

                            <th width="10%"><?= _("Identificador") ?></th>
                            <th width="80%"><?= _("Nombre") ?></th>
                            <th width="10%"><?= _("Sexo") ?></th>
                        </tr>
                        <?php
// sacamos los datos del array

                        $sql2 = "SELECT ID, nombre_usuario,sexo  FROM $tbn7 WHERE id_votacion=" . $idvot . " ";
                        $result2 = mysqli_query($con, $sql2);
                        if ($row2 = mysqli_fetch_array($result2)) {
                            mysqli_field_seek($result2, 0);

                            do {
                                ?>
                                <tr>
                                    <td><?php echo $row2[0]; ?></td>
                                    <td><?php echo $row2[1]; ?></td>
                                    <td><?php echo $row2[2]; ?></td>


                                </tr>

                                <?php
                            } while ($row2 = mysqli_fetch_array($result2));
                        }
                        ?>

                    </table>



                    <h2><?= _("Lista de todos los votos incluidos manualmente en esta votación") ?></h2>
                    <p> <?= _("El segundo dato corresponde al identificador del candidato u opcion y la puntuacion asignada dependiendo del orden que se haya marcado") ?> </p>
                    <p> <?= _("Los distintos candidatos u opciones estan separados por") ?> #</p>
                    <?php
                    //$sql = "SELECT a.id_candidato, a.incluido ,b.nombre_usuario  FROM $tbn10 a, $tbn7 b WHERE (a.id_candidato=b.ID) and a.id_votacion = '$idvot' and a.especial =1";
                    $sql = "SELECT id_candidato, incluido   FROM $tbn10 WHERE  id_votacion = '$idvot' and especial =1 and id_provincia = $_SESSION[id_localidad] ";

                    $result = mysqli_query($con, $sql);

                    if ($row = mysqli_fetch_array($result)) {
                        $i = 1;
                        ?>

                          <table id="tabla1" class="table table-striped table-bordered" data-page-length="25">
                            <thead>
                                <tr>
                                    <th width="5%">&nbsp;</th>
                                    <th width="70%"><?= _("Voto") ?></th>

                                    <th width="25%"><?= _("Interventores") ?></th>



                                </tr>
                            </thead>

                            <tbody>

                                <?php
                                mysqli_field_seek($result, 0);

                                do {
                                    ?>


                                    <tr <?php
                                    if ($row[0] == 1) {
                                        echo "class=\"linea\" ";
                                    }
                                    ?> >
                                        <td><?php echo $i++ ?> </td>
                                        <td> <?php echo "$row[0]" ?> </td>
                                        <td>  <?php echo "$row[1]" ?> </td>
                                    </tr>



                                    <?php
                                } while ($row = mysqli_fetch_array($result));
                                ?>
                            </tbody>
                        </table>

                        <?php
                    } else {

                    }
                    ?>

                    <p></p>
                    <!--Final-->
                  </div>
                             </div>
                           </div>

                           <?php
                         }
                         ?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                               Por favor, no elimines este aviso de licencia,                                                            ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###                                                         ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que lista los votantes que han votado en una votación. Este archivo se usa en varias páginas
*/
///// este scrip se usa en varias paginas

if ($var_carga == true) {
    $contar = 0;
    $result_vot = mysqli_query($con, "SELECT id_provincia, activa,nombre_votacion,tipo_votante, id_grupo_trabajo, demarcacion,id_ccaa, id_municipio  FROM $tbn1 where id=$idvot");
    $row_vot = mysqli_fetch_row($result_vot);

    $id_provincia = $row_vot[0];
    $id_ccaa = $row_vot[6];
    $activa = $row_vot[1];
    $tipo_votante = $row_vot[3];
    $id_grupo_trabajo = $row_vot[4];
    $id_municipio = $row_vot[7];



    $sql = "SELECT a.ID, 	a.nombre_usuario , a.nif, b.fecha, b.forma_votacion,a.tipo_votante FROM $tbn9 a,  $tbn2 b where (a.id=b.id_votante) and a.id_provincia = '$id_provincia' and a.tipo_votante <='$tipo_votante' and b.id_votacion=$idvot ";
    if ($row_vot[5] == 1) {
        $id_provincia = $id_nprov;
        $sql = "SELECT a.ID, 	a.nombre_usuario , a.nif, b.fecha, b.forma_votacion,a.tipo_votante  FROM $tbn9 a,  $tbn2 b where (a.id=b.id_votante) and a.id_provincia = '$id_provincia' and a.tipo_votante <='$tipo_votante' and b.id_votacion=$idvot ";
    } else if ($row_vot[5] == 2) {
        $sql = "SELECT a.ID, 	a.nombre_usuario , a.nif, b.fecha, b.forma_votacion,a.tipo_votante  FROM $tbn9 a,  $tbn2 b where (a.id=b.id_votante) and a.id_ccaa = '$id_ccaa' and a.tipo_votante <='$tipo_votante' and b.id_votacion=$idvot ";
    } else if ($row_vot[5] == 3) {
        $sql = "SELECT a.ID, 	a.nombre_usuario , a.nif, b.fecha, b.forma_votacion,a.tipo_votante  FROM $tbn9 a,  $tbn2 b where (a.id=b.id_votante) and a.id_provincia = '$id_provincia' and a.tipo_votante <='$tipo_votante' and b.id_votacion=$idvot ";
    } else if ($row_vot[5] == 7) {
        $sql = "SELECT a.ID, 	a.nombre_usuario , a.nif, b.fecha, b.forma_votacion,a.tipo_votante  FROM $tbn9 a,  $tbn2 b where (a.id=b.id_votante) and a.id_municipio = '$id_municipio' and a.tipo_votante <='$tipo_votante' and b.id_votacion=$idvot ";
    } else {
        //falta los grupos   $sql = "SELECT a.ID, a.nombre_usuario , a.correo_usuario,a.tipo_votante FROM $tbn9 a,$tbn6 b  WHERE (a.ID= b.id_usuario) and id_grupo_trabajo='$id_grupo_trabajo' and a.tipo_votante <='$tipo_votante' ";
//$sql = "SELECT a.ID, 	a.nombre_usuario , a.correo_usuario, b.fecha, b.forma_votacion FROM $tbn9 a,  $tbn2 b, $tbn6 c  where (a.id=b.id_votante) and (a.id=c.id_votante)  and c.id_grupo_trabajo='$id_grupo_trabajo' and a.tipo_votante <='$tipo_votante' and b.id_votacion=$idvot ";
        $sql = "SELECT a.ID, a.nombre_usuario , a.nif
 FROM $tbn9 a,$tbn6 c
 WHERE  EXISTS (
	 SELECT  *
	 FROM $tbn2 b
	 WHERE a.id=b.id_votante  and b.id_votacion=$idvot
 ) and (a.ID=c.id_usuario) and a.tipo_votante <=$tipo_votante and c.id_grupo_trabajo='$id_grupo_trabajo' ";
        //////sin terminar
    }


    $result = mysqli_query($con, $sql);
    ?>

    <h2><?= _("Votación de") ?> <?php echo "$row_vot[2]" ?></h2>

    <h3 class="card-title"><?= _("Listado del censo que SI ha votado") ?> </h3>

<h4> <?php if ($es_municipal == false){ ?>
      <?php if ($row_vot[5] == 1 and $id_nprov != "") { ?>
         <?= _("para la provincia") ?> <strong> <?php echo $id_nprov; ?></strong> <?= _("y tipo de votación") ?>
        <?php } else if ($row_vot[5] == 2) { ?>
            <?= _("para la comunidad autonoma") ?> <strong><?php echo $row_vot[6]; ?></strong> <?= _("y tipo de votación") ?>
        <?php } else if ($row_vot[5] == 3) { ?>
            <?= _("para la provincia") ?> <strong><?php echo $row_vot[0]; ?></strong> <?= _("y tipo de votación") ?>
        <?php } else { ?>
            <?= _("Tipo de votación") ?>  <?php }
          }?>
    <?php
        if ($row_vot[3] == 1) {
            echo _("solo para socios");
        } else if ($row_vot[3] == 2) {
            echo _("solo pata socios y simpatizantes");
        } else if ($row_vot[3] == 3) {
            echo _("abierta");
        }
        ?> </h4>

    <?php
    if ($row = mysqli_fetch_array($result)) {
        ?>


        <table id="tabla1<?php echo $cen; ?>" class="table table-striped table-bordered dt-responsive nowrap" data-page-length="25">
            <thead>
                <tr>

                    <th width="30%"><?= _("NOMBRE") ?></th>
                    <th width="20%"><?= _("DNI") ?></th>
                    <th width="10%"><?= _("FECHA VOTACIÓN") ?></th>
                    <th width="10%"><?= _("TIPO VOTACIÓN") ?></th>
                    <th width="5%"><?= _("TIPO VOTANTE") ?></th>

                </tr>
            </thead>

            <tbody>
                <?php
                mysqli_field_seek($result, 0);
                do {
                    ?>
                    <tr>

                        <td><?php echo "$row[1]"; ?></td>
                        <td><?php echo "$row[2]"; ?></td>
                        <td><?php echo "$row[3]"; ?></td>
                        <td><?php
                            if ($row[4] == 1 or $row[4] == 2 or $row[4] == 3) {
                                echo _("On line");
                            } else if ($row[4] == 5){
                              echo _("voto en urna presencial");
                            } else if ($row[4] == 6){
                              echo _("voto en sala");
                            }
                            else {
                                echo "$row[4]";
                            }
                            ?></td>
                        <td><?php
                            if ($row[5] == 1) {
                                echo _("socio");
                            } else if ($row[5] == 2) {
                                echo _("simpatizante verificado");
                            } else if ($row[5] == 3) {
                                echo _("simpatizante");
                            } else if ($row[5] == 5) {
                                echo _("Aqui hay un error");
                            }
                            ?></td>
                    </tr>
                    <?php
                } while ($row = mysqli_fetch_array($result));
                ?>
            </tbody>
        </table>


        <?php
    } else {
        if ($id_provincia == "") {

            echo '<div class="alert alert-warning">' . _('Escoja la provincia para la que quiere ver el censo') . '</div>';
        } else {
            echo '<div class="alert alert-warning">'  . _('¡No se ha encontrado votantes para esta votación!') .  '</div> ';
        }
    }
} else {
    echo _("error acceso");
}
?>

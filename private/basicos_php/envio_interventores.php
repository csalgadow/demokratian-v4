<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que realiza el envío de los correos a los interventores con lo que ha votado el usuario si está activada la opción de seguridad de envío de votaciones
*/

require_once("../private/config/config.inc.php");
include('../private/config/config-correo.inc.php');
include('../private/inc_web/conexion.php');

//require_once('../private/modulos/PHPMailer/PHPMailerAutoload.php');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once '../private/modulos/PHPMailer/src/PHPMailer.php';
require_once '../private/modulos/PHPMailer/src/SMTP.php';
require_once '../private/modulos/PHPMailer/src/Exception.php';
//include("../private/modulos/PHPMailer/class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded
$msg_result = "";
$mensaje = "";

$mensaje = _("Este mensaje fue enviado de forma automatica por el sistema de votaciones") . " <br/>";

$mensaje .= _("El") . ": " . date('d/m/Y', time());

$mensaje .= "<br /> " . _("el usuario ha votado lo siguiente") . ":
<br />";
if (isset($variables['vote_id'])) {
    $mensaje .= $variables['vote_id'];
}
$mensaje .= "  -  " . $datos_votado . "; <br/>";


$asunto_mens_ref = _("Nuevo voto en") . " " . $nombre_votacion;

//<$mail->IsSendmail(); // telling the class to use SendMail transport

//$mensaje = str_replace("\n", "<br>", $mensaje);
//$mensaje = str_replace("\t", "    ", $mensaje);

if ($correo_smtp == true) {  //comienzo envio smtp
    $mail = new PHPMailer();
    $mail->CharSet = 'UTF-8';
    $mail->ContentType = 'text/plain';
    //$mail->IsHTML(false);
    if ($mail_IsHTML == true) {
        $mail->IsHTML(true);
    } else {
        $mail->IsHTML(false);
    }

    if ($mail_sendmail == true) {
        $mail->IsSendMail();
    } else {
        $mail->IsSMTP();
    }

    //$mail->SMTPAuth = true;
    if ($mail_SMTPAuth == true) {
        $mail->SMTPAuth = true;
    } else {
        $mail->SMTPAuth = false;
    }

    if ($mail_SMTPSecure == false) {
        $mail->SMTPSecure = false;
        $mail->SMTPAutoTLS = false;
    } else if ($mail_SMTPSecure == "SSL") {
        $mail->SMTPSecure = 'ssl';
    } else {
        $mail->SMTPSecure = 'tls';
    }

    if ($mail_SMTPOptions == true) {  //algunos servidores con certificados incorrectos no envian los correos por SMTP por lo que quitamos la validadcion de los certificados, NO SE RECOMIENDA EN ABSOLUTO usar esta opción
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
    }

    $mail->Port = $puerto_mail; // Puerto a utilizar
    $mail->Host = $host_smtp;
    $mail->SetFrom($email_control, $nombre_eq);
    $mail->Subject = $asunto_mens_ref;

    $mail->MsgHTML($mensaje);

    ///miramos las direcciones de correo a la que hay que enviar el correo


    $sql = "SELECT nombre, apellidos,correo FROM $tbn11 WHERE id_votacion = '$idvot' and tipo<=1 ";
    $result = mysqli_query($con, $sql);

    if ($row = mysqli_fetch_array($result)) {
        mysqli_field_seek($result, 0);
        do {

            $correo_interventor = "$row[2]";
            $nombre_interventor = "$row[0] $row[1]";
            $mail->AddAddress($correo_interventor, $nombre_interventor);
        } while ($row = mysqli_fetch_array($result));
    }
    //fin del bucle para enviar el correo


    $mail->Username = $user_mail;
    $mail->Password = $pass_mail;

    if (!$mail->Send()) {
        echo "<div class=\"alert alert-danger\">" . _("Error en el envio") . " " . $mail->ErrorInfo . "</div>";

        $process_result = "ERROR";
        $msg_result .= _("Error en el envio") . " " . $mail->ErrorInfo;
    } else {
// echo "Enviado correctamente!";
    }
}// fin envio por stmp

if ($correo_smtp == false) { ///correo mediante mail de php
    //para el env�o en formato HTML
    $headers = "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

//direcci�n del remitente
    $headers .= "From: $nombre_eq <$email_control>\r\n";


//ruta del mensaje desde origen a destino
    $headers .= "Return-path: $email_control\r\n";


    ///miramos las direcciones de correo a la que hay que enviar el correo


    $sql = "SELECT nombre, apellidos,correo FROM $tbn11 WHERE id_votacion = '$idvot' and tipo<=1 ";
    $result = mysqli_query($con, $sql);

    if ($row = mysqli_fetch_array($result)) {
        mysqli_field_seek($result, 0);
        do {

            $correo_inter .= $row[2] . ",";
        } while ($row = mysqli_fetch_array($result));
    }
//fin del bucle para enviar el correo

    mail($correo_inter, $asunto_mens_ref, $mensaje, $headers);
}
?>

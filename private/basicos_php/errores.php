<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que contiene la función del envío de la querry a la base de datos, si da error, escribe en el log el error, y si está activado, envía un correo a la dirección del administrador
* De envío de la querry, hay dos funciones, una que devuelve los datos, true o false, y otra que es para inserciones en la bbdd que devuelve el id
* @todo ver si separamos en dos archivos, una para las funciones sql y otro para los errores, o solo cambiamos el nombre del archivo por uno más identificativo.
*/
include('../private/config/config-correo.inc.php');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once ('../private/modulos/PHPMailer/src/PHPMailer.php');
require_once ('../private/modulos/PHPMailer/src/SMTP.php');
require_once ('../private/modulos/PHPMailer/src/Exception.php');

//require_once("../private/modulos/PHPMailer/class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded

####################################  Funciones de control de errores de inserccion a la BBDD  ##########################################
//$b_debugmode = 1; // 0 || 1

function db_query($conexion, $query, $mensaje_esp) {
    global $b_debugmode;
    global $b_debugmode_install;
    // Perform Query
    $result = mysqli_query($conexion, $query);

    // Check result
    // This shows the actual query sent to MySQL, and the error. Useful for debugging.
    if (!$result) {

        $message = '\r\n Invalid query: \r\n' . mysqli_error($conexion) . '\r\n';
        $message .= '<b>Whole query:</b>\r\n' . $query . ' ';
        $message .= "\r\n " . $mensaje_esp;

        if ($b_debugmode) {

            die($message);
        }else  if ($b_debugmode_install){
        raise_error_install('db_query_error: ' . $message);
        die('ERROR# <div class="alert alert-danger"> Se ha producido el siguiente error: Error al incluir datos en la Base de datos: ' .$message. '</div>');
      }else{
        raise_error('db_query_error: ' . $message);
      }
    }

    return $result;
}

####################################  Funcion de control de errores de insercion a la BBDD que devuelve el id del ultimo registro ##########################################
//$b_debugmode = 1; // 0 || 1

function db_query_id($conexion, $query, $mensaje_esp) {
    global $b_debugmode;

    // Perform Query
    $result = mysqli_query($conexion, $query);

    // Check result
    // This shows the actual query sent to MySQL, and the error. Useful for debugging.
    if (!$result) {

        $message = '\r\n Invalid query:\r\n' . mysqli_error($conexion) . '\r\n';
        $message .= 'Whole query:<br>' . $query . ' ';
        $message .= "\r\n " . $mensaje_esp;

        if ($b_debugmode) {

            die($message);
        }

        raise_error('db_query_error: ' . $message);
    }

    return mysqli_insert_id($conexion);
}

############################### FUNCION DE ENVIO DE MESAJE SI HAY ERROR Y AÑADIR A LOGS #######################3

function raise_error($message) {
    global $email_error_tecnico, $email_sistema, $correo_smtp, $user_mail, $pass_mail, $mail_IsHTML, $mail_sendmail, $mail_SMTPAuth, $mail_SMTPSecure, $puerto_mail, $mail_SMTPOptions, $host_smtp;

    $serror = "Env:       " . $_SERVER['SERVER_NAME'] . "\r\n" .
            "timestamp: " . date('m/d/Y H:i:s') . "\r\n" .
            "script:    " . $_SERVER['PHP_SELF'] . "\r\n" .
            "error:     " . $message . "\r\n\r\n";

    // ABRIR EL FICHERO DE LOGS Y ESCRIBIR EL ERROR
    $fhandle = fopen('../private/logs/errors' . date('Ymd') . '.txt', 'a');
    if ($fhandle) {
        fwrite($fhandle, $serror);
        fclose(( $fhandle));
    }

    // ENVIAR UN CORREO ELECTRONICO
    //if (!$b_debugmode) {
    if (empty($b_debugmode)) {
        //mail($email_error_tecnico, 'error: '.$message, $serror, 'From: ' . $email_sistema );
        $asunto_mens_error = "ERROR SISTEMA VOTACIONES | problema base de datos";
        $nombre_ref = "Sistema";
        $nombre_eq = "tecnico";

        if ($correo_smtp == true) {  //comienzo envio smtp
            $mail = new PHPMailer();
            //$mail->SMTPDebug = 2;////habilitamos los errores de envios  para saber que pasa
            $mail->CharSet = 'UTF-8';
            $mail->ContentType = 'text/plain';
            if ($mail_IsHTML == true) {
                $mail->IsHTML(true);
            } else {
                $mail->IsHTML(false);
            }

            if ($mail_sendmail == true) {
                $mail->IsSendMail();
            } else {
                $mail->IsSMTP();
            }

            //$mail->SMTPAuth = true;
            if ($mail_SMTPAuth == true) {
                $mail->SMTPAuth = true;
            } else {
                $mail->SMTPAuth = false;
            }

            if ($mail_SMTPSecure == false) {
                $mail->SMTPSecure = false;
                $mail->SMTPAutoTLS = false;
            } else if ($mail_SMTPSecure == "SSL") {
                $mail->SMTPSecure = 'ssl';
            } else {
                $mail->SMTPSecure = 'tls';
            }

            if ($mail_SMTPOptions == true) {  //algunos servidores con certificados incorrectos no envian los correos por SMTP por lo que quitamos la validadcion de los certificados, NO SE RECOMIENDA EN ABSOLUTO usar esta opción
                $mail->SMTPOptions = array(
                    'ssl' => array(
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                        'allow_self_signed' => true
                    )
                );
            }

            $mail->Port = $puerto_mail; // Puerto a utilizar
            $mail->Host = $host_smtp;
            $mail->SetFrom($email_sistema, $nombre_ref);
            $mail->Subject = $asunto_mens_error;

            $mail->MsgHTML($serror);

            $mail->AddAddress($email_error_tecnico, $nombre_eq);


            $mail->Username = $user_mail;
            $mail->Password = $pass_mail;

            if (!$mail->Send()) {
                echo " Error en el envio " . $mail->ErrorInfo;
            } else {
                // echo "Enviado correctamente!";
            }
        }// fin envio por stmp

        if ($correo_smtp == false) { ///correo mediante mail de php
            //para el envío en formato HTML
            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

            //dirección del remitente
            $headers .= "From: $nombre_ref <$email_sistema>\r\n";
            //ruta del mensaje desde origen a destino
            $headers .= "Return-path: $email_sistema\r\n";

            $asunto = "$asunto_mens_error";
            mail($email_error_tecnico, $asunto_mens_error, $serror, $headers);
        }
    }
}

###########################

function raise_error_install($message) {


    $serror = "Env:       " . $_SERVER['SERVER_NAME'] . "\r\n" .
            "timestamp: " . date('m/d/Y H:i:s') . "\r\n" .
            "script:    " . $_SERVER['PHP_SELF'] . "\r\n" .
            "error:     " . $message . "\r\n\r\n";

    // ABRIR EL FICHERO DE LOGS Y ESCRIBIR EL ERROR
    $fhandle = fopen('../private/logs/errors' . date('Ymd') . '.txt', 'a');
    if ($fhandle) {
        fwrite($fhandle, $serror);
        fclose(( $fhandle));
    }


}



##############################  FIN  de  Funciones de control de errores de inserccion a la BBDD  ##########################################
?>

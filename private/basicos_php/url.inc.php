<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* archivo con tres funciones relacionadas con las urls, una para generar url amigables y otra para encriptar y desencriptar las urls
*/
function urls_amigables($url) {

    // Tranformamos todo a minusculas
    $url = strtolower($url);

    //Rememplazamos caracteres especiales latinos
    $find = array('á', 'é', 'í', 'ó', 'ú', 'ñ');
    $repl = array('a', 'e', 'i', 'o', 'u', 'n');
    $url = str_replace($find, $repl, $url);

    // Añadimos los guiones
    $find = array(' ', '&', '\r\n', '\n', '+');
    $url = str_replace($find, '-', $url);

    // Eliminamos y Reemplazamos demás caracteres especiales
    $find = array('/[^a-z0-9\-<>]/', '/[\-]+/', '/<[^>]*>/');
    $repl = array('', '-', '');
    $url = preg_replace($find, $repl, $url);
    return $url;
}


 /**
 * function to encrypt
 * @param string $data
 * @param string $key
 */
function encrypt_url($data,$key)  // genera una cadena , decidir si preferimos otro tipo de cadena mas corta
{
    $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
    $encrypted=openssl_encrypt($data, "aes-256-cbc", $key, 0, $iv);
    // return the encrypted string with $iv joined
     $string=str_replace(array('+', '/', '='), array('-', '_', ''),base64_encode($encrypted."::".$iv));
    //$rand= substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,4); //generamos 4 digitos aleatorios

    return $string;
}

/**
 * function to decrypt
 * @param string $data
 * @param string $key
 */
function decrypt_url($data,$key){
    $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
    $data = str_replace(array('-', '_'), array('+', '/'), $data);
    list($encrypted_data, $iv) = explode('::', base64_decode($data), 2);
    return openssl_decrypt($encrypted_data, 'aes-256-cbc', $key, 0, $iv);
}


/////////////////////////  codificación de la parte del blog Pendiente de ver si merece la pena codificarlo o es mejor pensar en una ofuscación sencilla para los id

//Return encrypted string
function stringEncrypt ($data, $key = '7R7zX2Urc7qvjhkr') {

  $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-128-cbc'));
  $encrypted=openssl_encrypt($data, "aes-128-cbc", $key, 0, $iv);
  // return the encrypted string with $iv joined
   $string=str_replace(array('+', '/', '='), array('-', '_', ''),base64_encode($encrypted."::".$iv));
  //$rand= substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,4); //generamos 4 digitos aleatorios

  return $string;

}


//Return decrypted string
function stringDecrypt ($data, $key = '7R7zX2Urc7qvjhkr') {
  $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-128-cbc'));
  $data = str_replace(array('-', '_'), array('+', '/'), $data);
  list($encrypted_data, $iv) = explode('::', base64_decode($data), 2);
  return openssl_decrypt($encrypted_data, 'aes-128-cbc', $key, 0, $iv);
}


?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
*  archivo que carga el idioma seleccionado, este archivo debe de evolucionar para detectar el idioma del usuario
*/

if (false === function_exists('gettext')) {
    echo "No tiene la libreria gettext instalada.";
    exit();
} else {
//$defaul_lang="es_ES.UTF-8";  //idioma por defecto
    /////////////////  ESTE ES EL SCRIP QUE FUNCIONARA CUANDO LOS USUARIOS PUEDAN COGER EL IDIOMA ////////////////////
    $dir_lang = "locale"; //directorio conde tenemos las traducciones, ojo, cambiar para paginas interiores

    if (isset($_GET["lang"])) {
        $locale = $_GET["lang"];
        $_SESSION["locale"] = $locale;
    } else if (isset($_SESSION["locale"])) {
        $locale = $_SESSION["locale"];
    }
    /* else {
      $lang = locale_accept_from_http($_SERVER['HTTP_ACCEPT_LANGUAGE']);
      echo $lang;
      $nombre_fichero = $dir_lang . "/" . $lang;

      if (file_exists($nombre_fichero)) {  //comprobamos si exite el idioma
      $locale = $lang;
      $_SESSION["locale"] = $lang;
        } */
        else {  // si no exite ponemos  el idioma por defecto
        $locale = $defaul_lang;
        $_SESSION["locale"] = $defaul_lang;
    }
    //}

    $locale = $locale . ".UTF-8";
    //$locale = $defaul_lang . ".UTF-8";  //// eliminar esta linea cuando los usuarios puedan coger el idioma por defecto
// hay que mirar si con el forzado a UTF funcionan todos los idiomas bien

    $lang = substr($locale, 0, 2);
    putenv('LC_ALL=' . $lang);
    setlocale(LC_ALL, $locale);
    bindtextdomain("messages", "./locale"); // locale para las paginas exteriores
    bindtextdomain("messages", "./../locale"); // locale para el interior
    bind_textdomain_codeset('default', 'UTF-8');
    textdomain("messages");
}

/////  separamos el string para coger solo las dos primeras letras del idioma, necesario para los idiomas de ckeditor

$first_lang = explode("_", $defaul_lang);
$short_lang= $first_lang[0]; // porción1

?>

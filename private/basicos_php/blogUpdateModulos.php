<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################

/**
* Archivo que resuelve la petición de ajax que hace el archivo admin/blog_gestion_modulos cuando se ordenan los módulos que se presentan en las páginas públicas
*/

if(!isset($carga)){
  $carga =false;
  exit;
}
if($carga!="OK"){
  exit;
}else{
  if($variables['idcab']!=""){
    $idcab = fn_filtro_numerico($con, $variables['idcab']);
    require_once("../private/config/config.inc.php");
    include_once('../private/inc_web/conexion.php');

    $datos="";
      foreach($_POST['blog'] as $key=>$value) {
        $datos.=$value.',';
        }
    $datos = trim($datos, ','); ///quitamos la ultima coma de la cadena para meterlo en la bbdd

    $sSQL = "UPDATE $tbn35 SET bloque=\"$datos\" WHERE id='$idcab'";

    //  mysqli_query($con, $sSQL) or die("Imposible modificar pagina");

    $mens = _("ERROR en la modificacion del blog en el archivo") . " basicos_pho/blogUpdateModulos.php";
    $modifica_blog = db_query($con, $sSQL, $mens);

    if ($modifica_blog == false) {
      //$process_result = 'ERROR';
        $inmsg_error = "<div class='alert alert-danger'>
        <p>" . _("Hay algun tipo de error, no se han actualizado los datos").  "</p>
        </div>";
    } else {

    //$process_result = 'OK';

    $msg_result = "<div class='alert alert-success'>
    <p>" . _("Actualizados los modulos en la base de datos de forma correcta.") .  "</p>
    </div>";
    }
    echo $msg_result;
    //echo $process_result . "#" . $msg_result;
  }
}
?>

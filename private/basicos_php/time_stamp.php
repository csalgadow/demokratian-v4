<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* archivo con la función que sirve para calcular el tiempo transcurrido (usado en los comentarios del debate)
*/

if (@preg_match("time_stamp.php", $_SERVER['SCRIPT_NAME'])) {
    Header("Location: ../index.php");
    die();
}

function time_stamp($session_time) { //Tiempo transcurrido
    $time_difference = time() - $session_time;
    $seconds = $time_difference;
    $minutes = round($time_difference / 60);
    $hours = round($time_difference / 3600);
    $days = round($time_difference / 86400);
    $weeks = round($time_difference / 604800);
    $months = round($time_difference / 2419200);
    $years = round($time_difference / 29030400);

    if ($seconds <= 60) {
        echo _("Hace") . $seconds . _("segundos");
    } else if ($minutes <= 60) {
        if ($minutes == 1) {
            echo _("Hace un minuto");
        } else {
            echo _("Hace $minutes minutos");
        }
    } else if ($hours <= 24) {
        if ($hours == 1) {
            echo _("Hace una hora");
        } else {
            echo _("Hace") . "  " . $hours . ( " horas");
        }
    } else if ($days <= 7) {
        if ($days == 1) {
            echo _("Hace un día");
        } else {
            echo _("Hace") . "  " . $days . "  " . _("días");
        }
    } else if ($weeks <= 4) {
        if ($weeks == 1) {
            echo _("Hace una semana");
        } else {
            echo _("Hace") . "  " . $weeks . "  " . _("semanas");
        }
    } else if ($months <= 12) {
        if ($months == 1) {
            echo _("Hace un mes");
        } else {
            echo _("Hace") . "  " . $months . "  " . _("meses");
        }
    } else {
        if ($years == 1) {
            echo _("Hace un año");
        } else {
            echo _("Hace") . "  " . $years . "  " . _("años");
        }
    }
}

?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* archivo que contiene la función que lee las url y saca las variables necesarias para la zona de votación y administración
*/

/*
#####################   construccion de  los datos ###################################

c=<?php echo encrypt_url(votacion/mis_grupos/id=000005/idvut=000003,$clave_encriptacion) ?>
parametros de la url que generamos separados por /

primer parametro  carpeta
segundo parametro  pagina
resto parametros  cadena tipo url   --->  variable1=dato1&variable2=dato2....

*/
if(isset($_GET['c'])){

  $datos = fn_filtro($con, $_GET['c']);
  $cadena=decrypt_url($datos,$clave_encriptacion);  //desncriptamos la cadena
  $arr = explode('/', $cadena); // separamos la cadena en 3 datos
  $carpeta=$arr[0];
  $pagina=$arr[1];

  $variables=array();

  if (isset($arr[2])){  // si hemos pasado variables por la url
  parse_str($arr[2],$variables);  // como tenemos un string tipo url , lo parseamos para sacar el dato de las variables


  /*uso solo durevte desarrollo */
//  echo '<pre>';
//  print_r(var_dump($arr[2]));
 // echo '</pre>';
  /*  fin  uso  solo durante desarrollo */
}

  $laPagina ="../private/". $carpeta."/".$pagina.".php";

  /*uso solo durevte desarrllo */
// echo '<h1>nos encontramos en:' .$carpeta .' | ' .$pagina.'</h1>';
/*  fin  uso  solo durante desarrollo */
}

?>

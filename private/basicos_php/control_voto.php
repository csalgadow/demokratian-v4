<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que, dependiendo el tipo de seguridad que lleve la aplicación, presenta los correos electrónicos de los interventores y envía los correos,
* además, si es una votación que lleve clave, añade la opción de añadir la clave.
*/
?>
<div id="control_seg">

    <?php if ($seguridad == 4 or $seguridad == 3) { ?>
        <div class="card">
          <div class="card-body">
            <?= _("Esta votación tiene interventores que recibiran su papeleta de voto de forma anonima y son") ?>:

            <?php
            $sql = "SELECT nombre, apellidos FROM $tbn11 WHERE id_votacion = '$idvot'  and tipo<=1  ORDER BY 'nombre'";
            $result = mysqli_query($con, $sql);
            if ($row = mysqli_fetch_array($result)) {
                ?>

                <ul>
                    <?php
                    mysqli_field_seek($result, 0);
                    do {
                        ?>
                        <li> <?php echo "$row[0]" ?> <?php echo "$row[1]" ?></li>

                        <?php
                    } while ($row = mysqli_fetch_array($result));
                    ?>
                </ul>
            <?php
        }?>
        </div>
      </div>
<?php
    }
    ?>

    <?php
    // sistema para incluir internventores o clave voto seguro
    if ($seguridad == 2 or $seguridad == 4) {
        ?>
        <p class="text-info"><?= _("Esta votación permite comprobacion de voto posterior, para ello debe introducir una clave que debe recordar y que le servira para una comprobacion posterior") ?></p>
        <p class="text-info"> <?= _("Se recomienda no usar la misma clave que usa para identificarse al acceder a la plataforma de votaciones para evitar algun tipo de vinculación de su voto con su usuario") ?></p>
        <div class="form-group row">
            <label for="clave_seg"  class="col-sm-3 control-label"><?= _("Clave de seguridad") ?></label>
            <div class="col-sm-3">
             <!--<input type="text" name="clave_seg" id="clave_seg" />-->
                <input type="password" name="clave_seg" id="clave_seg" value="" placeholder="<?= _("Introduzca su clave") ?>" autofocus required class="form-control"/>
            </div></div>
        <p>&nbsp;</p>
    <?php } else { ?>
        <input name="clave_seg" type="hidden" id="clave_seg" value="non_use" />
    <?php } ?>
</div>

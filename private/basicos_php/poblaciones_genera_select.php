<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que lista los pueblos y los codigos de los pueblos
*/

if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{
//$nivel_acceso = 11;
//include('../private/inc_web/nivel_acceso.php');

   if(isset($_GET['id_provincia'])){

    ?>
<br/>

<table class="table table-hover">
  <caption><?= _("municipio") ?></caption>
  <thead>
    <tr>
        <th scope="row"><p><?= _("Municipio") ?></p>
            </th>
        <td scope="row"><p><?= _("codigo") ?></p></td>
    </tr>

</thead>
<tbody>
    <?php
    require_once("../private/config/config.inc.php");
    include_once('../private/inc_web/conexion.php');

    $id_provincia = fn_filtro_numerico($con, $_GET['id_provincia']);

    $consulta = "SELECT * from $tbn18 WHERE id_provincia = " . $id_provincia;
    $query = mysqli_query($con, $consulta) or die("error: " . mysqli_error($con));
    while ($fila = mysqli_fetch_array($query)) {

        echo '<tr>
    <th scope=\"row\"> ' . $fila['nombre'] . ' </th>
    <td> ' . $fila['id_municipio'] . ' </td>
  </tr>';
    };
    ?>
</tbody>
</table>



<?php
}
} ?>

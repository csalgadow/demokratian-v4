<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################

/**
* Archivo que permite subir las imagenes de ckeditor
*/

if(!isset($carga)){
  $cargaA =false;
  exit;
}
if($carga!="OK"){
  exit;
}else{
$nivel_acceso = 7;
include('../private/inc_web/nivel_acceso.php');

// para la subida de archivos de CKEDITOR

// Define file upload path
$upload_dir = array(
    'img'=> $baseUrl,
);


// Allowed image properties
$imgset = array(
    'maxsize' => 2000,
    'maxwidth' => 11024,
    'maxheight' => 1800,
    'minwidth' => 10,
    'minheight' => 10,
    'type' => array('bmp', 'gif', 'jpg', 'jpeg', 'png'),
);

// If 0, will OVERWRITE the existing file
define('RENAME_F', 1);

/**
 * Set filename
 * If the file exists, and RENAME_F is 1, set "img_name_1"
 *
 * $p = dir-path, $fn=filename to check, $ex=extension $i=index to rename
 */
function setFName($p, $fn, $ex, $i){
    if(RENAME_F ==1 && file_exists($p .$fn .$ex)){
        return setFName($p, F_NAME .'_'. ($i +1), $ex, ($i +1));
    }else{
        return $fn .$ex;
    }
}

$re = '';
if(isset($_FILES['upload']) && strlen($_FILES['upload']['name']) > 1) {

    define('F_NAME', preg_replace('/\.(.+?)$/i', '', basename($_FILES['upload']['name'])));

    // Get filename without extension
    $sepext = explode('.', strtolower($_FILES['upload']['name']));
    $type = end($sepext);    /** gets extension **/

    // Upload directory
    $upload_dir = in_array($type, $imgset['type']) ? $upload_dir['img'] : $upload_dir['audio'];
    $upload_dir = trim($upload_dir, '/') .'/';

    // Validate file type
    if(in_array($type, $imgset['type'])){
        // Image width and height
        list($width, $height) = getimagesize($_FILES['upload']['tmp_name']);

        if(isset($width) && isset($height)) {
            if($width > $imgset['maxwidth'] || $height > $imgset['maxheight']){
                $re .= '\\n Ancho y Alto = '. $width .' x '. $height .' \\n El alto y ancho maximo es de:: '. $imgset['maxwidth']. ' x '. $imgset['maxheight'];
            }

            if($width < $imgset['minwidth'] || $height < $imgset['minheight']){
                $re .= '\\n Ancho y Alto = '. $width .' x '. $height .'\\n El alto y ancho minimo es de: '. $imgset['minwidth']. ' x '. $imgset['minheight'];
            }

            if($_FILES['upload']['size'] > $imgset['maxsize']*1000){
                $re .= '\\n No se puede añadir ya que el archivo excede el tamaño permitido.\\n El tamaño maximo del archivo es de: '. $imgset['maxsize']. ' KB.';
            }
        }
    }else{
        $re .= 'El archivo: '. $_FILES['upload']['name']. ' no es un tipo de archivo valido para añadir al servidor.';
    }

    // File upload path
    $f_name = setFName($_SERVER['DOCUMENT_ROOT'] .'/'. $upload_dir, F_NAME, ".$type", 0);
    $uploadpath = $upload_dir . $f_name;

    // If no errors, upload the image, else, output the errors
    if($re == ''){
        if(move_uploaded_file($_FILES['upload']['tmp_name'], $uploadpath)) {
            $CKEditorFuncNum = $_GET['CKEditorFuncNum'];
            $url =  $upload_dir . $f_name;
            $msg = 'La imagen'.F_NAME .'.'. $type .' ha sido añadida correctamente al servidor: \\n- Tamaño: '. number_format($_FILES['upload']['size']/1024, 2, '.', '') .' KB';
            $re = in_array($type, $imgset['type']) ? "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>":'<script>var cke_ob = window.parent.CKEDITOR; for(var ckid in cke_ob.instances) { if(cke_ob.instances[ckid].focusManager.hasFocus) break;} cke_ob.instances[ckid].insertHtml(\' \', \'unfiltered_html\'); alert("'. $msg .'"); var dialog = cke_ob.dialog.getCurrent();dialog.hide();</script>';
        }else{
            $re = '<script>alert("No se ha podido subir el fichero")</script>';
        }
    }else{
        $re = '<script>alert("'. $re .'")</script>';
    }
}

// Render HTML output
@header('Content-type: text/html; charset=utf-8');
echo $re;




// ------------------------
// Input parameters: optional means that you can ignore it, and required means that you
// must use it to provide the data back to CKEditor.
// ------------------------

// Optional: instance name (might be used to adjust the server folders for example)
//$CKEditor = $_GET['CKEditor'] ;

// Required: Function number as indicated by CKEditor.
//$funcNum = $_GET['CKEditorFuncNum'] ;

// Optional: To provide localized messages
//$langCode = $_GET['langCode'] ;


// ------------------------
// Write output
// ------------------------
// We are in an iframe, so we must talk to the object in window.parent
//echo "<script type='text/javascript'> window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message')</script>";
}
?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Contiene una función, sin terminar, para los envíos de correos, hacer esta llamada y no tener que incluir toda la configuración en cada uno de los archivos. Sin usar ni terminar aún
*/


function send_smtp_mail($to_name, $to, $subject, $body, $from_name, $from, $reply_to_name, $reply_to) {
    $mail = new PHPMailer(true);  // the true param means it will throw exceptions on errors, which we need to catch
    $mail->IsSMTP();  // telling the class to use SMTP
    try {
        $mail->Host = "smtp.googlemail.com";  // SMTP server
        $mail->Port = 465;                    // SMTP port
        $mail->Username = "username@yourdomain";  // GMAIL username
        $mail->Password = "password";             // GMAIL password
        $mail->SMTPDebug = 0;        // 1 to enables SMTP debug (for testing), 0 to disable debug (for production)
        $mail->SMTPAuth = true;    // enable SMTP authentication
        $mail->SMTPSecure = "ssl";  // ssl required

        $mail->SetFrom($from, $from_name);
        $mail->AddReplyTo($reply_to, $reply_to_name);
        $mail->AddAddress($to, $to_name);
        $mail->Subject = $subject;
        $mail->MsgHTML($body);

        $mail->Send();
    } catch (phpmailerException $e) {
        echo $e->errorMessage();  //Pretty error messages from PHPMailer
    } catch (Exception $e) {
        echo $e->getMessage();  //Boring error messages from anything else!
    }
}

?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################

/**
* Archivo con las funciones para evitar ataques  por XSS y un ajuste de la zona horaria
* @todo ver si separamos en otro archivo el ajuste de la zona horaria.
*/

######################### Ajuste de la zona horaria si el servidor no esta en la hora de la aplicación #########################

if ($timezone == false) {

} else {
    date_default_timezone_set($timezone);
}


######################### Funciones de seguridad para evitar ataques por XSS #########################

function fn_filtro_editor($conexion, $cadena) {
    $vowels = array("javascript", "alert(", "alert (", "cookie", "onload", "document.location", "onclick", "onmouseover", "onmouseout", "onunload", "onsubmit", "onselect", "onresize", "onreset", "onmouseup", "onmousemove", "onmousedown", "onkeyup", "onkeypress", "onkeydown", "onfocus", "ondblclick", "onresize", "onclick", "onblur", "onchange", "getElementById", "this");
    $cadena1 = str_ireplace($vowels, "****", $cadena);
    $cadena2 = strip_tags($cadena1, '<h1><h2><h3><h4><h5><h6><p><hr><pre><blockquote><ol><ul><li><dl><dt><dd><div><center><a><basefont><br><em><font><span><strong><table><caption><colgroup><col><tbody><thead><tfoot><tr><td><th><img>');
    return mysqli_real_escape_string($conexion, $cadena2);
}

function fn_filtro($conexion, $cadena) {

    $cadena = strip_tags($cadena);
    return mysqli_real_escape_string($conexion, $cadena);
}

function fn_filtro_numerico($conexion, $cadena) {


    if (is_numeric($cadena)) {
        $cadena = $cadena;
    } else {
        $cadena = "";
        echo "<script> window.location='".$url_vot."/index.php?error_login=9'; </script>";
      //  Header("Location: ../index.php?error_login=9");
        exit;
    }
    $cadena = strip_tags($cadena);
    return mysqli_real_escape_string($conexion, $cadena);
}

function fn_filtro_nodb($cadena) {

    return strip_tags($cadena);
}

function fn_filtro_nodb_numerico($cadena) {

    if (is_numeric($cadena)) {
        $cadena = $cadena;
    } else {
        $cadena = "";
          echo "<script> window.location='".$url_vot."/index.php?error_login=9'; </script>";
        //Header("Location: ../index.php?error_login=4");
        exit;
    }
    return strip_tags($cadena);
}

###################### FIN de Funciones de seguridad para evitar ataques por XSS #########################

?>

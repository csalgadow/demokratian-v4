<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivos de conexión con la base de datos, incluye la relación de las tablas con la variable
*/

###################################################################################################################################
############################################## Nombres de las tablas en la BBDD####################################################

$tbn1 = $extension . "votacion";             // nombre votacion y caracteristicas
$tbn2 = $extension . "usuario_x_votacion";  /////tabla que dice quien ha votado en una votacion especifica
$tbn3 = $extension . "ccaa";
$tbn4 = $extension . "grupo_trabajo";
$tbn5 = $extension . "usuario_admin_x_provincia";
$tbn6 = $extension . "usuario_x_g_trabajo";
$tbn7 = $extension . "candidatos";
$tbn8 = $extension . "provincia";
$tbn9 = $extension . "votantes";
$tbn10 = $extension . "votos";  //
$tbn11 = $extension . "interventores";
$tbn12 = $extension . "debate_comentario";
$tbn13 = $extension . "debate_preguntas";
$tbn14 = $extension . "debate_votos";
$tbn15 = $extension . "elvoto";  //para el VUT  -- sutituye a $tbn10
$tbn16 = $extension . "acciones";
$tbn17 = $extension . "votantes_x_admin";
$tbn18 = $extension . "municipios";
$tbn19 = $extension . "votos_seg";
$tbn20 = $extension . "voto_temporal";
$tbn21 = $extension . "codificadores";
$tbn22 = $extension . "votacion_web";
$tbn23 = $extension . "nivel_usuario";
$tbn24 = $extension . "lenguajes";
$tbn25 = $extension . "lenguaje_cod_pais";
$tbn26 = $extension . "debate_preguntas_lang";
$tbn27 = $extension . "grupo_trabajo_lang";
$tbn28 = $extension . "candidatos_lang";
$tbn29 = $extension . "votacion_lang";
$tbn30 = $extension . "blog_cabeceras";
$tbn31 = $extension . "blog_categorias";
$tbn32 = $extension . "blog_categorias_lang";
$tbn33 = $extension . "blog_entradas";
$tbn34 = $extension . "blog_entradas_lang";
$tbn35 = $extension . "blog_estructura_inicio"; // estrucrura de la pagina de inicio. Reservadas 10 primeras lineas
$tbn36 = $extension . "blog_menu";
$tbn37 = $extension . "blog_menu_lang";
$tbn38 = $extension . "blog_paginas";
$tbn39 = $extension . "blog_paginas_lang";
$tbn40 = $extension . "blog_cabeceras_lang";
$tbn41 = $extension . "relacion_votaciones";


$con = @mysqli_connect("$host", "$hostu", "$hostp") or die("no se puede conectar");
mysqli_set_charset($con, "utf8");
$db = @mysqli_select_db($con, "$dbn") or die("no se puede acceder a la tabla");
?>

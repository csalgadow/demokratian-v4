<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo de acceso dentro del sistema a la zona de administración que requiere un nuevo logueo como la zona para generar nuevos administradores, backup de la bbdd, etc
*/
require_once("../private/config/config.inc.php");
require_once("conexion.php");
include_once ('../private/basicos_php/basico.php');
include_once ('../private/basicos_php/errores.php');
include_once ('../private/inc_web/version.php');
require_once("../private/basicos_php/url.inc.php");
require_once("../private/basicos_php/lang.php");



$federada = null;
$db_conexion = mysqli_connect("$host", "$hostu", "$hostp");
mysqli_set_charset($db_conexion, "utf8");
mysqli_select_db($db_conexion, "$dbn");



    if (!$cfg_autenticacion_solo_local && isset($_REQUEST['federada']) && $_REQUEST['federada'] == "1") {

        $as = new SimpleSAML_Auth_Simple('default-sp');
        $as->requireAuth(); // esto hace exit si no esta autenticado

        $attributes = $as->getAttributes();

        $federada = true;
        $_POST['user_admin'] = $attributes['usuario'][0];
        $_POST['pass_admin'] = "";

        // creamos usuario si hemos elegido la opcion en configuracion
        if ($cfg_crear_usuarios_automaticamente) {

            $username = fn_filtro($db_conexion, $_POST['user_admin']);
            $passw = "NOIMPORTAESMD5";
            $nivel_usuario = 1;
            $nivel_acceso = 10; //maximo nivel de administrador
            $tipo_votante = $cfg_tipo_usuario;

            $nif = fn_filtro($db_conexion, $_POST['user_admin']);
            $nombre_usuario_new = fn_filtro($db_conexion, $_POST['user_admin']);
            $email = $attributes['correo_usuario'][0];

            $usuario_consulta = mysqli_query($db_conexion, "SELECT usuario FROM $tbn9 WHERE usuario='" . $username . "'");

            // no existe lo creamos
            if (mysqli_num_rows($usuario_consulta) == 0) {
                $insql = "insert into $tbn9 (pass,usuario,correo_usuario,tipo_votante,nivel_acceso,nivel_usuario,nombre_usuario,apellido_usuario,nif) values (  \"$passw\",  \"$nombre_usuario_new\", \"$email\", \"$tipo_votante\", \"$nivel_acceso\", \"$nivel_usuario\", \"$username\",\"$nif\")";
                mysqli_query($con, $insql);
            }

            if (mysqli_error($con)) {
                session_destroy();
                echo "<script> window.location='admin.php?c=".encrypt_url('admin/usuarios/s=SD',$clave_encriptacion)."&error_login=8'; </script>";
                exit;
            }
        }

        $federada = true;
    }


    if ($federada || (isset($_POST['user_admin']) && isset($_POST['pass_admin']))) {

        //evitamos el sql inyeccion
        $user1 = fn_filtro($db_conexion, $_POST['user_admin']);
        $pass1 = fn_filtro($db_conexion, $_POST['pass_admin']);

        $usuario_consulta = mysqli_query($db_conexion, "SELECT usuario,pass,nivel_usuario,nivel_acceso   FROM $tbn9 WHERE usuario='" . $user1 . "'") or die(header("Location:  $redir?error_login=1"));

        if (mysqli_num_rows($usuario_consulta) != 0) {
            $login = stripslashes($user1);
            //$password = md5($pass1);

            $usuario_datos = mysqli_fetch_array($usuario_consulta);
            mysqli_free_result($usuario_consulta);

            mysqli_close($db_conexion);


            if ($login != $usuario_datos['usuario']) {
                  echo "<script> window.location='admin.php?c=".encrypt_url('admin/usuarios/s=SD',$clave_encriptacion)."&error_login=4'; </script>";
                exit;
            }

            // if (!$federada && $password != $usuario_datos['pass']) {
            if (!$federada && !password_verify($pass1, $usuario_datos['pass'])) {
                  echo "<script> window.location='admin.php?c=".encrypt_url('admin/usuarios/s=SD',$clave_encriptacion)."&error_login=3'; </script>";
                exit;
            }


            unset($login);
            unset($password);



            $_SESSION['usuario_login'] = $usuario_datos['usuario'];
            $_SESSION['nivel_usu'] = $usuario_datos['nivel_usuario'];  //tipo de usuario (administardor, votante, etc
            $_SESSION['usuario_nivel'] = $usuario_datos['nivel_acceso']; //nivel del usuario dentro del tipo
          //  echo "<script> window.location='admin.php?c=admin/usuarios_gestion/".encrypt_url('s=SD',$clave_encriptacion)."'; </script>";
            //Header("Location:" . $_SERVER['PHP_SELF'] . "?");
            //exit;
        }else{
          echo "<script> window.location='admin.php?c=".encrypt_url('admin/usuarios/s=SD',$clave_encriptacion)."&error_login=4'; </script>";
        }
    }



// comprueba que las tres principales variables de session existen , si falta alguna, destruimos la session y salimos
if (!isset($_SESSION['usuario_login']) || !isset($_SESSION['usuario_password']) || !isset($_SESSION['usuario_nivel'])) {
    session_destroy();
      echo "<script> window.location='index.php?error_login=6'; </script>";
    exit;
}
?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################

/**
* Archivo con la versión actual de la aplicación y que comprueba si hay una versión superior.
*/


$DKversion = "4.5.4";  // verion de esta distribución

if (isset($_SESSION['usuario_nivel'])) {
    if ($_SESSION['usuario_nivel'] == "0") {

        if ($info_versiones == true) {
/*
            function get_content($URL) {
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_URL, $URL);
                $data = curl_exec($ch);
                curl_close($ch);
                return $data;
            }
*/
            if (isset($_SESSION['version'])) {
                $la_ultima_version = $_SESSION['version'];
            } else {
                $la_ultima_version = @file_get_contents('https://actualizaciones.demokratian.org/version.txt'); //quitamos el error por si no hay conexiona internet y estan trabajando en local
                $_SESSION['version'] = $la_ultima_version; // metemos la informacion en la sesion para no tener que hacer continuamente la llamada al doc de versiones
            }



            $ultima_version = explode("##", $la_ultima_version);

            if ($ultima_version[0] > $DKversion) {
                ?>

                    <?php
                    // Lista de prioridades que salen de $ultima_version[2]
                    // 1 -- Critica seguridad
                    // 2 -- muy Importante, correccion de bug
                    // 3 -- Importante
                    // 4 -- Mejora
                    ///hacemos una reserva para un desarrollo posterior
                    ?>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"> <span class="badge badge-warning"> <?= _("Nueva versión dsiponible") ?> <span class="badge badge-danger"> <?php echo " $ultima_version[0]"; ?> </span><b class="caret"></b></a>
                        <div class="dropdown-menu">
                          <a class="dropdown-item" href="<?php echo "$ultima_version[1]"; ?>" target="_blank" ><?= _("Descargar") ?> <?php echo " $ultima_version[0]"; ?></a>
                          <a class="dropdown-item" href="<?php echo "$ultima_version[3]"; ?>" target="_blank" ><?= _("¿Que hay nuevo?") ?> <?php echo " $ultima_version[0]"; ?></a>

                          <div class="dropdown-divider"></div>

                        <a  class="dropdown-item" href="admin.php?c=<?php echo encrypt_url('admin/constantes/s=SD',$clave_encriptacion); ?>"  ><?= _("deshabilitar avisos") ?></a>

                        </div>
                      </li>

                <?php
            }
        }
    }
}
?>

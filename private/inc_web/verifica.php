<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que verifica el acceso de los usuarios al sistema, comprueba el captcha si se ha habilitado, el usuario y contraseña y genera todas las variables de session necesarias. Verifica si existe en todo momento una serie de sesiones necesarias
* @todo ver si separamos en un archivo independiente la última parte donde se verifica en cada momento que sí existen una serie de sessiones
*/
require_once("../private/config/config.inc.php");
require_once("conexion.php");
include_once('../private/basicos_php/basico.php');
include_once('../private/basicos_php/errores.php');
include_once('../private/inc_web/version.php');
require_once("../private/basicos_php/url.inc.php");

/* establecer el limitador de cache a 'private' */

session_cache_limiter('private');
/* establecer la caducidad de la cache a 30 minutos */
session_cache_expire(30);
//session_name($usuarios_sesion);
session_start();


require_once("../private/basicos_php/lang.php");
/*
if(isset($_SERVER['HTTP_REFERER'])) {
    $url = explode("?", $_SERVER['HTTP_REFERER']);
    $pag_referida = $url[0];
    echo $pag_referida;
    $url_vot = $pag_referida;
   }
*/


$federada = null;


    if (!$cfg_autenticacion_solo_local && isset($_REQUEST['federada']) && $_REQUEST['federada'] == "1") {

      $db_conexion = mysqli_connect("$host", "$hostu", "$hostp") or die(header("Location:  $url_vot/index.php?error_login=0&d=".random_int(1000000, 9999999)));
      mysqli_set_charset($db_conexion, "utf8");
      mysqli_select_db($db_conexion, "$dbn");

        $as = new SimpleSAML_Auth_Simple('default-sp');
        $as->requireAuth(); // esto hace exit si no esta autenticado

        $attributes = $as->getAttributes();

        $federada = true;
        $_POST['user'] = $attributes['usuario'][0];
        $_POST['pass'] = "";

        // creamos usuario si hemos elegido la opcion en configuracion
        if ($cfg_crear_usuarios_automaticamente) {

            $username = fn_filtro($db_conexion, $_POST['user']);
            $passw = "NOIMPORTAESMD5";
            $nivel_usuario = 1;
            $nivel_acceso = 10; //maximo nivel de administrador
            $tipo_votante = $cfg_tipo_usuario;

            $nif = fn_filtro($db_conexion, $_POST['user']);
            $nombre_usuario_new = fn_filtro($db_conexion, $_POST['user']);
            $email = $attributes['correo_usuario'][0];

            $usuario_consulta = mysqli_query($db_conexion, "SELECT usuario FROM $tbn9 WHERE usuario='" . $username . "'");

            // no existe lo creamos
            if (mysqli_num_rows($usuario_consulta) == 0) {
                $insql = "insert into $tbn9 (pass,usuario,correo_usuario,tipo_votante,nivel_acceso,nivel_usuario,nombre_usuario,apellido_usuario,nif) values (  \"$passw\",  \"$nombre_usuario_new\", \"$email\", \"$tipo_votante\", \"$nivel_acceso\", \"$nivel_usuario\", \"$username\",\"$nif\")";
                mysqli_query($con, $insql);
            }

            if (mysqli_error($con)) {
                session_destroy();
                Header("Location: $url_vot/index.php?error_login=8&d=".random_int(1000000, 9999999));
                exit;
            }
        }

        $federada = true;
    }


///////  si no usamos la autentificación federada

    if ($federada || (isset($_POST['user']) && isset($_POST['pass']))) {

      if ($reCaptcha == true) {

        if ($tipo_reCaptcha == "reCAPTCHA_v2") {
            if(isset($_POST['g-recaptcha-response'])){
              $captcha=$_POST['g-recaptcha-response'];
            }
            if(!$captcha){
              // falta el g-recaptcha
              $validoCaptcha = false;
              Header("Location: $url_vot/index.php?error_login=10&d=".random_int(1000000, 9999999));
              exit;
            }

            $ip = $_SERVER['REMOTE_ADDR'];
            // post request to server
            $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($reCAPTCHA_secret_key) .  '&response=' . urlencode($captcha);
            $response = file_get_contents($url);
            $responseKeys = json_decode($response,true);
            // should return JSON with success as true
            if($responseKeys["success"]) {
                    //Captcha correcto
                    $validoCaptcha = true;
            } else {
                    // eres un robot, asi que terminamos
                    $validoCaptcha = false;
                    Header("Location: $url_vot/index.php?error_login=10&d=".random_int(1000000, 9999999));
                    exit;
            }
          }else{  // se se usa recatcha v3
              define("RECAPTCHA_V3_SECRET_KEY", $reCAPTCHA_secret_key);

              $token = $_POST['token'];
              $action = $_POST['action'];

                      // call curl to POST request
              $ch = curl_init();
              curl_setopt($ch, CURLOPT_URL,"https://www.google.com/recaptcha/api/siteverify");
              curl_setopt($ch, CURLOPT_POST, 1);
              curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('secret' => RECAPTCHA_V3_SECRET_KEY, 'response' => $token)));
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              $response = curl_exec($ch);
              curl_close($ch);
              $arrResponse = json_decode($response, true);

                if($arrResponse["success"] != '1' && $arrResponse["action"] != $action && $arrResponse["score"] <= $sensibilidad) {
                  // Si entra aqui, es un robot....
                  $validoCaptcha = false;
                    Header("Location: $url_vot/index.php?error_login=10&d=".random_int(1000000, 9999999));
                    exit;
                } else {
                    //Captcha correcto
                    $validoCaptcha = true;
                }
          }
///////////////////////////////////////
      } else {
          // si no tenemos activado el sistema de captcha miramos el token a ver si esta bien
          //// comprobamos el token es correcto y llega bien
          $server =$_SERVER['SERVER_NAME'];
          $token=$_POST['token'];
          $clave= $clave_encriptacion2.$server;

              if(!password_verify($clave, $token)){ // comprobamos token para evitar accesos por ataque csrf
                $validoCaptcha = false;
                  Header("Location: $url_vot/index.php?error_login=12&d=".random_int(1000000, 9999999));
                  exit;
              }else{
                // si esta bien el token, damos el ok
          $validoCaptcha = true;
        }
      }


      if($validoCaptcha == true){  // si la validación del Captcha es correta o no esta activada

        $db_conexion = mysqli_connect("$host", "$hostu", "$hostp") or die(header("Location:  $url_vot/index.php?error_login=0&d=".random_int(1000000, 9999999)));
        mysqli_set_charset($db_conexion, "utf8");
        mysqli_select_db($db_conexion, "$dbn");


            if ($_SERVER['HTTP_REFERER'] == "") {
                Header("Location: $url_vot/index.php?error_login=0&d=".random_int(1000000, 9999999));
                exit;
                }

            //evitamos el sql inyeccion
            $user1 = fn_filtro($db_conexion, $_POST['user']);
            $pass1 = fn_filtro($db_conexion, $_POST['pass']);


            $usuario_consulta = mysqli_query($db_conexion, "SELECT ID,usuario,pass,id_provincia,nombre_usuario,apellido_usuario ,tipo_votante,bloqueo,id_ccaa,nivel_usuario,nivel_acceso,imagen_pequena,fecha_control,id_municipio,razon_bloqueo,admin_blog   FROM $tbn9 WHERE usuario='" . $user1 . "'") or die(header("Location:  $redir?error_login=1"));

            if (mysqli_num_rows($usuario_consulta) != 0) {
                $login = stripslashes($user1);
                //$password = md5($pass1);


                $usuario_datos = mysqli_fetch_array($usuario_consulta);
                mysqli_free_result($usuario_consulta);

                mysqli_close($db_conexion);


                if ($login != $usuario_datos['usuario']) {
                    Header("Location: $url_vot/index.php?error_login=4&d=".random_int(1000000, 9999999));
                    exit;
                }

                // if (!$federada && $password != $usuario_datos['pass']) {
                if (!$federada && !password_verify($pass1, $usuario_datos['pass'])) {
                    Header("Location: $url_vot/index.php?error_login=3&d=".random_int(1000000, 9999999));
                    exit;
                }


                if ($usuario_datos['bloqueo'] == "si") {
                    Header("Location: $url_vot/index.php?error_login=13&tx=" . $usuario_datos['razon_bloqueo'] . "");
                    exit;
                }

                unset($login);
                unset($password);

              /*  if ($civi == true) {
                     $datetime1 = $usuario_datos['fecha_control'];
                      $datetime2 = date("Y-m-d");
                      $interval = date_diff($datetime1, $datetime2);
                      $interval->format('%R%a d�as');

                } */

                if ($federada) {
                    $_SESSION['federada'] = true;
                }

                $_SESSION['ID'] = $usuario_datos['ID'];
                $_SESSION['tipo_votante'] = $usuario_datos['tipo_votante'];
                $_SESSION['usuario_login'] = $usuario_datos['usuario'];
                $_SESSION['usuario_password'] = $usuario_datos['pass'];
                $_SESSION['nombre_usu'] = $usuario_datos['nombre_usuario'];
                $_SESSION['apellido_usu'] = $usuario_datos['apellido_usuario'];
                $_SESSION['localidad'] = $usuario_datos['id_provincia'];
                $_SESSION['id_ccaa_usu'] = $usuario_datos['id_ccaa'];
                $_SESSION['id_municipio'] = $usuario_datos['id_municipio'];
            //    $_SESSION['id_subgrupo_usu'] = $usuario_datos['id_subgrupo'];
                $_SESSION['nivel_usu'] = $usuario_datos['nivel_usuario'];  //tipo de usuario (administardor, votante, etc
                $_SESSION['usuario_nivel'] = $usuario_datos['nivel_acceso']; //nivel del usuario dentro del tipo
                $_SESSION['imagen'] = $usuario_datos['imagen_pequena']; /////imagen del usuario
                $_SESSION["ultimoAcceso"] = date("Y-n-j H:i:s");  // ponemos la fehca y hora actual para ponerle caducidad a la sesion
                $_SESSION['admin_blog'] = $usuario_datos['admin_blog']; // sesion que indica si es administrador del blog


                ///////////////// miramos el codigo de la provincia para meterlo en la sesion
                $options = "select DISTINCT ID,provincia from $tbn8  where ID = " . $usuario_datos['id_provincia'] . "";
                $resulta = mysqli_query($con, $options) or die("error: " . mysqli_error());
                while ($listrows = mysqli_fetch_array($resulta)) {
                    $provin = $listrows['ID'];
                    $_SESSION['provincia'] = $listrows['provincia'];
                }

                ///////////////////////miramos la comunidad autonoma para meterlo tambien en la sesion

                $options2 = "select DISTINCT ID,ccaa from $tbn3  where ID = " . $usuario_datos['id_ccaa'] . " ";
                $resulta2 = mysqli_query($con, $options2) or die("error: " . mysqli_error());
                while ($listrows2 = mysqli_fetch_array($resulta2)) {
                    $_SESSION['ccaa'] = $listrows2['ccaa'];
                }

                ///////////////////////miramos el municipio para meterlo tambien en la sesion

                $options2 = "select DISTINCT nombre from $tbn18  where id_municipio = " . $usuario_datos['id_municipio'] . " ";
                $resulta2 = mysqli_query($con, $options2) or die("error: " . mysqli_error());
                while ($listrows2 = mysqli_fetch_array($resulta2)) {
                    $_SESSION['municipio'] = $listrows2['nombre'];
                }


                $fecha = date("Y-m-d H:i:s");
                $insql = "UPDATE $tbn9 SET fecha_ultima =\"$fecha\" WHERE id=" . $usuario_datos['ID'] . " ";
                $inres = @mysqli_query($con, $insql);

                Header("Location:" . $_SERVER['PHP_SELF'] . "?d=".random_int(1000000, 9999999));
                exit;
            }
    } else {
            // eres un robot, asi que terminamos
            $validoCaptcha = false;
            Header("Location: $url_vot/index.php?error_login=10&d=".random_int(1000000, 9999999));
            exit;
    }
    }


// comprueba que las tres principales variables de session existen , si falta alguna, destruimos la session y salimos
if (!isset($_SESSION['usuario_login']) || !isset($_SESSION['usuario_password']) || !isset($_SESSION['usuario_nivel'])) {
    session_destroy();
    Header("Location: $url_vot/index.php?error_login=7&d=".random_int(1000000, 9999999));
    exit;
}
?>

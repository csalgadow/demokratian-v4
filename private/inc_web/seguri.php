<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que comprueba si se está pasando una variable con el identificador de la votación, si es así, saca todos los datos  de esa votación que las mete en variables y comprueba si el usuario puede votar en esa votación, renueva la hora de la sesión para evitar que caduque.
* @todo Este archivo hay que repasar si conviene separarlo en varias partes, ver si la comprobación de si el usuario puede acceder a esa votación o no , se debe de hacer aquí, y el renovar la hora de la sesión, si hacerlo también en archivo separado
*/

if ($variables['idvot'] != "") { //variable que nos llega con la votacion
    $idvot=$variables['idvot'];
    //$idvot = fn_filtro_numerico($con, $_GET['idvot']);
    $activa = "si"; //Si la votacion esta activa o no
    $sql_vot = "SELECT id_provincia,activa,tipo,tipo_votante,nombre_votacion,texto,resumen,numero_opciones,id_ccaa,id_subzona,id_grupo_trabajo, demarcacion, seguridad,activos_resultados,fecha_com,fecha_fin,recuento,id_municipio,encripta FROM $tbn1  WHERE ID='$idvot' and activa='$activa' ";
    $res_votacion = mysqli_query($con, $sql_vot);
    $row_vot = mysqli_fetch_row($res_votacion);

    $id_provincia = $row_vot[0];
    $activa = $row_vot[1];
    $tipo = $row_vot[2];
    $tipo_votante = $row_vot[3]; // Tipo de acceso para esta página.
    $nombre_votacion = $row_vot[4];
    $texto = $row_vot[5];
    $resumen = $row_vot[6];
    $numero_opciones = $row_vot[7];
    $id_ccaa = $row_vot[8];
    $id_subzona = $row_vot[9];
    $id_grupo_trabajo = $row_vot[10];
    $demarcacion = $row_vot[11];
    $seguridad = $row_vot[12];
    $activos_resultados = $row_vot[13];
    $fecha_com = $row_vot[14];
    $fecha_fin = $row_vot[15];
    $recuento = $row_vot[16];
    $id_municipio = $row_vot[17];
    $encripta = $row_vot[18];

    mysqli_free_result($res_votacion);
    if ($row_vot[3] != 5) {
    //    require_once ("verifica.php");
        //$nivel_acceso=$row_vot[3]; // Tipo de acceso para esta página.
        if ($_SESSION['tipo_votante'] > $tipo_votante) {
            session_destroy();
            header("Location: $url_vot?error_login=5");
            exit;
        } else {
            $fechaGuardada = $_SESSION["ultimoAcceso"];
            $ahora = date("Y-n-j H:i:s");
            $tiempo_transcurrido = (strtotime($ahora) - strtotime($fechaGuardada)); //comparamos el tiempo transcurrido

            if ($tiempo_transcurrido >= $tiempo_session) { //miramos si el tiempo es superior al que tenemos por configuración
                session_destroy();
                header("Location: $url_vot/index.php?error_login=11"); //envío al usuario a la pag. de autenticación
                exit;
            } else {
                $_SESSION["ultimoAcceso"] = $ahora; //actualizamos el tiempo
            }
        }
    } else { // es es una encuesta abierta el usuario puede participar
        echo "";
    }

} else {  // no existe la variable del identificador de la votación, asi que salimos del sistema
    Header("Location: $url_vot/index.php?error_login=6");
    session_destroy();
    exit;
}
?>

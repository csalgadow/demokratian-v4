<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
*  Archivo que lista todos los votos
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{

  if ($variables['idvot'] != "") {
      $idvot=$variables['idvot'];
      //$idvot = fn_filtro_numerico($con, $_GET['idvot']); //variable que nos llega con la votacion
      //$activa = "si"; //Si la votacion esta activa o no
      //$sql_vot = "SELECT nombre_votacion,resumen FROM $tbn1  WHERE ID='$idvot' and activa='$activa' ";
      $sql_vot = "SELECT nombre_votacion,resumen FROM $tbn1  WHERE ID='$idvot' ";
      if($res_votacion = mysqli_query($con, $sql_vot)){
      $row_vot = mysqli_fetch_row($res_votacion);


      $nombre_votacion = $row_vot[0];
      $resumen = $row_vot[1];

      mysqli_free_result($res_votacion);
    }
  }

//include('../private/inc_web/seguri.php');
//$nivel_acceso = 10;
//include('../private/inc_web/nivel_acceso.php');


ini_set('memory_limit', '3064M'); //cantidad de memoria
ini_set('max_execution_time', 900); //900 seconds = 15 minutes
$timeStart = microtime(true);
ob_start();
?>
<link href="temas/<?php echo "$tema_web"; ?>/css/vutStyle.css" rel="stylesheet">
<div class="card-header-votaciones "> <h1 class="card-title"><?php echo "$nombre_votacion"; ?></h1> </div>


<div class="card-body">

  <?php if($resumen!=""){?>
                  <div class="card">
                    <div class="card-body">
                      <?php echo "$resumen";?>
                    </div>
                  </div>
                      <?php
                    }
                    ?>

                </div>
            </div>


            <h2><?= _("Candidatos u opciones de esta votación") ?></h2>

            <table class="table table-striped">
                <tr>

                    <th width="10%"><?= _("Identificador") ?></th>
                    <th width="80%"><?= _("Nombre") ?></th>
                    <th width="10%"><?= _("Sexo") ?></th>
                </tr>
                <?php
// sacamos los datos del array

                $sql2 = "SELECT id_vut, nombre_usuario,sexo  FROM $tbn7 WHERE id_votacion=" . $idvot . " ";
                $result2 = mysqli_query($con, $sql2);
                if ($row2 = mysqli_fetch_array($result2)) {
                    mysqli_field_seek($result2, 0);

                    do {
                        ?>
                        <tr>
                            <td><?php echo $row2[0]; ?></td>
                            <td><?php echo $row2[1]; ?></td>
                            <td><?php echo $row2[2]; ?></td>


                        </tr>

                        <?php
                    } while ($row2 = mysqli_fetch_array($result2));
                }
                ?>

            </table>



            <h2><?= _("Lista de todos los votos de esta votación") ?></h2>

            <p><?= _("Identificador del candidato u opcion y el orden que se haya marcado") ?> </p>

            <?php
            $Ballotname = md5($idvot); //encriptamos el id de la votacion
            $enlace_vot = $FilePath . $Ballotname . "_ballots.txt";
            $i = 1;
            $file = fopen($enlace_vot, "r");
            //fseek($file, -1, SEEK_END);
            while (!feof($file)) {

                //echo $i++ ." | " ;
                echo fgets($file) . "<br />";
            }
            fclose($file);
            ?>


            <!--fin nuevo bloque-->



<?php
$contenido = ob_get_contents();
ob_end_flush();
$idvot_encriptada = md5($idvot);
$archivo = $FileRec . $idvot_encriptada . "_list.php";
$crear = fopen($archivo, "w");
$grabar = fwrite($crear, $contenido);
fclose($crear);

$timeEnd = microtime(true);
$timeElapsed = $timeEnd - $timeStart;
echo "<br/>";

printf("Memory used: %s kB\n", memory_get_peak_usage() / 1024);
printf("Total time: %s s\n", $timeElapsed);
/* fin nuevo */
?>
<?php } ?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
*  Archivo que genera el voto del tipo VUT
*/
if(!isset($carga)){
  $carga =false;
  exit;
}
if($carga!="OK"){
  exit;
}else{
  include('../private/inc_web/seguri.php');

require ("../private/basicos_php/funcion_control_votacion.php");

$Seats = $numero_opciones;
$Ballotname = md5($variables['idvot']);
$Pollname = md5($variables['idvot']);
$valores = "";
$datos_votado = "";
?>
<link href="temas/<?php echo "$tema_web"; ?>/css/vutStyle.css" rel="stylesheet">
                    <!--Comiezo-->

                    <div class="card-header-votaciones "> <h1 class="card-title"><?php echo"$nombre_votacion"; ?></h1> </div>


                    <div class="card-body">


                    <?php
                    if (ISSET($_POST["submit"])) {
                      $i=1;
                      foreach ($_POST as $v) {
                            $datos_votacion[$i] = $v;
                            $i++;
                        }


                        //$datos_votacion = each($_POST);

                        if ($datos_votacion[1] == "--") {

                            $errores = _("No ha votado nada") . " <br/> " . _("vuelva a realizar la votación");
                        } else if ($_POST['clave_seg'] == "") {
                            $errores = _("Falta la clave de seguridad") . " <br> " . _("vuelva a realizar la votación");
                        } else {
                            ?>



                            <?php

                            $id_ccaa = $_SESSION['id_ccaa_usu'];
                            $clave_seg = fn_filtro($con, $_POST['clave_seg']);

                            $Cands = fn_filtro_numerico($con, $_POST['Cands']);


                            if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                            } elseif (isset($_SERVER['HTTP_VIA'])) {
                                $ip = $_SERVER['HTTP_VIA'];
                            } elseif (isset($_SERVER['REMOTE_ADDR'])) {
                                $ip = $_SERVER['REMOTE_ADDR'];
                            }

                            $forma_votacion = 2;


                            list ($estado, $razon, $tipo_votante) = fn_mira_si_puede_votar($demarcacion, $_SESSION['ID'], $idvot, $id_ccaa, $id_provincia, $id_grupo_trabajo, $id_municipio);

//if(!$error) {

                            if ($estado == "error") {
                                if ($razon == "direccion_no_existe") {
                                    $errores = "<br/>" . _("Esta direccion de correo no la tenemos registrada para esta provincia, quizas sea un error de nuestra base de datos , si consideras que tienes derecho a votar haz") . "<a href=\"../votaciones/voto_contacto.php\">" . _("click aqui para enviarnos tus datos a traves de nuestro formulario") . "</a><br/>";
                                }
                                if ($razon == "ya_ha_votado") {
                                    $errores = "<br/>" . _("Ya ha votado en esta votación") . "<br/>";
                                }
                            } else if ($estado == "TRUE" and $razon == "usuario_ok") {
                                /////////////////////////// si podemos procesar el formulario
                                $codi = hash("sha512", $clave_seg);
                                ?>

                                <?php
                                reset($_POST);
                                unset($_POST['clave_seg']); ///borramos la otra variable para dejar solo los datos de votos
                                unset($_POST['Cands']); ///borramos la otra variable para dejar solo los datos de votos
                                $cuenta = 1;
                                foreach ($_POST as $clave => $val){
                              //  while (list ($clave, $val) = each($_POST)) {
                                    // echo "$clave - $val +; ";
                                    $array_id = explode('__', $clave);
                                    if ($clave == "submit") {

                                    } else {

                                        if ($val == "--") {

                                        } else {
                                            ?>


                                            <?php
                                            //$valores .= "$array_id[1]" . ","; ///montamos una cadena separada por comas con los id de los candidatos para meterlos en la hoja de recuento
                                            $valores .= fn_filtro($con, $array_id[1]) . ","; ///montamos una cadena separada por comas con los id de los candidatos para meterlos en la hoja de recuento

                                            $datos_votado .= _("Identificador candidato") . " -->" . $array_id[1] . " | " . _("Orden del voto") . " --- " . $cuenta . "<br/>";
                                        }
                                    }
                                    $cuenta++;
                                }
                                ?>

                                <?php
                                $valores = trim($valores, ','); ///quitamos la ultima coma de la cadena
                                $valores_fin = $valores . "\n";


                                $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
                                $cad = "";
                                for ($i = 0; $i < 4; $i++) {
                                    $cad .= substr($str, rand(0, 62), 1);
                                }


                                $time = microtime(true);
                                $timecad = $time . $cad;
                                $res_id = hash("sha256", $timecad);


                                $insql = "insert into $tbn15 (ID,voto,id_candidato,id_provincia, id_votacion,codigo_val) values (\"$res_id\",\"$valores_fin\",\"0\"," . $_SESSION['localidad'] . ", \"$idvot\",\"$codi\")";
                                $mens = "mensaje añadido";
                                $result = db_query($con, $insql, $mens);

                                if (!$result) {
                                    echo "<strong><font color=#FF0000 siz<br/> UPSSS!!!!<br/>" . _("esto es embarazoso, hay un error y su votacion no ha sido registrada") . " </font></strong>";
                                }
                                if ($result) {
                                    //si hay resultado del proceso anterior ejecutamos el resto
                                    // abrimos el fichero y escribmos los datos
                                    $fp = fopen($FilePath . $Ballotname . "_ballots.txt", "a+");
                                    // fputs($fp,$NuBallot.$cr);
                                    if (fwrite($fp, $valores_fin) === FALSE) {
                                        $errores = _("No se puede escribir al archivo")."(" .$nombre_archivo.")";
                                        exit;
                                    }
                                    fclose($fp);
                                    // ALTA del usuario en la lista de los que han votado para que no pueda volver a votar
                                    $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
                                    $cad = "";
                                    for ($i = 0; $i < 6; $i++) {
                                        $cad .= substr($str, rand(0, 62), 1);
                                    }
                                    $time = microtime(true);
                                    $timecad2 = $time . $cad;
                                    $user_id = hash("sha256", $timecad2);

                                    if ($seguridad==3 or $seguridad==4){
                                      $fecha_env = date("Y-m-d 00:00:01"); // si la votación lleva envios de correos a los interventores, quitamos la hora para que no se pueda vincular la hora del voto
                                    }else{
                                      $fecha_env = date("Y-m-d H:i:s");
                                    }
                                    $insql = "insert into $tbn2 (ID,id_provincia,id_votacion,id_votante,fecha,tipo_votante,ip,forma_votacion) values (\"$user_id\"," . $_SESSION['localidad'] . ",\"$idvot\"," . $_SESSION['ID'] . ",\" $fecha_env\",\" $tipo_votante\",\" $ip\",\" $forma_votacion\")";
                                    $mens = "<br/>" . _("¡¡¡ATENCION!!!!, el voto ha sido registrado , pero el usuario no ha sido bloqueado") . " <br>" . _("el ID de usuario es") . ":" . $_SESSION['ID'];
                                    $resulta = db_query($con, $insql, $mens);



                                    //////////////////////metemos la seguridad del envio de correos a interventores

                                    if ($seguridad == 3 or $seguridad == 4) {
                                        include('../private/basicos_php/envio_interventores.php');
                                    }
                                    ///////// fin envio a interventores

                                    ?>
                                      <p><?= _("Esto es lo que ha votado") ?> </p>

                                    <div class="table-responsive">
                                        <table width="90" class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th width="17%">
                                                          <?= _("Orden") ?></th>
                                                    <th width="83%"><?= _("Candidato")?> - <?= _("Opción") ?></th> </tr>
                                                <?php
                                                $valoresarray = explode(',', $valores);
                                                $numrows = count($valoresarray);
                                                for ($i = 0; $i < $numrows; $i++) {
                                                    ?>
                                                    <tr><td>
                                                            <?php
                                                            $cuenta = $i + 1;
                                                            echo "$cuenta";
                                                            ?>

                                                        </td ><td>
                                                            <?php
                                                            $result2 = mysqli_query($con, "SELECT nombre_usuario,imagen_pequena FROM $tbn7 WHERE id_vut = '$valoresarray[$i]' and id_votacion='$idvot'");
                                                            $linea = mysqli_fetch_row($result2);
                                                            if ($linea[1] == "") {
                                                                 } else { ?>
                                                                   <img src="<?php echo $upload_cat; ?>/<?php echo $linea[1]; ?>" alt="<?php echo $linea[0]; ?>" width="60" height="60"  />
                                                            <?php
                                                            }
                                                            echo $linea[0];
                                                            ?>
                                                        </td></tr>
                                                <?php } ?>

                                        </table></div>
                                    <!--si todo va bien damos las gracisa por participar-->
                                    <div class="alert alert-success">
                                        <h3  align="center"><?= _("Gracias por participar") ?></h3>
                                        <strong><?= _("En  breve estaran los resultados") ?></strong></div>


                                    <?php
                                }
                            }
                        }
                    } else {

                        $errores = _("No ha accedido de forma correcta a esta votación");
                    }
                    ?>
                    <?php if (isset($errores)) { ?>
                        <div class="alert alert-danger">
                            <strong> <?php echo $errores; ?></strong>
                        </div>
                    <?php } ?>
                    <!--Final-->
</div>
<?php } ?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
*  Archivo que lanza el recuento de tipo VUT mediante el archivo dctally.php y genera un archivo con ese recuento con el  votacion_tally.txt
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{


//  include('../private/inc_web/seguri.php');
//  $nivel_acceso = 10;
//  include('../private/inc_web/nivel_acceso.php');

//require ("../private/basicos_php/funcion_control_votacion.php");  //ver si sobra
//echo $idvot;

$idvot = fn_filtro_numerico($con, $variables['idvot']); //variable que nos llega con la votacion
$activa = "si"; //Si la votacion esta activa o no
$sql_vot = "SELECT nombre_votacion,numero_opciones,activos_resultados,fecha_com,fecha_fin,recuento FROM $tbn1  WHERE ID='$idvot'  ";
$res_votacion = mysqli_query($con, $sql_vot);
$row_vot = mysqli_fetch_row($res_votacion);


$nombre_votacion = $row_vot[0];
$numero_opciones = $row_vot[1];
$activos_resultados = $row_vot[2];
$fecha_com = $row_vot[3];
$fecha_fin = $row_vot[4];
$recuento = $row_vot[5];


$Seats = $numero_opciones;
$Ballotname = md5($idvot);
$Pollname = md5($idvot);

$conta = "SELECT id  FROM $tbn7 WHERE id_votacion = '$idvot' ";

$result2 = mysqli_query($con, $conta);
$Cands = mysqli_num_rows($result2); /// nuemro de opciones existentes (sacar de la tabla candidatos)

?>
<link href="temas/<?php echo "$tema_web"; ?>/css/vutStyle.css" rel="stylesheet">
                    <!--Comiezo-->

                    <div class="card-header-votaciones "> <h1 class="card-title"><?php echo"$nombre_votacion"; ?></h1> </div>


                    <div class="card-body">
                    <h2><?= _("Lanzado recuento de la votación") ?></h2>


                    <p>&nbsp;</p>


                    <?php

//----------------------------------------- corremos el script

                    function getmicrotime() { ///  si quitamos el calculo  de recuento de votos podemos quitarlo
                        list($usec, $sec) = explode(" ", microtime());
                        return ((float) $usec + (float) $sec);
                    }

                    /**/

                    echo "Realizando recuento de votos... ";
                    $time_start = getmicrotime();

                    require("../private/vota_vut/dctally.php");
                    $Ballots = NULL;
                    $time_end = getmicrotime();
                    echo "<p>Hecho en " . round($time_end - $time_start, 2) . " seg.</p>";
                ?>
                <div class="jumbotron">
                <?php
                    //Cargamos elarchivo que se ha generado
                    $url=$FilePath.$Ballotname."_tally.txt";
                    $datosVotacion = file_get_contents($url);
                    $datosVotacion = nl2br($datosVotacion);
                    echo $datosVotacion;

                    ?>

                </div>
            <p><a href="votaciones.php?c=<?php echo encrypt_url('vota_vut/resultados/idvot='.$idvot,$clave_encriptacion) ?>"  class="btn btn-primary btn-xs" target="_blank"><?= _("Ver resultados en formato final");?> </a></p>
            <p></p>
                    <!--Final-->
</div>
              <?php }?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que emite el voto del tipo primarias por una petición ajax desde el archivo  primarias_vota.php con el archivo vota_orden/vot_orden_2.js o con vota_orden/vot_orden.js
*/
require_once ("../private/inc_web/verifica.php");
require_once ('../private/inc_web/seguri.php');

require_once ("../private/basicos_php/funcion_control_votacion.php");


$process_result = 'OK';
$msg_result = '';
$datos_votado='';

$idvot = fn_filtro_numerico($con, $variables['idvot']);
$id_provincia = fn_filtro_numerico($con, $_POST['id_provincia']);
$valores = fn_filtro($con, $_POST['valores']);
$id_ccaa = fn_filtro($con, $_POST['id_ccaa']);
if (isset($_POST['id_subzona']) && !empty($_POST['id_subzona'])) {
    $id_subzona = fn_filtro($con, $_POST['id_subzona']);
}
$id_grupo_trabajo = fn_filtro($con, $_POST['id_grupo_trabajo']);
$demarcacion = fn_filtro($con, $_POST['demarcacion']);
$clave_seg = fn_filtro($con, $_POST['clave_seg']);
$recuento = fn_filtro($con, $_POST['recuento']);
$mixto = fn_filtro($con, $_POST['mixto']);
$encripta = fn_filtro($con, $_POST['encripta']);

if (isset($_POST['blanco'])) {
    $blanco = true;
} else {
    $blanco = false;
}
if (isset($_POST['nulo'])) {
    $nulo = true;
} else {
    $nulo = false;
}
$nulo_blanco=0;

$sql3 = "SELECT seguridad, nombre_votacion, numero_opciones,id_municipio,id_grupo_trabajo FROM $tbn1 WHERE ID='$idvot' ";
$resulta3 = mysqli_query($con, $sql3) or die("error: " . mysqli_error());

while ($listrows3 = mysqli_fetch_array($resulta3)) {
    $seguridad = $listrows3['seguridad'];
    $nombre_votacion = $listrows3['nombre_votacion'];
    $numero_opciones = $listrows3['numero_opciones'];
    $id_municipio = $listrows3['id_municipio'];
    $id_grupo_trabajo = $listrows3['id_grupo_trabajo'];
}

if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} elseif (isset($_SERVER['HTTP_VIA'])) {
    $ip = $_SERVER['HTTP_VIA'];
} elseif (isset($_SERVER['REMOTE_ADDR'])) {
    $ip = $_SERVER['REMOTE_ADDR'];
}

if ($seguridad == 2 or $seguridad == 4) {
    if ($clave_seg == "") {
        $process_result = "ERROR";
        $msg_result = _("No ha indicado clave de seguridad para recuperar su voto");
        //break;
    } else {
        $codi = hash("sha512", $clave_seg);
        $sql_seg = "SELECT ID FROM $tbn10 WHERE id_votacion='$idvot'and codigo_val='$codi' ";
        $resulta_seg = mysqli_query($con, $sql_seg) or die("error: " . mysqli_error());
        $quants_seg = mysqli_num_rows($resulta_seg);
        if ($quants_seg != 0) {
            $process_result = "ERROR";
            $msg_result = _("Esa clave ya esta siendo usada, por favor, cambie por otra");
        }
    }
}
$forma_votacion = 1;
$valores = trim($valores, ';'); ///quitamos el ultimo punto y coma de la cadena
$array_valores = explode(";", $valores);

list ($estado, $razon, $tipo_votante) = fn_mira_si_puede_votar($demarcacion, $_SESSION['ID'], $idvot, $id_ccaa, $id_provincia, $id_grupo_trabajo, $id_municipio);

if ($estado == "error") {
    $process_result = "ERROR";

    if ($razon == "direccion_no_existe") {
        $msg_result = _("Esta dirección no esta registrada para esta votación en nuestra base de datos.");
    }
    if ($razon == "ya_ha_votado") {
        $msg_result = _("ya has votado en esta votación");
    }
} else if ($estado == "TRUE" and $razon == "usuario_ok") {
  if ($nulo != true and $blanco!= true){
    // CHECK VOTACIO
    $men = array();
    $women = array();
    $total = 0;
    foreach ($array_valores as $v) {
        $array_valor = explode(",", $v);
        $position = $array_valor[0];
        $candi = $array_valor[1];
        if ($position == "") {
            continue;
        }
        $total = $total + 1;

        if ($position < 1 || $position > $numero_opciones) {
            $process_result = "ERROR";
            $msg_result = _("Posicion erronea en la lista") . ":" . $v;
            break;
        }
        $sql = "SELECT sexo FROM $tbn7 WHERE id_votacion = '$idvot' and ID = '$candi'";
        $result = mysqli_query($con, $sql);
        if ($row = mysqli_fetch_array($result)) {
            if ($row[0] == "H") {
                if (array_key_exists($position, $men)) {
                    $process_result = "ERROR";
                    $msg_result = _("Posición repetida en la lista de hombres") . ":" . $position;
                    break;
                }
                $men[$position] = $candi;
            } else {
                if (array_key_exists($position, $women)) {
                    $process_result = "ERROR";
                    $msg_result = _("Posicion repetida en la lista de mujeres") . " : " . $position;
                    break;
                }
                $women[$position] = $candi;
            }
        } else {
            // candidat no trobat
            $process_result = "ERROR";
            $msg_result = _("Candidato no existe en la base de datos") . " ( " . $candi . ")" . _("Notifica el error a la  Comisión de Primarias");
            break;
        }
    }
    // Check blanc
    if ($total == 0) {
        if (!$blanco) {
            $process_result = "ERROR";
            $msg_result = _("No se ha seleccionado candidato y no ha indicado que el voto sea en blanco.");
        }
    } else {
        // Check ratio...
        // ugly hack. Check ratio only if numero_opciones > 2
        if ($mixto == "NOesMixto") {

        } else {
            if ($numero_opciones > 2) {
                //$ratio_men = count($men) / $total;
                //$ratio_men = count($men);
                $max_men =  count($men)-count($women);
                $max_women = count($women) - count($men);
                //if ($ratio_men > 0.6 || $ratio_men < 0.4) {
                //if ($ratio_men > $total_max || $ratio_men < $total_min) {
                  if ($max_men > 1 || $max_women > 1) {
                    if (!$nulo) {
                        $process_result = "ERROR";
                        $msg_result = _("El voto es nulo y no se ha indicado como tal");
                    }
                }
            }
        }
    }
}

    if ($process_result != "ERROR") {

        $codi = hash("sha512", $clave_seg);
        $okVot = true;
/*
        $text = '';
        for ($i = 0; $i < 6; $i++) {
            $d = rand(1, 30) % 2;
            $text .= $d ? chr(rand(65, 90)) : chr(rand(48, 57));
        }


        //$transactionid = "INSVOT" . $text;
        //mysqli_query($con, 'BEGIN ' . $transactionid);
*/

        // GET RANDOM ID FOR THE WHOLE VOTE
        $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
        $cad = "";
        for ($i = 0; $i < 4; $i++) {
            $cad .= substr($str, rand(0, 62), 1);
        }
        $time = microtime(true);
        $timecad = $time . $cad;
        $vote_id = hash("sha256", $timecad);
        $total = 0;

    if ($nulo != true and $blanco!= true){
        foreach ($array_valores as $v) {
            $array_valor = explode(",", $v);

            $position = $array_valor[0];
            $candi = fn_filtro($con, $array_valor[1]);
            if ($position == "") {
                continue;
            }
            $total = $total + 1;
            if ($position > 0 && $position <= $numero_opciones) {

                /////introduce el tipo de recuento

                if ($recuento == 0) {
                    $vot = $numero_opciones + 1 - $position; /////introduce valor del voto recuento borda
                } else if ($recuento == 1) {
                    $vot = 1 / $position; // mete el recuento DOWDALL
                    $vot =  round($vot, 4);
                }

                $datos_votado .= $candi . "-" . $vot . "#";
                $datos_del_voto = trim($datos_votado, '#'); ///quitamos la ultima coma de la cadena para meterlo en la bbdd
            }
        }
      }
        if ($nulo) {
            $nulo_blanco = 1;
            $datos_votado = "NULO";
            $datos_del_voto = "NULO-1";
            $vot=1;
        }

        if ($total == 0) {
            // ALTA VOT BLANC
            $nulo_blanco = 2;
            $datos_votado = "BLANCO";
            $datos_del_voto = "BLANCO-1";
            $vot=1;
        }

        $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
        $cad = "";
        for ($i = 0; $i < 4; $i++) {
            $cad .= substr($str, rand(0, 62), 1);
        }
        $time = microtime(true);
        $timecad2 = $cad . $time;
        $res_id = hash("sha256", $timecad2);



        $insql = "insert into $tbn10 (ID,voto,id_candidato,id_provincia,id_votacion,codigo_val,vote_id,otros) values (\"$res_id\",\"$vot\",\"$datos_del_voto\"," . $_SESSION['localidad'] . ",\"$idvot\",\"$codi\",\"$vote_id\",\"$nulo_blanco\")";
        $mens = _("ERROR en el añadido  voto");
        $result = db_query($con, $insql, $mens);

        if (!$result) {
            $okVot = false;
            //break;
        }

        if (!$okVot) {
          //  mysqli_query($con, '');
            $process_result = "ERROR";
            $msg_result = _("ERROR al introducir su voto en la base de datos.") . "<br/>" . _("Intentelo en unos minutos o contacte con el administrador del sitio");
        } else {

            // ALTA del usuario en la lista de los que han votado para que no pueda volver a votar
            $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
            $cad = "";
            for ($i = 0; $i < 6; $i++) {
                $cad .= substr($str, rand(0, 62), 1);
            }
            $time = microtime(true);
            $timecad2 = $time . $cad;
            $user_id = hash("sha256", $timecad2);

            if ($seguridad==3 or $seguridad==4){
              $fecha_env = date("Y-m-d 00:00:01"); // si la votación lleva envios de correos a los interventores, quitamos la hora para que no se pueda vincular la hora del voto
            }else{
              $fecha_env = date("Y-m-d H:i:s");
            }
            $insql = "insert into $tbn2 (ID,id_provincia,id_votacion,id_votante,fecha,tipo_votante,ip,forma_votacion) values (\"$user_id\"," . $_SESSION['localidad'] . ",\"$idvot\"," . $_SESSION['ID'] . ",\" $fecha_env\",\" $tipo_votante\",\" $ip\",\" $forma_votacion\")";
            $mens = "<br/>" . _("¡¡¡ATENCION!!!!, el voto ha sido registrado , pero el usuario no ha sido bloqueado") . " <br/>" . _("el ID de usuario es:") . $_SESSION['ID'];
            $result = db_query($con, $insql, $mens);
            if (!$result) {
                //mysqli_query($con, 'ROLLBACK');
                $process_result = "ERROR";
                $msg_result = _("Error en la marca de su voto en la base de datos") . "<br/>" . _("Contacte con el administrador del sitio");
            } else {
                //mysqli_query($con, 'COMMIT');
                ////metemos el voto en un txt y lo registramos como medida de seguridad
                $Pollname = md5($idvot);
                $file = $FilePath . $Pollname . "_ballots.txt";
                $fh = fopen($file, 'a');
                fwrite($fh, $datos_votado . PHP_EOL) or die("Could not write file!");
                fclose($fh);  //  Cerramos el fichero
                //añadimo un voto mas en el otro fichero
                $file2 = $FilePath . $Pollname . "_tally.txt";
                $fh2 = fopen($file2, 'r+');
                $bufer = fgets($fh2, 100);
                $partes = explode("|", $bufer);
                $partes1 = $partes[1] + 1;
                $partes = $partes[0] . "|" . $partes1;
                fseek($fh2, 0);
                fwrite($fh2, $partes) or die("Could not write file!");
                fclose($fh2);  //  Cerramos el fichero
                ///// metemos una copia encriptada del voto
                //// primero sacamos los datos de la tabla temporal para hacer una cadena quemeteremos tambien encriptada
                $sql = "SELECT 	ID, datos  FROM $tbn20 where id_votacion=\"$Pollname\" ORDER BY ID DESC";
                $mens = "Error en la consulta a la tabla temporal";
                $result = db_query($con, $sql, $mens);
                $cadena1="";
                if ($row = mysqli_fetch_array($result)) {
                    mysqli_field_seek($result, 0);
                    do {
                        $cadena1 .= $row[1] . "_";  ////generamos la caena que vamos a meter en la tabla de votos de seguridad
                        $id_borrado = $row[0];
                    } while ($row = mysqli_fetch_array($result));
                }


                $cadena1 = trim($cadena1, '_'); // quitamos de la cadena el ultimo _

                $borrado = "DELETE FROM $tbn20 WHERE ID=" . $id_borrado . " ";
                $mens = " error en el borrado de " . $id_borrado;
                $result = db_query($con, $borrado, $mens);


                //metemos los datos codificados en la bbdd
                $ident = md5($timecad2);

                $cadena_temp = $res_id . "+" . $datos_del_voto; //esta variable sirve para esta querry y para la querry de la tabla temporal siguiente
                /////// miramos si hay que encriptar la cadena de voto
                /* if ($encripta=="SHA"){
                  $cadena_temp = hash('sha256', $cadena_temp);
                  }else */
                if ($encripta == "si") {
                    include('../private/modulos/phpseclib/Crypt/RSA.php');
                    /* require_once "Crypt/AES.php"; */

                    //Function for encrypting with RSA
                    function rsa_encrypt($string, $public_key) {
                        //Create an instance of the RSA cypher and load the key into it
                        $cipher = new Crypt_RSA();
                        $cipher->loadKey($public_key);
                        //Set the encryption mode
                        $cipher->setEncryptionMode(CRYPT_RSA_ENCRYPTION_PKCS1);
                        //Return the encrypted version
                        return base64_encode($cipher->encrypt($string));
                    }

                    // hacemos una busqueda y cargamos la clave publica
                    $result = mysqli_query($con, "SELECT clave_publica FROM $tbn21  WHERE id_votacion='$idvot' ORDER by orden");
                    if ($row = mysqli_fetch_array($result)) {
                        mysqli_field_seek($result, 0);
                        do {
                            $clave_publica = $row[0];

                            $cadena1 = rsa_encrypt($cadena1, $clave_publica);
                        } while ($row = mysqli_fetch_array($result));
                    }
                }

                $shadatovoto = hash('sha256', $cadena_temp);
                $insql1 = "insert into $tbn19 (ID,voto,cadena,id_votacion) values (\"$ident\",\"$shadatovoto\",\"$cadena1\",\"$Pollname\")";
                $mens1 = "ERROR en el añadido voto encriptado ";
                $result = db_query($con, $insql1, $mens1);

                //metemos el dato en la tabla temporal
                $cadena_temp = $res_id . "+" . $datos_del_voto;
                $insql2 = "insert into $tbn20 (datos,id_votacion) values (\"$cadena_temp\",\"$Pollname\")";
                $mens2 = "ERROR en el añadido  del voto tabla temporal";
                $result = db_query($con, $insql2, $mens2);



                if ($seguridad == 3 or $seguridad == 4) { //metemos los envios de correos de interventores

                    require ("../private/basicos_php/envio_interventores.php");
                }
                $process_result = 'OK';

                $msg_result = "<div class='alert alert-success'>
		<p>" . _("El voto se ha guardado en la base de datos de forma correcta.") . "<br />" . _("Gracias por participar en esta votaci&oacute;n.") . "</p>
		<p>" . _("Guarde este codigo de su voto si quiere comprobar que es correctamente contabilizado") . "</p>
		</div>
		<div class='alert alert-success'>
		<p> " . $res_id . "  </p>
		</div>";
            }
        }
    }
}
mysqli_close($con);
echo $process_result . "#" . $msg_result;
?>

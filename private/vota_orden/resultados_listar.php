<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que lista todos los votos y los compara con la tabla de los votos encriptados
*/
if(!isset($cargaA)){
  $cargaA =false;
  exit;
}
if($cargaA!="OK"){
  exit;
}else{

  if ($variables['idvot'] != "") {
      $idvot=$variables['idvot'];
      //$idvot = fn_filtro_numerico($con, $_GET['idvot']); //variable que nos llega con la votacion
      //$activa = "si"; //Si la votacion esta activa o no
      //$sql_vot = "SELECT nombre_votacion,resumen FROM $tbn1  WHERE ID='$idvot' and activa='$activa' ";
      $sql_vot = "SELECT nombre_votacion,resumen FROM $tbn1  WHERE ID='$idvot'";

      if($res_votacion = mysqli_query($con, $sql_vot)){
      $row_vot = mysqli_fetch_row($res_votacion);


      $nombre_votacion = $row_vot[0];
      $resumen = $row_vot[1];

      mysqli_free_result($res_votacion);
    }
  }
//  include_once('../private/inc_web/seguri.php');
//$nivel_acceso = 10;
//include('../private/inc_web/nivel_acceso.php');


ini_set('memory_limit', '3064M'); //cantidad de memoria
ini_set('max_execution_time', 900); //900 seconds = 15 minutes
ob_start();
?>

<div class="card-header-votaciones "> <h1 class="card-title"><?php echo "$nombre_votacion"; ?></h1> </div>


<div class="card-body">

                    <!--Comiezo-->
                    <?php echo "$resumen"; ?>




            <h3><?= _("Candidatos u opciones de esta votación") ?></h3>

            <table class="table table-striped">
                <tr>

                    <th width="10%"><?= _("Identificador") ?></th>
                    <th width="80%"><?= _("Nombre") ?></th>
                    <th width="10%"><?= _("Sexo") ?></th>
                </tr>
                <?php
// sacamos los datos del array

                $sql2 = "SELECT ID, nombre_usuario,sexo  FROM $tbn7 WHERE id_votacion=" . $idvot . " ";
                $result2 = mysqli_query($con, $sql2);
                if ($row2 = mysqli_fetch_array($result2)) {
                    mysqli_field_seek($result2, 0);

                    do {
                        ?>
                        <tr>
                            <td><?php echo $row2[0]; ?></td>
                            <td><?php echo $row2[1]; ?></td>
                            <td><?php echo $row2[2]; ?></td>


                        </tr>

                        <?php
                    } while ($row2 = mysqli_fetch_array($result2));
                }
                ?>

            </table>



            <h3><?= _("Lista de todos los votos de esta votación") ?></h3>
            <p class="text-info"><?= _("El primer dato es el identificador unico, si has guardado el tuyo puedes comprobar que esta en la lista") ?></p>
            <p class="text-info"><?= _("El segundo dato corresponde al identificador del candidato u opcion y la puntuacion asignada dependiendo del orden que se haya marcado") ?>
            ( <?= _("Los distintos candidatos u opciones estan separados por") ?>  # )</p>
            <p class="text-info"> <?= _("El ultimo parametro corresponde al estado de la comprobacion del voto con la copia de seguridad encritada") ?></p>
            <?php
            $Pollname = md5($idvot); //encriptamos el id de la votacion
            /* nuevo */
            $timeStart = microtime(true);
            $i = 1;
            $sql = "SELECT ID, id_candidato FROM $tbn10 WHERE id_votacion = '$idvot' and especial=0  ";
            $result = mysqli_query($con, $sql);
            if ($row = mysqli_fetch_array($result)) {
                mysqli_field_seek($result, 0);
                echo "<table  class=\"table table-hover\">
				  <tr>
					<th width=\"2%\" scope=\"row\">#</th>
					<td width=\"55%\">HASH</td>
					<td width=\"35%\">" . _("VOTOS") . "</td>
					<td width=\"8%\">&nbsp;</td>
				  </tr>";
                do {

                    $cadena_temp = $row[0] . "+" . $row[1];
                    $shadatovoto = hash('sha256', $cadena_temp);

                    $conta = "SELECT id  FROM $tbn19 WHERE id_votacion = \"" . $Pollname . "\"  and voto = \"" . $shadatovoto . "\" ";
                    $result_cont = mysqli_query($con, $conta);
                    $quants = mysqli_num_rows($result_cont);
                    if ($quants == 1) {
                        $resulta = '<span class="text-success"> OK <i class="fa fa-check" aria-hidden="true"></i></span>';
                    } else {
                        $resulta = '<span class="text-danger">ERROR <i class="fa fa-warning"></i></span>';
                    }

                    echo "<tr><th scope=\"row\">" . $i++ . " </td><td> " . $row[0] . " </td><td> " . $row[1] . " </td><td> " . $resulta . "</td></tr>";
                } while ($row = mysqli_fetch_array($result));
                echo" </table>";
            }
            ?>



            <?php
            $i = 1;
            $sql = "SELECT ID, id_candidato,incluido FROM $tbn10 WHERE id_votacion = '$idvot' and especial=1  ";
            $result = mysqli_query($con, $sql);
            if ($row = mysqli_fetch_array($result)) {
                ?>
                <h3><?= _("Votos presenciales incluidos por los interventores") ?></h3>
                <p><?= _("El ultimo parametro corresponde a los identificadores de los interventores que han incluido el voto") ?></p>
                <?php
                mysqli_field_seek($result, 0);
                echo "<table  class=\"table table-hover\">
				  <tr>
					<th width=\"3%\" scope=\"row\">#</th>
					<td width=\"45%\">HASH</td>
					<td width=\"32%\">" . _("VOTOS") . "</td>
					<td width=\"25%\">" . _("INTERVENTORES") . "</td>
				  </tr>";
                do {

                    echo "<tr><th scope=\"row\">" . $i++ . " </td><td>  " . $row[0] . " </td><td>  " . $row[1] . " </td><td>  " . $row[2] . "</td></tr>";
                } while ($row = mysqli_fetch_array($result));
                echo" </table>";
            }
            ?>

</div>

            <!--Final-->

<?php
$contenido = ob_get_contents();
ob_end_flush();
$idvot_encriptada = md5($idvot);
$archivo = $FileRec . $idvot_encriptada . "_list.php";
$crear = fopen($archivo, "w");
$grabar = fwrite($crear, $contenido);
fclose($crear);
?>
<?php
$timeEnd = microtime(true);
$timeElapsed = $timeEnd - $timeStart;
echo "<br/>";

printf("Memory used: %s kB\n", memory_get_peak_usage() / 1024);
printf("Total time: %s s\n", $timeElapsed);
/* fin nuevo */
?>
<?php } ?>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo donde se selecciona y ordena el voto y se emite mediante primarias_func.inc.php
*/
if(!isset($carga)){
  $cargaA =false;
  exit;
}
if($carga!="OK"){
  exit;
}else{
  include('../private/inc_web/seguri.php');
?>
<link href="temas/<?php echo "$tema_web"; ?>/css/ordenStyle.css" rel="stylesheet">




                    <!---->
                    <div class="card-header-votaciones "> <h1 class="card-title"><?php echo "$nombre_votacion"; ?></h1> </div>


<div class="card-body">

  <?php if($resumen!=""){?>
                  <div class="card">
                    <div class="card-body">
                      <?php echo "$resumen";?>
                    </div>
                  </div>
                      <?php
                    }
                    ?>

                    <form id="form2" name="form2" action="<?php $_SERVER['PHP_SELF'] ?>" method="post" >

                        <p>
                            <!-- Campo oculto que almacenará los valores votados y su ordenación -->
                            <input type="hidden" id="hfIDS" name="hfIDS" value="" />
                        </p>




                        <div class="well">

                            <!-- Contenedor general -->
                            <div class="contenedor">
                                <!-- tabla1 , lado izquierdo, candidatos-->

                                <div id="respuestaOK" class="well" style="display:none;">
                                    <div></div>
                                    <button class="btn btn-lg btn-block btn-primary" id="backlist"><?= _("Volver a la lista de votaciones") ?></button>
                                </div>

                                <div id="votacion">

                                    <?php require('../private/basicos_php/control_voto.php'); // sistema para incluir internventores o clave voto seguro  ?>

                                    <div id="voto" class="card">
                                      <div class="card-body">
                                      <?= _("El numero de candidat@s o propuestas que se eligen es de") ?>: <?php echo "$numero_opciones"; ?>
                                    </div>
                                  </div>
                                    <div id="selection">
                                        <?php
                                        $sql = "SELECT * FROM $tbn7 WHERE id_votacion = '$idvot'  and sexo = 'H'  ORDER BY rand(" . time() . " * " . time() . ")  ";
                                        $result = mysqli_query($con, $sql);
                                        $numero_fem = mysqli_num_rows($result); // obtenemos el número de filas
                                        if ($row = mysqli_fetch_array($result)) {
                                            mysqli_field_seek($result, 0);
                                            ?>

                                            <div class="row">
                                                <div id="table-men-c" class="col-md-6">
                                                    <h3><?= _('Lista de candidatos'); ?></h3>
                                                    <ul class="candidates men unselected">
                                                        <?php
                                                        do {
                                                            ?>
                                                            <li data-id="<?php echo "$row[0]" ?>" data-type="men">
                                                              <span class="profile">  <a href="javascript:void(0);" data-href="aux_vota.php?c=<?php echo encrypt_url('votacion/perfil/idgr='.$row[0],$clave_encriptacion) ?>" class="opennormalModal btn btn-info btn-sm" title="<?php echo $row['nombre_usuario']; ?>" >  +<?= _("info") ?></a> </span>
                                                                <span class="name"><?php if ($row['imagen_pequena'] == "") { ?><?php } else { ?>
                                                                  <img src="<?php echo $upload_cat; ?>/<?php echo $row['imagen_pequena']; ?>" alt="<?php echo $row['nombre_usuario']; ?> " width="80" height="80" class="img-circle img-profile" /> <?php } ?><?php echo "$row[3]" ?></span>

                                                                <button class="add-candidate btn btn-primary"><?= _("Posición") ?><span class="position">1</span> <i class="fa fa-arrow-circle-right"></i></button>
                                                                <div class="clearfix"></div>
                                                            </li>
                                                            <?php
                                                        } while ($row = mysqli_fetch_array($result));
                                                        ?>
                                                    </ul>
                                                </div>

                                                <div id="table-men-v" class="col-md-6">
                                                    <h3><?= _("Selección por orden de preferencia de candidatos") ?></h3>
                                                    <ul class="candidates men selected">
                                                        <?php for ($i = 1; $i <= $numero_opciones; $i++) { ?>
                                                            <li data-position="<?php echo "$i"; ?>"  data-id="0" class="empty">
                                                                <span class="position"><?php echo "$i"; ?></span>
                                                                <span class="name"></span>
                                                                <div class="pull-right">
                                                                    <button class="del-candidate btn btn-danger" title="<?= _('Elimina'); ?>"><i class="fa fa-minus-circle"></i></button>
                                                                    <?php if ($i > 1) { ?>
                                                                        <button class="up-candidate btn btn-primary" title="<?= _('Sube'); ?>"><i class="fa fa-arrow-circle-up"></i></button>
                                                                    <?php } ?>
                                                                    <?php if ($i < $numero_opciones) { ?>
                                                                        <button class="down-candidate btn btn-primary" title="<?= _('Baja'); ?>"><i class="fa fa-arrow-circle-down"></i></button>
                                                                    <?php } ?>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </li>
                                                        <?php } ?>
                                                    </ul>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        ?>

                                        <div class="clearfix"></div>


                                        <?php
                                        $sql = "SELECT * FROM $tbn7 WHERE id_votacion = '$idvot' and sexo = 'M'  ORDER BY rand(" . time() . " * " . time() . ")  ";
                                        $result = mysqli_query($con, $sql);
                                        $numero_mas = mysqli_num_rows($result); // obtenemos el número de filas
                                        if ($row = mysqli_fetch_array($result)) {
                                            mysqli_field_seek($result, 0);
                                            ?>
                                            <div class="row">
                                                <div id="table-women-c" class="col-md-6">
                                                    <h3><?= _("Lista de candidatas") ?></h3>
                                                    <ul class="candidates women unselected">
                                                        <?php
                                                        do {
                                                            ?>
                                                            <li data-id="<?php echo "$row[0]" ?>"  data-type="women">
                                                                <span class="profile"><a href="javascript:void(0);" data-href="aux_vota.php?c=<?php echo encrypt_url('votacion/perfil/idgr='.$row[0],$clave_encriptacion) ?>" class="opennormalModal btn btn-info btn-sm" title="<?php echo $row['nombre_usuario']; ?>" >+ <?= _("info") ?></a> </span>
                                                                <span class="name"><?php if ($row['imagen_pequena'] == "") { ?><?php } else { ?>
                                                                  <img src="<?php echo $upload_cat; ?>/<?php echo $row['imagen_pequena']; ?>" alt="<?php echo $row['nombre_usuario']; ?> " width="80" height="80" class="img-circle img-profile" /> <?php } ?>
                                                                  <?php echo "$row[3]" ?></span>

                                                                <button class="add-candidate btn btn-primary">Posición <span class="position">1</span> <i class="fa fa-arrow-circle-right"></i></button>
                                                                <div class="clearfix"></div>
                                                            </li>
                                                            <?php
                                                        } while ($row = mysqli_fetch_array($result));
                                                        ?>
                                                    </ul>
                                                </div>

                                                <div id="table-women-v" class="col-md-6">
                                                    <h3><?= _('Selección por orden de preferencia de candidatas'); ?></h3>
                                                    <ul class="candidates women selected">
                                                        <?php for ($i = 1; $i <= $numero_opciones; $i++) { ?>
                                                            <li data-position="<?php echo "$i"; ?>" data-id="0" class="empty">
                                                                <span class="position"><?php echo "$i"; ?></span>
                                                                <span class="name"></span>
                                                                <div class="pull-right">
                                                                    <button class="del-candidate btn btn-danger" title="<?= _('Elimina'); ?>"><i class="fa fa-minus-circle"></i></button>
                                                                    <?php if ($i > 1) { ?>
                                                                        <button class="up-candidate btn btn-primary" title="<?= _('Sube'); ?>"><i class="fa fa-arrow-circle-up"></i></button>
                                                                    <?php } ?>
                                                                    <?php if ($i < $numero_opciones) { ?>
                                                                        <button class="down-candidate btn btn-primary" title="<?= _('Baja'); ?>"><i class="fa fa-arrow-circle-down"></i></button>
                                                                    <?php } ?>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </li>
                                                        <?php } ?>
                                                    </ul>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                        <!---->
                                        <div class="clearfix"></div>
                                        <?php if ($numero_mas == 0 or $numero_fem == 0) { ?>

                                            <?php
                                            $sql = "SELECT * FROM $tbn7 WHERE id_votacion = '$idvot' and sexo = 'O'  ORDER BY rand(" . time() . " * " . time() . ")  ";
                                            $result = mysqli_query($con, $sql);
                                            $numero_mas = mysqli_num_rows($result); // obtenemos el número de filas
                                            if ($row = mysqli_fetch_array($result)) {
                                                mysqli_field_seek($result, 0);
                                                ?>
                                                <div class="row">
                                                    <div id="table-neutro-c" class="col-md-6">
                                                        <h3><?= _("Lista de OPCIONES") ?></h3>
                                                        <ul class="candidates neutro unselected">
                                                            <?php
                                                            do {
                                                                ?>
                                                                <li data-id="<?php echo "$row[0]" ?>"  data-type="neutro">
                                                                  <span class="profile"><a href="javascript:void(0);" data-href="aux_vota.php?c=<?php echo encrypt_url('votacion/perfil/idgr='.$row[0],$clave_encriptacion) ?>" class="opennormalModal btn btn-info btn-sm" title="<?php echo $row['nombre_usuario']; ?>" >+<?= _("info") ?></a> </span>
                                                                    <span class="name"><?php if ($row['imagen_pequena'] == "") { ?><?php } else { ?>
                                                                      <img src="<?php echo $upload_cat; ?>/<?php echo $row['imagen_pequena']; ?>" alt="<?php echo $row['nombre_usuario']; ?> " width="80" height="80" class="img-circle img-profile" /> <?php } ?><?php echo "$row[3]" ?></span>
                                                                    <button class="add-candidate btn btn-primary">Posición <span class="position">1</span> <i class="fa fa-arrow-circle-right"></i></button>
                                                                    <div class="clearfix"></div>
                                                                </li>
                                                                <?php
                                                            } while ($row = mysqli_fetch_array($result));
                                                            ?>
                                                        </ul>
                                                    </div>

                                                    <div id="table-neutro-v" class="col-md-6">
                                                        <h3><?= _('Selección por orden de preferencia de opciones'); ?></h3>
                                                        <ul class="candidates women selected">
                                                            <?php for ($i = 1; $i <= $numero_opciones; $i++) { ?>
                                                                <li data-position="<?php echo "$i"; ?>" data-id="0" class="empty">
                                                                    <span class="position"><?php echo "$i"; ?></span>
                                                                    <span class="name"></span>
                                                                    <div class="pull-right">
                                                                        <button class="del-candidate btn btn-danger" title="<?= _('Elimina'); ?>"><i class="fa fa-minus-circle"></i></button>
                                                                        <?php if ($i > 1) { ?>
                                                                            <button class="up-candidate btn btn-primary" title="<?= _('Sube'); ?>"><i class="fa fa-arrow-circle-up"></i></button>
                                                                        <?php } ?>
                                                                        <?php if ($i < $numero_opciones) { ?>
                                                                            <button class="down-candidate btn btn-primary" title="<?= _('Baja'); ?>"><i class="fa fa-arrow-circle-down"></i></button>
                                                                        <?php } ?>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </li>
                                                            <?php } ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            ?>

                                        <?php } ?>
                                        <!---->



                                        <?php if ($numero_mas == 0 or $numero_fem == 0) { ?>
                                            <input name="mixto" type="hidden" id="mixto" value="NOesMixto" />
                                        <?php } else { ?>
                                            <input name="mixto" type="hidden" id="mixto" value="SIesMixto" />
                                        <?php } ?>
                                        <input name="id_provincia" type="hidden" id="id_provincia" value="<?php echo "$id_provincia" ?>" />
                                        <input name="id_ccaa" type="hidden" id="id_ccaa" value="<?php echo "$id_ccaa" ?>" />
                                        <input name="id_subzona" type="hidden" id="id_subzona" value="<?php echo "$id_subzona" ?>" />
                                        <input name="id_grupo_trabajo" type="hidden" id="id_grupo_trabajo" value="<?php echo "$id_grupo_trabajo" ?>" />
                                        <input name="demarcacion" type="hidden" id="demarcacion" value="<?php echo "$demarcacion" ?>" />
                                        <input name="recuento" type="hidden" id="recuento" value="<?php echo "$recuento" ?>" />
                                        <input name="id_municipio" type="hidden" id="id_municipio" value="<?php echo "$id_municipio" ?>" />
                                        <input name="encripta" type="hidden" id="encripta" value="<?php echo "$encripta" ?>" />
                                        <!-- -->
                                        <div class="clear"></div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                              <button class="btn  btn-block btn-secondary" id="cancelvote"><?= _("Cancela (votar más tarde)") ?></button>
                                            </div>
                                            <div class="col-sm-6">
                                              <button class="btn  btn-block btn-primary" id="vote"><?= _("VOTA (Previsualizar)") ?></button>
                                            </div>
                                          </div>
                                            <div class="row">
                                                <div class="col-sm-12 texto">
                                                  <?php if($texto!=""){?>
                                                    <div class="card">
                                                      <div class="card-body">
                                                <?php   echo $texto; ?>
                              </div>
                              </div>
                                              <?php   }?>
                                                </div>
                                            </div>

                                    </div>


                                    <div id="confirm" style="display:none">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h2><?= _("Confirme la votación") ?></h2>
                                            </div>
                                            <div id="alert-error" class="col-sm-12" style="display:none">
                                                <div class="alert alert-danger">
                                                </div>
                                            </div>
                                            <div id="confirm-men" class="col-sm-6">
                                                <h3><?= _("Hombres") ?></h3>
                                                <ul class="list-group">
                                                </ul>
                                            </div>
                                            <div id="confirm-women" class="col-sm-6">
                                                <h3><?= _("Mujeres") ?></h3>
                                                <ul class="list-group">
                                                </ul>
                                            </div>
                                            <div id="confirm-neutro" class="col-sm-12">
                                                <h3><?= _("Su seleccion") ?></h3>
                                                <ul class="list-group">
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div id="alert-vot-nul" class="col-sm-12" style="display:none">
                                                <div class="alert alert-info">
                                                    <?= _("Tu voto no es paritario, por lo cual no es valido. Has indicado un") ?> <span class="men"></span>% <?= _("de hombres y un") ?> <span class="women"></span>% <?= _("de mujeres") ?><br />
                                                    <?= _("Marca la casilla para confirmarlo, o usa el boton de") ?>' <b> <?= _("Volver (modifica el voto)") ?></b>'.
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="confirm-nul" name="confirm-nul">  <?= _("Confirmar que quiere hacer un") ?> <b> <?= _("voto nulo") ?> </b>
                                                    </label>
                                                </div>
                                            </div>
                                            <div id="alert-vot-blanc" class="col-sm-12" style="display:none">
                                                <div class="alert alert-info">
                                                    <?= _("Tu voto es en blanco.") ?><br/>
                                                    <?= _("Marca la casilla para confirmar, o usa el boton de") ?> '<b> <?= _("Volver (modifica el voto)") ?></b>'.
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="confirm-blanc" name="confirm-blanc"> <?= _("Confirmar que quiere") ?>  <b><?= _("votar en blanco") ?></b>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                              <button class="btn btn-lg btn-block btn-secondary" id="backtovote"><?= _("VOLVER (modificar el voto)") ?></button>
                                            </div>
                                            <div class="col-sm-6">
                                              <button class="btn btn-lg btn-block btn-primary" id="confirmvote"><?= _("CONFIRMAR VOTO") ?></button>

                                            </div>
                                        </div>
                                    </div>


                                </div>

                            </div>


                        </div>
                      </form>
                        <!---->

        <script type="text/javascript">
              var number = 'aux_vota.php?c=<?php echo encrypt_url('vota_orden/func.inc/idvot='.$idvot,$clave_encriptacion) ?>';
            </script>
        <?php if ($numero_mas == 0 or $numero_fem == 0) { ?>
            <script src="assets/js/vota_orden/vot_orden_2.js" type="text/javascript"  ></script>
        <?php } else {
            ?>
            <script src="assets/js/vota_orden/vot_orden.js" type="text/javascript"  ></script>
        <?php } ?>

</div>

    <?php } ?>

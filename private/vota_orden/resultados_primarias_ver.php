<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
/**
* Archivo que realiza la consulta a la base de datos y compara la contraseña que ha metido el usuario al votar, y enseña el voto realizado para comprobar que está correctamente contabilizado
*/
if(!isset($carga)){
  $carga =false;
  exit;
}
if($carga!="OK"){
  exit;
}else{
  include('../private/inc_web/seguri.php');
?>


                    <!---->

                    <?php
                    $clave_seg = fn_filtro($con, $_POST['clave_seg']);
                    $codigo_val = hash("sha512", $clave_seg);
                    ?>

                    <div class="card-header-votaciones "> <h1 class="card-title"> <?php echo "$nombre_votacion"; ?>    </h1> </div>


                    <div class="card-body">



                    <div>

                      <h2><?= _("Esto es lo que hay registrado como su voto en la votación") ?>    </h2>
                        <?php
                        $sql = "SELECT ID, id_candidato, otros FROM $tbn10 WHERE  id_votacion = '$idvot' and codigo_val LIKE '$codigo_val'  ";
                        $result = mysqli_query($con, $sql);
                        $total = mysqli_num_rows($result);
                        if ($total == 1) {
                            $row = mysqli_fetch_row($result);
                            if ($row[2] == 2) {  // Votos en blanco
                                echo " <ul class=\"candidates_res\"><li class=\"ne\">" . _("Su voto es en blanco") . " </li></ul>";
                            } elseif ($row[2] == 1) {  // Votos en nulos
                                echo " <ul class=\"candidates_res\"><li class=\"ne\">" . _("Su voto es Nulo") . "</li></ul>";
                            } elseif ($row[2] == 0) {
                                $arr = explode('#', $row[1]);
                                foreach ($arr as $val) {
                                    $arr2 = explode('-', $val);
                                    foreach ($arr2 as $val2) {

                                    }


                                    $sql2 = "SELECT  nombre_usuario,sexo,imagen_pequena FROM $tbn7 WHERE ID = '" . $arr2[0] . "'  ";
                                    $result2 = mysqli_query($con, $sql2);
                                    if ($row2 = mysqli_fetch_array($result2)) {
                                        mysqli_field_seek($result2, 0);
                                        ?>

                                        <div class="row">
                                            <div id="table-men-c" class="col-md-12">
                                                <ul class="candidates men ">
                                                    <?php
                                                    do {
                                                        ?>
                                                        <?php
                                                        if ($row2[1] == "H") {
                                                            $type = _("Masculino") . " | ";
                                                        } else if ($row2[1] == "M") {
                                                            $type = _("Femenino") . " | ";
                                                        } else {
                                                            $type = "";
                                                        }
                                                        ?>
                                                        <li data-id="<?php echo "$row2[0]" ?>">
                                                            <?php echo $type; ?> <?= _("Puntuación asignada") ?> : <?php echo $arr2[1]; ?> | <span class="name"><?php if ($row2['imagen_pequena'] == "") { ?><?php } else { ?><img src="<?php echo $upload_cat.'/'. $row2['imagen_pequena']; ?>" alt="<?php echo $row2['nombre_usuario']; ?>" width="60" height="60"  /> <?php } ?><?php echo "$row2[0]" ?> </span>

                                                        </li>

                                                        <?php
                                                    } while ($row2 = mysqli_fetch_array($result2));
                                                    ?>
                                                </ul>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                            } else {?>
                      <div class="alert alert-danger">
                        <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                        <i class="fa fa-times simple-remove" aria-hidden="true"></i>
                        </button>
                        <span>
                          <b> Info - </b> <?=_("No hay resultados asociados a esa clave. Quizas la clave sea erronea");?></span>
                        </div>
                        <?php
                            }
                        } else {?>
                      <div class="alert alert-danger">
                        <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                        <i class="fa fa-times simple-remove" aria-hidden="true"></i>
                        </button>
                        <span>
                          <b> Info - </b> <?=_("No hay resultados asociados a esa clave. Quizas la clave sea erronea");?></span>
                        </div>
                        <?php
                        }
                        ?>

                    </div>

</div>
                    <!---->

<?php } ?>

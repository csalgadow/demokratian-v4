<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
//error_reporting(E_ALL);
//ini_set('display_errors', '1');
$timezone=false;
$b_debugmode_install=1;
//include_once("../private/basicos_php/basico.php");
//include_once("../private/basicos_php/modifika_config.php");

include_once('../private/basicos_php/errores.php');

$dbname = fn_filtro_nodb($_POST['dbname']);
$username = fn_filtro_nodb($_POST['username']);
$pass = fn_filtro_nodb($_POST['pass']);
$dbhost = fn_filtro_nodb($_POST['dbhost']);
$prefijo = fn_filtro_nodb($_POST['prefijo']);
$mensaje = "";
$mensaje1 = "";
$mensaje2 = "";
$tipo_engine = "";
$tipo_engine_ok = "";
$datos_engine = "";


#Conectando con el servidor de bases de datos, si no se puede conectar, generamos error
$conexion = mysqli_connect($dbhost, $username, $pass);

if (mysqli_connect_errno()) {
  $estado_error = "conError";
    echo "ERROR#<div class=\"alert alert-danger\"> Lo sentimos, no se ha podido conectar con la base de datos MySQL <br/> <i>Error de conexión número:</i> " . mysqli_connect_errno() . " <i>equivalente a:</i> " . mysqli_connect_error() . "</div> ";
    exit;
  }



#Conectando con la base de datos, si no se puede conectar, generamos error
$dbconnect = mysqli_select_db($conexion, "$dbname");
if ($dbconnect == 0) {
  $estado_error = "conError";
  echo "ERROR# <div class=\"alert alert-danger\">Se logró conectar con el servidor de bases de datos
	<br>pero no se ha podido conectar con la base datos: $dbname </div>";
  exit;
  }


  if (!mysqli_set_charset($conexion, "utf8")) { //establecemos como utf8 los carateres de la base de datos
    $estado_error = "conError";
    echo 'ERROR# <div class="alert alert-danger">Error cargando el conjunto de caracteres utf8:'  . mysqli_error($conexion) . '</div>';
    exit;
  }

//comprobamos la tabla y el prefijo que convertimosen minusculas

  $prefijo = strtolower($prefijo); //lo convertimos en minusculas

  function comprobar_prefijo($nombre_usuario) {

      //compruebo que los caracteres sean los permitidos
      $permitidos = "abcdefghijklmnopqrstuvwxyz0123456789-_";
      for ($i = 0; $i < strlen($nombre_usuario); $i++) {
          if (strpos($permitidos, substr($nombre_usuario, $i, 1)) === false) {
              $error = "error";
              return $error;
          }
      }
      //echo $nombre_usuario . " es válido<br>";
      return $nombre_usuario;
  }

  $prefijo = comprobar_prefijo($prefijo);
  if ($prefijo == "error") {
    $estado_error = "conError";
      echo "ERROR#<div class=\"alert alert-danger\">Hay un error en la extension, solo estan permitidos estos caracteres <strong> abcdefghijklmnopqrstuvwxyz0123456789-_</strong></div>";
      exit;
  }


// si ya existe la tabla en la bbdd damos error
    $tabla = $prefijo . "votantes";

    $SQL= "  SELECT *  FROM information_schema.tables WHERE table_schema = '".$dbname."'   AND table_name ='". $tabla."'" ;
    $idresult = mysqli_query($conexion, $SQL);
    //$idresult = mysqli_query($conexion, " SHOW TABLES FROM 'votaciones_demokratian' LIKE 'dk_votantes'");
    //  $idresult = mysqli_query($conexion, "SHOW TABLES LIKE dk_votantes");

    $row_cnt = mysqli_num_rows($idresult);

  if ($row_cnt !=0) { // si ya existe la tabla en la bbdd damos error
    $estado_error = "conError";
      echo 'ERROR# <div class="alert alert-danger"> Se ha podido conectar con la BBDD pero hay un error porque parece que hay una instalacion con el prefijo '.$prefijo.' <br> Cambie el prefijo si quiere continuar con la instalación</div>';
      exit;
    }



  $file = "../private/config/config.inc.php"; //archivo de configuración
  if (file_exists($file)) {
      $estado_error = "conError";
      echo 'ERROR# <div class="alert alert-danger">¡¡ Ya existe un archivo de configuración!!, no se puede continuar con el proceso, proceda manualmente o eliminelo</div>';
      exit;
    }

  // realizamos las comprobaciones para ver si el servidor de bases de datos es compatible con nuestra base de datos. existe la posibilidad de generar distintos tipos de tablas seun las bases de datos aunque ahora no existe, ver version de la rama 3

  $servidor = mysqli_get_server_info($conexion);

  //Miramos si el motor InnoDB  esta instlado en el servidor de bases de datos y resto de caracteristicas del servidor
  $res = mysqli_query($conexion, " SELECT SUPPORT,ENGINE,TRANSACTIONS,Comment FROM INFORMATION_SCHEMA.ENGINES  where ENGINE='InnoDB' and SUPPORT !='no' ");
  if (mysqli_num_rows($res) > 0) {
    $rowa = mysqli_fetch_assoc($res);
    $datos_engine = "<br/> Suport - " . $rowa['SUPPORT'] . " | Engine - " . $rowa['ENGINE'] . " | Transaccion - " . $rowa['TRANSACTIONS']. " <br> Comment - " . $rowa['Comment'];
      if (preg_match("/MariaDB/i", $servidor)) { ///miramos si es una base de datos MariaDB
          $tipo_engine_ok = "<br/>Esta usando Ud una base de datos MariaDB que es una variante de la base de datos MySQL.";
          $datosmariaDB = explode("-", $servidor);
          if($datosmariaDB[1]=="MariaDB"){ // añadimos esta opción ya que algunos servicdores de mariaDB dan la información como 10.3.29-MariaDB-log-cll-lve
            $versionMariaDB =$datosmariaDB[0];
          }else{
            $versionMariaDB =$datosmariaDB[1]; // si la información la dan como 5.5.5-10.3.29-MariaDB con la version en segundo termino
          }

          if (version_compare($versionMariaDB, '10.0.5') >= 0) {  //miramos la version de mariaDB  es anterior a la 10.0.5  que no admite busqueda fultext
              // si es superior metemos las tablas con fulltext
              $texto = file_get_contents("install/tablas_bbdd_demokratian.sql");
              $estado_error = "OK";
            } else {
              // InnoDB sin fulltext
              echo ' ERROR# <div class="alert alert-danger">Server info: '.$servidor.'<br>'.$datos_engine.'<br>¡¡¡ATENCION!!!! Esta usando una base de datos mariaDB anterior a 10.0.5. Tiene que actualizar su base de datos para continuar</div>';
              $estado_error = "conError";
              exit;
            }
        } else {
            if ($servidor < 5.6) {//miramos la version de mysql si es anterior a la 5.6
                //  mysql anteriores a la 5.6 sin busqueda fulltext
                echo ' ERROR# <div class="alert alert-danger">Server info: '.$servidor.'<br>'.$datos_engine.'<br>¡¡¡ATENCION!!!! Esta usando una base de datos MySQL anterior a 5.6. Tiene que actualizar su base de datos para continuar</div>';
                $estado_error = "conError";
                exit;
              } else {
                // incluimos las tablas InnoDB,
                  $texto = file_get_contents("install/tablas_bbdd_demokratian.sql");
                  $estado_error = "OK";
              }
          }
        } else {
          $res2 = mysqli_query($conexion, " SELECT SUPPORT,ENGINE,TRANSACTIONS,Comment FROM INFORMATION_SCHEMA.ENGINES  where ENGINE='MyISAM' and SUPPORT !='no' ");
          if (mysqli_num_rows($res2) > 0) {
            $rowb = mysqli_fetch_assoc($res2);
            $datos_engine = "<br/> Suport - " . $rowb['SUPPORT'] . " | Engine - " . $rowb['ENGINE'] . " | Transaccion - " . $rowb['TRANSACTIONS']. " <br> Comment - " . $rowa['Comment'];
            //  si no esta habilitado lo metemos las tablas como MyISAM
            //  $texto = file_get_contents("install/tablas_bbdd_MyISAM_demokratian.sql");

            if (preg_match("/MariaDB/i", $servidor)) { ///miramos si es una mase de datos MariaDB
              echo ' ERROR# <div class="alert alert-danger">Server info: '.$servidor.'<br>'.$datos_engine.'<br>¡¡¡ATENCION!!!! . Su base de datos es del tipo MyISAM. Actualice su servidor de BBDD ya  no tiene habilitado en su servidor de bases de datos InnoDB.</div>';
              $estado_error = "conError";
              exit;
            } else {
              echo ' ERROR# <div class="alert alert-danger">Server info: '.$servidor.'<br>'.$datos_engine.'<br>¡¡¡ATENCION!!!! . Su base de datos es del tipo MyISAM.  Actualice su servidor de BBDD ya  no tiene habilitado en su servidor de bases de datos InnoDB</div>';
              $estado_error = "conError";
              exit;
            }
          } else {
            echo ' ERROR# <div class="alert alert-danger">Server info: '.$servidor.'<br>'.$datos_engine.'<br>¡¡¡ATENCION!!!! . Su base de datos no tiene ni el motor MyISAM ni InnoDB por lo que es posible que la aplicación no funcione o lo haga con problemas.Actualice su servidor de BBDD ya  necesita un servidor de bases de datos InnoDB</div>';
            $estado_error = "conError";
            exit;
          }
        }


if ($estado_error == "OK"){  // si todo ha ido bien con las comprobaciones de la bbdd

  ///// Si tenemos los datos vamos a escribir los datos en el archivo config
  /////// creamos los docigos aleatorios de las claves de encriptacion y de las sesiones

  $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
  $cad1 = "";
  $cad2 = "";
  $cad3 = "";
  $cad4 = "";
  for ($i = 0; $i < 12; $i++) {
      $cad1 .= substr($str, rand(0, 62), 1);
      $cad2 .= substr($str, rand(0, 62), 1);
      $cad3 .= substr($str, rand(0, 62), 1);
      $cad4 .= substr($str, rand(0, 62), 1);
  }

  //miramos la url que se usa de base en la instalacion para meeterla luego en nuestars variables

  $host = $_SERVER["HTTP_HOST"];
  $url = $_SERVER["REQUEST_URI"];
  //$url = substr($url, 0, -1); //quitamos la ultima barra de la url

  $url = explode("/", $url);
  $url2 = array_splice($url, -2); //quitamos los dos ultimos elementos del array

  $url = implode("/", $url);
  $laUrl = "http://" . $host . $url;

  if (isset($_SERVER['HTTPS'])) {
      if ($_SERVER['HTTPS'] == "on") {
          $laUrl = "https://" . $host . $url;
      }
  }



    // iniciamos la creación del archivo config.inc.php añadiendo la cabecera y los datos iniciales (el resto se incluye en el archivo cpnfigira 2)

    $fichero = "install/cabecera_config.php"; //fichero que nos sirve de base para crear el nuevo

    if (!copy($fichero, $file)) {
      echo "Error al copiar $fichero...\n";
    }

    #Abrimos el fichero en modo de escritura

    $fh = fopen($file, 'r+');


    #preparamos el string con los datos
    $string1 = "
\$hostu = \"" . $username . "\";                              // Usario de la BBDD
\$hostp = \"" . $pass . "\";                          // Contraseña de la BBDD
\$dbn = \"" . $dbname . "\";              // Nombre de la BBDD
\$host = \"" . $dbhost . "\";                            // localhost de la BBDD
\$extension = \"" . $prefijo . "\";                             // prefijo de las tablas de la base de datos	\n


\$url_vot = \"" . $laUrl . "\";      // Url donde instalamos nuestra aplicación
\$carpetaPublica=\"public_html\";            //nombre de la carpeta publica

##################################################################################################################################
###############     variables del sistema de subida y/o redimension de images de los candidatos y roots       ###############
###############                    Ojo, darle permisos de escritura en el servidor si corre en Linux               ###############
##################################################################################################################################

\$upload_cat = \"data/upload_pic\";          //carpeta donde se guardan las images de los candidatos
\$upload_user = \"data/upload_user\";        //carpeta donde se guardan las images de los usuarios
\$baseUrl = \"/data/userfile/\";      //   carpeta donde se guardan las images y archivos de gestor ckeditor (siempre con el / al principio y al final)

##################################################################################################################################
###############                                        configuración de carpetas                                   ###############
###############                    Ojo, darle permisos de escritura en el servidor si corre en Linux               ###############
##################################################################################################################################

\$FileRec = \"../private/data/data_rec/\";                  //   carpeta donde se generan los archivos de recuento
\$FilePath = \"../private/data/data_vut/\";                 //   carpeta donde se generan los archivos del vut
\$path_bakup_bbdd = \"../private/data/backup\";       // Carpeta donde se guardan los back-up de la bbdd


###################################################################################################################################
#############                       Todo este grupo de variables no tienen porque ser modificadas.                    #############
#############                                 hacerlo solo si se tiene conocimientos                                  #############
###################################################################################################################################

\$b_debugmode = 0; // 0 || 1  Forma de errores cuando hay problemas con la base de datos



##################################################################################################################################
#####################                               Otras viariables del sistema                             #####################
##################################################################################################################################
\$tiempo_session = 900;  // tiempo de caducidad de la sesion en segundos 900 son 15 minutos

\$usuarios_sesion = \"" . $cad1 . "\"; // nombre de la sesion
\$usuarios_sesion2 = \"" . $cad2 . "\"; // nombre de la sesion de los interventores
\$clave_encriptacion = \"" . $cad3 . "\";//
\$clave_encriptacion2 = \"" . $cad4 . "\";


?>";


    fseek($fh, -3, SEEK_END); // nos vamos 3 caraceres antes para quitar el cierre de php
    //  fwrite($fh, $string1) or die("Could not write file!");
     if (fwrite($fh, $string1) === FALSE){ //escribimos en el archivo
      echo 'ERROR# <div class="alert alert-danger"> No se puede escribir en el archivo ' .$file. '</div>';
      exit;
    }
    fclose($fh);  //  Cerramos el fichero

    //	metemos las tablas	en la base de datos

    $texto = str_replace("votaciones_demokratian", $dbname, $texto);
    $texto = str_replace("dk_", $prefijo, $texto);
    $sentencia = explode(";", $texto);

    for ($i = 0; $i < (count($sentencia) - 1); $i++) {
      $error = "Error de instalacion al incluir los datos ";
        $resultados =   db_query($conexion, $sentencia[$i], $error);
        if (!$resultados) {
            echo 'ERROR# <div class="alert alert-danger">Se ha producido el siguiente error: Error creating table: ' . mysqli_error($conexion). '</div>';
            $estado_error = "conError";
           exit;
        }
      }

    // incluimos los datos de las tablas

    $texto = file_get_contents("install/tablas_datos.sql");
    $texto = str_replace("dk_", $prefijo, $texto);
    $sentencia = explode(";", $texto);

    for ($i = 0; $i < (count($sentencia) - 1); $i++) {
    $error = "Error de instalacion al incluir los datos ";
    $resultados = db_query($conexion, $sentencia[$i], $error);
        if (!$resultados) {
            echo 'ERROR# <div class="alert alert-danger"> Se ha producido el siguiente error: Error al incluir datos en la Base de datos: ' . mysqli_error($conexion). '</div>';
            $estado_error = "conError";
            exit;
        }
    }
}


if ($estado_error == "conError") {

    echo 'ERROR# <br/>'.$mensaje .'<br/>
    <div class="alert alert-danger">CRITICAL ERROR!!: ' . $datos_engine .
    ' ' . $tipo_engine .
    '<br/>' . $servidor .'</div> ';
}

if ($estado_error == "OK") {

  $mensaje .= "
  Usuario <strong>$username </strong> correcto <br/>
  <strong>Password</strong> de la base de datos OK <br/>
  Host <strong> $dbhost </strong> correcto <br/>
  Base de datos: <strong>$dbname</strong>  conexion correcta<br />
  Prefijo de las tablas <strong> $prefijo </strong>";

    echo 'OK# <div class="alert alert-info">' . $datos_engine .
    ' <br/>'. $mensaje .
    '<br/>Las tablas de la base de datos han sido correctamente incluidas ' . $tipo_engine_ok .
    '<br/>' . $servidor . '</div>';
}

mysqli_close($conexion);
?>

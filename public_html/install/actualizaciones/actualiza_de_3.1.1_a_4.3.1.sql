/*
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
*/

/*#############################################################################################################################################################
##                                                                                                                                                           ##
##                                       Scrip de actualización de demokratian version 3.1.1 a la versión 4.3                                                ##
##                                                                                                                                                           ##
#############################################################################################################################################################*/

/*dk_candidatos*/

ALTER TABLE dk_candidatos
   MODIFY COLUMN modif TEXT
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NULL
                   DEFAULT NULL;

ALTER TABLE dk_candidatos
   MODIFY COLUMN texto MEDIUMTEXT
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NULL
                   DEFAULT NULL;

ALTER TABLE dk_candidatos
   MODIFY COLUMN anadido TEXT
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NOT NULL;

ALTER TABLE dk_candidatos
   MODIFY COLUMN imagen_pequena TEXT
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NULL
                   DEFAULT NULL;

ALTER TABLE dk_candidatos
   MODIFY COLUMN sexo CHAR(2)
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NOT NULL
                   DEFAULT 'H';

ALTER TABLE dk_candidatos
   MODIFY COLUMN imagen TEXT
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NULL
                   DEFAULT NULL;

ALTER TABLE dk_candidatos
   MODIFY COLUMN nombre_usuario TEXT
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NULL
                   DEFAULT NULL;

ALTER TABLE dk_candidatos
   COLLATE 'utf8mb4_unicode_520_ci';


/*dk_ccaa*/

ALTER TABLE dk_ccaa
   MODIFY COLUMN ccaa TEXT
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NOT NULL;

ALTER TABLE dk_ccaa
   COLLATE 'utf8mb4_unicode_520_ci';


   /*dk_codificadores*/

/*   ALTER TABLE dk_codificadores
   DROP PRIMARY KEY;*/

ALTER TABLE dk_codificadores
   MODIFY COLUMN nombre TEXT
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NULL
                   DEFAULT NULL;

ALTER TABLE dk_codificadores
   MODIFY COLUMN usuario TEXT
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NULL
                   DEFAULT NULL;

ALTER TABLE dk_codificadores
   MODIFY COLUMN clave_privada MEDIUMTEXT
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NULL
                   DEFAULT NULL;

ALTER TABLE dk_codificadores
   MODIFY COLUMN nif TEXT
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NULL
                   DEFAULT NULL;

ALTER TABLE dk_codificadores
   MODIFY COLUMN codigo_rec TEXT
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NULL
                   DEFAULT NULL;

ALTER TABLE dk_codificadores
   MODIFY COLUMN correo TEXT
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NOT NULL;

ALTER TABLE dk_codificadores
   MODIFY COLUMN clave_publica MEDIUMTEXT
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NULL
                   DEFAULT NULL;

ALTER TABLE dk_codificadores
   MODIFY COLUMN fecha_ultima DATETIME(0) NULL DEFAULT CURRENT_TIMESTAMP();

ALTER TABLE dk_codificadores
   MODIFY COLUMN fecha_modif DATETIME(0) NULL DEFAULT CURRENT_TIMESTAMP();

ALTER TABLE dk_codificadores
   MODIFY COLUMN pass TEXT
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NOT NULL;

/*ALTER TABLE dk_codificadores
   ADD INDEX fk_dk_codificadores_dk_votantes1_idx (anadido) USING BTREE;

ALTER TABLE dk_codificadores
   ADD CONSTRAINT fk_dk_codificadores_dk_votantes1 FOREIGN KEY(anadido)
          REFERENCES dk_votantes (`ID`) ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE dk_codificadores
   ADD PRIMARY KEY(`ID`, id_votacion, anadido);*/

ALTER TABLE dk_codificadores
   COLLATE 'utf8mb4_unicode_520_ci';


   /*dk_debate_comentario*/

   ALTER TABLE dk_debate_comentario
      MODIFY COLUMN comentario MEDIUMTEXT
                      CHARACTER SET utf8mb4
                      COLLATE utf8mb4_unicode_520_ci
                      NULL
                      DEFAULT NULL;

   ALTER TABLE dk_debate_comentario
      MODIFY COLUMN estado TEXT
                      CHARACTER SET utf8mb4
                      COLLATE utf8mb4_unicode_520_ci
                      NULL
                      DEFAULT NULL;

   ALTER TABLE dk_debate_comentario
      MODIFY COLUMN tema TEXT
                      CHARACTER SET utf8mb4
                      COLLATE utf8mb4_unicode_520_ci
                      NULL
                      DEFAULT NULL;

   ALTER TABLE dk_debate_comentario
      MODIFY COLUMN fecha VARCHAR(40)
                      CHARACTER SET utf8mb4
                      COLLATE utf8mb4_unicode_520_ci
                      NOT NULL;

   ALTER TABLE dk_debate_comentario
      COLLATE 'utf8mb4_unicode_520_ci';

      /*dk_debate_preguntas*/

  --    DROP INDEX `fk_dk_debate_preguntas_dk_votacion1_idx` ON dk_debate_preguntas;

ALTER TABLE dk_debate_preguntas
   MODIFY COLUMN modif TEXT
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NULL
                   DEFAULT NULL;

ALTER TABLE dk_debate_preguntas
   MODIFY COLUMN anadido INT(10) UNSIGNED ZEROFILL NOT NULL;

ALTER TABLE dk_debate_preguntas
   MODIFY COLUMN respuestas CHAR(2)
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NOT NULL;

ALTER TABLE dk_debate_preguntas
   MODIFY COLUMN pregunta TEXT
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NOT NULL;

/*ALTER TABLE dk_debate_preguntas
   ADD INDEX fk_dk_debate_preguntas_dk_votantes1_idx (anadido) USING BTREE;

ALTER TABLE dk_debate_preguntas
   ADD INDEX fk_dk_debate_preguntas_dk_votacion1_idx1
          (id_votacion)
          USING BTREE;

ALTER TABLE dk_debate_preguntas
   ADD CONSTRAINT fk_dk_debate_preguntas_dk_votantes1 FOREIGN KEY(anadido)
          REFERENCES dk_votantes (`ID`) ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE dk_debate_preguntas
   ADD CONSTRAINT fk_dk_debate_preguntas_dk_votacion1 FOREIGN KEY
          (id_votacion)
          REFERENCES dk_votacion (`ID`) ON UPDATE RESTRICT ON DELETE RESTRICT;*/

ALTER TABLE dk_debate_preguntas
   COLLATE 'utf8mb4_unicode_520_ci';

/*dk_debate_votos*/


ALTER TABLE dk_debate_votos
MODIFY COLUMN codigo_val CHAR(128)
                CHARACTER SET utf8mb4
                COLLATE utf8mb4_unicode_520_ci
                NULL
                DEFAULT NULL;

ALTER TABLE dk_debate_votos
MODIFY COLUMN voto CHAR(2)
                CHARACTER SET utf8mb4
                COLLATE utf8mb4_unicode_520_ci
                NOT NULL
                DEFAULT '0';

ALTER TABLE dk_debate_votos
COLLATE 'utf8mb4_unicode_520_ci';

/*dk_elvoto*/

ALTER TABLE dk_elvoto
   MODIFY COLUMN incluido TEXT
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NULL
                   DEFAULT NULL;

ALTER TABLE dk_elvoto
   MODIFY COLUMN voto MEDIUMTEXT
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NOT NULL;

ALTER TABLE dk_elvoto
   MODIFY COLUMN `ID` CHAR(64)
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NOT NULL;

ALTER TABLE dk_elvoto
   MODIFY COLUMN codigo_val CHAR(128)
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NULL
                   DEFAULT NULL;
/*
ALTER TABLE dk_elvoto
   ADD PRIMARY KEY(`ID`);
*/
ALTER TABLE dk_elvoto
   COLLATE 'utf8mb4_unicode_520_ci';

   /*dk_grupo_trabajo*/

   ALTER TABLE dk_grupo_trabajo
   MODIFY COLUMN tipo_votante CHAR(2)
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NOT NULL;

ALTER TABLE dk_grupo_trabajo
   MODIFY COLUMN texto MEDIUMTEXT
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NULL
                   DEFAULT NULL;

ALTER TABLE dk_grupo_trabajo
   MODIFY COLUMN subgrupo TEXT
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NOT NULL;

ALTER TABLE dk_grupo_trabajo
   COLLATE 'utf8mb4_unicode_520_ci';


   /*dk_interventores*/

   ALTER TABLE dk_interventores
   MODIFY COLUMN nombre TEXT
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NULL
                   DEFAULT NULL;

ALTER TABLE dk_interventores
   MODIFY COLUMN pass TEXT
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NULL
                   DEFAULT NULL;

ALTER TABLE dk_interventores
   MODIFY COLUMN modif TEXT
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NULL
                   DEFAULT NULL;

ALTER TABLE dk_interventores
   MODIFY COLUMN correo TEXT
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NOT NULL;

ALTER TABLE dk_interventores
   MODIFY COLUMN usuario TEXT
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NULL
                   DEFAULT NULL;

ALTER TABLE dk_interventores
   MODIFY COLUMN apellidos TEXT
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NULL
                   DEFAULT NULL;

ALTER TABLE dk_interventores
   MODIFY COLUMN codigo_rec TEXT
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NULL
                   DEFAULT NULL;

ALTER TABLE dk_interventores
   MODIFY COLUMN anadido TEXT
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NOT NULL;

ALTER TABLE dk_interventores
   COLLATE 'utf8mb4_unicode_520_ci';


   /* dk_municipios*/

ALTER TABLE dk_municipios
   MODIFY COLUMN nombre VARCHAR(100)
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NOT NULL
                   DEFAULT '';

ALTER TABLE dk_municipios
   COLLATE 'utf8mb4_unicode_520_ci';

   /*dk_nivel_usuario*/


ALTER TABLE dk_nivel_usuario
   MODIFY COLUMN nivel_usuario TEXT
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NOT NULL;

ALTER TABLE dk_nivel_usuario
   COLLATE 'utf8mb4_unicode_520_ci';

   /*dk_provincia*/

ALTER TABLE dk_provincia
   MODIFY COLUMN provincia TEXT
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NOT NULL;

ALTER TABLE dk_provincia
   MODIFY COLUMN correo_notificaciones TEXT
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NULL
                   DEFAULT NULL;

ALTER TABLE dk_provincia
   MODIFY COLUMN especial INT(3) NULL DEFAULT 0;

ALTER TABLE dk_provincia
   COLLATE 'utf8mb4_unicode_520_ci';

   /*dk_usuario_admin_x_provincia*/

/*   DROP PRIMARY KEY;

ALTER TABLE dk_usuario_admin_x_provincia
   ADD PRIMARY KEY(`ID`);*/

ALTER TABLE dk_usuario_admin_x_provincia
   COLLATE 'utf8mb4_unicode_520_ci';


    /*dk_usuario_x_g_trabajo*/
/*    ALTER TABLE dk_usuario_x_g_trabajo
       DROP PRIMARY KEY;*/

    ALTER TABLE dk_usuario_x_g_trabajo
       MODIFY COLUMN razon_bloqueo MEDIUMTEXT
                       CHARACTER SET utf8mb4
                       COLLATE utf8mb4_unicode_520_ci
                       NULL
                       DEFAULT NULL;

/*    ALTER TABLE dk_usuario_x_g_trabajo
       ADD PRIMARY KEY(`ID`);*/

    ALTER TABLE dk_usuario_x_g_trabajo
       COLLATE 'utf8mb4_unicode_520_ci';


     /*dk_usuario_x_votacion*/

  /*   ALTER TABLE dk_usuario_x_votacion
        DROP PRIMARY KEY;*/

     ALTER TABLE dk_usuario_x_votacion
        MODIFY COLUMN tipo_votante CHAR(2)
                        CHARACTER SET utf8mb4
                        COLLATE utf8mb4_unicode_520_ci
                        NOT NULL;

     ALTER TABLE dk_usuario_x_votacion
        MODIFY COLUMN `ID` CHAR(64)
                        CHARACTER SET utf8mb4
                        COLLATE utf8mb4_unicode_520_ci
                        NOT NULL;

     ALTER TABLE dk_usuario_x_votacion
        MODIFY COLUMN ip TEXT
                        CHARACTER SET utf8mb4
                        COLLATE utf8mb4_unicode_520_ci
                        NOT NULL;

     ALTER TABLE dk_usuario_x_votacion
        MODIFY COLUMN correo_usuario TEXT
                        CHARACTER SET utf8mb4
                        COLLATE utf8mb4_unicode_520_ci
                        NULL
                        DEFAULT NULL;

     ALTER TABLE dk_usuario_x_votacion
        MODIFY COLUMN forma_votacion TEXT
                        CHARACTER SET utf8mb4
                        COLLATE utf8mb4_unicode_520_ci
                        NOT NULL;

/*     ALTER TABLE dk_usuario_x_votacion
        ADD PRIMARY KEY(`ID`);*/

     ALTER TABLE dk_usuario_x_votacion
        COLLATE 'utf8mb4_unicode_520_ci';


      /*dk_votacion*/

      ALTER TABLE dk_votacion
         MODIFY COLUMN activa CHAR(2)
                         CHARACTER SET utf8mb4
                         COLLATE utf8mb4_unicode_520_ci
                         NOT NULL
                         DEFAULT 'no';

      ALTER TABLE dk_votacion
         MODIFY COLUMN resumen MEDIUMTEXT
                         CHARACTER SET utf8mb4
                         COLLATE utf8mb4_unicode_520_ci
                         NULL
                         DEFAULT NULL;

      ALTER TABLE dk_votacion
         MODIFY COLUMN modif TEXT
                         CHARACTER SET utf8mb4
                         COLLATE utf8mb4_unicode_520_ci
                         NULL
                         DEFAULT NULL;

      ALTER TABLE dk_votacion
         MODIFY COLUMN interventor CHAR(2)
                         CHARACTER SET utf8mb4
                         COLLATE utf8mb4_unicode_520_ci
                         NOT NULL
                         DEFAULT 'no';

      ALTER TABLE dk_votacion
         MODIFY COLUMN encripta CHAR(2)
                         CHARACTER SET utf8mb4
                         COLLATE utf8mb4_unicode_520_ci
                         NOT NULL
                         DEFAULT 'no';

      ALTER TABLE dk_votacion
         MODIFY COLUMN nombre_votacion TEXT
                         CHARACTER SET utf8mb4
                         COLLATE utf8mb4_unicode_520_ci
                         NOT NULL;

      ALTER TABLE dk_votacion
         MODIFY COLUMN tipo CHAR(2)
                         CHARACTER SET utf8mb4
                         COLLATE utf8mb4_unicode_520_ci
                         NOT NULL
                         DEFAULT '1';

      ALTER TABLE dk_votacion
         MODIFY COLUMN anadido TEXT
                         CHARACTER SET utf8mb4
                         COLLATE utf8mb4_unicode_520_ci
                         NOT NULL;

      ALTER TABLE dk_votacion
         MODIFY COLUMN tipo_votante CHAR(2)
                         CHARACTER SET utf8mb4
                         COLLATE utf8mb4_unicode_520_ci
                         NOT NULL
                         DEFAULT '1';

      ALTER TABLE dk_votacion
         MODIFY COLUMN fin_resultados CHAR(2)
                         CHARACTER SET utf8mb4
                         COLLATE utf8mb4_unicode_520_ci
                         NOT NULL
                         DEFAULT 'no';

      ALTER TABLE dk_votacion
         MODIFY COLUMN texto MEDIUMTEXT
                         CHARACTER SET utf8mb4
                         COLLATE utf8mb4_unicode_520_ci
                         NULL
                         DEFAULT NULL;

      ALTER TABLE dk_votacion
         MODIFY COLUMN activos_resultados CHAR(2)
                         CHARACTER SET utf8mb4
                         COLLATE utf8mb4_unicode_520_ci
                         NOT NULL
                         DEFAULT 'no';

      ALTER TABLE dk_votacion
         COLLATE 'utf8mb4_unicode_520_ci';


       /*dk_votacion_web*/
       ALTER TABLE dk_votacion_web
          MODIFY COLUMN diseno MEDIUMTEXT
                          CHARACTER SET utf8mb4
                          COLLATE utf8mb4_unicode_520_ci
                          NULL
                          DEFAULT NULL
                          COMMENT 'Incluir CSS personalizado';

       ALTER TABLE dk_votacion_web
          MODIFY COLUMN imagen_cab TEXT
                          CHARACTER SET utf8mb4
                          COLLATE utf8mb4_unicode_520_ci
                          NULL
                          DEFAULT NULL;

       ALTER TABLE dk_votacion_web
          MODIFY COLUMN aux TEXT
                          CHARACTER SET utf8mb4
                          COLLATE utf8mb4_unicode_520_ci
                          NULL
                          DEFAULT NULL;

       ALTER TABLE dk_votacion_web
          COLLATE 'utf8mb4_unicode_520_ci';


        /*dk_votantes*/

        ALTER TABLE dk_votantes
           MODIFY COLUMN pass TEXT
                           CHARACTER SET utf8mb4
                           COLLATE utf8mb4_unicode_520_ci
                           NULL
                           DEFAULT NULL;

        ALTER TABLE dk_votantes
           MODIFY COLUMN fecha_control DATETIME(0) NULL DEFAULT NULL;

        ALTER TABLE dk_votantes
           MODIFY COLUMN tipo_votante CHAR(2)
                           CHARACTER SET utf8mb4
                           COLLATE utf8mb4_unicode_520_ci
                           NOT NULL
                           COMMENT 'Si es socio verificado, sin verificar u otros';

        ALTER TABLE dk_votantes
           MODIFY COLUMN razon_bloqueo TEXT
                           CHARACTER SET utf8mb4
                           COLLATE utf8mb4_unicode_520_ci
                           NULL
                           DEFAULT NULL;

        ALTER TABLE dk_votantes
           MODIFY COLUMN nivel_usuario INT(2)
                           NOT NULL
                           DEFAULT 1
                           COMMENT 'Si es votante (1) o tipo de administrador';

        ALTER TABLE dk_votantes
           MODIFY COLUMN correo_usuario TEXT
                           CHARACTER SET utf8mb4
                           COLLATE utf8mb4_unicode_520_ci
                           NOT NULL;

        ALTER TABLE dk_votantes
           MODIFY COLUMN perfil MEDIUMTEXT
                           CHARACTER SET utf8mb4
                           COLLATE utf8mb4_unicode_520_ci
                           NULL
                           DEFAULT NULL;

        ALTER TABLE dk_votantes
           MODIFY COLUMN imagen VARCHAR(191)
                           CHARACTER SET utf8mb4
                           COLLATE utf8mb4_unicode_520_ci
                           NOT NULL
                           DEFAULT 'usuario.jpg';

        ALTER TABLE dk_votantes
           MODIFY COLUMN nombre_usuario TEXT
                           CHARACTER SET utf8mb4
                           COLLATE utf8mb4_unicode_520_ci
                           NOT NULL;

        ALTER TABLE dk_votantes
           MODIFY COLUMN nif TEXT
                           CHARACTER SET utf8mb4
                           COLLATE utf8mb4_unicode_520_ci
                           NOT NULL;

        ALTER TABLE dk_votantes
           MODIFY COLUMN apellido_usuario TEXT
                           CHARACTER SET utf8mb4
                           COLLATE utf8mb4_unicode_520_ci
                           NULL
                           DEFAULT NULL;

        ALTER TABLE dk_votantes
           MODIFY COLUMN nivel_acceso SMALLINT(4)
                           NOT NULL
                           DEFAULT 10
                           COMMENT 'Nivel de acceso a las paginas, esta relacionado con el nivel_usuario ';

        ALTER TABLE dk_votantes
           MODIFY COLUMN usuario TINYTEXT
                           CHARACTER SET utf8mb4
                           COLLATE utf8mb4_unicode_520_ci
                           NULL
                           DEFAULT NULL
                           COMMENT 'Nombre de usuario UNICO';

        ALTER TABLE dk_votantes
           MODIFY COLUMN imagen_pequena VARCHAR(191)
                           CHARACTER SET utf8mb4
                           COLLATE utf8mb4_unicode_520_ci
                           NOT NULL
                           DEFAULT 'peq_usuario.jpg';

        ALTER TABLE dk_votantes
           MODIFY COLUMN bloqueo CHAR(2)
                           CHARACTER SET utf8mb4
                           COLLATE utf8mb4_unicode_520_ci
                           NOT NULL
                           DEFAULT 'no';

        ALTER TABLE dk_votantes
           MODIFY COLUMN codigo_rec TEXT
                           CHARACTER SET utf8mb4
                           COLLATE utf8mb4_unicode_520_ci
                           NULL
                           DEFAULT NULL;

        ALTER TABLE dk_votantes
           ADD COLUMN idioma CHAR(10)
                        CHARACTER SET utf8mb4
                        COLLATE utf8mb4_unicode_520_ci
                        NOT NULL
                        DEFAULT 'es_ES'
           AFTER fecha_control;

        ALTER TABLE dk_votantes
           ADD COLUMN admin_blog INT(2) NOT NULL DEFAULT 0;

        ALTER TABLE dk_votantes
           COLLATE 'utf8mb4_unicode_520_ci';


        /*dk_votantes_x_admin*/

        ALTER TABLE dk_votantes_x_admin
           COLLATE 'utf8mb4_unicode_520_ci';



        /*dk_voto_temporal*/
        ALTER TABLE dk_voto_temporal
           MODIFY COLUMN id_votacion VARCHAR(40)
                           CHARACTER SET utf8mb4
                           COLLATE utf8mb4_unicode_520_ci
                           NOT NULL;

        ALTER TABLE dk_voto_temporal
           MODIFY COLUMN datos LONGTEXT
                           CHARACTER SET utf8mb4
                           COLLATE utf8mb4_unicode_520_ci
                           NOT NULL;

        ALTER TABLE dk_voto_temporal
           COMMENT 'Tabla que almacena los 5 ultimos votos para luego poder meterlo en la tabla votos seguridad. \\nEl id de la votacion tambien esta encriptado por lo que no tiene relacion directa con el id principal de la tabla de votaciones ' COLLATE 'utf8mb4_unicode_520_ci';


        /*dk_votos*/

        ALTER TABLE dk_votos
           MODIFY COLUMN codigo_val CHAR(128)
                           CHARACTER SET utf8mb4
                           COLLATE utf8mb4_unicode_520_ci
                           NULL
                           DEFAULT NULL;

        ALTER TABLE dk_votos
           MODIFY COLUMN `ID` CHAR(64)
                           CHARACTER SET utf8mb4
                           COLLATE utf8mb4_unicode_520_ci
                           NOT NULL;

        ALTER TABLE dk_votos
           MODIFY COLUMN incluido TEXT
                           CHARACTER SET utf8mb4
                           COLLATE utf8mb4_unicode_520_ci
                           NULL
                           DEFAULT NULL;

        ALTER TABLE dk_votos
           MODIFY COLUMN id_candidato TEXT
                           CHARACTER SET utf8mb4
                           COLLATE utf8mb4_unicode_520_ci
                           NOT NULL;

        ALTER TABLE dk_votos
           MODIFY COLUMN vote_id TEXT
                           CHARACTER SET utf8mb4
                           COLLATE utf8mb4_unicode_520_ci
                           NOT NULL;

    /*    ALTER TABLE dk_votos
           ADD PRIMARY KEY(`ID`);*/

        ALTER TABLE dk_votos
           COLLATE 'utf8mb4_unicode_520_ci';


        /*dk_votos_seg*/

ALTER TABLE dk_votos_seg
   MODIFY COLUMN cadena LONGTEXT
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NOT NULL;

ALTER TABLE dk_votos_seg
   MODIFY COLUMN `ID` CHAR(140)
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NOT NULL;

ALTER TABLE dk_votos_seg
   MODIFY COLUMN voto LONGTEXT
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NOT NULL;

ALTER TABLE dk_votos_seg
   MODIFY COLUMN id_votacion VARCHAR(40)
                   CHARACTER SET utf8mb4
                   COLLATE utf8mb4_unicode_520_ci
                   NULL
                   DEFAULT NULL;

ALTER TABLE dk_votos_seg
   COLLATE 'utf8mb4_unicode_520_ci';


   --
   -- Estructura de tabla para la tabla `dk_blog_cabeceras`
   --

   CREATE TABLE `dk_blog_cabeceras` (
     `id` int(10) UNSIGNED NOT NULL,
     `id_incluido` int(10) UNSIGNED ZEROFILL NOT NULL,
     `titulo` tinytext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
     `texto` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
     `fecha` datetime NOT NULL DEFAULT current_timestamp(),
     `imagen` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
     `id_categoria` smallint(3) NOT NULL,
     `zona_pagina` smallint(2) DEFAULT NULL,
     `activo` smallint(1) NOT NULL DEFAULT 0,
     `orden` smallint(2) DEFAULT NULL
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

   -- --------------------------------------------------------

   --
   -- Estructura de tabla para la tabla `dk_blog_cabeceras_lang`
   --

   CREATE TABLE `dk_blog_cabeceras_lang` (
     `id` int(10) NOT NULL,
     `id_incluido` int(10) UNSIGNED ZEROFILL NOT NULL,
     `lenguajes_id` int(4) UNSIGNED NOT NULL,
     `blog_cabeceras_id` int(10) UNSIGNED NOT NULL,
     `titulo` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
     `texto` mediumtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
     `cod_idioma` varchar(5) COLLATE utf8mb4_unicode_520_ci DEFAULT 'es_ES'
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

   -- --------------------------------------------------------

   --
   -- Estructura de tabla para la tabla `dk_blog_categorias`
   --

   CREATE TABLE `dk_blog_categorias` (
     `id` int(10) UNSIGNED NOT NULL,
     `id_incluido` int(10) UNSIGNED ZEROFILL NOT NULL,
     `titulo` tinytext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
     `texto` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
     `fecha` datetime NOT NULL DEFAULT current_timestamp(),
     `activo` smallint(1) NOT NULL DEFAULT 0 COMMENT '1 para categoría básica no borrable'
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

   -- --------------------------------------------------------

   --
   -- Estructura de tabla para la tabla `dk_blog_categorias_lang`
   --

   CREATE TABLE `dk_blog_categorias_lang` (
     `id` int(10) NOT NULL,
     `id_incluido` int(10) UNSIGNED ZEROFILL NOT NULL,
     `lenguajes_id` int(4) UNSIGNED NOT NULL,
     `blog_categorias_id` int(10) UNSIGNED NOT NULL,
     `titulo` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
     `texto` mediumtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
     `cod_idioma` varchar(5) COLLATE utf8mb4_unicode_520_ci DEFAULT 'es_ES'
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

   -- --------------------------------------------------------

   --
   -- Estructura de tabla para la tabla `dk_blog_entradas`
   --

   CREATE TABLE `dk_blog_entradas` (
     `id` int(10) UNSIGNED ZEROFILL NOT NULL,
     `id_incluido` int(10) UNSIGNED ZEROFILL NOT NULL,
     `titulo` tinytext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
     `texto` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
     `fecha` datetime DEFAULT current_timestamp(),
     `imagen` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
     `id_categoria` smallint(3) NOT NULL,
     `zona_pagina` smallint(2) DEFAULT NULL,
     `activo` smallint(1) NOT NULL DEFAULT 0 COMMENT 'si esta activo el bloque 1'
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

   -- --------------------------------------------------------

   --
   -- Estructura de tabla para la tabla `dk_blog_entradas_lang`
   --

   CREATE TABLE `dk_blog_entradas_lang` (
     `id` int(10) NOT NULL,
     `id_incluido` int(10) UNSIGNED ZEROFILL NOT NULL,
     `lenguajes_id` int(4) UNSIGNED NOT NULL,
     `blog_entradas_id` int(10) UNSIGNED NOT NULL,
     `titulo` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
     `texto` mediumtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
     `cod_idioma` varchar(5) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

   -- --------------------------------------------------------

   --
   -- Estructura de tabla para la tabla `dk_blog_estructura_inicio`
   --

   CREATE TABLE `dk_blog_estructura_inicio` (
     `id` int(10) NOT NULL,
     `id_incluido` int(10) DEFAULT NULL,
     `title` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
     `description` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
     `activo` smallint(1) NOT NULL DEFAULT 0 COMMENT 'si esta activo el bloque 1',
     `bloque` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
     `orden` smallint(2) DEFAULT NULL
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

   -- --------------------------------------------------------

   --
   -- Estructura de tabla para la tabla `dk_blog_menu`
   --

   CREATE TABLE `dk_blog_menu` (
     `id` int(10) NOT NULL,
     `id_incluido` int(10) UNSIGNED ZEROFILL NOT NULL,
     `title` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
     `description` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
     `url` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
     `nivel` smallint(2) NOT NULL DEFAULT 0,
     `padre` int(10) UNSIGNED ZEROFILL DEFAULT NULL,
     `orden` smallint(2) DEFAULT NULL,
     `activo` smallint(1) NOT NULL DEFAULT 0 COMMENT 'si esta activo como menú 1, si esta activo como menú pie 2'
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

   -- --------------------------------------------------------

   --
   -- Estructura de tabla para la tabla `dk_blog_menu_lang`
   --

   CREATE TABLE `dk_blog_menu_lang` (
     `id` int(10) NOT NULL,
     `id_incluido` int(10) UNSIGNED ZEROFILL NOT NULL,
     `lenguajes_id` int(4) UNSIGNED NOT NULL,
     `blog_menu_id` int(10) NOT NULL,
     `title` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
     `url` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
     `cod_idioma` varchar(5) COLLATE utf8mb4_unicode_520_ci DEFAULT 'es_ES'
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

   -- --------------------------------------------------------

   --
   -- Estructura de tabla para la tabla `dk_blog_paginas`
   --

   CREATE TABLE `dk_blog_paginas` (
     `id` int(10) UNSIGNED NOT NULL,
     `id_incluido` int(10) UNSIGNED ZEROFILL NOT NULL,
     `titulo` tinytext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
     `texto` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
     `fecha` datetime NOT NULL DEFAULT current_timestamp(),
     `borrable` char(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'si',
     `pagina` smallint(2) DEFAULT NULL,
     `zona_pagina` smallint(2) DEFAULT NULL,
     `activo` smallint(1) NOT NULL DEFAULT 0 COMMENT 'si esta activo el bloque 1'
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

   -- --------------------------------------------------------

   --
   -- Estructura de tabla para la tabla `dk_blog_paginas_lang`
   --

   CREATE TABLE `dk_blog_paginas_lang` (
     `id` int(10) NOT NULL,
     `id_incluido` int(10) UNSIGNED ZEROFILL NOT NULL,
     `lenguajes_id` int(4) UNSIGNED NOT NULL,
     `blog_paginas_id` int(10) UNSIGNED NOT NULL,
     `titulo` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
     `texto` mediumtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
     `cod_idioma` varchar(5) COLLATE utf8mb4_unicode_520_ci DEFAULT 'es_ES'
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

   -- --------------------------------------------------------

   --
   -- Estructura de tabla para la tabla `dk_candidatos_lang`
   --

   CREATE TABLE `dk_candidatos_lang` (
     `id` int(10) NOT NULL,
     `id_incluido` int(10) UNSIGNED ZEROFILL NOT NULL,
     `lenguajes_id` int(4) UNSIGNED NOT NULL,
     `candidatos_ID` int(6) UNSIGNED ZEROFILL NOT NULL,
     `nombre` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
     `texto` mediumtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
     `cod_idioma` varchar(5) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

   -- --------------------------------------------------------

   --
   -- Estructura de tabla para la tabla `dk_debate_preguntas_lang`
   --

   CREATE TABLE `dk_debate_preguntas_lang` (
     `id` int(10) NOT NULL,
     `id_incluido` int(10) UNSIGNED ZEROFILL NOT NULL,
     `lenguajes_id` int(4) UNSIGNED NOT NULL,
     `debate_preguntas_ID` int(6) UNSIGNED ZEROFILL NOT NULL,
     `pregunta` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
     `cod_idioma` varchar(5) COLLATE utf8mb4_unicode_520_ci DEFAULT 'es_ES'
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

   -- --------------------------------------------------------

   --
   -- Estructura de tabla para la tabla `dk_grupo_trabajo_lang`
   --

   CREATE TABLE `dk_grupo_trabajo_lang` (
     `id` int(10) NOT NULL,
     `id_incluido` int(10) UNSIGNED ZEROFILL NOT NULL,
     `lenguajes_id` int(4) UNSIGNED NOT NULL,
     `grupo_trabajo_ID` int(4) UNSIGNED ZEROFILL NOT NULL,
     `subgrupo` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
     `texto` mediumtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
     `cod_idioma` varchar(5) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

   -- --------------------------------------------------------

   --
   -- Estructura de tabla para la tabla `dk_lenguajes`
   --

   CREATE TABLE `dk_lenguajes` (
     `id` int(4) UNSIGNED NOT NULL,
     `lenguaje_cod_pais_id` int(10) UNSIGNED NOT NULL,
     `cod_LP` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
     `descripcion` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
     `tipo` int(2) NOT NULL DEFAULT 1 COMMENT 'Para indicar si es el idioma base de la aplicación poner 0',
     `activo` smallint(1) NOT NULL DEFAULT 0 COMMENT 'si esta activo el bloque 1'
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

   -- --------------------------------------------------------

   --
   -- Estructura de tabla para la tabla `dk_lenguaje_cod_pais`
   --

   CREATE TABLE `dk_lenguaje_cod_pais` (
     `id` int(10) UNSIGNED NOT NULL,
     `cod_LP` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
     `descripcion` varchar(60) CHARACTER SET utf8 DEFAULT NULL
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

   -- --------------------------------------------------------

   --
   -- Estructura de tabla para la tabla `dk_votacion_lang`
   --

   CREATE TABLE `dk_votacion_lang` (
     `id` int(10) NOT NULL,
     `votacion_ID` int(6) UNSIGNED ZEROFILL NOT NULL,
     `lenguajes_id` int(4) UNSIGNED NOT NULL,
     `id_incluido` int(10) UNSIGNED ZEROFILL NOT NULL,
     `resumen` mediumtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
     `texto` mediumtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
     `cod_idioma` varchar(5) COLLATE utf8mb4_unicode_520_ci DEFAULT 'es_ES'
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

   --
   -- Índices para tablas volcadas
   --

   --
   -- Indices de la tabla `dk_blog_cabeceras`
   --
   ALTER TABLE `dk_blog_cabeceras`
     ADD PRIMARY KEY (`id`),
     ADD KEY `id_categoria` (`id_categoria`),
     ADD KEY `fk_dk_blog_entradas_dk_votantes1_idx` (`id_incluido`);

   --
   -- Indices de la tabla `dk_blog_cabeceras_lang`
   --
   ALTER TABLE `dk_blog_cabeceras_lang`
     ADD PRIMARY KEY (`id`),
     ADD KEY `id_incluido` (`id_incluido`),
     ADD KEY `lenguajes_id` (`lenguajes_id`),
     ADD KEY `blog_cabeceras_id` (`blog_cabeceras_id`);

   --
   -- Indices de la tabla `dk_blog_categorias`
   --
   ALTER TABLE `dk_blog_categorias`
     ADD PRIMARY KEY (`id`),
     ADD KEY `fk_dk_blog_categrias_dk_votantes1_idx` (`id_incluido`) USING BTREE;

   --
   -- Indices de la tabla `dk_blog_categorias_lang`
   --
   ALTER TABLE `dk_blog_categorias_lang`
     ADD PRIMARY KEY (`id`),
     ADD KEY `fk_dk_categorias_lang_dk_lenguajes_idx` (`cod_idioma`),
     ADD KEY `fk_dk_blog_categorias_lang_dk_lenguajes1_idx` (`lenguajes_id`),
     ADD KEY `fk_dk_blog_categorias_lang_dk_blog_categorias1_idx` (`blog_categorias_id`),
     ADD KEY `fk_dk_blog_categorias_lang_dk_votantes1_idx` (`id_incluido`);

   --
   -- Indices de la tabla `dk_blog_entradas`
   --
   ALTER TABLE `dk_blog_entradas`
     ADD PRIMARY KEY (`id`),
     ADD KEY `id_categoria` (`id_categoria`),
     ADD KEY `fk_dk_blog_entradas_dk_votantes1_idx` (`id_incluido`);

   --
   -- Indices de la tabla `dk_blog_entradas_lang`
   --
   ALTER TABLE `dk_blog_entradas_lang`
     ADD PRIMARY KEY (`id`),
     ADD KEY `id_idioma` (`cod_idioma`),
     ADD KEY `fk_dk_blog_entradas_lang_dk_blog_entradas1_idx` (`blog_entradas_id`),
     ADD KEY `fk_dk_blog_entradas_lang_dk_lenguajes1_idx` (`lenguajes_id`),
     ADD KEY `fk_dk_blog_entradas_lang_dk_votantes1_idx` (`id_incluido`);

   --
   -- Indices de la tabla `dk_blog_estructura_inicio`
   --
   ALTER TABLE `dk_blog_estructura_inicio`
     ADD PRIMARY KEY (`id`);

   --
   -- Indices de la tabla `dk_blog_menu`
   --
   ALTER TABLE `dk_blog_menu`
     ADD PRIMARY KEY (`id`),
     ADD KEY `fk_dk_blog_menu_dk_votantes1_idx` (`id_incluido`);

   --
   -- Indices de la tabla `dk_blog_menu_lang`
   --
   ALTER TABLE `dk_blog_menu_lang`
     ADD PRIMARY KEY (`id`),
     ADD KEY `id_idioma` (`cod_idioma`),
     ADD KEY `fk_dk_blog_menu_lang_dk_blog_menu1_idx` (`blog_menu_id`),
     ADD KEY `fk_dk_blog_menu_lang_dk_lenguajes1_idx` (`lenguajes_id`),
     ADD KEY `fk_dk_blog_menu_lang_dk_votantes1_idx` (`id_incluido`);

   --
   -- Indices de la tabla `dk_blog_paginas`
   --
   ALTER TABLE `dk_blog_paginas`
     ADD PRIMARY KEY (`id`),
     ADD KEY `fk_dk_blog_paginas_dk_votantes1_idx` (`id_incluido`);

   --
   -- Indices de la tabla `dk_blog_paginas_lang`
   --
   ALTER TABLE `dk_blog_paginas_lang`
     ADD PRIMARY KEY (`id`),
     ADD KEY `id_idioma` (`cod_idioma`),
     ADD KEY `fk_dk_blog_paginas_lang_dk_lenguajes1_idx` (`lenguajes_id`),
     ADD KEY `fk_dk_blog_paginas_lang_dk_blog_paginas1_idx` (`blog_paginas_id`),
     ADD KEY `fk_dk_blog_paginas_lang_dk_votantes1_idx` (`id_incluido`);

   --
   -- Indices de la tabla `dk_candidatos_lang`
   --
   ALTER TABLE `dk_candidatos_lang`
     ADD PRIMARY KEY (`id`),
     ADD KEY `id_idioma` (`cod_idioma`),
     ADD KEY `fk_dk_candidatos_lang_dk_candidatos1_idx` (`candidatos_ID`),
     ADD KEY `fk_dk_candidatos_lang_dk_lenguajes1_idx` (`lenguajes_id`),
     ADD KEY `fk_dk_candidatos_lang_dk_votantes1_idx` (`id_incluido`);

   --
   -- Indices de la tabla `dk_debate_preguntas_lang`
   --
   ALTER TABLE `dk_debate_preguntas_lang`
     ADD PRIMARY KEY (`id`),
     ADD KEY `id_idioma` (`cod_idioma`),
     ADD KEY `fk_dk_debate_preguntas_lang_dk_debate_preguntas1_idx` (`debate_preguntas_ID`),
     ADD KEY `fk_dk_debate_preguntas_lang_dk_lenguajes1_idx` (`lenguajes_id`),
     ADD KEY `fk_dk_debate_preguntas_lang_dk_votantes1_idx` (`id_incluido`);

   --
   -- Indices de la tabla `dk_grupo_trabajo_lang`
   --
   ALTER TABLE `dk_grupo_trabajo_lang`
     ADD PRIMARY KEY (`id`),
     ADD KEY `id_idioma` (`cod_idioma`),
     ADD KEY `fk_dk_grupo_trabajo_lang_dk_lenguajes1_idx` (`lenguajes_id`),
     ADD KEY `fk_dk_grupo_trabajo_lang_dk_grupo_trabajo1_idx` (`grupo_trabajo_ID`),
     ADD KEY `fk_dk_grupo_trabajo_lang_dk_votantes1_idx` (`id_incluido`);

   --
   -- Indices de la tabla `dk_lenguajes`
   --
   ALTER TABLE `dk_lenguajes`
     ADD PRIMARY KEY (`id`),
     ADD UNIQUE KEY `cod_LP` (`cod_LP`) USING BTREE,
     ADD KEY `fk_dk_lenguajes_dk_lenguaje_cod_pais1_idx` (`lenguaje_cod_pais_id`);

   --
   -- Indices de la tabla `dk_lenguaje_cod_pais`
   --
   ALTER TABLE `dk_lenguaje_cod_pais`
     ADD PRIMARY KEY (`id`),
     ADD UNIQUE KEY `idx` (`id`),
     ADD UNIQUE KEY `cod_LP` (`cod_LP`) USING BTREE;

   --
   -- Indices de la tabla `dk_votacion_lang`
   --
   ALTER TABLE `dk_votacion_lang`
     ADD PRIMARY KEY (`id`),
     ADD KEY `id_idioma` (`cod_idioma`),
     ADD KEY `fk_dk_votacion_lang_dk_votacion1_idx` (`votacion_ID`),
     ADD KEY `fk_dk_votacion_lang_dk_lenguajes1_idx` (`lenguajes_id`),
     ADD KEY `fk_dk_votacion_lang_dk_votantes1_idx` (`id_incluido`);

   --
   -- AUTO_INCREMENT de las tablas volcadas
   --

   --
   -- AUTO_INCREMENT de la tabla `dk_blog_cabeceras`
   --
   ALTER TABLE `dk_blog_cabeceras`
     MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

   --
   -- AUTO_INCREMENT de la tabla `dk_blog_cabeceras_lang`
   --
   ALTER TABLE `dk_blog_cabeceras_lang`
     MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

   --
   -- AUTO_INCREMENT de la tabla `dk_blog_categorias`
   --
   ALTER TABLE `dk_blog_categorias`
     MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

   --
   -- AUTO_INCREMENT de la tabla `dk_blog_categorias_lang`
   --
   ALTER TABLE `dk_blog_categorias_lang`
     MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

   --
   -- AUTO_INCREMENT de la tabla `dk_blog_entradas`
   --
   ALTER TABLE `dk_blog_entradas`
     MODIFY `id` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;

   --
   -- AUTO_INCREMENT de la tabla `dk_blog_entradas_lang`
   --
   ALTER TABLE `dk_blog_entradas_lang`
     MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

   --
   -- AUTO_INCREMENT de la tabla `dk_blog_estructura_inicio`
   --
   ALTER TABLE `dk_blog_estructura_inicio`
     MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

   --
   -- AUTO_INCREMENT de la tabla `dk_blog_menu`
   --
   ALTER TABLE `dk_blog_menu`
     MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

   --
   -- AUTO_INCREMENT de la tabla `dk_blog_menu_lang`
   --
   ALTER TABLE `dk_blog_menu_lang`
     MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

   --
   -- AUTO_INCREMENT de la tabla `dk_blog_paginas`
   --
   ALTER TABLE `dk_blog_paginas`
     MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

   --
   -- AUTO_INCREMENT de la tabla `dk_blog_paginas_lang`
   --
   ALTER TABLE `dk_blog_paginas_lang`
     MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

   --
   -- AUTO_INCREMENT de la tabla `dk_candidatos_lang`
   --
   ALTER TABLE `dk_candidatos_lang`
     MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

   --
   -- AUTO_INCREMENT de la tabla `dk_debate_preguntas_lang`
   --
   ALTER TABLE `dk_debate_preguntas_lang`
     MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

   --
   -- AUTO_INCREMENT de la tabla `dk_grupo_trabajo_lang`
   --
   ALTER TABLE `dk_grupo_trabajo_lang`
     MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

   --
   -- AUTO_INCREMENT de la tabla `dk_lenguajes`
   --
   ALTER TABLE `dk_lenguajes`
     MODIFY `id` int(4) UNSIGNED NOT NULL AUTO_INCREMENT;

   --
   -- AUTO_INCREMENT de la tabla `dk_lenguaje_cod_pais`
   --
   ALTER TABLE `dk_lenguaje_cod_pais`
     MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

   --
   -- AUTO_INCREMENT de la tabla `dk_votacion_lang`
   --
   ALTER TABLE `dk_votacion_lang`
     MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

   --
   -- Restricciones para tablas volcadas
   --

   --
   -- Filtros para la tabla `dk_blog_cabeceras`
   --
   ALTER TABLE `dk_blog_cabeceras`
     ADD CONSTRAINT `fk_dk_blog_entradas_dk_votantes10` FOREIGN KEY (`id_incluido`) REFERENCES `dk_votantes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

   --
   -- Filtros para la tabla `dk_blog_cabeceras_lang`
   --
   ALTER TABLE `dk_blog_cabeceras_lang`
     ADD CONSTRAINT `dk_blog_cabeceras_lang_ibfk_1` FOREIGN KEY (`id_incluido`) REFERENCES `dk_votantes` (`ID`),
     ADD CONSTRAINT `dk_blog_cabeceras_lang_ibfk_2` FOREIGN KEY (`blog_cabeceras_id`) REFERENCES `dk_blog_cabeceras` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
     ADD CONSTRAINT `dk_blog_cabeceras_lang_ibfk_3` FOREIGN KEY (`lenguajes_id`) REFERENCES `dk_lenguajes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

   --
   -- Filtros para la tabla `dk_blog_categorias`
   --
  -- ALTER TABLE `dk_blog_categorias`
  --   ADD CONSTRAINT `fk_dk_blog_categorias_dk_votantes1_idx` FOREIGN KEY (`id_incluido`) REFERENCES `dk_votantes` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

   --
   -- Filtros para la tabla `dk_blog_categorias_lang`
   --
   ALTER TABLE `dk_blog_categorias_lang`
     ADD CONSTRAINT `fk_dk_blog_categorias_lang_dk_blog_categorias1` FOREIGN KEY (`blog_categorias_id`) REFERENCES `dk_blog_categorias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
     ADD CONSTRAINT `fk_dk_blog_categorias_lang_dk_lenguajes1` FOREIGN KEY (`lenguajes_id`) REFERENCES `dk_lenguajes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
     ADD CONSTRAINT `fk_dk_blog_categorias_lang_dk_votantes1` FOREIGN KEY (`id_incluido`) REFERENCES `dk_votantes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

   --
   -- Filtros para la tabla `dk_blog_entradas`
   --
   ALTER TABLE `dk_blog_entradas`
     ADD CONSTRAINT `fk_dk_blog_entradas_dk_votantes1` FOREIGN KEY (`id_incluido`) REFERENCES `dk_votantes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

   --
   -- Filtros para la tabla `dk_blog_entradas_lang`
   --
   ALTER TABLE `dk_blog_entradas_lang`
     ADD CONSTRAINT `fk_dk_blog_entradas_lang_dk_blog_entradas1` FOREIGN KEY (`blog_entradas_id`) REFERENCES `dk_blog_entradas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
     ADD CONSTRAINT `fk_dk_blog_entradas_lang_dk_lenguajes1` FOREIGN KEY (`lenguajes_id`) REFERENCES `dk_lenguajes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
     ADD CONSTRAINT `fk_dk_blog_entradas_lang_dk_votantes1` FOREIGN KEY (`id_incluido`) REFERENCES `dk_votantes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

   --
   -- Filtros para la tabla `dk_blog_menu`
   --
   ALTER TABLE `dk_blog_menu`
     ADD CONSTRAINT `fk_dk_blog_menu_dk_votantes1` FOREIGN KEY (`id_incluido`) REFERENCES `dk_votantes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

   --
   -- Filtros para la tabla `dk_blog_menu_lang`
   --
   ALTER TABLE `dk_blog_menu_lang`
     ADD CONSTRAINT `fk_dk_blog_menu_lang_dk_blog_menu1` FOREIGN KEY (`blog_menu_id`) REFERENCES `dk_blog_menu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
     ADD CONSTRAINT `fk_dk_blog_menu_lang_dk_lenguajes1` FOREIGN KEY (`lenguajes_id`) REFERENCES `dk_lenguajes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
     ADD CONSTRAINT `fk_dk_blog_menu_lang_dk_votantes1` FOREIGN KEY (`id_incluido`) REFERENCES `dk_votantes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

   --
   -- Filtros para la tabla `dk_blog_paginas`
   --
   ALTER TABLE `dk_blog_paginas`
     ADD CONSTRAINT `fk_dk_blog_paginas_dk_votantes1` FOREIGN KEY (`id_incluido`) REFERENCES `dk_votantes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

   --
   -- Filtros para la tabla `dk_blog_paginas_lang`
   --
   ALTER TABLE `dk_blog_paginas_lang`
     ADD CONSTRAINT `fk_dk_blog_paginas_lang_dk_blog_paginas1` FOREIGN KEY (`blog_paginas_id`) REFERENCES `dk_blog_paginas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
     ADD CONSTRAINT `fk_dk_blog_paginas_lang_dk_lenguajes1` FOREIGN KEY (`lenguajes_id`) REFERENCES `dk_lenguajes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
     ADD CONSTRAINT `fk_dk_blog_paginas_lang_dk_votantes1` FOREIGN KEY (`id_incluido`) REFERENCES `dk_votantes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

   --
   -- Filtros para la tabla `dk_candidatos_lang`
   --
   ALTER TABLE `dk_candidatos_lang`
     ADD CONSTRAINT `fk_dk_candidatos_lang_dk_candidatos1` FOREIGN KEY (`candidatos_ID`) REFERENCES `dk_candidatos` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
     ADD CONSTRAINT `fk_dk_candidatos_lang_dk_lenguajes1` FOREIGN KEY (`lenguajes_id`) REFERENCES `dk_lenguajes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
     ADD CONSTRAINT `fk_dk_candidatos_lang_dk_votantes1` FOREIGN KEY (`id_incluido`) REFERENCES `dk_votantes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

   --
   -- Filtros para la tabla `dk_debate_preguntas_lang`
   --
   ALTER TABLE `dk_debate_preguntas_lang`
     ADD CONSTRAINT `fk_dk_debate_preguntas_lang_dk_debate_preguntas1` FOREIGN KEY (`debate_preguntas_ID`) REFERENCES `dk_debate_preguntas` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
     ADD CONSTRAINT `fk_dk_debate_preguntas_lang_dk_lenguajes1` FOREIGN KEY (`lenguajes_id`) REFERENCES `dk_lenguajes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
     ADD CONSTRAINT `fk_dk_debate_preguntas_lang_dk_votantes1` FOREIGN KEY (`id_incluido`) REFERENCES `dk_votantes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

   --
   -- Filtros para la tabla `dk_grupo_trabajo_lang`
   --
   ALTER TABLE `dk_grupo_trabajo_lang`
     ADD CONSTRAINT `fk_dk_grupo_trabajo_lang_dk_grupo_trabajo1` FOREIGN KEY (`grupo_trabajo_ID`) REFERENCES `dk_grupo_trabajo` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
     ADD CONSTRAINT `fk_dk_grupo_trabajo_lang_dk_lenguajes1` FOREIGN KEY (`lenguajes_id`) REFERENCES `dk_lenguajes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
     ADD CONSTRAINT `fk_dk_grupo_trabajo_lang_dk_votantes1` FOREIGN KEY (`id_incluido`) REFERENCES `dk_votantes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

   --
   -- Filtros para la tabla `dk_lenguajes`
   --
   ALTER TABLE `dk_lenguajes`
     ADD CONSTRAINT `fk_dk_lenguajes_dk_lenguaje_cod_pais1` FOREIGN KEY (`lenguaje_cod_pais_id`) REFERENCES `dk_lenguaje_cod_pais` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

   --
   -- Filtros para la tabla `dk_votacion_lang`
   --
   ALTER TABLE `dk_votacion_lang`
     ADD CONSTRAINT `fk_dk_votacion_lang_dk_lenguajes1` FOREIGN KEY (`lenguajes_id`) REFERENCES `dk_lenguajes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
     ADD CONSTRAINT `fk_dk_votacion_lang_dk_votacion1` FOREIGN KEY (`votacion_ID`) REFERENCES `dk_votacion` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
     ADD CONSTRAINT `fk_dk_votacion_lang_dk_votantes1` FOREIGN KEY (`id_incluido`) REFERENCES `dk_votantes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;
   COMMIT;



   /*  datos para incluir */

   INSERT INTO `dk_blog_estructura_inicio` (`id`, `id_incluido`, `title`, `description`, `activo`, `bloque`, `orden`) VALUES
   (1, 0000000001, 'Cabecera de Inicio', 'Bloques de la cabecera de la pagina de inicio', 0, '12', NULL),
   (2, 0000000001, 'Cabecera otras páginas', 'Bloques de la cabecera de las páginas que no son de inicio', 0, '12', NULL),
   (3, 0000000001, 'Estructura de inicio', 'Orden de los distintos bloques en la página de inicio', 0, '15', NULL),
   (4, 0000000001, 'Estructura paginas superior', 'Estructura del resto de las páginas de la web esterior zona superior', 0, '', NULL),
   (11, 0000000001, 'Carrusel Cabecera', 'Cabecera tipo carrusel', 0, 'cabecera-carousel.php', NULL),
   (12, 0000000001, 'Cabecera fija', 'Cabecera fija con imagen estatica', 1, 'cabecera-fija.php', NULL),
   (13, 0000000001, 'Menú principal', 'Menú de navegación', 0, 'navbar.php', NULL),
   (14, 0000000001, 'Menú redes sociales', 'Menú de redes sociales', 0, 'social-navbar.php', NULL),
   (15, 0000000001, 'Login a la web', 'Bloque de acceso a la zona de votaciones', 1, 'login.php', NULL),
   (16, 0000000001, 'Acceso interventores', 'Bloque con el enlace al acceso de los interventores', 0, 'bloque_interventor.php', NULL),
   (18, 0000000001, 'Bloque texto azul', 'Bloque de texto con fondo azul', 0, 'section-bloktext-blue.php', NULL),
   (19, 0000000001, 'Bloque texto blanco', 'Bloque de texto con fondo blanco', 0, 'section-bloktext.php', NULL),
   (20, 0000000001, 'bloque noticias', 'Informaciones en vertical tipo noticias alternando imagen derecha e izquierda.', 0, 'section-noticias.php', NULL),
   (21, 0000000001, 'Bloque marqueting', 'Tres informaciones en horizontal con fondo azul.', 0, 'section-marqueting.php', NULL);

   INSERT INTO `dk_blog_categorias` (`id`, `id_incluido`, `titulo`, `texto`, `fecha`, `activo`) VALUES
   (1, 0000000001, 'Texto fondo Azul', NULL, '2021-02-16 16:37:17', 1),
   (2, 0000000001, 'Texto fondo blanco', NULL, '2021-02-16 16:37:17', 1),
   (3, 0000000001, 'Sección marketing', NULL, '2021-02-16 16:38:47', 1),
   (4, 0000000001, 'Sección noticias', NULL, '2021-02-16 20:02:35', 1),
   (5, 0000000001, 'Páginas', NULL, '2021-02-16 20:07:34', 1);

/*  Añadimos dos datos necesarios para que el administrador  sea adminsitrador del blog*/
   UPDATE `dk_votantes` SET `idioma`='es_ES',`admin_blog`=1 WHERE ID=1;

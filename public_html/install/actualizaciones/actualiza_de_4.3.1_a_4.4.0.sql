/*
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
*/

/*#############################################################################################################################################################
##                                                                                                                                                           ##
##                                       Scrip de actualización de demokratian version 4.3.1 a la versión 4.4.0                                              ##
##                                                                                                                                                           ##
#############################################################################################################################################################*/

/*  borramos los datos de dk_blog_estructura_inicios */

DELETE FROM `dk_blog_estructura_inicio`;

--
-- Volcado de datos para la tabla `dk_blog_estructura_inicio`
--

 INSERT INTO `dk_blog_estructura_inicio` (`id`, `id_incluido`, `title`, `description`, `activo`, `bloque`, `orden`) VALUES
 (1, 1, 'Cabecera de Inicio', 'Bloques de la cabecera de la pagina de inicio', 0, '12', NULL),
 (2, 1, 'Cabecera otras páginas', 'Bloques de la cabecera de las páginas que no son de inicio', 0, '12', NULL),
 (3, 1, 'Estructura de inicio', 'Orden de los distintos bloques en la página de inicio', 0, '15', NULL),
 (4, 1, 'Estructura paginas superior', 'Estructura del resto de las páginas de la web esterior zona superior', 0, '', NULL),
 (11, 1, 'Carrusel Cabecera', 'Cabecera tipo carrusel', 0, 'cabecera-carousel.php', NULL),
 (12, 1, 'Cabecera fija', 'Cabecera fija con imagen estatica', 1, 'cabecera-fija.php', NULL),
 (13, 1, 'Menú principal', 'Menú de navegació;n', 0, 'navbar.php', NULL),
 (14, 1, 'Menú redes sociales', 'Menú de redes sociales', 0, 'social-navbar.php', NULL),
 (15, 1, 'Login a la web', 'Bloque de acceso a la zona de votaciones', 1, 'login.php', NULL),
 (16, 1, 'Acceso interventores', 'Bloque con el enlace al acceso de los interventores', 0, 'bloque_interventor.php', NULL),
 (18, 1, 'Bloque texto azul', 'Bloque de texto con fondo azul', 0, 'section-bloktext-blue.php', NULL),
 (19, 1, 'Bloque texto blanco', 'Bloque de texto con fondo blanco', 0, 'section-bloktext.php', NULL),
 (20, 1, 'bloque noticias', 'Informaciones en vertical tipo noticias alternando imagen derecha e izquierda.', 0, 'section-noticias.php', NULL),
 (21, 1, 'Bloque marqueting', 'Tres informaciones en horizontal con fondo azul.', 0, 'section-marqueting.php', NULL);


 /*  borramos los datos de dk_lenguaje_cod_pais */

 DELETE FROM `dk_lenguaje_cod_pais`;

--
-- Volcado de datos para la tabla `dk_lenguaje_cod_pais`
--

INSERT INTO `dk_lenguaje_cod_pais` (`id`, `cod_LP`, `descripcion`) VALUES
(1, 'ar', 'árabe'),
(2, 'ar_AE', 'árabe [Emiratos Árabes Unidos]'),
(3, 'ar_BH', 'árabe [Bahráin]'),
(4, 'ar_DZ', 'árabe [Argelia]'),
(5, 'ar_EG', 'árabe [Egipto]'),
(6, 'ar_IQ', 'árabe [Iraq]'),
(7, 'ar_JO', 'árabe [Jordania]'),
(8, 'ar_KW', 'árabe [Kuwait]'),
(9, 'ar_LB', 'árabe [Líbano]'),
(10, 'ar_LY', 'árabe [Libia]'),
(11, 'ar_MA', 'árabe [Marruecos]'),
(12, 'ar_OM', 'árabe [Omán]'),
(13, 'ar_QA', 'árabe [Qatar]'),
(14, 'ar_SA', 'árabe [Arabia Saudita]'),
(15, 'ar_SD', 'árabe [Sudán]'),
(16, 'ar_SY', 'árabe [Siria]'),
(17, 'ar_TN', 'árabe [Túnez]'),
(18, 'ar_YE', 'árabe [Yemen]'),
(19, 'be', 'bielorruso'),
(20, 'be_BY', 'bielorruso [Bielorrusia]'),
(21, 'bg', 'búlgaro'),
(22, 'bg_BG', 'búlgaro [Bulgaria]'),
(23, 'ca', 'catalán'),
(24, 'ca_ES', 'catalán [España]'),
(25, 'cs', 'checo'),
(26, 'cs_CZ', 'checo [Chequia]'),
(27, 'da', 'danés'),
(28, 'da_DK', 'danés [Dinamarca]'),
(29, 'de', 'alemán'),
(30, 'de_AT', 'alemán [Austria]'),
(31, 'de_CH', 'alemán [Suiza]'),
(32, 'de_DE', 'alemán [Alemania]'),
(33, 'de_LU', 'alemán [Luxemburgo]'),
(34, 'el', 'griego'),
(35, 'el_CY', 'griego [Chipre]'),
(36, 'el_GR', 'griego [Grecia]'),
(37, 'en', 'inglés'),
(38, 'en_AU', 'inglés [Australia]'),
(39, 'en_CA', 'inglés [Canadá]'),
(40, 'en_GB', 'inglés [Reino Unido]'),
(41, 'en_IE', 'inglés [Irlanda]'),
(42, 'en_IN', 'inglés [India]'),
(43, 'en_MT', 'inglés [Malta]'),
(44, 'en_NZ', 'inglés [Nueva Zelanda]'),
(45, 'en_PH', 'inglés [Filipinas]'),
(46, 'en_SG', 'inglés [Singapur]'),
(47, 'en_US', 'inglés [Estados Unidos]'),
(48, 'en_ZA', 'inglés [Sudáfrica]'),
(49, 'es', 'español'),
(50, 'es_AR', 'español [Argentina]'),
(51, 'es_BO', 'español [Bolivia]'),
(52, 'es_CL', 'español [Chile]'),
(53, 'es_CO', 'español [Colombia]'),
(54, 'es_CR', 'español [Costa Rica]'),
(55, 'es_DO', 'español [República Dominicana]'),
(56, 'es_EC', 'español [Ecuador]'),
(57, 'es_ES', 'español [España]'),
(58, 'es_GT', 'español [Guatemala]'),
(59, 'es_HN', 'español [Honduras]'),
(60, 'es_MX', 'español [México]'),
(61, 'es_NI', 'español [Nicaragua]'),
(62, 'es_PA', 'español [Panamá]'),
(63, 'es_PE', 'español [Perú]'),
(64, 'es_PR', 'español [Puerto Rico]'),
(65, 'es_PY', 'español [Paraguay]'),
(66, 'es_SV', 'español [El Salvador]'),
(67, 'es_US', 'español [Estados Unidos]'),
(68, 'es_UY', 'español [Uruguay]'),
(69, 'es_VE', 'español [Venezuela]'),
(70, 'et', 'estonio'),
(71, 'et_EE', 'estonio [Estonia]'),
(72, 'fi', 'finés'),
(73, 'fi_FI', 'finés [Finlandia]'),
(74, 'fr', 'francés'),
(75, 'fr_BE', 'francés [Bélgica]'),
(76, 'fr_CA', 'francés [Canadá]'),
(77, 'fr_CH', 'francés [Suiza]'),
(78, 'fr_FR', 'francés [Francia]'),
(79, 'fr_LU', 'francés [Luxemburgo]'),
(80, 'ga', 'irlandés'),
(81, 'ga_IE', 'irlandés [Irlanda]'),
(82, 'hi_IN', 'hindú [India]'),
(83, 'hr', 'croata'),
(84, 'hr_HR', 'croata [Croacia]'),
(85, 'hu', 'húngaro'),
(86, 'hu_HU', 'húngaro [Hungría]'),
(87, 'in', 'indonesio'),
(88, 'in_ID', 'indonesio [Indonesia]'),
(89, 'is', 'islandés'),
(90, 'is_IS', 'islandés [Islandia]'),
(91, 'it', 'italiano'),
(92, 'it_CH', 'italiano [Suiza]'),
(93, 'it_IT', 'italiano [Italia]'),
(94, 'iw', 'hebreo'),
(95, 'iw_IL', 'hebreo [Israel]'),
(96, 'ja', 'japonés'),
(97, 'ja_JP', 'japonés [Japón]'),
(99, 'ko', 'coreano'),
(100, 'ko_KR', 'coreano [Corea del Sur]'),
(101, 'lt', 'lituano'),
(102, 'lt_LT', 'lituano [Lituania]'),
(103, 'lv', 'letón'),
(104, 'lv_LV', 'letón [Letonia]'),
(105, 'mk', 'macedonio'),
(106, 'mk_MK', 'macedonio [Macedonia]'),
(107, 'ms', 'malayo'),
(108, 'ms_MY', 'malayo [Malasia]'),
(109, 'mt', 'maltés'),
(110, 'mt_MT', 'maltés [Malta]'),
(111, 'nl', 'neerlandés'),
(112, 'nl_BE', 'neerlandés [Bélgica]'),
(113, 'nl_NL', 'neerlandés [Holanda]'),
(114, 'no', 'noruego'),
(115, 'no_NO', 'noruego [Noruega]'),
(117, 'pl', 'polaco'),
(118, 'pl_PL', 'polaco [Polonia]'),
(119, 'pt', 'portugués'),
(120, 'pt_BR', 'portugués [Brasil]'),
(121, 'pt_PT', 'portugués [Portugal]'),
(122, 'ro', 'rumano'),
(123, 'ro_RO', 'rumano [Rumania]'),
(124, 'ru', 'ruso'),
(125, 'ru_RU', 'ruso [Rusia]'),
(126, 'sk', 'eslovaco'),
(127, 'sk_SK', 'eslovaco [Eslovaquia]'),
(128, 'sl', 'eslovenio'),
(129, 'sl_SI', 'eslovenio [Eslovenia]'),
(130, 'sq', 'albanés'),
(131, 'sq_AL', 'albanés [Albania]'),
(132, 'sr', 'serbio'),
(133, 'sr_BA', 'serbio [Bosnia y Hercegovina]'),
(134, 'sr_CS', 'serbio [Serbia y Montenegro]'),
(135, 'sr_ME', 'serbio [Montenegro]'),
(136, 'sr_RS', 'serbio [Serbia]'),
(137, 'sv', 'sueco'),
(138, 'sv_SE', 'sueco [Suecia]'),
(139, 'th', 'tailandés'),
(140, 'th_TH', 'tailandés [Tailandia]'),
(142, 'tr', 'turco'),
(143, 'tr_TR', 'turco [Turquía]'),
(144, 'uk', 'ucranio'),
(145, 'uk_UA', 'ucranio [Ucrania]'),
(146, 'vi', 'vietnamita'),
(147, 'vi_VN', 'vietnamita [Vietnam]'),
(148, 'zh', 'chino'),
(149, 'zh_CN', 'chino [China]'),
(150, 'zh_HK', 'chino [Hong Kong]'),
(151, 'zh_SG', 'chino [Singapur]'),
(152, 'zh_TW', 'chino [Taiwán]');

/*
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
*/

/*#############################################################################################################################################################
##                                                                                                                                                           ##
##                                       Scrip de actualización de demokratian version 4.0.1 a la versión 4.3.1                                              ##
##                                                                                                                                                           ##
#############################################################################################################################################################*/

/*dk_blog_cabecerass*/
ALTER TABLE dk_blog_cabeceras
   ADD COLUMN activo SMALLINT(1) NOT NULL DEFAULT 0 AFTER zona_pagina;

ALTER TABLE dk_blog_cabeceras
   ADD COLUMN orden SMALLINT(2) NULL DEFAULT NULL;

/*dk_blog_categorias*/
   ALTER TABLE dk_blog_categorias
      DROP FOREIGN KEY `fk_dk_blog_categorias_dk_votantes1_idx`;

   ALTER TABLE dk_blog_categorias
      MODIFY COLUMN activo SMALLINT(1)
                      NOT NULL
                      DEFAULT 0
                      COMMENT '1 para categoría básica no borrable';
   DROP INDEX `fk_dk_blog_categrias_dk_votantes1_idx` ON dk_blog_categorias;



/*  borramos los datos de dk_blog_estructura_inicio */

DELETE FROM `dk_blog_estructura_inicio`;

/*  borramos los datos de dk_blog_categorias */

DELETE FROM `dk_blog_categorias`;

 /*  datos para incluir */

 INSERT INTO `dk_blog_estructura_inicio` (`id`, `id_incluido`, `title`, `description`, `activo`, `bloque`, `orden`) VALUES
 (1, 0000000001, 'Cabecera de Inicio', 'Bloques de la cabecera de la pagina de inicio', 0, '12', NULL),
 (2, 0000000001, 'Cabecera otras páginas', 'Bloques de la cabecera de las páginas que no son de inicio', 0, '12', NULL),
 (3, 0000000001, 'Estructura de inicio', 'Orden de los distintos bloques en la página de inicio', 0, '15', NULL),
 (4, 0000000001, 'Estructura paginas superior', 'Estructura del resto de las páginas de la web esterior zona superior', 0, '', NULL),
 (11, 0000000001, 'Carrusel Cabecera', 'Cabecera tipo carrusel', 0, 'cabecera-carousel.php', NULL),
 (12, 0000000001, 'Cabecera fija', 'Cabecera fija con imagen estatica', 1, 'cabecera-fija.php', NULL),
 (13, 0000000001, 'Menú principal', 'Menú de navegación', 0, 'navbar.php', NULL),
 (14, 0000000001, 'Menú redes sociales', 'Menú de redes sociales', 0, 'social-navbar.php', NULL),
 (15, 0000000001, 'Login a la web', 'Bloque de acceso a la zona de votaciones', 1, 'login.php', NULL),
 (16, 0000000001, 'Acceso interventores', 'Bloque con el enlace al acceso de los interventores', 0, 'bloque_interventor.php', NULL),
 (18, 0000000001, 'Bloque texto azul', 'Bloque de texto con fondo azul', 0, 'section-bloktext-blue.php', NULL),
 (19, 0000000001, 'Bloque texto blanco', 'Bloque de texto con fondo blanco', 0, 'section-bloktext.php', NULL),
 (20, 0000000001, 'bloque noticias', 'Informaciones en vertical tipo noticias alternando imagen derecha e izquierda.', 0, 'section-noticias.php', NULL),
 (21, 0000000001, 'Bloque marqueting', 'Tres informaciones en horizontal con fondo azul.', 0, 'section-marqueting.php', NULL);

 INSERT INTO `dk_blog_categorias` (`id`, `id_incluido`, `titulo`, `texto`, `fecha`, `activo`) VALUES
 (1, 0000000001, 'Texto fondo Azul', NULL, '2021-02-16 16:37:17', 1),
 (2, 0000000001, 'Texto fondo blanco', NULL, '2021-02-16 16:37:17', 1),
 (3, 0000000001, 'Sección marketing', NULL, '2021-02-16 16:38:47', 1),
 (4, 0000000001, 'Sección noticias', NULL, '2021-02-16 20:02:35', 1),
 (5, 0000000001, 'Páginas', NULL, '2021-02-16 20:07:34', 1);

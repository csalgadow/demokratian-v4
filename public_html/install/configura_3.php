<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
//error_reporting(E_ALL);
//ini_set('display_errors', '1');
require_once("../private/config/config.inc.php");
require_once("../private//inc_web/conexion.php");
include_once("../private/basicos_php/basico.php");


$user = fn_filtro($con, $_POST['user']);
$email = fn_filtro($con, $_POST['email']);
$pass = fn_filtro($con, $_POST['pass']);
$pass2 = fn_filtro($con, $_POST['pass2']);
$username = fn_filtro($con, $_POST['username']);
$username2 = fn_filtro($con, $_POST['username2']);
$nif = fn_filtro($con, $_POST['nif']);
$ides = explode("#", $_POST['provincia']);
$provincia = fn_filtro($con, $ides[0]);
$id_ccaa = fn_filtro($con, $ides[1]);
if (isset($_POST['id_municipio'])) {
    $id_municipio = fn_filtro($con, $_POST['id_municipio']);
}
$autenticacion_solo_local = $_POST['autenticacion_solo_local'];
$dir_simplesaml = $_POST['dir_simplesaml'];
$crear_usuarios_automaticamente = $_POST['crear_usuarios_automaticamente'];
$tipo_usuario = fn_filtro($con, $_POST['tipo_usuario']);

$errores = "";
$incluir_datos=false;

function show_error($error) {
    echo "ERROR#" . $error;
    die;
}

function show_ok($msg) {
    echo "OK#" . $msg;
    die;
}

if ($_POST['provincia'] == 'Escoja_una#error ') {
    show_error("No ha seleccionado ninguna provincia");
}

function comprobar_nombre_usuario($nombre_usuario) {
    //compruebo que el tamaño del string sea válido.
    if (strlen($nombre_usuario) < 4 || strlen($nombre_usuario) > 21) {

        $error = "error1";
        return $error;
    }

    //compruebo que los caracteres sean los permitidos
    $permitidos = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_";
    for ($i = 0; $i < strlen($nombre_usuario); $i++) {
        if (strpos($permitidos, substr($nombre_usuario, $i, 1)) === false) {
            $error = "error2";
            return $error;
        }
    }
    //echo $nombre_usuario . " es válido<br>";
    return $nombre_usuario;
}

if ($autenticacion_solo_local == 'false' && !file_exists($dir_simplesaml . "/lib/_autoload.php")) {
    show_error("La instalación de simplesamlphp no es correcta (" . $dir_simplesaml . ") ");
}

$nombre_usuario_new = comprobar_nombre_usuario($user);

if ($nombre_usuario_new == "error1") {
    $errores = "El nombre de usuario" . $nombre_usuario . " no es válido<br/> Tiene que tener entre 5 y 20 caracteres <br/>";


    //return false;
} elseif ($nombre_usuario_new == "error2") {
    $errores = "El nombre de usuario" . $nombre_usuario . " no es válido, esta mal formado, no puede tener espacios en blanco, acentos, ñ ...<br/>";
} else {

    if ($pass != $pass2) {
        $errores = "Hay un error, los password eran distintos.";
    } else {


        /// miramos si ese nombre de usuario se esta usando
        $result_usu = mysqli_query($con, "SELECT usuario FROM $tbn9 WHERE usuario='$nombre_usuario_new'") or die("No se pudo realizar la consulta a la Base de datos");
        $quants_usu = mysqli_num_rows($result_usu);

        if ( $quants_usu == 0) {
            $passw = password_hash($pass, PASSWORD_BCRYPT);
            //$passw = md5($pass);
            $nivel_usuario = 2; // nivel de administrador
            $nivel_acceso = 0; //maximo nivel de administrador
            $tipo_votante = 1; //usuario tipo socio
            $admin_blog =1; // le hacemos administrador del blog
            $ID = 000001; //identificador para que solo se pueda crear el 1

            $insql = "insert into $tbn9 (ID,pass,id_provincia,usuario,correo_usuario,tipo_votante,nivel_acceso,nivel_usuario,nombre_usuario,apellido_usuario,id_ccaa,nif,id_municipio,admin_blog) values (  \"$ID\", \"$passw\",  \"$provincia\", \"$nombre_usuario_new\", \"$email\", \"$tipo_votante\", \"$nivel_acceso\", \"$nivel_usuario\", \"$username\", \"$username2\",\"$id_ccaa\",\"$nif\", \" $id_municipio\", \" $admin_blog\" )";
            mysqli_query($con, $insql);
            if (mysqli_error($con)) {

              if (mysqli_error($con)=="Duplicate entry '0000000001' for key 'PRIMARY'"){
                $errores = " Ya hay un usuario administrador en la base de datos por lo que no se puede incluir otro";
              }else{
                //$errores = "Error al incluir datos: " . mysqli_error($con);
                $errores = "Error al incluir datos: "; //quitamos la razon del error para incluir los datos por seguridad. Pasar cuando se pueda a qgenerar log de errores
              }
            } else {
              $incluir_datos=true;
                $correcto = " Sus datos han sido incluidos <br/> Ya puede acceder al sistema de votaciones";
            }
        } else {
            $errores = "Este nombre de usuario ya se esta usando, cambielo, por favor";
        }
    }
}


if($incluir_datos==true){

$file = "../private/config/config.inc.php"; //archivo que hay que modificar
$fh = fopen($file, 'r+');
$string1 = "
###################################################################################################################################
##########                                               Autenticacion                                                   ##########
###################################################################################################################################
\$cfg_autenticacion_solo_local = " . $autenticacion_solo_local . ";    // ¿Permitimos autenticacion federada? cfg_autenticacion_solo_local==false -> SI
\$cfg_dir_simplesaml=\"" . $dir_simplesaml . "\";                    // Directorio donde hemos instalado simplesamlphp en modo SP.
\$cfg_crear_usuarios_automaticamente = " . $crear_usuarios_automaticamente . ";    // Cuando tenemos autenticacion federada, ¿creamos usuarios automaticamente?
\$cfg_tipo_usuario = " . $tipo_usuario . ";    // Al crear automaticamente, el tipo por defecto

?>";

if ($autenticacion_solo_local == 'false') {
    $string1 .= "if ( \$cfg_autenticacion_solo_local == false ) { require_once(\"" . $dir_simplesaml . "/lib/_autoload.php\"); } \n?>";
}


fseek($fh, -3, SEEK_END); // nos vamos 3 caraceres antes para quitar el cierre de php
fwrite($fh, $string1) or show_error("Could not write file!");
fclose($fh);  //  Cerramos el fichero

}

if ($errores != "") {
    show_error($errores . " ");
}

if ($errores == "") {

    show_ok("<div class=\"alert alert-success\">
              <a class=\"close\" data-dismiss=\"alert\">x</a>" . $correcto . " Se han guardado los datos correctamente
</div>");
}
?>

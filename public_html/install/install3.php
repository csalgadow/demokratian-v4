<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
//error_reporting(E_ALL);
//ini_set('display_errors', '1');
require_once("../../private/config/config.inc.php");
require_once("../../private/inc_web/conexion.php");
?>
<!DOCTYPE html>
<html lang="es">
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content=" ">
      <meta name="keywords" content=""/>
      <meta name="language" content="es">
      <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
      <meta http-equiv="pragma" CONTENT="no-cache">
      <meta http-equiv="Pragma" content="no-cache">
      <meta http-equiv="Expires" content="0">
        <title>Sistema de instalación de DEMOKRATIAN</title>
        <meta name="author" content="Carlos Salgado">
        <link rel="icon"  type="image/png"  href="../temas/demokratian/images/icono.png">




        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../assets/bootstrap-4.5.0/css/bootstrap.min.css" rel="stylesheet">
        <!--    <link href="temas/emokratian/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">-->
        <link href="../temas/demokratian/css/blogStyle.css" rel="stylesheet">
        <!--<link href="temas/emokratian/estilo_login.css" rel="stylesheet">-->
    </head>

    <body>

        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="cabecera_instalacion.png" class="img-fluid" alt="Logo DEMOKRATIAN">

            </div>

            <!-- END cabecera
            ================================================== -->
            <div id="success"> </div> <!-- mensajes -->

            <div class="row" id="1_fase1">
                <div class="col-lg-6">
                    <div class="well">
                        <h2>Instalador del sistema de votaciones DEMOKRATIAN</h2>


                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="well">
                         <div class="alert alert-danger">
                           <p>Recuerde,<strong> Ahora cuando termine, debe borrar la carpeta "install" y todos los archivos incluidos en el </strong>de su servidor para evitar problemas.</p>
                           <p>Si no lo hace puede tener problemas de seguridad y será su responsabilidad. </p>
                         </div>

                    </div>
                </div>
            </div>
            <?php
            $sql = "SELECT ID FROM $tbn9";
            $result = mysqli_query($con, $sql);

            if ($row = mysqli_fetch_array($result)) {
                ?>
                <div class="row" id="2_fase1">
                    <div class="col-lg-12">
                        <div class="well">

                            <div class="alert alert-danger"> Upss!!!! hay algún error, ya existe un usuario y no se pueden crear más. Recuerde que si ya ha realizado la instalación tendría que haber borrado la carpeta install</div>

                        </div>
                    </div>
                </div>
                <?php
            } else {
                ?>


                <div class="row" id="2_fase1">
                    <div class="col-lg-12">
                        <div class="well">
                            <form  method="post" name="FormBBDD"  class="form-horizontal" id="FormBBDD" role="form" >
                                <h3 class="form-signin-heading">Tercer paso</h3>
                                <fieldset>
                                <legend>Datos del administrador</legend>


                                <div id="success2"> </div>


                                <div class="row form-group">
                                    <label for="user" class="col-sm-4 control-label">Usuario  administrador</label>
                                    <div class="col-sm-4">
                                        <input name="user" id="user" type="text" class="form-control" size="25"  placeholder="demokratian" required  autofocus /></td>
                                    </div>
                                    <div class="col-sm-4">Entre 5 y 20 caracteres. No se pueden dejar espacios en blanco ni usar acentos o la ñ </div>
                                </div>

                                <div class="row form-group">
                                    <label for="username" class="col-sm-4 control-label">Nombre  del administrador</label>
                                    <div class="col-sm-4">
                                        <input name="username" id="username" type="text" class="form-control" size="25" placeholder="Nombre del usuario" required  autofocus />
                                    </div>
                                    <div class="col-sm-4">

                                    </div>
                                </div>

                                <div class="row form-group">
                                    <label for="username" class="col-sm-4 control-label">Apellidos del administrador</label>
                                    <div class="col-sm-4">
                                        <input name="username2" id="username2" type="text" class="form-control" size="25" placeholder="Apellidos del usuario" required  autofocus />
                                    </div>
                                    <div class="col-sm-4">

                                    </div>
                                </div>

                                <div class="row form-group">
                                    <label for="pass" class="col-sm-4 control-label">Contraseña</label>
                                    <div class="col-sm-4">
                                        <input name="pass" id="pass" type="text" class="form-control"  size="25" placeholder="contraseña"  autofocus />
                                    </div>
                                    <div class="col-sm-4">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <label for="pass2" class="col-sm-4 control-label">Contraseña</label>
                                    <div class="col-sm-4">
                                        <input name="pass2" id="pass2" type="text" class="form-control"  size="25" placeholder="contraseña"  autofocus />
                                    </div>
                                    <div class="col-sm-4">  Vuelva a escribir su contraseña
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <label for="email" class="col-sm-4 control-label">Correo electronico</label>
                                    <div class="col-sm-4">
                                        <input type="email" class="form-control" placeholder="Su correo electronico" id="email"  name="email" size="25" required  data-validation-required-message="Por favor, ponga su correo electronico" />

                                    </div>
                                    <div class="col-sm-4"></div>
                                </div>

                                <div class="row form-group">
                                    <label for="nif" class="col-sm-4 control-label">NIF</label>
                                    <div class="col-sm-4">
                                        <input name="nif" id="nif" type="text" class="form-control"  size="25" placeholder="Su nif"  autofocus />
                                    </div>
                                    <div class="col-sm-4">
                                    </div>
                                </div>
                                <?php
                                require_once("../../private/config/config.inc.php");
                                if ($es_municipal == false) {
                                    ?>
                                    <div class="row form-group">
                                        <label for="dbhost" class="col-sm-4 control-label">Provincia en la que participa el administrador</label>
                                        <div class="col-sm-4">
                                            <?php
                                            $tbn8 = $extension . "provincia";
                                            $lista = "";
                                            $con = @mysqli_connect("$host", "$hostu", "$hostp") or die("no se puede conectar");
                                            mysqli_set_charset($con, "utf8");
                                            $db = @mysqli_select_db($con, "$dbn") or die("no se puede acceder a la tabla");
                                            // listar para meter en una lista del tipo enlace
                                            $activo = 0;


                                            $options = "select DISTINCT id, provincia, id_ccaa from $tbn8  order by ID";
                                            $resulta = mysqli_query($con, $options) or die("error: " . mysqli_error($con));

                                            while ($listrows = mysqli_fetch_array($resulta)) {
                                                $id_pro = $listrows['id'];
                                                $name1 = $listrows['provincia'];
                                                $id_ccaa = $listrows['id_ccaa'];

                                                $lista .= "<option value=\"" . $id_pro . "#" . $id_ccaa . "\" > $name1</option>";
                                            }
                                            ?>

                                            <div class="form-group">
                                                <select name="provincia" class="form-control custom-select"  id="provincia" >
                                                    <option value="Escoja_una#error " > Seleccione una provincia</option>";
                                                    <?php echo "$lista"; ?>
                                                </select>



                                            </div>
                                        </div>

                                        <div class="col-sm-4">

                                        </div>
                                    </div>

                                    <div id="municipio">  </div>

                                    <?php
                                } else {
                                    ?>
                                    <input name="provincia" id="provincia" type="hidden" value="001#016">
                                    <input name="id_municipio" id="id_municipio"  type="hidden" value="1">
                                    <?php
                                }
                                ?>
</fieldset>
                                <fieldset>
                                    <legend>Configuración de la autenticación</legend>

                                    <div class="row form-group">
                                        <label for="autenticacion_solo_local" class="col-sm-4 control-label">Autenticación</label>
                                        <div class="col-sm-4">

                                            <label>
                                                <input type="radio" name="autenticacion_solo_local" value="true" onClick="deshabilita_local()" checked="CHECKED">
                                                Local</label>

                                            <label>
                                                <input type="radio" name="autenticacion_solo_local" value="false" onClick="habilita_local()">
                                                Federada</label>
                                            <br>

                                        </div>
                                        <div class="col-sm-4">
                                            Elige federada si quieres que los usuarios se autentiquen en otra plataforma (por ejemplo joomla/drupal como IdP, o cualquier otro compatible con SAML2). El usuario
                                            de administración siempre usará la autenticación local, independientemente de esta opción. Por defecto <strong>Local</strong>.
                                        </div>
                                    </div>
                                    <div id="div_local"  class="caja_de_display" style="display:none" >
                                        <div class="row form-group">
                                            <label for="dir_simplesaml" class="col-sm-4 control-label">Directorio de instalación de simplesamlphp</label>
                                            <div class="col-sm-4">
                                                <input name="dir_simplesaml" type="text"  class="form-control" id="dir_simplesaml"  placeholder="/var/www/html/simplesamlphp" size="25"  autofocus />
                                            </div><div id="municipio">  </div>
                                            <div class="col-sm-4">
                                                SimpleSamlPhp en modo SP. Si eliges autenticación federada sigue <a href="https://bitbucket.org/csalgadow/demokratian_2.3.x/wiki/Demokratian%20como%20Service%20Provider" target="_otra">esta guía</a> y completa al menos el paso 1 antes de continuar.
                                            </div>
                                        </div>


                                        <div class="row form-group">
                                            <label for="crear_usuarios_automaticamente" class="col-sm-4 control-label">Crear usuarios al autenticarse</label>
                                            <div class="col-sm-4">

                                                <label>
                                                    <input type="radio" name="crear_usuarios_automaticamente" value="false">
                                                    No</label>

                                                <label>
                                                    <input type="radio" name="crear_usuarios_automaticamente" value="true"  checked="CHECKED">
                                                    Si</label>
                                                <br>

                                            </div>
                                            <div class="col-sm-4">
                                                Crear usuarios automáticamente después de autenticarse (sólo votantes, para los campos "usuario" y "correo_usuario"). Por defecto <strong>Si</strong>.
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <label for="tipo_usuario" class="col-sm-4 control-label">Tipo de usuario por defecto</label>
                                            <div class="col-sm-4">

                                                <label>
                                                    <input type="radio" name="tipo_usuario" value="1" checked="CHECKED">
                                                    Socio</label>
                                                <br>

                                                <label>
                                                    <input type="radio" name="tipo_usuario" value="2" >
                                                    simpatizantes verificado</label>
                                                <br>

                                                <label>
                                                    <input type="radio" name="tipo_usuario" value="3" >
                                                    simpatizantes</label>
                                                <br>

                                            </div>
                                            <div class="col-sm-4">
                                                Tipo de usuario cuando se crea automáticamente. Por defecto <strong>Socio</strong>.
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>



                                <div class="row form-group">
                                    <div class="col-sm-12">
                                        <button class="btn btn-ttc btn-lg btn-primary btn-block" type="submit">Configurar Administrador</button>
                                    </div>
                                </div>

                            </form>

                        </div>

                    </div>

                </div>

                <?php
            }
            ?>
            <div id="cargando" > <img class="cargador" src='../temas/demokratian/images/loading.gif'/> <div  class="cargador2" > <h4>Esto puede tardar unos minutos...</h4></div>
            </div>
            <!--segunda fase de la configuración-->
            <div class="row" id="final_fase"> <div class="col-lg-12"> <br/>
                    <p class="bg-success"> Ha terminado con la instalación, todo parece haber ido bien, asi que deberia poder acceder a DEMOKRATIAN <a href="../index.php" class="btn btn-primary btn-lg active" role="button">Acceder</a></p>
                    <br/>
                    <div class="alert alert-danger">Recuerde borrar la carpeta <strong>"install" </strong>de su servidor para evitar problemas.</div>
                </div>
            </div>
            <!--fin segunda fase-->

            <div id="footer" class="row">
                <div  class="pie_demokratia">

                    <div class="pie_demokratia2">
                        <a href="http://www.demokratian.org" target="_blank"><img src="../temas/demokratian/images/logo_pie.png" class="img-fluid"  alt="DEMOKRATIA | plataforma de votaciones"  /></a>
                    </div>
                    <div class="pie_demokratia1"> </div>

                </div>
            </div>




        </div>

        <script
          src="https://code.jquery.com/jquery-3.5.1.min.js"
          integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
          crossorigin="anonymous" ></script>
        <script>
          window.jQuery || document.write('<script src="../assets/jquery-3.5.1/jquery.min.js"><\/script>')
        </script>
        <script type="text/javascript">
              $(document).ready(function () {
                $("#final_fase").hide();
                $("#cargando").hide();
                  });

              $("#cargando").on("ajaxStart", function () {
                  $(this).show();
                }).on("ajaxStop", function () {
                  $(this).hide();
              });
        </script>
   <!--<script src="js/jquery.validate.js"></script>-->
        <script src="../assets/bootstrap-4.5.0/js/bootstrap.min.js" ></script>
        <script src="configura_3.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#provincia').change(function () {
                  var id_provincia = $('#provincia').val();
                    $('#municipio').load('genera_select.php?id_provincia=' + id_provincia);
                  //  $("#municipio").html(data);
                  });
                });
        </script>

        <script type='text/javascript'>
            function habilita_local() {
                $('#div_local').show('slow');
            }
            function deshabilita_local() {
                  $('#div_local').hide('slow')
            }

        </script>
    </body>
</html>

<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
//error_reporting(E_ALL);
//ini_set('display_errors', '1');
?>
<!DOCTYPE html>
<html lang="es">
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content=" ">
      <meta name="keywords" content=""/>
      <meta name="language" content="es">
      <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
      <meta http-equiv="pragma" CONTENT="no-cache">
      <meta http-equiv="Pragma" content="no-cache">
      <meta http-equiv="Expires" content="0">
        <title>Sistema de instalación de DEMOKRATIAN</title>
        <meta name="author" content="Carlos Salgado">
        <link rel="icon"  type="image/png"  href="../temas/demokratian/images/icono.png">




        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../assets/bootstrap-4.5.0/css/bootstrap.min.css" rel="stylesheet">
        <!--    <link href="temas/emokratian/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">-->
        <link href="../temas/demokratian/css/blogStyle.css" rel="stylesheet">
        <!--<link href="temas/emokratian/estilo_login.css" rel="stylesheet">-->
    </head>

    <body>

        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="cabecera_instalacion.png" class="img-fluid" alt="Logo DEMOKRATIAN">

            </div>

            <!-- END cabecera
            ================================================== -->
            <div id="success"> </div> <!-- mensajes -->

            <div class="row" id="1_fase1">
                <div class="col-lg-6">
                    <div class="well">
                        <h2>Instalador del sistema de votaciones DEMOKRATIAN</h2>


                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="well">
                        <p> <div class="alert alert-danger">Recuerde borrar la carpeta <strong>"install" </strong>de su servidor una vez que haya terminado la instalación para evitar problemas.</div></p>

                    </div>
                </div>
            </div>



            <div class="row" id="2_fase1">
                <div class="col-lg-12">
                    <div class="well">
                        <form  method="post"  class="form-horizontal" role="form" name="FormBBDD" id="FormBBDD" >


                            <div class="alert alert-info"> Tranquil@ , que estos datos, salvo los datos de tipo de servicio, podra modificarlos posteriormente mediante el sistema de administracion de DEMOKRATIAN </div>

                            <div id="success2"> </div>
                            <h3 class="form-signin-heading">Segundo paso</h3>
                            <div class="card">
                              <div class="card-body">

                            <fieldset>
                                <legend>Datos del tipo de servicio</legend>
                                <div class="row form-group">

                                    <label for="Correo_insti" class="col-sm-4 control-label">Usar correos corporativos</label>
                                    <div class="col-sm-4">

                                        <p>
                                            <label>
                                                <input name="correo_insti" type="radio" id="correo_insti_0" value="false" onClick="deshabilita_local()" checked="CHECKED">
                                                NO
                                            </label>
                                        </p>
                                        <p>
                                            <label>
                                                <input name="correo_insti" type="radio" id="correo_insti_1" value="true" onClick="habilita_local()">
                                                SI
                                            </label>


                                            (Si usa esta opción, usara circunscripcion unica)</p>
                                    </div>
                                    <div class="col-sm-4">
                                        <p>Permite votar usando correos corporativos o institucionales de una empresa, ong, universidad, etc sin necesidad de registro previo por el administrador (solo podran votar los que tengan un correo de ese tipo). </p>
                                    </div>
                                </div>



                                <div id="div_local"  class="caja_de_display" style="display:none" >
                                    <div class="row form-group">
                                        <label for="elCorreo_insti" class="col-sm-4 control-label"> </label>
                                        <div class="col-sm-4">
                                            <input type="text" name="elCorreo_insti" id="elCorreo_insti" class="form-control" size="25"  placeholder="@midominio.es"  autofocus>
                                        </div>
                                        <div class="col-sm-4">
                                            <p>dominio del correo incluyendo la @</p>
                                        </div>
                                    </div>
                                    <input type="hidden" name="tipo_circuns" type="radio" id="tipo_circuns_3" value="true">
                                    <div class="row form-group">
                                        <label for="nombre_empresa" class="col-sm-4 control-label"> </label>
                                        <div class="col-sm-4">
                                            <input type="text" name="nombre_empresa" id="nombre_empresa" class="form-control" size="25"  placeholder="Nombre de la institucion, ong,empresa..."  autofocus>
                                        </div>
                                        <div class="col-sm-4">
                                            <p>Nombre de la ONG, Institición, universidad, empresa...</p>
                                        </div>
                                    </div>
                                </div>



                                <div id="div_circunscripcion" class="row form-group">
                                    <label for="dbhost" class="col-sm-4 control-label">Numero de circunscripciones</label>
                                    <div class="col-sm-4">
                                        <p>
                                            <label>
                                                <input name="tipo_circuns" type="radio" id="tipo_circuns_0" value="false">
                                                4   </label></p>
                                        <p>
                                            <label>

                                                <input name="tipo_circuns" type="radio" id="tipo_circuns_1" value="true" checked="CHECKED">
                                                Solo una , por defecto </label>
                                        </p>


                                    </div>
                                    <div class="col-sm-4">
                                        <p>Si elige 4 circunscripciones tendrá: circunscripción estatal, autonómica, provincial y municipal</p>
                                    </div>
                                </div>


  <br/>
                              </fieldset>

                                <fieldset>
                                    <legend>Otros datos de configuración</legend>
                                <!--  -->
                                <div class="row form-group">
                                    <label for="Correo_insti" class="col-sm-4 control-label">Zona horia </label>
                                    <div class="col-sm-4">
                                        <div class="letra_c_user_red">El servidor tiene la sigiente hora:  <?php
                                            echo date("H:i:s");
                                            ?> </div><br/>

                                        <label>
                                            <input name="zona_horaria" type="radio" id="zona_horaria_0" value="false" onClick="deshabilita_hora()" checked="CHECKED">
                                            La hora es correcta
                                        </label>
                                        <br/>
                                        <label>
                                            <input name="zona_horaria" type="radio" id="zona_horaria_1" value="true" onClick="habilita_hora()">
                                            La hora NO es correcta
                                        </label>
                                    </div>
                                    <div class="col-sm-4">
                                        <p>Si la hora del servidor no es la de su zona horaria, indiquelo para no tener desajustes cuando habilite una votación o realice ciertas operaciones </p>
                                    </div>
                                </div>



                                <div id="div_hora"  class="caja_de_display" style="display:none" >
                                    <div class="row form-group">
                                        <label for="elCorreo_insti" class="col-sm-4 control-label"> Escoja su zona horaria</label>

                                        <div class="col-sm-4">
                                            <?php
                                            $regions = array(
                                                'Europa' => DateTimeZone::EUROPE,
                                                'America' => DateTimeZone::AMERICA,
                                                'Africa' => DateTimeZone::AFRICA,
                                                'Antartica' => DateTimeZone::ANTARCTICA,
                                                'Asia' => DateTimeZone::ASIA,
                                                'Atlantica' => DateTimeZone::ATLANTIC,
                                                'Indian' => DateTimeZone::INDIAN,
                                                'Pacific' => DateTimeZone::PACIFIC
                                            );
                                            $timezones = array();
                                            foreach ($regions as $name => $mask) {
                                                $zones = DateTimeZone::listIdentifiers($mask);
                                                foreach ($zones as $timezone) {
                                                    // Lets sample the time there right now
                                                    $time = new DateTime(NULL, new DateTimeZone($timezone));
                                                    // Us dumb Americans can't handle millitary time
                                                    $ampm = $time->format('H') > 12 ? ' (' . $time->format('g:i a') . ')' : '';
                                                    // Remove region name and add a sample time
                                                    $timezones[$name][$timezone] = substr($timezone, strlen($name) + 1) . ' - ' . $time->format('H:i') . $ampm;
                                                }
                                            }
// View
                                            echo '<select id="timezone" name="timezone" class="form-control custom-select"> ';
                                            foreach ($timezones as $region => $list) {
                                                print '<optgroup label="' . $region . '">' . "\n";
                                                foreach ($list as $timezone => $name) {
                                                    if ($timezone == "Europe/Madrid") {
                                                        $checked = "selected";
                                                    } else {
                                                        $checked = "";
                                                    }
                                                    echo '<option value="' . $timezone . '" ' . $checked . '>' . $name . '</option>' . "\n";
                                                }
                                                echo '<optgroup>' . "\n";
                                            }
                                            echo '</select>';
                                            ?>
                                        </div>
                                        <div class="col-sm-4"></div>
                                    </div>

                                    <div class="row form-group">
                                        <label for="nombre_empresa" class="col-sm-4 control-label"> </label>
                                    </div>
                                </div><br/>




                                <!--   -->
                                <div class="row form-group">
                                    <label for="nombre_web" class="col-sm-4 control-label">Idioma por defecto de la web</label>
                                    <div class="col-sm-4">

                                        <?php
                                        $carpeta_locale = "../locale"; //ruta actual
                                        $lista_locale = "";
                                        if (is_dir($carpeta_locale)) {
                                            if ($dir_locale = opendir($carpeta_locale)) {
                                                while (($archivo_locale = readdir($dir_locale)) !== false) {
                                                    if ($archivo_locale != '.' && $archivo_locale != '..' && $archivo_locale != '.htaccess' && $archivo_locale != 'index.html' && $archivo_locale != 'index.php' && $archivo_locale != 'messages.po' && $archivo_locale != 'messages.pot') {


                                                        if ($archivo_locale == "es_ES") {
                                                            $check_locale = "selected=\"selected\" ";
                                                        } else {
                                                            $check_locale = "";
                                                        }

                                                        $lista_locale .= "<option value=\"" . $archivo_locale . "\" $check_locale > " . $archivo_locale . "</option>";
                                                        $lista_locale .= $archivo_locale;
                                                    }
                                                }
                                                closedir($dir_locale);
                                            }
                                        }
                                        ?>
                                        <select name="idioma" class="form-control custom-select"  id="idioma" >
                                            <?php echo "$lista_locale"; ?>
                                        </select>

                                    </div>
                                    <div class="col-sm-4">
                                        <p>Idiomas incluidos en la carpeta LOCALE </p>


                                    </div>
                                </div>





                                <div class="row form-group">
                                    <label for="nombre_web" class="col-sm-4 control-label">Nombre de la web</label>
                                    <div class="col-sm-4">
                                        <input name="nombre_web" id="nombre_web" type="text" class="form-control" size="25"  placeholder="DEMOKRATIAN |  Centro de votaciones" required  autofocus /></td>
                                    </div>
                                    <div class="col-sm-4">

                                    </div>
                                </div>


                                <div class="row form-group">
                                    <label for="theme" class="col-sm-4 control-label">Tema del sitio</label>
                                    <div class="col-sm-4">
                                        <?php
                                        $carpeta = "../temas"; //ruta actual
                                        $lista = "";
                                        if (is_dir($carpeta)) {
                                            if ($dir = opendir($carpeta)) {
                                                while (($archivo = readdir($dir)) !== false) {
                                                    if ($archivo != '.' && $archivo != '..' && $archivo != '.htaccess' && $archivo != 'index.html' && $archivo != 'index.php') {

                                                        if ($archivo == "demokratian") {
                                                            $check = "selected=\"selected\" ";
                                                        } else {
                                                            $check = "";
                                                        }

                                                        $lista .= "<option value=\"" . $archivo . "\" $check > " . $archivo . "</option>";
                                                        $lista .= $archivo;
                                                    }
                                                }
                                                closedir($dir);
                                            }
                                        }
                                        ?>
                                        <select name="theme" class="form-control custom-select"  id="theme" >
                                            <?php echo "$lista"; ?>
                                        </select>

                                    </div>
                                    <div class="col-sm-4">
                                        Si no ha añadido uno deje el tema por defecto "demokratian".
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset>
                                <legend>Configuración del servidor de correo</legend>
                                <div class="row form-group">

                                    <label for="dbhost" class="col-sm-4 control-label">Forma de envio</label>
                                    <div class="col-sm-4">
                                        <p>
                                            <label>
                                                <input name="smtp" type="radio" id="smtp_0" value="true" checked="CHECKED" onClick="habilita_smtp()">
                                                smtp <br>
                                            </label>
                                        </p>
                                        <p>
                                            <label>
                                                <input name="smtp" type="radio" id="smtp_2" value="false"  onClick="deshabilita_smtp()">
                                                phpmail</label>
                                        </p>

                                    </div>
                                    <div class="col-sm-4">
                                        Muy recomendado usar  <strong>SMTP.</strong> <br/>
                                        La función phpmail() no funciona en la mayor parte de los servidores.
                                    </div>
                                </div>
                                <div id="div_smtp"  class="caja_de_display" >

                                    <div class="row form-group">
                                        <label for="theme" class="col-sm-4 control-label">servidor de correo</label>
                                        <div class="col-sm-4">
                                            <input name="server_correo" type="text"  class="form-control" id="server_correo"  placeholder="smtp.MiServidor.net" size="25"  />
                                        </div>
                                        <div class="col-sm-4">

                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <label for="user_correo" class="col-sm-4 control-label">Usuario  de correo</label>
                                        <div class="col-sm-4">
                                            <input name="user_correo" type="text"  class="form-control" id="user_correo"  placeholder="nombre usuario del correo" size="25" />
                                        </div>
                                        <div class="col-sm-4">

                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <label for="pass_correo" class="col-sm-4 control-label">Contraseña</label>
                                        <div class="col-sm-4">
                                            <input name="pass_correo" id="pass_correo" type="text" class="form-control" size="25" placeholder="contraseña del correo"  />
                                        </div>
                                        <div class="col-sm-4"> Contraseña de correo.
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <label for="dbhost" class="col-sm-4 control-label">Puerto del servidor de correo</label>
                                        <div class="col-sm-4">
                                            <input name="puerto_correo" id="puerto_correo" class="form-control" type="text" size="25" value="25" required  autofocus />
                                        </div>
                                        <div class="col-sm-4">
                                            Por defecto <strong>25</strong>.
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <label for="dbhost" class="col-sm-4 control-label">SMTPSecure</label>
                                        <div class="col-sm-4">

                                            <label>
                                                <input name="SMTPSecure" type="radio" id="SMTPSecure_0" value="false" checked="CHECKED">
                                                Deshabilitado</label>
                                            <label>
                                                <input name="SMTPSecure" type="radio" id="SMTPSecure_2" value="TLS" >
                                                TLS</label>
                                            <label>
                                                <input name="SMTPSecure" type="radio"  id="SMTPSecure_1" value="SSL" >
                                                SSL</label>
                                            <br>

                                        </div>
                                        <div class="col-sm-4">
                                            Por defecto <strong>Deshabilitado</strong>.
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <label for="dbhost" class="col-sm-4 control-label">Autentificación por SMTP (SMTPAuth)</label>
                                        <div class="col-sm-4">

                                            <label>
                                                <input name="SMTPAuth" type="radio" id="SMTPAuth_0" value="false">
                                                FALSE</label>

                                            <label>
                                                <input name="SMTPAuth" type="radio" id="SMTPAuth_1" value="true" checked="CHECKED">
                                                TRUE</label>
                                            <br>

                                        </div>
                                        <div class="col-sm-4">
                                            Por defecto <strong>True</strong>.
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <label for="dbhost" class="col-sm-4 control-label">Forma de envio de correo</label>
                                        <div class="col-sm-4">

                                            <label>
                                                <input name="tipo_envio" type="radio" id="tipo_envio_0" value="false" checked="CHECKED">
                                                IsSMTP</label>

                                            <label>
                                                <input type="radio" name="tipo_envio" value="true" id="tipo_envio_1">
                                                IsSendMail</label>
                                            <br>

                                        </div>

                                        <div class="col-sm-4">
                                            Algunos sevidores como 1&1 hay que usar IsSendMail. Por defecto <strong>IsSMTP</strong>.
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <label for="dbhost" class="col-sm-4 control-label">Envio html o texto plano(IsHTML)</label>
                                        <div class="col-sm-4">

                                            <label>
                                                <input name="IsHTML" type="radio" id="IsHTML_0" value="false" checked="CHECKED">
                                                FALSE</label>

                                            <label>
                                                <input name="IsHTML" type="radio" id="IsHTML_1" value="true">
                                                TRUE</label>
                                            <br>

                                        </div>
                                        <div class="col-sm-4">
                                            Por defecto <strong>False</strong>.
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <legend>Direccion de correo principal del sistema de votaciones</legend>

                                <div class="row form-group">
                                    <label for="email_env" class="col-sm-4 control-label">Direccion de correo general</label>
                                    <div class="col-sm-4">
                                        <input name="email_env" id="email_env" class="form-control" type="text"   size="25" required  placeholder="info1@demokratian.org" autofocus />
                                    </div>
                                    <div class="col-sm-4">
                                        Posteriormente mediante el panel de administración podra cambiar y configurar otras direcciones de correo.
                                    </div>
                                </div>



                            </fieldset>

                            <div class="row form-group">
                                <div class="col-sm-12">
                                    <button class="btn btn-ttc btn-lg btn-primary btn-block" type="submit">Configurar</button>
                                </div>
                            </div>
                        </form>

                    </div>
                    </div>

                    </div>

                </div>

            </div>
            <div id="cargando" > <img class="cargador" src='../temas/demokratian/images/loading.gif'/> <div  class="cargador2" > <h4>Esto puede tardar unos minutos...</h4></div>
            </div>
            <!--segunda fase de la configuración-->
            <div class="row" id="tercera_fase"> <div class="col-lg-12"> <br/>
                    <p class="bg-success"> Ha terminado con la segunda fase de la configuración, enseguida terminamos, puede pasar a la tercera fase &nbsp;&nbsp;<a href="install3.php" class="btn btn-primary btn-lg active" role="button">Tercera fase</a></p>
                    <br/></div>
            </div>
            <!--fin segunda fase-->

            <div id="footer" class="row">
                <div  class="pie_demokratia">

                    <div class="pie_demokratia2">
                        <a href="http://www.demokratian.org" target="_blank"><img src="../temas/demokratian/images/logo_pie.png" class="img-fluid"  alt="DEMOKRATIA | plataforma de votaciones"  /></a>
                    </div>
                    <div class="pie_demokratia1"> </div>

                </div>
            </div>




        </div>

        <script
          src="https://code.jquery.com/jquery-3.5.1.min.js"
          integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
          crossorigin="anonymous" ></script>
        <script>
          window.jQuery || document.write('<script src="../assets/jquery-3.5.1/jquery.min.js"><\/script>')
        </script>

        <script type="text/javascript">
                                                    $(document).ready(function () {
                                                        $("#tercera_fase").hide();
                                                        $("#cargando").hide();
                                                    });

                                                    $("#cargando").on("ajaxStart", function () {
                                                        // this hace referencia a la div con la imagen.
                                                        $(this).show();
                                                    }).on("ajaxStop", function () {
                                                        $(this).hide();
                                                    });
        </script>
   <!--<script src="js/jquery.validate.js"></script>-->
        <script src="../assets/bootstrap-4.5.0/js/bootstrap.min.js" ></script>
        <script src="configura_2.js" ></script>
        <script type='text/javascript'>
        function habilita_smtp() {
          $('#div_smtp').show('slow');
        }
        function deshabilita_smtp() {
          $('#div_smtp').hide('slow');
        }
        </script>

        <script type='text/javascript'>
            function habilita_local() {
              $('#div_local').show('slow');
              $('#div_circunscripcion').hide('slow');
            }
            function deshabilita_local() {
              $('#div_local').hide('slow');
              $('#div_circunscripcion').show('slow');
            }
        </script>

        <script type='text/javascript'>
            function habilita_hora() {
              $('#div_hora').show('slow');
            }
            function deshabilita_hora() {
              $('#div_hora').hide('slow')
            }

        </script>
    </body>
</html>

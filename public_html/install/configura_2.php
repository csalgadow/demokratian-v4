<?php
/*#############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
#############################################################################################################################################################*/
//error_reporting(E_ALL);
//ini_set('display_errors', '1');
$timezone = false;
include_once("../private/basicos_php/basico.php");
include_once("../private/config/config.inc.php");
if (isset($nombre_web)) { //miramos que no exista ya la variable en el archivo
    echo "ERROR#La configuracion de este paso ya se ha realizado o hay algun error en el archivo config";
} else {
    $nombre_web = fn_filtro_nodb($_POST['nombre_web']);
    $email_env = fn_filtro_nodb($_POST['email_env']);
    $tipo_envio = fn_filtro_nodb($_POST['tipo_envio']);
    $puerto_correo = fn_filtro_nodb($_POST['puerto_correo']);
    $pass_correo = fn_filtro_nodb($_POST['pass_correo']);
    $user_correo = fn_filtro_nodb($_POST['user_correo']);
    $server_correo = fn_filtro_nodb($_POST['server_correo']);
    $theme = fn_filtro_nodb($_POST['theme']);
    $SMTPAuth = fn_filtro_nodb($_POST['SMTPAuth']);
    $SMTPSecure = fn_filtro_nodb($_POST['SMTPSecure']);
    $IsHTML = fn_filtro_nodb($_POST['IsHTML']);
    $smtp = fn_filtro_nodb($_POST['smtp']);
    $correo_insti = fn_filtro_nodb($_POST['correo_insti']);
    $elCorreo_insti = fn_filtro_nodb($_POST['elCorreo_insti']);
    $nombre_empresa = fn_filtro_nodb($_POST['nombre_empresa']);
    $idioma = fn_filtro_nodb($_POST['idioma']);
    if ($correo_insti == "true") {
        $es_municipal = "true";
    }
    if ($correo_insti == "false") {
        $es_municipal = fn_filtro_nodb($_POST['tipo_circuns']);
    }

    if ($_POST['zona_horaria'] == "false") {
        $zona_horaria = "false";
    } else if ($_POST['zona_horaria'] == "true") {
        $zona_horaria = fn_filtro_nodb($_POST['timezone']);
    }

    //$es_municipal = fn_filtro_nodb($_POST['tipo_circuns']);




    #preparamos el string con los datos
    $string1 = "
###################################################################################################################################
##########                                               Configuración sitio                                             ##########
###################################################################################################################################
\$nombre_web = \"" . $nombre_web . "\";    // Nombre del sitio web
\$tema_web = \"" . $theme . "\";                       // Nombre del tema (carpeta donde se encuentra)
\$info_versiones = true;         //sistema de avisos de nuevas versiones, por defecto habilitado (true)";

    if ($zona_horaria == "false") {
    $string2 = "
\$timezone = false ;    // zona horaria";
    } else {
    $string2 = "
\$timezone = \"" . $zona_horaria . "\";    // zona horaria";
    }

    $string3 = "
\$defaul_lang = \"" . $idioma . "\";  //idioma por defecto

###################################################################################################################################
##########                                 Configuración para uso municipal                                              ##########
###################################################################################################################################
\$es_municipal = " . $es_municipal . ";  // true si vamos a trabajar solo con un tipo de circunscripción y no queremos que pregunte la provincia
                          //Si se usa la opción de correos institucionales debe de ser true

###################################################################################################################################
##########              Configuración para uso de correos institucionales de empresas u organizaciones                   ##########
###################################################################################################################################
\$insti = " . $correo_insti . ";   //si usa el sistema de inscripción de correos institucionales, por defecto false
\$term_mail = \"" . $elCorreo_insti . "\";    // terminación del correo institucional incluyendo la arroba, por defecto vacio
\$term_empre = \"" . $nombre_empresa . "\";    // Nombre de la empresa

###################################################################################################################################
##########                                 Configuración del uso de recaptcha                                            ##########
###################################################################################################################################
\$reCaptcha = false;  // true si ponemos en marcha el recaptcha
\$tipo_reCaptcha = \"reCAPTCHA_v2\"; // si queremos usar reCaptchaV2 o V3
\$reCAPTCHA_site_key = \"\";        //Clave pública que proporciona google recaptcha
\$reCAPTCHA_secret_key = \"\";      //Clave privada que proporciona google recaptcha
\$sensibilidad = 0.5;            // sensibilidad para reCaptcha V3

###################################################################################################################################
##########                                 Configuración de voto ponderado                                               ##########
###################################################################################################################################
\$votoPonderado = false;  // true si queremos que se permita voto ponderado en las encuestas

###################################################################################################################################
##########                                  Configuración de URLs amigables                                              ##########
###################################################################################################################################
\$UrlAmigable = true;  // true si queremos permitir urls amigables


?>";
#preparamos el string con los datos de correo

$string_correo1 = "
###################################################################################################################################
##########                          Direcciones de correo para los distintos tipos de envíos                             ##########
###################################################################################################################################
\$email_env = \"" . $email_env . "\"; //dirección de correo general
\$email_error = \"" . $email_env . "\"; //sitio al que enviamos los correos de los que tienen problemas y no están en la bbdd,
                                         //este correo es el usado si no hay datos en la bbdd de los contactos por provincias
\$email_control = \"" . $email_env . "\"; //Dirección que envía el correo para el control con interventores

\$email_error_tecnico = \"" . $email_env . "\";//correo electrónico del responsable técnico
\$email_sistema = \"" . $email_env . "\"; //correo electrónico del sistema, de momento incluido en el envió de errores de la bbdd

\$asunto_mens_error = \"Usuario de votaciones " . $nombre_web . " con problemas \"; //asunto del mensaje de correo cuando hay problemas de acceso
\$nombre_eq = \"Votaciones " . $nombre_web . "\"; //asunto del correo

\$nombre_sistema = \"Sistema de votaciones " . $nombre_web . "\"; // Nombre del sistema cuando se envía el correo de recuperación de clave
\$asunto = \"Recuperar tu contraseña en " . $nombre_web . "\";// asunto para recuperar la contraseña

###################################################################################################################################
#############         Configuración del correo smtp , solo si es tenemos como true la variable \$correo_smtp          #############
###################################################################################################################################
\$correo_smtp = " . $smtp . ";        //poner en false si queremos que el envió se realice con phpmail() y true si es por smtp
\$user_mail = \"" . $user_correo . "\";        // root de correo
\$pass_mail = \"" . $pass_correo . "\";  //  de correo
\$host_smtp = \"" . $server_correo . "\";        // localhost del correo
\$puerto_mail = " . $puerto_correo . ";
\$mail_sendmail = " . $tipo_envio . "; // en algunos servidores como 1¬1 hay que usar IsSendMail() en vez de IsSMTP() por defecto dejar en false";

    if ($SMTPSecure == "false") {
        $string_correo2 = "
\$mail_SMTPSecure = " . $SMTPSecure . "; //Set the encryption system to use - ssl (deprecated) or tls";
        } else {
        $string_correo2 = "
\$mail_SMTPSecure = \"" . $SMTPSecure . "\"; //Set the encryption system to use - ssl (deprecated) or tls";
        }

        $string_correo3 = "
\$mail_SMTPAuth = " . $SMTPAuth . "; //Whether to use SMTP authentication
\$mail_IsHTML = " . $IsHTML . "; //
\$mail_SMTPOptions = false; //por defecto false. Algunos servidores con certificados incorrectos no envían los correos por SMTP por lo que quitamos la validación de los certificados, NO SE RECOMIENDA EN ABSOLUTO usar esta opción en true.


?>";


  //  $string = $string1 . $string5 . $string4 . $string2 . $string3;
  $string = $string1 . $string2 . $string3;


  $file = "../private/config/config.inc.php"; //archivo que hay que modificar

  #Abrimos el fichero en modo de escritura

  $fh = fopen($file, 'r+');


    fseek($fh, -3, SEEK_END); // nos vamos 3 caraceres antes para quitar el cierre de php
    if (fwrite($fh, $string) === FALSE){ //escribimos en el archivo
     echo 'ERROR# <div class="alert alert-danger"> No se puede escribir en el archivo ' .$file. '</div>';
     exit;
   }
  //  fwrite($fh, $string) or die("Could not write file!");
    fclose($fh);  //  Cerramos el fichero
    chmod($file, 0600); //cambiamos los permisos de ese archivo a 600
    $mns1="<div class=\"alert alert-info\"> Se han guardado los datos generales correctamente</div>";

// y ahora modificamos el otro archivo con los datos de  configuración de correo

  $string_correo = $string_correo1 . $string_correo2 . $string_correo3;

  $file_correo = "../private/config/config-correo.inc.php"; //archivo que hay que modificar

  #Abrimos el fichero en modo de escritura

  $fh_correo = fopen($file_correo, 'r+');


    fseek($fh_correo, -4, SEEK_END); // nos vamos 3 caraceres antes para quitar el cierre de php
    if (fwrite($fh_correo, $string_correo) === FALSE){ //escribimos en el archivo
     echo 'ERROR# <div class="alert alert-danger"> No se puede escribir en el archivo ' .$file_correo. '</div>';
     exit;
   }
    //fwrite($fh_correo, $string_correo) or die("Could not write file!");
    fclose($fh_correo);  //  Cerramos el fichero
    chmod($file_correo, 0600); //cambiamos los permisos de ese archivo a 600
    $mns2= "<div class=\"alert alert-info\"> Se han guardado los datos correctamente de configuración de correo</div>";


echo "OK#".$mns1.$mns2;
}
?>

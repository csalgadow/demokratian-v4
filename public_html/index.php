<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
$nombre_fichero = '../private/config/config.inc.php';

//require ("basicos_php/lang.php");

if (!file_exists($nombre_fichero)) {
    header('Location: install/index.php');
    echo "<p>El fichero de configuracion no existe</p><p>si es la primera vez que instala la aplicación debe de ir a la carpeta de instalación (SuSitio.org/install)</p>";
} else {
    // Inicializar la sesión.
    require_once("../private/inc_web/blog.inc.php");
//    session_name($usuarios_sesion);
    // Destruir todas las variables de sesión.
    $_SESSION = array();

    // Si se desea destruir la sesión completamente, borre también la cookie de sesión.
    // Nota: ¡Esto destruirá la sesión, y no la información de la sesión!
    if (ini_get("session.use_cookies")) {
        $params = session_get_cookie_params();
        setcookie(
          session_name(),
            '',
            time() - 42000,
            $params["path"],
            $params["domain"],
            $params["secure"],
            $params["httponly"]
        );
    }

    session_start();
    ;
    // Finalmente, destruir la sesión.
    session_destroy();

// si estamos en la pagina de interventores, procedemos a borrar tambien todas las sessiones
    if(isset($_GET['c'])){
        if ($_GET['c']=="acceso_interventores"){
//          session_name($usuarios_sesion2);
          session_start();
          // session_name($usuarios_sesion);
          // Destruir todas las variables de sesión.
          $_SESSION = array();

          // Si se desea destruir la sesión completamente, borre también la cookie de sesión.
          // Nota: ¡Esto destruirá la sesión, y no la información de la sesión!
          if (ini_get("session.use_cookies")) {
              $params = session_get_cookie_params();
              setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]
              );
          }

          // Finalmente, destruir la sesión.
        session_destroy();
        }
    }
///////   fin borrado de sesiones de los interventores

    //// incluimos idioma
    require_once("../private/basicos_php/lang.php");
    require_once("../private/basicos_php/url.inc.php");

    $con = @mysqli_connect("$host", "$hostu", "$hostp") or die("no se puede conectar");
    mysqli_set_charset($con, "utf8");
    $db = @mysqli_select_db($con, "$dbn") or die("no se puede acceder a la tabla");

// funcion contar palabras para recortar textos
    function cortar_palabras($texto, $largor = 20, $puntos = "...")
    {
       $palabras = explode(' ', $texto);
       if (count($palabras) > $largor)
       {
         return implode(' ', array_slice($palabras, 0, $largor)) ." ". $puntos;
       } else
             {
               return $texto;
             }
    }
// fin función  contar palabras



    ?>
    <!DOCTYPE html>
    <html lang="es">
        <head>
          <?php include("temas/$tema_web/blogHead.php"); ?>
        </head>

        <body>
          <header>
            <?php $carga="OK";
        if (isset($_GET['c'])) {
            $idcab=2; // cabecera otras páginas
        } else {
            $idcab=1; // cabecera página index
        }
        include("../private/blog/bloque_header.php"); ?>
          </header>

          <main role="main">
              <!-- PRINCIPAL  messaging and featurettes, DON´T REMOVE
              ================================================== -->
            <?php
              if (isset($_GET['c'])) {
                  if (file_exists($laPagina)) {
                     include($laPagina);
                   } else {
                      include("../private/blog/error404.php");
                  }
                } else {
                  $idpg=3;
                  include("../private/blog/bloque_activos.php");
                } ?>
                        <!-- START THE TEXT -->
              </main>
            <?php include("../private/votacion/modals.php"); ?>
            <?php include("../private/blog/pie.php"); ?>

          <script src="assets/bootstrap-4.5.0/js/bootstrap.min.js"> </script>
          <script src="assets/js/modals.js"> </script>
          <script src="assets/js/contact_me.js"> </script>
          <script type="text/javascript">
              // limpiamos la carga de modal para que no vuelva a cargar lo mismo -->
              $('#contacta').on('hidden.bs.modal', function () {
              $(this).removeData('bs.modal');
              });
          </script>
            <?php
    } ?>
            <script src="assets/jquery-sticky/jquery.sticky.js"> </script>
            <script src="assets/js/blogMain.js"> </script>


    </body>
</html>

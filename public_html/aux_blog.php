<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################

// modal auxilar para zona publica
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Fecha en el pasado
$timezone =false; // para que no de error de falta de variable, quizas seria mejor sacar de basico.php esto y ponerlo en un archivo separado
include_once("../private/basicos_php/basico.php");
include_once('../private/basicos_php/errores.php');

    if (file_exists('../private/config/config.inc.php')) {
    include_once('../private/config/config.inc.php');
    }

    /* para los accesos normales desde el blog */
    if(isset($_GET['a'])){
      $carga="OK";
      $pagina = fn_filtro_nodb($_GET['a']);
      $arr = explode('/', $pagina); // separamos la cadena en 3 datos
      $pagina=$arr[0];

      if(isset($arr[1])){  // en el blog, si esta en una subcarpeta lo ponemos en segundo termino
       $carpeta=$arr[1];
       $pagina = $carpeta.'/'.$pagina;
        }

        $variables=array();

       if (isset($arr[2])){  // si hemos pasado variables por la url
        parse_str($arr[2],$variables);  // como tenemos un string tipo url , lo parseamos para sacar el dato de las variables
        }

        $laPagina ="../private/blog/".$pagina.".php";

        if (file_exists($laPagina)) {
          include($laPagina);
        }else{
          include("../private/blog/error404.php");
        }
     }

    /*  accesos desde los interventores*/

    if(isset($_GET['i'])){
      $cargaI="OK";
      $pagina = fn_filtro_nodb($_GET['i']);

      $laPagina ="../private/interventores/".$pagina.".php";
      if (file_exists($laPagina)) {
        include($laPagina);
      }else{
        include("../private/blog/error404.php");
      }
     }

     /*  accesos desde los codificadores*/

     if(isset($_GET['c'])){
       $cargaI="OK";
       $pagina = fn_filtro_nodb($_GET['c']);

       $arr = explode('/', $pagina); // separamos la cadena en 3 datos
       $pagina=$arr[0];

       if(isset($arr[1])){  // en el blog, si esta en una subcarpeta lo ponemos en segundo termino
        $carpeta=$arr[1];
        $pagina = $carpeta.'/'.$pagina;
         }
       $laPagina ="../private/codificadores/".$pagina.".php";
       if (file_exists($laPagina)) {
         include($laPagina);
       }else{
         include("../private/blog/error404.php");
       }
      }

     if(isset($_GET['install'])){
       $carga="OK";
       $pagina = fn_filtro_nodb($_GET['install']);

       $laPagina ="install/".$pagina.".php";

       if (file_exists($laPagina)) {
         include($laPagina);
       }else{
         include("../private/blog/error404.php");
       }
      }

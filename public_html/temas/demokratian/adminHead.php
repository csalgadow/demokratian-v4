<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
 ?>
 <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 <meta name="description" content="">
 <meta name="generator" content="">
 <meta name="keywords" content=""/>
 <meta name="language" content="es">
 <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
 <meta http-equiv="pragma" CONTENT="no-cache">
 <meta http-equiv="Pragma" content="no-cache">
 <meta http-equiv="Expires" content="0">
 <meta name="author" content="Carlos Salgado">

 <title><?php echo "$nombre_web"; ?></title>

 <!-- Favicons -->
 <link href="temas/<?php echo "$tema_web"; ?>/images/icono.png" rel="icon"  type="image/png" />
 <link href="temas/<?php echo "$tema_web"; ?>/apple-touch-icon.png"  rel="apple-touch-icon" />
 <link href="temas/<?php echo "$tema_web"; ?>/favicon.ico" type="image/x-icon"  rel="shortcut icon" />
 <link href="temas/<?php echo "$tema_web"; ?>/apple-touch-icon-144x144.png"  rel="apple-touch-icon" sizes="144x144" />

 <!-- Google Fonts -->
 <link href="https://fonts.googleapis.com/css?family=Raleway:400,500,700|Roboto:400,900" rel="stylesheet">

 <!-- Bootstrap core CSS -->
 <link href="assets/bootstrap-4.5.0/css/bootstrap.css" rel="stylesheet">
 <!-- font-awesome CSS Files -->
 <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
 <!-- DataTables for Bootstrap4 CSS File -->
 <link rel="stylesheet" type="text/css" href="assets/DataTables/datatables.min.css" />
 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.bootstrap4.min.css" />
 <!-- Template Main CSS File -->
 <link href="temas/<?php echo "$tema_web"; ?>/css/adminStyle.css" rel="stylesheet">

 <!--[if lt IE 9]>

 <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
 <![endif]-->


 <script
   src="https://code.jquery.com/jquery-3.5.1.min.js"
   integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
   crossorigin="anonymous" ></script>
 <script>
   window.jQuery || document.write('<script src="assets/jquery-3.5.1/jquery.min.js"><\/script>')
 </script>

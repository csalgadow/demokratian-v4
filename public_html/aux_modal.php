<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
// modal aunxilar zona administración
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

//header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
//header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Fecha en el pasado


if(isset($_GET['c'])){

  require_once("../private/config/config.inc.php");
  require_once("../private/inc_web/conexion.php");
  include_once('../private/basicos_php/basico.php');
  require_once("../private/basicos_php/url.inc.php");
  include_once("../private/basicos_php/url.php");

  if (file_exists($laPagina)) {
    $cargaA="OK";
    if (file_exists($laPagina)) {

      include($laPagina);
    }else{
      include("../private/blog/error404.php");
    }
  }else{
    include("../private/blog/error404.php");
  }
}

else if(isset($_GET['i'])){    /*  accesos desde los interventores*/
  $timezone =false; // para que no de error de falta de variable, quizas seria mejor sacar de basico.php esto y ponerlo en un archivo separado
  include_once("../private/basicos_php/basico.php");

  $cargaI="OK";
  $lapagina = fn_filtro_nodb($_GET['i']);
      require_once("../private/interventores/verifica.php");
      include_once("../private/basicos_php/url.inc.php");
      $cadena=decrypt_url($lapagina,$clave_encriptacion);  //desncriptamos la cadena
      $arr = explode('/', $cadena); // separamos la cadena en 3 datos
      $carpeta=$arr[0];
      $pagina=$arr[1];

      $variables=array();

      if (isset($arr[2])){  // si hemos pasado variables por la url
      parse_str($arr[2],$variables);  // como tenemos un string tipo url , lo parseamos para sacar el dato de las variables
    }
    $laPagina ="../private/". $carpeta."/".$pagina.".php";

    include_once('../private/interventores/seguri_inter.php');

    if (file_exists($laPagina)) {
      include($laPagina);
    }else{
      include("../private/blog/error404.php");
    }
  }else{
    echo "Hay un error de algun tipo";
  }

?>

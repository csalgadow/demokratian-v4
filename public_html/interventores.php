<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Fecha en el pasado
include_once('../private/interventores/verifica.php');
include_once("../private/basicos_php/url.php");
include('../private/inc_web/tiempoSession.php');
//require ("../basicos_php/lang.php");
?>

<!DOCTYPE html>
<html lang="es">
    <head>
      <?php include("temas/$tema_web/votacionesHead.php"); ?>
    </head>
    <body>
  <header>

        <?php include("../private/votacion/cabecera.php"); ?>
  </header>

  <main role="main">
    <div class="row">



                <!-- ================= zona central de la web  ========== -->

                  <?php  $cargaI="OK";
                    if(isset($_GET['c'])){
                      if (file_exists($laPagina)) {
                        include($laPagina);
                      }else{
                        include("../private/blog/error404.php");
                      }
                    }else{
                      include_once("../private/interventores/inicio_interventores.php");
                    }
                  ?>

                <!-- ================= / fin zona central de la web  ========== -->



       </div>
     </main>

                <?php include("../private/votacion/modals.php"); ?>
                <?php include("../private/blog/pie.php"); ?>


        <script src="assets/bootstrap-4.5.0/js/bootstrap.min.js"> </script>
        <script src="assets/bootstrap-4.5.0/js/bootstrap.bundle.js"> </script>
        <!--  <script src="assets/jquery-sticky/jquery.sticky.js"></script>-->

        <script src="assets/js/modals.js"> </script>
        <!-- necesario para habilitar balloons tooltip -->
<script>
  $(function() {
    $('[data-toggle="tooltip"]').tooltip()
  })
</script>
<!-- abrir menu admin de forma automatica -->
<script type="text/javascript">
        $('.dropdown').hover(function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(100);
}, function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(100);
});
</script>
    </body>
</html>

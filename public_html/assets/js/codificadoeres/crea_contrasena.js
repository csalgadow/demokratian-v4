// JavaScript Document
/*
 Jquery Validation using jqBootstrapValidation
 example is taken from jqBootstrapValidation docs
 */


$(document).ready(function () {
    $("#contactForm").bind("submit",function(){
        // Capturamnos el boton de envío
        //var btnEnviar = $("#btnEnviar");

        $.ajax({
            type: "POST",
            //url: $(this).attr("action"),
            url: "aux_blog.php?c=procesar_rec_contr/procesar",
            data:$(this).serialize(),
            cache: false,
            beforeSend: function(){
                /*
                * Esta función se ejecuta durante el envió de la petición al
                * servidor.
                * */
                // btnEnviar.text("Enviando"); Para button
                //btnEnviar.val("Enviando"); // Para input de tipo button
                //btnEnviar.attr("disabled","disabled");
            },
            complete:function(data){
                /*
                * Se ejecuta al termino de la petición
                * */
                //btnEnviar.val("Enviar formulario");
                //btnEnviar.removeAttr("disabled");
            },
            success: function(data){
                /*
                * Se ejecuta cuando termina la petición y esta ha sido
                * correcta
                * */
                var result = data.trim().split("##");
                if (result[0] == 'OK') {
                  $('#caja_contrasena').hide("slow");
                  $('#success').html(result[1]);
                  $('#success').show("slow");
                  $('#contactForm').trigger("reset");

                } else if (result[0] == 'ERROR') {
                  $("#success").show("slow");
                  $('#success').html(result[1]);
                  $('#contactForm').trigger("reset");
                }else {
                  $("#success").show("slow");
                  $('#success').html(data);
                  $('#contactForm').trigger("reset");
                }
            },
            error: function(data){
                /*
                * Se ejecuta si la peticón ha sido erronea
                * */
                $('#success').html("<div class='alert alert-danger'>");
                $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append(" " +contrasenaForm+ " , hay un error</button>");
            }
        });
        // Nos permite cancelar el envio del formulario
        return false;
    });
});


/*When clicking on Full hide fail/success boxes */
$('#name').focus(function() {
    $('#success').html('');
});

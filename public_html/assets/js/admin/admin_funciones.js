/*#############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                     DEMOKRATIAN   http://demokratian.org                                                                ###
###                                                   Copyright (C) 2020 CARLOS SALGADO WERNER                                                              ###
###                              Este programa ha sido creado por Carlos Salgado Werner (http://carlos-salgado.es)                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia GNU Affero General Public License según es       ###
### publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia GNU Affero General Public License para más detalles.                                    ###
### Debería haber recibido una copia de la Licencia GNU Affero General Public License. Si no ha sido así, puede encontrarla en https://www.gnu.org/licenses ###
###                                             -------------  English Version  --------------                                                              ###
### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the    ###
### Free Software Foundation, either version 3 of the License, or (at your option) any later version.                                                       ###
###                                                                                                                                                         ###
### This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        ###
### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero        ###
### General Public License along with this program.  If not, see https://www.gnu.org/licenses/.                                                             ###
###                                             -----------------------------------------------                                                             ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                                 No puedes eliminar este aviso de licencia,                                                              ###
###                  ni el enlace con el copy que se ve al ejecutar el programa en el pie de las páginas index.php y resto de páginas.                      ###
###                                                                                                                                                         ###
###############################################################################################################################################################
#############################################################################################################################################################*/
// JavaScript Document

function habilita_estatal() {
    $('#autonomico').hide('slow');
    $('#provincial').hide('slow');
    $('#local').hide('slow');
    $('#g_trabajo').hide('slow');
    $('#g_trabajo_general').hide('slow');
    $('#g_municipal').hide('slow');
}

function habilita_autonomico() {
  $('#autonomico').show('slow');
  $('#provincial').hide('slow');
  $('#local').hide('slow');
  $('#g_trabajo').hide('slow');
  $('#g_trabajo_general').hide('slow');
  $('#g_municipal').hide('slow');
}

function habilita_provincial() {
  $('#autonomico').hide('slow');
  $('#provincial').show('slow');
  $('#local').hide('slow');
  $('#g_trabajo').hide('slow');
  $('#g_trabajo_general').hide('slow');
  $('#g_municipal').hide('slow');
}

function habilita_local() {
  $('#autonomico').hide('slow');
  $('#provincial').hide('slow');
  $('#local').show('slow');
  $('#g_trabajo').hide('slow');
  $('#g_trabajo_general').hide('slow');
  $('#g_municipal').hide('slow');

}
function habilita_g_trabajo() {
  $('#autonomico').hide('slow');
  $('#provincial').hide('slow');
  $('#local').hide('slow');
  $('#g_trabajo').show('slow');
  $('#g_trabajo_general').hide('slow');
  $('#g_municipal').hide('slow');
}
function habilita_g_trabajo_general() {
  $('#autonomico').hide('slow');
  $('#provincial').hide('slow');
  $('#local').hide('slow');
  $('#g_trabajo').hide('slow');
  $('#g_trabajo_general').show('slow');
  $('#g_municipal').hide('slow');
}
function habilita_municipal() {
  $('#autonomico').hide('slow');
  $('#provincial').hide('slow');
  $('#local').hide('slow');
  $('#g_trabajo').hide('slow');
  $('#g_trabajo_general').hide('slow');
  $('#g_municipal').show('slow');
}

////////////////////////////////////////////

function pon_opciones1() {
    $('#accion_opciones').show('slow');
    $('#recuento').show('slow');
    $('#tipo_s_4').show('slow');
    $('#tipo_s_6').show('slow');
    $('#opciones').show('slow');
    $('#tipo_encuesta').hide('slow');
}
function pon_opciones2() {
    $('#accion_opciones').show('slow');
    $('#recuento').hide('slow');
    $('#tipo_s_4').hide('slow');
    $('#tipo_s_6').hide('slow');
    $('#opciones').show('slow');
    $('#tipo_encuesta').hide('slow');
}
function pon_opciones3() {
    $('#accion_opciones').show('slow');
    $('#recuento').hide('slow');
    $('#tipo_s_4').show('slow');
    $('#tipo_s_6').show('slow');
    $('#opciones').show('slow');
    $('#tipo_encuesta').hide('slow');
}
function pon_opciones4() {
    $('#accion_opciones').hide('slow');
    $('#recuento').hide('slow');
    $('#tipo_s_4').show('slow');
    $('#tipo_s_6').show('slow');
    $('#opciones').hide('slow');
    $('#tipo_encuesta').hide('slow');
}
function pon_opciones5() {
    $('#accion_opciones').show('slow');
    $('#recuento').hide('slow');
    $('#tipo_encuesta').show('slow');
    $('#tipo_s_4').show('slow');
    $('#tipo_s_6').show('slow');
}
function pon_opciones6() {
    $('#opciones').show('slow');
}
function pon_opciones7() {
    $('#opciones').hide('slow');
}

////////////////////////////////////////////
